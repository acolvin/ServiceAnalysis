package internalQueues;

import java.util.logging.Logger;

    public class Message {
	private String correlationID=null;
	private Long messageTime=null;
	private Long messageTimeOut=null;
	private Queue replyQueue=null;
	
	protected Message(){}
	
	public String getCorrelationID() {
		return correlationID;
	}

	public void setCorrelationID(String correlationID) {
		this.correlationID = correlationID;
	}

	public void setExpiry(long ttl)
	{
		messageTime=System.currentTimeMillis();
		messageTimeOut=messageTime+ttl;
	}
	
	public Queue getReplyTo()
	{
		return replyQueue;
	}
	public void setReplyQueue(Queue q)
	{
		replyQueue=q;
	}
	
	public Queue setTempReplyQueue()
	{
		QueueServer qs=QueueServer.getInstance();
		Queue q=qs.createTemporaryQueue();
		replyQueue=q;
		return q;
	}
	
	public boolean isActive()
	{
		Logger.getLogger("Message").fine("message time out value: "+messageTimeOut);
		if(messageTimeOut==null)return true;
		else if( messageTimeOut<System.currentTimeMillis())return false;
		else return true;
	}
	
}
