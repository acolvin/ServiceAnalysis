package internalQueues;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

 public class Queue {

	protected String name = "";

	public String getQueueName() {
		return name;
	}

	public boolean addMessage(Message m) {
		try {
			
			queue.put(m);
			Logger.getLogger("QueueSystem").fine("Message Added to Queue, "+name);
		} catch (InterruptedException e) {
			Logger.getLogger("QueueSystem").info("Putting Message on Queue, "+name +", has been Interrupted");
		}
		return true;
	}

	protected Message receiveMessage(long timeout) throws ServerRunningException, QueueDeletedException {
		Message m=null;
		boolean gotit=false;
		try {
			do
			{	
				if(!deleted & QueueServer.getInstance().isRunning())
					m=queue.poll(timeout, TimeUnit.MILLISECONDS);
				
				if(deleted && m==null) throw new QueueDeletedException(name);
				if(!QueueServer.getInstance().isRunning()& m==null) throw new ServerRunningException("The server has been stopped");
				
				if(m==null)gotit=true; //we timedout
				else gotit=m.isActive();
				
			}while(!gotit);
			if(m==null) Logger.getLogger("QueueSystem").fine("Waiting for Message Timed Out");
		} catch (InterruptedException e) {
			Logger.getLogger("QueueSystem").info("Retrieving Message from Queue, "+name +", has been Interrupted");
		}
		return m;
	}

	protected Message receiveMessage() throws QueueDeletedException, ServerRunningException {
		boolean gotit=false;
		Message m=null;
		try {
			do
			{
				while(!deleted & m==null & QueueServer.getInstance().isRunning())
					m=queue.poll(500,TimeUnit.MILLISECONDS);
				//m=queue.take();
				
				if(deleted && m==null) throw new QueueDeletedException(name);
				if(!QueueServer.getInstance().isRunning()& m==null) throw new ServerRunningException("The server has been stopped");
				
				if(m==null)gotit=true; //we timed out - should never happen
				else gotit=m.isActive(); //we got an active message
				 
				
			}while(!gotit);
				if(m==null) Logger.getLogger("QueueSystem").fine("Waiting for Message Timed Out");
		} catch (InterruptedException e) {
			
			Logger.getLogger("QueueSystem").info("Retrieving Message from Queue, "+name +", has been Interrupted");
		}
		return m;
	}

	public void clearQueue() {
		queue.clear();
	}

	private ArrayBlockingQueue<Message> queue = new ArrayBlockingQueue<Message>(1000,true);
	protected static int number = 0;

	public Queue(String name) throws QueueNamingException, ServerRunningException
	{

		if(name==null | name.equals("")) throw new QueueNamingException("Queue Must Have a Name");
		else if(QueueServer.getInstance().isRunning()) throw new ServerRunningException("The Server is in Running State!  You cannot create a queue");
		else this.name=name;
	}
	
	protected Queue() {
		super();
	}
	protected boolean deleted=false;
}