package systemSimulator;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.*;

public class SimulatorUI implements WindowListener {

	public SimulatorUI()
	{
		
		CreateUI();
	}
	
	JFrame mainFrame=null;
	ModelPanel mp=new ModelPanel();
	
	private void CreateUI()
	{
		mainFrame=new JFrame("System Simulator");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setBounds(10, 10, 500, 350);
		//mp.setOpaque(true);
		JScrollPane sp=new JScrollPane(mp);
		mp.scroll=sp;
		//mainFrame.setLayout(new BoxLayout(mainFrame.getContentPane(), BoxLayout.X_AXIS));
		//sp.add(mp);
		//JPanel jp=new JPanel();
		//jp.setMinimumSize(mp.getPreferredSize());
		sp.setMinimumSize(mp.getPreferredSize());
		mainFrame.setContentPane(sp);
		//jp.add(sp);
		//sp.setMinimumSize(mp.getPreferredSize());
		mp.setBounds(mainFrame.getBounds());
		mp.setOpaque(true);
		//jp.setVisible(true);
		sp.setVisible(true);
		mp.setVisible(true);
		mainFrame.pack();
		//sp.setMinimumSize(mp.getPreferredSize());
		//mainFrame.setMinimumSize(mp.getPreferredSize());
		
		mainFrame.addWindowListener(this);
	}
	
	public void ShowUI()
    {
	javax.swing.SwingUtilities.invokeLater(new Runnable()
	{
	    public void run()
	    {
		mainFrame.setVisible(true);
		// createAndShowGUI();
	    }
	});
    }
	
//Window Listener Events
	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
//end of window listener events
	
}
