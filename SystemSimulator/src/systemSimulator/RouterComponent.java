package systemSimulator;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import javax.swing.SwingUtilities;

public class RouterComponent extends SimComponent{
	
	public String component="";
	public String function="";
	//public int mean=100;
	//public int standardDeviation=50;
	public String queueName=null;
	//public int temperature=0;
	public boolean isRunning=false;
	
	public RouterComponent(String component, String function,String queueName)
	{
		this.component=component;
		this.function=function;
		this.queueName=queueName;
		Simulator.simLog.fine("Router id: "+id);
		resize=true;
	}
	
	public String toString()
	{
		String o=id+",router,"+location.x+","+location.y+","+location.width+","+location.height+",";
		o+=component+","+function+","+queueName+",";
		o+=""+threads;
		return o;
	}
	
	public void paint(Graphics g) {
       Graphics2D clippedg2d=(Graphics2D)g.create(); 
       Graphics2D g2d=(Graphics2D)g.create();
       if(resize){resize=false;resize(g);}
       g2d.setColor(Color.lightGray);
       clippedg2d.clipRect(location.x, location.y, location.width, location.height);
       Graphics2D bold2d=(Graphics2D) clippedg2d.create();
       bold2d.setFont(bold2d.getFont().deriveFont(Font.BOLD));
       g2d.fill3DRect(location.x, location.y, location.width, location.height, true);
       g2d.draw3DRect(location.x, location.y, location.width, location.height, true);
       g2d.setColor(Color.BLACK);
       bold2d.drawString("Router", 3+location.x, location.y+1+12);
       clippedg2d.drawString(component, 3+location.x, location.y+1+30);
       clippedg2d.drawString(function, 3+location.x, location.y+1+42);
       clippedg2d.drawString(Integer.toString(threads), 3+location.x, location.y+1+66);
       //clippedg2d.drawString(Integer.toString(mean)+"/"+Integer.toString(standardDeviation), 3+location.x, location.y+1+48);
       int x=location.x+location.width;
       int y=location.y+location.height/2;
       //for(SimComponent s:children)
       //{
    	//   g2d.drawLine(x, y, s.location.x, s.location.y+s.location.height/2);
       //}
       //if(isRunning)
       //{
    //	   g2d.setColor(getTemperatureColour());
    	//   g2d.fillOval(location.x-10, location.y+50-10, 10, 10);
       //}
    }

	@Override
	public void resize(Graphics g) {
		Graphics2D g2d=(Graphics2D) g;
		int width=50;
		int i=SwingUtilities.computeStringWidth( g2d.getFontMetrics(),  component );
		width=Math.max(i+3, width);
		i=SwingUtilities.computeStringWidth( g2d.getFontMetrics(),  function );
		width=Math.max(i+3, width);
		//i=SwingUtilities.computeStringWidth( g2d.getFontMetrics(),  Integer.toString(mean)+"/"+Integer.toString(standardDeviation) );
		//width=Math.max(i+3, width);
		location.width=width+3;
		
	}

	private ArrayList<ClientComponent> children=new ArrayList<ClientComponent>();
	
	@Override
	public void addSubordinate(SimComponent sc) {

		if(sc instanceof ClientComponent)
		{
			((ClientComponent)sc).router=this;
			children.add((ClientComponent)sc);
		}
	}
	
	

	public void removeClient(ClientComponent c)
	{
		children.remove(c);
		c.unsetRouter();
	}

	@Override
	public void deleteComponent() {
		for(ClientComponent c:children)
			c.unsetRouter();
		
	}

	@Override
	protected String getChildString() {
		if(children.size()<1)return null;
		String o="children";
		for(ClientComponent c:children)
			o+=","+c.id;
		
		return o;
	}

	@Override
	public void removeSubordinate(SimComponent sc)
	{
		removeClient((ClientComponent) (sc));
		
	}  

}
