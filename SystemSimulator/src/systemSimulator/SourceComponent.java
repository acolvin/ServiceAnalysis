package systemSimulator;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import javax.swing.SwingUtilities;

public class SourceComponent extends SimComponent implements ComponentListener{
	
	public String component="";
	public String function="";
	public int mean=100;
	public int standardDeviation=50;
	public String queueName=null;
	public int temperature=0;
	//public boolean isRunning=false;
	public int wait;
	
	public SourceComponent(String component, String function,String queueName)
	{
		this.component=component;
		this.function=function;
		this.queueName=queueName;
		resize=true;
		Simulator.simLog.fine("Source id: "+id);
	}
	
	public String toString()
	{
		String o=id+",source,"+location.x+","+location.y+","+location.width+","+location.height+",";
		o+=component+","+function+","+mean+","+standardDeviation+","+queueName+",";
		o+=""+threads+","+wait;
		return o;
	}
	
	private Color getTemperatureColour()
	{
		if(temperature<15) return Color.GREEN;
		if(temperature<30) return Color.ORANGE;
		if(temperature<40) return Color.PINK;
		else return Color.RED;
	}
	public void setTemperatureFromValue(int value)
	{
		if(value<=2000) temperature=Math.max(0,temperature-3);
		else if(value<=5000 & temperature<15) temperature=Math.min(25,temperature+1);
		else if(value<=5000 & temperature>=30) temperature=Math.max(15,temperature-3);
		else if(value<=5000) temperature = Math.max(20, temperature-1);
		else if(value>5000 & value<=10000) temperature=Math.min(35,temperature+1);
		else if(value>10000 & value<=29900) temperature=Math.min(35,temperature+3);
		else temperature=Math.min(60,temperature+40);
		sum+=value;
		count++;
		repaint();
	}
	
	public void resetValues()
	{
		sum=0;
		count=0;
		temperature=0;
	}
	
	int sum=0;
	int count=0;
	
	public void paint(Graphics g) {
       Graphics2D clippedg2d=(Graphics2D)g.create();
       
       Graphics2D g2d=(Graphics2D)g.create();
       if(resize){resize=false;resize(g);}
       g2d.setColor(Color.YELLOW);
       clippedg2d.clipRect(location.x, location.y, location.width, location.height);
       Graphics2D bold2d=(Graphics2D) clippedg2d.create();
       bold2d.setFont(bold2d.getFont().deriveFont(Font.BOLD));
       g2d.fill3DRect(location.x, location.y, location.width, location.height, true);
       g2d.draw3DRect(location.x, location.y, location.width, location.height, true);
       g2d.setColor(Color.BLACK);
       bold2d.drawString("source", 3+location.x, location.y+1+12);
       clippedg2d.drawString("Component: "+component, 3+location.x, location.y+1+30);
       clippedg2d.drawString("Function: "+function, 3+location.x, location.y+1+42);
       clippedg2d.drawString("mean/stddev: "+Integer.toString(mean)+"/"+Integer.toString(standardDeviation), 3+location.x, location.y+1+54);
       clippedg2d.drawString("Threads: "+Integer.toString(threads), 3+location.x, location.y+1+66);
       clippedg2d.drawString("Pause: "+Integer.toString(wait), 3+location.x, location.y+1+78);
       
       int x=location.x+location.width;
       int y=location.y+location.height/2;
       for(SimComponent s:children)
       {
    	   if(children.indexOf(s)!=children.lastIndexOf(s))
    	   { //well we have multiple lines
    		   int c=0;
    		   for(SimComponent sc:children)
    			   if(sc==s)c++;
    		   g2d.drawLine(x, y, s.location.x, s.location.y+s.location.height/2);
    		   g2d.drawString(""+c, (x+s.location.x)/2, (y+s.location.y+s.location.height/2)/2);
    	   }
    	   else
    		   g2d.drawLine(x, y, s.location.x, s.location.y+s.location.height/2);
       }
       if(ModelPanel.simRunning)
       {
    	   g2d.drawLine(3+location.x, location.y+92,3+location.x+SwingUtilities.computeStringWidth( g2d.getFontMetrics(), "Running Values"),location.y+92);
    	   g2d.drawString("Running Values", location.x+3, location.y+92);
    	   g2d.drawString("Mean: "+Integer.toString(count==0?0:sum/count), 3+location.x, location.y+105);
    	   g2d.drawString("Iterations: "+count, 3+location.x, location.y+117);
    	   g2d.setColor(getTemperatureColour());
    	   g2d.fillOval(location.x-10, location.y+50-10, 10, 10);
       }
    }

	@Override
	public void resize(Graphics g) {
		Graphics2D g2d=(Graphics2D) g;
		int width=50;
		int i=SwingUtilities.computeStringWidth( g2d.getFontMetrics(),  "Component: "+component );
		width=Math.max(i+6, width);
		i=SwingUtilities.computeStringWidth( g2d.getFontMetrics(),  "Function: "+function );
		width=Math.max(i+6, width);
		i=SwingUtilities.computeStringWidth( g2d.getFontMetrics(),  "mean/stddev: "+Integer.toString(mean)+"/"+Integer.toString(standardDeviation) );
		width=Math.max(i+6, width);
		i=SwingUtilities.computeStringWidth( g2d.getFontMetrics(),  "Delay: "+wait );
		width=Math.max(i+6, width);
		i=SwingUtilities.computeStringWidth( g2d.getFontMetrics(), "Running Values");
		width=Math.max(i+3, width);
		if(count!=0)
			i=SwingUtilities.computeStringWidth( g2d.getFontMetrics(), "Mean: "+sum/count);
		width=Math.max(i+3, width);
		i=SwingUtilities.computeStringWidth( g2d.getFontMetrics(), "Iterations: "+count);
		width=Math.max(i+3, width);
		location.width=width;
		location.height=120;
		
		
	}
	
	public ArrayList<String> getClientCallNames()
	{
		ArrayList<String> a=new ArrayList<String>();
		for(SimComponent s:children)
		{
			String qs=null;
			ClientComponent c=(ClientComponent)s;
			if(c.router!=null)qs=c.router.component+"."+c.router.function+",";
			else qs="";
			qs=qs+c.component+"."+c.function;
			
			//System.out.println(qs);
			a.add(qs);
					
					
		}
		
		
		return a;
	}

	private ArrayList<SimComponent> children=new ArrayList<SimComponent>();
	
	@Override
	public void addSubordinate(SimComponent sc) {
		if(sc instanceof SourceComponent)
		{
			systemSimulator.Simulator.simLog.warning("Source cannot be driven");
			
		}
		if(sc instanceof RouterComponent) systemSimulator.Simulator.simLog.warning("Source cannot be controlled by a pool");
		else
		{
			if(sc==this) return;
			sc.addListener(this);
			children.add(sc);
		}
		
	}
	
	public void removeSubordinate(SimComponent sc)
	{
		while(children.remove(sc)){}
	}
	
	protected String getChildString() {
		if(children.size()<1)return null;
		String o="children";
		for(SimComponent c:children)
			o+=","+c.id;
		
		return o;
	}  

	@Override
	public void deleteComponent() {
		children.clear();
		
	}

	@Override
	public void componentDeleted(ComponentDeleteEvent e) {
		
		children.remove(e.component);
		
	}  
}
