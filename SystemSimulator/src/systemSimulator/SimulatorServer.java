package systemSimulator;

import java.util.ArrayList;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import ServiceAnalysisClient.SAClient;

public class SimulatorServer extends Thread {

	public SimulatorServer(String component, String function, int mean, int stddev, ArrayList<String> queues , int wait)
	{
		this.component=component;
		this.function=function;
		this.queues=queues;
		this.mean=mean;
		this.stddev=stddev;
		this.wait=wait;
		
		try {
			context = new InitialContext();
			factory = (ConnectionFactory) context.lookup(factoryName);		
			//a.queue = (Queue) a.context.lookup(a.destName);
			connection = factory.createConnection();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			//session.createQueue(queuename);
			
			
			setup=true;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setup=false;
		}
		
		
		
		
		
	}
	
	String component,function,queuename;
	int mean,stddev;
	SAClient sac=SAClient.getClient();
	int wait;
	
	boolean setup=true;
	
	Context context = null;
    ConnectionFactory factory = null;
    Connection connection = null;
    String factoryName = "connectionFactory";
    Destination dest = null;
    Queue queue=null;
    Session session=null;
    ArrayList<String> queues=null;
    private TemporaryQueue tempQ=null;
    private MessageConsumer receiver=null;
    
    
    private Integer msgid=0;
    
    public void run()
    {
    	//System.out.println("Starting server "+this.currentThread().getName());
    	String txn;
    	MessageProducer sender=null;
		try {
			Thread.sleep((long)(Math.random()*100));
			connection.start();
			
		} catch (Exception e1) {
			e1.printStackTrace();
			setup=false;
		}
	    
		
		
		
    	while(setup )
    	{
    		long start=System.currentTimeMillis();
    		long waitstart=start;
    		txn=sac.startTiming(component, function, "BW");
    		int time=Simulator.getSample(false, mean, stddev);
    		
    		try {
    			for(String qn:queues)
    			{	
    				String qns[]=qn.split(",");
    				dest = (Destination) context.lookup(qns[0]);
    				String msg=txn;
    				if(qns.length==2)msg=msg+","+qns[1];
    				sender = session.createProducer(dest);
    				sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
    				
    				if(tempQ==null)
    					tempQ=session.createTemporaryQueue();
    				TextMessage m=session.createTextMessage(msg);
    				m.setJMSReplyTo(tempQ);
    				Integer currentMsg=msgid++;
    				m.setJMSCorrelationID(currentMsg.toString());
    				
    				sender.setTimeToLive(20000l);
    				//System.out.println("sending message to queue "+qn);
    				sender.send(m);
    				
    				sender.close();
    				
    				//System.out.println("message sent to queue "+qn);
    				//wait for replay on the temp queue
    				if(receiver==null)
    					receiver = session.createConsumer(tempQ);
    				
    				boolean gotMyMessage=false;
    				Message mm =null;
    				while(!gotMyMessage)
    				{
    					mm = receiver.receive(30000l);
    					//tempQ.delete();
    					if(mm==null)
    					{
    						System.err.println("Server: Oh dear we timed out waiting for the reply");
    						gotMyMessage=true; //no really true but need out of the loop
    					}
    					else
    					{
    						if(mm.getJMSCorrelationID().equals(currentMsg.toString()))
    						{
    							gotMyMessage=true;
    						}
    						else
    						{
    							System.err.println("Server: Got an old message - retrying");
    						}
    					}
    				}
    				long wait2=System.currentTimeMillis();
    				long waitReply=wait2-waitstart;
    				waitstart=wait2;
    				sac.addWait(txn, (int) waitReply);
    				
    			}
				Thread.sleep(time);
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		sac.endTiming(txn);
    		try {
				Thread.sleep(Math.max(0, Simulator.getSample(true, wait, wait/2)));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
    	}
    	
    	{
            // close the context
	   
            if (context != null) {
                try {
                    context.close();
                } catch (NamingException exception) {
                    exception.printStackTrace();
                }
            }

            // close the connection
            if (connection != null) {
                try {
                    if(session!=null)session.close();
                    connection.close();
                } catch (JMSException exception) {
                    exception.printStackTrace();
                }
            }
    	}}
	
	
}
