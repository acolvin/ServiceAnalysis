package systemSimulator;


import java.awt.*;
import java.awt.event.*;
import java.text.NumberFormat;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.text.*;


public class SimCompDiag extends JPanel implements ActionListener, FocusListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2826188934673423346L;
	JTextField compField, funcField; 
	JFormattedTextField meanField, stddevField, waitField;
	JSpinner threadSpinner;
    
    boolean valuesSet = false;
   
    final static int GAP = 10;
    JFrame myWindow;
    ModelPanel mp;
    int cx=0,cy=0;
    String compType;
    
    public SimCompDiag(JFrame myWindow,ModelPanel model,int x, int y, String type ) {
    	this.myWindow=myWindow;
    	mp=model;
    	cx=x;
    	cy=y;
    	compType=type;
    	//System.out.println(compType);
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        
        JPanel leftHalf = new JPanel() {
            /**
			 * 
			 */
			private static final long serialVersionUID = 2683984283573649535L;

			//Don't allow us to stretch vertically.
            public Dimension getMaximumSize() {
                Dimension pref = getPreferredSize();
                return new Dimension(Integer.MAX_VALUE,
                                     pref.height);
            }
        };
        leftHalf.setLayout(new BoxLayout(leftHalf,
                                         BoxLayout.PAGE_AXIS));
        leftHalf.add(createEntryFields());
        leftHalf.add(createButtons());
 
        add(leftHalf);
        add(new Panel());
    }
    
    SimComponent currentComponent;
    boolean edit=false;
    
    public SimCompDiag(JFrame myWindow,ModelPanel model,SimComponent component ) {
    	this.myWindow=myWindow;
    	mp=model;
    	if(component instanceof SourceComponent) compType="source";
    	else if(component instanceof ClientComponent)compType="client";
    	else compType="router";
    	edit=true;
    	currentComponent=component;
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        
        JPanel leftHalf = new JPanel() {
            /**
			 * 
			 */
			private static final long serialVersionUID = -7385950784451451110L;

			//Don't allow us to stretch vertically.
            public Dimension getMaximumSize() {
                Dimension pref = getPreferredSize();
                return new Dimension(Integer.MAX_VALUE,
                                     pref.height);
            }
        };
        leftHalf.setLayout(new BoxLayout(leftHalf,
                                         BoxLayout.PAGE_AXIS));
        leftHalf.add(createEntryFields());
        leftHalf.add(createButtons());
 
        add(leftHalf);
        add(new Panel());
    }
	
    protected JComponent createButtons() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        JButton button;
        if(edit) button = new JButton("Save Component");
        else button = new JButton("Create Component");
        button.addActionListener(this);
        button.setActionCommand(edit? "save":"create");
        panel.add(button);
 
        button = new JButton("Cancel");
        button.addActionListener(this);
        button.setActionCommand("clear");
        panel.add(button);
 
        //Match the SpringLayout's gap, subtracting 5 to make
        //up for the default gap FlowLayout provides.
        panel.setBorder(BorderFactory.createEmptyBorder(0, 0,
                                                GAP-5, GAP-5));
        return panel;
    }
	
    private String getComponentValue(String Field)
    {
    	SourceComponent sc=null;
    	ClientComponent cc=null;
    	RouterComponent rc=null;

    	if(compType.equals("source"))sc=(SourceComponent)currentComponent;
    	if(compType.equals("client"))cc=(ClientComponent)currentComponent;
    	if(compType.equals("router"))rc=(RouterComponent)currentComponent;
    	if(Field.equals("comp")&sc!=null) return sc.component;
    	if(Field.equals("comp")&cc!=null) return cc.component;
    	if(Field.equals("comp")&rc!=null) return rc.component;
    	if(Field.equals("func")&sc!=null) return sc.function;
    	if(Field.equals("func")&cc!=null) return cc.function;
    	if(Field.equals("func")&rc!=null) return rc.function;
    	if(Field.equals("mean")&sc!=null) return ""+sc.mean;
    	if(Field.equals("mean")&cc!=null) return ""+cc.mean;
    	if(Field.equals("stddev")&sc!=null) return ""+sc.standardDeviation;
    	if(Field.equals("stddev")&cc!=null) return ""+cc.standardDeviation;
    	if(Field.equals("thread")&sc!=null) return ""+sc.threads;
    	if(Field.equals("thread")&cc!=null) return ""+cc.threads;
    	if(Field.equals("thread")&rc!=null) return ""+rc.threads;
    	if(Field.equals("wait")&sc!=null) return ""+sc.wait;
    	
    	return "";
    }
	
    protected JComponent createEntryFields() {
        JPanel panel = new JPanel(new SpringLayout());
        
		String[] labelStrings = new String[1];
        
        if(compType.equals("router"))
        {
        	ArrayList<String> a=new ArrayList<String>();
        	
        		a.add("Component: ");
        		a.add("Function: ");
        		a.add("Threads: ");
        		labelStrings=a.toArray(labelStrings);
        	
        }
        else if(compType.equals("client"))
        {
        	ArrayList<String> a=new ArrayList<String>();
    	
        	a.add("Component: ");
        	a.add("Function: ");
        	a.add("Mean: ");
        	a.add("Std Dev: ");
        	a.add("Threads: ");
        	labelStrings=a.toArray(labelStrings);
       
        }else //if(compType.equals("server"))
        {
        	ArrayList<String> a=new ArrayList<String>();
    	
        	a.add("Component: ");
        	a.add("Function: ");
        	a.add("Mean: ");
        	a.add("Std Dev: ");
        	a.add("Pause Time: ");
        	a.add("Threads: ");
        	
        	labelStrings=a.toArray(labelStrings);
       
        }
        JLabel[] labels = new JLabel[labelStrings.length];
        JComponent[] fields = new JComponent[labelStrings.length];
        int fieldNum = 0;
 
        //Create the text field and set it up.
        compField  = new JTextField();
        compField.setColumns(25);
        if(edit)compField.setText(getComponentValue("comp"));
        fields[fieldNum++] = compField;
 
        funcField = new JTextField();
        funcField.setColumns(25);
        if(edit)funcField.setText(getComponentValue("func"));
        fields[fieldNum++] = funcField;
 
        
        if(!compType.equals("router"))
        {
        	NumberFormat format = NumberFormat.getInstance();
        	NumberFormatter formatter = new NumberFormatter(format);
        	formatter.setValueClass(Integer.class);
        	formatter.setMinimum(1);
        	formatter.setMaximum(30000);
        	
        	meanField = new JFormattedTextField(formatter);
        	//meanField=new JTextField();
        	meanField.setColumns(10);
        	if(edit)meanField.setText(getComponentValue("mean"));
        	fields[fieldNum++] = meanField;
        	
        	stddevField=new JFormattedTextField(formatter);
        	//stddevField=new JTextField();
        	stddevField.setColumns(10);
        	if(edit)stddevField.setText(getComponentValue("stddev"));
        	fields[fieldNum++] = stddevField;
        	
        	if(compType.equals("source"))
        	{
        		waitField=new JFormattedTextField(formatter);
            	//stddevField=new JTextField();
            	waitField.setColumns(10);
            	if(edit)waitField.setText(getComponentValue("wait"));
            	fields[fieldNum++] = waitField;
        	}
        }
        int startValue=1;
        //System.out.println(getComponentValue("thread"));
        if(edit)startValue=Integer.parseInt(getComponentValue("thread"));
        threadSpinner = new JSpinner(new SpinnerNumberModel(startValue,1,1000,1));
        
        fields[fieldNum++] = threadSpinner;
        
        
        
 
        //Associate label/field pairs, add everything,
        //and lay it out.
        for (int i = 0; i < labelStrings.length; i++) {
            labels[i] = new JLabel(labelStrings[i],
                                   JLabel.LEFT);
            labels[i].setLabelFor(fields[i]);
            panel.add(labels[i]);
            panel.add(fields[i]);
 
            //Add listeners to each field.
            JTextField tf = null;
            if (fields[i] instanceof JSpinner) {
                tf = getTextField((JSpinner)fields[i]);
            } else {
                tf = (JTextField)fields[i];
            }
            tf.addActionListener(this);
            tf.addFocusListener(this);
        }
        SpringUtilities.makeCompactGrid(panel,
                                       labelStrings.length, 2,
                                       GAP, GAP, //init x,y
                                       GAP, GAP/2);//xpad, ypad
        return panel;
    }
	
    public JFormattedTextField getTextField(JSpinner spinner) {
        JComponent editor = spinner.getEditor();
        if (editor instanceof JSpinner.DefaultEditor) {
            return ((JSpinner.DefaultEditor)editor).getTextField();
        } else {
            System.err.println("Unexpected editor type: "
                               + spinner.getEditor().getClass()
                               + " isn't a descendant of DefaultEditor");
            return null;
        }
    }
    
    //Workaround for formatted text field focus side effects.
    protected void selectItLater(Component c) {
        if (c instanceof JFormattedTextField) {
            final JFormattedTextField ftf = (JFormattedTextField)c;
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    ftf.selectAll();
                }
            });
        }
    }
    
	@Override
	public void focusGained(FocusEvent e) {
		
	    Component c = e.getComponent();
        if (c instanceof JFormattedTextField) {
            selectItLater(c);
        } else if (c instanceof JTextField) {
            ((JTextField)c).selectAll();
        }
    
	}

	@Override
	public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		//System.out.println(arg0.getActionCommand());
		
		if(arg0.getActionCommand().equals("save"))
		{ 
			if(checkEntries())
			{
				if(compType.equals("source"))
				{//System.out.println("edit source");
					SourceComponent s=(SourceComponent)currentComponent;
					s.component=compField.getText();
					s.function=funcField.getText();
					s.resize=true;
					s.mean=Integer.parseInt(meanField.getText().replaceAll(",", ""));
					s.standardDeviation=Integer.parseInt(stddevField.getText().replaceAll(",", ""));
					s.wait=Integer.parseInt(waitField.getText().replaceAll(",", ""));
					s.threads=(Integer)threadSpinner.getValue();
					
				}
				else if(compType.equals("router"))
				{
					RouterComponent s=(RouterComponent)currentComponent;
					s.component=compField.getText();
					s.function=funcField.getText();
					s.resize=true;
					
					s.threads=(Integer)threadSpinner.getValue();
				}else
				{
					ClientComponent s=(ClientComponent)currentComponent;
					s.component=compField.getText();
					s.function=funcField.getText();
					s.resize=true;
					s.mean=Integer.parseInt(meanField.getText());
					s.standardDeviation=Integer.parseInt(stddevField.getText());
					s.threads=(Integer)threadSpinner.getValue();
					
				}
				mp.dialogShown=false;
				Simulator.simLog.fine("dialog set to: "+mp.dialogShown);
				mp.repaint();
				myWindow.dispose();
			}
		}
		if(arg0.getActionCommand().equals("create"))
		{
			
			//System.out.println(checkEntries());
			if(checkEntries())
			{
				SimComponent sc;
				if(compType.equals("source"))
				{
					SourceComponent s=new SourceComponent(compField.getText(),funcField.getText(),"test");
					s.location.x=cx;
					s.location.y=cy;
					s.resize=true;
					s.mean=Integer.parseInt(meanField.getText().replaceAll(",", ""));
					s.standardDeviation=Integer.parseInt(stddevField.getText().replaceAll(",", ""));
					s.wait=Integer.parseInt(waitField.getText().replaceAll(",", ""));
					s.threads=(Integer)threadSpinner.getValue();
					sc=s;
				}
				else if(compType.equals("router"))
				{
					RouterComponent s=new RouterComponent(compField.getText(),funcField.getText(),"test");
					s.location.x=cx;
					s.location.y=cy;
					s.resize=true;
					s.threads=(Integer)threadSpinner.getValue();
					sc=s;
				} else
				{
					ClientComponent s=new ClientComponent(compField.getText(),funcField.getText(),"test");
					s.location.x=cx;
					s.location.y=cy;
					s.resize=true;
					
					s.mean=Integer.parseInt(meanField.getText().replaceAll(",", ""));
					s.standardDeviation=Integer.parseInt(stddevField.getText().replaceAll(",", ""));
					
					s.threads=(Integer)threadSpinner.getValue();
					sc=s;
				}
				mp.addSimulatorComponent(sc);
				mp.dialogShown=false;
				Simulator.simLog.fine("dialog set to: "+mp.dialogShown);
				mp.repaint();
				myWindow.dispose();
			}
			
		}
		if(arg0.getActionCommand().equals("clear"))
		{
			mp.dialogShown=false;
			Simulator.simLog.fine("dialog set to: "+mp.dialogShown);
			myWindow.dispose();
		}
		
	}
	public boolean checkEntries()
	{
		if(compField.getText().equals("")) return false;
		if(funcField.getText().equals("")) return false;
		if(compType.equals("router"))return true;
		if(meanField.getText().equals("")) return false;
		if(stddevField.getText().equals("")) return false;
		if(compType.equals("client")) return true;
		if(waitField.getText().equals("")) return false;
		return true;
	}
}
