package systemSimulator;

import java.awt.SplashScreen;
import java.io.File;
import java.util.ArrayList;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import ServiceAnalysisClient.SAClient;

public class Simulator {
	// --component=component --function=function --mean=nnn --stddev=nnn --threads=nnn --simulatorClient=queuename <--queue=queueName> 
	public static void main(String[] args)
	{
		String component="component",function="function",queue=null;
		int mean=100,stddev=120,threads=1;
		ArrayList<Simulator> sim=new ArrayList<Simulator>();
		boolean isServer=true,isRouter=false,isClient=false,isGUI=true;
		ArrayList<String> queues=new ArrayList<String>();
		String filename="timings.csv";
		int wait=0;
		String level=System.getProperty("Simulator.logLevel","info");
		if(level.equals("fine"))
			simLog.setLevel(Level.FINE);
		else
			simLog.setLevel(Level.WARNING);
		LogManager.getLogManager().getLogger(Logger.GLOBAL_LOGGER_NAME).setLevel(Level.FINE); 
		//System.out.println("logger: "+log.getLevel());
		Handler[] hh=simLog.getHandlers();
		ConsoleHandler ch=new ConsoleHandler();
		ch.setLevel(Level.FINEST);
		simLog.addHandler(ch);
		
		
		
		for(String s:args)
		{
			if(!s.startsWith("-D"))isGUI=false;
			if(s.startsWith("--component=")) component=s.substring(12);
			if(s.startsWith("--function=")) function=s.substring(11);
			if(s.startsWith("--mean=")) mean=new Integer(s.substring(7));
			if(s.startsWith("--stddev=")) stddev=new Integer(s.substring(9));
			if(s.startsWith("--threads=")) threads=new Integer(s.substring(10));
			if(s.startsWith("--queue=")) queues.add(s.substring(8));
			if(s.startsWith("--client=")) {isServer=false;isRouter=false;isClient=true;queue=s.substring(9);}
			if(s.startsWith("--file=")) filename=s.substring(7);
			if(s.startsWith("--router=")) {isServer=false;isClient=false;isRouter=true;queue=s.substring(9);}
			if(s.startsWith("--wait=")) wait=new Integer(s.substring(7));
		}
		if(isGUI)
		{
				isServer=false;
				final SplashScreen splash = SplashScreen.getSplashScreen();
		        if (splash == null) {
		            System.out.println("System Simulator");
		            System.out.println("Copyright 2013, Andrew Colvin");
		            
		        }
		        else
		        {
		        	
		        	try {
						Thread.sleep(5000l);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        	splash.close();
		        }
		        
				SimulatorUI s=new SimulatorUI();
				s.ShowUI();
		}
		if(isServer)
		{
			for(int i=0;i<threads;i++)
				sim.add(new Simulator(0,component,function,mean,stddev,queues,wait));
		}
		if(isClient)
		{
			for(int i=0;i<threads;i++)
			{
				
				sim.add(new Simulator(1,component,function,mean,stddev,queues,queue));
			}
		}
		if(isRouter)
		{
			for(int i=0;i<threads;i++)
			{
				
				sim.add(new Simulator(2,queue));
			}
		}
		
		SAClient.getClient().setOutputFile(new File(filename));
		for(Simulator s:sim)
		{
			//System.out.println("Starting simulatorClient");
			s.start();
			//System.out.println("started client");
		}
		try {
			Thread.sleep(Long.MAX_VALUE);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public static Logger simLog=Logger.getAnonymousLogger();//.getLogger("sim");
	
	//hgfjhfsdfjk
	public Simulator(int mode,String component,String function,int mean, int stddev, ArrayList<String> queues,int wait)
	{
		if(mode==0)this.isServer=true;
		if(mode==1)this.isClient=true;
		if(mode==2)this.isRouter=true;
		if(isServer)
		{
			//System.out.println("creating server from simulator");
			this.server=new SimulatorServer(component,function,mean,stddev,queues,wait);
		}
	}
	
	public Simulator(int mode,String component,String function,int mean, int stddev, ArrayList<String> queues, String clientQueue)
	{
		if(mode==0)this.isServer=true;
		if(mode==1)this.isClient=true;
		if(mode==2)this.isRouter=true;
		if(isClient)
		{
			simulatorClient=new SimulatorClient(component,function,mean,stddev,queues,clientQueue);
		}
		
	}
	
	public Simulator(int mode,String routerQueue)
	{
		if(mode==0)this.isServer=true;
		if(mode==1)this.isClient=true;
		if(mode==2)this.isRouter=true;
		if(isRouter)
		{
			simulatorRouter=new SimulatorRouter(routerQueue);
		}
	}
	
	boolean isServer=false, isClient=false,isRouter=false;
	
	SimulatorServer server=null;
	SimulatorClient simulatorClient=null;
	SimulatorRouter simulatorRouter=null;
	
	
	private void start()
	{
		if(isServer) server.start();
		else if(isClient)simulatorClient.start();
		else if(isRouter)simulatorRouter.start();
	}
	

	
	
	public static int getSample(boolean normal,double mean, double stdDev)
	{
	    double mean2=mean*mean;
	    double variance=stdDev*stdDev;
	    double  mu=Math.log(mean2/Math.sqrt(variance+mean2));
	    double sigma=Math.sqrt(Math.log1p(variance/(mean2)));
	    int i=0;
	    if(!normal){
		double dd=box_muller(0.0, 1.0);
		i=(int) Math.exp(mu+sigma*dd);//(Math.random()*3000d);
	    } else {
		i=(int) box_muller(mean, stdDev);
	    }
	    return i;
	}
	
	public static double box_muller(double m, double s)	/* normal random variate generator */
	{				        /* mean m, standard deviation s */
		double x1, x2, w, y1;
		double y2=0;;
		boolean use_last = false;

		if (use_last)		        /* use value from previous call */
		{
			y1 = y2;
			use_last = true;
		}
		else
		{
			do {
				x1 = 2.0 *  Math.random()- 1.0;
				x2 = 2.0 * Math.random() - 1.0;
				w = x1 * x1 + x2 * x2;
			} while ( w >= 1.0 );

			w = Math.sqrt( (-2.0 * Math.log( w ) ) / w );
			y1 = x1 * w;
			y2 = x2 * w;
			use_last = true;
		}

		return( m + y1 * s );
	}

	
	
	
	

}
