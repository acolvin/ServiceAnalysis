package systemSimulator;

import internalQueues.QueueNamingException;
import internalQueues.QueueServer;
import internalQueues.ServerRunningException;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JViewport;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;


import ServiceAnalysisClient.SAClient;

import systemSimulator.simulators.iSimulatorClient;
import systemSimulator.simulators.iSimulatorRouter;
import systemSimulator.simulators.iSimulatorServer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

public class ModelPanel extends JPanel implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6673171590881092164L;


	public ModelPanel()
	{
		SAClient.getClient().setVMid("sim");
		//Simulator.simLog.setLevel(Level.ALL);
		
		setBorder(BorderFactory.createLineBorder(Color.black));
		setBackground(Color.white);
		//addSimulatorComponent(new SourceComponent("testof long string that will not fit","test","test"));
		//addSimulatorComponent(new SourceComponent("short one","test","test"));
		JMenuItem menuItem = new JMenuItem("add source...");
		menuItem.setActionCommand("source");
	    menuItem.addActionListener(this);
	    popup.add(menuItem);
	    menuItem = new JMenuItem("add client...");
	    menuItem.setActionCommand("client");
	    menuItem.addActionListener(this);
	    popup.add(menuItem);
	    menuItem = new JMenuItem("add router...");
	    menuItem.setActionCommand("router");
	    menuItem.addActionListener(this);
	    popup.add(menuItem);
	    popup.addSeparator();
	    menuItem = new JMenuItem("Remove Component");
	    menuItem.setActionCommand("remove");
	    menuItem.addActionListener(this);
	    popup.add(menuItem);
	    menuItem = new JMenuItem("Edit Component...");
	    menuItem.setActionCommand("edit");
	    menuItem.addActionListener(this);
	    popup.add(menuItem);
	    //menuItem = new JMenuItem("Produce shell commands");
	    //menuItem.setActionCommand("shell");
	    //menuItem.addActionListener(this);
	    //popup.add(menuItem);
	    popup.addSeparator();
	    menuItem = new JMenuItem("Start Simulation");
	    menuItem.setActionCommand("simulate");
	    menuItem.addActionListener(this);
	    popup.add(menuItem);
	    popup.addSeparator();
	    menuItem = new JMenuItem("Save Simulation");
	    menuItem.setActionCommand("save");
	    menuItem.addActionListener(this);
	    popup.add(menuItem);
	    menuItem = new JMenuItem("Load Simulation");
	    menuItem.setActionCommand("load");
	    menuItem.addActionListener(this);
	    popup.add(menuItem);
	    popup.addSeparator();
	    menuItem = new JMenuItem("Stop Simulation");
	    menuItem.setActionCommand("stop");
	    menuItem.addActionListener(this);
	    simPopup.add(menuItem);
	    
	    currentPopup=popup;
	    
		addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				
				if(e.getClickCount()==2 && sc!=null)
				{
					sc.resize=true;
					repaint();
				}
			}
	        public void mousePressed(MouseEvent e) {
	        	
	        	if(e.isPopupTrigger()) 
	        	{
	        		startDrag=null; endDrag=null;
	        		showPopup(e);
	        	}
	        	else if(e.isControlDown() & !simRunning)
	        	{ //draw line to connect components
	        		SimComponent sc2=findComponent(e.getX(),e.getY());
	        		sc=null;
	        		scxoffset=0;
	        		scyoffset=0;
	        		if(sc2!=null)
	        		{
	        			startDrag=new Point(sc2.location.x+sc2.location.width,sc2.location.y+25);
	        			endDrag=new Point(e.getX(),e.getY());
	        			dragFrom=sc2;
	        		}
	        	}
	        	else if(!e.isControlDown())
	        	{	//System.out.println("dragging component");
	        		startDrag=null; endDrag=null; dragFrom=null;
	        		sc=null;
	        		scxoffset=0;
	        		scyoffset=0;
	        		for(SimComponent s:components)
	        		{
	        			if(s.location.contains(e.getPoint())) 
	        			{	
	        				scxoffset=e.getX()-s.location.x;
	        				scyoffset=e.getY()-s.location.y;
	        				sc=s;
	        			}
	        		}
	        	}
	        }
	        
	        public void mouseReleased(MouseEvent e)
	        {
	        	if(e.isPopupTrigger())
	        	{
	        		showPopup(e);
	        	}
	        	else if(dragFrom!=null)
	        	{
	        		SimComponent dragTo=findComponent(e.getX(),e.getY());
	        		if(dragTo==null)
	        		{
	        			startDrag=null; endDrag=null; dragFrom=null;
	        			repaint();
	        		}
	        		else
	        		{
	        			if(!e.isShiftDown())
	        			{
	        				dragFrom.addSubordinate(dragTo);
	        				
	        			}
	        			else
	        			{
	        				dragFrom.removeSubordinate(dragTo);
	        				
	        			}
	        			dragFrom=null; endDrag=null; startDrag=null;
        				repaint();
	        		}
	        	}
	        	else repaint();
        		//sc=null;
        		//scxoffset=0;
        		//scyoffset=0;
	        }
	        
	        public void showPopup(MouseEvent e)
	        {
	        	sc=null;
        		scxoffset=0;
        		scyoffset=0;
	        	popupx=e.getX();
	        	popupy=e.getY();
	        	currentPopup.show(e.getComponent(), e.getX(), e.getY());
	        }
	    });

	    addMouseMotionListener(new MouseAdapter() {
	        public void mouseDragged(MouseEvent e) {
	        	if(dragFrom!=null)
	        	{
	        		endDrag.x=e.getX();
	        		endDrag.y=e.getY();
	        		repaint();
	        	}
	        	if(sc!=null)
	        		moveSimulatorComponent(sc,e.getX(),e.getY());
	        }
	    });
	}
	
	private void drawline()
	{
		int x,w,y,h;
		x=Math.min(startDrag.x, endDrag.x);
		y=Math.min(startDrag.y, endDrag.y);
		w=Math.abs(startDrag.x- endDrag.x);
		h=Math.abs(startDrag.y-endDrag.y);
		repaint(x,y,w,h);
	}
	
	private Point startDrag=null;
	private Point endDrag=null;
	private SimComponent dragFrom=null;
	
	
	public Dimension getMinimumSize()
	{
		return new Dimension(500,350);
	}
	
	public Dimension getPreferredSize()
	{
		int w=500,h=350;
		for(SimComponent sc:components)
		{
			w=Math.max(sc.location.x+sc.location.width+20, w);
			h=Math.max(sc.location.y+sc.location.height+20, h);
			
		}
		return new Dimension(w,h);
	}
	
	private int popupx=0;
	private int popupy=0;
	
	private SimComponent sc=null;
	private int scxoffset=0,scyoffset=0;
	
	public void addSimulatorComponent(SimComponent sc)
	{
		sc.mp=this;
		components.add(sc);
		//dialogShown=false;
		//Simulator.simLog.fine("dialogShown set to: "+dialogShown);
	}
	
	public void startSimulation()
	{
		//saveSimulation();
		clientSims.clear();
		routerSims.clear();
		sourceSims.clear();
		currentPopup=simPopup;
		simRunning=true;
		ArrayList<ClientComponent> clients=new ArrayList<ClientComponent>();
		ArrayList<RouterComponent> routers=new ArrayList<RouterComponent>();
		ArrayList<SourceComponent> sources=new ArrayList<SourceComponent>();
		for(SimComponent s:components)
			if(s instanceof ClientComponent) 
			{
				clients.add((ClientComponent) s);
				((ClientComponent) s).resetValues();
			}
			else if(s instanceof SourceComponent) 
			{
				sources.add((SourceComponent) s);
				((SourceComponent) s).resetValues();
			}
			else if(s instanceof RouterComponent) routers.add((RouterComponent) s);
		QueueServer qs=QueueServer.getInstance();
		
		
		//process clients
		for(ClientComponent c:clients)
		{
			try {
				qs.addQueue(c.component+"."+c.function);
				for(int i=0;i<c.threads;i++)
				{
					systemSimulator.simulators.iSimulatorClient cc=new systemSimulator.simulators.iSimulatorClient(c.component,c.function,c.mean,c.standardDeviation,c.getClientCallNames(),c.component+"."+c.function);
					cc.client=c;
					clientSims.add(cc);
				}
			} catch (QueueNamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServerRunningException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		//process routers
		for(RouterComponent c:routers)
		{
			try {
				qs.addQueue(c.component+"."+c.function);
				for(int i=0;i<c.threads;i++)
				{
					systemSimulator.simulators.iSimulatorRouter cc=new systemSimulator.simulators.iSimulatorRouter(c.component+"."+c.function);				
					routerSims.add(cc);
				}
				
				
				
			} catch (QueueNamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServerRunningException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		//process servers
		for(SourceComponent c:sources)
		{
			
				//qs.addQueue(c.component+c.function);
				for(int i=0;i<c.threads;i++)
				{
					iSimulatorServer cc=new iSimulatorServer(c.component,c.function,c.mean,c.standardDeviation,c.getClientCallNames(),c.wait);
					cc.client=c;
					sourceSims.add(cc);
				}
			
			
		}
		
		
		qs.start();
		
		//set all threads running
		for(iSimulatorClient c: clientSims)
		{
			c.start();
		}
		for(iSimulatorRouter c:routerSims)
		{
			c.start();
		}
		for(iSimulatorServer c:sourceSims)
		{
			c.start();
		}
	}
	
	private ArrayList<systemSimulator.simulators.iSimulatorClient> clientSims=new ArrayList<systemSimulator.simulators.iSimulatorClient>();
	private ArrayList<systemSimulator.simulators.iSimulatorRouter> routerSims=new ArrayList<systemSimulator.simulators.iSimulatorRouter>();
	private ArrayList<systemSimulator.simulators.iSimulatorServer> sourceSims=new ArrayList<systemSimulator.simulators.iSimulatorServer>();
	
	
	public void stopSimulation()
	{
		
		Simulator.simLog.fine("in stop");
		//System.out.println("in stop");
		for(iSimulatorClient c: clientSims)
		{
			c.stopSim();
		}
		clientSims.clear();
		for(iSimulatorRouter c:routerSims)
		{
			c.stopSim();
		}
		routerSims.clear();
		for(iSimulatorServer c:sourceSims)
		{
			c.stopSim();
		}
		sourceSims.clear();
		clientSims.clear();
		routerSims.clear();
		QueueServer qs=QueueServer.getInstance();
		qs.stop();
		
		qs.dropAllQueues();
		
		currentPopup=popup;
		simRunning=false;
		repaint();
	}
	
	public static boolean simRunning=false;
	
	
	//public Dimension getPreferredSize() {
    //    return new Dimension(500,350);
    //}
	
	public void paintComponent(Graphics g) {
        super.paintComponent(g);       
        for(SimComponent sc: components) sc.paint(g);
        if(dragFrom!=null)
        {
        	((Graphics2D)g).drawLine(startDrag.x,startDrag.y,endDrag.x,endDrag.y);
        }
        
    }  
	
	public JScrollPane scroll=null;
	
	private void moveSimulatorComponent(SimComponent c,int x, int y)
	{
		int OFFSET = 3;
        if ((sc.location.x!=x) || (sc.location.y!=y)) {
        	
        	if(scroll!=null)
        	{
        		//Simulator.simLog.fine("setting scrollrect");
        		JViewport view = scroll.getViewport();
        		Point scrollPosition = view.getViewPosition();
        		scrollPosition.x = Math.max(0, x+500);
        		scrollPosition.y = Math.max(0, y+500);
        		view.setViewPosition(scrollPosition);
        		//.s.scrollRectToVisible(new Rectangle(x-100,y-100,100,100));
        	}
        		
        	
            repaint(sc.location.x-11,sc.location.y,sc.location.width+OFFSET+11,sc.location.height+OFFSET);
            sc.location.x=Math.max(0, x-scxoffset);
            sc.location.y=Math.max(0, y-scyoffset);
            repaint(sc.location.x-11,sc.location.y,sc.location.width+OFFSET,sc.location.height+OFFSET);
            repaint();  //repaint it all for the lines
            this.getParent().doLayout();//may have moved off screen
        } 
	}
	private JPopupMenu simPopup=new JPopupMenu();
	private JPopupMenu popup=new JPopupMenu();
	private JPopupMenu currentPopup;
	
	private ArrayList<SimComponent> components=new ArrayList<SimComponent>();
	
	public void saveSimulation()
	{
		File f=null;
		final JFileChooser fc = new JFileChooser();
		
		int returnVal = fc.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION)
		{
		
			f=fc.getSelectedFile();
			if(f.exists())f.delete();
			try {
				f.createNewFile();
				FileOutputStream fos = new FileOutputStream(f,true);
				fos.write("#Simulator#\n".getBytes());
			
		
				for(SimComponent s:components)
				{
					String o=s.toString();
					Simulator.simLog.fine(o);
					fos.write(o.getBytes());
					fos.write('\n');
				}
				//now write children
				for(SimComponent s:components)
				{
					String cs=s.getChildString();
					if(cs==null)
					{
						Simulator.simLog.fine("no children for: "+s.id);
					}
					else
					{
						String o=s.id+","+cs;
						Simulator.simLog.fine(o);
						fos.write(o.getBytes());
						fos.write('\n');
					}			
				}
				fos.flush();
				fos.close();
			
			} catch (IOException e) {
			
					e.printStackTrace();
			}
		}
		
	}
	
	public void loadSimulation()
	{
		components.clear();
		HashMap<Integer,SimComponent> ids=new HashMap<Integer,SimComponent>();
		//File f=new File("simsave.txt");
		
		File f=null;
		final JFileChooser fc = new JFileChooser();
		
		int returnVal = fc.showOpenDialog(this);
		if (returnVal != JFileChooser.APPROVE_OPTION)return;
		f=fc.getSelectedFile();	
		if(f.exists())
		{
			Logger log=Simulator.simLog;

			log.fine("save file found");
			try {
				FileReader fr=new FileReader(f);
				BufferedReader br=new BufferedReader(fr);
				String s;
				int line=0;
				boolean simfile=true;
				boolean onChildren=false;
				while((s = br.readLine()) != null & simfile)
				{
					if(line==0 & s.equals("#Simulator#"))
					{
						log.fine("This is a simulator save file");
						line++;
					}
					else if(line==0)
					{
						simfile=false;
						log.fine("This is not a simulator save file");
					}
					else
					{
						log.fine("simulator line read, line: "+line);
						line++;
						String sp[]=s.split(",");
						if(sp[1].equals("source") & sp.length==13)
						{
							log.fine("source line found");
							if(onChildren)
							{
								log.warning("a source line found after we have started to load children");
							}
							if(ids.containsKey(Integer.parseInt(sp[0])))
							{
								log.severe("a Simulator component has already been loaded with this ID: "+sp[0]);
							}
							else
							{
								//process the source line here
								SourceComponent sc=new SourceComponent(sp[6],sp[7],sp[10]);
								sc.id=Integer.parseInt(sp[0]);
								SimComponent.nextid=Math.max(SimComponent.nextid, Integer.parseInt(sp[0]));
								sc.mean=Integer.parseInt(sp[8]);
								sc.standardDeviation=Integer.parseInt(sp[9]);
								sc.threads=Integer.parseInt(sp[11]);
								sc.wait=Integer.parseInt(sp[12]);
								sc.location.x=Integer.parseInt(sp[2]);
								sc.location.y=Integer.parseInt(sp[3]);
								sc.location.width=Integer.parseInt(sp[4]);
								sc.location.height=Integer.parseInt(sp[5]);
								ids.put(sc.id, sc);
								addSimulatorComponent(sc);
							}
						}
						else if(sp[1].equals("router")& sp.length==10)
						{
							log.fine("router line found");
							if(onChildren)
							{
								log.warning("a router line found after we have started to load children");
							}
							if(ids.containsKey(Integer.parseInt(sp[0])))
							{
								log.severe("a Simulator component has already been loaded with this ID: "+sp[0]);
							}
							else
							{
								RouterComponent sc=new RouterComponent(sp[6],sp[7],sp[8]);
								sc.id=Integer.parseInt(sp[0]);
								SimComponent.nextid=Math.max(SimComponent.nextid, Integer.parseInt(sp[0]));
								//sc.mean=Integer.parseInt(sp[8]);
								//sc.standardDeviation=Integer.parseInt(sp[9]);
								sc.threads=Integer.parseInt(sp[9]);
								//sc.wait=Integer.parseInt(sp[12]);
								sc.location.x=Integer.parseInt(sp[2]);
								sc.location.y=Integer.parseInt(sp[3]);
								sc.location.width=Integer.parseInt(sp[4]);
								sc.location.height=Integer.parseInt(sp[5]);
								ids.put(sc.id, sc);
								addSimulatorComponent(sc);
							}
						}
						else if(sp[1].equals("client")& sp.length==12)
						{
							log.fine("client line found");
							if(onChildren)
							{
								log.warning("a client line found after we have started to load children");
							}
							if(ids.containsKey(Integer.parseInt(sp[0])))
							{
								log.severe("a Simulator component has already been loaded with this ID: "+sp[0]);
							}
							else
							{
								ClientComponent sc=new ClientComponent(sp[6],sp[7],sp[10]);
								sc.id=Integer.parseInt(sp[0]);
								SimComponent.nextid=Math.max(SimComponent.nextid, Integer.parseInt(sp[0]));
								sc.mean=Integer.parseInt(sp[8]);
								sc.standardDeviation=Integer.parseInt(sp[9]);
								sc.threads=Integer.parseInt(sp[11]);
								//sc.wait=Integer.parseInt(sp[12]);
								sc.location.x=Integer.parseInt(sp[2]);
								sc.location.y=Integer.parseInt(sp[3]);
								sc.location.width=Integer.parseInt(sp[4]);
								sc.location.height=Integer.parseInt(sp[5]);
								ids.put(sc.id, sc);
								addSimulatorComponent(sc);
							}
						}
						else if(sp[1].equals("children"))
						{
							log.fine("child line found");
							SimComponent parent=ids.get(Integer.parseInt(sp[0]));
							if(sp.length>2)
							{
								for(int i=2;i<sp.length;i++)
								{
									ClientComponent c=(ClientComponent)ids.get(Integer.parseInt(sp[i]));
									parent.addSubordinate(c);
								}
							}
							
							onChildren=true;
						}
						else 
						{
							log.warning("incorrect line format found: "+s);
						}
					}
				}
				repaint();
				log.fine("File Loaded");
				this.getParent().doLayout();
				br.close();
			} catch (IOException e) {
				log.severe("Unable to read file");
				e.printStackTrace();
			}
			
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		//System.out.println("Action Event Raised");
		if(simRunning)
		{
			if(e.getActionCommand().equals("stop"))stopSimulation();
		}else
		if(e.getActionCommand().equals("simulate"))startSimulation();
		else if(e.getActionCommand().equals("save")) saveSimulation();
		else if(e.getActionCommand().equals("load"))loadSimulation();
		else
		if(e.getActionCommand().equals("source")|e.getActionCommand().equals("client")|e.getActionCommand().equals("router"))
		{
			if(!dialogShown)
			{
				JFrame frame = new JFrame("Simulator Component Creation");
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				dialogShown=true;
				Simulator.simLog.fine("dialog set to: "+dialogShown);
				
				frame.setLocationRelativeTo(this);
				//Add contents to the window.
				frame.add(new SimCompDiag(frame,this,popupx,popupy,e.getActionCommand()));
	 
				//Display the window.
				frame.pack();
				frame.setVisible(true);
			}
		} 
		
		else if(e.getActionCommand().equals("remove"))
		{
			removeComponent(popupx,popupy);
		}
		else if(e.getActionCommand().equals("edit"))
		{
			if(!dialogShown)
				editComponent(popupx,popupy);
		}
			
	}
	int count=0;
	boolean dialogShown=false;
	public SimComponent findComponent(int x, int y)
	{
		Point p=new Point(x,y);
		SimComponent sc=null;
		for(SimComponent s:components)
		{
			if(s.location.contains(p)) 
			{	
				sc=s;
			}
		}
		return sc;
	}
	
	public void editComponent(int x, int y)
	{
		SimComponent s=findComponent(x,y);
		if(s==null) return;
		
		JFrame frame = new JFrame("Edit Simulator Component");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        dialogShown=true;
        Simulator.simLog.fine("dialog set to: "+dialogShown);
        //Add contents to the window.
        frame.add(new SimCompDiag(frame,this,s));
 
        //Display the window.
        frame.pack();
        frame.setVisible(true);
	}
	
	public void removeComponent(int x, int y)
	{
		SimComponent s=findComponent(x,y);
		if(s==null) return;
		s.deleteComponent();
		components.remove(s);
		repaint();
		this.getParent().doLayout();
	}
	
}
