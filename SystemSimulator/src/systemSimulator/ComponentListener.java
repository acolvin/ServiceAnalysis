package systemSimulator;

public interface ComponentListener {
	public void componentDeleted(ComponentDeleteEvent e);
}
