package systemSimulator.simulators;

import internalQueues.*;

import java.util.ArrayList;
import systemSimulator.SourceComponent;
/*
 import javax.jms.Connection;
 import javax.jms.ConnectionFactory;
 import javax.jms.DeliveryMode;
 import javax.jms.Destination;
 import javax.jms.JMSException;
 import javax.jms.Message;
 import javax.jms.MessageConsumer;
 import javax.jms.MessageProducer;
 import javax.jms.Queue;
 import javax.jms.Session;
 import javax.jms.TemporaryQueue;
 import javax.jms.TextMessage;
 import javax.naming.Context;
 import javax.naming.InitialContext;
 import javax.naming.NamingException;
 */
import ServiceAnalysisClient.SAClient;

public class iSimulatorServer extends Thread
{

	public iSimulatorServer(String component, String function, int mean,
			int stddev, ArrayList<String> queues, int wait)
	{
		this.component = component;
		this.function = function;
		this.queues = queues;
		this.mean = mean;
		this.stddev = stddev;
		this.wait = wait;
		server = QueueServer.getInstance();
		session = server.createSession();
		setup = true;

	}

	private void logFine(String msg)
	{
		String s = "Server " + component + "." + function + "[" + this.getId()
				+ "], " + msg;

		systemSimulator.Simulator.simLog.fine(s);
	}

	public SourceComponent client = null;

	String component, function, queuename;
	int mean, stddev;
	SAClient sac = SAClient.getClient();
	int wait;
	QueueServer server;

	boolean setup = true;

	Queue dest = null;
	Queue queue = null;
	Session session = null;
	ArrayList<String> queues = null;
	private TemporaryQueue tempQ = null;
	private MessageConsumer receiver = null;

	private Integer msgid = 0;

	public void run()
	{
		// System.out.println("Starting server "+this.currentThread().getName());
		String txn;
		MessageProducer sender = null;
		try
		{
			Thread.sleep((long) (Math.random() * 100));
		} catch (InterruptedException e2)
		{
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		while (setup)
		{
			logFine("starting call sequence");
			long start = System.currentTimeMillis();
			long waitstart = start;
			txn = sac.startTiming(component, function, "BW");
			// System.out.println("txn: "+txn);
			logFine("transaction number=" + txn);
			int time = systemSimulator.Simulator.getSample(false, mean, stddev);
			if (time < 0)
				systemSimulator.Simulator.simLog.severe("time is negative!");
			for (String qn : queues)
			{
				logFine("calling " + qn);
				String qns[] = qn.split(",");
				dest = server.getQueue(qns[0]);
				// System.out.println("dest is "+dest);
				String msg = txn;
				if (qns.length == 2)
					msg = msg + "," + qns[1];
				sender = session.createProducer(dest);

				if (tempQ == null)
					tempQ = session.createTemporaryQueue();
				TextMessage m = session.createTextMessage(msg);
				m.setReplyQueue(tempQ);
				Integer currentMsg = msgid++;
				m.setCorrelationID(currentMsg.toString());

				sender.setTimeToLive(20000l);
				// System.out.println("sending message to queue "+qn);
				try
				{
					logFine("calling client");
					sender.send(m);
				} catch (ServerRunningException e2)
				{
					setup = false;
					logFine("server stopped");
					return;
				} catch (QueueDeletedException e2)
				{
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}

				// sender.close();

				// System.out.println("message sent to queue "+qn);
				// wait for replay on the temp queue
				if (receiver == null)
					receiver = session.createConsumer(tempQ);

				boolean gotMyMessage = false;
				Message mm = null;

				while (!gotMyMessage)
				{
					try
					{
						logFine("waiting for reply");
						mm = receiver.receive(30000l);
						// System.out.println(mm.toString());
					} catch (QueueDeletedException e)
					{
						systemSimulator.Simulator.simLog
								.warning("Queue Deleted: " + e.getMessage());
					} catch (ServerRunningException e)
					{
						setup = false;
						logFine("server stopped");
						return;
					}
					// tempQ.delete();
					if (mm == null)
					{
						if (setup)
							systemSimulator.Simulator.simLog
									.warning("Server: Oh dear we timed out waiting for the reply");
						gotMyMessage = true; // no really true but need out of
												// the loop
					} else
					{
						if (mm.getCorrelationID().equals(currentMsg.toString()))
						{
							logFine("received my message");
							gotMyMessage = true;
						} else
						{
							systemSimulator.Simulator.simLog
									.warning("Server: Got an old message - retrying");
							// System.err.println("Server: Got an old message - retrying");
						}
					}
				}
				long wait2 = System.currentTimeMillis();
				long waitReply = wait2 - waitstart;
				waitstart = wait2;
				sac.addWait(txn, (int) waitReply);

				try
				{
					Thread.sleep(time);
				} catch (InterruptedException e1)
				{
					e1.printStackTrace();
				}

			}
			logFine("call complete");
			sac.endTiming(txn);
			client.setTemperatureFromValue(((Long) (System.currentTimeMillis() - start))
					.intValue());
			try
			{
				Thread.sleep(Math.max(0, systemSimulator.Simulator.getSample(
						true, wait, wait / 2)));
			} catch (InterruptedException e)
			{
			}
		}
		logFine("simulator ended");

	}

	public void stopSim()
	{
		setup = false;
	}
}
