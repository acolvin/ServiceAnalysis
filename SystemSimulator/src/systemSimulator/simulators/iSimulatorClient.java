package systemSimulator.simulators;

import internalQueues.Queue;
import internalQueues.Message;
import internalQueues.MessageConsumer;
import internalQueues.MessageProducer;
import internalQueues.QueueDeletedException;
import internalQueues.QueueServer;
import internalQueues.ServerRunningException;
import internalQueues.Session;
import internalQueues.TemporaryQueue;
import internalQueues.TextMessage;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import systemSimulator.ClientComponent;

/*import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
*/
import ServiceAnalysisClient.SAClient;

public class iSimulatorClient extends Thread{

	public iSimulatorClient(String component,String function,int mean, int stddev, ArrayList<String> queues, String clientQueue)
	{
		//System.out.println("Creating simulatorClient");
		this.component=component;
		this.function=function;
		this.queues=queues;
		this.mean=mean;
		this.stddev=stddev;
		this.clientQueue=clientQueue;
		
			setup=true;
		
	}
	
	public ClientComponent client=null;
	
	private void logFine(String msg)
	{
		String s="Client "+component+"."+function+"["+this.getId()+"], "+msg;
		
		systemSimulator.Simulator.simLog.fine(s);
	}
	
    public void run()
    {
    	String txn;
    	MessageConsumer receiver=null;
    	
			//session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		QueueServer server=QueueServer.getInstance();
		
		session=server.createSession();
			//connection.start();	
		dest=server.getQueue(clientQueue);
			//dest = (Destination) context.lookup(clientQueue);
		if(dest==null) setup=false;
		else receiver = session.createConsumer(dest);
		
		
    	
    	while(setup)
    	{
    		//System.out.println("waiting for message");
    		
			TextMessage mm =null;
			
			try {
				mm= (TextMessage) receiver.receive();
				
			} catch ( ServerRunningException e1) {
				systemSimulator.Simulator.simLog.fine("Server stopped");
				setup=false;
			} catch (QueueDeletedException e) {
				systemSimulator.Simulator.simLog.warning("Queue Deleted: "+dest.getQueueName());
				setup=false;
			} 
				//System.out.println("message received");
			if(mm!=null)	
			{
				long start=System.currentTimeMillis();
				txn=mm.getText();
				logFine("Received Msg: "+txn+" , correlation id="+mm.getCorrelationID());
				String txn2=sac.startTiming(txn, component, function, "BW");
				int delay=systemSimulator.Simulator.getSample(false, mean, stddev);
				Queue replyDest=mm.getReplyTo();
				logFine("Reply to: "+replyDest.getQueueName());
				for(String q:queues)
				{
					logFine("call string: "+q);
					int wait=0;
					String sp[]=q.split("\\|\\|");
					
					if(sp.length==1){
						logFine("calling single child");
						wait=(int)callClient(q,txn2, session,null);
						
						logFine("child returned");
					}
					else if(sp.length>1)
					{
						ArrayList<String> parallel=new ArrayList<String>();
						for(int i=1;i<sp.length;i++) {
							//System.out.println("parallel queue "+sp[i]);
							parallel.add(sp[i]);
						}
						logFine("calling children in parallel");
						wait=(int)callClient(sp[0],txn2, session,parallel);
						logFine("children returned");
					} 
					
					sac.addWait(txn2, wait);
				}
				try {
					logFine("doing my process time");
					Thread.sleep(delay);
				} catch (InterruptedException e) {
					
					e.printStackTrace();
				}
				client.setTemperatureFromValue(((Long)(System.currentTimeMillis()-start)).intValue());

				if(replyDest!=null) //check we aren't a parallel task
				{
					logFine("Now to send my reply");
					MessageProducer sender = session.createProducer(replyDest);
					//sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
					sender.setTimeToLive(20000l);
					Message m=session.createMessage();
					m.setCorrelationID(mm.getCorrelationID());
					sac.endTiming(txn2);
						
						try {
							sender.send(m);
						} catch (ServerRunningException e) {
							setup=false;
						} catch (QueueDeletedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
				}
			}
    	}
    	
    	//System.out.println("Stopping Client: "+this.getId());
    	logFine("simulator stopped");
    }
    
    private TemporaryQueue tempQ=null;
    MessageConsumer receiver=null;
    private Integer msgid=0;
    
    public long callClient(String qn,String txn, Session session,ArrayList<String> parallels) //throws NamingException, JMSException
    {
        
    	long starttime=System.currentTimeMillis();
    	try{
    	String sp[]=qn.split(",");
    	//System.out.println(sp[0]);
    	QueueServer server=QueueServer.getInstance();
    	Queue cdest = server.addQueue(sp[0]);
    	MessageProducer sender = session.createProducer(cdest);
		//sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		TemporaryQueue myReplyQ;
		if((session==this.session) & tempQ==null) //not the class session then this is a worker thread
			tempQ=this.session.createTemporaryQueue();
		if(session==this.session) myReplyQ=tempQ;
		else {myReplyQ=session.createTemporaryQueue();
		
		}
			
		String msg=txn;
		if(sp.length==2)msg=msg+","+sp[1];
		//System.out.println("msg to send: "+msg);
		TextMessage m=session.createTextMessage(msg);
		//if(session!=this.session) System.out.println(myReplyQ.toString());
		m.setReplyQueue(myReplyQ);
		Integer currentMsg=msgid++;
		
		sender.setTimeToLive(20000l);
		m.setCorrelationID(currentMsg.toString());
		//System.out.println("sending message to queue "+qn);
		
		
		CountDownLatch startSignal = new CountDownLatch(1);
		CountDownLatch doneSignal=null;
		if(parallels!=null)
		{
			doneSignal = new CountDownLatch(parallels.size());
			for(String s:parallels)
			{
				logFine("create new thread for parallel task");
				new Thread(new Worker(startSignal, doneSignal, txn, s)).start();
			}
		//setup all the parallels
		}
		logFine("start parallel threads");
		startSignal.countDown();
		sender.send(m);
		//sender.close();
		//System.out.println("message sent to queue "+qn);
		//wait for replay on the temp queue
		MessageConsumer myReceiver;
		if(session==this.session & receiver==null)
			receiver = session.createConsumer(tempQ);
		if(session==this.session) myReceiver=receiver;
		else myReceiver=session.createConsumer(myReplyQ);
		boolean gotMyMessage=false;
		Message mm =null;
		while(!gotMyMessage)
		{
			logFine("waiting for reply from client(s)");
			mm = myReceiver.receive(30000l);

			//tempQ.delete();
			if(mm==null)
			{
				logFine("Oh dear we timed out waiting for the reply: "+qn);
				gotMyMessage=true; //no really true but need out of the loop
			}
			else
			{
				if(mm.getCorrelationID().equals(currentMsg.toString()))
				{
					logFine("receive expected reply");
					gotMyMessage=true;
				}
				else
				{
					logFine("Got an old message - retrying. Expected Message Number - "+ currentMsg+"; got Message Number - "+ mm.getCorrelationID() );
					//System.err.println("Client: Got an old message - retrying");
					//System.err.println("Client: Expected Message Number - "+ currentMsg);
					//System.err.println("Client: Got Message Number - "+ mm.getCorrelationID());
				}
			}
		}
		if(parallels!=null) doneSignal.await(); // wait for parallel calls to complete
		logFine("all children replied");
		if(session!=this.session)//clean up the temporaries
		{
			//myReceiver.close();
			//myReplyQ.delete(); 
			logFine("delete temporary queue for child call");
			server.deleteQueue(myReplyQ.getQueueName());
		}
		
		
		//tempQ.delete();
		//receiver.close();
				//tempQ.delete();
    	}catch(Exception e){//e.printStackTrace(); 
    	setup=false;}
    	return System.currentTimeMillis()-starttime;
    }
	
	String component,function,queuename;
	int mean,stddev;
	SAClient sac=SAClient.getClient();
	
	
	boolean setup=true;
	
	public void stopSim()
	{
		setup = false;
	}
	
    String factoryName = "connectionFactory";
    Queue dest = null;
    Queue queue=null;
    Session session=null;
    ArrayList<String> queues=null;
    String clientQueue;
    
    
    private class Worker implements Runnable {
    	   private final CountDownLatch startSignal;
    	   private final CountDownLatch doneSignal;
    	   private final String queue,txn;
    	   Worker(CountDownLatch startSignal, CountDownLatch doneSignal, String txn, String queue) {
    	      this.startSignal = startSignal;
    	      this.doneSignal = doneSignal;
    	      this.queue=queue;
    	      this.txn=txn;
    	   }
    	   public void run() {
    	      try {
    	        startSignal.await();
    	        doWork();
    	        doneSignal.countDown();
    	      } catch (InterruptedException ex) {} // return;
    	   }

    	   void doWork() 
    	   {  
    		   Session parallelSession=null;
    		   QueueServer server=QueueServer.getInstance();
    		   parallelSession=server.createSession();
				//parallelSession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
				callClient(queue,txn,parallelSession,null);
				//parallelSession.close();
			
    	   }
    	 }
    
	
}
