package systemSimulator.simulators;

import java.util.ArrayList;
import internalQueues.*;
/*
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
*/

import ServiceAnalysisClient.SAClient;

public class iSimulatorRouter extends Thread{

	public iSimulatorRouter(String routerQueue)
	{
		//System.out.println("Creating simulatorRouter");

		this.clientQueue=routerQueue;
		setup=true;
	}
	
	QueueServer server=QueueServer.getInstance();
	
	private void logFine(String msg)
	{
		String s="Router "+component+"."+function+"["+this.getId()+"], "+msg;
		
		systemSimulator.Simulator.simLog.fine(s);
	}
	
    public void run()
    {
    	//System.out.println("Starting simulatorRouter");
    	String txn;
    	MessageConsumer receiver=null;
    		
    		//connection = factory.createConnection();
    		session=server.createSession();
			//session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			//connection.start();	
			dest = server.getQueue(clientQueue);
			if(dest==null) setup=false;
			else 
				receiver = session.createConsumer(dest);
			
			
			
		
    	
    	while(setup)
    	{
    		//System.out.println("Router waiting for message");
    		
			TextMessage mm=null;
			try {
				logFine("wait for a message");
				mm = (TextMessage) receiver.receive();
				logFine("got a message");
			} catch (Exception e) {
				setup=false;
				logFine("server stopped");
				return;
			}
				//System.out.println("Router: message received");
				
			if(mm!=null)
			{
				String rawmsg=mm.getText();
				String rmsg[]=rawmsg.split(",");
					//long start=System.currentTimeMillis();
				txn=rmsg[0];
					//queues.clear();
					//if(rmsg.length==2)queues.add(rmsg[1]);
					//String txn2=sac.startTiming(txn, component, function, "BW");
					//int delay=Simulator.getSample(false, mean, stddev);
				Queue replyDest=mm.getReplyTo();
				
				if(rmsg.length==2)
				{
					
					logFine("calling "+rmsg[1]+" txn="+txn);
					
						callClient(rmsg[1],txn);
					logFine("client call returned");
				}
				else 
				{
					systemSimulator.Simulator.simLog.warning("Router: no forward in message");
				}

				MessageProducer sender = session.createProducer(replyDest);
				//sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
				sender.setTimeToLive(20000l);
				Message m=session.createMessage();
				m.setCorrelationID(mm.getCorrelationID());
				//sac.endTiming(txn2);
				
					try {
						logFine("reply to caller");
						sender.send(m);
					} catch (ServerRunningException e) {
						setup=false;
						logFine("server stopped");
					} catch (QueueDeletedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//sender.close();

			}
    	}
    	//System.out.println("Stopping Router: "+this.getId());
    	logFine("simulator stopped");
    }
    
    public void stopSim()
	{
		setup = false;
	}
    
    private TemporaryQueue tempQ=null;
    MessageConsumer receiver=null;
    private Integer msgid=0;
    
    public long callClient(String qn,String txn) //throws NamingException, JMSException
    {
    	long starttime=System.currentTimeMillis();
    	try{
    	Queue cdest = server.getQueue(qn);
    	MessageProducer sender = session.createProducer(cdest);
		//sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		if(tempQ==null)
			tempQ=session.createTemporaryQueue();
		TextMessage m=session.createTextMessage(txn);
		m.setReplyQueue(tempQ);
		Integer currentMsg=msgid++;
		sender.setTimeToLive(20000l);
		m.setCorrelationID(currentMsg.toString());
		//System.out.println("sending message to queue "+qn);
		logFine("sending message to client");
		sender.send(m);
		//sender.close();
		
		//System.out.println("message sent to queue "+qn);
		//wait for replay on the temp queue
		if(receiver==null)
			receiver = session.createConsumer(tempQ);
		
		boolean gotMyMessage=false;
		Message mm =null;
		while(!gotMyMessage)
		{
			mm = receiver.receive(30000l);
			//tempQ.delete();
			if(mm==null)
			{
				systemSimulator.Simulator.simLog.warning("Router: Oh dear we timed out waiting for the reply");
				gotMyMessage=true; //no really true but need out of the loop
			}
			else
			{
				if(mm.getCorrelationID().equals(currentMsg.toString()))
				{
					logFine("received my reply" );
					gotMyMessage=true;
				}
				else
				{
					systemSimulator.Simulator.simLog.warning("Router: Got an old message - retrying");
				}
			}
		}
		
		
		
		//tempQ.delete();
		//receiver.close();
		
		//tempQ.delete();
    	}
    	catch(Exception e)
    	{
    		//e.printStackTrace();
    		logFine("caught error killing simulator thread");
    		setup=false;
    		
    	}
    	return System.currentTimeMillis()-starttime;
    }
	
	String component,function,queuename;
	int mean,stddev;
	SAClient sac=SAClient.getClient();
	
	
	boolean setup=true;
	
	//Context context = null;
    //ConnectionFactory factory = null;
    //Connection connection = null;
    String factoryName = "connectionFactory";
    Queue dest = null;
    Queue queue=null;
    Session session=null;
    ArrayList<String> queues=null;
    String clientQueue;
	
}
