package systemSimulator;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;

public abstract class SimComponent {
	public Rectangle location=new Rectangle(20,20,50,100);
	public abstract void paint(Graphics g);
	public abstract void resize(Graphics g);
	public boolean resize=false;
	public int threads=1;
	public abstract void addSubordinate(SimComponent sc);
	public abstract void deleteComponent(); 
	public ModelPanel mp=null;
	public void addListener(ComponentListener s){ listeners.add(s); }
	public void removeListener(ComponentListener s){ listeners.remove(s); }
	public void repaint() {if(mp!=null) mp.repaint();}
	protected ArrayList<ComponentListener> listeners=new ArrayList<ComponentListener>();
	protected static int nextid=0;
	protected int id=++nextid;
	protected abstract String getChildString();
	public abstract void removeSubordinate(SimComponent sc);

}
