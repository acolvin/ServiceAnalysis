package systemSimulator;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import ServiceAnalysisClient.SAClient;

public class SimulatorClient extends Thread{

	public SimulatorClient(String component,String function,int mean, int stddev, ArrayList<String> queues, String clientQueue)
	{
		//System.out.println("Creating simulatorClient");
		this.component=component;
		this.function=function;
		this.queues=queues;
		this.mean=mean;
		this.stddev=stddev;
		this.clientQueue=clientQueue;
		try {
			context = new InitialContext();
			factory = (ConnectionFactory) context.lookup(factoryName);		
			//a.queue = (Queue) a.context.lookup(a.destName);
			connection = factory.createConnection();
			
			//session.createQueue(queuename);
			
			
			setup=true;
			
		} catch (NamingException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setup=false;
		} catch (JMSException e){
			e.printStackTrace();
			setup=false;
		}
	}
	
    public void run()
    {
    	String txn;
    	MessageConsumer receiver=null;
    	try {
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			
			connection.start();	
			dest = (Destination) context.lookup(clientQueue);
			receiver = session.createConsumer(dest);
		} catch (Exception  e2) {
			System.err.println("Client: "+e2.getMessage());
			System.err.println("Client: "+e2.getCause().getMessage());
			setup=false;
		}
    	//System.out.println("Starting simulatorClient");

    	
    	while(setup)
    	{
    		//System.out.println("waiting for message");
    		try {
				TextMessage mm = (TextMessage) receiver.receive();
				//System.out.println("message received");
				
				//long start=System.currentTimeMillis();
				txn=mm.getText();
				String txn2=sac.startTiming(txn, component, function, "BW");
				int delay=Simulator.getSample(false, mean, stddev);
				Destination replyDest=mm.getJMSReplyTo();
				
				for(String q:queues)
				{
					int wait=0;
					String sp[]=q.split("\\|\\|");
					
					if(sp.length==1)
						wait=(int)callClient(q,txn2, session,null);
					else if(sp.length>1)
					{
						ArrayList<String> parallel=new ArrayList<String>();
						for(int i=1;i<sp.length;i++) {
							//System.out.println("parallel queue "+sp[i]);
							parallel.add(sp[i]);
						}
						wait=(int)callClient(sp[0],txn2, session,parallel);
					} 
					
					sac.addWait(txn2, wait);
				}
				try {
					Thread.sleep(delay);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(replyDest!=null) //check we aren't a parallel task
				{
					MessageProducer sender = session.createProducer(replyDest);
					sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
					sender.setTimeToLive(20000l);
					Message m=session.createMessage();
					m.setJMSCorrelationID(mm.getJMSCorrelationID());
					sac.endTiming(txn2);
					try{	
						sender.send(m);
					} catch (Exception e)
					{
						System.err.println("Client: Reply Queue has been deleted, the caller has timed out");
			
					}
					sender.close();
				}
			} catch (javax.jms.IllegalStateException  e) 
			{
				System.err.println("Simulator Client: IllegalState! ");
				//remake the connection
				try{
					connection = factory.createConnection();
					session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
					connection.start();	
					dest = (Destination) context.lookup(clientQueue);
					receiver = session.createConsumer(dest);
					
				} catch (Exception  jmse)
				{
					System.err.println("Simulator Client: can't remake connection! Exiting ");
					setup=false;
				}
				
				//e.printStackTrace();
			} catch (JMSException e)
			{
				e.printStackTrace();
			}
    	}
    	
    	
    }
    
    private TemporaryQueue tempQ=null;
    MessageConsumer receiver=null;
    private Integer msgid=0;
    
    public long callClient(String qn,String txn, Session session,ArrayList<String> parallels) //throws NamingException, JMSException
    {
        
    	long starttime=System.currentTimeMillis();
    	try{
    	String sp[]=qn.split(",");
    	//System.out.println(sp[0]);
    	Destination cdest = (Destination) context.lookup(sp[0]);
    	MessageProducer sender = session.createProducer(cdest);
		sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		TemporaryQueue myReplyQ;
		if((session==this.session) & tempQ==null)
			tempQ=this.session.createTemporaryQueue();
		if(session==this.session) myReplyQ=tempQ;
		else {myReplyQ=session.createTemporaryQueue();
		
		}
			
		String msg=txn;
		if(sp.length==2)msg=msg+","+sp[1];
		//System.out.println("msg to send: "+msg);
		TextMessage m=session.createTextMessage(msg);
		//if(session!=this.session) System.out.println(myReplyQ.toString());
		m.setJMSReplyTo(myReplyQ);
		Integer currentMsg=msgid++;
		
		sender.setTimeToLive(20000l);
		m.setJMSCorrelationID(currentMsg.toString());
		//System.out.println("sending message to queue "+qn);
		
		
		CountDownLatch startSignal = new CountDownLatch(1);
		CountDownLatch doneSignal=null;
		if(parallels!=null)
		{
			doneSignal = new CountDownLatch(parallels.size());
			for(String s:parallels)
			{
				new Thread(new Worker(startSignal, doneSignal, txn, s)).start();
			}
		//setup all the parallels
		}
		startSignal.countDown();
		sender.send(m);
		sender.close();
		//System.out.println("message sent to queue "+qn);
		//wait for replay on the temp queue
		MessageConsumer myReceiver;
		if(session==this.session & receiver==null)
			receiver = session.createConsumer(tempQ);
		if(session==this.session) myReceiver=receiver;
		else myReceiver=session.createConsumer(myReplyQ);
		boolean gotMyMessage=false;
		Message mm =null;
		while(!gotMyMessage)
		{
			mm = myReceiver.receive(30000l);

			//tempQ.delete();
			if(mm==null)
			{
				System.err.println("Client: Oh dear we timed out waiting for the reply: "+qn);
				gotMyMessage=true; //no really true but need out of the loop
			}
			else
			{
				if(mm.getJMSCorrelationID().equals(currentMsg.toString()))
				{
					gotMyMessage=true;
				}
				else
				{
					System.err.println("Client: Got an old message - retrying");
					System.err.println("Client: Expected Message Number - "+ currentMsg);
					System.err.println("Client: Got Message Number - "+ mm.getJMSCorrelationID());
				}
			}
		}
		if(parallels!=null) doneSignal.await(); // wait for parallel calls to complete
		if(session!=this.session)//clean up the temporaries
		{
			myReceiver.close();
			myReplyQ.delete(); 
			
		}
		
		
		//tempQ.delete();
		//receiver.close();
				//tempQ.delete();
    	}catch(Exception e){e.printStackTrace(); setup=false;}
    	return System.currentTimeMillis()-starttime;
    }
	
	String component,function,queuename;
	int mean,stddev;
	SAClient sac=SAClient.getClient();
	
	
	boolean setup=true;
	
	Context context = null;
    ConnectionFactory factory = null;
    Connection connection = null;
    String factoryName = "connectionFactory";
    Destination dest = null;
    Queue queue=null;
    Session session=null;
    ArrayList<String> queues=null;
    String clientQueue;
    
    
    private class Worker implements Runnable {
    	   private final CountDownLatch startSignal;
    	   private final CountDownLatch doneSignal;
    	   private final String queue,txn;
    	   Worker(CountDownLatch startSignal, CountDownLatch doneSignal, String txn, String queue) {
    	      this.startSignal = startSignal;
    	      this.doneSignal = doneSignal;
    	      this.queue=queue;
    	      this.txn=txn;
    	   }
    	   public void run() {
    	      try {
    	        startSignal.await();
    	        doWork();
    	        doneSignal.countDown();
    	      } catch (InterruptedException ex) {} // return;
    	   }

    	   void doWork() 
    	   {  
    		   Session parallelSession=null;
    		   try {
				parallelSession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
				callClient(queue,txn,parallelSession,null);
				parallelSession.close();
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				
			}
    	   }
    	 }
    
	
}
