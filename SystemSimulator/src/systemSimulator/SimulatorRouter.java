package systemSimulator;

import java.util.ArrayList;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import ServiceAnalysisClient.SAClient;

public class SimulatorRouter extends Thread{

	public SimulatorRouter(String routerQueue)
	{
		//System.out.println("Creating simulatorRouter");

		this.clientQueue=routerQueue;
		try {
			context = new InitialContext();
			factory = (ConnectionFactory) context.lookup(factoryName);		
			//a.queue = (Queue) a.context.lookup(a.destName);
			
			//session.createQueue(queuename);
			
			
			setup=true;
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
			setup=false;
		}
	}
	
    public void run()
    {
    	//System.out.println("Starting simulatorRouter");
    	String txn;
    	MessageConsumer receiver=null;
    	try {	
    		connection = factory.createConnection();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			connection.start();	
			dest = (Destination) context.lookup(clientQueue);
			receiver = session.createConsumer(dest);
			
			
			
		} catch (Exception e1) {
			e1.printStackTrace();
			setup=false;
		}
    	
    	while(setup)
    	{
    		//System.out.println("Router waiting for message");
    		try {
				TextMessage mm = (TextMessage) receiver.receive();
				//System.out.println("Router: message received");
				String rawmsg=mm.getText();
				String rmsg[]=rawmsg.split(",");
				//long start=System.currentTimeMillis();
				txn=rmsg[0];
				//queues.clear();
				//if(rmsg.length==2)queues.add(rmsg[1]);
				//String txn2=sac.startTiming(txn, component, function, "BW");
				//int delay=Simulator.getSample(false, mean, stddev);
				Destination replyDest=mm.getJMSReplyTo();
				
				if(rmsg.length==2)
				{
					
					
					
						callClient(rmsg[1],txn);
					
				}
				else 
				{
					System.err.println("Router: no forward in message");
				}

				MessageProducer sender = session.createProducer(replyDest);
				sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
				sender.setTimeToLive(20000l);
				Message m=session.createMessage();
				m.setJMSCorrelationID(mm.getJMSCorrelationID());
				//sac.endTiming(txn2);
				try{
					sender.send(m);
					sender.close();
				} catch (Exception e)
				{
					System.err.println("Router: Reply Queue has been deleted, the caller has timed out");
					sender.close();
				}
				
				
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				setup=false;
				
			}catch (Exception e)
			{
				e.printStackTrace();
				setup=false;
			}
    	}
    	
    	
    }
    
    private TemporaryQueue tempQ=null;
    MessageConsumer receiver=null;
    private Integer msgid=0;
    
    public long callClient(String qn,String txn) //throws NamingException, JMSException
    {
    	long starttime=System.currentTimeMillis();
    	try{
    	Destination cdest = (Destination) context.lookup(qn);
    	MessageProducer sender = session.createProducer(cdest);
		sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		if(tempQ==null)
			tempQ=session.createTemporaryQueue();
		TextMessage m=session.createTextMessage(txn);
		m.setJMSReplyTo(tempQ);
		Integer currentMsg=msgid++;
		sender.setTimeToLive(20000l);
		m.setJMSCorrelationID(currentMsg.toString());
		//System.out.println("sending message to queue "+qn);
		sender.send(m);
		sender.close();
		
		//System.out.println("message sent to queue "+qn);
		//wait for replay on the temp queue
		if(receiver==null)
			receiver = session.createConsumer(tempQ);
		
		boolean gotMyMessage=false;
		Message mm =null;
		while(!gotMyMessage)
		{
			mm = receiver.receive(30000l);
			//tempQ.delete();
			if(mm==null)
			{
				System.err.println("Router: Oh dear we timed out waiting for the reply");
				gotMyMessage=true; //no really true but need out of the loop
			}
			else
			{
				if(mm.getJMSCorrelationID().equals(currentMsg.toString()))
				{
					gotMyMessage=true;
				}
				else
				{
					System.err.println("Router: Got an old message - retrying");
				}
			}
		}
		
		
		
		//tempQ.delete();
		//receiver.close();
		
		//tempQ.delete();
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    		setup=false;
    		
    	}
    	return System.currentTimeMillis()-starttime;
    }
	
	String component,function,queuename;
	int mean,stddev;
	SAClient sac=SAClient.getClient();
	
	
	boolean setup=true;
	
	Context context = null;
    ConnectionFactory factory = null;
    Connection connection = null;
    String factoryName = "connectionFactory";
    Destination dest = null;
    Queue queue=null;
    Session session=null;
    ArrayList<String> queues=null;
    String clientQueue;
	
}
