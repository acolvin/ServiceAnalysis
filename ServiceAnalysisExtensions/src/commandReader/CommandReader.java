package commandReader;

import serviceAnalysisModel.readersWriters.ServiceEntry;
import serviceAnalysisModel.readersWriters.extension.*;


public class CommandReader extends RealTimeReader {

	@Override
	public ServiceEntry processLogFileLine(String line) {
		// TODO Auto-generated method stub
		
		if(line.startsWith("COMMAND:"))
		{
			String s[]=line.trim().split("\\s",3);
			if(s.length==3)
				return new ServiceEntry(s[1],s[2]);
			else if(s.length==2) 
				return new ServiceEntry(s[1],"");
			else
				return new ServiceEntry("NOOP","");
				
		}
		return null;
	}

	@Override
	public int getOrderNumber() {
		// TODO Auto-generated method stub
		return 10000;
	}

}
