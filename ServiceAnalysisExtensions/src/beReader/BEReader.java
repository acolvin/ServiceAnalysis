package beReader;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.readersWriters.ServiceEntry;
import serviceAnalysisModel.readersWriters.extension.RealTimeReader;

public class BEReader extends RealTimeReader {
	
	private String location;
	public BEReader(){
		System.out.println("loaded BE Reader");
		try
		{
			location=InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e)
		{
			//e.printStackTrace();
			location=e.getMessage().substring(e.getMessage().length()/2+1);
			//location=null;
		}
		System.out.println(location);
	}

	@Override
	public int getOrderNumber() {
		// TODO Auto-generated method stub
		return 13;
	}

	@Override
	public ServiceEntry processLogFileLine(String line) {
		//System.out.println(line);
		if(line.contains("[Rules.Timings]")){
			//System.out.println("found rules engine line");
			//find position of 6th space
			String l=line.trim();  //remove extra spaces
			int i=1; //first character (0) cannot be a space now
			int j=0; //number spaces found
			boolean found=false;
			while (!found){
				if(l.charAt(i++)==' '){
					//System.out.println("Found space character at "+i);
					j++;
					if(j==6){
						found=true;
						i--;
					}
				}
				if(i>=l.length())break;
			}
			if(j!=6){
				System.out.println("Unable to find the date in the line!");
				return null;
			}
			String dateString=l.substring(0, i).trim();
			String tzString=dateString.substring(dateString.indexOf("GMT ")+4);
			dateString=dateString.substring(0, dateString.indexOf(" GMT"));
			//System.out.println("Date substring is "+dateString);
			Date date;
			synchronized(sdf){ 
				try {
					int tzhours=Integer.parseInt(tzString);
					TimeZone tz = TimeZone.getTimeZone("GMT"+tzString);
				//System.out.println(tz.getDisplayName());
					sdf.setTimeZone(tz);
					date=sdf.parse(dateString);
					//System.out.println(date);
				} catch (ParseException e) {
				// TODO Auto-generated catch block
					e.printStackTrace();
					
					return new ServiceEntry((Service) null,(Date)null,-1d);
				}
			}
			j=l.indexOf("Warning");
			String server=l.substring(i, j).trim();
			//System.out.println("Server substring is "+server);
			i=l.indexOf(": R")-1;
			j=l.lastIndexOf(' ', i-1);
			//System.out.println("op indexes are "+j+" - "+i);
			String operation=l.substring(j, i).trim();
			//System.out.println("Opertion is "+operation);
			i=l.indexOf(": Reply");
			j=l.indexOf(": Received");
			
			String txnNumber;
			if(i==-1 && j!=-1){ //found received line
				txnNumber=server+":"+l.substring(j+11);
				txns.put(txnNumber, date);
				return new ServiceEntry((Service) null,(Date)null,-1d);
			}else if(i!=-1 && j==-1){ //found reply line
				txnNumber=server+":"+l.substring(i+8);
				Date startDate=txns.get(txnNumber);
				if(startDate==null){
					System.out.println("found a reply line with no corresponding receive line");
					return new ServiceEntry((Service) null,(Date)null,-1d);
				}
				txns.remove(txnNumber);
				long millis=date.getTime()-startDate.getTime();
				//System.out.println("Call took "+millis+" milliseconds");
				ServiceEntry se = new ServiceEntry(new Service("BW","BE",operation,""),startDate,millis);
				se.location=location;
				se.txn=server+":"+operation+":"+txnNumber;
				return se;
				
			}else {// either both or none found - error in line
				return new ServiceEntry((Service) null,(Date)null,-1d);
			}
			
		}
		
		else return null;
	}

	private ConcurrentHashMap<String,Date> txns=new ConcurrentHashMap<String,Date>();
	private final String dateFormat="yyyy MMM dd HH:mm:ss:SSS";//2015 Aug 12 10:58:45:520 GMT +1
	private SimpleDateFormat sdf=new SimpleDateFormat(dateFormat);
}
