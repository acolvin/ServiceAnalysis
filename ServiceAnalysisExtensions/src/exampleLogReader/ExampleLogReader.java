package exampleLogReader;

import java.util.Date;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.readersWriters.ServiceEntry;
import serviceAnalysisModel.readersWriters.extension.*;


/**
 * This is an example extension.  It returns success for all lines 
 * and returns the same answer continuously.
 * 
 * @author apc 
 *
 */
public class ExampleLogReader extends RealTimeReader {

	@Override
	public ServiceEntry processLogFileLine(String line) {
		// TODO Auto-generated method stub
		Service service=new Service("BW","IM","here","");
		Date when=new Date();
		double response=45D;
		//return null;
		return new ServiceEntry(service,when,response,new Service("BW","PAR","ent",""));
	}

	@Override
	public int getOrderNumber() {
		// TODO Auto-generated method stub
		return 11;
	}

}
