package beaconReader;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.readersWriters.ServiceEntry;
import serviceAnalysisModel.readersWriters.extension.*;


/**
 * This is an extension that handles comments. A comment is any line starting with
 * a # or a ; 
 * <P>
 * It has an order of 10 so if any line needs to be processed earlier as it starts with 
 * a comment char must have an order under this value <P>
 * It returns a service entry with a null service
 * 
 * @author apc 
 *
 */
public class BeaconReader extends RealTimeReader {

	private String servertxn=null;
	private String location;
	
	public BeaconReader(){
		Random a = new Random();
		servertxn=""+a.nextInt()+"-"+a.nextInt()+"-";
		try
		{
			location=InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e)
		{
			//e.printStackTrace();
			location=e.getMessage().substring(e.getMessage().length()/2+1);
			//location=null;
		}
		System.out.println(location);
	}
	
	
	
	@Override
	public ServiceEntry processLogFileLine(String line) {
		// TODO Auto-generated method stub
		Service service=null;
		Date when=null;
		double response=-1D;
		if(line.contains("\"?v=0.9"))
		{
			String url=null;
			String newline=line.replace("%2F", "/");
			newline=newline.replace("%3A", ":");
			//System.out.println("comment found");
			url=getTag(line,"page_id");
			//if(line.contains("page_id"))
			//{
				//int index=line.indexOf("page_id=")+8;
				//int endIndex=line.indexOf("&", index);
				//url=line.substring(index, endIndex);
			//}
			if(url==null || url.length()==0)
			{
				
				int urlstart=newline.indexOf("&u");
				int urlend=newline.indexOf("&user");
				if(urlend==-1)
					urlend=newline.indexOf("&", urlstart+1);
				url=newline.substring(urlstart+3, urlend);
				url=url.replaceFirst("\\.do.*", "");
				url=url.substring(url.lastIndexOf('/')+1);
			}
			
			
			//int timestart=newline.indexOf("&t_done")+8;
			//int timeend=newline.indexOf("&",timestart);
			//if(timestart<8) return new ServiceEntry(service,when,response);
			String timestring=getTag(newline,"t_done");
			String servertime=getTag(newline,"t_resp");
			if(timestring==null)return new ServiceEntry(service,when,response);
			
			//newline.substring(timestart, timeend);
			
			String user=getTag(newline,"user");
			
			//determine date time string
			int dt_start=line.indexOf("[");
			int dt_end=line.indexOf("]");
			String date_string=line.substring(dt_start+1, dt_end);
			//System.out.println(date_string);
			//03/Feb/2015:07:48:05 +0000

			SimpleDateFormat df = new SimpleDateFormat(
					"dd/MMM/yyyy:HH:mm:ss Z");
			//SimpleTimeZone tz = new SimpleTimeZone(0, "GMT");
			//changed to use local timezone as this is more likely what the server is running
			df.setTimeZone(TimeZone.getDefault());
			try {
				when = df.parse(date_string);
				//System.out.println(when);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}

			
			
			//System.out.println(url);
			//System.out.println(timestring);
			
			if(url==null)url="null";
			if(url.length()==0)url="null";
			if(url.equals("null"))System.out.println(line);
			if(url.contains("."))url=url.replaceAll("\\.", "");
			
			service=new Service("UR","BEACON",url,"");
			
			response=new Double(timestring);
			Double serverRespTime=0D;
			try{
				serverRespTime=new Double(servertime);
			}catch (NumberFormatException e){
				
			}
			
			ServiceEntry se = new ServiceEntry(service,new Date(when.getTime()-new Long(timestring)),response);
			se.wait=serverRespTime;
			se.txn=servertxn+when.getTime();
			//System.out.println("got user "+user);
			se.user=user;
			se.location=location;
			//if(url.equals("CMS100")&& line.contains("AMSPortalWebProject/\"\"Mozi")){
			//	se.loginEvent=true;
			//}
			//if(line.contains("leaveTool")) se.logoutEvent=true;
			return se;
		}
		else return null;
	}

	@Override
	public int getOrderNumber() {
		// TODO Auto-generated method stub
		return 11;
	}
	
	private String getTag(String line, String tag)
	{
		String value=null;
		if(line==null || line.equals("") || tag==null || tag.equals(""))return null;
		
		if(line.contains(tag))
		{
			int index=line.indexOf(tag)+tag.length()+1;
			int endIndex=line.indexOf("&", index);
			if(endIndex==-1) endIndex=line.length();
			value=line.substring(index, endIndex);
		}
		
		return value;
	}

}
