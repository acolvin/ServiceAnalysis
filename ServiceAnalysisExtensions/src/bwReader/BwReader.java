package bwReader;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.readersWriters.ServiceEntry;
import serviceAnalysisModel.readersWriters.extension.*;


public class BwReader extends RealTimeReader {
	
	public BwReader()
	{
		try
		{
			location=InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e)
		{
			//e.printStackTrace();
			location=e.getMessage().substring(e.getMessage().length()/2+1);
			//location=null;
		}
		System.out.println(location);
	}
	
	String location=null;

	@Override
	public ServiceEntry processLogFileLine(String line) {
		// TODO Auto-generated method stub
		return newBWLogFileLine(line);
	}

	@Override
	public int getOrderNumber() {
		// TODO Auto-generated method stub
		return 12;
	}

	private ServiceEntry newBWLogFileLine(String s) {
		// TODO Auto-generated method stub
		//System.out.println(s);
		String time, servicename, serviceoperation;
		Float millisecs,wait;
		Date when=null;
		Service service=null;
		ServiceEntry se=null;

		if (s.startsWith("Job-")) {//this is the comment line
			
			se=new ServiceEntry(service,when,-1D);
			String b[] = s.split(",");
			if (b[6].trim().equals("success")) {
				if (!(s.contains("starterProcesses")
						|| s.contains("EngineStart.process") || s
						.contains("WSInspection"))) {
					time = b[4].trim();
					millisecs = Float.valueOf(time);
					wait=millisecs-Float.valueOf(b[5].trim());

					String so[] = b[1].trim().split("/");
					if(so.length>=4){
						servicename = so[3].toUpperCase();
						if (servicename.contains("SERVICE"))
							servicename = servicename.replace("SERVICE", "");
						serviceoperation = so[so.length - 1];
						int r = 0;
						if (serviceoperation.startsWith("ws"))
							r = 2;
						serviceoperation = serviceoperation.substring(r,
								serviceoperation.length() - 8).toLowerCase();
						if (serviceoperation.contains("_v")) {
							serviceoperation = serviceoperation.substring(0,
									serviceoperation.indexOf("_v"));
						}
						if (serviceoperation.contains("getreferencedata"))
							serviceoperation = "get";

						// System.out.println("reading jobstats: "+servicename+"."+
						// serviceoperation+" running in "+millisecs);

						if (serviceoperation.startsWith("getreference")) {
							serviceoperation = "get";
							servicename = "RDS_V1.0";
						}
						if (serviceoperation.contains("clocks_v"))
							serviceoperation = serviceoperation.split("_")[0];
						if (serviceoperation
								.contains("performcheckfordvandexam"))
							serviceoperation = "performcheckfordvandexam";
						if (serviceoperation
								.contains("performdeprecateactivebringups"))
							serviceoperation = "deprecateactivebringups";
						if (serviceoperation
								.contains("getopencashsession_v1.0"))
							serviceoperation = "getopencashsession";
						if (serviceoperation
								.contains("capturepaperapplicationjmsstarter"))
							serviceoperation = "capturepaperapplication";
						if (serviceoperation.contains("jmsstarter"))
							serviceoperation = serviceoperation.replace(
									"jmsstarter", "");


						if (servicename.length() > 0
								&& serviceoperation.length() > 0)
						{
							service = new Service("BW",servicename,serviceoperation,"");
							// add in event stuff here
							SimpleDateFormat df = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss:SSS");
							//SimpleTimeZone tz = new SimpleTimeZone(0, "GMT");
							//changed to use local timezone as this is more likely what the server is running
							df.setTimeZone(TimeZone.getDefault());


							if(service!=null)
							{

								try {
									when = df.parse(b[2]);
									se=new ServiceEntry(service,when,millisecs);
									se.wait=wait;
									se.location=location;
									se.txn=b[0];

								} catch (ParseException e) {
									// TODO Auto-generated catch block
									//e.printStackTrace();
								}

							} 
						}
					}
				}
			}
		}
		else if(s.startsWith("jobId,")) se=new ServiceEntry(service,when,-1D);
		return se;
	}
}



