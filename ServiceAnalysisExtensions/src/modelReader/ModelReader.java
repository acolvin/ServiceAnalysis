package modelReader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.VolumetricType;
import serviceAnalysisModel.Workstream;
import serviceAnalysisModel.readersWriters.ServiceEntry;
import serviceAnalysisModel.readersWriters.extension.*;


/**
 * This is an extension that handles model files.  It returns a service entry 
 * with appropriate values building the model.  Note that 
 * iterations are not yet covered.  The volumetric modelling isn't handled either 
 * 
 * 
 * @author apc 
 *
 */
public class ModelReader extends RealTimeReader {

	@Override
	public ServiceEntry processLogFileLine(String line) {
		// TODO Auto-generated method stub
		Service service=null;
		Service parent=null;
		Date when=null;
		double response=-1D;
		//System.out.println("model reader");
		if(line.startsWith("MODEL,") )
		{
			//System.out.println("model line");
			String[] split = line.split(",",10);
			String recType = split[0];
			String layer = split[1];
			String services = split[2];//.toUpperCase();
			String operation = split[3];//.toLowerCase();
			String subLayer="";
			String subService="";
			String subOperation="";
			String subProtocol="";
			String subAsync="";
			String addData="";
			//String subProtocol;
			//String subAsync;
			if (!split[4].equals(""))
			{
				if(split.length>=5)subLayer = split[4];
				if(split.length>=6)subService = split[5];//.toUpperCase();
				if(split.length>=7)subOperation = split[6];//.toLowerCase();
				if(split.length>=8)subProtocol = split[7];
				if(split.length>=9)subAsync = split[8];
				if(split.length>=10)addData=split[9];
			}
			else
			{
				//System.out.println(services+"."+operation);
				//System.out.print("split4="+split[4]+"- length="+split.length);
				subLayer = null;
				if(split.length==10){addData=split[9];}//;System.out.print(", "+addData+'\n');}
				//else System.out.print('\n');
				
			}
			
			if (recType.equalsIgnoreCase("MODEL") && !layer.equalsIgnoreCase("VOL"))
			{
				Service servObj;
				if(layer.equalsIgnoreCase("WS"))
					servObj=new Workstream(operation);
				else
					if(subLayer==null)
						servObj = new Service(layer, services, operation, addData);
					else
						servObj = new Service(layer, services, operation, "");

			//Service servObj = new Service(service + "." + operation);

				
				Service subServObj=null;
				if ((subLayer != null) && (subLayer.length()>0))
				{
					
					if(subLayer.equalsIgnoreCase("WS")){
						subServObj=new Workstream(subOperation);
					//System.out.println("workstream found "+ subServObj);
					}
					else
						//System.out.println(subService+"."+subOperation+","+subProtocol+","+subAsync+","+addData);
						subServObj = new Service(subLayer, subService, subOperation, subProtocol+","+subAsync+","+addData);
				//Service subServObj = new Service(subService + "." + subOperation);
					service=subServObj;
					parent=servObj;
				
				}
				else {
					service=servObj;
				}
				
			
			
			}
			else if(recType.equalsIgnoreCase("MODEL") && layer.equalsIgnoreCase("VOL"))
			{
				VolumetricType vt=new VolumetricType(split[3].toLowerCase());
				Workstream w=null;
				if(subLayer!=null)
				{
					//System.out.println("Adding Workstream "+ subOperation +" to "+vt.getName());
					w=new Workstream(subOperation);		
				}
				return new ServiceEntry(vt,w);
			}
			return new ServiceEntry(service,when,response,parent);
		}
		else if(line.startsWith("ITERATION,"))
		{ //iterations go here
			if(line.startsWith("ITERATION,VOL,VOLUMETRIC,"))
			{
				String s[]=line.split(",");
				String volName=s[3];//.toLowerCase();
				String volNumber=s[4];
				Integer i=new Integer(volNumber);
				//System.out.println("(volNumber,int)=("+volNumber+","+i+")");
				VolumetricType vt=new VolumetricType(volName);
				ServiceEntry se= new ServiceEntry(vt,null);
				//System.out.println(se.type);
				se.volume=i;
				return se;
			}
			if(line.startsWith("ITERATION,SER,VOLUMETRIC"))
			{
				Service w;
				String s[]=line.split(",");
				String volName=s[3];//.toLowerCase();
				VolumetricType vt=new VolumetricType(volName);
				if(s[4].equalsIgnoreCase("WS"))//workstream
				{
					w=new Workstream(s[6]);
				}
				else
				{
					w=new Service(s[4],s[5],s[6],"");
				}
				double it=new Double(s[7]);
				ServiceEntry se=new ServiceEntry(vt,w,it);
				return se;
				
			
			}
			if(line.startsWith("ITERATION,"))
			{
				String s[]=line.split(",");
				if(s[1].equalsIgnoreCase("WS"))parent=new Workstream(s[3]);
				else parent=new Service(s[1],s[2],s[3],"");
				service=new Service(s[4],s[5],s[6],""); 
				double it=new Double(s[7]);
				ServiceEntry se=new ServiceEntry(parent,service,it);
				return se;
			}
			
		}
		// MODELTIME,LAYER,SERVICE,OPERATION,TIME,ADJUSTMENT
		else if(line.startsWith("MODELTIME,"))
		{
			
			Service servObj;
			String s[]=line.split(",");
			if(s.length<5 || s.length>6) return null;
			String layer=s[1];//.toUpperCase();
			String servicename=s[2];//.toUpperCase();
			String operationname=s[3];//.toLowerCase();
			double time=new Double(s[4]);
			double adjustment=0;
			if(time<0)time=-1;
			if(s.length==6) adjustment=new Double(s[5]);
			if(layer.equalsIgnoreCase("WS")) return null;
			//System.out.println("servicename="+servicename);
			//System.out.println("operationname="+operationname);
			servObj = new Service(layer, servicename, operationname, "");
			return new ServiceEntry(servObj,time,adjustment);
		}
		// MODELEVENT,LAYER,SERVICE,OPERATION,DATE,TIME,WAIT,txn,user   (yyyy-MM-dd HH:mm:ss:SSS)
		else if(line.startsWith("MODELEVENT"))
		{
			String s[]=line.split(",");
			if(s.length<7) return null;
			service = new Service(s[1].trim(),s[2].trim(),s[3].trim(),"");
			
			
			// add in event stuff here
			SimpleDateFormat df = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss:SSS");
			SimpleTimeZone tz = new SimpleTimeZone(0, "GMT");
			df.setTimeZone(tz);
			int millisecs=new Integer(s[5].trim());
			int wait=new Integer(s[6].trim());
			String txn=null;
			if(s.length>7 && s[7]!=null && s[7].length()>0)txn=s[7];
			
			String user=null;
			if(s.length>8 && s[8]!=null && s[8].length()>0)user=s[8];
			ServiceEntry se=null;
			if(service!=null)
			{

				try {
					when = df.parse(s[4]);
					se=new ServiceEntry(service,when,millisecs);
					se.wait=wait;
					se.txn=txn;
					se.user=user;

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
				
			} 
			return se; 
		}
		return null;
	}

	@Override
	public int getOrderNumber() {
		// TODO Auto-generated method stub
		return 1;
	}
	
	

}
