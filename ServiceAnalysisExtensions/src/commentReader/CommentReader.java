package commentReader;

import java.util.Date;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.readersWriters.ServiceEntry;
import serviceAnalysisModel.readersWriters.extension.*;


/**
 * This is an extension that handles comments. A comment is any line starting with
 * a # or a ; 
 * <P>
 * It has an order of 10 so if any line needs to be processed earlier as it starts with 
 * a comment char must have an order under this value <P>
 * It returns a service entry with a null service
 * 
 * @author apc 
 *
 */
public class CommentReader extends RealTimeReader {

	@Override
	public ServiceEntry processLogFileLine(String line) {
		// TODO Auto-generated method stub
		Service service=null;
		Date when=null;
		double response=-1D;
		if(line.startsWith("#") | line.startsWith(";"))
		{
			//System.out.println("comment found");
			return new ServiceEntry(service,when,response);
		}
		else return null;
	}

	@Override
	public int getOrderNumber() {
		// TODO Auto-generated method stub
		return 10;
	}

}
