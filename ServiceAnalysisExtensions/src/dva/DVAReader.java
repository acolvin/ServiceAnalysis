package dva;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import serviceAnalysisModel.readersWriters.ServiceEntry;
import serviceAnalysisModel.readersWriters.extension.RealTimeReader;

public class DVAReader extends RealTimeReader {

	@Override
	public int getOrderNumber() {
		
		return 20;
	}

	
	@Override
	public ServiceEntry processLogFileLine(String arg0) {
		// TODO Auto-generated method stub
		if(arg0.contains("[ajp-bio") && arg0.contains("|INFO |u.g.h"))
		{
			System.out.println("found a DVA entry: \n\t"+arg0);
			String split[]=arg0.split("|");
			GregorianCalendar cal = new GregorianCalendar();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
			try {
				Date now=sdf.parse(split[0]);
				System.out.println(now+" "+split[7]);
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return null;
	}
	
	

}
