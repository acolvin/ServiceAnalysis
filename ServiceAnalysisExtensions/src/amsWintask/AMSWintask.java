package amsWintask;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.readersWriters.ServiceEntry;
import serviceAnalysisModel.readersWriters.extension.*;


/**
 * This is an example extension.  It returns success for all lines 
 * and returns the same answer continuously.
 * 
 * @author apc 
 *
 */
public class AMSWintask extends RealTimeReader {

	@Override
	public ServiceEntry processLogFileLine(String line) {
		// TODO Auto-generated method stub
		
		
		//System.out.println("AMSWinTask: "+line);
		
		
		if(!(line.startsWith("\"IPS\"") && line.contains("\"AMS\"")))return null; //not this extension
		//System.out.println("AMSWinTask: This plugin");
		if(!line.endsWith("\"Successful\"")) return null; //ignore line as call failed
		//System.out.println("AMSWinTask: my line");
		
		String split[]=line.replaceAll("\"", "").split(",");
		//System.out.println("AMSWinTask: my line: "+line.replaceAll("\"", ""));
		SimpleDateFormat df = new SimpleDateFormat(
				"dd/MM/yyyy HH:mm:ss");
		df.setTimeZone(TimeZone.getDefault());
		Date when;
		Double response;
		ServiceEntry se=null;
		try {
			when = df.parse(split[1]+" "+split[2]);
			//System.out.println("AMSWinTask: "+when);
			String location=split[3];
			String function=split[6];
			Integer duration=Integer.parseInt(split[9]);
			//System.out.println("AMSWinTask: "+duration);
			
			response=duration.doubleValue();
			
			Service locationspecific=new Service("F37",function,location,"");
			
			Service overall=new Service("F37",function,"all","");
			//System.out.println("AMSWinTask: "+overall);
			
			Calendar cal=new GregorianCalendar();
			cal.setTime(when);
			int i=cal.get(Calendar.MONTH)+1;
			String m=""+i;
			if(i<10) m="0"+m;
			String d=""+cal.get(Calendar.YEAR)+m;
			
			Service monthall=new Service("F37",function,"all"+d,"");
			Service monthloc=new Service("F37",function,location+d,"");
			
			
			
			ServiceEntry selocation=new ServiceEntry(locationspecific,when,response,overall);
			ServiceEntry seoverall=new ServiceEntry(overall,when,response);
			ServiceEntry semonthall=new ServiceEntry(monthall,when,response,overall);
			ServiceEntry semonthloc=new ServiceEntry(monthloc,when,response,monthall);
			ServiceEntry separent=new ServiceEntry(monthloc,null,-1d,locationspecific);
			
			
			List<ServiceEntry> lse=new ArrayList<ServiceEntry>();
			lse.add(seoverall);
			lse.add(selocation);
			lse.add(semonthall);
			lse.add(semonthloc);
			lse.add(separent);
			se=new ServiceEntry(lse);
			//se=seoverall;
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		
		
		//return null;
		//System.out.println("AMSWinTask: "+se);
		return se;
	}

	@Override
	public int getOrderNumber() {
		// TODO Auto-generated method stub
		return 121;
	}

}
