package amsPortal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.readersWriters.ServiceEntry;
import serviceAnalysisModel.readersWriters.extension.*;


public class AMSPortalReader extends RealTimeReader  {

	@Override
	public ServiceEntry processLogFileLine(String line) 
	{
		if(line.contains("<AMS_PERF>"))
			return processAMSPerfLine(line);
		else return null;
	}

	private ServiceEntry processAMSPerfLine(String s)
	{
		ServiceEntry senull,se=null;
		Service service=null;
		Date when=null;
		senull=new ServiceEntry(service,when,-1D);
		se=senull;
		if(s.contains("<Error>"))return se;
		//the next 2 lines are to handle the kernel logging user out on session expiry
		String ss=s.replace("<<", "<");
		ss=ss.replace(">>", ">");
		//if(s.contains("Correlation ID:")) ProcessCorrelation(ss);
		if(s.contains("request start:")) se=ProcessReqStart(ss);
		if(s.contains("service start:")) se=ProcessSerStart(ss);
		if(s.contains("service end:")) se=ProcessSerEnd(ss);
		if(s.contains("request end: ")) se=ProcessReqEnd(ss);
		
		
		return se;
	}
	
	private void ProcessCorrelation(String s) {
		String user=getUser(s);
		int index=s.indexOf(user)+user.length();
		if(!(s.substring(index)).contains(user)) System.out.println("User Issue!! for\n"+s);
		
	}

	private ServiceEntry ProcessReqEnd(String s) 
	{
		Service service;
		String server=getIndexString(s,5);
		ServiceEntry se = new ServiceEntry((Service) null,(Date)null,-1D);
		int y=s.indexOf("<Request #")+10;
		int z=s.indexOf(',', y);
		String reqNumber=server+"."+s.substring(y, z);
		
		String req= s.substring(s.indexOf("request end: ")+13,s.length()-1);
		String reqs[]=req.split(",");

		if(profileUpdate.contains(reqNumber)) profileUpdate.remove(reqNumber);
		
		if(requestService.containsKey(reqNumber)){
			service=requestService.get(reqNumber);
			requestService.remove(reqNumber);
		}
		else {
			String request=reqs[0];
			if(request.contains("[null]")) 
				request=request.substring(1, request.length()-7).trim();
			
			if(request.contains("[")){
				int i=request.indexOf("[");
				int j=request.indexOf("]");
				request=request.substring(i+1, j);
			}
			if(request.contains("systemtools"))
				request=request.substring(request.indexOf("systemtools")+12);
			if(request.contains("home/"))
				request=request.substring(request.indexOf("home/"));
			if(request.contains("?"))
				request=request.substring(0,request.indexOf("?"));
			
			if(request.startsWith("/AMSPortalWebProject/com/ams2/"))
				request=request.substring(30);
			
			if(request.startsWith("/AMSPortalWebProject/com/ams/"))
				request=request.substring(29);
			if(request.endsWith("do"))request=request.substring(0, request.length()-3);
			if(request.endsWith("jsp"))request=request.substring(0, request.length()-4);
			if(request.endsWith("jpf"))request=request.substring(0, request.length()-4);
			request=request.replace("showAMSBodyPortletSystemTools", "");
			
			service=new Service(Service.UI_TYPE,"PORTAL",request,"UI");

		}
		
		String duration=reqs[1].substring(reqs[1].indexOf(":")+1,reqs[1].length()-1).trim();
		Date when = null;
		if(requestStart.containsKey(reqNumber))
		{
			when=requestStart.get(reqNumber);
			requestStart.remove(reqNumber);
		}
		else
		{
			String ds[]=s.split(">");
			String dateString=ds[0].substring(5, ds[0].length()-12);
			Long L=new Long(ds[9].substring(2));
			long ll=L.longValue();
			when=new Date(ll);
		}
		Float millisecs=Float.parseFloat(duration);
		Double wait=0D;
		
		if(service!=null)
		{
			if(requestWait.containsKey(reqNumber)) 
			{
			    wait=requestWait.get(reqNumber);
			    //System.out.println(millisecs+" "+wait);
			    requestWait.remove(reqNumber);
			   
			}
			se=new ServiceEntry(service, when,millisecs);
			se.wait=wait;
			se.txn=reqNumber;
			se.location=getServer(s);
			String user=getUser(s);
			se.user=user;
			//if(!users.contains(user) & !(s.contains("unknown") && s.contains("Signout.do")) )
			//{
				//System.out.println("Adding user in request end "+user);
			//	se.logoutEvent=true;
			//	users.add(user);
			//}
			//System.out.println(se.location);
			//if(s.contains("unknown") && s.contains("Signout.do"))se.logoutEvent=true;
			
		}
		return se;
	}//end of request end line processing



	private ServiceEntry ProcessSerEnd(String s) 
	{
		String server=getIndexString(s,5);
	    int i=s.indexOf("duration(ms):");
	    String ws=s.substring(i+14,s.length()-2);
	    //System.out.println(ws);
	    Double wait=new Double(ws);
	    int y=s.indexOf("<Request #")+10;
	    int z=s.indexOf(',', y);
	    String reqNumber=server+"."+s.substring(y, z);
	    if(requestWait.containsKey(reqNumber))
	    {
	    	wait+=requestWait.get(reqNumber);
	    	requestWait.put(reqNumber, wait);
	    }
	    
		
	    ServiceEntry se=null;
	    if(serviceRequest.containsKey(reqNumber))
	    {
	    	se=new ServiceEntry(serviceRequest.get(reqNumber),serviceStarts.get(reqNumber),wait);
			se.txn=reqNumber+"--"+ ++txn;
			serviceRequest.remove(reqNumber);
			serviceStarts.remove(reqNumber);
			se.location=getServer(s);
			//System.out.println(se.location);
			String user=getUser(s);
			se.user=user;
			if(s.contains("AUDIT.startSession")) 
			{
				
				if(users.contains(user))
				{
					
					//System.out.println("User, "+user+", already active! Request number="+reqNumber);
					
				}
				else
				{
					users.add(user);
					se.loginEvent=true;
				}
			}
			if(s.contains("AUDIT.raiseSessionEvent") && !profileUpdate.contains(reqNumber)) 
			{
				
				if(users.contains(user))
				{
					se.logoutEvent=true;
					users.remove(user);
				} else
				{
					//System.out.println("User, "+user+", not active! Request number="+reqNumber);
					
				}
			}
			
	    }
	    {
	    	
	    }
		return se;
	}

	private static int txn=0;
	private HashMap<String, Double> requestWait=new HashMap<String, Double>();
	private HashMap<String, Service> serviceRequest=new HashMap<String,Service>();
	private HashMap<String, Date> serviceStarts=new HashMap<String,Date>();
	
	private ServiceEntry ProcessSerStart(String s) 
	{
		String server=getIndexString(s,5);
		Service service=null;
		ServiceEntry se=new ServiceEntry((Service) null,(Date) null, -1D);
		int y=s.indexOf("<Request #")+10;
		int z=s.indexOf(',', y);
		String reqNumber=server+"."+s.substring(y, z);
		if(requestService.containsKey(reqNumber)){
			service=requestService.get(reqNumber);
		}//else
		 //	System.out.println("Service without a request");
		y=s.indexOf("service start:")+15;
		z=s.length()-2;
		String so=s.substring(y, z);
		//System.out.println(so);
		String sn[]=so.split("\\.", 2);
		//System.out.println(sn[0]+"-"+sn[1]);
		String sname=sn[0].toUpperCase();
		String oname=sn[1].toLowerCase();
		Service service2=new Service("UIBW",sname,oname,"");
		//if(service==null)System.out.println(sname+"."+oname);
		serviceRequest.put(reqNumber, service2);
//		service.addDependentService(service2);
		String ds[]=s.split(">");
		String dateString=ds[0].substring(5, ds[0].length()-12);
		Long L=new Long(ds[9].substring(2));
		long ll=L.longValue();
		Date when=new Date(ll);
		serviceStarts.put(reqNumber, when);
			
		se=new ServiceEntry(service2, (Date) null, -1D, service);
			
		
		return se;
	}

	private ServiceEntry ProcessReqStart(String s)
	{
		String server=getIndexString(s,5);
		String request= s.substring(s.indexOf("request start: ")+15,s.length()-2);
		if(request.contains("[null]")) 
			request=request.substring(1, request.length()-7).trim();
		
		if(request.contains("[")){
			int i=request.indexOf("[");
			int j=request.indexOf("]");
			request=request.substring(i+1, j);
		}
		if(request.contains("systemtools"))
			request=request.substring(request.indexOf("systemtools")+12);
		if(request.contains("home/"))
			request=request.substring(request.indexOf("home/"));
		if(request.contains("?"))
			request=request.substring(0,request.indexOf("?"));
		
		if(request.startsWith("/AMSPortalWebProject/com/ams2/"))
			request=request.substring(30);
		
		if(request.startsWith("/AMSPortalWebProject/com/ams/"))
			request=request.substring(29);
		if(request.endsWith("do"))request=request.substring(0, request.length()-3);
		if(request.endsWith("jsp"))request=request.substring(0, request.length()-4);
		if(request.endsWith("jpf"))request=request.substring(0, request.length()-4);
		request=request.replace("showAMSBodyPortletSystemTools", "");
		//System.out.println("Processed Request: "+request);
		Service service=new Service(Service.UI_TYPE,"PORTAL",request,"UI");
			

		int y=s.indexOf("<Request #")+10;
		int z=s.indexOf(',', y);
		String reqNumber=server+"."+s.substring(y, z);
		requestService.put(reqNumber, service);
		String ds[]=s.split(">");
		String dateString=ds[0].substring(5, ds[0].length()-12);
		Long L=new Long(ds[9].substring(2));
		long ll=L.longValue();
		Date when=new Date(ll);
		requestStart.put(reqNumber, when);
		requestWait.put(reqNumber, 0D);
		if(s.contains("profile/updateProfile.do")) profileUpdate.add(reqNumber) ;
		
		ServiceEntry se=new ServiceEntry(service,(Date) null, -1D, (Service)null);
		
		return se;
		//return null;
	}
	
	private String getIndexString(String s, int index)
	{
		String is="";
		String d[]=s.split("<");
		if(d.length>index)
		{
			is=d[index].trim();
			//System.out.println(is);
			is=is.substring(0, is.length()-1);
			//System.out.println(is);
		}
		
		
		return is;
	}
	private ArrayList<String> profileUpdate=new ArrayList<String>();
	private HashMap<String, Date> requestStart=new HashMap<String,Date>();
	private HashMap<String, Service> requestService=new HashMap<String, Service>();
	
	private String getServer(String s)
	{
		String sp[]=s.split("<");
		int i=sp[5].indexOf('>', sp[5].length()-4);
		return sp[5].substring(0, i);
	}
	
	private String getUser(String s)
	{
		String user="";
		String sp[]=s.split("<");
		int i=sp[7].indexOf('>', sp[7].length()-4);
		user=sp[7].substring(0,i);
		//System.out.println("user="+user);
		if(user.equals("WLS Kernel") | user.equals("anonymous"))
		{
			// try and find the real user - this happens on session timeouts
			String sp1[]=sp[sp.length-1].split(",");
			if(sp.length>=2)user=sp[1].trim();
			if(user.equals("")) user="anonymous";
		}
		return user;
	}
	
	@Override
	public int getOrderNumber() {
		// TODO Auto-generated method stub
		return 7;
	}

	private List<String> users=Collections.synchronizedList(new ArrayList<String>());
	
	
}
