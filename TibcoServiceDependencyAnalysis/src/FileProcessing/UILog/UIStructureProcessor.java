package FileProcessing.UILog;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.Iterator;

public class UIStructureProcessor
{

	public static HashMap<String, String> LoadMappings(String filename)
	{
		HashMap<String, String> response = new HashMap<String, String>();

		// Load Service Map

		FileReader inMapFile;
		BufferedReader inMap;

		try
		{
			inMapFile = new FileReader(filename);
			inMap = new BufferedReader(inMapFile);
			boolean finished = false;
			String inLine = "";
			// Read Header Line
			inLine = inMap.readLine();
			while (!finished)
			{
				inLine = inMap.readLine();

				if (inLine == null)
				{
					finished = true;
				} else
				{
					int sepInd = inLine.indexOf(",");

					if (sepInd > -1)
					{
						String key = inLine.substring(0, sepInd);
						String value = inLine.substring(sepInd + 1,
								inLine.length());
						response.put(key, value);
					}
				}
			}
			inMap.close();

		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return response;
	}

	public static void main(String[] args)
	{
		System.out.println("Start processing\n");

		// Load Service Map

		HashMap<String, String> uiMap = null;
		HashMap<String, String> serviceMap = null;
		HashMap<String, UILogRecord> reqMap = new HashMap<String, UILogRecord>();

		uiMap = LoadMappings(args[0]);
		serviceMap = LoadMappings(args[1]);

		FileReader inFile;
		BufferedReader in;
		FileWriter outFile;
		BufferedWriter out;

		try
		{
			inFile = new FileReader(args[2]);
			in = new BufferedReader(inFile);
			outFile = new FileWriter(args[3]);
			out = new BufferedWriter(outFile);
			boolean finished = false;
			String inLine = "";
			String newLine = "";

			while (!finished)
			{
				inLine = in.readLine();
				UILogRecord newRec = null;

				if (inLine == null)
				{
					finished = true;
				} else
				{
					newRec = UILogRecord.processLine(inLine, uiMap, serviceMap);
				}

				if ((newRec != null) && !finished)
				{

					String key = newRec.getPortal() + "_" + newRec.getEvent();
					UILogRecord reqObj = reqMap.get(key);

					if (newRec.getEvent().equalsIgnoreCase("null"))
					{
						// ignore finaliser errors in the log file
					} else if (newRec.getType().equalsIgnoreCase(
							"request start"))
					{
						if (reqObj != null)
						{
							System.out
									.println("Error - Encountered a non unique request entry ("
											+ key + ")");
							reqMap.remove(key);
						}

						reqMap.put(key, newRec);
					} else if (newRec.getType().equalsIgnoreCase("request end"))
					{
						if (reqObj != null)
						{
							if (!reqObj.getService().equals(""))
							{
								out.write("MODEL,UI," + reqObj.getService()
										+ "," + reqObj.getOperation() + ",,,,");
								out.newLine();

								HashMap services = reqObj.getDependents();
								if (services != null)
								{
									Iterator iter = services.keySet()
											.iterator();

									while (iter.hasNext())
									{
										String servKey = (String) iter.next();
										UILogRecord servObj = (UILogRecord) services
												.get(servKey);

										out.write("MODEL,UI,"
												+ reqObj.getService() + ","
												+ reqObj.getOperation()
												+ ",BW," + servObj.getService()
												+ "," + servObj.getOperation()
												+ ",HTTP");
										out.newLine();
									}
								}
							} else
							{
								System.out
										.println("Error - Request has no Service name ("
												+ key + ")");
							}
							reqMap.remove(key);
						} else
						{
							System.out
									.println("Error - Request End without a matching Request Start ("
											+ key + ")");
						}
					} else if (newRec.getType().equalsIgnoreCase("service end"))
					{
						if (reqObj != null)
						{
							reqObj.addDependent(newRec);
						} else
						{
							System.out
									.println("Error - No request object available to match the service request ("
											+ key
											+ ") service("
											+ newRec.getService() + ")");
						}
					}
				}
			}
			in.close();
			out.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		System.out.println("\nEnd processing");
	}

}
