package FileProcessing.UILog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.HashMap;

public class UILogRecord
{
	String hostname = "";
	String portal = "";
	String user = "";
	Date timeStamp = null;
	String event = "";
	String type = "unknown";
	String service = "";
	String operation = "";
	String addData = "";
	long timeMilis = 0;
	int callCount = 1;
	long duration = 0;
	long waitTime = 0;
	private HashMap dependents = null;

	public void addDependent(UILogRecord dependent)
	{
		if (dependents == null)
			dependents = new HashMap();

		String key = dependent.getService() + "." + dependent.getOperation();

		UILogRecord servObj = (UILogRecord) dependents.get(key);

		if (servObj == null)
		{
			dependents.put(key, dependent);
		} else
		{
			servObj.callCount++;
		}

		waitTime += dependent.getDuration();
	}

	public HashMap getDependents()
	{
		return dependents;
	}

	public String getType()
	{
		return this.type;
	}

	public Date getTimeStamp()
	{
		return this.timeStamp;
	}

	public String getEvent()
	{
		return this.event;
	}

	public String getHostname()
	{
		return this.hostname;
	}

	public String getPortal()
	{
		return this.portal;
	}

	public String getName()
	{
		return this.service;
	}

	public String getService()
	{
		return this.service;
	}

	public String getOperation()
	{
		return this.operation;
	}

	public String getAddData()
	{
		return this.addData;
	}

	public long getDuration()
	{
		return this.duration;
	}

	public long getWaitTime()
	{
		return this.waitTime;
	}

	public void addWaitTime(long wait)
	{
		this.waitTime += wait;
	}

	public static UILogRecord processLine(String inLine,
			HashMap<String, String> uiMap, HashMap<String, String> serviceMap)
	{
		UILogRecord response = null;

		String[] split = inLine.split("> <");

		if (split[1].equalsIgnoreCase("INFO")
				&& split[2].equalsIgnoreCase("AMS_PERF"))
		{
			response = new UILogRecord();
			response.hostname = split[3];
			response.portal = split[4];
			response.user = split[6];
			response.timeMilis = Long.parseLong(split[9].trim());

			response.timeStamp = new Date(response.timeMilis);

			String dummy = split[11];
			response.event = dummy.substring(9, dummy.indexOf(","));

			int durIndex = dummy.indexOf("duration(ms):");
			if (durIndex > -1)
			{
				response.duration = Long.parseLong(dummy.substring(
						durIndex + 14, dummy.indexOf(">", durIndex)));
			}

			int servIndex = dummy.indexOf(", service");
			int reqIndex = dummy.indexOf(", request");
			int baseIndex = 0;

			if (servIndex > -1)
			{
				baseIndex = servIndex + 2;
			} else if (reqIndex > -1)
			{
				baseIndex = reqIndex + 2;
			}

			if (baseIndex > 0)
			{
				response.type = dummy.substring(baseIndex,
						dummy.indexOf(":", baseIndex));

				int endName = dummy.indexOf(",", baseIndex);
				if (endName < 0)
					endName = dummy.indexOf(">", baseIndex);

				String servName = dummy.substring(
						dummy.indexOf(":", baseIndex) + 2, endName);

				if (servName != null && servName.length() > 0)
				{
					response.ProcessServiceName(response.type, servName, uiMap,
							serviceMap);
				} else
				{
					response = null;
					// response.service = "PORTAL";
					// response.operation = "No URL To Process";
				}
			}
		}

		return response;
	}

	private void ProcessServiceName(String type, String url,
			HashMap<String, String> uiMap, HashMap<String, String> serviceMap)
	{
		String locService = "PORTAL";
		String locOperation = url;
		String locAddData = "";

		if (type.equalsIgnoreCase("request start")
				|| type.equalsIgnoreCase("request end"))
		{
			int overInd = locOperation.indexOf("actionOverride=");
			String command = "";
			boolean keyFound = false;

			if (overInd > -1)
			{
				locOperation = locOperation.substring(overInd + 15,
						locOperation.length()).trim();
				locOperation = locOperation.replaceAll("%2F", "/");
				String key = locOperation;

				if (uiMap.containsKey(key))
				{
					keyFound = true;
					command = uiMap.get(key);
				} else
				{
					int ampInd = key.indexOf("&");
					int braceInd = key.indexOf("[");

					if (ampInd > -1)
					{
						key = key.substring(0, ampInd).trim();
						keyFound = uiMap.containsKey(key);
					} else if (braceInd > -1)
					{
						key = key.substring(0, braceInd - 1).trim();
						keyFound = uiMap.containsKey(key);
					}

					if (keyFound)
					{
						command = uiMap.get(key);
					}
				}

				if (keyFound)
				{
					locOperation = ProcessUIMapping(command, locOperation);
				} else
				{
					System.out.println("Key not found in the UI URL Map for ("
							+ url + ")");
				}
			} else if (locOperation.startsWith("selectedWarningID"))
			{
				String key = "SelectedWarning";
				String action = GetURLNameValue("selectedAction", locOperation);
				if (action != null)
				{
					key = "SelectedWarningAction" + action;
				} else
				{
					String warning = GetURLNameValue("selectedWarning",
							locOperation);
					if (warning != null)
					{
						key = "SelectWarning_" + warning.replace("%20", " ");
					}
				}

				locOperation = key;
				if (uiMap.containsKey(key))
				{
					locOperation = ProcessUIMapping(uiMap.get(key), key);
				}
			} else if (locOperation.startsWith("selectedMatchID"))
			{
				String matchType = GetURLNameValue("selectedMatchType",
						locOperation);
				String key = "SelectedMatch";
				if (matchType != null)
				{
					key = "SelectedMatchType" + matchType;
				}

				locOperation = key;

				if (uiMap.containsKey(key))
				{
					locOperation = ProcessUIMapping(uiMap.get(key), key);
				}
			} else if (locOperation.startsWith("url="))
			{
				String key = locOperation.substring(4).trim();
				key = key.replaceAll("%2F", "/");
				locOperation = key;

				if (uiMap.containsKey(key))
				{
					locOperation = ProcessUIMapping(uiMap.get(key), key);
					System.out.println("Mapped url=" + key + " to "
							+ locOperation);
				} else
				{
					System.out.println("Failed to map url=" + key);
				}
			} else if (locOperation.contains("="))
			{
				String key = locOperation.substring(0,
						locOperation.indexOf("="));

				locOperation = key;

				if (uiMap.containsKey(key))
				{
					locOperation = ProcessUIMapping(uiMap.get(key), key);
					System.out.println("Mapped value pair=" + key + " to "
							+ locOperation);
				} else
				{
					System.out.println("Failed to map value pair=" + key);
				}
			} else
			{
				System.out.println("Not a recognised URL (" + url + ")");
			}
		} else if (type.equalsIgnoreCase("service start")
				|| type.equalsIgnoreCase("service end"))
		{
			if (serviceMap.containsKey(url))
			{
				url = serviceMap.get(url);
			}

			int dotInd = url.indexOf(".");
			if (dotInd > -1)
			{
				locOperation = url.substring(dotInd + 1, url.length());
				locService = url.substring(0, dotInd);
			}

			if (serviceMap.containsKey(locService))
			{
				locService = serviceMap.get(locService);
			}
		}

		service = locService;
		operation = locOperation;
		addData = locAddData;
	}

	private static String RemoveNoise(String url)
	{
		String response = url;
		int ampInd = response.indexOf("&");

		if (ampInd > -1)
		{
			response = response.substring(0, ampInd);
		}

		int braceInd = response.indexOf("[");

		if (braceInd > -1)
		{
			response = response.substring(0, braceInd - 1);
		}

		return response;
	}

	public String toString()
	{
		return this.formatTimeStamp(this.timeStamp) + "," + this.type + ","
				+ this.getName() + "," + this.event + "," + this.duration;
	}

	private String formatTimeStamp(Date time)
	{
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss.SSS");
		SimpleTimeZone tz = new SimpleTimeZone(0, "GMT");
		df.setTimeZone(tz);
		return df.format(time);
	}

	private static String ProcessUIMapping(String command, String url)
	{
		String response = url;

		if (command.startsWith("TRIM"))
		{
			response = RemoveNoise(url);
			int slashInd = response.lastIndexOf("/");

			if (slashInd > -1)
			{
				response = response.substring(slashInd + 1, response.length());
			}
		} else if (command.startsWith("REPLACE"))
		{
			response = command.substring(8);
		}

		return response;
	}

	private static String GetURLNameValue(String name, String url)
	{
		String response = null;

		if (name != null && url != null && url.length() > 0)
		{
			int nameLen = name.length();

			if (nameLen > 0)
			{
				int nameInd = url.indexOf(name);

				if (nameInd > -1)
				{
					int ampInd = url.indexOf("&", nameInd + nameLen);
					if (ampInd > nameInd)
					{
						response = url.substring(nameInd + nameLen + 1, ampInd);
					} else
					{
						response = url.substring(nameInd + nameLen + 1);
					}
				}
			}
		}

		return response;
	}
}
