package FileProcessing.UILog;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.SimpleTimeZone;
import java.util.HashMap;

import FileProcessing.JobStats.SimpleTime;

public class UIStatsProcessor
{

	public static HashMap<String, String> LoadMappings(String filename)
	{
		HashMap<String, String> response = new HashMap<String, String>();

		// Load Service Map

		FileReader inMapFile;
		BufferedReader inMap;

		try
		{
			inMapFile = new FileReader(filename);
			inMap = new BufferedReader(inMapFile);
			boolean finished = false;
			String inLine = "";
			// Read Header Line
			inLine = inMap.readLine();
			while (!finished)
			{
				inLine = inMap.readLine();

				if (inLine == null)
				{
					finished = true;
				} else
				{
					int sepInd = inLine.indexOf(",");

					if (sepInd > -1)
					{
						String key = inLine.substring(0, sepInd);
						String value = inLine.substring(sepInd + 1,
								inLine.length());
						response.put(key, value);
					}
				}
			}
			inMap.close();

		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return response;
	}

	public static void main(String[] args)
	{
		System.out.println("Start processing\n");

		HashMap<String, String> uiMap = null;
		HashMap<String, String> serviceMap = null;
		HashMap<String, UILogRecord> reqMap = new HashMap<String, UILogRecord>();

		int argCount = args.length;
		int argInd = 2;

		String inFilename = args[0];
		String outFilename = args[1];
		String format = "yyyy-MM-dd HH:mm:ss:SSS";

		long lowerTime = -1;
		long upperTime = -1;

		while (argInd < argCount)
		{
			if (args[argInd].equalsIgnoreCase("--UIMAP"))
			{
				argInd++;
				uiMap = LoadMappings(args[argInd]);
			}
			if (args[argInd].equalsIgnoreCase("--SRVMAP"))
			{
				argInd++;
				serviceMap = LoadMappings(args[argInd]);
			} else if (args[argInd].equalsIgnoreCase("--LT"))
			{
				argInd++;
				lowerTime = SimpleTime.TimeToMillisecs(args[argInd], format);
			} else if (args[argInd].equalsIgnoreCase("--UT"))
			{
				argInd++;
				upperTime = SimpleTime.TimeToMillisecs(args[argInd], format);
			}
			argInd++;
		}

		FileReader inFile;
		BufferedReader in;
		FileWriter outFile;
		BufferedWriter out;

		try
		{
			inFile = new FileReader(inFilename);
			in = new BufferedReader(inFile);
			outFile = new FileWriter(outFilename);
			out = new BufferedWriter(outFile);
			boolean finished = false;
			String inLine = "";

			while (!finished)
			{
				boolean drop = false;

				inLine = in.readLine();
				UILogRecord newRec = null;

				if (inLine == null)
				{
					finished = true;
				} else
				{
					newRec = UILogRecord.processLine(inLine, uiMap, serviceMap);
				}

				if ((newRec != null) && !finished)
				{
					if (lowerTime > 0 || upperTime > 0)
					{
						long recTime = newRec.timeMilis;

						if (lowerTime > 0 && recTime < lowerTime)
						{
							drop = true;
						} else if (upperTime > 0 && recTime > upperTime)
						{
							drop = true;
						}
					}

					if (!drop)
					{
						String key = newRec.getPortal() + "_"
								+ newRec.getEvent();
						UILogRecord reqObj = (UILogRecord) reqMap.get(key);

						if (newRec.getEvent().equalsIgnoreCase("null"))
						{
							// ignore finaliser errors in the log file
						} else if (newRec.getType().equalsIgnoreCase(
								"request start"))
						{
							if (reqObj != null)
							{
								System.out
										.println("Error - Encountered a non unique request entry ("
												+ key + ")");
								reqMap.remove(key);
							}

							reqMap.put(key, newRec);
						} else if (newRec.getType().equalsIgnoreCase(
								"request end"))
						{
							if (reqObj != null)
							{
								if (!reqObj.getService().equals(""))
								{
									out.write("SAMPLE,UI,"
											+ formatTimeStamp(reqObj
													.getTimeStamp())
											+ ","
											+ reqObj.getService()
											+ ","
											+ reqObj.getOperation()
											+ ","
											+ (newRec.getDuration() - reqObj
													.getWaitTime()) + ","
											+ newRec.getDuration());
									out.newLine();
								} else
								{
									System.out
											.println("Error - Request has no Service name ("
													+ key + ")");
								}
								reqMap.remove(key);
							} else
							{
								System.out
										.println("Error - Request End without a matching Request Start ("
												+ key + ")");
							}
						} else if (newRec.getType().equalsIgnoreCase(
								"service end"))
						{
							if (reqObj != null)
							{
								reqObj.addDependent(newRec);
							} else
							{
								System.out
										.println("Error - No request object available to match the service request ("
												+ key
												+ ") service("
												+ newRec.getService() + ")");
							}
						}
					} else
					{
						System.out.println("Record dropped by time Filter ("
								+ formatTimeStamp(new Date(newRec.timeMilis))
								+ ")");
					}
				}
			}
			in.close();
			out.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		System.out.println("\nEnd processing");
	}

	private static String formatTimeStamp(Date time)
	{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		SimpleTimeZone tz = new SimpleTimeZone(0, "GMT");
		df.setTimeZone(tz);
		return df.format(time);
	}

}
