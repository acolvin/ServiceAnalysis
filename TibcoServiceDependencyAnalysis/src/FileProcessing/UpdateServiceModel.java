package FileProcessing;

import java.util.List;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.TFileReader;

public class UpdateServiceModel
{

	public static void main(String[] args)
	{

		System.out.println("\nStart Processing");

		String oldModFile = args[0];
		String newModFile = args[1];

		TFileReader t = new TFileReader();
		List<Service> serviceList = null;

		try
		{
			t.ProcessFile(oldModFile);

			serviceList = t.getServiceOperation();

			// ModelFileReader.SaveServiceModel(newModFile, serviceList);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		System.out.println("\nEnd processing");
	}

}
