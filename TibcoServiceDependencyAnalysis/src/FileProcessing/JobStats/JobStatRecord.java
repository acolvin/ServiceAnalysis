package FileProcessing.JobStats;

import java.util.Date;
import java.util.HashMap;

public class JobStatRecord
{
	String JobID = "";
	String serviceRef = "";
	String startTime = "";
	String endTime = "";
	long elapsedTime = 0;
	long evalTime = 0;
	String status = "";
	Date timeStamp = null;
	String type = "BW";
	String service = "";
	String operation = "";

	public String getType()
	{
		return this.type;
	}

	public Date getTimeStamp()
	{
		return this.timeStamp;
	}

	public String getName()
	{
		return this.service;
	}

	public String getService()
	{
		return this.service;
	}

	public String getOperation()
	{
		return this.operation;
	}

	public long getElapsedTime()
	{
		return this.elapsedTime;
	}

	public long getEvalTime()
	{
		return this.evalTime;
	}

	public static JobStatRecord ProcessLine(String inLine,
			HashMap<String, String> serviceMap)
	{
		JobStatRecord record = null;

		if (!inLine.startsWith("jobId,"))
		{
			String[] split = inLine.split(",", 7);

			if (split.length == 7)
			{
				record = new JobStatRecord();

				record.JobID = split[0].trim();
				record.serviceRef = split[1].trim();
				record.startTime = split[2].trim();
				record.endTime = split[3].trim();
				record.elapsedTime = Long.parseLong(split[4].trim());
				record.evalTime = Long.parseLong(split[5].trim());
				record.status = split[6].trim();

				if (record.status.equalsIgnoreCase("SUCCESS"))
				{
					record = ProcessServiceRef(record, serviceMap);
				} else
				{
					record = null;
				}
			}
		}

		return record;
	}

	private static JobStatRecord ProcessServiceRef(JobStatRecord record,
			HashMap<String, String> serviceMap)
	{
		JobStatRecord respRecord = null;
		String servRef = record.serviceRef;
		String service = "";
		String operation = "";

		if (!(servRef.contains("starterProcesses")
				|| servRef.contains("EngineStart.process") || servRef
					.contains("WSInspection")))
		{
			String servSplit[] = servRef.trim().split("/");

			service = servSplit[3].trim().toUpperCase();
			operation = servSplit[servSplit.length - 1].trim().toLowerCase();

			service = service.replace("SERVICE", "");

			int opStart = 0;
			int opEnd = operation.length();
			if (operation.startsWith("ws"))
				opStart = 2;
			if (operation.endsWith(".process"))
				opEnd = opEnd - 8;

			operation = operation.substring(opStart, opEnd);

			int vInd = operation.indexOf("_v");
			if (vInd > -1)
				operation = operation.substring(0, vInd);

			if (serviceMap != null)
			{
				if (serviceMap.containsKey(service))
				{
					service = serviceMap.get(service);
				}

				if (serviceMap.containsKey(operation))
				{
					operation = serviceMap.get(operation);
				}
			}

			record.service = service;
			record.operation = operation;

			respRecord = record;
		}

		return respRecord;
	}

}
