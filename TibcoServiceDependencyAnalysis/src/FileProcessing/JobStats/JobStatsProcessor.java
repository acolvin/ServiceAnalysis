package FileProcessing.JobStats;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.SimpleTimeZone;
import java.util.HashMap;

public class JobStatsProcessor
{

	public static HashMap<String, String> LoadMappings(String filename)
	{
		HashMap<String, String> response = new HashMap<String, String>();

		// Load Service Map

		FileReader inMapFile;
		BufferedReader inMap;

		try
		{
			inMapFile = new FileReader(filename);
			inMap = new BufferedReader(inMapFile);
			boolean finished = false;
			String inLine = "";
			// Read Header Line
			inLine = inMap.readLine();
			while (!finished)
			{
				inLine = inMap.readLine();

				if (inLine == null)
				{
					finished = true;
				} else
				{
					int sepInd = inLine.indexOf(",");

					if (sepInd > -1)
					{
						String key = inLine.substring(0, sepInd);
						String value = inLine.substring(sepInd + 1,
								inLine.length());
						response.put(key, value);
					}
				}
			}
			inMap.close();

		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return response;
	}

	public static void main(String[] args)
	{
		System.out.println("Start processing\n");

		HashMap<String, String> serviceMap = null;
		String inFilename = null;
		String outFilename = null;
		int argCount = args.length;
		int argInd = 2;
		inFilename = args[0];
		outFilename = args[1];
		String format = "yyyy-MM-dd HH:mm:ss:SSS";

		long lowerTime = -1;
		long upperTime = -1;

		while (argInd < argCount)
		{
			if (args[argInd].equalsIgnoreCase("--MAP"))
			{
				argInd++;
				serviceMap = LoadMappings(args[argInd]);
			} else if (args[argInd].equalsIgnoreCase("--LT"))
			{
				argInd++;
				lowerTime = SimpleTime.TimeToMillisecs(args[argInd], format);
			} else if (args[argInd].equalsIgnoreCase("--UT"))
			{
				argInd++;
				upperTime = SimpleTime.TimeToMillisecs(args[argInd], format);
			}
			argInd++;
		}

		FileReader inFile;
		BufferedReader in;
		FileWriter outFile;
		BufferedWriter out;

		try
		{
			inFile = new FileReader(inFilename);
			in = new BufferedReader(inFile);
			outFile = new FileWriter(outFilename);
			out = new BufferedWriter(outFile);
			boolean finished = false;
			String inLine = "";

			JobStatRecord newRec = null;

			while (!finished)
			{
				boolean drop = false;

				inLine = in.readLine();

				if (inLine == null)
				{
					finished = true;
				} else
				{
					newRec = JobStatRecord.ProcessLine(inLine, serviceMap);
				}

				if ((newRec != null) && !finished)
				{

					if (lowerTime > 0 || upperTime > 0)
					{
						long recTime = SimpleTime.TimeToMillisecs(
								newRec.startTime, format);

						if (lowerTime > 0 && recTime < lowerTime)
						{
							drop = true;
						} else if (upperTime > 0 && recTime > upperTime)
						{
							drop = true;
						}
					}

					if (!drop)
					{
						out.write("SAMPLE," + newRec.getType() + ","
								+ newRec.startTime + "," + newRec.getService()
								+ "," + newRec.getOperation() + ","
								+ newRec.getEvalTime() + ","
								+ newRec.getElapsedTime());
						out.newLine();
					} else
					{
						System.out.println("Record dropped by time Filter ("
								+ newRec.startTime + ")");
					}
				}
			}
			in.close();
			out.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		System.out.println("\nEnd processing");
	}

	private static String formatTimeStamp(Date time)
	{
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss.SSS");
		SimpleTimeZone tz = new SimpleTimeZone(0, "GMT");
		df.setTimeZone(tz);
		return df.format(time);
	}

}
