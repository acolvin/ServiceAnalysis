package FileProcessing.JobStats;

import java.text.SimpleDateFormat;
import java.util.SimpleTimeZone;
import java.util.Date;

public class SimpleTime
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		String format = "";

		if (args[0].equalsIgnoreCase("TTOM"))
		{
			if (args.length > 2)
				format = args[2];

			System.out.println("TimeStamp to convert = " + args[1]
					+ " format = " + format);

			long millis = TimeToMillisecs(args[1], format);

			System.out.println("Resultant Milliseconds = " + millis);
		} else if (args[0].equalsIgnoreCase("MTOT"))
		{
			if (args.length > 2)
				format = args[2];
			long millis = Long.parseLong(args[1]);

			System.out.println("Millis to convert = " + args[1] + " format = "
					+ format);

			String timeStamp = MillisecsToTime(millis, format);

			System.out.println("Resultant Time Stamp = " + timeStamp);
		}
	}

	public static long TimeToMillisecs(String timeStamp, String format)
	{
		long resp = 0;
		if (format == null || format.equals(""))
			format = "dd/MM/yyyy-HH:mm:ss.SSS";

		SimpleDateFormat df = new SimpleDateFormat(format);
		SimpleTimeZone tz = new SimpleTimeZone(0, "GMT");
		df.setTimeZone(tz);

		try
		{
			Date dateTime = df.parse(timeStamp);
			resp = dateTime.getTime();
		} catch (Exception exp)
		{
		}

		return resp;
	}

	public static String MillisecsToTime(long millisecs, String format)
	{
		String resp = "";
		if (format == null || format.equals(""))
			format = "dd/MM/yyyy-HH:mm:ss.SSS";

		SimpleDateFormat df = new SimpleDateFormat(format);
		SimpleTimeZone tz = new SimpleTimeZone(0, "GMT");
		df.setTimeZone(tz);

		resp = df.format(new Date(millisecs));

		return resp;
	}

}
