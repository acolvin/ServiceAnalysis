package database;

import internalQueues.Queue;
import internalQueues.QueueNamingException;
import internalQueues.QueueServer;
import internalQueues.ServerRunningException;

import java.io.PrintWriter;
import java.net.UnknownHostException;

import org.apache.derby.drda.NetworkServerControl;

public class DBController //extends Thread
{
	
	public DBController()
	{
		QServer=QueueServer.getInstance();
		try {
			QServer.addQueue("DBCommitQueue");
			DBCommitQueue=QServer.getQueue("DBCommitQueue");
		} catch (QueueNamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServerRunningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBC=this;
	}
	
	public static DBController DBC=null;
	static boolean DB_UP=false;
	QueueServer QServer=null;
	Queue DBCommitQueue=null;
	
	public static void shutdown()
	{
		if(DB_UP & iStartedDB) 
		{
			try
			{
				NetworkServerControl nsc=new NetworkServerControl();
				nsc.shutdown();
				DB_UP=false;
				System.out.println("Database Shutdown");
			} catch (UnknownHostException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

	private static boolean iStartedDB=false;

	public static boolean isRunning()
	{
		return DB_UP;
	}
	
	
	public void start()
	{
		try
		{
			int i =0;
			byte b[]={0,0,0,0};
			System.out.println("starting derby");
			DB_UP=false;//true;
			NetworkServerControl nsc=new NetworkServerControl();//(InetAddress.getByAddress(b),1527);
			
			//check server isnt already up
			try
			{
				nsc.ping();
				if(iStartedDB==false)
				{
					System.out.println("DB is already started by another process");
					//iStartedDB=false;
					DB_UP=true;
				} else 
				{
					System.out.println("DB has already been started by this program");
					DB_UP=true;
				}
				return;
				
			}catch(Exception es)
			{
			
				nsc.start(new PrintWriter(System.out,true));
				iStartedDB=true;
				DB_UP=false;
				while(!DB_UP & i<6)
				{
					try
					{
						nsc.ping();
						DB_UP=true;
						System.out.println("Database up");
					}
						catch(Exception e)
							{System.out.println("Db still not up");}
					try {
							if(!DB_UP)Thread.sleep(5000);
						} catch(Exception e){}
				
				}
			}
			//Thread.sleep(1000);
			
			//while(true) Thread.sleep(1000000);
			
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			DB_UP=false;
		}
		
		
	}
}
