package database.snapshot;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBAction;
import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisModel.readersWriters.BWReader;
import serviceAnalysisModel.readersWriters.UserLoginEvent;
import serviceAnalysisModel.readersWriters.UserLoginProfile;
import serviceAnalysisModel.readersWriters.UserProfiles;

public class SaveSnapshot extends DBAction {

	@Override
	public void execute() throws SQLException {
		
		if(snapshottimestamp!=-1)
		{
			statement = connection.createStatement();
			
			statement.executeUpdate(
					"insert into SNAPSHOTS (SNAPSHOTTIMESTAMP, SNAPSHOTDESC) values ("
							+ snapshottimestamp + ",'"
							+ description + "')");
			
			//save user login data
			BWReader reader = Controller.getController().getReader();
			
			Set<String> loggedIn = reader.getLoggedIn();
			Map<String, Long> autoLogouts = reader.getAutoLogouts();
			SortedMap<Long, Integer> logins = reader.getLogins();
			//PreparedStatement pStatement1=connection.prepareStatement("insert into LOGINS (SNAPSHOTTIMESTAMP, TIMESTAMP,COUNT) values (?,?,?)");
			//PreparedStatement pStatement2=connection.prepareStatement("insert into LOGGEDIN (SNAPSHOTTIMESTAMP, USERNAME) values (?,?)");
			//PreparedStatement pStatement3=connection.prepareStatement("insert into AUTOLOGOUTS (SNAPSHOTTIMESTAMP, USERNAME,TIMESTAMP) values (?,?,?)");
			PreparedStatement pStatement4=connection.prepareStatement("insert into LOGIN_EVENTS (SNAPSHOTTIMESTAMP, USERNAME,STARTTIME,ENDTIME) values (?,?,?,?)");

			//pStatement1.setLong(1, snapshottimestamp);
			//pStatement2.setLong(1, snapshottimestamp);
			//pStatement3.setLong(1, snapshottimestamp);
			pStatement4.setLong(1, snapshottimestamp);
			
			/*for(String user:loggedIn){
				pStatement2.setString(2, user);
				pStatement2.executeUpdate();
			}
			for(Entry<Long, Integer> e:logins.entrySet()){
				pStatement1.setInt(3, e.getValue());
				pStatement1.setLong(2, e.getKey());
				pStatement1.executeUpdate();
			}
			for(Entry<String, Long> e:autoLogouts.entrySet()){
				pStatement3.setString(2, e.getKey());
				pStatement3.setLong(3, e.getValue());
				pStatement3.executeUpdate();
			}
			*/
			//save user login profile now
			UserProfiles p = reader.getProfiles();
			for(UserLoginProfile u:p.profiles.values()){
				Collection<UserLoginEvent> events = u.getEvents();
				for(UserLoginEvent event:events){
					pStatement4.setString(2, event.user);
					pStatement4.setLong(3, event.start);
					pStatement4.setLong(4, event.end);
					pStatement4.executeUpdate();
				}
			}
		}
	}
	
	private Statement statement=null;
	
	public void postCommit()
	{
		super.postCommit();
		try {
			statement.close();
		} catch (SQLException e) {
			
		}
		statement=null;
		
	}

	long snapshottimestamp=-1;
	String description=null;
	
	public void setSnapshot(Date date, String desc)
	{
		snapshottimestamp=date.getTime();
		if (desc == null || desc.isEmpty())
			description = date.toString();
		else description=desc;
		if (description.length() > 98)
			description = description.substring(0, 98);
		
	}
	
	
	
}
