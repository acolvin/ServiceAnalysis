package database.snapshot;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import serviceAnalysisModel.Service;
import database.DBAction;

public class SnapshotServices extends DBAction {

	@Override
	public void execute() throws SQLException {
		//System.out.println("saving summary");
		if(snapshottimestamp!=-1)
		{
			//statement = connection.createStatement();

			
			pStatement=connection.prepareStatement("insert into SERVICE (SNAPSHOTTIMESTAMP, SERVICE_NAME, SERVICE_OPERATION, SERVICE_LAYER,SERVICE_ADJ) " +
					"values (?,?,?,?,?)");
			PreparedStatement pStatement2=connection.prepareStatement("insert into SERVICE_HIER (SNAPSHOTTIMESTAMP, PARENT_SERVICE_NAME, PARENT_SERVICE_OPERATION, PARENT_SERVICE_LAYER, CHILD_SERVICE_NAME, CHILD_SERVICE_OPERATION, CHILD_SERVICE_LAYER) values (?,?,?,?,?,?,?)");
			PreparedStatement pStatement3=connection.prepareStatement("insert into samplesize(SNAPSHOTTIMESTAMP,SERVICE_NAME,SERVICE_OPERATION,SERVICE_LAYER ,SAMPLESIZE) values (?,?,?,?,?)");
			PreparedStatement pStatement4=connection.prepareStatement("insert into samplecounts (SNAPSHOTTIMESTAMP, SERVICE_NAME, SERVICE_OPERATION, SERVICE_LAYER, SAMPLE, COUNT, SSUM, SMIN, SMAX,SSQ) values (?,?,?,?,?,?,?,?,?,?)");
			PreparedStatement pStatement5=connection.prepareStatement("insert into SERVICE_SNAPSHOT (SNAPSHOTTIMESTAMP, SERVICE, RESPONSE, QTY,WAIT,LAYER) values (?,?,?,?,?,?)");
			
			pStatement.setLong(1, snapshottimestamp);
			pStatement2.setLong(1, snapshottimestamp);
			pStatement3.setLong(1, snapshottimestamp);
			pStatement4.setLong(1, snapshottimestamp);
			pStatement5.setLong(1, snapshottimestamp);
			
			for(Service s:services)
			{
				
				
				pStatement.setString(2, s.getServiceName());
				pStatement2.setString(5, s.getServiceName());
				pStatement3.setString(2, s.getServiceName());
				pStatement4.setString(2, s.getServiceName());
				pStatement.setString(3, s.getOperationName());
				pStatement2.setString(6, s.getOperationName());
				pStatement3.setString(3, s.getOperationName());
				pStatement4.setString(3, s.getOperationName());
				pStatement.setString(4, s.getType());
				pStatement2.setString(7, s.getType());
				pStatement3.setString(4, s.getType());
				pStatement4.setString(4, s.getType());
				pStatement5.setString(6, s.getType());
				
				pStatement5.setString(2, s.getServiceName()+"..."+s.getOperationName());
				
				pStatement.setFloat(5,s.getExplicitAdjustments());
				pStatement.executeUpdate();
				
				for (Map.Entry<Double, Double> entry : s.getSamples()
						.entrySet())
				{
					double response = entry.getKey();
					double qty = entry.getValue();
					double wait = s.getAvgWaitTime();
					pStatement5.setDouble(3, response);
					pStatement5.setDouble(4, qty);
					pStatement5.setDouble(5, wait);
					pStatement5.executeUpdate();
					fireProgressEvents();
				}
				
				
				
				
				Iterator<Service> it = s.getConsumers();
				
				while (it.hasNext())
				{
					Service ps = it.next();
					pStatement2.setString(2, ps.getServiceName());
					pStatement2.setString(3, ps.getOperationName());
					pStatement2.setString(4, ps.getType());
					pStatement2.executeUpdate();
					fireProgressEvents();
				}
				
				HashMap<Long,Integer> cps=s.getCountPerSecond();
				HashMap<Long,Double> sps=s.getSumsPerSecond();
				HashMap<Long,Double> ssq=s.getSumSqPerSecond();
				HashMap<Long, Double> maxPS = s.getMaxPerSecond();
				HashMap<Long, Double> minPS = s.getMinPerSecond();
				if(cps.size()>0)
				{//samplecounts to save
					pStatement3.setInt(5, s.getSamplePeriod());
					pStatement3.executeUpdate();
					for(Long l:cps.keySet())
					{
						int c=cps.get(l);
						if(sps.containsKey(l))
							pStatement4.setDouble(7, sps.get(l));
						else
							pStatement4.setNull(7, java.sql.Types.DOUBLE);
						if(minPS.containsKey(l))
							pStatement4.setDouble(8, minPS.get(l));
						else
							pStatement4.setNull(8, java.sql.Types.DOUBLE);
						if(maxPS.containsKey(l))
							pStatement4.setDouble(9, maxPS.get(l));
						else
							pStatement4.setNull(9, java.sql.Types.DOUBLE);
						if(ssq.containsKey(l))
							pStatement4.setDouble(10, ssq.get(l));
						else
							pStatement4.setNull(10, java.sql.Types.DOUBLE);
						
						pStatement4.setLong(5, l);
						pStatement4.setInt(6, c);
						pStatement4.executeUpdate();
						fireProgressEvents();
					}
					//fireProgressEvents();
				}
				fireProgressEvents();
			}
		}
	}
	
	private PreparedStatement pStatement=null;
	private Statement statement=null;
	
/*	public void postCommit()
	{
		super.postCommit();
		try {
			pStatement.close();
		} catch (SQLException e) {
			
		}
		pStatement=null;
		
	}
*/

	long snapshottimestamp=-1;
	List<Service> services=null;
	
	public void setSnapshot(Date date, List<Service> SO)
	{
		snapshottimestamp=date.getTime();
		services=SO;
		
	}
	
	protected int getProgressCount()
	{
		int size=0;
		size+= services.size();
		for(Service s:services)
		{
			size+=s.getSamples().size();
			size+=s.getConsumerCount();
			size+=s.getCountPerSecond().size();
		}
		return size;
	}
	
}
