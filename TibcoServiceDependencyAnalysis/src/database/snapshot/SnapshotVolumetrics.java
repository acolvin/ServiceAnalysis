package database.snapshot;

import java.util.Date;
import java.sql.SQLException;
import java.sql.PreparedStatement;

import serviceAnalysisModel.VolumetricModel;
import serviceAnalysisModel.VolumetricType;
import serviceAnalysisModel.Workstream;

import database.DBAction;

public class SnapshotVolumetrics extends DBAction {

	@Override
	public void execute() throws SQLException {
		
		if(snapshottimestamp!=-1 && vm != null)
		{
			PreparedStatement s1 = connection.prepareStatement("insert into VOLUMETRICS (SNAPSHOTTIMESTAMP, VOLUMETRIC, QTY) values (?,?,?)");
			PreparedStatement s2 = connection.prepareStatement("insert into VOLUMETRICS_HIER (SNAPSHOTTIMESTAMP, VOLUMETRIC, WORKSTREAM, ITERATION) values (?,?,?,?)");
			
			s1.setLong(1, snapshottimestamp);
			s2.setLong(1, snapshottimestamp);
			
			for (VolumetricType v : vm.model.keySet())
			{
				int qty = vm.model.get(v);
				s1.setString(2, v.getName());
				s2.setString(2, v.getName());
				
				s1.setInt(3, qty);
				s1.executeUpdate();
				// now do volume->ws

				for (Workstream ws : v.getWorkstreamProportion().keySet())
				{
					double multi = v.getWorkstreamMultiplier(ws);
					s2.setString(3, ws.getName());
					s2.setDouble(4, multi);
					s2.executeUpdate();
				}
				
				fireProgressEvents();
			}
		}
	}
	
//	private Statement statement=null;
	


	VolumetricModel vm=null;
	long snapshottimestamp=-1;
	
	public void setSnapshot(Date date,VolumetricModel vm)
	{
		snapshottimestamp=date.getTime();
		this.vm=vm;
	}
	
	protected int getProgressCount()
	{
		return vm.model.size();
	}
}
