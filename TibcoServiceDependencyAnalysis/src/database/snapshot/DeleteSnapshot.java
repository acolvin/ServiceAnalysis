package database.snapshot;

import java.util.Date;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBAction;

public class DeleteSnapshot extends DBAction {

	@Override
	public void execute() throws SQLException {
		
		if(snapshottimestamp!=-1)
		{
			statement = connection.createStatement();
			statement.executeUpdate(query1+snapshottimestamp);
			statement.executeUpdate(query2+snapshottimestamp);
			statement.executeUpdate(query3+snapshottimestamp);
			statement.executeUpdate(query4+snapshottimestamp);
			statement.executeUpdate(query5+snapshottimestamp);
			statement.executeUpdate(query6+snapshottimestamp);
			statement.executeUpdate(query7+snapshottimestamp);
			statement.executeUpdate(query8+snapshottimestamp);
			statement.executeUpdate(query9+snapshottimestamp);
			statement.executeUpdate(query+snapshottimestamp);
			
		}
	}
	
	private Statement statement=null;
	
	public void postCommit()
	{
		super.postCommit();
		try {
			statement.close();
		} catch (SQLException e) {
			
		}
		statement=null;
		
	}

	long snapshottimestamp=-1;
	
	public void setSnapshot(Date date)
	{
		snapshottimestamp=date.getTime();
	}
	private final String query="DELETE FROM SNAPSHOTS WHERE SNAPSHOTTIMESTAMP=";
	private final String query1="DELETE FROM SERVICE_SNAPSHOT WHERE SNAPSHOTTIMESTAMP=";
	private final String query2="DELETE FROM VOLUMETRICS WHERE SNAPSHOTTIMESTAMP=";
	private final String query3="DELETE FROM VOLUMETRICS_HIER WHERE SNAPSHOTTIMESTAMP=";
	private final String query4="DELETE FROM SERVICE WHERE SNAPSHOTTIMESTAMP=";
	private final String query5="DELETE FROM SERVICE_HIER WHERE SNAPSHOTTIMESTAMP=";
	private final String query6="DELETE FROM SAMPLESIZE WHERE SNAPSHOTTIMESTAMP=";
	private final String query7="DELETE FROM SAMPLECOUNTS WHERE SNAPSHOTTIMESTAMP=";
	private final String query8="DELETE FROM SUMMARY_SNAPSHOT WHERE SNAPSHOTTIMESTAMP=";
	private final String query9="DELETE FROM LOGIN_EVENTS WHERE SNAPSHOTTIMESTAMP=";
	
}
