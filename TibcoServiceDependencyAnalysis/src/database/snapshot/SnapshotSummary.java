package database.snapshot;

import java.util.Date;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import serviceAnalysisModel.Service;

import database.DBAction;

public class SnapshotSummary extends DBAction {

	@Override
	public void execute() throws SQLException {
		//System.out.println("saving summary");
		if(snapshottimestamp!=-1)
		{
			//statement = connection.createStatement();
			pStatement=connection.prepareStatement("insert into SUMMARY_SNAPSHOT (SNAPSHOTTIMESTAMP, LAYER, SERVICE, MEAN, STDDEV, MEDIAN, MODE, SAMPLECOUNT) " +
					"values (?,?,?,?,?,?,?,?)");
			for(Service s:services)
			{
				
				pStatement.setLong(1, snapshottimestamp);
				String layer=s.getType();
				pStatement.setString(2, layer);
				String service = s.getServiceName()
						+ "..." + s.getOperationName();
				//System.out.println("Layer...Service="+layer+"..."+service);
				pStatement.setString(3, service);
				double avg = s.getAvgtime();
				if(avg != avg ) avg=0d;
				//System.out.println("   avg="+avg);
				pStatement.setDouble(4, avg);
				double stddev = s.getOperationTimeStdDev();
				if (stddev != stddev)
					stddev = 0d;
				//System.out.println("   stddev="+stddev);
				pStatement.setDouble(5, stddev);
				double median = s.getOperationTimeLogMedian();
				if (median != median)
					median = 0d; // check for NaN
				//System.out.println("   median="+median);
				pStatement.setDouble(6, median);
				double mode = s.getOperationTimeLogMode();
				if (mode != mode)
					mode = 0d;
				//System.out.println("   mode="+mode);
				pStatement.setDouble(7, mode);
				int c = s.getCount();
				//System.out.println("   count="+c);
				pStatement.setInt(8, c);
				pStatement.executeUpdate();
			}

			
		}
	}
	
	private PreparedStatement pStatement=null;
	private Statement statement=null;
	
/*	public void postCommit()
	{
		super.postCommit();
		try {
			pStatement.close();
		} catch (SQLException e) {
			
		}
		pStatement=null;
		
	}
*/

	long snapshottimestamp=-1;
	List<Service> services=null;
	
	public void setSnapshot(Date date, List<Service> SO)
	{
		snapshottimestamp=date.getTime();
		services=SO;
		
	}
	
	
	
}
