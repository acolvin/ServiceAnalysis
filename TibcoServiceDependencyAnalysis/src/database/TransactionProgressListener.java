package database;

public interface TransactionProgressListener {
	public void progressEvent(int progressPercent);
}
