package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author andrew
 *
 */
public final class DBTransaction extends Thread implements DBProgressListener{
	Connection connection;
	private boolean executing=false;
	private ArrayList<DBAction> ActionList=new ArrayList<DBAction>();
	
	public synchronized void addDBAction(DBAction action)
	{
		if(!executing) ActionList.add(action);
	}
	
	//run this for synchronous behaviour
	private int maxEventCount,currentCount;
	public synchronized void execute()  throws SQLException 
	{
		maxEventCount=ActionList.size();
		currentCount=0;
		for(DBAction dba:ActionList)
		{
			dba.addProgressListener(this);
			maxEventCount+=dba.getProgressCount();
		}
		try{
		connection = DriverManager.getConnection(
			serviceAnalysisModel.SAControl.Controller.DBConnectionStr,
			serviceAnalysisModel.SAControl.Controller.DBUser,
			serviceAnalysisModel.SAControl.Controller.DBPasswd);
		connection.setAutoCommit(false);
		} catch (SQLException se)
		{
			DBController.DBC.start();
			if( DBController.isRunning())
			{
				connection = DriverManager.getConnection(
					serviceAnalysisModel.SAControl.Controller.DBConnectionStr,
					serviceAnalysisModel.SAControl.Controller.DBUser,
					serviceAnalysisModel.SAControl.Controller.DBPasswd);
				connection.setAutoCommit(false);
			}else throw se;
		}
		
		try {
			for(DBAction dba:ActionList)
			{
				dba.initialise(connection);
				dba.execute();
				
				//currentCount++;
				//fireTransactionProgressEvents();
				//System.out.println("Progress:"+(currentCount*100/maxEventCount));
			}
			for(DBAction dba:ActionList)
			{				
				dba.preCommit();
			}
			connection.commit();
			for(DBAction dba:ActionList)
			{				
				dba.postCommit();
				fireProgressEvent();
			}
			connection.close();
		}
		catch( SQLException e)
		{
			connection.rollback();
			for(DBAction dba:ActionList)
			{				
				dba.rollback();
			}
			connection.close();
			throw(e);
		}
	}
	
	// this is to allow async behaviour
	public void run()
	{
		try {
			execute();
		} catch (SQLException e) {
			System.err.println("background DB Action failed");
			e.printStackTrace();
		}
	}

	@Override
	public void fireProgressEvent() {
		currentCount++;
		//System.out.println("Progress:"+(currentCount*100/maxEventCount));
		fireTransactionProgressEvents();
	}
	
	private void fireTransactionProgressEvents()
	{
		int p=currentCount*100/maxEventCount;
		//System.out.println("Progress:"+currentCount+" , "+maxEventCount);
		for(TransactionProgressListener l: listeners)
		{
			l.progressEvent(p);
		}
	}
	
	public void addListener(TransactionProgressListener l)
	{
		listeners.add( l);
	}
	private ArrayList<TransactionProgressListener> listeners=new ArrayList<TransactionProgressListener>();
}
