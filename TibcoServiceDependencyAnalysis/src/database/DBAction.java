package database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Abstract class to inherit from to runn standard sql commands to implement a 
 * specific task, for example save snapshot as a single transaction. if you override 
 * non abstract functions remember to call the super version.
 * Do not commit or rollback 
 * 
 * @author andrew
 *
 */
public abstract class DBAction {
	
	final void initialise(Connection connection)
	{
		this.connection=connection;
	}
	
	//execute must trigger progress events if you want more than  1 
	// if getProgressCount <2 then it will be handled through the call completing
	public abstract void execute() throws SQLException;
	
	private ArrayList<DBProgressListener> listeners=new ArrayList<DBProgressListener>();
	
	protected void addProgressListener(DBProgressListener listener)
	{
		listeners.add(listener);
	}
	
	protected void fireProgressEvents()
	{
		for(DBProgressListener d:listeners) d.fireProgressEvent();
	}
	
	protected void preCommit(){}	
	protected void postCommit(){ connection=null;}
	protected void rollback(){connection = null;}
	//override this if you want a finer progress	
	protected int getProgressCount(){return 0;} 
	protected Connection connection=null;
}
