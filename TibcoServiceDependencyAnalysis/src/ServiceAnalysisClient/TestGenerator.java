package ServiceAnalysisClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.SimpleTimeZone;

/**
 * This is a test program to generate BW Stats Files. Options allow the Service,
 * Operation, Mean, Std Dev, the file to write out the the events to, whether to
 * produce a normal or log normal distribution and the delay between the events.
 * 
 * @author apc
 * 
 */
public class TestGenerator
{

	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException
	{
		// TODO Auto-generated method stub

		for (String s : args)
		{
			arg = false;
			if (s.equals("--mean"))
			{
				m = true;
				arg = true;
			} else if (m)
			{
				mean = new Double(s);
				m = false;
				arg = true;
			}
			if (s.equals("--stddev"))
			{
				std = true;
				arg = true;
			} else if (std)
			{
				stdDev = new Double(s);
				std = false;
				arg = true;
			}
			if (s.equals("--service"))
			{
				ser = true;
				arg = true;
			} else if (ser)
			{
				service = s;
				ser = false;
				arg = true;
			}
			if (s.equals("--operation"))
			{
				op = true;
				arg = true;
			} else if (op)
			{
				operation = s;
				op = false;
				arg = true;
			}
			if (s.equals("--file"))
			{
				fn = true;
				arg = true;
			} else if (fn)
			{
				filen = s;
				fn = false;
				arg = true;
			}
			if (s.equals("--normal"))
			{
				logn = false;
				arg = true;
			}
			if (s.equals("--portal"))
			{
				portalFormat = true;
				arg = true;
			}
			if (s.equals("--delay"))
			{
				del = true;
				arg = true;
			} else if (del)
			{
				delay = new Double(s);
				del = false;
				arg = true;
			}
			if (!arg)
			{// just a plan argument of form service!operation!mean!stddev
				String[] serstring = s.split("\\+");
				// System.out.println(serstring[0]+"-"+serstring[1]+"-");
				deparray.add(serstring);
			}
		}
		generateModel();
		if (!portalFormat)
			generateBW();
		else
			generatePortal();

	}

	private static void generateModel()
	{

		SAClient sac = SAClient.getClient();
		sac.setOutputFile(new File(filen));
		/*
		 * int txn1,txn21,txn22,txn311,txn312;
		 * txn1=sac.startTransaction("component1", "function1", "01");
		 * txn21=sac.startTransaction(txn1, "component2", "function2", "02");
		 * txn311=sac.startTransaction(txn21, "component3", "function3", "03");
		 * sac.endTransaction(txn311); txn312=sac.startTransaction(txn21,
		 * "component4", "function4", "03"); sac.endTransaction(txn312);
		 * sac.endTransaction(txn21); txn22=sac.startTransaction(txn1,
		 * "component5", "function5", "02"); sac.endTransaction(txn22);
		 * sac.endTransaction(txn1);
		 */
		String txn1, txn2;
		String layer;
		if (portalFormat)
			layer = "UI";
		else
			layer = "BW";
		txn1 = sac.startTransaction(service, operation, layer);
		if (deparray.size() > 0)
			for (String[] serstring : deparray)
			{

				String dser = serstring[0].trim();
				String dop = serstring[1].trim();
				txn2 = sac.startTransaction(txn1, dser, dop, "BW");
				sac.endTransaction(txn2);

			}
		sac.endTransaction(txn1);

	}

	private static void generatePortal() throws InterruptedException
	{
		String server = "aeportal" + (int) (Math.random() * 99);
		server += "_srv_" + (int) (Math.random() * 99);
		f = new File(filen);
		if (!f.exists())
			try
			{
				f.createNewFile();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
		SimpleDateFormat df = new SimpleDateFormat("dd-mmm-yyyy HH:mm:ss:SSS");
		SimpleTimeZone tz = new SimpleTimeZone(0, "GMT");
		df.setTimeZone(tz);
		try
		{
			int request = 0;
			@SuppressWarnings("resource")
			FileOutputStream fos = new FileOutputStream(f, true);
			while (true)
			{
				Date d = new Date();
				String ds = df.format(d);
				int waitTime = 0;

				// process request start
				String rs = "####<"
						+ ds
						+ " o'clock GMT> <Info> <AMS_PERF> <server> <"
						+ server
						+ "> <[ACTIVE] ExecuteThread: '61' for queue: 'weblogic.kernel.Default (self-tuning)'> <USER_A> <> <> <"
						+ d.getTime() + "> <BEA-000000> ";
				String ss1 = rs;
				if (operation.length() > 0)
					rs += "<Request #" + request + ", USER_A, request start: "
							+ "/AMSPortalWebProject/com/ams2/systemtools/"
							+ service + "/" + operation + ".do> \n";
				else
					rs += "<Request #" + request + ", USER_A, request start: "
							+ "/AMSPortalWebProject/com/ams2/systemtools/"
							+ service + ".do> \n";
				fos.write(rs.getBytes());
				fos.flush();

				int reqTime = getSample(!logn, mean, stdDev);

				// process services
				String ss = ss1 + "<Request #" + request
						+ ", USER_A, service start: ";
				String se = ss1 + "<Request #" + request
						+ ", USER_A, service end: ";

				if (deparray.size() > 0)
				{
					for (String[] serstring : deparray)
					{
						String dser = serstring[0].trim();
						String dop = serstring[1].trim();
						int depmean = new Integer(serstring[2]);
						int depstddev = new Integer(serstring[3]);
						int dersample = getSample(!logn, depmean, depstddev);
						waitTime += dersample;
						String deps = ss + dser + "." + dop + "> \n";
						fos.write(deps.getBytes());
						deps = se + dser + "." + dop + ", duration(ms): "
								+ dersample + "> \n";
						fos.write(deps.getBytes());
						fos.flush();
					}
				}

				// process request end
				int totalTime = reqTime + waitTime;
				String re = "####<"
						+ ds
						+ " o'clock GMT> <Info> <AMS_PERF> <server> <aeportal1_svr_1> <[ACTIVE] ExecuteThread: '61' for queue: 'weblogic.kernel.Default (self-tuning)'> <USER_A> <> <> <"
						+ d.getTime() + "> <BEA-000000> ";
				if (operation.length() > 0)
					re += "<Request #" + request + ", USER_A, request end: "
							+ "/AMSPortalWebProject/com/ams2/systemtools/"
							+ service + "/" + operation + ".do, duration(ms): "
							+ totalTime + "> \n";
				else
					re += "<Request #" + request + ", USER_A, request end: "
							+ "/AMSPortalWebProject/com/ams2/systemtools/"
							+ service + ".do, duration(ms): " + totalTime
							+ "> \n";
				fos.write(re.getBytes());
				fos.flush();
				request = request + 1;
				Thread.sleep((int) (Math.random() * delay));
			}

		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void generateBW() throws InterruptedException
	{
		SAClient sac = SAClient.getClient();
		sac.setOutputFile(new File(filen));

		System.out.println("generateBW");
		while (true)
		{
			String txn = sac.startTiming(service, operation, "BW");
			// Date d=new Date();
			// String ds=df.format(d);
			// int wait=0;
			if (deparray.size() > 0)
			{
				String subtxn;
				for (String[] serstring : deparray)
				{

					String dser = serstring[0].trim();
					String dop = serstring[1].trim();
					int depmean = new Integer(serstring[2]);
					int depstddev = new Integer(serstring[3]);
					subtxn = sac.startTiming(txn, dser, dop, "BW");
					int dersample = getSample(!logn, depmean, depstddev);
					sac.addWait(subtxn, 3 * dersample / 4);
					// wait+=dersample;
					Thread.sleep(dersample);
					int i = sac.endTiming(subtxn);
					sac.addWait(txn, i);

				}
			}

			int i = getSample(!logn, mean, stdDev);
			Thread.sleep(i);
			sac.endTiming(txn);
			Thread.sleep((int) (Math.random() * delay));

		}

	}

	private static File f;
	// =new File("BWtest.csv");
	private static boolean m = false, std = false, ser = false, op = false,
			fn = false, logn = true, del = false, arg = false;
	private static boolean portalFormat = false;

	private static String service = "SupportDoc_v1.0";
	private static String operation = "FindSupportDocs";
	private static String filen = "BWtest.csv";

	private static double delay = 1000d;
	private static double mean = 125, stdDev = 80;
	private static ArrayList<String[]> deparray = new ArrayList<String[]>();

	public static int getSample(boolean normal, double mean, double stdDev)
	{
		double mean2 = mean * mean;
		double variance = stdDev * stdDev;
		double mu = Math.log(mean2 / Math.sqrt(variance + mean2));
		double sigma = Math.sqrt(Math.log1p(variance / (mean2)));
		int i = 0;
		if (!normal)
		{
			double dd = TestGenerator.box_muller(0.0, 1.0);
			i = (int) Math.exp(mu + sigma * dd);// (Math.random()*3000d);
		} else
		{
			i = (int) TestGenerator.box_muller(mean, stdDev);
		}
		return i;
	}

	public static double box_muller(double m, double s) /*
														 * normal random variate
														 * generator
														 */
	{ /* mean m, standard deviation s */
		double x1, x2, w, y1;
		double y2 = 0;
		;
		boolean use_last = false;

		if (use_last) /* use value from previous call */
		{
			y1 = y2;
			use_last = true;
		} else
		{
			do
			{
				x1 = 2.0 * Math.random() - 1.0;
				x2 = 2.0 * Math.random() - 1.0;
				w = x1 * x1 + x2 * x2;
			} while (w >= 1.0);

			w = Math.sqrt((-2.0 * Math.log(w)) / w);
			y1 = x1 * w;
			y2 = x2 * w;
			use_last = true;
		}

		return (m + y1 * s);
	}

}
