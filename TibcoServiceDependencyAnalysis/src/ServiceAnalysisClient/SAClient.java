package ServiceAnalysisClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.dgc.VMID;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

public class SAClient
{
	private SAClient()
	{
		String vmide = System.getenv().get("SACLIENT_INSTANCE");
		if (vmide != null)
			vmid = vmide;
		vmid = System.getProperty("SAClient.instance", vmid);

	}

	public void setVMid(String s)
	{
		vmid = s;
	}

	private String vmid = new VMID().toString();

	private static SAClient instance = null;

	public static SAClient getClient()
	{
		if (instance == null)
			instance = new SAClient();
		return instance;

	}

	/**
	 * Starts a monitor transaction which allows structure but without timing
	 * 
	 * @param component
	 *            The component that generates the transaction
	 * @param function
	 *            The function that generates the transaction
	 * @param layer
	 *            A 2 letter abbreviation for the layer
	 * @return the transaction number
	 */
	public String startTransaction(String component, String function,
			String layer)
	{
		String txn = nextTransaction();
		SAEvent sae = new SAEvent(txn, layer, component, function);
		Stransactions.put(txn, sae);
		return txn;
	}

	/**
	 * completes a transaction - after this is sent no other entries should be
	 * submitted against the transaction. This will end all subtransactions
	 * 
	 * @param transactionNumber
	 */
	public void endTransaction(String transactionNumber)
	{
		if (Stransactions.containsKey(transactionNumber))
		{
			// remove from map and call print
			SAEvent sae = Stransactions.get(transactionNumber);
			Stransactions.remove(transactionNumber);
			sae.writeLog(fos);
		}
	}

	/**
	 * starts a subtransaction
	 * 
	 * @param transactionNumber
	 * @param component
	 * @param function
	 * @param layer
	 * @return the transaction number
	 */
	public String startTransaction(String transactionNumber, String component,
			String function, String layer)
	{
		String txn = transactionNumber + "--" + nextTransaction();
		if (Stransactions.containsKey(transactionNumber))
		{
			SAEvent sae = new SAEvent(txn, layer, component, function);
			Stransactions.put(txn, sae);
			SAEvent parent = Stransactions.get(transactionNumber);
			parent.addChild(sae);
		} else
		{
			SAEvent sae = new SAEvent(txn, layer, component, function);
			Stransactions.put(txn, sae);
		}

		return txn;
	}

	/*
	 * public String startTransaction(String transaction, String
	 * component,String function,String layer) { String txn=null;
	 * if(transaction.equalsIgnoreCase("")) txn=""+nextTransaction();
	 * if(Stransactions.containsKey(transaction)) {//child transaction
	 * txn=transaction+"--"+nextTransaction(); SAEvent sae=new
	 * SAEvent(txn,layer,component,function); Stransactions.put(txn, sae);
	 * SAEvent parent=Stransactions.get(transaction); parent.addChild(sae);
	 * 
	 * } else {//new transaction SAEvent sae=new
	 * SAEvent(txn,layer,component,function); Stransactions.put(txn, sae); }
	 * 
	 * 
	 * 
	 * return txn;
	 * 
	 * }
	 */

	/**
	 * Adds wait time to a transaction
	 * 
	 * @param transactionNumber
	 * @param wait
	 *            in millisecs.
	 */
	public void addWait(String transactionNumber, int wait)
	{
		if (Stransactions.containsKey(transactionNumber))
		{
			Stransactions.get(transactionNumber).addWait(wait);
		}
	}

	/**
	 * creates a timing transaction
	 * 
	 * @param component
	 * @param function
	 * @param layer
	 * @return
	 */
	public String startTiming(String component, String function, String layer)
	{

		String txn = nextTransaction();
		// System.out.println("Start Timing Called: "+txn);
		Date d = new Date();
		startTimes.put(txn, d.getTime());
		SAEvent sae = new SAEvent(txn, layer, component, function);
		Stransactions.put(txn, sae);
		return txn;
	}

	public String startTiming(String transactionNumber)
	{
		if (Stransactions.containsKey(transactionNumber))
		{
			// System.out.println("Start Timing Called: "+transactionNumber);
			Date d = new Date();
			startTimes.put(transactionNumber, d.getTime());
			return transactionNumber;
		}
		return "";
	}

	public String startTiming(String transactionNumber, String component,
			String function, String layer)
	{
		String txn;
		if (Stransactions.containsKey(transactionNumber))
		{
			if (startTimes.containsKey(transactionNumber))
			{
				txn = startTransaction(transactionNumber, component, function,
						layer);
				startTiming(txn);
			} else
			{
				SAEvent s = Stransactions.get(transactionNumber);
				if (s.component.equals(component)
						&& s.function.equals(function) && s.layer.equals(layer))
				{
					txn = transactionNumber;
					startTiming(txn);
				} else
				{
					txn = startTransaction(transactionNumber, component,
							function, layer);
					startTiming(txn);
				}
			}
		} else
		{
			txn = startTransaction(transactionNumber, component, function,
					layer);
			startTiming(txn);
		}

		return txn;
	}

	/**
	 * ends a timing transaction and returns the millisecs timing.
	 * 
	 * @param transactionNumber
	 * @return -1 transaction not known else millisecs from start to end
	 */
	public int endTiming(String transactionNumber)
	{
		// System.out.println("end Timing Called: "+transactionNumber);
		Date d = new Date();
		if (startTimes.containsKey(transactionNumber))
		{
			// System.out.println("found txn");
			long l = d.getTime();
			l = l - startTimes.get(transactionNumber);
			Date s = new Date(startTimes.get(transactionNumber));
			startTimes.remove(transactionNumber);
			int time = (new Long(l)).intValue();
			SAEvent sae = Stransactions.get(transactionNumber);
			sae.setTime(s, time);
			Stransactions.remove(transactionNumber);
			// System.out.println("writing");
			writeLog(sae);
			return time;
		}
		return -1;
	}

	private synchronized String nextTransaction()
	{
		return vmid + "::" + ++transaction;
	}

	public synchronized boolean setOutputFile(File filename)
	{
		if (filename == null)
		{
			if (fos != null)
				try
				{
					fos.close();
					output = null;
					return true;
				} catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					output = null;
					return false;
				}
			else
			{
				output = null;
				return true;
			}
		} else if (output != null)
		{

			try
			{
				fos.close();
				fos = null;
				output = null;
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				fos = null;
				output = null;
				return false;
			}

		}

		if (filename.exists())
		{
			// System.out.println("isFile="+filename.isFile());
			if (filename.isDirectory())
			{
				output = null;
				return false;
			} else
				output = filename;
		} else
			try
			{
				if (filename.createNewFile())
					output = filename;
				// else return false;
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				output = null;
				fos = null;
				return false;
			}
		try
		{
			fos = new FileOutputStream(output, true);
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			output = null;
			fos = null;
			return false;
		}

		return true;
	}

	private synchronized void writeLog(SAEvent sae)
	{
		if (fos == null)
			return;
		sae.writeLog(fos);

	}

	private ConcurrentHashMap<String, Long> startTimes = new ConcurrentHashMap<String, Long>();
	// private ConcurrentHashMap<Integer,SAEvent> transactions=new
	// ConcurrentHashMap<Integer,SAEvent>();
	// private ConcurrentHashMap<Integer,Long> startTimes=new
	// ConcurrentHashMap<Integer,Long>();
	private ConcurrentHashMap<String, SAEvent> Stransactions = new ConcurrentHashMap<String, SAEvent>();

	private FileOutputStream fos = null;
	private File output = null;
	private int transaction = 0;
	// private ConcurrentLinkedQueue ProcessQueue=new ConcurrentLinkedQueue();
}
