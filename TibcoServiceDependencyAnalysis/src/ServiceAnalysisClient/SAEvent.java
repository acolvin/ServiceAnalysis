package ServiceAnalysisClient;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.concurrent.ConcurrentHashMap;

public class SAEvent implements Comparable
{
	public String txn;
	public String layer;
	public String function;
	public String component;
	public Date startTime = null;
	public int wait = 0;
	public int processTime = -1;

	public SAEvent(String txn, String layer, String component, String function)
	{
		this.txn = txn;
		this.layer = layer;
		this.component = component;
		this.function = function;
		SimpleTimeZone tz = new SimpleTimeZone(0, "GMT");
		df.setTimeZone(tz);
	}

	public SAEvent(String txn, String layer, String component, String function,
			Date startTime, int duration)
	{
		this(txn, layer, component, function);
		this.setTime(startTime, duration);
	}

	public void addWait(int wait)
	{
		this.wait += wait;
	}

	public void setTime(Date d, int time)
	{
		startTime = d;
		processTime = time;
	}

	public void setTime(int time)
	{
		processTime = time;
	}

	public void writeLog(FileOutputStream fos)
	{
		if (fos == null)
			return;
		if (parent != null && !parent.TxnComplete) // just print this node
		{
			printModelElement(fos);
			// TxnComplete=true;
		} else if (parent != null && parent.TxnComplete)// we can print this as
														// the parent has been
														// printed
		{
			TxnComplete = true;
			// printModelElement(fos);
			printModelChildElement(fos);
			for (String saeTxn : children.keySet())
			{
				// process the children
				SAEvent sae = children.get(saeTxn);
				sae.writeLog(fos);

			}

		} else if (parent == null && startTime == null)// there is no parent so
														// print it if it isnt a
														// time event
		{
			printModelElement(fos);
			TxnComplete = true;
			for (String saeTxn : children.keySet())
			{
				// process the children
				SAEvent sae = children.get(saeTxn);
				sae.writeLog(fos);

			}
		}

		if (startTime != null)// time events come out as soon as they are
								// complete
		{
			String s = "MODELEVENT," + layer + "," + component + "," + function
					+ ",";
			s += df.format(startTime);
			s += "," + processTime + "," + wait + "," + txn + '\n';
			try
			{
				// System.out.println(s);
				fos.write(s.getBytes());
				fos.flush();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void printModelElement(FileOutputStream fos)
	{// MODEL,BW,PROCESS1,OP1,,,,,,
		String s = "MODEL," + layer + "," + component + "," + function
				+ ",,,,,," + '\n';
		try
		{
			// System.out.println(s);
			fos.write(s.getBytes());
			fos.flush();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void printModelChildElement(FileOutputStream fos)
	{
		String s = "MODEL," + parent.layer + "," + parent.component + ","
				+ parent.function + "," + layer + "," + component + ","
				+ function + ",,," + '\n';
		try
		{
			// System.out.println(s);
			fos.write(s.getBytes());
			fos.flush();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addChild(SAEvent child)
	{
		child.setParent(this);
		children.put(child.txn, child);

	}

	public void setParent(SAEvent parent)
	{
		this.parent = parent;
		parentTransaction = parent.txn;
	}

	public static String getRootTxn(String txn)
	{

		if (txn != null)
		{
			String s[] = txn.split("--");
			return s[0];

		} else
			return null;
	}

	public static String getParentTxn(String txn)
	{
		if (txn != null)
		{
			// System.out.println("in getparentTXN with not null " + txn);
			String transaction = "";
			String s[] = txn.split("--");
			// System.out.println(s[0]);
			if (s.length == 1)
				return null;
			// System.out.println("length is greater than one so there is a parent");
			for (int i = 0; i < s.length - 1; i++)
				if (i == 0)
					transaction = s[0];
				else
					transaction += "--" + s[i];
			// System.out.println("calculated ptransaction is "+transaction);
			return transaction;
		} else
			return null;
	}

	public String toString()
	{
		String ptxn = (parent == null) ? null : parent.txn;
		return txn + "/" + layer + "/" + component + "/" + function + "/"
				+ startTime + "/" + processTime + "/" + wait + "/" + ptxn + "/"
				+ children.size();
	}

	private SimpleDateFormat df = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss:SSS");
	// private ConcurrentHashMap<Integer,SAEvent> children=new
	// ConcurrentHashMap<Integer,SAEvent>();
	public ConcurrentHashMap<String, SAEvent> children = new ConcurrentHashMap<String, SAEvent>();

	private SAEvent parent = null;
	private String parentTransaction = "";
	private boolean TxnComplete = false;
	public String location="";
	public String file="";

	@Override
	public int compareTo(Object ob)
	{
		// TODO Auto-generated method stub
		if (ob == null)
			return -1;
		SAEvent o = (SAEvent) ob;
		if (startTime == null & o.startTime == null)
			return 0;
		if (startTime == null)
			return -1;
		if (o.startTime == null)
			return -1;
		if (startTime.getTime() < o.startTime.getTime())
			return -1;
		if (startTime.getTime() == o.startTime.getTime())
			return 0;
		return 1;
	}
	
	public boolean equivelent(SAEvent sae)
	{
		return this.txn.equals(sae.txn);
		
	}

}
