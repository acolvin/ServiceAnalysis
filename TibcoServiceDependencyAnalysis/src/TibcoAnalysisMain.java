import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.SplashScreen;
import java.io.File;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.swing.SwingUtilities;

import org.jfree.ui.RefineryUtilities;

import database.DBController;

import serviceAnalysisModel.ModelFileReader;
import serviceAnalysisModel.Service;
import serviceAnalysisModel.TFileReader;
import serviceAnalysisModel.VolumetricModel;
import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisModel.SAControl.OutputStreamTee;
import serviceAnalysisModel.SAControl.StdOutHandler;
import serviceAnalysisModel.readersWriters.DBWriter;
import serviceAnalysisUI.AverageTimeGraph;
import serviceAnalysisUI.MainForm;
import serviceAnalysisUI.ModelForm;
import serviceAnalysisUI.ServiceDependencyView;
import serviceAnalysisUI.TotalTimeGraph;

/**
 * The main entry point of the Service Analysis application
 * 
 * @author apc
 * 
 */
public class TibcoAnalysisMain
{

	/**
	 * The main entry point.
	 * 
	 * 
	 * @param args
	 *            The command line arguments
	 *            <P>
	 *            --DBEvents write each event to the embedded database (time
	 *            consuming)
	 *            <P>
	 *            --webFormat=
	 *            <P>
	 *            <li>t is time of action</li>
	 *            <li>T is response time</li>
	 *            <li>U is uri</li>
	 *            <li>c separation char (optional)</li>
	 *            <li>space is assumed as separation character</li>
	 *            <li>z must be last char</li>
	 *            <li>example format string t1T4U6z</li> --time (file) loads a
	 *            service time file
	 *            <P>
	 *            --adj (file) reads a service adjustment file
	 *            <P>
	 *            --job (file) reads a BW job file
	 *            <P>
	 *            --uiJob (file) reads a portal file
	 *            <P>
	 *            --mod (file) loads a model file
	 *            <P>
	 *            ServiceFile file that contains the initial list of services
	 *            and dependencies (mandatory)
	 * 
	 * 
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException
	{
		// TODO Auto-generated method stub

		
		
		String depFile = "", timeFile = "", adjFile = "", jobFile = "", uiJobFile = "", modFile = "";
		boolean timeFileExists = false, adjFileExists = false, jobFileExists = false, uiJobFileExists = false, modFileExists = false;
		
		//System.err.println("Main: orig printstream "+System.out);
		OutputStreamTee ost=new OutputStreamTee(new StdOutHandler(),System.out);
		//catch system out
		PrintStream ps=new PrintStream(ost);
		
		//System.err.println("Main: Tee="+ost);
		//System.err.println("Main: Tee PS="+ps);
		System.setOut(ps);
		
		for (String s : args)
		{
			if(s.equals("--noauto"))
			{
				Controller.runAuto=false;
			}
			else if (s.equals("--"))
			{
				Controller.stdin = true;
			} else if (s.equals("--headless"))
			{
				gui = false;
				System.out.println("starting headless");
			} else if (s.equals("--DBEvents"))
			{
				DBWriter.DBWriteServiceEvents = true;
			}

			else if (s.equals("--time"))
			{
				timeFileExists = true;
			} else if (timeFileExists)
			{
				timeFile = s;
				timeFileExists = false;
			} else
			{
				if (s.equals("--adj"))
				{
					adjFileExists = true;
				} else if (adjFileExists)
				{
					adjFile = s;
					adjFileExists = false;
				} else
				{
					if (s.equals("--job"))
					{
						jobFileExists = true;
					} else if (jobFileExists)
					{
						jobFile = s;
						jobFileExists = false;
					} else
					{
						if (s.equals("--uiJob"))
						{
							uiJobFileExists = true;
						} else if (uiJobFileExists)
						{
							uiJobFile = s;
							uiJobFileExists = false;
						} else
						{
							if (s.equals("--mod"))
							{
								modFileExists = true;
							} else if (modFileExists)
							{
								modFile = s;
								modFileExists = false;
							} else
							{
								depFile = s;
							}
						}
					}
				}
			}
		}
		SplashScreen splash=null;
		if (gui)  splash = SplashScreen.getSplashScreen();
		
		TFileReader t = new TFileReader();
		List<Service> serviceList = null;
		_VM = new VolumetricModel();
		
		boolean hyperExists=false;
		boolean derbyExists=false;

		File dirHyper = new File(".Database.SA");
		//if (!dirHyper.exists())
	//		dir.mkdir();
		if (dirHyper.isDirectory()) hyperExists=true;
	//	{
	//		System.out.println("oh dear directory exists as a file");

	//	}
		Graphics2D splashGraphics=null;
		if(splash!=null){
			splashGraphics=splash.createGraphics();
			splashGraphics.setBackground(Color.YELLOW);
			splashGraphics.setColor(Color.BLACK);
			Dimension d=splash.getSize();
			
			Graphics2D g2=splash.createGraphics();
			g2.setColor(Color.black);
			  g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		         RenderingHints.VALUE_ANTIALIAS_ON);
		      Font font = new Font("San Serif", Font.PLAIN, 18);
		      g2.setFont(font);
		      int len=SwingUtilities.computeStringWidth( g2.getFontMetrics(), serviceAnalysisUI.StatusBarText.getVersion());
		      g2.drawString(serviceAnalysisUI.StatusBarText.getVersion(), (d.width-len)/2, 80);
			
		}
		if(splashGraphics!=null){
			updateSplash(splash,splashGraphics, "Starting Database" );
			
			
		}
		//System.out.println("splash="+splash);
		//System.out.println("splashGraphics="+splashGraphics);
		File dirDerby = new File("SAdb/derbyDB");
		if(dirDerby.isDirectory()) derbyExists=true;
		dbcontroller=new DBController();
		dbcontroller.start();
		if(splashGraphics!=null){
			updateSplash(splash,splashGraphics, "Database Started" );
			
		}
		Connection con = null,con2=null;
		try
		{
			
			con = DriverManager.getConnection(
					Controller.DBConnectionStr+";create=true",
					//serviceAnalysisUI.MainForm.DBConnectionStr,
					Controller.DBUser,
					Controller.DBPasswd);
			if(!derbyExists && hyperExists)
			con2= DriverManager.getConnection(
					Controller.DBConnectionStrHSQL,
					//serviceAnalysisUI.MainForm.DBConnectionStr,
					Controller.DBUser,"");
					//Controller.DBPasswd);
			
		} catch (SQLException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();

		}
		boolean migrate=false;
		if (con2 != null)
			try
			{
				// test if db structure there
				PreparedStatement pst = con2
						.prepareStatement("select * from SA");
				pst.clearParameters();
				ResultSet rs = pst.executeQuery();
				rs.next();
				System.out.println("Hypersonic tables exist: version:"
						+ rs.getInt("VERSION"));
				int version = rs.getInt("VERSION");
				if (version == 4)
				{
					version = 5;
					if(splashGraphics!=null){
						updateSplash(splash,splashGraphics, "Migrating to Version 5" );
						
					}
					System.out.println("version 4 to 5 upgrade");
					con2.createStatement().executeUpdate("truncate table SA");
					con2.createStatement().executeUpdate(
							"insert into SA  (VERSION) values (5)");
					con2.createStatement()
							.executeUpdate(
									"ALTER TABLE SERVICE_SNAPSHOT ADD COLUMN WAIT DOUBLE DEFAULT 0");
					con2.commit();
					System.out.println("Tables upgraded to version 5");
					
				}
				if(version==5)
				{
					//version = 6;
					//System.out.println("version 5 for migration");
					//con2.createStatement().executeUpdate("truncate table SA");
					//con2.createStatement().executeUpdate(
					//		"insert into SA  (VERSION) values (0)");
					//con2.commit();
					migrate=true;
					//System.out.println("Tables upgraded to version 5");
				}
				//migration to derby from this point do not add migration code here
				
			} catch (java.sql.SQLSyntaxErrorException e2)
			{
				migrate=false;
			}
		
			if(con!=null)
			{
				try
				{
					// test if db structure there
					PreparedStatement pst = con
							.prepareStatement("select * from SA");
					pst.clearParameters();
					ResultSet rs = pst.executeQuery();
					rs.next();
					System.out.println("derby tables exist: version:"
							+ rs.getInt("VERSION"));
					int version = rs.getInt("VERSION");
					try //add new migration code in here
					{
						if(version==6) 
						{
							if(splashGraphics!=null){
								updateSplash(splash,splashGraphics,  "Migrate to version 7");
								
							}
							version=migratetoseven(con);
							//version=7;
							System.out.println("Migrated database to version "+version);
						}
						if(version==7) 
						{
							if(splashGraphics!=null){
								updateSplash(splash,splashGraphics, "Migrate to version 8" );
								
							}
							version=migratetoeight(con);
							//version=8;
							System.out.println("Migrated database to version "+version);
						}
						if(version==8)
						{
							if(splashGraphics!=null){
								updateSplash(splash,splashGraphics, "Migrate to version 9" );
								
							}
							version=migratetonine(con);
							System.out.println("Migrated database to version "+version);
							
						}
						if(version==9)
						{
							if(splashGraphics!=null){
								updateSplash(splash,splashGraphics, "Migrate to version 10" );
								
							}
							version=migratetoten(con);
							System.out.println("Migrated database to version "+version);
							
						}
						if(version==10)
						{
							if(splashGraphics!=null){
								updateSplash(splash,splashGraphics, "Migrate to version 11" );
								
							}
							version=migratetoeleven(con);
							System.out.println("Migrated database to version "+version);
							
						}
						if(version==11)
						{
							if(splashGraphics!=null){
								updateSplash(splash,splashGraphics, "Migrate to version 12" );
								
							}
							version=migratetotwelve(con);
							System.out.println("Migrated database to version "+version);
							
						}
						if(version==12)
						{
							if(splashGraphics!=null){
								updateSplash(splash,splashGraphics, "Migrate to version 13" );
								
							}
							version=migratetothirteen(con);
							System.out.println("Migrated database to version "+version);
							
						}
						if(version==13)//version 13 never made it to the outside world
						{
							if(splashGraphics!=null){
								updateSplash(splash,splashGraphics, "Migrate to version 14" );
								
							}
							version=migratetofourteen(con);
							System.out.println("Migrated database to version "+version);
							
						}
					}catch (java.sql.SQLSyntaxErrorException e2)
					{
						System.out.println("unable to migrate from version "+version);
						e2.printStackTrace();
					}
					
				}catch (java.sql.SQLSyntaxErrorException e2)
				{
					con = DriverManager.getConnection(
							Controller.DBConnectionStr+";create=true",
							//serviceAnalysisUI.MainForm.DBConnectionStr,
							Controller.DBUser,
							Controller.DBPasswd);
					if(splashGraphics!=null){
						updateSplash(splash,splashGraphics, "Creating Database Structure" );
						
						
					}
					con.createStatement().executeUpdate(
							"create table SA (VERSION INTEGER NOT NULL)");
					con.createStatement()
							.executeUpdate(
									"create table SERVICE_EVENT (ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SERVICE VARCHAR(100) NOT NULL, EVENT_TIME BIGINT, RESPONSE DOUBLE)");
					con.createStatement()
							.executeUpdate(
									"CREATE INDEX operation on SERVICE_EVENT(SERVICE,EVENT_TIME) ");
					con.createStatement().executeUpdate(
							"CREATE INDEX events on SERVICE_EVENT(EVENT_TIME) ");
					con.createStatement()
							.executeUpdate(
									"create table SERVICE_SNAPSHOT (ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, LAYER VARCHAR(6) NOT NULL DEFAULT 'BW', SERVICE VARCHAR(100) NOT NULL, RESPONSE DOUBLE, QTY DOUBLE, WAIT DOUBLE DEFAULT 0)");
					con.createStatement()
							.executeUpdate(
									"CREATE INDEX snapshot on SERVICE_SNAPSHOT(SNAPSHOTTIMESTAMP,SERVICE) ");
					con.createStatement()
							.executeUpdate(
									"create  table SUMMARY_SNAPSHOT (ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL,LAYER VARCHAR(6) NOT NULL DEFAULT 'BW', SERVICE VARCHAR(100) NOT NULL, MEAN DOUBLE, STDDEV DOUBLE, MEDIAN DOUBLE, MODE DOUBLE, SAMPLECOUNT INTEGER)");
					con.createStatement()
							.executeUpdate(
									"CREATE INDEX summary on SUMMARY_SNAPSHOT(SNAPSHOTTIMESTAMP,SERVICE) ");
					con.createStatement()
							.executeUpdate(
									"CREATE INDEX summaryservice on SUMMARY_SNAPSHOT(SERVICE) ");

					con.createStatement()
							.executeUpdate(
									"create  table VOLUMETRICS (ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, VOLUMETRIC VARCHAR(100) NOT NULL, QTY INTEGER)");
					con.createStatement()
							.executeUpdate(
									"CREATE INDEX volsnap on VOLUMETRICS(SNAPSHOTTIMESTAMP,VOLUMETRIC) ");

					con.createStatement()
							.executeUpdate(
									"create  table VOLUMETRICS_HIER (ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, VOLUMETRIC VARCHAR(100) NOT NULL, WORKSTREAM VARCHAR(100) NOT NULL, ITERATION DOUBLE DEFAULT 0)");
					con.createStatement()
							.executeUpdate(
									"CREATE INDEX voliter on VOLUMETRICS_HIER(SNAPSHOTTIMESTAMP,VOLUMETRIC) ");

					con.createStatement()
							.executeUpdate(
									"CREATE  table SERVICE (ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, SERVICE_NAME VARCHAR(100) NOT NULL, SERVICE_OPERATION VARCHAR(100) NOT NULL, SERVICE_LAYER VARCHAR(6) DEFAULT 'BW', SERVICE_ADJ DOUBLE DEFAULT 0)");
					con.createStatement()
							.executeUpdate(
									"CREATE  table SERVICE_HIER (ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, PARENT_SERVICE_NAME VARCHAR(100) NOT NULL, PARENT_SERVICE_OPERATION VARCHAR(100) NOT NULL, PARENT_SERVICE_LAYER VARCHAR(6) DEFAULT 'BW', CHILD_SERVICE_NAME VARCHAR(100) NOT NULL, CHILD_SERVICE_OPERATION VARCHAR(100) NOT NULL, CHILD_SERVICE_LAYER VARCHAR(6) DEFAULT 'BW')");
					// con.createStatement().executeUpdate("SET TABLE SERVICE_EVENT SOURCE \"servicedb.data;fs=|\" ");
	/*				con.createStatement().executeUpdate(
							"SET TABLE SERVICE SOURCE \"service_db.data;fs=|\" ");
					con.createStatement()
							.executeUpdate(
									"SET TABLE SERVICE_HIER SOURCE \"service_hier_db.data;fs=|\" ");
	*/
					con.createStatement()
							.executeUpdate(
									"CREATE  table SNAPSHOTS(ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, SNAPSHOTDESC VARCHAR(100))");
	/*				con.createStatement()
							.executeUpdate(
									"SET TABLE SNAPSHOTS SOURCE \"snapshot_db.data;fs=|\" ");
	*/
					//create tables to hold the samples for count graphs 
					con.createStatement()
					.executeUpdate(
							"CREATE  table samplesize(ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, SERVICE_NAME VARCHAR(100) NOT NULL, SERVICE_OPERATION VARCHAR(100) NOT NULL, SERVICE_LAYER VARCHAR(6) DEFAULT 'BW' NOT NULL, SAMPLESIZE INTEGER NOT NULL)");
					con.createStatement()
					.executeUpdate(
							"CREATE  table samplecounts(ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, SERVICE_NAME VARCHAR(100) NOT NULL, SERVICE_OPERATION VARCHAR(100) NOT NULL, SERVICE_LAYER VARCHAR(6) DEFAULT 'BW',SAMPLE BIGINT,COUNT INTEGER, SSUM DOUBLE, SMIN DOUBLE, SMAX DOUBLE, SSQ DOUBLE)");
					con.createStatement()
					.executeUpdate(
							"CREATE  index samplecountsindex on samplecounts (SNAPSHOTTIMESTAMP, SERVICE_NAME, SERVICE_OPERATION, SERVICE_LAYER)");
					con.createStatement()
					.executeUpdate(
							"CREATE TABLE LOGINS(ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL,TIMESTAMP BIGINT,COUNT INTEGER)");
					con.createStatement()
					.executeUpdate(
							"CREATE TABLE LOGGEDIN(ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL,USERNAME VARCHAR(100))");
					con.createStatement()
					.executeUpdate(
							"CREATE TABLE AUTOLOGOUTS(ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL,USERNAME VARCHAR(100), TIMESTAMP BIGINT)");
					con.createStatement()
					.executeUpdate(
							"CREATE TABLE LOGIN_EVENTS(ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL,USERNAME VARCHAR(100),STARTTIME BIGINT,ENDTIME BIGINT)");
							con.commit();

					con.createStatement().executeUpdate(
							"insert into SA  (VERSION) values (14)");
					// con.createStatement().executeUpdate("insert into SERVICE_EVENT (SERVICE) values ('FooBar')");
					con.commit();
					System.out.println("tables created");
					
					if(migrate)
					{
						if(splashGraphics!=null){
							updateSplash(splash,splashGraphics,"Migrating from Hypersonic DB to Derby DB");
							
							
						}
						con.setAutoCommit(false);
						System.out.println("migrate data");
						
						System.out.println("Migrate Service_Event Table");
						PreparedStatement pst = con2
								.prepareStatement("select * from SERVICE_EVENT");
						pst.clearParameters();
						ResultSet rs = pst.executeQuery();

						String insertTableSQL = "INSERT INTO SERVICE_EVENT"
								+ "(EVENT_TIME, SERVICE, RESPONSE) VALUES"
								+ "(?,?,?)";
						PreparedStatement ipst= con.prepareStatement(insertTableSQL);
						
						while(rs.next())
						{
							
							int id=rs.getInt("ID");
							long eventtime=rs.getLong("EVENT_TIME");
							String servicename=rs.getString("SERVICE");
							double response=rs.getDouble("RESPONSE");
							//System.out.println(id+","+eventtime+","+servicename+","+response);
							
							//ipst.setInt(1, id);
							ipst.setLong(1, eventtime);
							ipst.setString(2, servicename);
							ipst.setDouble(3, response);
							// execute insert SQL stetement
							ipst .executeUpdate();
						}
						pst.close();
						ipst.close();
						//SERVICE_SNAPSHOT (ID INTEGER PRIMARY KEY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, SERVICE VARCHAR(100) NOT NULL, RESPONSE DOUBLE, QTY DOUBLE, WAIT DOUBLE DEFAULT 0)");
						System.out.println("Migrate Service_Snapshot Table");
						pst = con2
								.prepareStatement("select * from SERVICE_SNAPSHOT");
						pst.clearParameters();
						rs = pst.executeQuery();
						
						insertTableSQL = "INSERT INTO SERVICE_SNAPSHOT"
								+ "(SNAPSHOTTIMESTAMP, SERVICE, RESPONSE, QTY, WAIT) VALUES"
								+ "(?,?,?,?,?)";
						ipst= con.prepareStatement(insertTableSQL);
						while(rs.next())
						{
							
							int id=rs.getInt("ID");
							long eventtime=rs.getLong("SNAPSHOTTIMESTAMP");
							String servicename=rs.getString("SERVICE");
							double response=rs.getDouble("RESPONSE");
							double qty=rs.getDouble("QTY");
							double wait=rs.getDouble("WAIT");
							//System.out.println(id+","+eventtime+","+servicename+","+response);
							//ipst.setInt(1, id);
							ipst.setLong(1, eventtime);
							ipst.setString(2, servicename);
							ipst.setDouble(3, response);
							ipst.setDouble(4, qty);
							ipst.setDouble(5, wait);
							// execute insert SQL stetement
							ipst .executeUpdate();
						}
						pst.close();
						ipst.close();
						//SUMMARY_SNAPSHOT (ID INTEGER PRIMARY KEY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, SERVICE VARCHAR(100) NOT NULL, MEAN DOUBLE, STDDEV DOUBLE, MEDIAN DOUBLE, MODE DOUBLE, SAMPLECOUNT INTEGER)
						migrate1(con,con2);
						//VOLUMETRICS (ID INTEGER PRIMARY KEY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, VOLUMETRIC VARCHAR(100) NOT NULL, QTY INTEGER)
						migrate2(con,con2);
						//VOLUMETRICS_HIER (ID INTEGER PRIMARY KEY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, VOLUMETRIC VARCHAR(100) NOT NULL, WORKSTREAM VARCHAR(100) NOT NULL, ITERATION DOUBLE DEFAULT 0)
						migrate3(con,con2);
						//SERVICE (ID INTEGER PRIMARY KEY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, SERVICE_NAME VARCHAR(100) NOT NULL, SERVICE_OPERATION VARCHAR(100) NOT NULL, SERVICE_LAYER VARCHAR(2) DEFAULT 'BW', SERVICE_ADJ DOUBLE DEFAULT 0)
						migrate4(con,con2);
						//SERVICE_HIER (ID INTEGER PRIMARY KEY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, PARENT_SERVICE_NAME VARCHAR(100) NOT NULL, PARENT_SERVICE_OPERATION VARCHAR(100) NOT NULL, PARENT_SERVICE_LAYER VARCHAR(2) DEFAULT 'BW', CHILD_SERVICE_NAME VARCHAR(100) NOT NULL, CHILD_SERVICE_OPERATION VARCHAR(100) NOT NULL, CHILD_SERVICE_LAYER VARCHAR(2) DEFAULT 'BW'
						migrate5(con,con2);
						//SNAPSHOTS(ID INTEGER PRIMARY KEY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, SNAPSHOTDESC VARCHAR(100))
						migrate6(con,con2);
						con.commit();
						/*try
						{
							// test if db structure there
							pst = con
									.prepareStatement("select * from SA");
							pst.clearParameters();
							rs = pst.executeQuery();
							rs.next();
							System.out.println("derby tables exist: version:"
									+ rs.getInt("VERSION"));
							int version = rs.getInt("VERSION");
						}catch(Exception e){}
						*/	
						
					}
					
					
					
					
				}
			}
		

				
			

		try
		{

			t.ProcessFile(depFile);
			if (!timeFile.equals(""))
			{
				if(splashGraphics!=null){
					updateSplash(splash,splashGraphics,"Processing Time File");
					
					
				}
				t.ProcessTimeFile(timeFile);

			}
			if (!adjFile.equals(""))
			{
				if(splashGraphics!=null){
					updateSplash(splash,splashGraphics,"Processing Adjustment File");
					
				}
				t.ProcessAdjFile(adjFile);
			}
			t.PrintTimes();

			// t.printConsumer("RDS_v1.0.get");

			serviceList = t.getServiceOperation();

			if (!modFile.equals("")){
				
			
				ModelFileReader.LoadNewModel(modFile, serviceList, _VM);
			}
			if (!jobFile.equals("")){
				t.ProcessJobStatsFile(jobFile);
			}
			if (!uiJobFile.equals("")){
				ModelFileReader.loadNewPerformanceTimings(uiJobFile,
						serviceList);
			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}

		if (gui)
		{
			if(splashGraphics!=null){
				updateSplash(splash,splashGraphics,"Starting UI");

			}
			MainForm mf = new MainForm(serviceList);
			mf.setVolumetricModel(_VM);
			mf.createGUI();
			ServiceDependencyView SDView = new ServiceDependencyView(
					serviceList);

			SDView.createGUI();
			mf.addSDView(SDView);

			mf.ShowUI();
			// SDView.ShowUI();
			// t.printServiceTimes();

			AverageTimeGraph chart = new AverageTimeGraph(
					"SERVICE.operation Average Time (ms)",
					TFileReader.getSortedTimeList(300, 1000000, "BW"));
			chart.pack();
			RefineryUtilities.centerFrameOnScreen(chart);
			// chart.setVisible(true);

			TotalTimeGraph chartT = new TotalTimeGraph(
					"SERVICE.operation Total Time (ms)", serviceList);

			chartT.pack();
			RefineryUtilities.centerFrameOnScreen(chartT);
			// chartT.setVisible(true);

			ModelForm modelF = new ModelForm(_VM);
			modelF.setVM(_VM);
			modelF.createGUI();
			// modelF.ShowUI();
			mf.LoadAutoRun();
			//Controller.getController().loadAutoRun();
		} else
		{
			if(splashGraphics!=null){
				updateSplash(splash,splashGraphics,"Starting without UI");
				
				try {
					Thread.sleep(2000l);
					
				} catch (InterruptedException e) {
					
					e.printStackTrace();
				}
				splash.close();
			}
			control = Controller.ControllerFactory(serviceList, _VM);
			// control.startUDPListener(5555);
			// control.startJMSListener();
		}
		_VM.simulateSave();
		serviceAnalysisModel.readersWriters.DBWriter.DBCheckEvent = false;

		// unit testing the service mapper
		// ServiceMapper.addMatch("PROCESS.", "PROCESS.", "PROCESS",
		// ServiceMapper.SERVICE);

	}

	private static void updateSplash(SplashScreen splash,Graphics2D splashGraphics, String text)
	{

		if(splashGraphics!=null){
			splashGraphics.clearRect(20, 85, 260, 20);
			splashGraphics.setColor(Color.YELLOW);
			splashGraphics.fill3DRect(20, 85, 260, 20, false);			
			splashGraphics.draw3DRect(20,85, 260, 20, false);
			splashGraphics.setColor(Color.BLACK);
			splashGraphics.drawString(text, 25, 100);
			splash.update();
		}
	}
	
	private static int migratetoseven(Connection con) throws SQLException {
		con.setAutoCommit(false);
		
		System.out.println("version 6 to 7 upgrade");
		con.createStatement().executeUpdate("truncate table SA");
		con.createStatement().executeUpdate(
				"insert into SA  (VERSION) values (7)");
		con.createStatement()
		.executeUpdate(
				"CREATE  table samplesize(ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, SERVICE_NAME VARCHAR(100) NOT NULL, SERVICE_OPERATION VARCHAR(100) NOT NULL, SERVICE_LAYER VARCHAR(6) DEFAULT 'BW' NOT NULL, SAMPLESIZE INTEGER NOT NULL)");

		con.createStatement()
		.executeUpdate(
				"CREATE  table samplecounts(ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL, SERVICE_NAME VARCHAR(100) NOT NULL, SERVICE_OPERATION VARCHAR(100) NOT NULL, SERVICE_LAYER VARCHAR(6) DEFAULT 'BW',SAMPLE BIGINT,COUNT INTEGER)");
		con.createStatement()
		.executeUpdate(
				"CREATE  index samplecountsindex on samplecounts (SNAPSHOTTIMESTAMP, SERVICE_NAME, SERVICE_OPERATION, SERVICE_LAYER)");
		con.commit();
		return 7;
	}
	
	private static int migratetoeight(Connection con) throws SQLException {
		con.setAutoCommit(false);
		
		System.out.println("version 7 to 8 upgrade");
		con.createStatement().executeUpdate("truncate table SA");
		con.createStatement().executeUpdate(
				"insert into SA  (VERSION) values (8)");
		con.createStatement();
		
		
		con.createStatement()
		.executeUpdate(
				"ALTER TABLE SUMMARY_SNAPSHOT ADD COLUMN LAYER VARCHAR(6) NOT NULL DEFAULT 'BW'");
		con.commit();
		
		
		return 8;
	}
	
	private static int migratetonine(Connection con) throws SQLException {
		con.setAutoCommit(false);
		
		System.out.println("version 8 to 9 upgrade");
		con.createStatement().executeUpdate("truncate table SA");
		con.createStatement().executeUpdate(
				"insert into SA  (VERSION) values (9)");
		con.createStatement();
		
		
		con.createStatement()
		.executeUpdate(
				"ALTER TABLE SERVICE_SNAPSHOT ADD COLUMN LAYER VARCHAR(6) NOT NULL DEFAULT 'BW'");
		con.commit();
		
		
		return 9;
	}
	
	private static int migratetoten(Connection con) throws SQLException {
		con.setAutoCommit(false);
		
		System.out.println("version 9 to 10 upgrade");
		con.createStatement().executeUpdate("truncate table SA");
		con.createStatement().executeUpdate(
				"insert into SA  (VERSION) values (10)");
		con.createStatement();
		
		
		con.createStatement()
		.executeUpdate(
				"ALTER TABLE SAMPLECOUNTS ADD COLUMN SSUM DOUBLE");
		con.commit();
		
		
		return 10;
	}
	
	private static int migratetoeleven(Connection con) throws SQLException {
		con.setAutoCommit(false);
		
		System.out.println("version 10 to 11 upgrade");
		con.createStatement().executeUpdate("truncate table SA");
		con.createStatement().executeUpdate(
				"insert into SA  (VERSION) values (11)");
		con.createStatement();
		
		
		con.createStatement()
		.executeUpdate(
				"ALTER TABLE SAMPLECOUNTS ADD COLUMN SMIN DOUBLE");
		con.createStatement()
		.executeUpdate(
				"ALTER TABLE SAMPLECOUNTS ADD COLUMN SMAX DOUBLE");
		con.commit();
		
		
		return 11;
	}
	
	private static int migratetotwelve(Connection con) throws SQLException {
		con.setAutoCommit(false);
		
		System.out.println("version 11 to 12 upgrade");
		con.createStatement().executeUpdate("truncate table SA");
		con.createStatement().executeUpdate(
				"insert into SA  (VERSION) values (12)");
		con.createStatement();
		
		
		con.createStatement()
		.executeUpdate(
				"ALTER TABLE SAMPLECOUNTS ADD COLUMN SSQ DOUBLE");
		
		con.commit();
		
		
		return 12;
	}
	
	private static int migratetothirteen(Connection con) throws SQLException {
		con.setAutoCommit(false);
		
		System.out.println("version 12 to 13 upgrade");
		con.createStatement().executeUpdate("truncate table SA");
		con.createStatement().executeUpdate(
				"insert into SA  (VERSION) values (13)");
		con.createStatement();
		
		
		con.createStatement()
		.executeUpdate(
				"CREATE TABLE LOGINS(ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL,TIMESTAMP BIGINT,COUNT INTEGER)");
		con.createStatement()
		.executeUpdate(
				"CREATE TABLE LOGGEDIN(ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL,USERNAME VARCHAR(100))");
		con.createStatement()
		.executeUpdate(
				"CREATE TABLE AUTOLOGOUTS(ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL,USERNAME VARCHAR(100), TIMESTAMP BIGINT)");

		con.commit();
		
		
		return 13;
	}
	
	private static int migratetofourteen(Connection con) throws SQLException {
		con.setAutoCommit(false);
		
		System.out.println("version 13 to 14 upgrade");
		con.createStatement().executeUpdate("truncate table SA");
		con.createStatement().executeUpdate(
				"insert into SA  (VERSION) values (14)");
		con.createStatement();
		
		
		con.createStatement()
		.executeUpdate(
				"CREATE TABLE LOGIN_EVENTS(ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, SNAPSHOTTIMESTAMP BIGINT NOT NULL,USERNAME VARCHAR(100),STARTTIME BIGINT,ENDTIME BIGINT)");
				con.commit();
		
		
		return 14;
	}

	public static VolumetricModel _VM;
	private static boolean gui = true;
	private static Controller control;
	
	private static void migrate1(Connection con ,Connection con2) throws SQLException
	{
		//SUMMARY_SNAPSHOT (
		//ID INTEGER PRIMARY KEY, 
		//SNAPSHOTTIMESTAMP BIGINT NOT NULL,
		//SERVICE VARCHAR(100) NOT NULL, 
		//MEAN DOUBLE, 
		//STDDEV DOUBLE, 
		//MEDIAN DOUBLE, 
		//MODE DOUBLE, 
		//SAMPLECOUNT INTEGER)

		System.out.println("Migrate Summary_Snapshot Table");
		PreparedStatement pst = con2.prepareStatement("select * from SUMMARY_SNAPSHOT");
		pst.clearParameters();
		ResultSet rs = pst.executeQuery();
		
		String insertTableSQL = "INSERT INTO SUMMARY_SNAPSHOT"
				+ "(SNAPSHOTTIMESTAMP, SERVICE, MEAN, STDDEV, MEDIAN, MODE, SAMPLECOUNT) VALUES"
				+ "(?,?,?,?,?,?,?)";
		PreparedStatement ipst= con.prepareStatement(insertTableSQL);
		while(rs.next())
		{
			
			int id=rs.getInt("ID");
			long eventtime=rs.getLong("SNAPSHOTTIMESTAMP");
			String servicename=rs.getString("SERVICE");
			double response=rs.getDouble("MEAN");
			double stddev=rs.getDouble("STDDEV");
			double median=rs.getDouble("MEDIAN");
			double mode=rs.getDouble("MODE");
			int count=rs.getInt("SAMPLECOUNT");
			//System.out.println(id+","+eventtime+","+servicename+","+response);
			//ipst.setInt(1, id);
			ipst.setLong(1, eventtime);
			ipst.setString(2, servicename);
			ipst.setDouble(3, response);
			ipst.setDouble(4, stddev);
			ipst.setDouble(5, median);
			ipst.setDouble(6, mode);
			ipst.setInt(7, count);
			
			// execute insert SQL stetement
			ipst .executeUpdate();
		}
		pst.close();
		ipst.close();
		
	}
	
	private static void migrate2(Connection con ,Connection con2) throws SQLException
	{
		//VOLUMETRICS (
		//ID INTEGER PRIMARY KEY, 
		//SNAPSHOTTIMESTAMP BIGINT NOT NULL, 
		//VOLUMETRIC VARCHAR(100) NOT NULL, 
		//QTY INTEGER)
		

		System.out.println("Migrate volumetrics Table");
		PreparedStatement pst = con2.prepareStatement("select * from VOLUMETRICS");
		pst.clearParameters();
		ResultSet rs = pst.executeQuery();
		
		String insertTableSQL = "INSERT INTO VOLUMETRICS"
				+ "(SNAPSHOTTIMESTAMP, VOLUMETRIC, QTY) VALUES"
				+ "(?,?,?)";
		PreparedStatement ipst= con.prepareStatement(insertTableSQL);
		while(rs.next())
		{
			
			int id=rs.getInt("ID");
			long eventtime=rs.getLong("SNAPSHOTTIMESTAMP");
			String servicename=rs.getString("VOLUMETRIC");

			int count=rs.getInt("QTY");
			//System.out.println(id+","+eventtime+","+servicename+","+count);
			//ipst.setInt(1, id);
			ipst.setLong(1, eventtime);
			ipst.setString(2, servicename);
			ipst.setInt(3, count);
			
			// execute insert SQL stetement
			ipst .executeUpdate();
		}
		pst.close();
		ipst.close();
		
	}
	private static void migrate3(Connection con ,Connection con2) throws SQLException
	{
		//VOLUMETRICS_HIER (
		//ID INTEGER PRIMARY KEY, 
		//SNAPSHOTTIMESTAMP BIGINT NOT NULL, 
		//VOLUMETRIC VARCHAR(100) NOT NULL, 
		//WORKSTREAM VARCHAR(100) NOT NULL, 
		//ITERATION DOUBLE DEFAULT 0)
		
		System.out.println("Migrate Volumetrics_Hier Table");
		PreparedStatement pst = con2.prepareStatement("select * from VOLUMETRICS_HIER");
		pst.clearParameters();
		ResultSet rs = pst.executeQuery();
		
		String insertTableSQL = "INSERT INTO VOLUMETRICS_HIER"
				+ "(SNAPSHOTTIMESTAMP, VOLUMETRIC, WORKSTREAM, ITERATION) VALUES"
				+ "(?,?,?,?)";
		PreparedStatement ipst= con.prepareStatement(insertTableSQL);
		while(rs.next())
		{
			
			int id=rs.getInt("ID");
			long eventtime=rs.getLong("SNAPSHOTTIMESTAMP");
			String volumetric=rs.getString("VOLUMETRIC");
			String workstream=rs.getString("WORKSTREAM");
			double iteration=rs.getDouble("ITERATION");
			
			//System.out.println(id+","+eventtime+","+volumetric+","+workstream);
			//ipst.setInt(1, id);
			ipst.setLong(1, eventtime);
			ipst.setString(2, volumetric);
			ipst.setString(3, workstream);
			ipst.setDouble(4, iteration);

			
			// execute insert SQL statement
			ipst .executeUpdate();
		}
		pst.close();
		ipst.close();
		
	}
	
	private static void migrate4(Connection con ,Connection con2) throws SQLException
	{
		//SERVICE (
		//ID INTEGER PRIMARY KEY, 
		//SNAPSHOTTIMESTAMP BIGINT NOT NULL, 
		//SERVICE_NAME VARCHAR(100) NOT NULL, 
		//SERVICE_OPERATION VARCHAR(100) NOT NULL, 
		//SERVICE_LAYER VARCHAR(2) DEFAULT 'BW', 
		//SERVICE_ADJ DOUBLE DEFAULT 0)

		
		System.out.println("Migrate Service Table");
		PreparedStatement pst = con2.prepareStatement("select * from SERVICE");
		pst.clearParameters();
		ResultSet rs = pst.executeQuery();
		
		String insertTableSQL = "INSERT INTO SERVICE"
				+ "(SNAPSHOTTIMESTAMP, SERVICE_NAME, SERVICE_OPERATION, SERVICE_LAYER, SERVICE_ADJ) VALUES"
				+ "(?,?,?,?,?)";
		PreparedStatement ipst= con.prepareStatement(insertTableSQL);
		while(rs.next())
		{
			
			int id=rs.getInt("ID");
			long eventtime=rs.getLong("SNAPSHOTTIMESTAMP");
			String service=rs.getString("SERVICE_NAME");
			String operation=rs.getString("SERVICE_OPERATION");
			String layer=rs.getString("SERVICE_LAYER");
			double adj=rs.getDouble("SERVICE_ADJ");
			
			//System.out.println(id+","+eventtime+","+volumetric+","+workstream);
			//ipst.setInt(1, id);
			ipst.setLong(1, eventtime);
			ipst.setString(2, service);
			ipst.setString(3, operation);
			ipst.setString(4, layer);
			ipst.setDouble(5, adj);

			
			// execute insert SQL statement
			ipst .executeUpdate();
		}
		pst.close();
		ipst.close();
		
	}
	
	private static void migrate5(Connection con ,Connection con2) throws SQLException
	{
		//SERVICE_HIER (
		//ID INTEGER PRIMARY KEY, 
		//SNAPSHOTTIMESTAMP BIGINT NOT NULL, 
		//PARENT_SERVICE_NAME VARCHAR(100) NOT NULL, 
		//PARENT_SERVICE_OPERATION VARCHAR(100) NOT NULL, 
		//PARENT_SERVICE_LAYER VARCHAR(2) DEFAULT 'BW', 
		//CHILD_SERVICE_NAME VARCHAR(100) NOT NULL, 
		//CHILD_SERVICE_OPERATION VARCHAR(100) NOT NULL, 
		//CHILD_SERVICE_LAYER VARCHAR(2) DEFAULT 'BW'

		

		
		System.out.println("Migrate Service_Hier Table");
		PreparedStatement pst = con2.prepareStatement("select * from SERVICE_HIER");
		pst.clearParameters();
		ResultSet rs = pst.executeQuery();
		
		String insertTableSQL = "INSERT INTO SERVICE_HIER"
				+ "(SNAPSHOTTIMESTAMP, PARENT_SERVICE_NAME, PARENT_SERVICE_OPERATION, PARENT_SERVICE_LAYER, CHILD_SERVICE_NAME, CHILD_SERVICE_OPERATION, CHILD_SERVICE_LAYER) VALUES"
				+ "(?,?,?,?,?,?,?)";
		PreparedStatement ipst= con.prepareStatement(insertTableSQL);
		while(rs.next())
		{
			
			int id=rs.getInt("ID");
			long eventtime=rs.getLong("SNAPSHOTTIMESTAMP");
			String service=rs.getString("PARENT_SERVICE_NAME");
			String operation=rs.getString("PARENT_SERVICE_OPERATION");
			String layer=rs.getString("PARENT_SERVICE_LAYER");
			String cservice=rs.getString("CHILD_SERVICE_NAME");
			String coperation=rs.getString("CHILD_SERVICE_OPERATION");
			String clayer=rs.getString("CHILD_SERVICE_LAYER");
			
			//System.out.println(id+","+eventtime+","+volumetric+","+workstream);
			//ipst.setInt(1, id);
			ipst.setLong(1, eventtime);
			ipst.setString(2, service);
			ipst.setString(3, operation);
			ipst.setString(4, layer);
			ipst.setString(5, cservice);
			ipst.setString(6, coperation);
			ipst.setString(7, clayer);
			
			// execute insert SQL statement
			ipst .executeUpdate();
		}
		pst.close();
		ipst.close();
		
	}
	
	private static void migrate6(Connection con ,Connection con2) throws SQLException
	{
		

		//SNAPSHOTS(
		//ID INTEGER PRIMARY KEY, 
		//SNAPSHOTTIMESTAMP BIGINT NOT NULL, 
		//SNAPSHOTDESC VARCHAR(100))

		
		System.out.println("Migrate SNAPSHOTS Table");
		PreparedStatement pst = con2.prepareStatement("select * from SNAPSHOTS");
		pst.clearParameters();
		ResultSet rs = pst.executeQuery();
		
		String insertTableSQL = "INSERT INTO SNAPSHOTS"
				+ "(SNAPSHOTTIMESTAMP, SNAPSHOTDESC) VALUES"
				+ "(?,?)";
		PreparedStatement ipst= con.prepareStatement(insertTableSQL);
		while(rs.next())
		{
			
			int id=rs.getInt("ID");
			long eventtime=rs.getLong("SNAPSHOTTIMESTAMP");
			String desc=rs.getString("SNAPSHOTDESC");
			
			
			//System.out.println(id+","+eventtime+","+volumetric+","+workstream);
			//ipst.setInt(1, id);
			ipst.setLong(1, eventtime);
			ipst.setString(2, desc);

			// execute insert SQL statement
			ipst .executeUpdate();
		}
		pst.close();
		ipst.close();
		
	}

	
	private static DBController dbcontroller;
}


