package networkAgent.agentTCP;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import networkAgent.collector.Base64Coder;


public class TCPTester {
	
	private static String server;
	private static int port;
	private static PublicKey pubk;

	public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException
	{
		server = args[0];
		port = Integer.parseInt(args[1]);
		Socket skt = new Socket(server,port);
		ObjectInputStream in = new ObjectInputStream(skt.getInputStream());
		PrintStream os = new PrintStream(skt.getOutputStream());
		int i=0;
		
		//os.println("Read /home/andrew/Documents/SA/timingsshort.csv");
		//os.flush();
		
		//os.println("Read /home/andrew/Documents/SA/Simulator csv");
		//os.flush();
		//os.println("Stop");
		//os.flush();
		//Thread.sleep(30000);
		//os.println("Read /home/andrew/Documents/SA/Simulator csv");
		//os.flush();
		//os.println("Read /home/andrew/Documents/SA/timingsshort.csv");
		//os.flush();
		boolean auth=false;
	    while(true)
	    {
	    	try {
				Object o=in.readObject();
				System.out.println(""+i+" "+o.toString());
				TCPMessage m=(TCPMessage) o;
				
				System.out.println(m.string);
					
				if(m!=null && m.string!=null && m.string.substring(0, 18).equalsIgnoreCase("start authenticate"))
				{
					String s=m.string.substring(19);
					System.out.println("pubkey="+m.string.substring(19));
					//pubk=new PublicKey();
					byte[] b=Base64Coder.decode(s);
					X509EncodedKeySpec keyspec=new X509EncodedKeySpec(b);
					KeyFactory kf = KeyFactory.getInstance("RSA");
					pubk=kf.generatePublic(keyspec);
					System.out.println(pubk);
					auth=true;
					
					    Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
					    cipher.init(Cipher.ENCRYPT_MODE, pubk);
					    byte[] enc=cipher.doFinal(("password").getBytes());
					    char[] encp=Base64Coder.encode(enc);
					System.out.println("authenticate received");
					os.println("user test,"+new String(encp));
					os.flush();
				}
					
				//if(m!=null && m.string==null){
					//		if(m.se==null)m=null;
					//		else System.out.println(m.se.toString());
					//		m=null;
					//	}
				if(m!=null) System.out.println(m.string);
				
				if(m!=null && m.string!=null && m.string.equalsIgnoreCase("authentication failed"))
				{
					System.out.println("failed authentication");
					os.println("quit");
					os.flush();
					System.exit(-2);
				}
						
				if(m!=null && m.string!=null && m.string.equalsIgnoreCase("authentication success"))
				{
					System.out.println("authentication successful");
				}
						
				
				if(!auth){
					System.out.println("send reply keep alive");
					os.println("Hello");
				}
			
				if(i++>10000){
					System.err.println("send quit");
					os.println("quit");
					//os.flush();
					//skt.getOutputStream().write("quit\n".getBytes());
					//skt.getOutputStream().flush();
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalBlockSizeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BadPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	         
	}

}
