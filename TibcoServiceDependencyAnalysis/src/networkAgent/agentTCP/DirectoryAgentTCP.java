package networkAgent.agentTCP;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;




import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.HiddenFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import serviceAnalysisModel.SAControl.StdOutHandler;
import serviceAnalysisModel.readersWriters.ExtensionHandler;
import serviceAnalysisModel.readersWriters.LogFileReader;
import serviceAnalysisModel.readersWriters.LogFileTailerListener;
import serviceAnalysisModel.readersWriters.ServiceEntry;

public class DirectoryAgentTCP implements FileAlterationListener {
	
	private static String location;
	//private FileAlterationMonitor monitor;
	private Map<DirectorySettings,FileAlterationMonitor> monitorsSettings=new ConcurrentHashMap<DirectorySettings,FileAlterationMonitor>();
	private CopyOnWriteArrayList<FileAlterationMonitor> monitors=new CopyOnWriteArrayList<FileAlterationMonitor>();
	private File directory;
	private String suffix;
	private int port;
	private ServerSocket srvr;
	private SocketListener slistener;
	//private Socket socket;
	private static final Logger logger=Logger.getLogger("Collector");
	private FileProcessor processor=new FileProcessor();
	private CopyOnWriteArrayList<TCPSender> senders=new CopyOnWriteArrayList<TCPSender>();
	private CopyOnWriteArrayList<TCPReceiver> receivers=new CopyOnWriteArrayList<TCPReceiver>();

	public boolean stopping=false;
	private Poller poller;
	
	private class DirectorySettings {
		public String path=null;
		public String suffix=null;
		public File directory=null;
		public String absPath=null;
		
		public DirectorySettings(String dirPath, String suffix)
		{
			path=dirPath;
			this.suffix=suffix;
			if(path!=null)
			{
				directory=new File(path);
				try {
					absPath=directory.getCanonicalPath();
				} catch (IOException e) {

				}
			}
		}
		
		public boolean contains(DirectorySettings ds)
		{
			
			boolean val=false;
			if(suffix==null | path==null | ds.path==null | ds.suffix==null ) {
				logger.finest("null in settings");
				return false;
			}
			if(suffix.equals(ds.suffix) )
			{
				logger.finest("suffixes are equal, "+suffix);
				if(!(absPath==null | ds.absPath==null))
				{
					if(ds.absPath.startsWith(absPath)) {
						logger.finest(absPath+" is contained in "+ds.absPath);
						val = true;
					}
				}
			}
			
			return val;
		}
		
	}
	
	
	
	public DirectoryAgentTCP(String directory, String suffix, String port) {
		if(logger.isLoggable(Level.INFO)) {
			//logger.entering("Collector", "Constructor");
			logger.info(directory+", "+suffix+", "+port+", "+logger.getLevel().getName());
		}
		
		try {
			
			this.directory = new File(directory);
			this.suffix=suffix;
			this.port=Integer.parseInt(port);
			openTCPPort();
			startMonitor();
			startPoller();
		} catch (NumberFormatException e) {
			logger.severe(e.getMessage());
	    	logger.log(Level.SEVERE, e.getMessage(), e);
	    	System.exit(-2);
	    	
		} catch (MonitorException e) {
				logger.severe(e.getMessage());
				logger.log(Level.SEVERE, e.getMessage(), e);
				System.exit(-4);
		}
	}
	
	private void startPoller()
	{
		poller=new Poller();
		poller.start();
	}

	public static void main(String[] args)
	{
		logger.setUseParentHandlers(false);
		for(Handler h:logger.getHandlers())
			logger.removeHandler(h);
		Handler consoleHandler = new StdOutHandler();//new ConsoleHandler();
		consoleHandler.setLevel(Level.FINEST);
		logger.addHandler(consoleHandler);

		
		
		if(args.length>3 && args[3].equals("-debug"))
			logger.setLevel(Level.FINEST);
		else if(args.length>3 && args[3].equals("-info"))
			logger.setLevel(Level.INFO);
		else if(args.length>3 && args[3].equals("-trace"))
			logger.setLevel(Level.FINE);
		else logger.setLevel(Level.SEVERE);
		
		// TODO Auto-generated method stub
		if(args.length<3)
		{
			logger.severe("usage: <directory> <file suffix> <TCP port> [<-debug|-info>]");
			System.err.println("usage: <directory> <file suffix> <TCP port>");
			System.exit(-1);
		}
		
			
		
		if(!(new File(args[0])).isDirectory()) 
		{
			logger.severe(args[0] +" is not a directory");
			System.err.println(args[0] +" is not a directory");
			System.exit(-1);
		}
		
		try
		{
			location=InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e)
		{
			//e.printStackTrace();
			location=e.getMessage().substring(e.getMessage().length()/2+1);
			//location=null;
		}
		
		DirectoryAgentTCP da=new DirectoryAgentTCP(args[0],args[1],args[2]);
		logger.info("Directory Agent Started");
		//System.out.println("Directory Monitor Started");
	}
	
	private void openTCPPort()
	{
		if(logger.isLoggable(Level.FINEST)) logger.finest("Entering openTCPPort");
		try {
	           srvr = new ServerSocket(port);
	           srvr.setSoTimeout(10000);
	           slistener=new SocketListener(srvr);
	           logger.finest("Start Socket Listener");
	           slistener.start();
	        }
	        catch (IOException e) {
	        	logger.log(Level.SEVERE, e.getMessage(), e);
	           
	        }   
		
		if(logger.isLoggable(Level.FINEST)) logger.finest("Exiting openTCPPort");
	}
	
	private class SocketListener extends Thread
	{
		private ServerSocket server;
		private boolean wait=true;

		public SocketListener(ServerSocket srvr)
		{
			this.setDaemon(false);
			server=srvr;
			
		}
		
		public void interrupt()
		{
			logger.info("socket listener interrupted");
			wait=false;
			super.interrupt();
		}
		
		public void run()
		{
			logger.info("Waiting for client connection");
			
			while(wait)
				try {
					
					Socket socket = server.accept();
					logger.info("Client Connected: remote port"+socket.getPort());
					
					// start the sender
					TCPSender sender = new TCPSender(socket);
					senders.add(sender);
					sender.start();
					//start the control line from client
					TCPReceiver receiver=new TCPReceiver(socket,sender);
					receivers.add(receiver);
					receiver.start();
					
					
				}catch (SocketTimeoutException e) {
					//catching this one to do nothing 
					//logger.info(logger.getLevel().getName());
					logger.finest("Timed out waiting for client connect");
				} 
				catch (IOException e) {
					logger.log(Level.SEVERE, e.getMessage(), e);
					interrupt();
				}
			logger.info("Connection Thread Ended");
		}
	}
	
	private boolean startMonitor(DirectorySettings ds) throws MonitorException
	{
		//check we are not already being monitored
		
		for(DirectorySettings setting:monitorsSettings.keySet())
		{
			if(setting.contains(ds)) {
				logger.finest(setting.absPath+" parent directory of "+ds.absPath);
				throw new MonitorException("Directory, "+ ds.absPath +", already monitored for extension "+ds.suffix);
			}
			
		}
		
		FileAlterationMonitor monitor;
		if(logger.isLoggable(Level.FINEST)) logger.finest("Entering startMonitor");
		if(ds.directory.exists() & ds.directory.isDirectory())
		{
			if(logger.isLoggable(Level.FINEST)) logger.finest("Directory "+ds.absPath+" exists and readable");
			
			// Create a FileFilter
		      IOFileFilter directories = FileFilterUtils.and(
		                                      FileFilterUtils.directoryFileFilter(),
		                                      HiddenFileFilter.VISIBLE);
		      IOFileFilter files       = FileFilterUtils.and(
		                                      FileFilterUtils.fileFileFilter(),
		                                      FileFilterUtils.suffixFileFilter(suffix));
		      IOFileFilter filter = FileFilterUtils.or(directories, files);

		      // Create the File system observer and register File Listeners
		      FileAlterationObserver observer = new FileAlterationObserver(this.directory, filter);
		      
		      observer.addListener(this);
		      
		      monitor = new FileAlterationMonitor();
		      monitor.addObserver(observer);
		      try
		      {
		    	if(logger.isLoggable(Level.FINEST)) logger.finest("Starting Monitor");  
				monitor.start();
				monitors.add(monitor);
				monitorsSettings.put(ds, monitor);
				if(logger.isLoggable(Level.FINEST)) logger.info("Directory Monitor Started");
				return true;
		      } catch (Exception e)
		      {
				// TODO Auto-generated catch block
		    	  logger.severe("can't start monitor");
		    	  logger.severe(e.getMessage());
		    	  logger.log(Level.SEVERE, e.getMessage(), e);
		    	  //e.printStackTrace();
				
				System.err.println("can't start monitor");
				return false;
		      }
		      
		}else {
			logger.severe(directory+" is not a Directory");
			return false;
		}
	}
	
	private void startMonitor() throws MonitorException
	{
		DirectorySettings ds=null;
		try {
			ds = new DirectorySettings(directory.getCanonicalPath(),suffix);
		} catch (IOException e) {
			logger.severe(directory+" unable to get Canonical Path");
			System.exit(-1);
		}
		
		
		if(logger.isLoggable(Level.FINEST)) logger.finest("Testing for Directory");
		if(this.directory.exists() & this.directory.isDirectory())
		{
			boolean success=startMonitor(ds);
			
		    if(!success)
		      {
				logger.severe("can't start monitor");
				System.err.println("can't start monitor");
				System.exit(-3);
		      }
		      
		}else {
			logger.severe(directory+" is not a Directory");
			System.exit(-1);
		}
		
		if(logger.isLoggable(Level.FINEST)) logger.finest("Exiting startMonitor");
	}
	
	
	
	@Override
	public void onDirectoryChange(File arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDirectoryCreate(File arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDirectoryDelete(File arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFileChange(File arg0) {
		onFileCreate(arg0);
		
	}

	@Override
	public void onFileCreate(File arg0) {
		if(stopping){
			logger.info("Stopping all monitors. Ignoring add for "+arg0.getAbsolutePath());
			return;
		}
		logger.finest(arg0.toString());
		String filename; 
		try {
			filename=arg0.getCanonicalPath();
		} catch (IOException e) {
			filename=arg0.getAbsolutePath();
		}
		if(!filenames.contains(filename))
		{
			
			LogFileReader lfr = new LogFileReader(filename);
			lfr.addLogFileTailerListener(processor);
			lfr.start();
			fileAgents.add(lfr);
			
			filenames.add(filename);
			logger.info("file added: "+filename);
			
		} else logger.finest("file already monitored");
	}

	@Override
	public void onFileDelete(File arg0) {
		if(stopping) return;
		String filename;
		try {
			filename=arg0.getCanonicalPath();
		} catch (IOException e) {
			filename=arg0.getAbsolutePath();
		}
		if(filenames.contains(filename)){
			for(LogFileReader lfr:fileAgents)
				if(lfr.getFilename().equals(filename)) {
					lfr.interrupt();
				}
			filenames.remove(filename);
			logger.info("file deleted: "+filename);
			
		}		
	}

	@Override
	public void onStart(FileAlterationObserver arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStop(FileAlterationObserver arg0) {
		// TODO Auto-generated method stub
		
	}
	private ArrayList<String> filenames=new ArrayList<String>();
	private ArrayList<LogFileReader> fileAgents=new ArrayList<LogFileReader>();

	private void trace(String message,TCPMessage t)
	{
		ServiceEntry nse = t.se;
		Integer number=t.txn;
		if(nse!=null)logger.fine("<Trace> <"+number+"> <"+message+"> "+nse.location+" "+nse.filename+" "+nse.txn);
		else logger.fine("<Trace> <"+number+"> <"+message+">");
	}
	
	private void trace(String message,ServiceEntry nse)
	{
		if(!logger.isLoggable(Level.FINE))	return;
		//System.out.println("doing trace");
		Integer number=tracenumber.get(nse);
		if(number==null){
			number=txn++;
			tracenumber.put(nse, number);
		}
		logger.fine("<Trace> <"+number+"> <"+message+"> "+nse.location+" "+nse.filename+" "+nse.txn);
		
	}
	
	private void cleanTrace(ServiceEntry nse){
		tracenumber.remove(nse);
	}
	
	private Integer getTraceNumber(ServiceEntry nse)
	{
		return tracenumber.get(nse);
	}
	private ConcurrentHashMap<ServiceEntry,Integer> tracenumber=new ConcurrentHashMap<ServiceEntry,Integer>();
	private int txn=0;
	
	private class FileProcessor implements LogFileTailerListener
	{
		
		@Override
		public void newLogFileLine(String line, String filename) {
			logger.finest("new log line received from file "+filename);
			logger.finest(line);
			
			//if(senders.isEmpty()) {logger.finest("No Clients! Ignoring");return;}
			//if (se == null)
			//	se = new ServiceEntry(new ArrayList<ServiceEntry>());
			ExtensionHandler RTR = ExtensionHandler.getInstance();
			ServiceEntry nse = RTR.ProcessLogFileLine(line);
			
			if (nse != null)
			{
				// System.out.println("service entry found");
				// se.seList.add(nse);
				nse.filename=filename;
				if(nse.location==null)nse.location=location;
				try {
					trace("Received",nse);
					boolean flag;
					if(!senders.isEmpty()){
						do {
							flag = queue.offer(nse,10,TimeUnit.SECONDS);
						} while (!flag);
						logger.finest("Service Entry Added to queue");
					}else{
						cleanTrace(nse);
						logger.finest("Drop Service Entry as there are no listeners");
					}
					//trace("Queued",nse);
				} catch (InterruptedException e) {
					
				}
				

			}
			
		}

		@Override
		public void fileEndReached(String filename) {
			// TODO Auto-generated method stub
			//onFileDelete(new File(filename));
			if(stopping) return;
			String canFilename;
			File file=new File(filename);
			try {
				canFilename=file.getCanonicalPath();
			} catch (IOException e) {
				canFilename=file.getAbsolutePath();
			}
				boolean b=filenames.remove(filename);
				if(b)logger.info("file removed from monitor: "+filename);
				
				
		}
		
	}
	
	private class TCPSender extends Thread
	{
		private ArrayBlockingQueue<TCPMessage> senderQueue=new ArrayBlockingQueue<TCPMessage>(10,true);
		private Socket senderSocket;
		private ObjectOutputStream objectOutput=null;
		public boolean alive=true;
		
		public TCPSender(Socket socket)
		{
			senderSocket=socket;
			try {
				objectOutput = new ObjectOutputStream(socket.getOutputStream());
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, e.getMessage(), e);
				run=false;
			}

			
		}
		
		public void interrupt()
		{
			logger.finest("Interrupt called");
			run=false;
			alive=false;
			super.interrupt();
			if(objectOutput!=null)
				try {
					objectOutput.close();
				} catch (IOException e) {
					logger.log(Level.SEVERE, e.getMessage(), e);
				}
		}
		
		public boolean addToQueue(ServiceEntry se) throws InterruptedException
		{
			logger.finest("addToQueue for TCPSender "+this+" se "+se);
			if(!alive){logger.finest("Received message but this sender is not alive "+this); return false;}
			TCPMessage m=new TCPMessage(se);
			m.txn=getTraceNumber(se);
			cleanTrace(se);
			return senderQueue.offer(m,10,TimeUnit.SECONDS);
		}
		
		public boolean addToQueue(String string) throws InterruptedException
		{
			TCPMessage m=new TCPMessage(string);
			return senderQueue.offer(m,10,TimeUnit.SECONDS);
		}
		
		private boolean run = true;
		
		public void run()
		{
			if(objectOutput!=null)
			{
				while(run){
					try {
						TCPMessage m=senderQueue.poll(30, TimeUnit.SECONDS);
						if(m!=null)
						{
							logger.finest("event to send to port "+senderSocket.getPort()+" with data "+m.toString());
							
							objectOutput.writeObject(m); 
							objectOutput.flush();
							objectOutput.reset();//without this we run out of heap!!
							trace("Sent to "+senderSocket.getPort(), m);
							
							
						}
						else
						{
							logger.finest("Nothing to send so do a keep-alive to port "+senderSocket.getPort());
							m=new TCPMessage();
							objectOutput.writeObject(m); 
							objectOutput.flush();
							objectOutput.reset();
						}
					} catch (InterruptedException e) {
						run=false;
					} catch (IOException e) {
						logger.log(Level.SEVERE, e.getMessage(), e);
						run=false;
						logger.severe("closing sender");
					}
				}
				try {
					objectOutput.close();
				} catch (IOException e) {
					logger.log(Level.SEVERE, e.getMessage(), e);
				}
			}
			alive=false;
		}
		
	}

	public void sendMessageToClients(String message)
	{
		
		try {
			for(TCPSender s:senders)
			{
				
				s.addToQueue(message);
			}
		} catch (InterruptedException e) {
			logger.info("Sending message interrupted");
		}
		
	}
	
	private ArrayBlockingQueue<ServiceEntry> queue=new ArrayBlockingQueue<ServiceEntry>(1000,true);
	
	private class Poller extends Thread
	{
		public Poller()
		{
			logger.finest("Poller created");
			setDaemon(true);
		}
		
		private boolean run=true;
		
		public void interrupt()
		{
			logger.info("poller interrupted, no more events will be sent!");
			run=false;
			super.interrupt();
		}
		
		public void run()
		{
			logger.finest("poller started");
			while(run)
			{
				try {
					logger.finest("waiting for message to appear");
					ServiceEntry se=queue.poll(10, TimeUnit.SECONDS);
					
					if(se!=null)
					{
						logger.finest("Got message from queue, "+se);
						for(TCPSender s:senders)
						{
							logger.finest("TCPSender in list "+s);
							boolean success=false;
							do {
								if(s.alive){
									logger.finest("TCPSender state is alive");
									success=s.addToQueue(se);
									if(!success)logger.finest("Timed out trying to add to TCPSender queue "+s.senderSocket.getPort());
									else logger.finest("Message added to TCPSender "+s.senderSocket.getPort());
								} else
								{
									logger.info("removing sender "+s.senderSocket.getPort());
									senders.remove(s);
									success=true;
								}
								
							}while(!success);
						}
						se=null;
					}
				} catch (InterruptedException e) {
					logger.log(Level.SEVERE, e.getMessage(), e);
				}
			}
		}
	}
	 
	
	
	private class TCPReceiver extends Thread
	{
		private boolean run=true;
		private Socket socket;
		private TCPSender sender;		
		private BufferedReader reader;
		
		public TCPReceiver(Socket socket,TCPSender sender)
		{
			setDaemon(true);	
			this.socket=socket;
			this.sender=sender;
			
			try {
				reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		 
				
			} catch (IOException e) {
				logger.severe("can't open TCP input stream for reading ");
				run=false;
			}
		}
		
		public void interrupt()
		{
			run=false;
			sender.interrupt();
			super.interrupt();
		}
		
		public void run()
		{
			while(run)
			{
				try {
					logger.finest("waiting for a command "+socket.getPort());
					String line = reader.readLine();
					logger.finest("line received: "+line);
					if(line==null){
						logger.info("Input stream EOF closing socket.  Has the client closed?");
						sender.interrupt();
						run=false;
						line="";
					}
					if(line.startsWith("quit")){
						logger.info("sender received quit command");
						sender.interrupt();
						run=false;
					}
					if(line.equals("Hello"))
					{
						logger.finest("Keep Alive Received");
					}
					if(line.toLowerCase().startsWith("stop")) {logger.finest("stop received");stopMonitor(line);}
					if(line.toLowerCase().startsWith("monitor")) {logger.finest("monitor received");monitorFile(line);}
					if(line.toLowerCase().startsWith("read"))
					{
						logger.info("Read command received");
						String split[] = line.split(" ");
						boolean directory=false;
						if(split.length==3  ){
							if(!split[1].startsWith("\"")){
								logger.finest("Read location is "+split[1]+", extension is "+split[2]);
								startReader(sender,split[1],split[2]);
								directory=true;
							}
							
						} else if(split.length>3 & split[split.length-2].endsWith("\""))
						{ //directory with spaces in directory name
							directory=true;
							String read=split[1].substring(1);
							for(int i=2;i<split.length-3;i++) 
								read=read+split[i];
							int i=split[split.length-2].length();
							read=read+split[split.length-2].substring(0, i);
							logger.finest("Read location is "+read+", extension is "+split[split.length-1]);
							startReader(sender,read,split[split.length-1]);
						}
						if(!directory & split.length>2 & !(split[split.length-1].endsWith("\"")&split[1].startsWith("\""))|split.length<2)
						{//error
							logger.severe("Incorrect arguments to Read Command received");
							logger.severe(line);
							
						}else if(!directory &split.length==2)
						{ //split[1] holds the file or directory
							startReader(sender,split[1],null);
							logger.finest("Read location is "+split[1]);
						}else if(!directory )
						{ //split holds string across several entries
							
							String read=split[1].substring(1);
							for(int i=2;i<split.length-2;i++) 
								read=read+split[i];
							int i=split[split.length-1].length();
							read=read+split[split.length-1].substring(0, i);
							logger.finest("Read location is "+read);
							startReader(sender,read,null);
						}
							
					}
				} catch (SocketException e){ 
					run=false;
					logger.log(Level.SEVERE, e.getMessage(), e);
					
				} catch (IOException e) {
					logger.log(Level.SEVERE, e.getMessage(), e);
					run=false;
				} 
			}
			try {
				reader.close();
			} catch (IOException e) {
				logger.log(Level.SEVERE, e.getMessage(), e);
			}
			if(sender.isAlive()&!sender.isInterrupted())sender.interrupt();
		}

		
		private void monitorFile(String line) {
			logger.finest("setting up a file monitor");
			String filename=line.substring(8).trim();
			if(filename.startsWith("\"")) filename=filename.substring(1,filename.length()-1);
			logger.finest("monitor for "+filename+" file requested");
			File file=new File(filename);
			try {
				if(file.exists() & file.isFile() & file.canRead())
				{
					String canonical;
					try {
					canonical=file.getCanonicalPath();
					} catch (IOException e){
						canonical=file.getAbsolutePath();
					}
					
					if(filenames.contains(canonical)) {
						logger.info("filename already being monitored");
						addToSenderQueue("file, "+filename+", is equivalent to, "+canonical+", which is already being monitored");
						
					}
					else {

						
						LogFileReader lfr = new LogFileReader(filename);
						lfr.addLogFileTailerListener(processor);
						lfr.start();
						
						logger.info("Monitor for file, "+filename+", started");
						fileAgents.add(lfr);
						
						filenames.add(filename);
						logger.info("file added: "+filename);
						addToSenderQueue("Monitor for file, "+filename+", started");
					}
				} else
				{
					addToSenderQueue("File, "+filename+", is not monitorable!");
					logger.info("File, "+filename+", is not monitorable!");
				}
			}catch (InterruptedException e) {
				
			}
		}

		private void stopMonitor(String line)
		{
			logger.finest("stopping all monitors");
			stopping=true;
			//stop all directory monitors
			for(DirectorySettings ds:monitorsSettings.keySet())
			{
				FileAlterationMonitor monitor = monitorsSettings.get(ds);
				try {
					monitor.stop(10000);
					logger.info("Stopped Directory Watcher "+ds.absPath+", extension "+ds.suffix);
					monitorsSettings.remove(ds);
					monitors.remove(monitor);
				} catch (Exception e) {
					
					logger.info("unable to stop watcher "+ds.absPath+", extension "+ds.suffix);
					logger.log(Level.INFO, e.getMessage(), e);
				}
			}
			//stop all file monitors
			
			logger.finest("stopping file readers");
			for(LogFileReader lfr:fileAgents)
			{
				
				lfr.stopTailing();
			}
			fileAgents.clear();
			filenames.clear();
			stopping=false;
		}
		
		private void startReader(TCPSender sender2, String filename, String extension) {
			// TODO Auto-generated method stub
			if(stopping) {
				logger.info("Cannot start reader as we are stopping all readers");
				try {
					addToSenderQueue("Cannot start reader as we are stopping all readers");
				} catch (InterruptedException e) {

				}
				return;
			}
			logger.finest("filename="+filename+", extension="+extension);
			File file=new File(filename);
			if(!file.exists())
			{
				logger.info("Read location does not exist, "+filename);
				try {
					addToSenderQueue("Read location does not exist, "+filename);
				} catch (InterruptedException e) {
					logger.finest("Unable to send message to client");
				}
				return;
			} else if(file.isDirectory())
			{
				logger.finest("Read location is a Directory");
				if(!file.canRead())
				{
					logger.info("Directory, "+filename +", is not readable");
					try {
						addToSenderQueue("Directory, "+filename +", is not readable");
					} catch (InterruptedException e) {
						logger.finest("Unable to send message to client");
					}
					return;
				}
				logger.finest("Watching a directory");
				//start directory reader here - what extension are we watching!
				DirectorySettings ds = new DirectorySettings(filename,extension);
				try {
					startMonitor(ds);
				} catch (MonitorException e) {
					try {
						logger.info(e.getMessage());
						addToSenderQueue(e.getMessage());
					} catch (InterruptedException e1) {
						
					}
				}
			}else if(file.isFile())
			{
				logger.finest("Read location is a File");
				if(!file.canRead())
				{
					logger.severe("File, "+filename +", is not readable");
					try {
						addToSenderQueue("File, "+filename +", is not readable");
					} catch (InterruptedException e) {

					}
					return;
				}
				
				//read file here
				LogFileReader lfr = new LogFileReader(file.getAbsolutePath(),true);
				LogFileReaderProcessor lfrProc=new LogFileReaderProcessor(sender,lfr);
				lfr.addLogFileTailerListener(lfrProc);
				lfr.start();
			}
			
		}
		
		private void addToSenderQueue(String s) throws InterruptedException
		{
			boolean flag=true;
			do {
				flag=sender.addToQueue(s);
			}while(!flag);
		}
		
		private void addToSenderQueue(ServiceEntry s) throws InterruptedException
		{
			boolean flag=true;
			do {
				flag=sender.addToQueue(s);
			}while(!flag);
		}
		
		private class LogFileReaderProcessor implements LogFileTailerListener
		{

			public LogFileReaderProcessor(TCPSender sender, LogFileReader lfr) {
				this.sender=sender;
				this.lfr=lfr;
				
			}
			TCPSender sender=null;
			LogFileReader lfr=null;
			
			@Override
			public void newLogFileLine(String line, String filename) {
				
				ExtensionHandler RTR = ExtensionHandler.getInstance();
				ServiceEntry nse = RTR.ProcessLogFileLine(line);
				
				if (nse != null)
				{
					// System.out.println("service entry found");
					// se.seList.add(nse);
					nse.filename=filename;
					if(nse.location==null)nse.location=location;
					try {
						trace("Received",nse);
						boolean flag;
						
							addToSenderQueue(nse);
						
						logger.finest("Service Entry Added to queue");	
						//trace("Queued",nse);
					} catch (InterruptedException e) {
						
					}
					

				}
			}
			

			@Override
			public void fileEndReached(String filename) {
				logger.info("File "+filename+" completed");
				
				try {
					addToSenderQueue("File "+filename+" completed");
				} catch (InterruptedException e) {
					
				}
				lfr.removeLogFileTailerListener(this);
			}
			
		}
	}
	
	private class MonitorException extends Exception
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 7752741263479716979L;

		public MonitorException(String string) {
			super(string);
		}

	}
	
}
