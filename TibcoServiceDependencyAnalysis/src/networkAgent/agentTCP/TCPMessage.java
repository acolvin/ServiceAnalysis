package networkAgent.agentTCP;

import java.io.Serializable;

import serviceAnalysisModel.readersWriters.ServiceEntry;



public class TCPMessage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2989798952432927290L;
	public ServiceEntry se;
	public String string=null;
	public Integer txn;
	
	public TCPMessage(ServiceEntry se)
	{
		this.se=se;
	}

	public TCPMessage() {
		se=null;
	}
	
	public TCPMessage(String message)
	{
		se=null;
		string=message;
	}
}
