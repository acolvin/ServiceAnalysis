package networkAgent.agentJMS;

import java.io.File;
import java.io.IOException;

import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Destination;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Queue;
import javax.jms.ObjectMessage;

import serviceAnalysisModel.readersWriters.*;

public class AgentJMS implements LogFileTailerListener
{
	public static void main(String[] args) throws IOException,
			InterruptedException
	{
		// new AgentUDPThread().start();
		AgentJMS a = new AgentJMS(args[0]);// ,args[1],args[2]);
		try
		{
			a.context = new InitialContext();
			a.factory = (ConnectionFactory) a.context.lookup(a.factoryName);
			a.dest = (Destination) a.context.lookup(a.destName);
			// a.queue = (Queue) a.context.lookup(a.destName);
			a.connection = a.factory.createConnection();
			a.session = a.connection.createSession(false,
					Session.AUTO_ACKNOWLEDGE);

			a.start();
			while (true)
			{
				Thread.sleep(1000);
			}

		} catch (NamingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JMSException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally
		{
			// close the context

			if (a.context != null)
			{
				try
				{
					a.context.close();
				} catch (NamingException exception)
				{
					exception.printStackTrace();
				}
			}

			// close the connection
			if (a.connection != null)
			{
				try
				{
					if (a.session != null)
						a.session.close();
					a.connection.close();
				} catch (JMSException exception)
				{
					exception.printStackTrace();
				}
			}
		}

	}

	Context context = null;
	ConnectionFactory factory = null;
	Connection connection = null;
	String factoryName = "connectionFactory";
	String destName = "SAevent";
	Destination dest = null;
	Queue queue = null;
	Session session = null;
	String filename="";

	public AgentJMS(String filename)// , String IP, String queue)
	{
		// this.queue1=queue1;
		if (filename.equals("--"))
		{
			pReader = new PipeReader(System.in);
			pReader.addListener(this);
			readPipe = true;
			this.filename="stdin";
		} else
		{
			file = new File(filename);
			lfr = new LogFileReader(file);
			lfr.addLogFileTailerListener(this);
			this.filename=filename;
		}
		try
		{
			// connect to jms

			// address = InetAddress.getByName(IP);
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			// System.err.println("Host "+IP+" not found");
			e.printStackTrace();

			System.exit(-2);
		}

	}

	private boolean readPipe = false;

	public void start()
	{
		// start socket listener

		// try
		// {
		// datagramSocket = new DatagramSocket();
		// } catch (SocketException e)
		// {
		// TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		if (readPipe)
			pReader.start();
		else
			lfr.start();

	}

	MessageProducer sender = null;

	@Override
	public void newLogFileLine(String line,String filename)
	{

		// TODO Auto-generated method stub
		if (se == null)
			se = new ServiceEntry(new ArrayList<ServiceEntry>());
		ExtensionHandler RTR = ExtensionHandler.getInstance();
		ServiceEntry nse = RTR.ProcessLogFileLine(line);
		nse.filename=filename;
		if (nse != null)
		{
			// System.out.println("service entry found");
			// se.seList.add(nse);
			try
			{
				if (sender == null)
				{
					sender = session.createProducer(dest);
					sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
				}
				ObjectMessage o = session.createObjectMessage(nse);
				// o.setJMSDeliveryMode(DeliveryMode.NON_PERSISTENT);

				sender.send(o);

				// System.out.println("sent message");
			} catch (JMSException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@Override
	public void fileEndReached(String filename)
	{
		// TODO Auto-generated method stub

	}

	// public InetAddress address;
	public ServiceEntry se = null;
	public LogFileReader lfr;
	public PipeReader pReader;
	public AgentJMS agent;
	// public int port=5555;
	public File file;
	// public String IP="127.0.0.1";

}
