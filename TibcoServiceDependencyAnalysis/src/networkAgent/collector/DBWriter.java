package networkAgent.collector;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.regex.PatternSyntaxException;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.WriteConcern;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.DeleteResult;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.readersWriters.ServiceEntry;

public class DBWriter {

	private static DBWriter instance=null;
	private boolean writeRecords=false;
	
	private MongoWriteThread initialWriter;
	
	private ThreadGroup DBThreads;
	
	private int counter=1;
	
	
	 private static int batchSize=100;

	
	public static int getBatchSize() {
		return batchSize;
	}

	public static void setBatchSize(int batchSize) {
		if(batchSize>0)DBWriter.batchSize = batchSize;
	}

	private DBWriter(){
		DBThreads=new ThreadGroup("DBWriters");
		//DBThreads.setDaemon(true);
		
		initialWriter=new MongoWriteThread(DBThreads);
		//Thread a = new Thread(DBThreads,initialWriter);
		initialWriter.start();
	}
	
	public static DBWriter getDBWriter(){
		if(instance==null) instance=new DBWriter();
		return instance;
	}
	
	public static void shutdown(){
		if(instance==null)return;
		
		if(instance.DBThreads==null) return;
		
		if(instance.DBThreads.isDestroyed()) return;
		instance.unsetWrite();
		instance.DBThreads.interrupt();
	}
	
	public void setWrite(){
		writeRecords=true;
	}
	
	public void unsetWrite(){
		writeRecords=false;
	}
	
	public boolean isWrite()
	{
		return writeRecords;
	}
	
	public void writeRecords(boolean b){
		writeRecords=b;
	}
	
	
	private boolean tempOff=false;
	private int poolThresholdPercent=80;
	public int maxThreads=50;
	public int newThreadThreshold=1000;
	public int maxQueueDepth=1000000;
	
	private ArrayBlockingQueue<ServiceEntry> queue=new ArrayBlockingQueue<ServiceEntry>(maxQueueDepth,false);

	
	public void writeMongo(ServiceEntry se){
		if(!writeRecords || tempOff)return;
		boolean t=queue.offer(se);
		int size=queue.size();
		if(size>950)Collector.logger.info("queue size now "+size+ " pool size now "+DBThreads.activeCount());
		if(!t) {
			Collector.logger.severe("db queue is full turning off write");
			tempOff=true;
		}
		//int c=DBThreads.activeCount();
		
		
		
		if(writerCount<maxThreads & size>newThreadThreshold || writerCount<1){
			//start a new writer
			Collector.logger.info("pool size now "+size +". Starting a new writer to bring number to "+writerCount+1);
			MongoWriteThread mt = new MongoWriteThread(DBThreads);
			//if(mt==null)System.err.println("this cannot be null");
			mt.start();
			newThreadThreshold+=100;
		}
	}
	
	
	
	private void writeToMongo(ServiceEntry se){
		if(se==null)return;
		//System.out.println(""+writeRecords+"-"+Config.db);
		if(se.when==null) return;//only interested in timing events atm
		if(Config.db==null) return;//no database connection
		if(!writeRecords) return; // writing is turned off
		//try {
			MongoCollection<Document> collection = Config.db.getCollection("timings");
		
		
			Document doc = new Document("service", se.service.getServiceName())
								.append("operation", se.service.getOperationName())
								.append("layer", se.service.getType())
								.append("time", se.when)
								.append("response", se.response)
								.append("wait", se.wait)
								.append("filename", se.filename)
								.append("location", se.location)
								.append("transaction", se.txn)
								.append("user", se.user)
								.append("login", se.loginEvent)
								.append("logout", se.logoutEvent);
			collection.withWriteConcern(WriteConcern.UNACKNOWLEDGED);
			collection.insertOne(doc);
			if(Collector.logger.isLoggable(Level.FINEST))Collector.logger.finest("collection has "+getTimingCollectionCount()+" records");
			//testQuery();
		
		//}catch (Exception e){
		//	Collector.logger.log(Level.SEVERE, "error writing to mongo", e);
		//	
		//}
		
		
	}
	
	private long getTimingCollectionCount(){
		
		if(Config.db==null) return -1;//no database connection
		MongoCollection<Document> collection = Config.db.getCollection("timings");
		return collection.count();
		
		
	}
	
	public void queryTimings(Date from, Date to, DBQueryCallBack callback, String layerFilter, String serviceFilter, String operationFilter, String userFilter, final double rate){
		final DBQueryCallBack lcallback=callback;
		if(callback==null) return;
		if(rate<=0){
			lcallback.message("Rate must be greater than zero");
			return;
		}
		if(Config.db==null) {
			lcallback.message("No database connection or configured");
			return;
		}
		
		final MongoCollection<Document> collection = Config.db.getCollection("timings");
		
		if(collection==null){
			lcallback.message("No timings available");
			return;
		}
		
		//get total number of records
		
		final List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
		obj.add(new BasicDBObject("time",new BasicDBObject("$gt", from).append("$lte", to) ));
		if(layerFilter!=null)
			obj.add(new BasicDBObject("layer", java.util.regex.Pattern.compile(layerFilter)));
		if(serviceFilter!=null)
			obj.add(new BasicDBObject("service", java.util.regex.Pattern.compile(serviceFilter)));
		if(operationFilter!=null)
			obj.add(new BasicDBObject("operation", java.util.regex.Pattern.compile(operationFilter)));
		if(userFilter!=null)
			obj.add(new BasicDBObject("user", java.util.regex.Pattern.compile(userFilter)));
		BasicDBObject query;
		if(obj.size()>1){
			query=new BasicDBObject();
			query.put("$and", obj);
		}
		else{
			query=obj.get(0);
		}
		FindIterable<Document> cursor = collection.find(query).sort(new BasicDBObject("time",1));    	    
	    if(cursor==null){
	    	lcallback.message("No records returned for dates ["+from+", "+to+"]");
	    	return;
	    }

	    //long total = 0;
	    final long total= collection.count(query);
	    if(total==0){
	    	String filterMessage="";
	    	if(layerFilter!=null | serviceFilter!=null | operationFilter!=null| userFilter!=null){
	    		filterMessage=" with filter:";
	    		if(layerFilter!=null) filterMessage+=" layer="+layerFilter;
	    		if(serviceFilter!=null) filterMessage+=" service="+serviceFilter;
	    		if(operationFilter!=null) filterMessage+=" operation="+operationFilter;
	    		if(userFilter!=null) filterMessage+=" user="+userFilter;
	    	}
	    	lcallback.message("No entries returned for dates ["+from+", "+to+"]"+filterMessage);
	    	return;

	    }
	    //lcallback.count(total,Thread.currentThread());
	    
	    //now select the records at the correct slow rate
	    final long startTime=from.getTime();
	    final long endTime=to.getTime();
	    final long realStartTime=System.currentTimeMillis();
	    
		
	    //calculate the next selection
	    (new Thread("rated query"){
	    	public void run(){
	    		lcallback.count(total,Thread.currentThread());
	    		long lastQueriedStart=startTime;
	    		int percent=0;
	    		int lastpercent=0;
	    		while(lastQueriedStart<endTime ){
	    			try {
	    				if(!lcallback.sendMore()){
	    					lastQueriedStart=endTime;
	    					Collector.logger.info("query stopped");
	    					lcallback.queryComplete(this);
	    					break;
	    				}else {
	    			
	    					Thread.sleep(200l);
	    					//calculate the current end time
	    					long endQuery=(long)((System.currentTimeMillis()-realStartTime)*rate)+startTime;
	    					if(endQuery>endTime)endQuery=endTime;
	    					//Collector.logger.info("("+startTime+","+endTime+"):("+lastQueriedStart+","+endQuery+")");
	    					obj.set(0,new BasicDBObject("time",new BasicDBObject("$gt", new Date(lastQueriedStart)).append("$lte", new Date(endQuery)) ));
	    					BasicDBObject query2;
	    					if(obj.size()>1){
	    						query2 = new BasicDBObject();
	    						query2.put("$and", obj);
	    					}
	    					else{
	    						query2 = obj.get(0);
	    					}
	    					FindIterable<Document> cursor2 = collection.find(query2).sort(new BasicDBObject("time",1)); 
	    					(new SendResults(lcallback,cursor2,0)).noTracking().run(); //purposely not starting a new thread
	    					lastQueriedStart=endQuery;
	    					percent=(int)((float)(endQuery-startTime)*100f/(float)(endTime-startTime));
	    					if(percent>lastpercent){
	    						lastpercent=percent;
	    						lcallback.message(""+percent+"%");
	    					}
	    				}
	    			} catch (InterruptedException e) {
	    				// TODO Auto-generated catch block
	    				Collector.logger.log(Level.SEVERE, "Thread interrupted ending queries", e);
	    				lastQueriedStart=endTime;
	    			}
	    	
	    		}
	    		lcallback.queryComplete(this);
	    	}
	    }).start();
	}

	public void queryTimings(Date from, Date to, DBQueryCallBack callback, String layerFilter, String serviceFilter, String operationFilter, String userFilter){
		if(callback==null) return;
		if(Config.db==null) {
			callback.message("No database connection or configured");
			return;
		}
		
		MongoCollection<Document> collection = Config.db.getCollection("timings");
		
		if(collection==null){
			callback.message("No timings available");
			return;
		}
		String field="layer"; //used to tell message the failed regex
		try {
			//Date gtDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse("2015-04-04T10:05:21.968");
			//Date lteDate = new Date();//SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse("2015-04-04T17:29:43.031");

			List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
			obj.add(new BasicDBObject("time",new BasicDBObject("$gt", from).append("$lte", to) ));
			
			if(layerFilter!=null)
				obj.add(new BasicDBObject("layer", java.util.regex.Pattern.compile(layerFilter)));
			field="service";
			if(serviceFilter!=null)
				obj.add(new BasicDBObject("service", java.util.regex.Pattern.compile(serviceFilter)));
			field="operation";
			if(operationFilter!=null)
				obj.add(new BasicDBObject("operation", java.util.regex.Pattern.compile(operationFilter)));
			field="user";
			if(userFilter!=null)
				obj.add(new BasicDBObject("user", java.util.regex.Pattern.compile(userFilter)));

			BasicDBObject query;
			if(obj.size()>1){
        	
				query=new BasicDBObject();
				query.put("$and", obj);
			}
			else{
				query=obj.get(0);
				//BasicDBObject dateQueryObj = new BasicDBObject("time",  new BasicDBObject("$gt", from).append("$lte", to));
			}
		    FindIterable<Document> cursor = collection.find(query);//.sort(new BasicDBObject("time",1));    
		    
		    if(cursor==null){
		    	callback.message("No cursor returned for dates ["+from+", "+to+"]");
		    	return;
		    }
		    
		    //int count=0;
		    long total = 0;
		    
		    total= collection.count(query);
		    if(total==0){
		    	String filterMessage="";
		    	if(layerFilter!=null | serviceFilter!=null | operationFilter!=null){
		    		filterMessage=" with filter:";
		    		if(layerFilter!=null) filterMessage+=" layer="+layerFilter;
		    		if(serviceFilter!=null) filterMessage+=" service="+serviceFilter;
		    		if(operationFilter!=null) filterMessage+=" operation="+operationFilter;
		    		if(userFilter!=null) filterMessage+=" user="+userFilter;
		    	}
		    	callback.message("No entries returned for dates ["+from+", "+to+"]"+filterMessage);
		    	return;

		    }
		    
		    
		    //for(Document d:cursor){
		    //	ServiceEntry se=processDocument(d);
		    //	if(se!=null){
		    		//send back the records here
		    //		callback.next(se, ++count);
		    //	}
		    //	System.out.println(d.toJson());
		    //}
		    (new SendResults(callback,cursor,total)).start();
		    

		} catch (PatternSyntaxException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			callback.message("invalid regex expression for "+field);
		}
	    
	}
	
	private class SendResults extends Thread {
		
		 int count=0;
		 DBQueryCallBack callback;
		 FindIterable<Document> cursor;
		 long total=0;
		 boolean track=true;
		 public SendResults(DBQueryCallBack callback,FindIterable<Document> cursor,long total)
		 {
			 this.callback=callback;
			 this.cursor=cursor;
			 this.total=total;
			 //callback.message("total is "+total);
		 }
		 
		 public SendResults noTracking(){
			 track=false;
			 return this;
		 }
		 
		 
		 public void run(){
			 if(track)callback.message("Starting to process records");
			 callback.count(total,Thread.currentThread());
			 //callback.message("debug "+total+" "+Thread.currentThread().getName());
			 ArrayList<ServiceEntry> group=new ArrayList<ServiceEntry>();
			 for(Document d:cursor){
				 
				 if(callback.sendMore()){
					 if(!callback.sendPaused()){
						 ServiceEntry se=processDocument(d);
						 if(se!=null){
							 //group.add(se);
							 ++count;
							 if(batchSize==1){
								 callback.next(se, count,Thread.currentThread());
							 }else{
			    			
								 group.add(se);
								 if(group.size()>=batchSize){  //group them up to reduce network overhead
									 //send back the records here
									 //Collector.logger.info("batched records ready to send");
									 callback.next(new ServiceEntry(group), count,Thread.currentThread());
									 group=new ArrayList<ServiceEntry>();
								 }
							 }
							 if(!track)count=0;
						 }
					 }
					 else {
						 try {
							Thread.sleep(1000l);
							callback.message("Paused");
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							//e.printStackTrace();
							
						}
					 }
				 }else {
					 callback.queryComplete(this);
					 break;
				 }
			 }
			 if(group.size()>0)callback.next(new ServiceEntry(group), count,Thread.currentThread());
			 if(track)callback.message("All records processed");
			 callback.queryComplete(Thread.currentThread());
		 }
	}

	private ServiceEntry processDocument(Document d){
		String servicename=d.getString("service");
	    String operation=d.getString("operation");
	    String layer=d.getString("layer");
	    
	    Date when=d.getDate("time");
	    Double response=d.getDouble("response");
	    
	    //check for a reasonable subset of data
	    if(servicename==null || operation==null || layer==null || when==null || response==null ) return null;
	    //if(response<0) response=0D; //lets have a zero bound
	    
	    Double wait=d.getDouble("wait");
	    String filename=d.getString("filename");
	    String location=d.getString("location");
	    String txn=d.getString("transaction");
		String user=d.getString("user");
		Boolean in=d.getBoolean("login",false);
		Boolean out=d.getBoolean("logout", false);
		ServiceEntry se=new ServiceEntry(new Service(layer,servicename,operation,""),when,response);
		if(wait!=null)se.wait=wait;
		if(filename!=null)se.filename=filename;
		if(location!=null)se.location=location;
		if(txn!=null)se.txn=txn;
		if(user!=null)se.user=user;
		se.loginEvent=in;
		se.logoutEvent=out;
		
		return se;
	}
	
	public void purgeRecords(long time)
	{
		(new MongoPurgeThread(time)).start();
	}
	
	private class MongoPurgeThread extends Thread{
		public MongoPurgeThread(long time){
			this.time=time;
			this.setDaemon(true);
			this.setName("Mongo Purge");
		}
		private long time;
		
		public void run(){
			
			MongoCollection<Document> collection = Config.db.getCollection("timings");
			
			if(collection==null){
				Collector.logger.info("No timings to purge");
				return;
			}
			BasicDBObject dateQueryObj = new BasicDBObject("time",  new BasicDBObject("$lt", new Date(time)));
			//FindIterable<Document> cursor = collection.find(dateQueryObj);    
		    
		    //if(cursor==null){
		    //	Collector.logger.info("No cursor returned for dates" );
		    //	return;
		    //}
		    long total = 0;
		    
		    total= collection.count(dateQueryObj);
		    if(total==0){
		    	Collector.logger.info("No records to purge");
		    	return;
		    }
		    DeleteResult res = collection.deleteMany(dateQueryObj);
		    Collector.logger.info("Deleted "+res.getDeletedCount()+" from expected "+total);
		}
	}
	
	private static int writerCount=0;
	
	private class MongoWriteThread extends Thread{
	
		
		
		public MongoWriteThread(ThreadGroup g){
			super(g,"MongoWriter-"+counter++);
			group=g;
			
		}
		
		ThreadGroup group;
		boolean runMe=true;
		int count=0;
		Random random=new Random();
		
		public void run(){
			
			int lifespan=2000+random.nextInt(5000);
			writerCount++;
			//this.setDaemon(true);
			//Thread.currentThread().setName("MongoWriter");
			Collector.logger.info("starting db writer thread "+Thread.currentThread().getName());
			
			while(runMe){
				try {
					ServiceEntry se = queue.poll(random.nextInt(9000)+1000,TimeUnit.MILLISECONDS);
					if(se==null ){ 
						if( writerCount>1){
						//if(Thread.currentThread().getThreadGroup()==group)
							System.out.println("Ending writer as nothing to write and more than one writer available "+Thread.currentThread().getName());
							 //the first created is never ended
							runMe=false;
						}
					}else {
						count++;
						try{
							writeToMongo(se);
						} catch (Exception e){
							Collector.logger.log(Level.SEVERE, "error writing to mongo", e);
							runMe=false;
						}
						
					}
					int size=queue.size();
					
					if(tempOff && size*100/maxQueueDepth<poolThresholdPercent) {
						Collector.logger.severe("DB Write turned back on");
						tempOff=false;
					}
					
					
					
					if(count>lifespan & writerCount>1 | count>lifespan+5000){
						Collector.logger.info("End of  life for "+Thread.currentThread().getName()+" having processed "+count+" records");

						runMe=false; //max number to process
					}
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					runMe=false;
					Collector.logger.info("caught interrupt on "+Thread.currentThread().getName());
				}
				
			}
			writerCount--;
			Collector.logger.info("stopping db writer thread "+Thread.currentThread().getName());
			newThreadThreshold-=100; 
			if(newThreadThreshold<1000 || newThreadThreshold>maxQueueDepth)newThreadThreshold=1000;
		}
	}
}
