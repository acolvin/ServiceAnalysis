package networkAgent.collector;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Cipher;

import networkAgent.collector.Config.Reconnection;

class UserConnection {
	Integer port=null;
	String user=null;
	boolean authenticated=false;
	public Collector.TCPReceiver receiver=null;
	
	public PublicKey pubk;
	public PrivateKey prvk;
	
	
	public static boolean authenticate=false;
	
	KeyPairGenerator kpg;
	
	char[] base64PublicKey;
	private final Logger logger=Collector.logger;
	
	private Config config;
	
	final String xform = "RSA/ECB/PKCS1PADDING";
	
	public UserConnection(Config config) {
		//collector=myCollector;
	    // Generate a key-pair
	    this.config=config;
		try {
			kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(512); // 512 is the keysize.
		    KeyPair kp = kpg.generateKeyPair();
		    pubk = kp.getPublic();
		    prvk = kp.getPrivate();
		    base64PublicKey = Base64Coder.encode(pubk.getEncoded());
		    //System.out.println(pubk.toString());
		    //System.out.println(base64PublicKey.toString());
		} catch (NoSuchAlgorithmException e) {
			Collector.logger.log(Level.SEVERE, "RSA Algorithm Not Found", e);
			System.exit(-2);
		}
	    
	}

	public byte[] encrypt(byte[] inpBytes) throws Exception {
	    Cipher cipher = Cipher.getInstance(xform);
	    cipher.init(Cipher.ENCRYPT_MODE, pubk);
	    return cipher.doFinal(inpBytes);
	}
	
	public byte[] decrypt(byte[] inpBytes) throws Exception{
		    Cipher cipher = Cipher.getInstance(xform);
		    cipher.init(Cipher.DECRYPT_MODE, prvk);
		    return cipher.doFinal(inpBytes);
	}
	
	public void disconnect(){
		if(receiver==null)return;
		receiver.interrupt();
	}
	
	public boolean validatePassword(String password){
		String p=Config.getHashedPassword(user);
		if(p==null)return false;
		if(p.startsWith("Hashed:")){
			String pp=p.substring(7);
			try {
				return PasswordHash.validatePassword(password, pp);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				return false;
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				return false;
			}
		}
		else return p.equals(password);
		
	}
	
	public boolean authenticateUser(String password)
	{
		if(password==null )logger.info("no password sent for user "+user);
		//System.out.println("authenticating user "+user);
		//System.out.println("encoded password="+password);
		String p=Config.getHashedPassword(user);
		if(p==null)return false;
		// add password checks here
		try {
			byte[] s=Base64Coder.decode(password);
			//System.out.println("encrypted password length="+s.length);
			byte[] pass=decrypt(s);
			
			//System.out.println(pass.length);
			String ss=new String(pass);
			//System.out.println(PasswordHash.createHash(ss));
			
			//System.out.println("password="+s);
			//System.out.println("passwords match "+p.equals(ss));
			if(p.startsWith("Hashed:")){
				String pp=p.substring(7);
				return PasswordHash.validatePassword(ss, pp);
			}
			else return p.equals(ss);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		
		
		
	}
	
	public boolean chpasswd(String passwordBundle) throws Exception{
		byte[] b=Base64Coder.decode(passwordBundle);
		byte[] pass=decrypt(b);
		String ss=new String(pass);
		//System.out.println(ss);
		String s[]=ss.split("::");
		String originalPassword=s[0];
		String newPassword=s[1];
		String currentPassword=Config.getHashedPassword(user);
		if(currentPassword==null || currentPassword.length()<8) return false;
		currentPassword=currentPassword.substring(7);
		if(PasswordHash.validatePassword(originalPassword, currentPassword))
		{
			return config.changeUserPassword(user, newPassword);
		} else return false; //wrong password
		
		
		
		//return true;
	}
	private ArrayList<String> sequences=new ArrayList<String>();
	
	public boolean isReplay(String sequence){
		boolean t= sequences.contains(sequence);
		if(!t)sequences.add(sequence);
		return t;
	}
	
	public String getDecrypted(String encryptedMessage){
		
		if(encryptedMessage==null || encryptedMessage.equals("")) return null;
		byte[] b=Base64Coder.decode(encryptedMessage);
		byte[] pass;
		try {
			pass = decrypt(b);
			String ss=new String(pass);
			return ss;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "unable to decypt", e);
		}
		
		return null;
	}
	public String resetPassword(String user,String message) throws Exception{
		byte[] b=Base64Coder.decode(message);
		byte[] pass=decrypt(b);
		String ss=new String(pass);
		//System.out.println(ss);
		String part[]=ss.split("::",3);
		//System.out.println("id="+part[0]);
		//System.out.println("admin password="+part[1]);
		//System.out.println("new password="+part[2]);
		if(sequences.contains(part[0])){
			logger.warning("user reset message replay!");
			return "Unable to change password"; // check for message replay
		}
		sequences.add(part[0]);
		if(!Config.isAdmin(this.user)) {
			logger.warning("non-admin user "+user+" trying to reset another users password");
			return "You are not an administrator"; //check i am an admin user
		}
		
		String p=Config.getHashedPassword(this.user);
		if(p.startsWith("Hashed:"))
			p=p.substring(7);
		
		if(!PasswordHash.validatePassword(part[1], p)){
			logger.warning("incorrect password sent for user "+this.user+ " during password reset");
			return "Unable to change password"; //wrong password sent
		}
		
		//ready to set users passwd
		
		
		return config.setUserPassword(user, part[2]);
	}
	
}