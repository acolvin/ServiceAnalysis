package networkAgent.collector;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import networkAgent.agentTCP.TCPMessage;
import networkAgent.collector.Config.Result;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.HiddenFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import serviceAnalysisModel.SAControl.StdOutHandler;
import serviceAnalysisModel.readersWriters.ExtensionHandler;
import serviceAnalysisModel.readersWriters.LogFileReader;
import serviceAnalysisModel.readersWriters.LogFileTailerListener;
import serviceAnalysisModel.readersWriters.ServiceEntry;

public class Collector  {
	
	private static String location;
	//private FileAlterationMonitor monitor;
//	private ConcurrentHashMap<DirectorySettings,FileAlterationMonitor> monitorsSettings=new ConcurrentHashMap<DirectorySettings,FileAlterationMonitor>();
	private CopyOnWriteArrayList<FileAlterationMonitor> monitors=new CopyOnWriteArrayList<FileAlterationMonitor>();
	private File directory;
	private String suffix;
	private int port;
	private ServerSocket srvr;
	private SocketListener slistener;
	//private Socket socket;
	static final Logger logger=Logger.getLogger("Collector");
	//private FileProcessor processor=new FileProcessor();
	private CopyOnWriteArrayList<TCPSender> senders=new CopyOnWriteArrayList<TCPSender>();
	private CopyOnWriteArrayList<TCPReceiver> receivers=new CopyOnWriteArrayList<TCPReceiver>();
	private CopyOnWriteArrayList<ListenerTCP> tcpconnectors=new CopyOnWriteArrayList<ListenerTCP>();
	public boolean stopping=false;
	private Poller poller;
	private ArrayList<UserConnection> userConnections=new ArrayList<UserConnection>();
	
/*	private class DirectorySettings {
		public String path=null;
		public String suffix=null;
		public File directory=null;
		public String absPath=null;
		
		public DirectorySettings(String dirPath, String suffix)
		{
			path=dirPath;
			this.suffix=suffix;
			if(path!=null)
			{
				directory=new File(path);
				try {
					absPath=directory.getCanonicalPath();
				} catch (IOException e) {

				}
			}
		}
		
		public boolean contains(DirectorySettings ds)
		{
			
			boolean val=false;
			if(suffix==null | path==null | ds.path==null | ds.suffix==null ) {
				logger.finest("null in settings");
				return false;
			}
			if(suffix.equals(ds.suffix) )
			{
				logger.finest("suffixes are equal, "+suffix);
				if(!(absPath==null | ds.absPath==null))
				{
					if(ds.absPath.startsWith(absPath)) {
						logger.finest(absPath+" is contained in "+ds.absPath);
						val = true;
					}
				}
			}
			
			return val;
		}
		
	}
	
	*/
	
	protected Config config;
	
	public Collector(Config config) {
		config.collector=this;
		if(logger.isLoggable(Level.INFO)) {
			//logger.entering("Collector", "Constructor");
			logger.info(config.port+", " +logger.getLevel().getName());
		}
		this.config=config;
		try {
			
			//this.directory = new File(directory);
			//this.suffix=suffix;
			this.port=config.port;
			openTCPPort();
			
			
			config.connectMongo();
			
			startPoller();
			for(Connection c:config.connections){
				try {
					ListenerTCP l=new ListenerTCP(this,c);
					c.listener=l;
					l.start();
					tcpconnectors.add(l);
					
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					logger.warning("unknown host, "+c.host);
					c.connected=false;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					c.connected=false;
					logger.warning("Unable to connect to "+c.host+":"+c.port);
					//e.printStackTrace();
					logger.log(Level.WARNING,"Unable to connect to "+c.host+":"+c.port,e);
					
				}
			}
			
			config.monitor.start();
			
		} catch (NumberFormatException e) {
			logger.severe(e.getMessage());
	    	logger.log(Level.SEVERE, e.getMessage(), e);
	    	System.exit(-2);
		}
	    	

	}
	
	void startConnection(Connection c){
		try {
			ListenerTCP l=new ListenerTCP(this,c);
			c.connected=true;
			c.listener=l;
			l.start();
			
			tcpconnectors.add(l);
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.warning("unknown host, "+c.host);
			c.connected=false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			c.connected=false;
			logger.warning("Unable to connect to "+c.host+":"+c.port);
			//e.printStackTrace();
			//logger.log(Level.WARNING,"Unable to connect to "+c.host+":"+c.port,e);
			
		}
	}
	
	private void startPoller()
	{
		poller=new Poller();
		poller.start();
	}

	public static void main(String[] args)
	{
		logger.setUseParentHandlers(false);
		for(Handler h:logger.getHandlers())
			logger.removeHandler(h);
		Handler consoleHandler = new StdOutHandler();//new ConsoleHandler();
		consoleHandler.setLevel(Level.FINEST);
		logger.addHandler(consoleHandler);
		logger.setLevel(Level.SEVERE);
		
		Config config=new Config();
		
		if(args.length>=1 && args[0].equals("-debug"))
			logger.setLevel(Level.FINEST);
		else if(args.length>=1 && args[0].equals("-info"))
			logger.setLevel(Level.INFO);
		else if(args.length>=1 && args[0].equals("-trace"))
			logger.setLevel(Level.FINE);
		else config.handleInputFile(args[0]);
		
		if(args.length>=2 && args[1].equals("-debug"))
			logger.setLevel(Level.FINEST);
		else if(args.length>=2 && args[1].equals("-info"))
			logger.setLevel(Level.INFO);
		else if(args.length>=2 && args[1].equals("-trace"))
			logger.setLevel(Level.FINE);
		else if(args.length>=2 )config.handleInputFile(args[1]);
		
		// TODO Auto-generated method stub
		if(args.length>2)
		{
			logger.severe("usage: [configFile] [<-debug|-info|-trace>]");
			System.err.println("usage: [configFile] [<-debug|-info|-trace>]");
			System.exit(-1);
		}
		
		
		try
		{
			location=InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e)
		{
			//e.printStackTrace();
			location=e.getMessage().substring(e.getMessage().length()/2+1);
			//location=null;
		}
		
		Collector da=new Collector(config);
		//Collector da=new Collector(args[0],args[1],args[2]);
		logger.info("Collector Agent Started");
		//System.out.println("Directory Monitor Started");
	}
	

	
	private void openTCPPort()
	{
		if(logger.isLoggable(Level.FINEST)) logger.finest("Entering openTCPPort");
		try {
	           srvr = new ServerSocket(port);
	           srvr.setSoTimeout(10000);
	           slistener=new SocketListener(srvr);
	           logger.finest("Start Socket Listener");
	           slistener.start();
	        }
	        catch (IOException e) {
	        	logger.log(Level.SEVERE, e.getMessage(), e);
	           
	        }   
		
		if(logger.isLoggable(Level.FINEST)) logger.finest("Exiting openTCPPort");
	}
	
	public void addToQueue(ServiceEntry nse){
		
		try{
			trace("Received",nse);
			DBWriter w = DBWriter.getDBWriter();
			w.writeMongo(nse);
			boolean flag;
			if(!senders.isEmpty()){
				do {
					
						flag = queue.offer(nse,10,TimeUnit.SECONDS);
						
					
				} while (!flag);
				logger.finest("Service Entry Added to queue");
			}else{
				cleanTrace(nse);
				logger.finest("Drop Service Entry as there are no listeners");
			}
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.log(Level.SEVERE,"Interruption whilst adding message to queue",e);
			
		}
	}
	
	public void closeUnknownUsers(){
		Set<String> users = config.users.keySet();
		for(UserConnection uc:userConnections){
			System.out.println("check disconnect for user "+uc.user);
			String user=uc.user;
			if(!users.contains(user)) {
				logger.info("Disconnect user "+user+" on port "+uc.port);
				uc.disconnect();
			}
			
		}
		
	}
	
	private class SocketListener extends Thread
	{
		private ServerSocket server;
		private boolean wait=true;

		public SocketListener(ServerSocket srvr)
		{
			this.setDaemon(false);
			server=srvr;
			
		}
		
		public void interrupt()
		{
			logger.info("socket listener interrupted");
			wait=false;
			super.interrupt();
		}
		
		public void run()
		{
			logger.info("Waiting for client connection");
			
			while(wait)
				try {
					
					Socket socket = server.accept();
					logger.info("Client Connected: remote port"+socket.getPort());
					
					// start the sender
					TCPSender sender = new TCPSender(socket);
					sender.start();
					
					//start the control line from client
					TCPReceiver receiver=new TCPReceiver(socket,sender);
					receiver.start();
					
					UserConnection uc=new UserConnection(config);
					sender.user=uc;
					receiver.user=uc;
					uc.port=socket.getPort();
					userConnections.add(uc);
					uc.receiver=receiver;
					
					
					try {
						
						logger.info("Tell client we are a collector");
						sender.addToQueue("collector");
					
						if(UserConnection.authenticate)
						{
						
							
							
							
							logger.info("Send start authenticate to client "+socket.getPort());
							sender.addToQueue("start authenticate "+new String(uc.base64PublicKey));
						}
						
							
					} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
					}
					
					
					senders.add(sender);
					receivers.add(receiver);
					
					
				}catch (SocketTimeoutException e) {
					//catching this one to do nothing 
					//logger.info(logger.getLevel().getName());
					logger.finest("Timed out waiting for client connect");
				} 
				catch (IOException e) {
					logger.log(Level.SEVERE, e.getMessage(), e);
					interrupt();
				}
			logger.info("Connection Thread Ended");
		}
	}
	

	
	
	public void trace(String message,TCPMessage t)
	{
		ServiceEntry nse = t.se;
		Integer number=t.txn;
		if(nse!=null)logger.fine("<Trace> <"+number+"> <"+message+"> "+nse.location+" "+nse.filename+" "+nse.txn);
		else logger.fine("<Trace> <"+number+"> <"+message+">");
	}
	
	public void trace(String message,ServiceEntry nse)
	{
		if(!logger.isLoggable(Level.FINE))	return;
		//System.out.println("doing trace");
		Integer number=tracenumber.get(nse);
		if(number==null){
			number=txn++;
			tracenumber.put(nse, number);
		}
		logger.fine("<Trace> <"+number+"> <"+message+"> "+nse.location+" "+nse.filename+" "+nse.txn);
		
	}
	
	public void cleanTrace(ServiceEntry nse){
		tracenumber.remove(nse);
	}
	
	private Integer getTraceNumber(ServiceEntry nse)
	{
		return tracenumber.get(nse);
	}
	private ConcurrentHashMap<ServiceEntry,Integer> tracenumber=new ConcurrentHashMap<ServiceEntry,Integer>();
	private int txn=0;
	
	
	protected class TCPSender extends Thread
	{
		private ArrayBlockingQueue<TCPMessage> senderQueue=new ArrayBlockingQueue<TCPMessage>(10,true);
		private Socket senderSocket;
		private ObjectOutputStream objectOutput=null;
		public boolean alive=true;
		public boolean sendLive=true;
		
		UserConnection user=null;
		
		public TCPSender(Socket socket)
		{
			senderSocket=socket;
			try {
				objectOutput = new ObjectOutputStream(socket.getOutputStream());
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, e.getMessage(), e);
				run=false;
			}

			
		}
		
		public void interrupt()
		{
			logger.finest("Interrupt called");
			run=false;
			alive=false;
			super.interrupt();
			if(objectOutput!=null)
				try {
					objectOutput.close();
				} catch (IOException e) {
					logger.log(Level.SEVERE, e.getMessage(), e);
				}
		}
		
		public boolean addToQueue(ServiceEntry se) throws InterruptedException
		{
			logger.finest("addToQueue for TCPSender "+this+" se "+se);
			if(!alive){
				logger.finest("Received message but this sender is not alive "+this); 
				return false;
			}
			if(UserConnection.authenticate && !user.authenticated){
				logger.finest("Received message but this sender needs authenticating "+this); 
				return false;
			}
			
			if(!sendLive){
				logger.finest("Dropping Live Message from this client as they are turned off");
				cleanTrace(se);
				return true;
			}

			TCPMessage m=new TCPMessage(se);
			m.txn=getTraceNumber(se);
			cleanTrace(se);
			return senderQueue.offer(m,10,TimeUnit.SECONDS);
		}
		
		public boolean addToQueueIgnoreLive(ServiceEntry se) throws InterruptedException
		{
			logger.finest("addToQueue for TCPSender "+this+" se "+se);
			if(!alive){
				logger.finest("Received message but this sender is not alive "+this); 
				return false;
			}
			if(UserConnection.authenticate && !user.authenticated){
				logger.finest("Received message but this sender needs authenticating "+this); 
				return false;
			}
			
			

			TCPMessage m=new TCPMessage(se);
			m.txn=getTraceNumber(se);
			cleanTrace(se);
			return senderQueue.offer(m,10,TimeUnit.SECONDS);
		}
		
		public boolean addToQueue(String string) throws InterruptedException
		{
			TCPMessage m=new TCPMessage(string);
			
			if(UserConnection.authenticate && !user.authenticated){
				if(!(string.startsWith("start authenticate") || string.equalsIgnoreCase("authentication failed") || string.equals("collector"))) return false;
			}
			
			return senderQueue.offer(m,10,TimeUnit.SECONDS);
		}
		
		private boolean run = true;
		
		public void run()
		{
			if(objectOutput!=null)
			{
				while(run){
					try {
						TCPMessage m=senderQueue.poll(30, TimeUnit.SECONDS);
						if(m!=null)
						{
							logger.finest("event to send to port "+senderSocket.getPort()+" with data "+m.toString());
							
							objectOutput.writeObject(m); 
							objectOutput.flush();
							objectOutput.reset();//without this we run out of heap!!
							trace("Sent to "+senderSocket.getPort(), m);
							
							
						}
						else
						{
							logger.finest("Nothing to send so do a keep-alive to port "+senderSocket.getPort());
							m=new TCPMessage();
							objectOutput.writeObject(m); 
							objectOutput.flush();
							objectOutput.reset();
						}
					} catch (InterruptedException e) {
						run=false;
						user.authenticated=false;
					} catch (IOException e) {
						logger.log(Level.SEVERE, e.getMessage(), e);
						run=false;
						logger.severe("closing sender");
						user.authenticated=false;
					}
				}
				try {
					user.authenticated=false;
					objectOutput.close();
					
				} catch (IOException e) {
					//logger.log(Level.SEVERE, e.getMessage(), e);
					//already closed
				}
			}
			alive=false;
			user.authenticated=false;
		}
		
	}

	public void sendMessageToClients(String message)
	{
		
		try {
			for(TCPSender s:senders)
			{
				
				s.addToQueue(message);
			}
		} catch (InterruptedException e) {
			logger.info("Sending message interrupted");
		}
		
	}
	
	private ArrayBlockingQueue<ServiceEntry> queue=new ArrayBlockingQueue<ServiceEntry>(1000,true);
	
	private class Poller extends Thread
	{
		public Poller()
		{
			logger.finest("Poller created");
			setDaemon(true);
		}
		
		private boolean run=true;
		
		public void interrupt()
		{
			logger.info("poller interrupted, no more events will be sent!");
			run=false;
			super.interrupt();
		}
		
		public void run()
		{
			logger.finest("poller started");
			while(run)
			{
				try {
					logger.finest("waiting for message to appear");
					ServiceEntry se=queue.poll(10, TimeUnit.SECONDS);
					
					if(se!=null)
					{
						if(se.type==ServiceEntry.SE_TYPE_COMMAND){
							// drop commands 
							logger.info("Dropped Command Event received from file "+se.filename+" @ location "+se.location+"\n"+se.command+" "+se.args);
						}
						else {
							logger.finest("Got message from queue, "+se);
							logger.finest(""+se.type);
							for(TCPSender s:senders)
							{
								logger.finest("TCPSender in list "+s);
								boolean success=false;
								do {
									if(s.alive){
										logger.finest("TCPSender state is alive");
										success=s.addToQueue(se);
										if(!success)logger.finest("Timed out trying to add to TCPSender queue "+s.senderSocket.getPort());
										else logger.finest("Message added to TCPSender "+s.senderSocket.getPort());
									} else
									{
										logger.info("removing sender "+s.senderSocket.getPort());
										senders.remove(s);
										success=true;
									}
								
								}while(!success);
							}
							se=null;
						}
					}
				} catch (InterruptedException e) {
					logger.log(Level.SEVERE, e.getMessage(), e);
				}
			}
		}
	}
	 
	
	
	public class TCPReceiver  extends Thread implements DBQueryCallBack
	{
		public UserConnection user;
		private boolean run=true;
		private Socket socket;
		private TCPSender sender;		
		private BufferedReader reader;
		
		public boolean sendMore=true;
		public boolean sendPaused=false;
		
		public TCPReceiver(Socket socket,TCPSender sender)
		{
			setDaemon(true);	
			this.socket=socket;
			this.sender=sender;
			
			try {
				reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		 
				
			} catch (IOException e) {
				logger.severe("can't open TCP input stream for reading ");
				run=false;
			}
		}
		
		public void interrupt()
		{
			run=false;
			sender.interrupt();
			super.interrupt();
		}
		
		public void disconnect(){
			run=false;
			try {
				this.addToSenderQueue("quit");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}
		
		public void run()
		{
			while(run)
			{
				try {
					logger.finest("waiting for a command "+socket.getPort());
					String line = reader.readLine();
					logger.finest("line received: "+line);
					if(line==null){
						logger.info("Input stream EOF closing socket.  Has the client closed?");
						sender.interrupt();
						run=false;
						line="";
					}
					if(line.startsWith("quit")){
						logger.info("sender received quit command");
						setSendMore(false);
						sender.interrupt();
						run=false;
					}
					if(line.equals("Hello"))
					{
						logger.finest("Keep Alive Received");
					}
					if(line.toLowerCase().startsWith("stop")) {logger.finest("stop received");stopMonitor(line);}
					if(line.toLowerCase().startsWith("monitor")) {logger.finest("monitor received");monitorFile(line);}
					if(line.toLowerCase().startsWith("user")) {
						logger.finest("user received");
						boolean b=setUser(line);
						if(!b){
							logger.info("sender received quit command");
							sender.interrupt();
							run=false;
						}
					}
					if(line.toLowerCase().startsWith("chpasswd")){
						logger.info("change of password received");
						boolean t=chpasswd(line);
						if(t)addToSenderQueue("Password changed successfully");
						else addToSenderQueue("Password change unsuccessful");
					}
					if(line.toLowerCase().startsWith("resetpasswd")){
						logger.info("reset of password received: "+line);
						String t=resetPasswd(line);
						addToSenderQueue(t);
						
						
					}
					if(line.toLowerCase().startsWith("disableuser")){
						logger.info("disable of user received: "+line);
						disableUser(line);
						
						
					}
					if(line.toLowerCase().startsWith("adduser")){
						logger.info("request to add user received: "+line);
						addUser(line);
						
						
					}
					if(line.toLowerCase().startsWith("addadmin")){
						logger.info("request to add an admin user from "+user.user);
						setAdmin(line,true);
						
						
					}
					if(line.toLowerCase().startsWith("deladmin")){
						logger.info("request to delete an admin user from "+user.user);
						setAdmin(line,false);
						
						
					}
					if(line.toLowerCase().startsWith("read"))
					{
						logger.info("Read command received\n\t"+line);
						String split[] = line.split(" ");
						boolean directory=false;
						if(split.length==3  ){
							if(!split[1].startsWith("\"")){
								logger.finest("Read location is "+split[1]+", extension is "+split[2]);
								startReader(sender,split[1],split[2]);
								directory=true;
							}
							
						} else if(split.length>3 & split[split.length-2].endsWith("\""))
						{ //directory with spaces in directory name
							directory=true;
							String read=split[1].substring(1);
							for(int i=2;i<split.length-3;i++) 
								read=read+split[i];
							int i=split[split.length-2].length();
							read=read+split[split.length-2].substring(0, i);
							logger.finest("Read location is "+read+", extension is "+split[split.length-1]);
							startReader(sender,read,split[split.length-1]);
						}
						if(!directory & split.length>2 & !(split[split.length-1].endsWith("\"")&split[1].startsWith("\""))|split.length<2)
						{//error
							logger.severe("Incorrect arguments to Read Command received");
							logger.severe(line);
							
						}else if(!directory &split.length==2)
						{ //split[1] holds the file or directory
							startReader(sender,split[1],null);
							logger.finest("Read location is "+split[1]);
						}else if(!directory )
						{ //split holds string across several entries
							
							String read=split[1].substring(1);
							for(int i=2;i<split.length-2;i++) 
								read=read+split[i];
							int i=split[split.length-1].length();
							read=read+split[split.length-1].substring(0, i);
							logger.finest("Read location is "+read);
							startReader(sender,read,null);
						}
							
					}
					if(line.equalsIgnoreCase("live off")){
						sender.sendLive=false;
					}
					if(line.equalsIgnoreCase("live on")){
						sender.sendLive=true;
					}
					if(line.equalsIgnoreCase("select stop")){
						Collector.logger.info("select stop received");
						setSendMore(false);
					}
					if(line.equalsIgnoreCase("select pause")){
						Collector.logger.info("select pause received");
						setSendPaused(true);
					}
					if(line.equalsIgnoreCase("select unpause")){
						Collector.logger.info("select unpause received");
						setSendPaused(false);
					}
					if(line.toLowerCase().startsWith("select timings")){//select timings from to (dates in ms since equinox)
						String split[]=line.split(" ");
						if(split.length<3){
							sendMessage("select timings requires at least a start date");
							return;
						}
						int type=0;
						boolean doSlow=false;  //set to true to limit rate
						try{
							long sd=Long.parseLong(split[2]);
							type=1;
							Date start=new Date(sd);
							Date end;
							if(split.length==3)end=new Date();
							else {
								long ed=Long.parseLong(split[3]);
								end=new Date(ed);
							}
							type=2;
							String layerFilter=null;
							String serviceFilter=null;
							String operationFilter=null;
							double rate=1000000d;
							String userFilter=null;
							if(split.length>4){
								// filter parameters sent
								if(!split[4].equals(".*"))layerFilter=split[4];
							}
							if(split.length>5){
								if(!split[5].equals(".*"))serviceFilter=split[5];
							}
							if(split.length>6){
								if(!split[6].equals(".*"))operationFilter=split[6];
							}
							if(split.length>7){
								if(!split[7].equals(".*"))userFilter=split[7];
							}
							if(split.length>8){
								rate=Double.parseDouble(split[8]);
								doSlow=true;
								
							}
							
							
							setSendMore(true);
							if(doSlow)DBWriter.getDBWriter().queryTimings(start, end, this, layerFilter, serviceFilter, operationFilter,userFilter,rate);
							else DBWriter.getDBWriter().queryTimings(start, end, this, layerFilter, serviceFilter, operationFilter,userFilter);
						}catch (NumberFormatException e){
							logger.log(Level.INFO, "not a number", e);
							if(type==0)sendMessage("select timings start date invalid");
							else if(type==1)sendMessage("select timings end date invalid");
							else if(type==2)sendMessage("select timings rate invalid "+split[8]);
							return;
						}
						
					}
					
				} catch (SocketException e){ 
					if(run){
						run=false;
					
						user.authenticated=false;
						logger.log(Level.SEVERE, e.getMessage(), e);
					}
					
				} catch (IOException e) {
					logger.log(Level.SEVERE, e.getMessage(), e);
					user.authenticated=false;
					run=false;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					user.authenticated=false;
					run=false;
				} 
			}
			try {
				user.authenticated=false;
				reader.close();
			} catch (IOException e) {
				logger.log(Level.SEVERE, e.getMessage(), e);
			}
			if(sender.isAlive()&!sender.isInterrupted())sender.interrupt();
			user.authenticated=false;
		}

		private boolean chpasswd(String line){
			if(!UserConnection.authenticate){
				logger.info("Cannot change password when authentication is off");
				return false; //cannot change password if auth turned off
			}
			if(user.authenticated==false) {
				logger.info("cannot change password if you are not authenticated");
				return false; //cannot change password until logged in
			}
			String s[]=line.substring(9).split(",",2);
			if(!s[0].equalsIgnoreCase(user.user)) {
				logger.info("trying to change password for another user "+s[0]+" "+user.user);
				return false; //you are trying to change a user that isnt you
			}
			try {
				return user.chpasswd(s[1]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "unable to change password", e);
				return false;
			}
			
			//return true;
		}
		
		private String resetPasswd(String line){
			if(!UserConnection.authenticate){
				logger.info("Cannot change password when authentication is off");
				//return false; //cannot change password if auth turned off
				return "Cannot change password when authentication is off";
			}
			if(user.authenticated==false) {
				logger.info("cannot change password if you are not authenticated");
				//return false; //connot change password until logged in
				return "cannot change password if you are not authenticated";
			}
			logger.info(line.substring(12));
			String s[]=(line.substring(12)).split(",",2);
			logger.info("change password for user: "+s[0]);
			try {
				return user.resetPassword(s[0],s[1]);
				
				//logger.info("reset password body: "+user.decryptMessage(s[1]));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				
				logger.log(Level.SEVERE, "Message decryption failure", e);
			}
			
			return "Unable to change password" ;
		}
		
		private void disableUser(String line) {
			if(!UserConnection.authenticate) {
				sendMessage("Cannot disable user when authentication is off");
				return;
			}
			//command is disableuser=<user>,<unique id>,<encrypted/base64 with public key of unique id>
			String parts[]=line.substring(12).split(",",3);
			if(parts.length!=3) {
				logger.warning("Invalid command structure from "+user.user+", attempting admin command");
				
				//sendMessage("Invalid command");
				return;
			}
			// check user is an admin
			if(!Config.isAdmin(user.user)){
				logger.warning("non-admin user "+user.user+" attempting admin command");
				sendMessage("Failed to disable user");
				return;
			}
			// check part[1] and part[2] match
			
			String dec=user.getDecrypted(parts[2]);
			if(dec==null){
				logger.warning("incorrect admin message sent (admin="+user.user+")");
				sendMessage("Failed to disable user");
				return;
			}
			if(!dec.equals(parts[1])){
				logger.severe("Message ID not confirmed");
				sendMessage("Failed to disable user");
				return;
			}
			if(user.isReplay(parts[1])){
				logger.severe("Message ID indicates replay");
				sendMessage("Failed to disable user");
				return;
			}
			// all ok so do disable
			Result result = config.removeUser(parts[0]);
			if(result.success){
				//closeUnknownUsers();// called in removeUser
				sendMessage("user "+parts[0]+" disabled successfully");
			}
			else sendMessage(result.message);
		}
		
		private void setAdmin(String line,boolean add){ //addAdmin code,encrypted string
			//encrypted string=code::my password::user
			if(!UserConnection.authenticate) {
				sendMessage("Cannot set admin user when authentication is off");
				return;
			}
			if(!Config.isAdmin(user.user)){
				logger.warning("non-admin user "+user.user+" attempting admin command");
				sendMessage("Failed to set admin user - you are not an admin!");
				return;
			}
			int len=add? 9:9;
			String parts[]=line.substring(len).split(",",2);
			if(parts.length!=2){
				logger.warning("Invalid command structure from "+user.user+", attempting admin command");
			
				//sendMessage("Invalid command");
				return;
			}
			String dec=user.getDecrypted(parts[1]);
			if(dec==null){
				logger.warning("incorrect admin message sent (admin="+user.user+")");
				sendMessage("Failed to set admin user - password failure");
				return;
			}
			String eparts[]=dec.split("::",3);
			//System.out.println(dec);
			if(eparts.length!=3){
				logger.warning("Invalid command structure from "+user.user+", attempting admin command");
				
				//logger.warning(dec);
				//sendMessage("Invalid command");
				return;
			}
			if(!eparts[0].equals(parts[0])){//someone trying to spoof message
				logger.severe("Invalid command sent to create a user from account "+user.user);
				return;
			}
			if(user.isReplay(parts[0])){
				logger.severe("Message ID indicates replay");
				sendMessage("Failed to change admin status");
				return;
			}
			if(!Config.isAdmin(user.user)){
				logger.severe("non-admin user attempting an administration command");
				sendMessage("Failed to change admin status");
				return;
			}
			if(!user.validatePassword(eparts[1])){
				logger.severe("invalid admin password provided for user "+user.user);
				sendMessage("Failed to change admin status");
				return;
			}
			logger.info("ready to set admin user "+eparts[2]);
			Result result = config.setAdmin(eparts[2], add);
			if(result.success){
				//closeUnknownUsers();// called in removeUser
				sendMessage("admin change successful");
			}
			else sendMessage(result.message);
		}
		
		private void addUser(String line){ //adduser code,encrypted string
			//encrypted string=code::my password::user::passwd
			if(!UserConnection.authenticate) {
				sendMessage("Cannot create user when authentication is off");
				return;
			}
			if(!Config.isAdmin(user.user)){
				logger.warning("non-admin user "+user.user+" attempting admin command");
				sendMessage("Failed to create user - you are not an admin!");
				return;
			}
			String parts[]=line.substring(8).split(",",2);
			if(parts.length!=2){
				logger.warning("Invalid command structure from "+user.user+", attempting admin command");
			
				//sendMessage("Invalid command");
				return;
			}
			String dec=user.getDecrypted(parts[1]);
			if(dec==null){
				logger.warning("incorrect admin message sent (admin="+user.user+")");
				sendMessage("Failed to create user - password failure");
				return;
			}
			String eparts[]=dec.split("::",4);
			if(eparts.length!=4){
				logger.warning("Invalid command structure from "+user.user+", attempting admin command");
				//logger.warning(dec);
				//sendMessage("Invalid command");
				return;
			}
			if(!eparts[0].equals(parts[0])){//someone trying to spoof message
				logger.severe("Invalid command sent to create a user from account"+user.user);
				return;
			}
			if(user.isReplay(parts[0])){
				logger.severe("Message ID indicates replay");
				sendMessage("Failed to create user");
				return;
			}
			if(!user.validatePassword(eparts[1])){
				logger.severe("invalid admin password provided for user "+user.user);
				sendMessage("Failed to create user");
				return;
			}
			logger.info("ready to create user "+eparts[2]);
			Result result = config.addUser(eparts[2], eparts[3]);
			if(result.success){
				//closeUnknownUsers();// called in removeUser
				sendMessage("user "+eparts[2]+" created successfully");
			}
			else sendMessage(result.message);
		}
		
		private void sendMessage(String message){
			logger.info(message);
			//return false; //cannot change password if auth turned off
			try {
				addToSenderQueue(message);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				
			}
		}
		
		private boolean setUser(String line) {
			if(!UserConnection.authenticate)return true;
			if(UserConnection.authenticate && user.authenticated==false)
			{
				//System.out.println(line);
				String s[]=line.substring(5).split(",",2);
				//System.out.println(s[1]);
				user.user=s[0];
				//add check that user allowed
				user.authenticated=user.authenticateUser(s[1] );
				//logger.info("user "+user.user+" authentication was "+user.authenticated);
				if(!user.authenticated){
					try {
						logger.info("user "+user.user+" authentication failed");
						addToSenderQueue("authentication failed");
						return false;
					} catch (InterruptedException e) {
						
						e.printStackTrace();
						return false;
					}
				}
				else {
					try {
						logger.info("user "+user.user+" authenticated");
						
						addToSenderQueue("authentication success");
						
						return true;
					} catch (InterruptedException e) {
						
						e.printStackTrace();
						return false;
					
					}
				}
			}
			return true;
		}

		private void monitorFile(String line) {
			logger.finest("setting up a file monitor");
		}

		private void stopMonitor(String line)
		{
			logger.finest("stopping all monitors");
		}
		
		private void startReader(TCPSender sender2, String filename, String extension) {
		
		}
		
		private void addToSenderQueue(String s) throws InterruptedException
		{
			boolean flag=true;
			do {
				flag=sender.addToQueue(s);
			}while(!flag & sender.isAlive());
		}
		
		private void addToSenderQueue(ServiceEntry s,boolean isLive) throws InterruptedException
		{
			boolean flag=true;
			do {
				if(isLive)flag=sender.addToQueue(s);
				else flag=sender.addToQueueIgnoreLive(s);
			}while(!flag & sender.isAlive());
		}

		private ConcurrentHashMap<Thread,Integer> counts=new ConcurrentHashMap<Thread,Integer>();
		private ConcurrentHashMap<Thread,Long> queryTotals=new ConcurrentHashMap<Thread,Long>();
		
		@Override
		public void next(ServiceEntry se, int number,Thread t) {
			//System.out.println("next called "+t.getName());
			if(!counts.containsKey(t))counts.put(t, 0);
			if(se!=null)
				try {
					if((number-counts.get(t))>=1000){
						message("record: "+number+" of "+queryTotals.get(t));
						counts.put(t, number-number%1000);
					}
					//logger.info("number of entries contained: "+se.seList.size());
					addToSenderQueue(se,false);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}
		
		public void queryComplete(Thread t){
			counts.remove(t);
			queryTotals.remove(t);
			//queryTotal=0;
		}

		//long queryTotal=0;
		
		
		@Override
		public void count(long total,Thread t) {
			System.out.println(t.getName());
			queryTotals.put(t, total);
			
		}

		@Override
		public void message(String message) {
			sendMessage(message);
			
		}

		@Override
		public boolean sendMore() {
			// TODO Auto-generated method stub
			//System.out.println("sendMore is "+sendMore);
			return sendMore;
		}
		
		@Override
		public boolean sendPaused() {
			// TODO Auto-generated method stub
			//System.out.println("sendMore is "+sendMore);
			return sendPaused;
		}
		
		public void setSendMore(boolean b){
			sendMore=b;
			Collector.logger.info("set sendMore to "+sendMore);
		}
		
		public void setSendPaused(boolean b){
			sendPaused=b;
			Collector.logger.info("set sendPaused to "+sendPaused);
		}
		
	}
	
	private class MonitorException extends Exception
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 7752741263479716979L;

		public MonitorException(String string) {
			super(string);
		}

	}
	
}
