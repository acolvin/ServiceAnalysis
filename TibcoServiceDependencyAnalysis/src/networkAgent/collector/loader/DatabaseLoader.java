/**
 * 
 */
package networkAgent.collector.loader;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PushbackInputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;










import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

import org.bson.Document;



import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCommandException;
import com.mongodb.MongoTimeoutException;
import com.mongodb.WriteConcern;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.readersWriters.ExtensionHandler;
import serviceAnalysisModel.readersWriters.ServiceEntry;



/**
 * @author andrew
 *
 */
public class DatabaseLoader {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	public static void main(String[] args) throws SecurityException, IOException {
		System.setProperty("DEBUG.MONGO", "false");
		Logger mongoLogger = Logger.getLogger( "com.mongodb" );
		
		mongoLogger.setLevel(Level.SEVERE);
		mongoLogger.setUseParentHandlers(false);
		//FileHandler handler = new FileHandler("/dev/null");
		for(Handler h:Logger.getAnonymousLogger().getHandlers()) Logger.getAnonymousLogger().removeHandler(h);
		for(Handler h:Logger.getGlobal().getHandlers()) Logger.getGlobal().removeHandler(h);

		for(Handler h:mongoLogger.getHandlers()) mongoLogger.removeHandler(h);
		//mongoLogger.addHandler(handler);
		boolean stdin=false;
		String connection=null;
		ArrayList<String> filenames=new ArrayList<String>();
		if(args.length<2) {
			System.out.println("incorrect number of args");
			usage();
		}
		boolean locationFlag=false;
		for(String s:args){
			if(locationFlag){
				defaultLocation=s;
				locationFlag=false;
			}
			else if(s.equals("--"))stdin=true;
			else if(s.equals("-d"))dedup=true;
			else if(s.equals("-l"))locationFlag=true;
			else if(s.startsWith("mongodb://")) connection=s;
			else filenames.add(s);
		}
		
		if(connection==null){System.out.println("no connection string");usage();}
		if(stdin & filenames.size()>0){System.out.println("cannot define stdin and files");usage();}
		if(!stdin & filenames.size()==0){System.out.println("no files specified"); usage();}
		
		DatabaseLoader dbl;
		if(stdin){
			dbl=new DatabaseLoader(connection);
		}else {
			dbl=new DatabaseLoader(connection,filenames);
		}
		if(!dbl.connect()){
			System.out.println("failed to connect to the database");
			System.exit(-2);
		}
		
		dbl.execute();

	}
	
	private static boolean dedup=false;
	
	private static String defaultLocation=null;
	
	
	private static void usage(){
		System.out.println("usage:");
		System.out.println("DBloader [-d] [-l location] <connection string> <file> [<file> ...]");
		System.out.println("Load the specified file(s) into the collector database specified by the connection string");
		System.out.println("DBloader [-d] [-l location] <connection string> --");
		System.out.println("Load the data from standard in");
		System.out.println("optional argument -d causes a check for duplicate records");
		System.out.println("optional argument -l sets the server name");
		
		System.exit(-1);
	}

	private MongoClient mongoClient;
	private MongoDatabase db;
	private MongoCollection<Document> collection=null;
	
	public DatabaseLoader(String connection, List<String> filenames){
		// load files into database
		this.filenames=filenames;
		connectionString=connection;
	}
	
	public DatabaseLoader(String connection){
		//load records from stdin
		stdin=true;
		connectionString=connection;
	}
	
	public boolean connect(){
		Logger mongoLogger = Logger.getLogger( "com.mongodb" );
		mongoLogger.setLevel(Level.SEVERE);
		if(connectionString==null) return false;
		try{

			mongoClient = new MongoClient( new MongoClientURI(connectionString));
			db = mongoClient.getDatabase("ASAP");
		}catch(Exception e){
			System.err.println("caught exception getting db");
			System.err.println(e.getMessage());
			//e.printStackTrace();
			db=null;
		}
		if(db!=null){
			try{
				boolean flag=false;
				for(String s:db.listCollectionNames())
					if(s.equals("timings")) flag=true;
				if(!flag){
					db.createCollection("timings");
				 
				}
				collection = db.getCollection("timings");
				BasicDBObject bdo = new BasicDBObject();
				bdo.append("time", 1);
				collection.createIndex(bdo);
				bdo = new BasicDBObject();
				bdo.append("layer", 1);
				collection.createIndex(bdo);
				bdo = new BasicDBObject();
				bdo.append("service", 1);
				collection.createIndex(bdo);
				bdo = new BasicDBObject();
				bdo.append("operation", 1);
				collection.createIndex(bdo);
				collection.withWriteConcern(WriteConcern.UNACKNOWLEDGED);
			}catch( MongoCommandException e ){
			
			}catch( MongoTimeoutException e){
				System.err.println("Unable to connect to the mongo database");
				System.exit(-2);
			}
		}
		//MongoCollection<Document> collection = db.getCollection("timings");
		if(db!=null){
			
				Runtime.getRuntime().addShutdownHook(new disconnectMongo());
				return true;
		}
		else return false;	
		
		
		
		
		
	}

	public class disconnectMongo extends Thread
	{
		public void run(){
		
			
			if(mongoClient!=null) mongoClient.close();
			 System.out.println("");
		}
	}
	
	public void execute(){
		if(!stdin){
			for(String filename:filenames) loadFile(filename);
		}
		else {loadStdin();}
		
		if(filenames!=null && filenames.size()>1){
			System.out.println("Total line processed was "+linesProcessed);
			System.out.println("Total records saved to the database was "+recordsFound);
			if(dedup)System.out.println("Total duplicate records found was "+totalDuplicates);
		}
	}
	
	private void loadFile(String filename){
		currentFilename=filename;
		duplicates=0;
		int count=0;
		int records=0;
		if(filename==null || filename.equals("")){
			System.out.println("invalid filename");
			return;
		}
		File f=new File(filename);
		if(!f.canRead()){
			System.out.println("cannot read file: "+filename);
			return;
		}
		if(f.isDirectory()){
			System.out.println(filename+" is a director");
			return;
		}
		if(!f.isFile()){
			System.out.println("not a file: "+filename+" will try to read as a pipe");
			loadPipe(filename);
			return;
		}
		System.out.println("processing file: "+filename);
		try { 
			FileReader fr = new FileReader(filename); 
			BufferedReader br = new BufferedReader(fr);
			String s;
			while ((s = br.readLine()) != null)
			{
				count++;
				try{
					if(writeToMongo(ExtensionHandler.getInstance().ProcessLogFileLine(s)))records++;
					if(count%1000==0)System.out.print('.');
				}catch (Exception e){
					if(e.getClass().isInstance(IOException.class)) {  e.printStackTrace(); break;}
				}
			}
			br.close();
			fr.close();
		}catch(IOException e){
			System.out.println("io error on file: "+filename);
		}
		
		linesProcessed+=count;
		recordsFound+=records;

		System.out.println("\nProcessed "+count+" lines and saved "+records+" records, with "+duplicates+" duplicates");

	}
	private String currentFilename;
	
	private void loadStdin(){
		currentFilename="stdin";
		int count=0;
		int records=0;
		String s = "";
		InputStreamReader converter = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(converter);
		boolean end=false;
		while (!end)
		{
			try
			{
				s = in.readLine();
				if (s != null)
				{
					count++;
					try{
						if(writeToMongo(ExtensionHandler.getInstance().ProcessLogFileLine(s)))records++;
						if(count%1000==0)System.out.print('.');
					}
					catch(Exception e){
						if(e.getClass().isInstance(IOException.class)){ end=true; e.printStackTrace();}
					}
				}else end=true;

			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		linesProcessed+=count;
		recordsFound+=records;
		System.out.println("\nProcessed "+count+" lines and saved "+records+" records, with "+ duplicates+" duplicates");
	}
	
	private void loadPipe(String filename){
		duplicates=0;
		currentFilename=filename;
		int count=0;
		int records=0;
		String s = "";
		InputStreamReader converter; 
		BufferedReader pipe;
		try {
			converter = new InputStreamReader(DatabaseLoader.maybeDecompress(new FileInputStream(filename)));
			pipe = new BufferedReader(converter);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return;
		} 
		//RandomAccessFile pipe;
		//try {
		//	pipe = new RandomAccessFile(filename,"r");
		//catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
		//	return;
		//}
		
		boolean end=false;
		while (!end)
		{
			try
			{
				s = pipe.readLine();
				if (s != null)
				{
					count++;
					if(writeToMongo(ExtensionHandler.getInstance().ProcessLogFileLine(s)))records++;
					if(count%1000==0)System.out.print('.');
				}else end=true;

			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		try {
			pipe.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
		}
		linesProcessed+=count;
		recordsFound+=records;

		System.out.println("\nProcessed "+count+" lines and saved "+records+" records, with "+duplicates+" duplicates");
	}

	
	private boolean writeToMongo(ServiceEntry se){
		if(se==null)return false;
		//System.out.println(""+writeRecords+"-"+Config.db);
		if(se.when==null) return false;//only interested in timing events atm
		if(collection==null) return false;//no database connection
		
		if(defaultLocation!=null )se.location=defaultLocation;
		
		if(dedup){
			//check that the record isnt already in the database
			final List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
			obj.add(new BasicDBObject("time",se.when ));
			
			obj.add(new BasicDBObject("layer", java.util.regex.Pattern.compile(se.service.getType())));
			obj.add(new BasicDBObject("service", java.util.regex.Pattern.compile(se.service.getServiceName())));
			obj.add(new BasicDBObject("operation", java.util.regex.Pattern.compile(se.service.getOperationName())));
			BasicDBObject query;
			if(obj.size()>1){
				query=new BasicDBObject();
				query.put("$and", obj);
			}
			else{
				query=obj.get(0);
			}
			    	    
			if(collection.count(query)>0){
				//check to see if my record is in the query or if it is just 2 at the same time
				FindIterable<Document> cursor = collection.find(query);
				
				for(Document d:cursor){
					ServiceEntry nse=processDocument(d);
					se.filename=currentFilename;
					if(isEqual(se,nse)){
						//they are the same
						duplicates++;
						totalDuplicates++;
						return false;
					}
				}
			}
		}
		
		
		Document doc = new Document("service", se.service.getServiceName())
								.append("operation", se.service.getOperationName())
								.append("layer", se.service.getType())
								.append("time", se.when)
								.append("response", se.response)
								.append("wait", se.wait)
								.append("filename", currentFilename)
								.append("location", se.location)
								.append("transaction", se.txn)
								.append("user", se.user)
								.append("login", se.loginEvent)
								.append("logout", se.logoutEvent);
		try{	
		collection.insertOne(doc);
		}catch (Exception e){
			//e.printStackTrace();
		}
		return true;
	}
	
	private ServiceEntry processDocument(Document d){
		String servicename=d.getString("service");
	    String operation=d.getString("operation");
	    String layer=d.getString("layer");
	    Date when=d.getDate("time");
	    Double response=d.getDouble("response");
	    
	    //check for a reasonable subset of data
	    if(servicename==null || operation==null || layer==null || when==null || response==null ) return null;
	    //if(response<0) response=0D; //lets have a zero bound
	    
	    Double wait=d.getDouble("wait");
	    String filename=d.getString("filename");
	    String location=d.getString("location");
	    String txn=d.getString("transaction");
		String user=d.getString("user");
		Boolean in=d.getBoolean("login",false);
		Boolean out=d.getBoolean("logout", false);
		ServiceEntry se=new ServiceEntry(new Service(layer,servicename,operation,""),when,response);
		if(wait!=null)se.wait=wait;
		if(filename!=null)se.filename=filename;
		if(location!=null)se.location=location;
		if(txn!=null)se.txn=txn;
		if(user!=null)se.user=user;
		se.loginEvent=in;
		se.logoutEvent=out;
		
		return se;
	}
	
	private boolean isEqual(ServiceEntry se1,ServiceEntry se2){
		if(se1==null || se2==null) return false;
		//System.out.println(se1.txn+"|"+se2.txn);
		if(se1.txn==null & se2.txn!=null)return false;
		if(se2.txn==null &se1.txn!=null) return false;
		if(!se1.txn.equals(se2.txn)) return false;
		//System.out.println(se1.filename+"|"+se2.filename);
		//if(se1.filename==null & se2.filename!=null)return false;
		//if(se2.filename==null &se1.filename!=null) return false;
		//if(!se1.filename.equals(se2.filename)) return false;
		//System.out.println("Are equal");
		//return true;
		return (se1.response==se2.response & se1.wait==se2.wait);
	}
	private boolean stdin=false;
	private List<String> filenames;
	private String connectionString;
	
	private int linesProcessed=0;
	private int recordsFound=0;
	private int duplicates=0;
	private int totalDuplicates=0;

	
	public static InputStream maybeDecompress(InputStream input) throws IOException {
	    final PushbackInputStream pb = new PushbackInputStream(input, 2);

	    int header = pb.read();
	    if(header == -1) {
	        return pb;
	    }

	    int b = pb.read();
	    if(b == -1) {
	        pb.unread(header);
	        return pb;
	    }

	    pb.unread(new byte[]{(byte)header, (byte)b});

	    header = (b << 8) | header;

	    if(header == GZIPInputStream.GZIP_MAGIC) {
	        return new GZIPInputStream(pb);
	    } else {
	        return pb;
	    }
	}
	
	public static boolean isGZipped(File file) {   
	     RandomAccessFile raf;
		try {
			raf = new RandomAccessFile(file, "r");
			boolean b= GZIPInputStream.GZIP_MAGIC == (raf.read() & 0xff | ((raf.read() << 8) & 0xff00));
			raf.close();
			return b;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	    
	}
}
