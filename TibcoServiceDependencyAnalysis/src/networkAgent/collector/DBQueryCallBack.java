package networkAgent.collector;

import serviceAnalysisModel.readersWriters.ServiceEntry;

public interface DBQueryCallBack {
	public void next(ServiceEntry se, int number,Thread t);
	public void count(long total,Thread t);
	public void message(String message);
	public boolean sendMore();
	public boolean sendPaused();
	public void queryComplete(Thread t);
}
