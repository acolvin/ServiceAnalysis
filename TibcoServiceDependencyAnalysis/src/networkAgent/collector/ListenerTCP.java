package networkAgent.collector;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import networkAgent.agentTCP.TCPMessage;
import networkAgent.collector.Collector.TCPSender;
import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisModel.readersWriters.ServiceEntry;

//import serviceAnalysisModel.readersWriters.BWReader;

public class ListenerTCP extends Thread
{
	private final Logger logger=Collector.logger;
	private String traceMessage="";
	int localPort;
	
	public ListenerTCP(Collector sender, Connection c)
			throws UnknownHostException, IOException
	{
		
		super("TCP Receiver");
		this.setDaemon(true);
		connection=c;
		traceMessage="ServiceEntry Received from "+c.host+":"+c.port;
		socket =  new Socket(c.host,c.port);
		this.sender = sender;
		this.port = c.port;
		in = new ObjectInputStream(socket.getInputStream());
		os = new PrintStream(socket.getOutputStream());
		localPort=socket.getLocalPort();

	}
	private ObjectInputStream in;
	private PrintStream os;
	private Socket socket;

	public void interrupt()
	{
		os.println("quit");
		os.flush();
		stop=true;	
		super.interrupt();
	}
	
	public void run()
	{

		//Controller.getController().sendMessage("TCP Client started on port "+ socket.getLocalPort());
		connection.connected=true;
		if(logger.isLoggable(Level.INFO)) {
			logger.info("TCP Client Starting on port "+socket.getLocalPort());
		}
		while (!stop)
		{
			try
			{
				Object o=in.readObject();
				

				TCPMessage m=(TCPMessage) o;
				
				ServiceEntry se = m.se;
				// if(se!=null &&
				// se.service!=null)System.out.println(se.service.getSName());
				if (se != null){
					sender.trace(traceMessage, se);
					sender.addToQueue(se);
				}
				else if(m.string==null)
				{
					// if null need to do a keep-alive back
					os.println("Hello");
					os.flush();
					
				}
				else
				{
					//  we received a string message
					//Controller.getController().sendMessage(m.string);
					if(logger.isLoggable(Level.INFO)) {
						logger.info(m.string);
					}
					//TODO handle commands coming this way (auth messages etc)
				}
				// String received = new String(dgp.getData(), 0,
				// dgp.getLength());
				// System.out.println("Quote of the Moment: " + received);
			} catch(EOFException e){
				stop=true;
				logger.info("TCP Client stopped");
				//Controller.getController().sendMessage("TCP Client stopped");
			}catch (IOException e)
			{
				// TODO Auto-generated catch block
				logger.log(Level.WARNING, e.getMessage(),e);
				//e.printStackTrace();
				//Controller.getController().sendMessage("TCP Client stopped");

				stop=true;
			} catch (ClassNotFoundException e)
			{
				// TODO Auto-generated catch block
				logger.log(Level.WARNING, e.getMessage(),e);
				//e.printStackTrace();
				//Controller.getController().sendMessage("TCP Client stopped");

				stop=true;
			} 
		}
		
		os.println("quit");
		connection.connected=false;
	}

	public boolean stop = false;
	int port;
	Collector sender;
	Connection connection;

	public void sendcommand(String command) {
		os.println(command);
		os.flush();
		
	}
}
