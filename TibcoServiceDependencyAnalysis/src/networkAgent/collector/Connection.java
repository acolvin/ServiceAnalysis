package networkAgent.collector;

import java.util.logging.Logger;

class Connection {
	String host=null;
	Integer port=null;
	boolean connected=false;
	public boolean removeMe=false;
	public ListenerTCP listener=null;
	
	public boolean equals(Object o){
		if(o==null){ 
			//System.out.println("connection is null"); 
			return false;
		}
		else if(o instanceof Connection)
		{
			//System.out.println("we have a connection object");
			Connection c=(Connection) o;
			if(c.host.equalsIgnoreCase(host) & c.port.equals(port)){
				//System.out.println("connections are equal"); 
				return true;
			}
			//else{
			//	System.out.println(c.host);
			//	System.out.println(host);
			//	System.out.println(c.port);
			//	System.out.println(port);
			//	System.out.println("hosts equal="+c.host.equalsIgnoreCase(host));
			//	System.out.println("ports equal="+(c.port==port));
			//}
			
			
		}
		//System.out.println("connections are not equal "+o.toString()+"<>"+toString()); 
		return false;
	}
	
	public int hashCode(){
		return (""+host+":"+port).hashCode();
	}
	
	public String toString(){
		return ""+host+":"+port;
	}
	
	public void stopListener()
	{
		if(listener==null)return;
		Logger logger=Collector.logger;
		logger.info("stopping collector on "+toString()+" local port="+listener.localPort);
		listener.stop=true;
	}
}

