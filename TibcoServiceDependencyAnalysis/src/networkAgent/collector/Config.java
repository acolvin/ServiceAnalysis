package networkAgent.collector;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Cipher;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientOptions.Builder;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCommandException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import networkAgent.collector.Config.Result;




class Config {
	Integer port=5555;
	ArrayList<Connection> connections=new ArrayList<Connection>();
	static HashMap<String,String> users=new HashMap<String,String>();
	static HashSet<String> admins=new HashSet<String>();
	
	private static boolean remoteManagement=true;
	//KeyPairGenerator kpg;
	//PublicKey pubk;
	//PrivateKey prvk;
	//char[] base64PublicKey;
	private final Logger logger=Collector.logger;
	
	private static boolean saveToDB=false;
	
	Collector collector=null;
	public Reconnection monitor=new Reconnection();;
	
	private PurgeTimer purgeTimer=new PurgeTimer();
	
	//final String xform = "RSA/ECB/PKCS1PADDING";
	
	public Config() {
		
		purgeTimer.start();
		//collector=myCollector;
	    // Generate a key-pair - now done per user
	    /*
		try {
			kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(512); // 512 is the keysize.
		    KeyPair kp = kpg.generateKeyPair();
		    pubk = kp.getPublic();
		    prvk = kp.getPrivate();
		    base64PublicKey = Base64Coder.encode(pubk.getEncoded());
		    //System.out.println(pubk.toString());
		    //System.out.println(base64PublicKey.toString());
		} catch (NoSuchAlgorithmException e) {
			Collector.logger.log(Level.SEVERE, "RSA Algorithm Not Found", e);
			System.exit(-2);
		}
	    */
	}

	/*
	public byte[] encrypt(byte[] inpBytes) throws Exception {
	    Cipher cipher = Cipher.getInstance(xform);
	    cipher.init(Cipher.ENCRYPT_MODE, pubk);
	    return cipher.doFinal(inpBytes);
	}
	
	public byte[] decrypt(byte[] inpBytes) throws Exception{
		    Cipher cipher = Cipher.getInstance(xform);
		    cipher.init(Cipher.DECRYPT_MODE, prvk);
		    return cipher.doFinal(inpBytes);
	}
	
	*/
	private Long modified=null;
	String file=null;
	
	public void handleInputFile(String string) {
		if(file==null)file=string;
		File file=new File(string);
		rewriteUser = false;
		Long l=file.lastModified();
		
		boolean purgeSet=false;
		boolean purgeDelaySet=false;
		boolean saveStoreSet=false;
		boolean dburlSet=false;
		
		if(file.exists() && file.isFile() && file.canRead() && (modified==null || l>modified && modified!=-1L))
		try {
			logger.info("Processing config file "+file.getName());
			modified=-1L;
			
			
			for(Connection c:connections) c.removeMe=true;
			users.clear();
			admins.clear();
			
			FileReader fr;
			
				fr = new FileReader(file);
			
			BufferedReader br = new BufferedReader(fr);
			String s;
			ArrayList<String> lines=new ArrayList<String>();
			//if(reader==null)System.out.println("reader is null");
			while ((s = br.readLine()) != null){
				
				lines.add(s);
				String split[]=s.split("=",2);
				if(split.length!=2) {} //ignore this line
				else if(split[0].equalsIgnoreCase("port")) savePort(split[1]);
				else if(split[0].equalsIgnoreCase("connection")) saveConnection(split[1]);
				else if(split[0].equalsIgnoreCase("user")) saveUser(split[1]);
				else if(split[0].equalsIgnoreCase("auth")) saveAuth(split[1]);
				else if(split[0].equalsIgnoreCase("admin")) saveAdmin(split[1]);
				else if(split[0].equalsIgnoreCase("remote")) saveRemoteManagement(split[1]);
				else if(split[0].equalsIgnoreCase("store")) {
					saveStoreSet=true;
					saveStore(split[1]);
				}
				else if(split[0].equalsIgnoreCase("mongourl")){
					dburlSet=true;
					saveMongo(split[1]);
				}
				else if(split[0].equalsIgnoreCase("purgedelay")){
					System.out.println("found purge delay config");
					purgeDelaySet=true;
					savePurgeDelay(split[1]);
				}
				else if(split[0].equalsIgnoreCase("purgeperiod")){
					purgeSet=true;
					savePurgePeriod(split[1]);
				}
				
			}
			if(!dburlSet){
				saveMongo(defaultMongoURL);
			}
			if(!saveStoreSet){
				saveStore("false");
			}
			if(!purgeDelaySet){
				savePurgeDelay(""+defaultPurgeDelay);
			}
			if(!purgeSet){
				savePurgePeriod(""+defaultPurgePeriod);
			}
			fr.close();
			
			if(file.canWrite() && rewriteUser){
				//writeConfigFile(lines);
				writeConfigFile();
			}	
			modified=file.lastModified();
			//remove remaining connections from oldies
			ArrayList<Integer> indexes=new ArrayList<Integer>();
			for(int i=0;i<connections.size();i++){
				if(connections.get(i).removeMe){
					indexes.add(i);
					
				}
			}
			Collections.sort(indexes);
			for(int j=indexes.size()-1;j>=0;j--)
			{
				int i=indexes.get(j);
				System.out.println("removing index "+i);
				Connection c=connections.get(i);
				connections.remove(c);
				c.stopListener();
			}
			if(collector!=null)
				collector.closeUnknownUsers();
			
		} catch (IOException e) 
		{
			
		}
		
	}
	
	private void saveMongo(String string) {
		if(!mongoURL.equalsIgnoreCase(string)){
			mongoURL=string;
			disconnectMongo();
			connectMongo();
		}
		
		
	}
	
	private void savePurgePeriod(String period) {
		try {
			long l=Long.parseLong(period);
			if(l>=0)purgePeriod=l;
			else logger.severe("purgePeriod cannot be negative");
		} catch (NumberFormatException e) {
			logger.severe(period +" is not a number for purgePeriod");
		}
	}
	
	private void savePurgeDelay(String hours) {
		try {
			setPurgeDelay(Integer.parseInt(hours));
		} catch (NumberFormatException e) {
			logger.severe(hours +" is not a number for purgeDelay");
		}
	}

	public void writeConfigFile() throws IOException
	{
		logger.info("Save config file "+this.file);
		File configfile=new File(this.file);
		File backupFile=new File(this.file+".backup");
		backupFile.deleteOnExit();
		logger.fine("move config file to .backup");
		configfile.renameTo(backupFile);
		
		
		
		
		
		synchronized(users){
			modified=-1L;
			File file=new File(this.file);
			FileWriter fw=new FileWriter(file);
			BufferedWriter bw=new BufferedWriter(fw);
			logger.fine("writing port "+port);
			bw.write("port="+port);
			bw.newLine();
			logger.fine("writing auth "+UserConnection.authenticate);
			bw.write("auth="+UserConnection.authenticate);
			bw.newLine();
			logger.fine("writing remote "+remoteManagement);
			bw.write("remote="+remoteManagement);
			bw.newLine();
			logger.fine("writing store "+saveToDB);
			bw.write("store="+saveToDB);
			bw.newLine();
			logger.fine("writing mongo url "+mongoURL);
			bw.write("mongoURL="+mongoURL);
			bw.newLine();
			logger.fine("writing purgePeriod "+purgePeriod);
			bw.write("purgePeriod="+purgePeriod);
			bw.newLine();
			logger.fine("writing purgeDelay "+purgeDelay);
			bw.write("purgeDelay="+purgeDelay);
			bw.newLine();
			for(String user:users.keySet()){
				String s="user="+user+","+users.get(user);
				logger.fine("writing user "+user);
				bw.write(s);
				bw.newLine();
			}
			for(String admin:admins){
				logger.fine("writing admin user "+admin);
				String s="admin="+admin;
				bw.write(s);
				bw.newLine();
			}
			for(Connection c:connections){
				logger.fine("writing connection "+c.host+":"+c.port);
				String s="connection="+c.host+":"+c.port;
				bw.write(s);
				bw.newLine();
			}
			bw.flush();
			fw.close();
			logger.fine("delete backup file");
			backupFile.delete();
			modified=file.lastModified();
		}
	}
	
/*	public void writeConfigFile(ArrayList<String> lines) throws IOException
	{
		writeConfigFile();
		File configfile=new File(this.file);
		File backupFile=new File(this.file+".backup");
		backupFile.deleteOnExit();
		configfile.renameTo(backupFile);
		
		
		
		synchronized(users){
			modified=-1L;
			File file=new File(this.file);
			logger.info("Writing config file "+file.getName());
			FileWriter fw=new FileWriter(file);
			BufferedWriter bw=new BufferedWriter(fw);
			for(String line:lines){
				if(line.length()>4 && line.substring(0, 4).equalsIgnoreCase("user")){ //&& !line.contains("Hashed:")){
					String ss[]=line.split(",");
					try {
						logger.fine("user:"+ss[0].substring(5));
						if(users.containsKey(ss[0].substring(5))) line=ss[0]+","+users.get(ss[0].substring(5));
						else {
							logger.fine("writing out original line, commented out with hashed password");
							if(!line.contains("Hashed"))
								line="#"+ss[0]+",Hashed:"+PasswordHash.createHash(ss[1]);
							else line="#"+line;
						}
						logger.fine(line);
					} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
						logger.log(Level.SEVERE, "Cannot Decrypt", e);
					} catch (InvalidKeySpecException e) {
					// TODO Auto-generated catch block
						logger.log(Level.SEVERE, "Cannot Decrypt", e);
					}
				}
				if(!line.startsWith("admin=")){
					bw.write(line);
					bw.newLine();
					bw.flush();
				}
			}
			logger.info(""+admins.size()+" admin entries");
			for(String admin:admins){
				logger.info("writing admin user "+admin);
				String s="admin="+admin;
				bw.write(s);
				bw.newLine();
				bw.flush();
			}
			bw.flush();
			fw.close();
			backupFile.delete();
			modified=file.lastModified();
		}
	}
*/	
	private static boolean rewriteUser=false;
/*
	public boolean authenticateUser(UserConnection user, String password)
	{
		if(password==null )logger.info("no password sent for user "+user.user);
		//System.out.println("authenticating user "+user);
		//System.out.println("encoded password="+password);
		if(!users.containsKey(user.user))return false;
		// add password checks here
		try {
			byte[] s=Base64Coder.decode(password);
			//System.out.println("encrypted password length="+s.length);
			byte[] pass=user.decrypt(s);
			
			//System.out.println(pass.length);
			String ss=new String(pass);
			//System.out.println(PasswordHash.createHash(ss));
			String p=users.get(user.user);
			//System.out.println("password="+s);
			//System.out.println("passwords match "+p.equals(ss));
			if(p.startsWith("Hashed:")){
				String pp=p.substring(7);
				return PasswordHash.validatePassword(ss, pp);
			}
			else return users.get(user).equals(ss);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		
		
		
	}*/
	
	
	
	
	public static String getHashedPassword(String user){
		return users.get(user);
	}
	
	
	
	private static void saveAdmin(String admin){
		admins.add(admin);
	}
	
	public static boolean isAdmin(String user){
		return admins.contains(user);
	}
	
	public String setUserPassword(String user, String password){
		if(!remoteManagement){
			logger.warning("Remote Password Management Disabled");
			return "Remote Password Management Disabled in Config";
		}
		
		String hashedPassword;
		if(!password.startsWith("Hashed:")){
			try {
				
				hashedPassword="Hashed:"+PasswordHash.createHash(password);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "Cannot Hash new Password", e);
				return "An error occurred - please see collector log";
				
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "Cannot Hash new Password", e);
				return "An error occured - please see collector log";
			}
			
			
		}else {
			hashedPassword=password;
		}
		if(!users.containsKey(user)){
			logger.warning("Trying to change a password for non-existant user");
			return "Unable to change password";
		}
		synchronized(users){
			users.put(user, hashedPassword);
//			FileReader fr;
//			logger.fine("reading current config file");
			try {
//				fr = new FileReader(file);
//				BufferedReader br = new BufferedReader(fr);
//				String s;
//				ArrayList<String> lines=new ArrayList<String>();
//			
//				while ((s = br.readLine()) != null){
//				
//					lines.add(s);
//				}
				writeConfigFile();
				//writeConfigFile(lines);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				logger.severe("unable to write config file - file not found and connot be read");
				logger.log(Level.SEVERE,"file cannot be found",e);
				logger.warning("Password changed for this session only");
				return "Password set for this session only - please see collector log";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				logger.severe("unable to write config file - io error reading file");
				logger.warning("Password changed for this session only");
				logger.log(Level.SEVERE,"unable to write config file - io error reading file",e);
				return "Password set for this session only - please see collector log";
			}
		
			
			
		}
		return "Password set successfully";
		

		
	}
	
	private void rewriteConfigFile() throws IOException{
		synchronized(users){
			
//			FileReader fr;
//			logger.fine("reading current config file");
//			
//			fr = new FileReader(file);
//			BufferedReader br = new BufferedReader(fr);
//			String s;
//			ArrayList<String> lines=new ArrayList<String>();
//			
//			while ((s = br.readLine()) != null){
//				
//				lines.add(s);
//			}
				
//			writeConfigFile(lines);
			writeConfigFile();
		}
	}
	private void rewriteConfigFile(String user, String hPassword) throws IOException{
		synchronized(users){
			
//			FileReader fr;
//			logger.fine("reading current config file");
			
//			fr = new FileReader(file);
//			BufferedReader br = new BufferedReader(fr);
//			String s;
//			ArrayList<String> lines=new ArrayList<String>();
			
//			while ((s = br.readLine()) != null){
//				
//				lines.add(s);
//			}
//			lines.add("user="+user+","+hPassword+"\n");	
//			writeConfigFile(lines);
			writeConfigFile();
		}
	}
	public boolean changeUserPassword(String user, String password){
		String hashedPassword;
		if(!remoteManagement){
			logger.warning("Remote Password Management Disabled");
			return false;
		}
		if(!password.startsWith("Hashed:")){
			try {
				
				hashedPassword="Hashed:"+PasswordHash.createHash(password);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "Cannot Hash new Password", e);
				return false;
				
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "Cannot Hash new Password", e);
				return false;
			}
			
			
		}else {
			hashedPassword=password;
		}
		
		if(!users.containsKey(user)){
			logger.warning("Trying to change a password for non-existant user");
			return false;
		}
		
		try {
			if(PasswordHash.validatePassword(password,users.get(user).substring(7))){
				logger.info("Changing password to existing value");
				return false; //same password
			}
			synchronized(users){
				users.put(user, hashedPassword);
//				FileReader fr;
//				logger.fine("reading current config file");
				try {
//					fr = new FileReader(file);
//					BufferedReader br = new BufferedReader(fr);
//					String s;
//					ArrayList<String> lines=new ArrayList<String>();
//				
//					while ((s = br.readLine()) != null){
//					
//						lines.add(s);
//					}
					
//					writeConfigFile(lines);
					writeConfigFile();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					logger.severe("unable to write config file - file not found and connot be read");
					logger.warning("Password changed for this session only");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					logger.severe("unable to write config file - io error reading file");
					logger.warning("Password changed for this session only");
				}
			
				
				
			}
			return true;
			
		} catch (NoSuchAlgorithmException  e) {
			// TODO Auto-generated catch block
			return false;
		} catch (InvalidKeySpecException e) {
			return false;
		}
	}

	public Result removeUser(String user){
		if(!remoteManagement){
			logger.warning("Remote Password Management Disabled");
			return new Result(false,Result.MANAGEMENT,"Remote Management is Disabled");
		}
		if(!users.containsKey(user)) {
			logger.info("Cannot disable user, "+user+" does not exist");
			return new Result(false,Result.INVALIDUSER,"Cannot disable user, "+user+" does not exist");
		}
		users.remove(user);
		if(collector!=null)
			collector.closeUnknownUsers();
		try {
			rewriteConfigFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IO Error writing config file", e);
			return new Result(false,Result.WRITEERROR,"Failed to permanently disable user due to being unable to write config file");
		}
		return new Result(true,Result.SUCCESS,"User "+user+" disbaled successfully");
	}
	
	public Result addUser(String user,String password){
		if(!remoteManagement){
			logger.warning("Remote Management Disabled");
			return new Result(false,Result.MANAGEMENT,"Remote Management is Disabled");
		}
		if(users.containsKey(user)) {
			logger.info("Cannot add user, "+user+" already exist");
			return new Result(false,Result.INVALIDUSER,"Cannot create user, "+user+" already exist");
		}
		String hPassword;
		try {
			hPassword="Hashed:"+PasswordHash.createHash(password);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"cannot hash password",e);
			return new Result(false,Result.MANAGEMENT,"Cannot hash password");

		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"cannot hash password",e);
			return new Result(false,Result.MANAGEMENT,"Cannot hash password");
		}
		users.put(user, hPassword);
		try {
			rewriteConfigFile(user, hPassword);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IO Error writing config file", e);
			return new Result(false,Result.WRITEERROR,"Failed to permanently create user due to being unable to write config file");
		}
		return new Result(true,Result.SUCCESS,"User "+user+" created successfully");

	}
	
	public class Result {
		
		public final boolean success;
		public final String message;
		public final int resultCode;
		
		public Result(boolean success,int code, String message){
			this.success=success;
			this.message=message;
			this.resultCode=code;
		}
		
		public static final int INVALIDUSER=-2;
		public static final int SUCCESS=0;
		public static final int WRITEERROR=-3;
		public static final int MANAGEMENT=-1;
		
				
	}
	
	private static void saveUser(String user) { //username,password
		// TODO Auto-generated method stub
		Logger.getAnonymousLogger().fine("saveUser called with "+ user);
		String s[]=user.split(",");
		//System.out.println(s[0]);
		//if(!users.containsKey(s[0])) 
		String pass=s[1];
		if(!s[1].startsWith("Hashed:")){
			try {
				pass="Hashed:"+PasswordHash.createHash(pass);
				rewriteUser=true;
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				
			}
		}
		users.put(s[0], pass); //replace or add
		
	}

	private static void saveRemoteManagement(String bool){
		Logger.getAnonymousLogger().fine("saveRemoteManagement called with "+bool);
		remoteManagement=Boolean.parseBoolean(bool);
	}
	
	
	private Connection saveConnection(String string) { //host:port
		//System.out.println("connections length="+connections.size());
		Integer port=5555;
		String host;
		String split[]=string.split(":",2);
		host=split[0].trim();
		if(split[1].length()==2) port=Integer.parseInt(split[1]);
		Connection c=new Connection();
		c.host=host;
		c.port=port;
		if(!connections.contains(c)){
			//System.out.println("adding to connections "+c.hashCode());
			connections.add(c);
			return c;
		}
		else {
			c=connections.get(connections.indexOf(c));
			c.removeMe=false;
			return c;
		}
	}

	private void saveStore(String bool) {
		boolean saveOrig=saveToDB;
		saveToDB=Boolean.parseBoolean(bool);
		if(!saveToDB & saveOrig){
			//we turned off db writes
			disconnectMongo();
		}
		if(saveToDB & !saveOrig){
			connectMongo();
		}
	}

	private void savePort(String string) {
		port=Integer.parseInt(string);
		
	}
	
	private void saveAuth(String string) {
		UserConnection.authenticate=Boolean.parseBoolean(string);
	}
	
	class Reconnection extends Thread
	{
		
		boolean run=true;
		public Reconnection(){
			this.setDaemon(true);
		}
		
		public void run(){
			while(run){
				for(Connection c:connections){
					if(c.connected==false){
						//do restart
						collector.startConnection(c);
					}
				
				}
				handleInputFile(file);
				try {
					Thread.sleep(10000L);
				} catch (InterruptedException e) {
					run=false;
				}
			}
		}
		
	}

	public Result setAdmin(String user, boolean add) {
		if(!remoteManagement){
			logger.warning("Remote Management Disabled");
			return new Result(false,Result.MANAGEMENT,"Remote Management is Disabled");
		}
		if(!users.containsKey(user)){
			logger.info("Cannot change admin status for user, "+user+", as user does not exist");
			return new Result(false,Result.INVALIDUSER,"Cannot change admin status for user, "+user+", as user does not exist");
		
		}
		if(admins.contains(user) & add) {
			logger.info("Cannot set user, "+user+" as admin, already an admin");
			return new Result(false,Result.INVALIDUSER,"Cannot set user, "+user+" as admin already exist");
		}
		if(!admins.contains(user) & !add) {
			logger.info("Cannot unset user, "+user+" as admin, not an admin");
			return new Result(false,Result.INVALIDUSER,"Cannot unset user, "+user+" as not an admin");
		}
		if(add)admins.add(user);
		else admins.remove(user);
		try {
			rewriteConfigFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IO Error writing config file", e);
			return new Result(false,Result.WRITEERROR,"Failed to permanently change admin user. Error writing config file");
		}
		return new Result(true,Result.SUCCESS,"Admin user changed successfully");

		
		
	}
	
	public void connectMongo()
	{
		if(saveToDB)
			(new MongoConnect()).start();
	}
	
	public void disconnectMongo()
	{
		DBWriter.shutdown();
		if(mongoClient!=null) {
			mongoClient.close();
		
			db=null;
			syncMe=0;
			mongoClient=null;
		}
		
	}
	
	private class MongoConnect extends Thread{
	
		
		
		public void run(){
			Thread.currentThread().isDaemon();
			connectMongo();
			DBWriter.getDBWriter().setWrite();
		}
		
		public void connectMongo()
		{
			synchronized(syncMe){	
				try{
				//Builder builder = new MongoClientOptions.Builder();
				//builder.connectTimeout(5000);
				//builder.maxWaitTime(10000);
					mongoClient = new MongoClient( new MongoClientURI(mongoURL));
			
					db = mongoClient.getDatabase("ASAP");
				}catch(Exception e){
					System.err.println("caught exception getting db");
					e.printStackTrace();
					db=null;
				}
		
		
				try{
					boolean flag=false;
					for(String s:db.listCollectionNames())
						if(s.equals("timings")) flag=true;
					if(!flag){
						 db.createCollection("timings");
						 
					}
					MongoCollection<Document> collection = db.getCollection("timings");
					 BasicDBObject bdo = new BasicDBObject();
					 bdo.append("time", 1);
					 collection.createIndex(bdo);
					 bdo = new BasicDBObject();
					 bdo.append("layer", 1);
					 collection.createIndex(bdo);
					 bdo = new BasicDBObject();
					 bdo.append("service", 1);
					 collection.createIndex(bdo);
					 bdo = new BasicDBObject();
					 bdo.append("operation", 1);
					 collection.createIndex(bdo);
					 bdo = new BasicDBObject();
					 bdo.append("user", 1);
					 collection.createIndex(bdo);
				}catch( MongoCommandException e ){
					if(e.getErrorCode()==48)
						logger.info("collection already exists");
				}
				//MongoCollection<Document> collection = db.getCollection("timings");
				if(db!=null){
					if(syncMe==0){
						logger.info("add shutdown hook");
						Runtime.getRuntime().addShutdownHook(new disconnectMongo());
					}
					syncMe=1;
				}
			}
		}
	}
	static Integer syncMe=0;
	static String mongoURL="mongodb://localhost:27017/";
	static MongoClient mongoClient = null;
	static MongoDatabase db=null;
	
	
	
	public class disconnectMongo extends Thread
	{
		public void run(){
			Collector.logger.info("Disconnect from Mongo shutdown hook");
			if(mongoClient!=null) mongoClient.close();
		}
	}
	
	
	static long purgePeriod=0;
	static long lastPurge=0;
	static long purgeDelay=7l*24l*3600000l; //(7 days)
	
	static final long defaultPurgePeriod=0;
	static final long defaultPurgeDelay=7l*24l*3600000l;
	static final String defaultMongoURL="mongodb://localhost:27017/";

	
	public void setPurgeDelay(int hours){
		if(hours<1)return;
		purgeDelay=hours*3600000l;
		Collector.logger.info("purge records older than "+purgeDelay+" ms");

	}
	
	class PurgeTimer extends Thread {
		private boolean run=true;
		
		public PurgeTimer(){
			this.setDaemon(true);
			this.setName("Purge Timer");
		}
		
		
		public void run() {
			logger.info("starting purge timer");
			while(run){
				
				if(db!=null && purgePeriod!=0 && System.currentTimeMillis()-lastPurge>=purgePeriod*60000){
					//do the purge
					lastPurge=System.currentTimeMillis();
					logger.info("Purging records older than "+(new Date(lastPurge-purgeDelay)));
					DBWriter.getDBWriter().purgeRecords(lastPurge-purgeDelay);
				}else{
					logger.info("not purging:\n PurgePeriod="+purgePeriod*60000+"\n and period since last purge="+(System.currentTimeMillis()-lastPurge));
				}
				try {
					Thread.sleep(60000l);
				} catch (InterruptedException e) {
				// TODO Auto-generated catch block
					run=false;
				}
			}
			
			logger.info("purge timer stopped");
		}
		
		public void interrupt(){
			run=false;
			super.interrupt();
		}
	}
}