package networkAgent.agentUDP;

import java.io.*;
import java.net.*;

public class AgentUDPReader extends Thread
{

	protected DatagramSocket socket = null;
	protected BufferedReader in = null;
	protected boolean moreQuotes = true;

	private AgentUDPReader() throws IOException
	{
		this("NetworkThread");
	}

	private AgentUDPReader(String name) throws IOException
	{
		super(name);
		socket = new DatagramSocket(4445);
	}

	public AgentUDPReader(AgentUDP aUDP) throws SocketException
	{
		super("NetworkThread");
		agent = aUDP;
		socket = new DatagramSocket(aUDP.port);
	}

	private AgentUDP agent;
}
