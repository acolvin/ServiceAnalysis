package networkAgent.agentUDP;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.HiddenFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

public class DirectoryAgentUDP implements FileAlterationListener
{

	
	private FileAlterationMonitor monitor;
	public DirectoryAgentUDP(String directory, String suffix, String host,
			String port)
	{
		this.directory = new File(directory);
		this.suffix=suffix;
		try
		{
			this.host=host;
			address = InetAddress.getByName(host);
		} catch (UnknownHostException e)
		{
			// TODO Auto-generated catch block
			System.err.println("Host not found");
			System.exit(-2);
		}
		this.port=port;
		if(this.directory.exists() & this.directory.isDirectory())
		{
			// Create a FileFilter
		      IOFileFilter directories = FileFilterUtils.and(
		                                      FileFilterUtils.directoryFileFilter(),
		                                      HiddenFileFilter.VISIBLE);
		      IOFileFilter files       = FileFilterUtils.and(
		                                      FileFilterUtils.fileFileFilter(),
		                                      FileFilterUtils.suffixFileFilter(suffix));
		      IOFileFilter filter = FileFilterUtils.or(directories, files);

		      // Create the File system observer and register File Listeners
		      FileAlterationObserver observer = new FileAlterationObserver(this.directory, filter);
		      
		      observer.addListener(this);
		      
		      monitor = new FileAlterationMonitor();
		      monitor.addObserver(observer);
		      try
		      {
				monitor.start();
		      } catch (Exception e)
		      {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.err.println("can't start monitor");
				System.exit(-3);
		      }
		      
		}else {System.exit(-1);}
		
	}

	File directory;
	String suffix="";
	InetAddress address;
	String host,port;
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		if(args.length!=4)
		{
			System.err.println("usage: <directory> <file suffix> <host> <port>");
			System.exit(-1);
		}
		if(!(new File(args[0])).isDirectory()) 
		{
			System.err.println(args[0] +" is not a directory");
			System.exit(-1);
		}
		DirectoryAgentUDP da=new DirectoryAgentUDP(args[0],args[1],args[2],args[3]);
		System.out.println("Directory Monitor Started");
	}

	@Override
	public void onDirectoryChange(File arg0)
	{
		//not needed
		
	}

	@Override
	public void onDirectoryCreate(File arg0)
	{
		//not needed
		
	}

	@Override
	public void onDirectoryDelete(File arg0)
	{
		//end if it is this directory
		if(arg0.equals(this.directory)) System.exit(1);
		
	}

	@Override
	public void onFileChange(File arg0)
	{
		//check to see if we need to monitor and start listener
		onFileCreate(arg0);
	}

	@Override
	public void onFileCreate(File arg0)
	{
		// start listener
		
			if(!filenames.contains(arg0.getAbsolutePath()))
			{
				AgentUDP lfr=new AgentUDP(arg0.getAbsolutePath(),host,Integer.parseInt(port));
				lfr.start();
				fileAgents.add(lfr);
				filenames.add(arg0.getAbsolutePath());
				System.out.println("file added: "+arg0.getAbsolutePath());
			}
	}

	@Override
	public void onFileDelete(File arg0)
	{
		if(filenames.contains(arg0.getAbsolutePath()))
			filenames.remove(arg0.getAbsolutePath());
		System.out.println("file deleted: "+arg0.getAbsolutePath());
			//don't need to worry about the lfr it will clear itself
	}

	@Override
	public void onStart(FileAlterationObserver arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStop(FileAlterationObserver arg0)
	{
		// TODO Auto-generated method stub
		
	}
	
	private ArrayList<AgentUDP> fileAgents=new ArrayList<AgentUDP>();
	private ArrayList<String> filenames=new ArrayList<String>();

}
