package networkAgent.agentUDP;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import serviceAnalysisModel.readersWriters.*;

public class AgentUDP implements LogFileTailerListener
{
	public static void main(String[] args) throws IOException,
			InterruptedException
	{
		// new AgentUDPThread().start();
		AgentUDP a = new AgentUDP(args[0], args[1], new Integer(args[2]));
		a.start();
		while (true)
		{
			Thread.sleep(1000);
		}
	}

	public AgentUDP(String filename, String IP, int port)
	{
		this.port = port;
		name=filename;
		if (filename.equals("--"))
		{
			pReader = new PipeReader(System.in);
			pReader.addListener(this);
			readPipe = true;
			name="stdin";
		} else
		{
			file = new File(filename);
			if(file.isDirectory()) 
			{
				System.err.println("This is a directory.  Please use the directory agent");
				System.exit(-3);
			}
			else
			{
				lfr = new LogFileReader(file);
				lfr.addLogFileTailerListener(this);
			}
		}
		try
		{
			this.IP=IP;
			address = InetAddress.getByName(IP);
		} catch (UnknownHostException e)
		{
			// TODO Auto-generated catch block
			System.err.println("Host " + IP + " not found");
			e.printStackTrace();

			System.exit(-2);
		}
		try
		{
			location=InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e)
		{
			//e.printStackTrace();
			location=e.getMessage().substring(e.getMessage().length()/2+1);
			//location=null;
		}
		System.out.println(location);

	}

	private String location=null;
	private boolean readPipe = false;
	private String name="";

	public void start()
	{
		// start socket listener

		try
		{
			datagramSocket = new DatagramSocket();
		} catch (SocketException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (readPipe)
			pReader.start();
		else if(lfr!=null)
			lfr.start();
		else //start directory watching
		{
			
		}
			
	}

	DatagramSocket datagramSocket;

	@Override
	public void newLogFileLine(String line,String filename)
	{
		// TODO Auto-generated method stub
		if (se == null)
			se = new ServiceEntry(new ArrayList<ServiceEntry>());
		ExtensionHandler RTR = ExtensionHandler.getInstance();
		ServiceEntry nse = RTR.ProcessLogFileLine(line);
		
		if (nse != null)
		{
			// System.out.println("service entry found");
			// se.seList.add(nse);
			nse.filename=filename;
			if(nse.location==null)nse.location=location;
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] yourBytes;
			try
			{
				ObjectOutput out = new ObjectOutputStream(bos);
				out.writeObject(nse);
				// out.writeBytes("++end++");
				out.flush();
				out.close();
				yourBytes = bos.toByteArray();
				bos.flush();

				bos.close();
				// System.out.println(new String(yourBytes));
				DatagramPacket packet = new DatagramPacket(yourBytes,
						yourBytes.length, address, port);

				datagramSocket.send(packet);

			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@Override
	public void fileEndReached(String filename)
	{
		// TODO Auto-generated method stub

	}

	public InetAddress address;
	public ServiceEntry se = null;
	public LogFileReader lfr=null;
	public PipeReader pReader;
	public AgentUDP agent;
	public int port = 5555;
	public File file;
	public String IP = "127.0.0.1";
}
