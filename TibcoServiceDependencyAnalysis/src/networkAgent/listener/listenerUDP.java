package networkAgent.listener;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.ConcurrentLinkedQueue;

import serviceAnalysisModel.readersWriters.ServiceEntry;

//import serviceAnalysisModel.readersWriters.BWReader;

public class listenerUDP extends Thread
{
	public listenerUDP(ConcurrentLinkedQueue<ServiceEntry> queue, int port)
			throws SocketException
	{
		super("UDP Receiver");
		this.setDaemon(true);
		socket = new DatagramSocket(port);
		this.queue = queue;
		this.port = port;
	}

	private DatagramSocket socket;

	public void run()
	{

		DatagramPacket dgp = new DatagramPacket(new byte[65000], 65000);
		while (!stop)
		{
			try
			{
				socket.receive(dgp);

				ByteArrayInputStream bis = new ByteArrayInputStream(
						dgp.getData(), 0, dgp.getLength());
				ObjectInput in = new ObjectInputStream(bis);
				Object o = in.readObject();
				ServiceEntry se = (ServiceEntry) o;
				// if(se!=null &&
				// se.service!=null)System.out.println(se.service.getSName());
				if (se != null)
					queue.offer(se);
				// String received = new String(dgp.getData(), 0,
				// dgp.getLength());
				// System.out.println("Quote of the Moment: " + received);
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public boolean stop = false;
	int port;
	ConcurrentLinkedQueue<ServiceEntry> queue;
}
