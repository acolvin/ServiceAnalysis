package networkAgent.listener;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import networkAgent.agentTCP.TCPMessage;
import networkAgent.collector.Base64Coder;
import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisModel.readersWriters.ServiceEntry;

//import serviceAnalysisModel.readersWriters.BWReader;

public class ListenerTCP extends Thread
{
	public static String password="password";
	
	private boolean collector=false;

	private String host;
	
	public boolean isCollector(){
		return collector;
	}
	
	public ListenerTCP(ConcurrentLinkedQueue<ServiceEntry> queue, String host,int port)
			throws UnknownHostException, IOException
	{
		super("TCP Receiver");
		this.setDaemon(true);
		socket =  new Socket(host,port);
		this.queue = queue;
		this.port = port;
		this.host=host;
		in = new ObjectInputStream(socket.getInputStream());
		os = new PrintStream(socket.getOutputStream());

	}
	private ObjectInputStream in;
	private PrintStream os;
	private Socket socket;
	private PublicKey pubk=null;

	public void interrupt()
	{
		os.println("quit");
		os.flush();
		stop=true;	
		super.interrupt();
	}
	
	
	public char[] buildBase64EncMessage(String message){
		if(pubk==null) return null;
		Cipher cipher;
		try {
			cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, pubk);
		    byte[] enc=cipher.doFinal(message.getBytes());
		    char[] encp=Base64Coder.encode(enc);
		    return encp;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return null;
	}
	
	public void run()
	{

		Controller.getController().sendMessage("TCP Client started on port "+ socket.getLocalPort());

		while (!stop)
		{
			try
			{
				Object o=in.readObject();
				

				TCPMessage m=(TCPMessage) o;
				
				ServiceEntry se = m.se;
				// if(se!=null &&
				// se.service!=null)System.out.println(se.service.getSName());
				if (se != null){
					if(se.type==ServiceEntry.SE_TYPE_SET){
						for(ServiceEntry sei:se.seList){
							queue.offer(sei);//put them on individually
						}
					}
					else queue.offer(se);
					//System.out.println("queue size "+queue.size());
					if(queue.size()>100000){
						//System.out.println("queue size "+queue.size());
						os.println("select pause");
						os.flush();
					}
				}
				else if(m.string==null)
				{
					// if null need to do a keep-alive back
					os.println("Hello");
					os.flush();
					if(queue.size()<=100000){
						//System.out.println("queue size "+queue.size());
						os.println("select unpause");
						os.flush();
					}
					
				}
				else
				{
					//  we received a string message
					//System.out.println(m.string);
					//mark as a collector
					if(m.string.length()>=9 && m.string.equals("collector")){
						collector=true;
						System.out.println("connected to a collector");
					}
					//handle auth request here
					if(m.string.length()>=18 && m.string.substring(0, 18).equalsIgnoreCase("start authenticate"))
					{
						String s=m.string.substring(19);
						byte[] b=Base64Coder.decode(s);
						X509EncodedKeySpec keyspec=new X509EncodedKeySpec(b);
						KeyFactory kf = KeyFactory.getInstance("RSA");
						pubk=kf.generatePublic(keyspec);
						Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
					    cipher.init(Cipher.ENCRYPT_MODE, pubk);
					    byte[] enc=cipher.doFinal((password).getBytes());
					    char[] encp=Base64Coder.encode(enc);
					    os.println("user "+System.getProperty("user.name")+","+new String(encp));
					    os.flush();
					    Controller.getController().sendMessage("Authenticating against user "+System.getProperty("user.name"));
					} else if(m.string.equals("Paused")){
						if(queue.size()<=100000){
							//System.out.println("queue size "+queue.size());
							os.println("select unpause");
							os.flush();
						}
					}
					else Controller.getController().sendMessage(m.string);
					
					
				}
				
			} catch(EOFException e){
				stop=true;
				Controller.getController().sendMessage("TCP Client stopped");
			}catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				Controller.getController().sendMessage("TCP Client stopped");

				stop=true;
			} catch (ClassNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				Controller.getController().sendMessage("TCP Client stopped");

				stop=true;
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				stop=true;
				Controller.getController().sendMessage("Failed to read public key");
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				stop=true;
				Controller.getController().sendMessage("Failed to read public key");
				e.printStackTrace();
			} catch (NoSuchPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				stop=true;
				Controller.getController().sendMessage("Failed to encrypt password");
				
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				stop=true;
				Controller.getController().sendMessage("Failed to encrypt password");
				e.printStackTrace();
			} catch (IllegalBlockSizeException e) {
				// TODO Auto-generated catch block
				stop=true;
				Controller.getController().sendMessage("Failed to encrypt password");
				e.printStackTrace();
			} catch (BadPaddingException e) {
				// TODO Auto-generated catch block
				stop=true;
				Controller.getController().sendMessage("Failed to encrypt password");
				e.printStackTrace();
			}
		}
		
		os.println("quit");

	}

	public boolean stop = false;
	int port;
	ConcurrentLinkedQueue<ServiceEntry> queue;

	public void sendcommand(String command) {
		os.println(command);
		os.flush();
		
	}
	
	public String toString(){
		return host+":"+port;
	}
}
