package networkAgent.listener;

import java.util.concurrent.ConcurrentLinkedQueue;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.jms.JMSException;
import javax.jms.Destination;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.Queue;
import javax.jms.ObjectMessage;
import javax.jms.Message;

import serviceAnalysisModel.readersWriters.ServiceEntry;

//import serviceAnalysisModel.readersWriters.BWReader;

public class listenerJMS extends Thread
{
	public listenerJMS(ConcurrentLinkedQueue<ServiceEntry> queue)
	{
		super("JMS Receiver");
		this.setDaemon(true);
		System.out.println("creating jms listener");
		this.queue = queue;

		try
		{
			context = new InitialContext();
			factory = (ConnectionFactory) context.lookup(factoryName);
			dest = (Destination) context.lookup(destName);
			// a.queue = (Queue) a.context.lookup(a.destName);
			connection = factory.createConnection();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			stop = false;

		} catch (NamingException e)
		{
			// TODO Auto-generated catch block
			System.out.println("No JNDI available");
		} catch (JMSException e)
		{
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.out.println("error configuring the JMS connection");
		}
	}

	Context context = null;
	ConnectionFactory factory = null;
	Connection connection = null;
	String factoryName = "connectionFactory";
	String destName = "SAevent";
	Destination dest = null;
	Queue jmsqueue = null;
	Session session = null;

	public void run()
	{

		MessageConsumer receiver = null;

		try
		{
			if (session != null)
				receiver = session.createConsumer(dest);
		} catch (JMSException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try
		{
			if (receiver != null)
				connection.start();
			else
				System.out.println("no receiver");
		} catch (JMSException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		while (!stop & connection != null)
		{

			Message m;
			try
			{
				// System.out.println("waiting for message");
				m = receiver.receive(500L);

				if (m != null && m instanceof ObjectMessage)
				{
					// System.out.println("got a message");
					Object o = ((ObjectMessage) m).getObject();
					ServiceEntry se = (ServiceEntry) o;
					if (se != null)
						queue.offer(se);
				}
			} catch (JMSException e)
			{

				if (e instanceof javax.jms.IllegalStateException
						| e.getCause() instanceof java.io.EOFException)
				{// handles jms server being closed
					connection = null;
					stop = true;
					System.out
							.println("lost connection to JMS server.  Closing JMS listener");
				} else
					e.printStackTrace();
			}

		}

		if (LE != null)
			LE.closed();
		try
		{
			if (connection != null)
			{
				connection.stop();
				connection.close();
				session.close();
				context.close();
			}
		} catch (JMSException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void registerListener(ListenerEvent l)
	{
		LE = l;
	}

	ListenerEvent LE = null;
	public boolean stop = true; // will be set when to false when the connection
								// is setup
	int port;
	ConcurrentLinkedQueue<ServiceEntry> queue;
}
