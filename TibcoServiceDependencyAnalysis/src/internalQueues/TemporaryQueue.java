package internalQueues;

public class TemporaryQueue extends Queue {

	protected TemporaryQueue(String name) throws QueueNamingException, QueueAlreadyExistsException
	{	
		
		super();
		if(name==null | name.equals(""))
				throw new QueueNamingException("Queue Must Have a Name");
		if(QueueServer.getInstance().getQueue(name)!=null)throw new QueueAlreadyExistsException();
		this.name=name;

	}
}
