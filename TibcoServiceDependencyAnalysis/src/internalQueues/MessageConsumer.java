package internalQueues;

 public class MessageConsumer {
	protected MessageConsumer(Queue q)
	{
		queue=q;
	}
	Queue queue=null;
	
	public Message receive() throws QueueDeletedException, ServerRunningException
	{
		return queue.receiveMessage();
	}
	
	public Message receive(long l) throws ServerRunningException, QueueDeletedException
	{
		return queue.receiveMessage(l);
	}
}
