package internalQueues;

import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;

public class QueueServer {

	private QueueServer()
	{
		
	}
	
	public static QueueServer getInstance()
	{
		if(instance==null) instance=new QueueServer();
		return instance;
	}
	
	private static QueueServer instance=null; 
	
	private ConcurrentHashMap<String,Queue> queues=new ConcurrentHashMap<String,Queue>();
	
	public synchronized void addQueue(Queue q)
	{
		if(!queues.containsKey(q.getQueueName()))queues.put(q.getQueueName(), q);
	}
	
	public synchronized Queue addQueue(String queueName) throws QueueNamingException, ServerRunningException
	{
		if(!queues.containsKey(queueName))queues.put(queueName, new Queue(queueName));
		return queues.get(queueName);
	}

	
	public Queue getQueue(String name)
	{
		return queues.get(name);
	}
	
	public synchronized TemporaryQueue createTemporaryQueue()
	{
		String name=null;
		TemporaryQueue q;
		do
		{
			name="TQ"+(++number);
			if(queues.containsKey(name)) name=null;
			try {
				q = new TemporaryQueue(name);
				addQueue(q);
				return q;
			} catch (QueueNamingException  e) {
				//just should not happen but go round again if it does
				name=null;
			}catch (QueueAlreadyExistsException e)
			{
				name=null;
			}
			
		}while(name==null);
		return null;
		
	}
	
	public void deleteQueue(String name)
	{
		if(queues.containsKey(name))
		{
			queues.get(name).clearQueue();
			queues.remove(name);
		}
	}
	
	public void dropAllQueues()
	{
		Enumeration<Queue> e = queues.elements();
		while(e.hasMoreElements())
		{
			e.nextElement().clearQueue();
		}
		queues.clear();
	}
	
	public void clearAllQueues()
	{
		Enumeration<Queue> e = queues.elements();
		while(e.hasMoreElements())
		{
			e.nextElement().clearQueue();
		}
	}
	private int number=1;
	
	public Session createSession()
	{
		return new Session();
	}
	
	private boolean running=false;
	
	public boolean isRunning() {return running;}
	public void stop() {running=false;}
	public void start() 
	{
		clearAllQueues();
		running=true;
	}
	
}
