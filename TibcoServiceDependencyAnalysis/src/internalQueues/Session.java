package internalQueues;

public class Session {
	
	public MessageConsumer createConsumer(Queue q)
	{
		return new MessageConsumer(q);
	}
	
	public MessageProducer createProducer(Queue q)
	{
		return new MessageProducer(q);
	}
	
	
	public TextMessage createTextMessage(String m)
	{
		TextMessage s=new TextMessage(m);
		
		return s;
	}
	public Message createMessage()
	{
		return new Message();
	}
	
	public TemporaryQueue createTemporaryQueue()
	{
		return QueueServer.getInstance().createTemporaryQueue();
	}
}
