package internalQueues;

public  class MessageProducer {

	protected MessageProducer(Queue q)
	{
		queue=q;
	}
	Queue queue=null;
	
	public boolean send(Message m) throws ServerRunningException, QueueDeletedException
	{
		if(queue==null) return false;
		if(!QueueServer.getInstance().isRunning())throw new ServerRunningException("Server has been stopped");
		if(queue.deleted)throw new QueueDeletedException("Queue is marked for deletion");
		return queue.addMessage(m);
	}
	
	public void setTimeToLive(long l)
	{
		if(l<=0) ttl=null;
		else ttl=l;
	}
	
	private Long ttl=null;
}
