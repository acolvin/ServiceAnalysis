package serviceAnalysisUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class ReportCreator extends JFrame implements WindowListener, ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7866551476441666879L;
	public ReportCreator()
	{
		this.setTitle("Report Creator");
		if(MainForm.image!=null)
			setIconImage(MainForm.image);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.addWindowListener(this);
	}
	
	public void showUI()
	{
		JPanel jp=new JPanel(new BorderLayout());
		jp.setPreferredSize(new Dimension(400,400));
		setContentPane(jp);
		jp.add(reportName,BorderLayout.NORTH);
		jp.add(OK,BorderLayout.SOUTH);
		JPanel mainPanel=new JPanel();
		jp.add(mainPanel);
		
		
		setBounds(300,200,400,400);
       	//System.out.println(output);
       	//output.append("test");
    	javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
		    	pack();  
				f.setVisible(true);
				
			}
		});
    	
	}
	
	JButton OK=new JButton("OK");
	JFrame f=this;
	JTextField reportName=new JTextField();
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
