package serviceAnalysisUI;

import javax.swing.RowFilter;
import serviceAnalysisModel.Service;
import serviceAnalysisUI.ServiceTableModel;


public class UIRowFilter extends RowFilter< ServiceTableModel,Integer>
{

	private String layerFilter = "";
	private String serviceFilter = "";
	private boolean modelled = true;
	private boolean highlighted=false;

	public UIRowFilter(String layer, String service)
	{
		this.layerFilter = layer;
		this.serviceFilter = service;
	}

	public UIRowFilter(String layer, String service, boolean modelled)
	{
		this.layerFilter = layer;
		this.serviceFilter = service;
		this.modelled = modelled;
	}

	public void filterHighlighted(boolean highlight)
	{
		highlighted=highlight;
		//System.out.println("highlight is "+highlight);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.RowFilter#include(javax.swing.RowFilter.Entry)
	 */
	@Override
	public boolean include(javax.swing.RowFilter.Entry<? extends ServiceTableModel,? extends Integer> entry)
	{
		boolean response = true;
		ServiceTableModel tableModel = entry.getModel();
		Integer row= entry.getIdentifier();		
		Service serv = tableModel.getService(row);

		if (!layerFilter.equals("ALL"))
		{
			if (!serv.getType().equals(layerFilter))
			{
				response = false;
			}
		}

		if (serviceFilter!=null &&!serviceFilter.equals(""))
		{
			//if (!serv.getSName().startsWith(serviceFilter))
			//{
			//	response = false;
			//}
			try{
			//boolean match=serv.getSName().matches(serviceFilter);
			if (!(serv.getSName().matches(serviceFilter) | serv.getSName().startsWith(serviceFilter)))
			{
				response = false;
			}
			}catch (java.util.regex.PatternSyntaxException e)
			{
				response=false;
				MainForm.sbar.setMessage(e.getMessage());
				System.out.println(e.getMessage());
			}
		}

		if (modelled != serv.isModelled())
			response = false;
		//System.out.println(highlighted+" "+response+" "+(highlighted & !tableModel.isHighlighted(row)));
		if(highlighted & !tableModel.isHighlighted(row) )response=false;
		//System.out.println("returning "+response);
		return response;
	}



	/*
	 * public boolean include() { }
	 */
}
