package serviceAnalysisUI;

import java.util.List;

import javax.swing.SwingWorker;

import database.DBTransaction;
import database.TransactionProgressListener;

import serviceAnalysisModel.SAControl.Controller;

public class SaveSnapshotWorker extends SwingWorker<Void, Integer> implements TransactionProgressListener
{

	public void setDesc(String desc){ this.desc=desc;}
	public String desc;
	private long start;
	public void addStatusBar(StatusBar sb)
	{
		this.sb=sb;
		sb.showProgress();
	}
	private StatusBar sb=null;

	DBTransaction dbt=null;


	@Override
	protected Void doInBackground()
	{
		
		dbt=new DBTransaction();
		dbt.addListener(this);
		start=System.currentTimeMillis();
		Controller.getController().saveTimings(desc,dbt);
		return null ;
	}
	
	protected void process(List<Integer> ps)
	{
		if(sb!=null) sb.setStatusBarValue(ps.get(ps.size()-1));
		//System.out.println("process: ");
	}

	protected void done()
	{
		
		if(sb!=null)
		{
			sb.setStatusBarValue(100);
			sb.removeProgress();
		}
		MainForm.sbar.setMessage("Save Snapshot Complete "
				+ desc + " in "+ (System.currentTimeMillis()-start) +" ms");

	}
	
	private int currentvalue=-1;
	@Override
	public void progressEvent(int progressPercent) {
		// TODO Auto-generated method stub
		if(currentvalue!=progressPercent)
		{
			System.out.println("Progress %: "+progressPercent);
			publish(progressPercent);
			currentvalue=progressPercent;
		}
	}



}
