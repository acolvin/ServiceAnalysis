package serviceAnalysisUI;

import javax.imageio.ImageIO;
import javax.swing.*;

import org.apache.derby.impl.store.raw.data.BufferedByteHolderInputStream;

import serviceAnalysisModel.Service;
//import serviceAnalysisModel.ServiceMapper;
import serviceAnalysisModel.VolumetricModel;
//import serviceAnalysisModel.VolumetricType;
//import serviceAnalysisModel.Workstream;
import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisModel.SAControl.DataModelChangeListener;
import serviceAnalysisModel.SAControl.FileMonitorChangeListener;
import serviceAnalysisModel.SAControl.MonitorChangeEvent;
import serviceAnalysisModel.SAControl.MonitorWindowListener;
import serviceAnalysisModel.SAControl.PDFReport;
import serviceAnalysisModel.SAControl.StartMonitorWindowEvent;
import serviceAnalysisModel.Transaction.EventCollection;
import serviceAnalysisModel.readersWriters.*;
import serviceAnalysisUI.Comparison.SnapshotComparison;
import serviceAnalysisUI.selectionWindow.TCPConnectDialog;
import serviceAnalysisUI.selectionWindow.TimingSelectionDialog;
import serviceAnalysisUI.selectionWindow.TimingSelectionStopDialog;
import serviceAnalysisUI.transaction.TransactionView;
//import javax.swing.event.*;
//import javax.swing.table.AbstractTableModel;
//import javax.swing.table.TableRowSorter;




import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
//import java.awt.Toolkit;
//import java.awt.Component;
import java.awt.Frame;

//import java.net.SocketException;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
//import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
//import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

/**
 * This class implements the main form of the service analysis application
 * 
 * @author apc
 * 
 */
public class MainForm implements ActionListener, ItemListener, WindowListener,
		FileMonitorChangeListener, DataModelChangeListener,
		MonitorWindowListener, EventCollection.CollectionListener
{

	/**
	 * Interface to receive notifications when the snapshot selection dialogue
	 * is closed either with a selection or cancel
	 * 
	 * @author apc
	 * 
	 */
	public interface SnapShotSelectionListener
	{
		public void SnapShotAction(boolean b, Date d);
	}

	/**
	 * Inner class that implements the snapshot selection dialog. A snapshot is
	 * a saved collection of service events. The dialogue works like the file
	 * selector
	 * 
	 * @author apc
	 * 
	 */
	public class SnapShotSelection implements ActionListener, MouseListener,
			WindowListener
	{
		JList dataList;
		Map<Date, String> snapshots;
		public Date snapshot = null;
		private Boolean b = null;
		private JFrame jf;

		/**
		 * @param snapShots
		 * @param l
		 */
		SnapShotSelection(Map<Date, String> snapShots,
				SnapShotSelectionListener l)
		{
			snapshots = snapShots;
			ArrayList<String> p = new ArrayList<String>();
			for (Date d : snapshots.keySet())
			{
				String s = snapshots.get(d);
				if (s == null || s.isEmpty())
				{
					s = d.toString();
				}
				p.add(s);
			}
			dataList = new JList(p.toArray());
			dataList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			listener = l;
		}

		/**
		 * Displays the snapshot dialogue and sets this inner class to listen
		 * for window and mouse events
		 */

		public String snapshotButton = "Load Snapshot";

		public void makeSelection()
		{
			JScrollPane scrollPane = new JScrollPane(dataList);
			jf = new JFrame("Select a snapshot");
			if(MainForm.image!=null) jf.setIconImage(MainForm.image);
			jf.addWindowListener(this);
			JButton jb = new JButton(snapshotButton);
			jb.setActionCommand("load");
			jb.addActionListener(this);
			JButton jbc = new JButton("Cancel");
			jbc.setActionCommand("cancel");
			jbc.addActionListener(this);

			JPanel lp = new JPanel();
			JPanel lpb = new JPanel();
			lp.setLayout(new BoxLayout(lp, BoxLayout.PAGE_AXIS));
			lpb.setLayout(new BoxLayout(lpb, BoxLayout.LINE_AXIS));
			lp.add(scrollPane);

			lpb.add(jb);
			lpb.add(jbc);
			lp.add(lpb);
			jf.add(lp);
			jf.setSize(300, 200);
			jf.setMinimumSize(new Dimension(300, 200));
			jf.setLocation(100, 100);

			dataList.addMouseListener(this);
			jf.setVisible(true);

		}

		private SnapShotSelectionListener listener;

		public void addListener(SnapShotSelectionListener l)
		{
			listener = l;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			// TODO Auto-generated method stub
			if ("load".equals(e.getActionCommand()))
			{
				// load selected
				// System.out.println("load selected");
				int i = dataList.getSelectedIndex();
				if (i != -1)
				{
					String s = (String) dataList.getSelectedValue();

					for (java.util.Map.Entry<Date, String> es : snapshots
							.entrySet())
					{
						if (es.getValue().equalsIgnoreCase(s))
							snapshot = es.getKey();
					}
					// snapshot = snapshots.get(dataList.getSelectedIndex());
					b = true;
				} else
					b = false;
				jf.setEnabled(false);
				jf.dispose();

				listener.SnapShotAction(b, snapshot);
			} else
			{
				// cancel selected
				snapshot = null;
				jf.setEnabled(false);
				jf.dispose();
				b = false;
				listener.SnapShotAction(b, snapshot);
			}

		}

		@Override
		public void mouseClicked(MouseEvent e)
		{
			// TODO Auto-generated method stub
			if (e.getClickCount() == 2)
			{
				int index = dataList.locationToIndex(e.getPoint());
				// System.out.println("Double clicked on Item " + index);
				@SuppressWarnings("unchecked")
				Entry<Date, String> entry = (Entry<Date, String>) snapshots.entrySet().toArray()[index];

				snapshot = entry.getKey();
				jf.setEnabled(false);
				jf.dispose();
				b = true;
				listener.SnapShotAction(b, snapshot);
			}
		}

		@Override
		public void mouseEntered(MouseEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void windowActivated(WindowEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void windowClosed(WindowEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void windowClosing(WindowEvent e)
		{
			// TODO Auto-generated method stub
			listener.SnapShotAction(false, null);
		}

		@Override
		public void windowDeactivated(WindowEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void windowDeiconified(WindowEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void windowIconified(WindowEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void windowOpened(WindowEvent e)
		{
			// TODO Auto-generated method stub

		}
	}

	String layerFilter = "ALL";
	String serviceFilter = "";
	private BWReader bwReader;
	private JFileChooser fc = new JFileChooser();

	public class FileChooserListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			// TODO Auto-generated method stub

		}

	}

	private class GraphPoller extends Thread
	{
		public GraphPoller()
		{
			super("Graph Poller");
			this.setDaemon(true);
		}
		
		public void run()
		{
			while(true){
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {}
			for (RealtimeGraph rtg : RTGraphs)
			{
				rtg.sync();
			}
			}
		}
	}
	
	/**
	 * This method builds the window of the application
	 * 
	 * 
	 */
	public void createGUI()
	{
		// Create and set up the window.
		wl = this;

		//SystemOutWindow sow=new SystemOutWindow();
		//sow.showUI();
		sbar=new StatusBar();
		mainFrame = new JFrame("ASAP ("+StatusBarText.getVersion()+") - Service Table");
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		java.net.URL imageURL  = ClassLoader.getSystemResource("menuicon.png");
		if(imageURL==null)System.out.println("Can't find image");
		else
			try {
				image=ImageIO.read(imageURL);
				
				mainFrame.setIconImage(image);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		mainFrame.addWindowListener(this);

		// Create and set up the content pane.
		JPanel newContentPane2 = new JPanel(new BorderLayout());
		JPanel cpbar = new JPanel(new BorderLayout());
		newContentPane = new ServiceTable(SO);
		newContentPane.main=this;
		// newContentPane2.setLayout(new BorderLayout());
		newContentPane.setOpaque(true); // content panes must be opaque
		newContentPane2.add(newContentPane);// ,java.awt.BorderLayout.NORTH);
		sbar.setOpaque(true);
		sbar.setVisible(true);
		sbar.setMessage("Ready and listening on UDP port "+Controller.getController().getUDPPort());
		cpbar.setOpaque(true);
		cpbar.setVisible(true);
		cpbar.add(sbar, java.awt.BorderLayout.PAGE_START);
		cpbar.setOpaque(true);
		cpbar.setVisible(true);
		newContentPane2.add(cpbar, java.awt.BorderLayout.SOUTH);
		newContentPane2.setOpaque(true);
		// JDesktopPane dtopFrame=new JDesktopPane();
		newContentPane2.validate();
		mainFrame.setContentPane(newContentPane2);
		// frame.setContentPane(dtopFrame);

		JMenu file = new JMenu("File");
		file.setMnemonic('F');
		// file.add(createMenuItem("Load Model", this, "LoadModel", 'L',
		// KeyEvent.VK_L));
		// file.add(createMenuItem("Save Model", this, "SaveModel", 'S',
		// KeyEvent.VK_S));
		// file.addSeparator();
		file.add(createMenuItem("Reset Timings", this, "ResetTimings", 'R',
				KeyEvent.VK_R));
		file.add(createMenuItem("Clear Services", this, "ClearServices", 'C',
				KeyEvent.VK_C));
		file.addSeparator();
		file.add(createMenuItem("Save Snapshot", this, "SaveTimings", 'V',
				KeyEvent.VK_V));
		file.add(createMenuItem("Load Snapshot", this, "LoadTimings", 'T',
				KeyEvent.VK_T));
		file.add(createMenuItem("Compare Snapshot", this, "CompareTimings",
				'D', KeyEvent.VK_D));
		file.add(createMenuItem("Remove Snapshot", this, "DeleteTimings",
				'R', KeyEvent.VK_R));
		file.addSeparator();
		file.add(createMenuItem("Load File", this, "LoadBW", 'N', KeyEvent.VK_N));
		file.add(createMenuItem("Load Matches", this, "LoadMatches", 'M',
				KeyEvent.VK_M));
		file.addSeparator();
		file.add(createMenuItem("Exit", this, "Exit", 'Q',
				KeyEvent.VK_Q));
		// file.add(createMenuItem("Load Tibco Timings", this, "LoadBWTimings",
		// 'T', KeyEvent.VK_T));
		// file.add(createMenuItem("Load DB Timings", this, "LoadDBTimings",
		// 'D',
		// KeyEvent.VK_D));

		JMenu filter = new JMenu("Filter");
		filter.setMnemonic('i');
		JMenu layer = new JMenu("Layer Filter");
		layer.setMnemonic('L');
		layer.add(createMenuItem("All", this, "FilterLayer_ALL", 'A',
				KeyEvent.VK_A));
		layer.add(createMenuItem("UI Only", this, "FilterLayer_UI", 'U',
				KeyEvent.VK_U));
		layer.add(createMenuItem("Tibco Only", this, "FilterLayer_BW", 'T',
				KeyEvent.VK_T));
		layer.add(createMenuItem("DB Only", this, "FilterLayer_DB", 'D',
				KeyEvent.VK_D));
		layer.add(createMenuItem("Not Modelled", this, "FilterNotModelled",
				'N', KeyEvent.VK_N));
		

		JMenuItem highlight = createMenuItem("Show Highlighted", this,
				"FilterHighlight", 'H', KeyEvent.VK_H);
		JMenuItem service = createMenuItem("Service Filter", this,
				"FilterService", 'S', KeyEvent.VK_S);
		

		JMenuItem fileFilter = createMenuItem("Filter File Events", this,
				"FilterDates", 'F', KeyEvent.VK_F);
		JMenuItem matchesFilter = createMenuItem("Manage Matches", this,
				"ManageMatches", 'M', KeyEvent.VK_M);
		
		
		JMenu windows = new JMenu("Windows");
		windows.setMnemonic('W');

		// JMenu layer = new JMenu("Model");

		windows.add(jVModel = createToggleMenuItem("Volumetric Model", this,
				"Toggle_Vol", 'V', KeyEvent.VK_V));

		jFreq = createMenuItem("Service Frequency", this, "Frequency",
				'F', KeyEvent.VK_F);
		//		jFreq = createToggleMenuItem("Service Frequency", this, "Toggle_Freq",
		//		'F', KeyEvent.VK_F);
		jSTimes = createToggleMenuItem("Top Service Times", this,
				"Toggle_STimes", 'T', KeyEvent.VK_T);
		
		jSDView = createMenuItem("Service Dependencies", this,
				"SDView", 'M', KeyEvent.VK_M);
		//jSDView = createToggleMenuItem("Service Dependencies", this,
		//		"Toggle_SDView", 'M', KeyEvent.VK_M);
		
		windows.add(jSTimes);
		windows.add(createMenuItem("Transaction View", this,
				"Transaction_View", 'X', KeyEvent.VK_X));
		windows.addSeparator();
		windows.add(jSDView);
		windows.add(jFreq);
		windows.add(createMenuItem("Real Time Graph", this,
				"Monitor_Graph_BW", 'G', KeyEvent.VK_G));
		windows.add(createMenuItem("Quantised View", this,
				"Count_Graph_BW", 'C', KeyEvent.VK_C));
		
		JMenuItem stdout = createMenuItem("Open System.out", this,
				"stdout", 'F', KeyEvent.VK_O);
		JMenuItem saveWindows = createMenuItem("Save Window Settings", this,
				"saveWindows", 'S', KeyEvent.VK_S);
		
		windows.addSeparator();
		windows.add(stdout);
		windows.add(saveWindows);
		
		filter.add(layer);
		filter.add(service);
		filter.add(highlight);
		filter.add(fileFilter);
		filter.add(matchesFilter);

		monitoring = new JMenu("Monitoring...");
		realtime = new JMenu("Real Time");
		realtime.setMnemonic('R');
		realtime.add(createMenuItem("Monitor File", this, "Monitor_File_BW",
				'M', KeyEvent.VK_M));
		realtime.add(createMenuItem("Load Monitor File List", this,
				"Monitor_List_BW", 'L', KeyEvent.VK_L));
		realtime.add(monitoring);
		realtime.addSeparator();
		realtime.add(createMenuItem("Reset Rate Meters", this, "Reset_Rates",
				'S', KeyEvent.VK_S));
		realtime.addSeparator();
		realtime.add(createMenuItem("Real Time Graph", this,
				"Monitor_Graph_BW", 'G', KeyEvent.VK_G));
		realtime.add(createMenuItem("Quantised View", this,
				"Count_Graph_BW", 'C', KeyEvent.VK_C));
		realtime.addSeparator();
		realtime.add(createMenuItem("Transaction View", this,
				"Transaction_View", 'T', KeyEvent.VK_T));

		capture=createToggleMenuItem("Capture Transactions", this,
				"CaptureTxns", 'C', KeyEvent.VK_C);
		realtime.add(capture);

		realtime.add(createMenuItem("Clear Transactions", this,
				"Transaction_Clear", 'D', KeyEvent.VK_D));

		JMenu reports = new JMenu("Reports");
		reports.add(createMenuItem("Text Report",this,"Text_Report",'W',KeyEvent.VK_W));
		reports.add(createMenuItem("PDF Service Report",this,"PDF_Service_Report",'P',KeyEvent.VK_P));

		JMenu collectorMenu = new JMenu("TCP Collector");
		collectorMenu.add(createMenuItem("Connect...",this,"TCPCONNECT",'C',KeyEvent.VK_C));
		collectorMenu.add(createMenuItem("Select Timings",this,"SELECT_TIMINGS",'S',KeyEvent.VK_S));
		collectorMenu.add(createMenuItem("Manage Collector",this,"MANAGE_COLLECTOR",'M',KeyEvent.VK_M));
		
		
		JMenuBar menu = new JMenuBar();
		menu.add(file);
		menu.add(filter);
		menu.add(windows);
		menu.add(realtime);
		menu.add(reports);
		menu.add(collectorMenu);
		mainFrame.setJMenuBar(menu);

		// JInternalFrame iFrame=new JInternalFrame();
		// iFrame.setContentPane(newContentPane);

		// Display the window.
		// iFrame.pack();
		// iFrame.setVisible(true);

		mainFrame.setBounds(0, 0, 900, 900);
		sbar.setPerferredWidth(896);
		// iFrame.setBounds(5, 5, 700, 700);
		// dtopFrame.setBounds(10, 10, 800, 800);
		// dtopFrame.add(iFrame);

		mainFrame.pack();
		// frame.setVisible(true);
		graphPoller.start();
		//controller.loadAutoRun();
	}

	private JCheckBoxMenuItem capture;
	private GraphPoller graphPoller=new GraphPoller();
	public static StatusBar sbar = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 * 
	 * Listens for menu item selection and processes these selections
	 */
	public void actionPerformed(ActionEvent event)
	{
		//this next line takes care of the user not being able to select the service when there is only one
		if(newContentPane.selectedService==null && SO!=null &&SO.size()>0)newContentPane.selectedService=SO.get(0);
		JMenuItem item = (JMenuItem) event.getSource();
		String cmd = item.getActionCommand();
		openSDView(event);
		openCFG(event);
		saveWindows(event);
		if(cmd.equals("stdout"))
		{
			SystemOutWindow sow=new SystemOutWindow();
			sow.showUI();
		}
		if(cmd.equals("Text_Report"))
		{
			//System.out.println("output window");
			ReportWindow rw=new ReportWindow();
			rw.showUI();
		}
		if(cmd.equals("PDF_Service_Report"))
		{
			Service selected=newContentPane.selectedService;
			if(selected!=null)
			{
				PDFReport aaa = new PDFReport(selected);
				aaa.produceServiceReport(newContentPane
						.getCumulativeFrequencyChart());
			}
		}
		if(cmd.equals("TCPCONNECT")){
			TCPConnectDialog d = new TCPConnectDialog();
			//System.out.println(d.getCommand());
		}
		if(cmd.equals("SELECT_TIMINGS")){
			TimingSelectionDialog d = new TimingSelectionDialog();
			//System.out.println(d.getCommand());
		}
		if(cmd.equals("MANAGE_COLLECTOR")){
			TimingSelectionStopDialog d = new TimingSelectionStopDialog();
			//System.out.println(d.getCommand());
		}
		if(cmd.equalsIgnoreCase("Exit"))
		{
			for(Frame f:JFrame.getFrames()) f.dispose();
			controller.shutdown();
		}
		if (cmd.equalsIgnoreCase("FilterDates"))
		{
			System.out.println("time filter selected");
			TimeFilterDialog tfd = new TimeFilterDialog();
			tfd.showDialog(mainFrame);
		}
		if(cmd.equalsIgnoreCase("ManageMatches"))
		{
			MapperWindow mw=new MapperWindow();
			mw.setVisible(true);
			mw.setBounds(50, 50, 600, 500);
		}
		if (cmd.equalsIgnoreCase("ClearServices"))
		{
			// bwReader.pauseReaders();

			layerFilter = "ALL";
			serviceFilter = "";
			highlightFilter=false;
			setFilter("ALL", "", true);
			sbar.setFiltered(false);
			controller.ClearServices();
			// SO.clear();
			// MF.clearModel();
			// ServiceTable.model.fireTableDataChanged();
			// newContentPane.repaint();
			// ttg.redrawGraph(SO);
			// bwReader.restartReaders();
		}
		if (cmd.equalsIgnoreCase("Transaction_View"))
		{
			TransactionView g = new TransactionView();
			g.createGUI();
			// g.setBounds(50, 50, 800, 600);
			g.ShowUI();
		}
		if (cmd.equalsIgnoreCase("Transaction_Clear"))
		{
			EventCollection.getInstance().clear();
		}
		if (cmd.equalsIgnoreCase("ResetTimings"))
		{
			// bwReader.pauseReaders();
			// for (Service s : SO)
			// {
			// s.resetService();
			// s.notifyEvents();
			// }
			controller.resetServiceTimings();
			ttg.redrawGraph(SO);
			// bwReader.restartReaders();
		}
		if (cmd.equalsIgnoreCase("SaveTimings"))
		{

			String desc = JOptionPane
					.showInputDialog("Enter Snapshot's Description");
			
			if(desc!=null)
			{
				//long start=System.currentTimeMillis();
				MainForm.sbar.setMessage("Saving Snapshot " + desc);
				SaveSnapshotWorker sw=new SaveSnapshotWorker();
				sw.setDesc(desc);
				sw.addStatusBar(sbar);
				sw.execute();
				//MainForm.sbar.setMessage("Save Snapshot Complete "
				//		+ desc + " in "+ (System.currentTimeMillis()-start) +" ms");

				//controller.saveTimings(desc);
			}
		}
		if(cmd.startsWith("DeleteTimings"))
		{
			Map<Date, String> snapShots = bwReader.getSnapshots();

			SnapShotSelectionListener snsl = new SnapShotSelectionListener()
			{

				@Override
				public void SnapShotAction(boolean b, Date d)
				{
					// TODO Auto-generated method stub
					if (b)
					{
						// handle delete code here
						Controller.getController().deleteTimings(d);
						sbar.setMessage("Snapshot "+d+" deleted");
					}

				}

			};

			SnapShotSelection sns = new SnapShotSelection(snapShots, snsl);
			sns.snapshotButton = "Delete Snapshot";
			sns.makeSelection();
		}

		if (cmd.startsWith("LoadTimings"))
		{
			// System.out.println("load timings selected");

			Map<Date, String> snapShots = bwReader.getSnapshots();

			SnapShotSelectionListener snsl = new SnapShotSelectionListener()
			{

				@Override
				public void SnapShotAction(boolean b, Date d)
				{
					// TODO Auto-generated method stub
					if (b)
					{
						// System.out.println(d + " selected");
						MainForm.sbar.setMessage("Loading Snapshot " + d);
						controller.loadTimings(d);
						ttg.redrawGraph(SO);
						MainForm.sbar.setMessage("Loaded Snapshot " + d);
					}

				}

			};

			SnapShotSelection sns = new SnapShotSelection(snapShots, snsl);
			sns.snapshotButton = "Load Snapshot";
			sns.makeSelection();

		}
		if (cmd.equals("CompareTimings"))
		{
			// ask user for a snapshot
			Map<Date, String> snapShots = bwReader.getSnapshots();

			SnapShotSelectionListener snsl = new SnapShotSelectionListener()
			{

				@Override
				public void SnapShotAction(boolean b, Date d)
				{
					// TODO Auto-generated method stub
					if (b)
					{
						// handle compare code here
						SnapshotComparison sc = new SnapshotComparison(SO, d);
						sc.ShowUI();
						// System.out.println("comparing against snapshot "+d);
					}

				}

			};

			SnapShotSelection sns = new SnapShotSelection(snapShots, snsl);
			sns.snapshotButton = "Compare Snapshot";
			sns.makeSelection();

		}
		if (cmd.equalsIgnoreCase("LoadBW"))
		{
			fc.setMultiSelectionEnabled(true);
		    

		    //File[] files = chooser.getSelectedFiles();
			
			int returnVal2 = fc.showOpenDialog(mainFrame);
			if (returnVal2 == JFileChooser.APPROVE_OPTION)
			{
				File[] files = fc.getSelectedFiles();
				for(File ff:files){
				//File ff = fc.getSelectedFile();
					try
					{
						controller.loadFile(ff.getCanonicalPath());
					} catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		}

		if (cmd.equalsIgnoreCase("LoadMatches"))
		{
			int returnVal2 = fc.showOpenDialog(mainFrame);
			if (returnVal2 == JFileChooser.APPROVE_OPTION)
			{
				File ff = fc.getSelectedFile();
				try
				{
					controller.loadMatchFile(ff.getCanonicalPath());

				} catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

		if (cmd.startsWith("Monitor_"))
		{
			if (cmd.contains("File"))
			{
				// System.out.println("monitor file clicked");
				int returnVal = fc.showOpenDialog(mainFrame);
				if (returnVal == JFileChooser.APPROVE_OPTION)
				{
					File f = fc.getSelectedFile();

					try
					{
						if (!menuItems.containsKey(f.getCanonicalPath()))
						{
							controller.monitorFile(f.getCanonicalPath());
							/*
							 * JMenuItem j =
							 * createMenuItem(f.getCanonicalPath(), this,
							 * "Monitor_Tail-" + f.getCanonicalPath(), 'T',
							 * KeyEvent.VK_T); monitoring.add(j);
							 * menuItems.put(f.getCanonicalPath(), j);
							 */
						}
					} catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			} else if (cmd.contains("List"))
			{
				// list file selected
				int returnVal = fc.showOpenDialog(mainFrame);
				if (returnVal == JFileChooser.APPROVE_OPTION)
				{
					File f = fc.getSelectedFile();
					try
					{
						FileReader fr = new FileReader(f);
						BufferedReader br = new BufferedReader(fr);
						String s;
						while ((s = br.readLine()) != null)
						{
							if (!menuItems.containsKey(s))
							{
								File ff = new File(s);
								if (ff.exists())
								{
									controller.monitorFile(s);
									/*
									 * event driven JMenuItem j =
									 * createMenuItem(s, this, "Monitor_Tail-" +
									 * s, 'T', KeyEvent.VK_T);
									 * monitoring.add(j); menuItems.put(s, j);
									 */
								}
							}
						}
						br.close();
						fr.close();

					} catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			} else if (cmd.contains("Tail"))
			{
				// System.out.println("tailing file clicked ");
				String fname = cmd.substring(13);
				controller.removeFileMonitor(fname);
				/*
				 * these lines will get called by event JMenuItem j =
				 * menuItems.get(fname); menuItems.remove(fname);
				 * monitoring.remove(j);
				 */

			}

			else if(newContentPane.selectedService!=null)
			{
				// System.out.println("monitoring graph selected "
				// + newContentPane.selectedService.getSName());
				
				RealtimeGraph r = new RealtimeGraph(
						newContentPane.selectedService, bwReader);
				r.ShowUI();
				RTGraphs.add(r);
				r.setRTGraphs(RTGraphs);
			}
		} else if (cmd.startsWith("Reset"))
		{
			controller.resetRateMeters();
		} else if (cmd.startsWith("Count"))
		{
			// System.out.println("count graph");
			if (newContentPane.selectedService != null)
			{
				CountGraphWindow g = new CountGraphWindow(
						"Quantised Service Performance View for "
								+ newContentPane.selectedService.getType()+"."+newContentPane.selectedService.getSName(),
						newContentPane.selectedService);
				
				g.setVisible(true);
				g.setBounds(50, 50, 800, 600);
			}
		} else if (cmd.startsWith("FilterLayer_"))
		{
			layerFilter = cmd.substring(12);
			setFilter(layerFilter, serviceFilter, true);
			setHighlightFilter(highlightFilter);
			if (layerFilter.equalsIgnoreCase("All")
					&& serviceFilter.equalsIgnoreCase(""))
			{
				sbar.setFiltered(false);
			} else
			{
				sbar.setFiltered(true);
			}
		} else if (cmd.equals("FilterService"))
		{
			serviceFilter = JOptionPane.showInputDialog(mainFrame,
					"Enter Service Prefix...", serviceFilter);
			setFilter(layerFilter, serviceFilter, true);
			setHighlightFilter(highlightFilter);
			if (layerFilter.equalsIgnoreCase("All")
					&& (serviceFilter==null || serviceFilter.equalsIgnoreCase("")))
			{
				//System.out.println("setting filtered to false");
				sbar.setFiltered(false);
			} else
			{
				sbar.setFiltered(true);
			}
			
		} else if(cmd.equals("FilterHighlight"))
		{
			if(highlightFilter)highlightFilter=false;
			else highlightFilter=true;
			setHighlightFilter(highlightFilter);
		}
		else if (cmd.equals("FilterNotModelled"))
		{
			setFilter("ALL", "", false);
			setHighlightFilter(highlightFilter);
			sbar.setFiltered(false);
		} else if (cmd.equals("FilterDates"))
		{

		}
	}

	
	private void saveWindows(ActionEvent event) {
		if(!event.getActionCommand().equals("saveWindows"))return;
		fc.setMultiSelectionEnabled(false);
	    

	    //File[] files = chooser.getSelectedFiles();
		
		int returnVal2 = fc.showSaveDialog(mainFrame);
		if (returnVal2 == JFileChooser.APPROVE_OPTION){
			File f=fc.getSelectedFile();
			try{
				if(f==null | f.equals("")){
					sbar.setMessage("Please specify a file name to save the window settings to");
					return;
				}
				if(f.exists()){
				//we will be overwriting
					if(!f.delete()) throw new IOException("Cannot delete existing file");
				}
				f.createNewFile();
				StringBuffer sb=getWindowSettings();
				FileOutputStream fo=new FileOutputStream(f);
				PrintStream ps=new PrintStream(fo);
				ps.print(sb);
				ps.flush();
				ps.close();
			
			}catch(IOException e){
				sbar.setMessage(e.getMessage());
			}
		}
		
		
	}

	public static Image image = null; //getToolkit().getImage(ClassLoader.getSystemResource("fantasyhexwar/Images/plains100.gif"));
	private HashMap<String, JMenuItem> menuItems = new HashMap<String, JMenuItem>();

	/**
	 * A method used internally to make a menu item
	 * 
	 * @param label
	 *            The label to be shown
	 * @param listener
	 *            The listener that will receive events on this menu item
	 * @param command
	 *            The command that is being requests when this menu item is
	 *            selected
	 * @param mnemonic
	 *            The key to show that activates the menu item
	 * @param accKey
	 *            The physical key
	 * @return The created menu item
	 */
	public JMenuItem createMenuItem(String label, ActionListener listener,
			String command, int mnemonic, int accKey)
	{
		JMenuItem item = new JMenuItem(label);
		item.addActionListener(listener);
		item.setActionCommand(command);

		if (mnemonic != 0)
			item.setMnemonic(mnemonic);
		if (accKey != 0)
			item.setAccelerator(KeyStroke.getKeyStroke(accKey,
					java.awt.Event.CTRL_MASK));
		return item;
	}

	/**
	 * Creates a toggle menu item instead of a plan menu item
	 * 
	 * @param label
	 *            The label to be shown
	 * @param listener
	 *            The listener that will receive events on this menu item
	 * @param command
	 *            The command that is being requests when this menu item is
	 *            selected
	 * @param mnemonic
	 *            The key to show that activates the menu item
	 * @param accKey
	 *            The physical key
	 * @return The created menu item
	 * 
	 */
	public static JCheckBoxMenuItem createToggleMenuItem(String label,
			ItemListener listener, String command, int mnemonic, int accKey)
	{
		JCheckBoxMenuItem item = new JCheckBoxMenuItem(label);
		item.setState(false);
		// item.addActionListener(listener);
		item.addItemListener( listener);

		item.setActionCommand(command);
		if (mnemonic != 0)
			item.setMnemonic(mnemonic);
		if (accKey != 0)
			item.setAccelerator(KeyStroke.getKeyStroke(accKey,
					java.awt.Event.CTRL_MASK));
		return item;
	}

	/**
	 * Allows the application of a filter to restrict the display of Service
	 * objects
	 * 
	 * @param layer
	 *            subsystem type
	 * @param service
	 *            individual service
	 * @param modelled
	 *            not used any more
	 */
	public void setFilter(String layer, String service, boolean modelled)
	{
		// System.out.println("Filter (" + layer + "," + service + "," +
		// modelled
		// + ")");
		newContentPane.setFilter(layer, service, modelled);
		sbar.setFiltered(true);
	}
	
	public void setHighlightFilter(boolean highlight)
	{
		newContentPane.setHighlightFilter(highlight);
	}

	/**
	 * Displays the main window on the screen on the dispatcher thread
	 */
	public void ShowUI()
	{
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				mainFrame.setVisible(true);
				// createAndShowGUI();
				//controller.loadAutoRun();
			}
		});
	}
	
	public void LoadAutoRun()
	{
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				//mainFrame.setVisible(true);
				// createAndShowGUI();
				controller.loadAutoRun();
			}
		});
	}
	
	

	public void SDPaint()
	{

	}

	/**
	 * Sets the list of services to be displayed in the window and creates a
	 * {@link BWReader} which is used in support of file reading
	 * 
	 * 
	 * 
	 * @param so
	 *            The List of services
	 */
	public MainForm(List<Service> so)
	{
		super();
		SO = so;
		bwReader = new BWReader(so);
		bwReader.setVolumetricModel(ModelForm._VM);
		controller = Controller.ControllerFactory(bwReader);
		controller.addFileMonitorListener(this);
		controller.addModelChangedListener(this);
		controller.addMonitorWindowListener(this);
		// controller.startUDPListener(5555);
		// controller.startJMSListener();
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		

	}

	public MainForm(Controller control)
	{
		controller = control;
		SO = control.getServices();
		bwReader = control.getReader();
		controller.addFileMonitorListener(this);
		controller.addModelChangedListener(this);
		// controller.startUDPListener(5555);
		// controller.startJMSListener();
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);

	}

/*	private MainForm()
	{
		createGUI();
	}
*/
	public void setVolumetricModel(VolumetricModel VM)
	{
		bwReader.setVolumetricModel(VM);
	}

	/**
	 * Registers the {@link ServiceDependencyView} with this object
	 * 
	 * @param SD
	 */
	public void addSDView(ServiceDependencyView SD)
	{
		sd = SD;
		newContentPane.addSDView(SD);
	}

	protected JFrame mainFrame = null;
	private List<Service> SO;
	private ServiceDependencyView sd = null;
	private ServiceTable newContentPane = null;
	private JMenu realtime, monitoring;

	JMenuItem jFreq;
	JMenuItem jSDView;
	private JCheckBoxMenuItem jVModel, jSTimes;

	@Override
	public void itemStateChanged(ItemEvent e)
	{
		//this next line takes care of the user not being able to select the service when there is only one
		if(newContentPane.selectedService==null && SO!=null &&SO.size()>0)newContentPane.selectedService=SO.get(0);

		// TODO Auto-generated method stub
		JCheckBoxMenuItem item = ((JCheckBoxMenuItem) e.getItem());
		int state = e.getStateChange();
		// System.out.println("menu item selected " + item.getActionCommand()
		// + " " + state);
		if (item.getActionCommand().equalsIgnoreCase("Toggle_Vol"))
		{
			if (state == java.awt.event.ItemEvent.DESELECTED)
				togVol = false;
			else
				togVol = true;
			MF.toggleWindow();
		}
		if (item.getActionCommand().equalsIgnoreCase("Toggle_Freq"))
		{
			if (state == java.awt.event.ItemEvent.DESELECTED)
				togFreq = false;
			else
				togFreq = true;
			newContentPane.toggleFreqWindow();
		}
		if (item.getActionCommand().equalsIgnoreCase("Toggle_SDView"))
		{
			if (state == java.awt.event.ItemEvent.DESELECTED)
				togSDView = false;
			else
				togSDView = true;
			sd.toggleWindow();
		}
		if (item.getActionCommand().equalsIgnoreCase("Toggle_STimes"))
		{
			if (state == java.awt.event.ItemEvent.DESELECTED)
				togSTimes = false;
			else
				togSTimes = true;

			ttg.redrawGraph(SO);
			atg.toggleWindow();
			ttg.toggleWindow();
		}
		if (item.getActionCommand().equalsIgnoreCase("CaptureTxns"))
		{
			System.out.println("capture txn called");
			boolean b=serviceAnalysisModel.Transaction.EventCollection.getInstance().setCollection(capture.getState());
			if(b)EventCollection.getInstance().addListener(this);
			else if(capture.getState())
			{ 
				JOptionPane.showMessageDialog( mainFrame, "Capture not started! You currently have more than 100,000 events","Capture not Started",JOptionPane.WARNING_MESSAGE);           	
				capture.setSelected(false);
			}
		}
	}

	public static boolean togFreq = false, togSDView = false, togVol = false,
			togSTimes = false;

	@Override
	public void windowActivated(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e)
	{
		// TODO Auto-generated method stub
		if (((Frame) e.getWindow()).getTitle().contains("Service Table"))
		{
			for(Frame f:JFrame.getFrames()) f.dispose();
			controller.shutdown();
		}
		if (e.getWindow().toString().contains("CumulativeFrequencyGraph"))
		{
			togFreq = false;
			//jFreq.setSelected(false);
			// System.out.println("closing CFG");
		}
		if (((Frame) e.getWindow()).getTitle().contains("Service Dependency"))
		{
			togSDView = false;
			jSDView.setSelected(false);
		}
		if (((Frame) e.getWindow()).getTitle().contains("Volumetric Model"))
		{
			togVol = false;
			jVModel.setSelected(false);
		}
		if (((Frame) e.getWindow()).getTitle().contains("Average Time"))
		{
			togSTimes = false;
			jSTimes.setSelected(false);
			ttg.toggleWindow();
		}
		if (((Frame) e.getWindow()).getTitle().contains("Total Time"))
		{
			togSTimes = false;
			jSTimes.setSelected(false);
			atg.toggleWindow();
		}
		// System.out.println(e.paramString() + " "
		// + ((JFrame) e.getWindow()).getTitle() + " "
		// + e.getWindow().getName());
	}

	@Override
	public void windowDeactivated(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	private Controller controller = null;

	public static WindowListener wl;
	public static ModelForm MF;
	public static AverageTimeGraph atg;
	public static TotalTimeGraph ttg;
	
	private boolean highlightFilter=false;

	//public static final String DBConnectionStr = "jdbc:hsqldb:file:.Database.SA/SAdb";
	//public static final String DBUser = "SA";
	//public static final String DBPasswd = "";

	@Override
	public void fileMonitorChangeListener(MonitorChangeEvent event)
	{
		// TODO Auto-generated method stub
		if (event.getEventType() == MonitorChangeEvent._ADD)
		{// adding monitor
						// System.out.println("monitor added");
			JMenuItem j = createMenuItem(event.getFile(), this, "Monitor_Tail-"
					+ event.getFile(), 'T', KeyEvent.VK_T);
			monitoring.add(j);
			menuItems.put(event.getFile(), j);
			sbar.setMessage("File Monitor Added for file "+event.getFile());

		} else
		{// removing monitor
			// System.out.println("monitor removed");
			JMenuItem j = menuItems.get(event.getFile());
			menuItems.remove(event.getFile());
			if(j!=null) monitoring.remove(j);
			sbar.setMessage("File Monitor Removed for file "+event.getFile());

		}

	}

	@Override
	public void modelChanged()
	{
		// TODO Auto-generated method stub
		ServiceTable.model.fireTableDataChanged();
		newContentPane.resetSelectedRow();
		MF.clearModel();
		newContentPane.repaint();
		ttg.redrawGraph(SO);
		
	}

	@Override
	public void dataChanged()
	{
		// TODO Auto-generated method stub
		ServiceTable.model.fireTableDataChanged();
	}

	@Override
	public void StartMonitor(StartMonitorWindowEvent event)
	{
		// TODO Auto-generated method stub
		//System.out.println(event.getService()+", "+event.getX()+", "+event.getY()+", "+event.getW()+", "+event.getH()+", "+event.getTime());
		RealtimeGraph r = new RealtimeGraph(event.getService(), bwReader,
				event.getX(), event.getY(), event.getW(), event.getH(),
				event.getTime(),event.isTitle());
		r.ShowUI();
		RTGraphs.add(r);
		r.setRTGraphs(RTGraphs);

	}

	private ArrayList<RealtimeGraph> RTGraphs = new ArrayList<RealtimeGraph>();

	public void dumpWindows(File f)
	{
		StringBuffer sb=getWindowSettings();
	}
	
	public void dumpWindows()
	{
		System.out.print(getWindowSettings());
	}
	
	public StringBuffer getWindowSettings()
	{
		StringBuffer sb=new StringBuffer(2000);
		Rectangle stbounds = newContentPane.getBounds();
		Point p=newContentPane.getLocationOnScreen();
		sb.append("COMMAND: Window ServiceTable position "+p.x+" "+p.y+" "+stbounds.width+" "+stbounds.height);
		sb.append('\n');
		
		//System.out.println("COMMAND: Window ServiceTable position "+stbounds.x+" "+stbounds.y+" "+stbounds.width+" "+stbounds.height);
		if(highlightFilter)
			sb.append("COMMAND: Window ServiceTable highlight on\n");
		else sb.append("COMMAND: Window ServiceTable highlight off\n");
		
		List<String> commands=newContentPane.getColumnsCommands();
		for(String s:commands) sb.append(s+'\n');

		for (RealtimeGraph rtg : RTGraphs)
		{
			Rectangle r = rtg.getBounds();
			int time = rtg.getTimeRange();
			Service s = rtg.getService();
			sb.append("COMMAND: Window Monitor \"" + s.getType()+"\" \"" + s.getServiceName()
					+ "\" \"" + s.getOperationName() + "\" " + r.x + " " + r.y + " "
					+ r.width + " " + r.height + " " + time + " "+ rtg.isTitleShown());
			sb.append('\n');
			
			
		}
		sb.append(CountGraphWindow.getCountGraphWindowsSettings());
		sb.append('\n');
		return sb;
	}

	@Override
	public void collectorChange(boolean state, final int collectionsize) {
		//System.out.println("collectorChange fired");
		if(state==false && capture.getState()==true)
		{
			
			if (EventQueue.isDispatchThread()) {
	        	
				capture.setSelected(false);
	        } else {

	        	javax.swing.SwingUtilities.invokeLater(new Runnable() {
	                @Override
	                public void run() {	                	
	                	capture.setSelected(false);
	                	JOptionPane.showMessageDialog( mainFrame, "Capture Stopped! "+collectionsize +" events","Collection Stopped",JOptionPane.WARNING_MESSAGE);
	                	
	                }
	            });

	        }
			
			//EventCollection.getInstance().removeListener(this);
		}
		
		
	}
	
	private void openSDView(ActionEvent event)
	{
		if(event.getActionCommand().equals("SDView"))
		{
			System.out.println("sdview selecected");
			Service selectedService=newContentPane.selectedService;
			if(selectedService!=null)
			{
				ServiceDependencyView sdview = new ServiceDependencyView(SO);
				sdview.createGUI();
				sdview.newContentPane.setService(selectedService);
				sdview.ShowUI();
			}
		}
	}
	
	private void openCFG(ActionEvent event)
	{
		if(event.getActionCommand().equals("Frequency"))
		{
			Service selectedService=newContentPane.selectedService;
			if(selectedService!=null)
			{
				System.out.println("frequency");
				Map<Service, Map<Double, Double>> clist = new HashMap<Service, Map<Double, Double>>();
				Map<Service, Map<Double, Double>> dlist = new HashMap<Service, Map<Double, Double>>();

				Iterator<Service> is = selectedService
						.getConsumers();
				while (is.hasNext())
				{
					Service cs = is.next();
					clist.put(cs, cs.getSamples());
				}
				is = selectedService.getDependents();
				while (is.hasNext())
				{
					Service cs = is.next();
					dlist.put(cs, cs.getSamples());
				}
				if (clist.isEmpty())
					clist = null;
				if (dlist.isEmpty())
					dlist = null;

				CumulativeFrequencyGraph c=new CumulativeFrequencyGraph(selectedService
						.getSName(), selectedService.getSamples(),
						selectedService
								.calculateCumulativeDistribution(),clist,dlist);
				c.pack();
				c.setVisible(true);
				newContentPane.cfg=c;
			}
		}
	}

	@Override
	public void adjustWindow(final String name, final int x, final int y, final int w, final int h) {
		// TODO Auto-generated method stub
		if(name.equals("ServiceTable"))
		{
			if (EventQueue.isDispatchThread()) {
				System.out.println(x+","+y+","+w+","+h);
				mainFrame.setExtendedState(JFrame.NORMAL);
				mainFrame.setBounds(x, y, w, h);
				System.out.println(mainFrame.getBounds());
			} else {
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
	                @Override
	                public void run() {
	                    adjustWindow(name,x,y,w,h);
	                }
	            });
			}
		}
	}

	@Override
	public void setHighlight(final String name, final boolean on) {
		if(name.equals("ServiceTable"))
		{
			highlightFilter=on;
			if (EventQueue.isDispatchThread()) {
				setHighlightFilter(highlightFilter);
			} else {
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
	                @Override
	                public void run() {
	                    setHighlight(name,on);
	                }
	            });
			}
		}
		
	}

	@Override
	public void addColumn(final String name, final String column, final boolean on) {
		// TODO Auto-generated method stub
		if(name.equals("ServiceTable"))
		{
			if (EventQueue.isDispatchThread()) {
				if(on)newContentPane.addColumn(column);
				else newContentPane.removeColumn(column);
			}else {
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
	                @Override
	                public void run() {
	                    addColumn(name,column,on);
	                }
	            });
			}
		}
	}

	@Override
	public void setColumn(final String name, final String column, final int width) {
		if(name.equals("ServiceTable"))
		{
			if (EventQueue.isDispatchThread()) {
				newContentPane.setWidth(column, width);
			}else {
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
	                @Override
	                public void run() {
	                    setColumn(name,column,width);
	                }
	            });
			}
		}
		
	}

	@Override
	public void openWindow(String name, Service s, int x, int y, int w, int h,
			int period) {
		if(name.equalsIgnoreCase("Count")){
			System.out.println("received count graph open event");
			System.out.println(s);
			CountGraphWindow g = new CountGraphWindow(
						"Quantised Service Performance for "
								+ s.getType()+"."+s.getSName(),s, period);
			g.setBounds(x, y, w, h);
			g.setVisible(true);
			g.setBounds(x, y, w, h);
			
		}
		
	}
	
	
}
