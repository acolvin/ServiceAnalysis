package serviceAnalysisUI;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import serviceAnalysisModel.ServiceMapper;

public class MapperTable extends JPanel implements TableModelListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1553340483522569993L;
	private MapperTableModel model;
	private JTable table;
	private JScrollPane scrollPane;
	private MapperWindow mapperWindow=null;

	public MapperTable()
	{
		model=new MapperTableModel();
		table=new JTable(model);
		model.addTableModelListener(this);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		table.setDragEnabled(false);
		table.getTableHeader().setReorderingAllowed(false);
	}
	
	public MapperTable(MapperWindow  mw)
	{
		this();
		mapperWindow=mw;
	}
	
	public void setMapperWindow(MapperWindow mw)
	{
		mapperWindow=mw;
	}
	
	
	
	public void refresh()
	{
		model.fireTableDataChanged();
	}
	
	public void deleteMatch(int row)
	{
		
		String types=(String) table.getValueAt(row, 0);
		int type=0;
		if(types.equalsIgnoreCase("service")) type=ServiceMapper.SERVICE;
		else type=ServiceMapper.OPERATION;
		String match=(String) table.getValueAt(row, 1);
		ServiceMapper.removeMatch(match, type);
		refresh();
	}
	
	public int[] getSelectedRows()
	{
		return table.getSelectedRows();
	}
	
	public void createGUI()
	{
		this.setLayout(new BorderLayout());
		scrollPane = new JScrollPane(table);
		add(scrollPane);
		//add(table);
		setVisible(true);
		//scrollPane.setVisible(true);
	}

	@Override
	public void tableChanged(TableModelEvent e) {
		// TODO Auto-generated method stub
		int row = e.getFirstRow();
		int column = e.getColumn();
		MapperTableModel model = (MapperTableModel) e.getSource();
		// String columnName = model.getColumnName(column);
		String data = (String) model.getValueAt(row, column);
		//System.out.println("mapper data changed to "+data);
	}
	
	public void move(boolean up,int row)
	{
		int newrow=row-1;
		if(!up)newrow+=2;
		int i=ServiceMapper.moveMatch(row, newrow);
		System.out.println("row "+i);
		table.setRowSelectionInterval(i, i);
	}


}
