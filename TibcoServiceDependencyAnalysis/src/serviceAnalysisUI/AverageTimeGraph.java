package serviceAnalysisUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import serviceAnalysisModel.Service;
import serviceAnalysisModel.TFileReader;
import serviceAnalysisModel.SAControl.Controller;

public class AverageTimeGraph extends JFrame implements ActionListener
{

	public class LowerSliderListener implements ChangeListener
	{

		@Override
		public void stateChanged(ChangeEvent e)
		{
			// TODO Auto-generated method stub
			JSlider js = (JSlider) e.getSource();
			int l = js.getValue();
			// System.out.println(l);
			if (upperLimit.getValue() < l)
				upperLimit.setValue(l);
			upperLimit.setMinimum(l);

			List<Service> sl;
			if (selectName.equals(""))
				sl = TFileReader.getSortedTimeList(lowerLimit.getValue(),
						upperLimit.getValue());
			else
				sl = TFileReader.getSortedTimeList(lowerLimit.getValue(),
						upperLimit.getValue(), selectName);

			CategoryDataset cd = createDataset(sl);
			selections=getLayers();
			selectList.setModel(new DefaultComboBoxModel(selections));
			chart = createChart(cd);
			chartPanel.setChart(chart);
		}

	}

	public class UpperSliderListener implements ChangeListener
	{

		@Override
		public void stateChanged(ChangeEvent e)
		{
			// TODO Auto-generated method stub
			JSlider js = (JSlider) e.getSource();
			int l = js.getValue();
			// System.out.println(l);
			if (lowerLimit.getValue() > l)
				lowerLimit.setValue(l);
			lowerLimit.setMaximum(l);

			List<Service> sl;
			if (selectName.equals(""))
				sl = TFileReader.getSortedTimeList(lowerLimit.getValue(),
						upperLimit.getValue());
			else
				sl = TFileReader.getSortedTimeList(lowerLimit.getValue(),
						upperLimit.getValue(), selectName);

			CategoryDataset cd = createDataset(sl);
			selections=getLayers();
			
			selectList.setModel(new DefaultComboBoxModel(selections));
			chart = createChart(cd);
			chartPanel.setChart(chart);
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 7805538327628229828L;
	JComboBox selectList;

	public AverageTimeGraph(String title, List<Service> services)
	{
		super(title);
		selections=getLayers();
		if(MainForm.image!=null)
			setIconImage(MainForm.image);
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		CategoryDataset dataset = createDataset(services);
		chart = createChart(dataset);

		// JFreeChart chart = ChartFactory.createBarChart(
		// "Average Service Times", "Service", "Time (ms)", dataset,
		// PlotOrientation.HORIZONTAL, true, false, false);
		chartPanel = new ChartPanel(chart);

		chartPanel.setPreferredSize(new java.awt.Dimension(600, dataset
				.getColumnCount() * 15));

		chartPanel.setDomainZoomable(true);
		// chartPanel.setMaximumDrawHeight(dataset.getColumnCount()*1000);
		chartPanel.setRangeZoomable(true);

		selectList = new JComboBox(selections);
		selectList.setSelectedIndex(0);
		selectList.addActionListener(this);

		this.add(selectList, BorderLayout.PAGE_START);

		lowerLimit = new JSlider(JSlider.VERTICAL, 0, 5000, 300);
		upperLimit = new JSlider(JSlider.VERTICAL, 300, 20000, 20000);

		lowerLimit.addChangeListener(new LowerSliderListener());
		upperLimit.addChangeListener(new UpperSliderListener());

		JScrollPane sp = new JScrollPane(chartPanel);
		// setContentPane(sp);
		this.add(sp, BorderLayout.CENTER);
		this.add(lowerLimit, BorderLayout.LINE_START);
		this.add(upperLimit, BorderLayout.LINE_END);

		// setContentPane(chartPanel);
		MainForm.atg = this;
		this.addWindowListener(MainForm.wl);
	}

	public void actionPerformed(ActionEvent e)
	{
		JComboBox cb = (JComboBox) e.getSource();
		selectName = (String) cb.getSelectedItem();
		// do something here
		// System.out.println("combo: "+ selectName);
		List<Service> sl;
		
		if (selectName.equals(""))
			sl = TFileReader.getSortedTimeList(lowerLimit.getValue(),
					upperLimit.getValue());
		else
			sl = TFileReader.getSortedTimeList(lowerLimit.getValue(),
					upperLimit.getValue(), selectName);

		CategoryDataset cd = createDataset(sl);
		selections=getLayers();
		selectList.setModel(new DefaultComboBoxModel(selections));
		chart = createChart(cd);
		chartPanel.setChart(chart);

	}

	private CategoryDataset createDataset(List<Service> services)
	{
		// double[][] data=new double[1][services.size()];
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		for (int i = 0; i < services.size(); i++)
			dataset.addValue((double) services.get(i).getAvgtime(),
					"Average Times (ms)", services.get(i).getSName());
		// for(int i=0;i<services.size();i++)
		// data[0][i]=(double)services.get(i).getAvgtime();

		return dataset;
		// return DatasetUtilities.createCategoryDataset( "Service Time",
		// "SERVICE.operation ", data);
	}

	private JFreeChart createChart(final CategoryDataset dataset)
	{

		final JFreeChart chart = ChartFactory.createBarChart("Service Time",
				"Service Operation", "Time (ms)", dataset,
				PlotOrientation.HORIZONTAL, false, true, false);

		chart.setBackgroundPaint(new Color(249, 231, 236));

		CategoryPlot plot = chart.getCategoryPlot();
		plot.getRenderer().setSeriesPaint(0, new Color(128, 0, 0));
		// plot.getRenderer().setSeriesPaint(1, new Color(0, 0, 255));

		return chart;
	}

	public void toggleWindow()
	{
		System.out.println("making AvTimeGraph window " + MainForm.togSTimes);
		if (MainForm.togSTimes){
			selections=getLayers();
			selectList.setModel(new DefaultComboBoxModel(selections));
			List<Service> sl;
			if (selectName==null || selectName.equals("")){
				sl = TFileReader.getSortedTimeList(lowerLimit.getValue(),
						upperLimit.getValue());
				selectList.setSelectedIndex(0);
			}
			else {
				sl = TFileReader.getSortedTimeList(lowerLimit.getValue(),
						upperLimit.getValue(), selectName);
				for(String s:selections)if(s.equals(selectName)){
					selectList.setSelectedItem(s);
					break;
				}
			}
			CategoryDataset cd = createDataset(sl);
			
			chart = createChart(cd);
			chartPanel.setChart(chart);
			this.setVisible(true);
		}
		else
			this.setVisible(false);
	}

	private String[] getLayers(){
		HashSet<String> layers=new HashSet<String>();
		List<Service> services=new ArrayList<Service>(Controller.getController().getServices());
		layers.add("");
		for(Service s:services) layers.add(s.getType());
		return layers.toArray(new String[0]);
		
	}
	
	ChartPanel chartPanel;
	private JFreeChart chart;
	private String selectName = "BW";
	private JSlider lowerLimit, upperLimit;
	private  String[] selections ={""};
	

	// @Override
	// public void stateChanged(ChangeEvent e) {
	// TODO Auto-generated method stub
	// ((JSlider)e.getSource());
	// }
}
