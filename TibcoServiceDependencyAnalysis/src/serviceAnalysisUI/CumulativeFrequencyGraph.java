package serviceAnalysisUI;

import java.awt.BorderLayout;
import java.awt.Color;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import java.util.Map;
import java.util.Random;


import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jdesktop.swingx.JXMultiSplitPane;
import org.jdesktop.swingx.MultiSplitLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;

import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;


import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import org.jfree.util.ShapeUtilities;

import serviceAnalysisModel.Service;

public class CumulativeFrequencyGraph extends JFrame implements ActionListener,
		ChangeListener
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7805538327628229829L;

	private boolean CFGon = true;
	private JCheckBox CFGcheck;
	private JSlider bucketSize;
	
	private XYSeries series1;
	private XYSeries series2;
	private Map<Double, Double> samples;
	private XYSeriesCollection dataset;

	private Map<Service, Map<Double, Double>> consumersamples = null,
			dependantsamples = null;

	private JCheckBox markerCheck;

	private boolean markeron=false;

	public CumulativeFrequencyGraph(String title, Map<Double, Double> samples,
			Map<Double, Double> cumulative)
	{
		super(title);
		
		this.samples = samples;
		if(MainForm.image!=null)
			setIconImage(MainForm.image);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		// System.out.println("building chart");

		//JLabel text = new JLabel("Cumulative Frequency Graph");
		CFGcheck = new JCheckBox("Cumulative Frequency Graph", CFGon);
		CFGcheck.addActionListener(this);
		CFGcheck.setActionCommand("off");
		
		//JLabel markerText=new JLabel("Show Markers");
		markerCheck=new JCheckBox("Show Markers",markeron);
		markerCheck.addActionListener(this);
		markerCheck.setActionCommand("marker");

		bucketSize = new JSlider(0, 100, 1);
		bucketSize.setMajorTickSpacing(20);
		bucketSize.setMinorTickSpacing(5);
		bucketSize.setPaintTicks(true);
		bucketSize.setPaintTrack(true);
		bucketSize.setName("Sample Width");
		bucketSize.setPaintLabels(true);

		bucketSize.addChangeListener(this);

		JLabel sliderLabel = new JLabel("Sample Width");

		String layoutDef = "(COLUMN (ROW tickbox slider) (ROW (LEAF name=main weight=1.0) (COLUMN weight=0.0 (LEAF name=consumer weight=0.5) (LEAF name=dependant weight=0.5) (LEAF name=plot weight=0.0)) ))";

		MultiSplitLayout.Node modelRoot = MultiSplitLayout
				.parseModel(layoutDef);
		// JXMultiSplitPane msp=new JXMultiSplitPane();
		msp.getMultiSplitLayout().setModel(modelRoot);

		JPanel jp = new JPanel();
		JPanel jp2 = new JPanel();
		JPanel jp3=new JPanel(new BorderLayout());
		jp3.add(CFGcheck, BorderLayout.NORTH);
		jp3.add(markerCheck,BorderLayout.CENTER);
		jp.add(jp3, BorderLayout.LINE_START);
		jp2.add(sliderLabel, BorderLayout.NORTH);
		jp2.add(bucketSize, BorderLayout.CENTER);
		jp.add(jp2, BorderLayout.LINE_END);

		msp.add(jp3, "tickbox");
		msp.add(jp2, "slider");

		series1 = new XYSeries(title);
		Iterator<Double> i = samples.keySet().iterator();
		Double x, y;
		while (i.hasNext())
		{
			x = i.next();
			y = samples.get(x);
			series1.add(x, y);
		}

		series2 = new XYSeries("Cumulative " + title);
		for (Double d : cumulative.keySet())
		{
			series2.add(d, cumulative.get(d));
		}

		dataset = new XYSeriesCollection();
		
		dataset.addSeries(series1);
		// dataset.addSeries(series2);
		XYSeriesCollection dataset2 = new XYSeriesCollection();
		dataset2.addSeries(series2);

		chart = ChartFactory.createXYLineChart(title,
		// chart title
				"Response",
				// x axis label
				"Frequency",// y axis label
				dataset,
				// data
				PlotOrientation.VERTICAL, true,
				// include legend
				true,
				// tooltips
				false
		// urls
				);

		// CategoryDataset dataset = createDataset(services);
		// JFreeChart chart = createChart(dataset);

		
		
		NumberAxis axis2 = new NumberAxis("Cumulative Frequency");

		XYPlot p = chart.getXYPlot();
		setMarkers(false);
		// p.setRangeMinorGridlinesVisible(true);
		XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer();
		p.setRangeAxis(1, axis2);
		p.setBackgroundPaint(Color.white);
		p.setDomainGridlinePaint(Color.BLACK);
		p.setRangeGridlinePaint(Color.BLACK);

		renderer1.setSeriesPaint(0, Color.blue);
		renderer1.setSeriesShapesVisible(0, false);
		p.setRenderer(1, renderer1);
		p.setDataset(1, dataset2);

		p.mapDatasetToRangeAxis(1, 1);

		// JFreeChart chart = ChartFactory.createBarChart(
		// "Average Service Times", "Service", "Time (ms)", dataset,
		// PlotOrientation.HORIZONTAL, true, false, false);
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(600, 900));
		jp.setPreferredSize(new java.awt.Dimension(600, 50));
		// this.add(jp, BorderLayout.PAGE_START);
		// this.add(chartPanel);
		msp.add(chartPanel, "main");
		this.add(msp);
	}

	JXMultiSplitPane msp = new JXMultiSplitPane();


	public void setMarkers(boolean on)
	{
		XYPlot p = chart.getXYPlot();
		XYLineAndShapeRenderer sssss=(XYLineAndShapeRenderer)(p.getRenderer());
		sssss.setSeriesShape(0, ShapeUtilities.createUpTriangle(2));
		sssss.setSeriesShapesVisible(0, on);
		
		
	}
	
	public CumulativeFrequencyGraph(String title, Map<Double, Double> samples,
			Map<Double, Double> cumulative,
			Map<Service, Map<Double, Double>> consumersamples,
			Map<Service, Map<Double, Double>> dependantsamples)
	{
		this(title, samples, cumulative);
		this.consumersamples = consumersamples;
		this.dependantsamples = dependantsamples;

		JPanel jpLists = new JPanel();
		jpLists.setPreferredSize(new Dimension(200, 400));

		ArrayList<Service> cal = new ArrayList<Service>();
		ArrayList<Service> dal = new ArrayList<Service>();

		if (consumersamples != null)
		{
			cal.addAll(consumersamples.keySet());
			consumerList = new JList(cal.toArray());
			JScrollPane listScroller = new JScrollPane(consumerList);
			jpLists.add(listScroller, BorderLayout.NORTH);
			msp.add(listScroller, "consumer");
			for (Service s : cal)
			{
				plotColour.put(s, new Color(randomGenerator.nextInt(255), 230,
						randomGenerator.nextInt(255)));
				XYSeries series = new XYSeries(s.getSName());
				serviceSeries.put(s, series);
				int count = dataset.getSeriesCount();
				XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) chart
						.getXYPlot().getRenderer();
				dataset.addSeries(series);
				renderer.setSeriesPaint(count, plotColour.get(s));
				renderer.setSeriesVisible(count, false);
				renderer.setSeriesShapesVisible(count, false);
				replotSeries(series, consumersamples.get(s));

			}

		}
		if (dependantsamples != null)
		{
			dal.addAll(dependantsamples.keySet());
			dependantList = new JList( dal.toArray());
			JScrollPane listScroller = new JScrollPane(dependantList);
			jpLists.add(listScroller, BorderLayout.SOUTH);
			msp.add(listScroller, "dependant");
			for (Service s : dal)
			{
				plotColour.put(s, new Color(randomGenerator.nextInt(255),
						randomGenerator.nextInt(255), 190));
				XYSeries series = new XYSeries(s.getSName());
				serviceSeries.put(s, series);
				int count = dataset.getSeriesCount();
				XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) chart
						.getXYPlot().getRenderer();
				dataset.addSeries(series);
				renderer.setSeriesPaint(count, plotColour.get(s));
				renderer.setSeriesShapesVisible(count, false);
				renderer.setSeriesVisible(count, false);
				replotSeries(series, dependantsamples.get(s));

			}
		}
		plotButton = new JButton("Plot");
		plotButton.addActionListener(new plotListener());
		jpLists.add(plotButton, BorderLayout.SOUTH);
		if (!(consumersamples == null & dependantsamples == null))
		{
			// this.add(jpLists,BorderLayout.LINE_END);
			msp.add(plotButton, "plot");
		}

	}

	private Random randomGenerator = new Random();

	private class plotListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0)
		{

			// System.out.println(arg0.getActionCommand());
			if (arg0.getActionCommand().equalsIgnoreCase("Plot"))
			{
				XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) chart
						.getXYPlot().getRenderer();
				for (int i = 1; i < dataset.getSeriesCount(); i++)
				{// turn off all supplementary plots

					renderer.setSeriesVisible(i, false);
				}

				if (consumersamples != null)
				{

					Object[] o = consumerList.getSelectedValues();
					for (Object s : o)
					{
						Service ss = (Service) s;
						// System.out.println("Consumer service "+ss);
						renderer.setSeriesVisible(
								dataset.indexOf(serviceSeries.get(ss)), true);
					}
				}
				if (dependantsamples != null)
				{
					Object[] o = dependantList.getSelectedValues();
					for (Object s : o)
					{
						Service ss = (Service) s;
						// System.out.println("Dependant service "+ss);
						renderer.setSeriesVisible(
								dataset.indexOf(serviceSeries.get(ss)), true);
					}
				}
			}
		}

	}

	private HashMap<Service, Color> plotColour = new HashMap<Service, Color>();
	private HashMap<Service, XYSeries> serviceSeries = new HashMap<Service, XYSeries>();
	//private HashMap<Service, XYLineAndShapeRenderer> serviceRenderers = new HashMap<Service, XYLineAndShapeRenderer>();
	private JList<Service> consumerList = null, dependantList = null;
	private JButton plotButton;

	private JFreeChart chart;

	/*private CategoryDataset createDataset(List<Service> services)
	{
		// double[][] data=new double[1][services.size()];
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		for (int i = 0; i < services.size(); i++)
			dataset.addValue((double) services.get(i).getAvgtime(),
					"Average Times (ms)", services.get(i).getSName());
		// for(int i=0;i<services.size();i++)
		// data[0][i]=(double)services.get(i).getAvgtime();

		return dataset;
		// return DatasetUtilities.createCategoryDataset( "Service Time",
		// "SERVICE.operation ", data);
	}*/

/*	private JFreeChart createChart(final CategoryDataset dataset)
	{

		final JFreeChart chart = ChartFactory.createBarChart("Service Time",
				"Service Operation", "Time (ms)", dataset,
				PlotOrientation.HORIZONTAL, false, true, false);

		chart.setBackgroundPaint(new Color(249, 231, 236));
		// chart.setBackgroundPaint(new Color(0,0,0));
		CategoryPlot plot = chart.getCategoryPlot();
		plot.getRenderer().setSeriesPaint(0, new Color(128, 0, 0));
		// plot.getRenderer().setSeriesPaint(1, new Color(0, 0, 255));

		return chart;
	}
*/
	@Override
	public void actionPerformed(ActionEvent e)
	{
		
		// System.out.println("action "+e.paramString());
		if (e.getActionCommand() == "off")
		{
			CFGon = false;
			CFGcheck.setActionCommand("on");
			chart.getXYPlot().getRenderer(1).setSeriesVisible(0, false);
			chart.getXYPlot().getRangeAxis(1).setVisible(false);

		}
		if (e.getActionCommand() == "on")
		{
			CFGon = true;
			CFGcheck.setActionCommand("off");
			chart.getXYPlot().getRenderer(1).setSeriesVisible(0, true);
			chart.getXYPlot().getRangeAxis(1).setVisible(true);
			// chart.getXYPlot().getRangeAxis(1).
		}
		if(e.getActionCommand().equals("marker"))
		{
			setMarkers(markerCheck.getModel().isSelected());
		}

	}

	@Override
	public void stateChanged(ChangeEvent e)
	{
		JSlider source = (JSlider) e.getSource();
		int bs = 1;
		if (!source.getValueIsAdjusting())
		{
			series1.clear();
			for (Service s : serviceSeries.keySet())
			{
				serviceSeries.get(s).clear();
			}
			bs = (int) source.getValue();
			if (bs != 0)
			{

				// Map<Double,Double> ss=reQuantise(samples,bs);
				replotSeries(series1, reQuantise(samples, bs));
				for (Service s : serviceSeries.keySet())
				{
					if (s != null)
					{
						Map<Double, Double> samples = null;
						if (consumersamples != null
								&& consumersamples.containsKey(s))
							samples = consumersamples.get(s);
						else if (dependantsamples != null)
							samples = dependantsamples.get(s);
						if (samples != null)
							replotSeries(serviceSeries.get(s),
									reQuantise(samples, bs));

					}
				}

			}
		}

	}

	private void replotSeries(XYSeries series, Map<Double, Double> ss)
	{
		series.clear();
		Double xx, yy;
		
		Iterator<Double> ii = ss.keySet().iterator();
		while (ii.hasNext())
		{
			xx = ii.next();
			yy = ss.get(xx);
			// System.out.println("("+xx+","+yy+")");
			series.add(xx, yy);
		}
		series.fireSeriesChanged();
	}

	private Map<Double, Double> reQuantise(Map<Double, Double> base,
			int quantisationSize)
	{
		HashMap<Double, Double> ss = new HashMap<Double, Double>();
		Iterator<Double> i = base.keySet().iterator();
		Double x, y;

		while (i.hasNext())
		{
			x = i.next();
			// Math.floor((double)newtime/5.0d)*5.0d
			double samplex = Math.floor(x / (double) quantisationSize)
					* (double) quantisationSize + ((double) quantisationSize)
					/ 2.0;
			y = base.get(x);
			if (ss.containsKey(samplex))
			{
				ss.put(samplex, new Double(ss.get(samplex).doubleValue() + y));
			} else
				ss.put(samplex, y);

		}

		return ss;
	}

	public JFreeChart getChart()
	{
		return chart;
	}
}
