package serviceAnalysisUI.selectionWindow;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import networkAgent.listener.ListenerTCP;

public class ConnectPanel extends JPanel {
	
	/**
	 * 
	 */
	JTextField usernameField;
	JPasswordField passwordField;
	JTextField operationFilterField;

	JTextField hostField; 
	JFormattedTextField portField;
	long port=5556;
	String host="localhost";
	String command=null;
	SelectPanelCallback callback;
	String username="";
	

	public ConnectPanel(SelectPanelCallback callback){
		
		this.callback=callback;
		pane();
	}
	
	
	private void pane(){
		JPanel backPanel=this;
		username=System.getProperty("user.name");
		
		backPanel.setLayout(new BoxLayout(backPanel,BoxLayout.PAGE_AXIS));
        
        JLabel hostLabel=new JLabel("Host: ");
        JLabel portLabel=new JLabel("Port: ");
        
        hostField=new JTextField(host);
        hostField.setPreferredSize(new Dimension(150,hostField.getPreferredSize().height));
        hostField.setToolTipText("Enter a host name or IP address");
        
        NumberFormat nf = NumberFormat.getIntegerInstance();
        portField=new JFormattedTextField(nf);
        portField.setValue(port);
        portField.setToolTipText("Enter a port number (greater than zero)");
        
        JPanel serverPanel=new JPanel();
        serverPanel.setLayout(new BoxLayout(serverPanel,BoxLayout.LINE_AXIS));
        
        serverPanel.add(hostLabel);
        serverPanel.add(hostField);
        serverPanel.add(portLabel);
        serverPanel.add(portField);
        
        backPanel.add(serverPanel);
        

        
        JLabel userLabel=    new JLabel("User Name: ");
        JLabel passwordLabel=new JLabel("Password: ");
        
        passwordLabel.setPreferredSize(userLabel.getPreferredSize());
        
        
        JPanel userPanel=new JPanel();
        userPanel.setLayout(new BoxLayout(userPanel,BoxLayout.LINE_AXIS));
        usernameField=new JTextField(username);
        usernameField.setToolTipText("Enter your username");

        JPanel passwordPanel=new JPanel();
        passwordPanel.setLayout(new BoxLayout(passwordPanel,BoxLayout.LINE_AXIS));
        passwordField=new JPasswordField();
        passwordField.setToolTipText("Enter your password");
        
        
        userPanel.add(userLabel);
        userPanel.add(usernameField);
        passwordPanel.add(passwordLabel);
        passwordPanel.add(passwordField);

        
        JPanel buttonPanel=new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
        
        JButton okButton=new JButton("Connect");
        okButton.setActionCommand("CONNECT");
        okButton.addActionListener(new ButtonListener());
        
        JButton cancelButton=new JButton("Cancel");
        cancelButton.setActionCommand("cancel");
        cancelButton.addActionListener(new ButtonListener());
        
        
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(okButton);
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(cancelButton);
        buttonPanel.add(Box.createHorizontalGlue());
        
        
        backPanel.add(serverPanel);
        backPanel.add(userPanel);
        backPanel.add(passwordPanel);
        backPanel.add(buttonPanel);
		
	}
    
    private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equals("cancel")){
				System.out.println("cancel selected");
				if(callback!=null)callback.cancelled();
			}else if(e.getActionCommand().equals("CONNECT")){
				System.out.println("select selected");

				boolean isok=true;

				if(hostField.getText()==null || hostField.getText().length()==0){
					isok=false;
					hostField.setBackground(Color.YELLOW);
				}else {
					hostField.setBackground((new JTextField()).getBackground());
					
				}
				try{
					if((Long)portField.getValue()<1){
						isok=false;
						portField.setBackground(Color.YELLOW);
					}else portField.setBackground((new JTextField()).getBackground());
					
				}catch(ClassCastException ee){
					isok=false;
					portField.setBackground(Color.YELLOW);
				}
				System.out.println("All fields parsed successfully is "+isok);
				
				
				if(isok){
					host=hostField.getText();
					port=(Long) portField.getValue();
					String user=usernameField.getText();
					if(user!=null && !user.equals("") ){
						System.setProperty("user.name", user);
						System.out.println("username set to "+user);
					}
					
					ListenerTCP.password=new String(passwordField.getPassword());
					
					
					
					command="COMMAND: TCPConnect "+host+" "+port;
					//System.out.println(command);
					if(callback!=null)callback.success(command);
					
				}
			}
			
		}
    	
    }
	
	
}
