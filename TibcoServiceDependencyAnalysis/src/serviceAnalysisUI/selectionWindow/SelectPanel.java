package serviceAnalysisUI.selectionWindow;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
//import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import serviceAnalysisModel.SAControl.Controller;
import networkAgent.listener.ListenerTCP;

public class SelectPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 378523551301050333L;
	DateTimePicker dateTimePickerFrom;
	DateTimePicker dateTimePickerTo;
	JTextField layerFilterField;
	JTextField serviceFilterField;
	JTextField operationFilterField;
	JTextField userFilterField;
	JTextField rateField;
	JPanel datePanel=new JPanel();
	JTextField hostField; 
	JFormattedTextField portField;
	long port=5556;
	String host="localhost";
	String command=null;
	SelectPanelCallback callback;
	
	JComboBox<ListenerTCP> combo;
	ArrayList<ListenerTCP> collectors=new ArrayList<ListenerTCP>();
	ArrayList<String> connections=new ArrayList<String>();
	
	private static Date previousStartDate=null;
	private static Date previousEndDate=null;


	

	public SelectPanel(SelectPanelCallback callback, String host, long port){
		this.host=host;
		this.port=port;
		this.callback=callback;
		pane();
	}
	
	public SelectPanel(SelectPanelCallback callback){
		this.callback=callback;
		pane();
	}
	private void pane(){
		
		List<ListenerTCP> a = Controller.getController().getReader().getTCPClientList();
		for(ListenerTCP e:a){
			if(e.isCollector()){
				collectors.add(e);
				
			}
			
		}
		ListenerTCP[] dd=new ListenerTCP[1];
		combo=new JComboBox<ListenerTCP>(collectors.toArray(dd));

		
		Calendar tocal=new GregorianCalendar();
		Date date = new Date();
		
		Calendar fromcal=new GregorianCalendar();
		fromcal.setTime(date);
		fromcal.roll(Calendar.HOUR_OF_DAY, false);
		JPanel backPanel=this;
		if(previousStartDate==null){
			tocal.setTime(date);
			fromcal.setTime(date);
			fromcal.roll(Calendar.HOUR_OF_DAY, false);
		}else{
			tocal.setTime(previousEndDate);
			fromcal.setTime(previousStartDate);
		}
		
		backPanel.setLayout(new BoxLayout(backPanel,BoxLayout.PAGE_AXIS));
        
 //       JLabel hostLabel=new JLabel("Host: ");
 //       JLabel portLabel=new JLabel("Port: ");
        
 //       hostField=new JTextField(host);
 //       hostField.setToolTipText("Enter a host name or IP address");
        
 //       NumberFormat nf = NumberFormat.getIntegerInstance();
 //       portField=new JFormattedTextField(nf);
 //       portField.setValue(port);
  //      portField.setToolTipText("Enter a port number (greater than zero)");
        
        JPanel serverPanel=new JPanel();
        serverPanel.setLayout(new BoxLayout(serverPanel,BoxLayout.LINE_AXIS));

        serverPanel.add(combo);
//        serverPanel.add(hostLabel);
//        serverPanel.add(hostField);
//        serverPanel.add(portLabel);
//        serverPanel.add(portField);
        
        backPanel.add(serverPanel);
        
        datePanel.setLayout(new BoxLayout(datePanel,BoxLayout.LINE_AXIS));
        backPanel.add(datePanel);
        
        JLabel fromDateLabel=new JLabel("From: ");
        dateTimePickerFrom = new DateTimePicker();
        dateTimePickerFrom.setFormats( DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.SHORT ) );
        dateTimePickerFrom.setTimeFormat( DateFormat.getTimeInstance( DateFormat.SHORT ) );

        dateTimePickerFrom.setDate(fromcal.getTime());

        
        datePanel.add(fromDateLabel);
        datePanel.add(dateTimePickerFrom);
        datePanel.add(Box.createHorizontalGlue());
        
        JLabel toDateLabel=new JLabel("To: ");
        dateTimePickerTo = new DateTimePicker();
        dateTimePickerTo.setFormats( DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.SHORT ) );
        dateTimePickerTo.setTimeFormat( DateFormat.getTimeInstance( DateFormat.SHORT ) );
        
       // System.out.println("setting end date to "+tocal.getTime());
        dateTimePickerTo.setDate(tocal.getTime());
        datePanel.add(toDateLabel);
        datePanel.add(dateTimePickerTo);
        
        JLabel layerLabel=new JLabel("Layer Filter: ");
        JLabel serviceLabel=new JLabel("Service Filter: ");
        JLabel operationLabel=new JLabel("Operation Filter: ");
        JLabel userLabel=new JLabel("User Filter: ");
        JLabel rateLabel=new JLabel("Rate: ");
        
        layerLabel.setPreferredSize(operationLabel.getPreferredSize());
        serviceLabel.setPreferredSize(operationLabel.getPreferredSize());
        rateLabel.setPreferredSize(operationLabel.getPreferredSize());
        userLabel.setPreferredSize(operationLabel.getPreferredSize());
        
        JPanel layerPanel=new JPanel();
        layerPanel.setLayout(new BoxLayout(layerPanel,BoxLayout.LINE_AXIS));
        layerFilterField=new JTextField();
        layerFilterField.setActionCommand("regexp");
        TextFieldListener layerListener=new TextFieldListener();
        layerFilterField.addActionListener(layerListener);
        layerFilterField.setToolTipText("Enter a regular expression to restrict the retrieve records");

        
        JPanel servicePanel=new JPanel();
        servicePanel.setLayout(new BoxLayout(servicePanel,BoxLayout.LINE_AXIS));
        serviceFilterField=new JTextField();
        serviceFilterField.setActionCommand("regexp");
        TextFieldListener serviceListener=new TextFieldListener();
        serviceFilterField.addActionListener(serviceListener);
        serviceFilterField.setToolTipText("Enter a regular expression to restrict the retrieve records");
        
        JPanel operationPanel=new JPanel();
        operationPanel.setLayout(new BoxLayout(operationPanel,BoxLayout.LINE_AXIS));
        operationFilterField=new JTextField();
        operationFilterField.setActionCommand("regexp");
        TextFieldListener operationListener=new TextFieldListener();
        operationFilterField.addActionListener(operationListener);
        operationFilterField.setToolTipText("Enter a regular expression to restrict the retrieve records");
  
        JPanel userPanel=new JPanel();
        userPanel.setLayout(new BoxLayout(userPanel,BoxLayout.LINE_AXIS));
        userFilterField=new JTextField();
        userFilterField.setActionCommand("regexp");
        TextFieldListener userListener=new TextFieldListener();
        userFilterField.addActionListener(userListener);
        userFilterField.setToolTipText("Enter a regular expression to restrict the retrieve records");

        
        JPanel ratePanel=new JPanel();
        ratePanel.setLayout(new BoxLayout(ratePanel,BoxLayout.LINE_AXIS));
        rateField=new JTextField();
        rateField.setActionCommand("rate");
        TextFieldListener rateListener=new TextFieldListener();
        rateField.addActionListener(rateListener);
        rateField.setToolTipText("<html>Enter a double number greater than zero or leave blank. <p>The value 1 replays at the original speed. <p>60 is a minute each second. <p>0.5 is half rate. </html>");
        
        layerPanel.add(layerLabel);
        layerPanel.add(layerFilterField);
        servicePanel.add(serviceLabel);
        servicePanel.add(serviceFilterField);
        operationPanel.add(operationLabel);
        operationPanel.add(operationFilterField);
        userPanel.add(userLabel);
        userPanel.add(userFilterField);
        ratePanel.add(rateLabel);
        ratePanel.add(rateField);
        
        JPanel buttonPanel=new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
        
        JButton okButton=new JButton("Select");
        
        okButton.setActionCommand("select");
        okButton.addActionListener(new ButtonListener());
        defaultButton=okButton;
        
        JButton cancelButton=new JButton("Cancel");
        cancelButton.setActionCommand("cancel");
        cancelButton.addActionListener(new ButtonListener());
        
        
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(okButton);
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(cancelButton);
        buttonPanel.add(Box.createHorizontalGlue());
        
        
        backPanel.add(layerPanel);
        backPanel.add(servicePanel);
        backPanel.add(operationPanel);
        backPanel.add(userPanel);
        backPanel.add(ratePanel);
        backPanel.add(buttonPanel);
        
        
		
	}
    
    private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equals("cancel")){
				System.out.println("cancel selected");
				if(callback!=null)callback.cancelled();
			}else if(e.getActionCommand().equals("select")){
				System.out.println("select selected");
				ActionEvent event=new ActionEvent(layerFilterField,1,"regexp");
				TextFieldListener l = new TextFieldListener();
				l.actionPerformed(event);
				boolean isok=true;
				isok=isok&l.isOK();
				event=new ActionEvent(serviceFilterField,1,"regexp");
				l.actionPerformed(event);
				isok=isok&l.isOK();
				event=new ActionEvent(operationFilterField,1,"regexp");
				l.actionPerformed(event);
				isok=isok&l.isOK();
				event=new ActionEvent(userFilterField,1,"regexp");
				l.actionPerformed(event);
				isok=isok&l.isOK();
				event=new ActionEvent(rateField,1,"rate");
				l.actionPerformed(event);
				isok=isok&l.isOK();
				Date fromDate=dateTimePickerFrom.getDate();
				Date toDate=dateTimePickerTo.getDate();
				
				GregorianCalendar fromCal=new GregorianCalendar();
				GregorianCalendar toCal=new GregorianCalendar();
				fromCal.setTime(fromDate);
				toCal.setTime(toDate);
				if(toCal.before(fromCal)){
					isok=false;
					datePanel.setBackground(Color.yellow);
				}else {
					datePanel.setBackground((new JPanel()).getBackground());
				}
				
				if(combo.getSelectedItem()!=null){
					combo.setBackground((new JTextField()).getBackground());
					ListenerTCP collector= (ListenerTCP) combo.getSelectedItem();
					String colname=collector.toString();
					int i=colname.indexOf(':');
					if(i!=-1){
						host=colname.substring(0, i);
						port=Integer.parseInt(colname.substring(i+1, colname.length()));
					}else{
						combo.setBackground(Color.YELLOW);
						isok=false;
					}
					
				}else{
					combo.setBackground(Color.YELLOW);
					isok=false;
				}
				
//				if(hostField.getText()==null || hostField.getText().length()==0){
//					isok=false;
//					hostField.setBackground(Color.YELLOW);
//				}else {
//					hostField.setBackground((new JTextField()).getBackground());
//					
//				}
//				try{
//					if((Long)portField.getValue()<1){
//						isok=false;
//						portField.setBackground(Color.YELLOW);
//					}else portField.setBackground((new JTextField()).getBackground());
//					
//				}catch(ClassCastException ee){
//					isok=false;
//					portField.setBackground(Color.YELLOW);
//				}
				System.out.println("All fields parsed successfully is "+isok);
				
				
				if(isok){
					//host=hostField.getText();
					//port=(long) portField.getValue();
					String layer=layerFilterField.getText();
					String service=serviceFilterField.getText();
					String operation=operationFilterField.getText();
					String user=userFilterField.getText();
					
					if(layer==null || layer.length()==0) layer=".*";
					if(service==null || service.length()==0) service=".*";
					if(operation==null || operation.length()==0) operation=".*";
					if(user==null || user.length()==0) user=".*";
					
					layer=" layer \""+layer+"\"";
					service=" service \""+service+"\"";
					operation=" operation \""+operation+"\"";
					user=" user \""+user+"\"";
					
					previousStartDate=fromDate;
					previousEndDate=toDate;
					SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy HH:mm");
					String from="from \""+sdf.format(fromDate)+"\"";
					String to=" to \""+sdf.format(toDate)+"\"";
					
					
					String rate="";
					if(rateField.getText()!=null && rateField.getText().length()>0){
						rate=" rate "+rateField.getText();
					}
					
					command="TCPCommand select "+host+" "+port+" timings "+from+to+layer+service+operation+user+rate;
					//System.out.println(command);
					if(callback!=null)callback.success(command);
					
				}
			}
			
		}
    	
    }

    private JButton defaultButton;
    
	public JButton getDefaultButton() {
		// TODO Auto-generated method stub
		return defaultButton;
	}
	
	
}
