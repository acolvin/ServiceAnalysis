package serviceAnalysisUI.selectionWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListModel;

import networkAgent.listener.ListenerTCP;
import serviceAnalysisModel.SAControl.Controller;

public class SelectionStopPane extends JPanel {
	ArrayList<ListenerTCP> collectors=new ArrayList<ListenerTCP>();
	ArrayList<String> connections=new ArrayList<String>();
	SelectPanelCallback callback;
	
	JComboBox combo;
	
	public SelectionStopPane(SelectPanelCallback callback){
		this.callback=callback;
		List<ListenerTCP> a = Controller.getController().getReader().getTCPClientList();
		for(ListenerTCP e:a){
			if(e.isCollector()){
				collectors.add(e);
				
			}
			
		}
		setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		combo=new JComboBox(collectors.toArray());
		this.add(combo);
		
		JPanel buttons=new JPanel();
		buttons.setLayout(new BoxLayout(buttons,BoxLayout.LINE_AXIS));
		
		JButton stop=new JButton("Stop Query");
		stop.setActionCommand("stop");
		ActionListener al=new ButtonListener();
		stop.addActionListener(al);
		JButton disconnect=new JButton("Close Connection");
		disconnect.setActionCommand("close");
		disconnect.addActionListener(al);
		JButton cancel=new JButton("cancel");
		cancel.setActionCommand("cancel");
		cancel.addActionListener(al);
		buttons.add(stop);
		buttons.add(disconnect);
		buttons.add(cancel);
		add(buttons);
	}
	
	private class ButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(arg0.getActionCommand().equals("stop")){
				if(combo.getSelectedItem()!=null){
					ListenerTCP collector=(ListenerTCP) combo.getSelectedItem();
					System.out.println("selected item to stop \""+collector+"\"");
					String command = "COMMAND: TCPCommand select ";
					String colname=collector.toString();
					int i=colname.indexOf(':');
					if(i!=-1){
						command+=colname.substring(0, i);
						command+=" ";
						command+=colname.substring(i+1, colname.length());
						command+= " stop";
						System.out.println(command);
						if(callback!=null)callback.success(command);
					}
				}
				
			}else if(arg0.getActionCommand().equals("cancel")){
				if(callback!=null)callback.cancelled();
			}else if(arg0.getActionCommand().equals("close")){
				if(combo.getSelectedItem()!=null){
					ListenerTCP collector=(ListenerTCP) combo.getSelectedItem();
					System.out.println("selected item to close \""+collector+"\"");
					String command = "COMMAND: TCPConnect STOP ";
					String colname=collector.toString();
					int i=colname.indexOf(':');
					if(i!=-1){
						command+=colname.substring(0, i);
						command+=" ";
						command+=colname.substring(i+1, colname.length());
						//command+= " STOP";
						System.out.println(command);
						if(callback!=null)callback.success(command);
					}
				}
			}
			
		}
		
	}
	
}

