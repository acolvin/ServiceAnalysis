package serviceAnalysisUI.selectionWindow;

import java.awt.MouseInfo;
import java.awt.Point;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JFrame;

import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisUI.MainForm;

public class TimingSelectionDialog implements SelectPanelCallback {
	JFrame frame = new JFrame();
	SelectPanel selectPanel;
	boolean cancelled=false;
	
	static protected long port;
	static protected String host;
	

	public TimingSelectionDialog(){
		
		

		
		Point location = MouseInfo.getPointerInfo().getLocation();
		frame.setLocation(location.x, location.y);
		if(MainForm.image!=null) frame.setIconImage(MainForm.image);
	     frame.setTitle("Select Timings from TCPCollector");
	        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	        selectPanel = new SelectPanel(this);
	        frame.getContentPane().add(selectPanel);  
	        frame.getRootPane().setDefaultButton(selectPanel.getDefaultButton());
	        frame.pack();
	        javax.swing.SwingUtilities.invokeLater(new Runnable()
			{
				public void run()
				{
					frame.setVisible(true);
					// createAndShowGUI();
				}
			});
	        
	}
	
	public String getCommand(){
		if(!cancelled)return selectPanel.command;
		else return null;
	}

	@Override
	public void cancelled() {
		cancelled=true;
		frame.setVisible(false);
		frame.dispose();
		
	}

	@Override
	public void success(String command) {
		//System.out.println(command);
		port=selectPanel.port;
		host=selectPanel.host;
		
		String text="COMMAND: "+command;
		Controller.getController().getReader().newLogFileLine(text, "input line");
		
		frame.setVisible(false);
		frame.dispose();
		
	}
}
