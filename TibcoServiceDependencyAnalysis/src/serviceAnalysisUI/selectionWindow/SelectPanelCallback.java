package serviceAnalysisUI.selectionWindow;

public interface SelectPanelCallback {
	public void cancelled();
	public void success(String command);
}
