package serviceAnalysisUI.selectionWindow;

import java.awt.MouseInfo;
import java.awt.Point;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JFrame;

import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisUI.MainForm;

public class TCPConnectDialog implements SelectPanelCallback {
	JFrame frame = new JFrame();
	ConnectPanel selectPanel;
	boolean cancelled=false;
	


	public TCPConnectDialog(){

		Point location = MouseInfo.getPointerInfo().getLocation();
		frame.setLocation(location.x, location.y);
		if(MainForm.image!=null) frame.setIconImage(MainForm.image);
	    frame.setTitle("TCPCollector Connection");
	    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    selectPanel = new ConnectPanel(this);
	    frame.getContentPane().add(selectPanel);  
	    frame.pack();
	    javax.swing.SwingUtilities.invokeLater(new Runnable()
			{
				public void run()
				{
					frame.setVisible(true);
					// createAndShowGUI();
				}
			});
	        
	}
	
	//public String getCommand(){
	//	if(!cancelled)return selectPanel.command;
	//	else return null;
	//}

	@Override
	public void cancelled() {
		cancelled=true;
		frame.setVisible(false);
		frame.dispose();
		
	}

	@Override
	public void success(String command) {
		//System.out.println(command);
		//port=selectPanel.port;
		//host=selectPanel.host;
		
		//String text="COMMAND: "+command;
		Controller.getController().getReader().newLogFileLine(command, "input line");
		
		frame.setVisible(false);
		frame.dispose();
		
	}
}
