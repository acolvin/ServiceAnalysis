package serviceAnalysisUI.selectionWindow;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.PatternSyntaxException;

import javax.swing.JTextField;

public class TextFieldListener implements ActionListener {

	private boolean ok=true;
	
	public boolean isOK(){
		return ok;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getActionCommand().equals("regexp")){
			try{
				JTextField field=(JTextField)arg0.getSource();
				field.setBackground(Color.white);
				String text=field.getText();
				java.util.regex.Pattern.compile(text);
				ok=true;
			}catch (ClassCastException e){
				
			}catch (PatternSyntaxException e){
				JTextField field=(JTextField)arg0.getSource();
				field.setBackground(Color.YELLOW);
				ok=false;
			}
		}else 
		if(arg0.getActionCommand().equals("rate")){
			try{
				JTextField field=(JTextField)arg0.getSource();
				field.setBackground(Color.white);
				String text=field.getText();
				if(text!=null && text.length()>0){
					Double d=Double.parseDouble(text);
					if(d<0){
						throw new NumberFormatException("negative number");
					}
				}
				ok=true;
			}catch (ClassCastException e){
				
			}catch (NumberFormatException e){
				JTextField field=(JTextField)arg0.getSource();
				field.setBackground(Color.YELLOW);
				ok=false;
			}
		}

	}

}
