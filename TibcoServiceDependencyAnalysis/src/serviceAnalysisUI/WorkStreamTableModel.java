package serviceAnalysisUI;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.*;

import serviceAnalysisModel.VolumetricType;
import serviceAnalysisModel.Workstream;

public class WorkStreamTableModel extends AbstractTableModel
{

	final int adjColumn = -1;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WorkStreamTableModel(List<Workstream> wks)
	{
		workstreams = wks;

		// Iterator<Service> i=SO.iterator();
		// while(i.hasNext()){
		// i.next().addServiceListener(this);
		// }

	}

	public Workstream getWorkstream(int index)
	{
		return workstreams.get(index);
	}

	@Override
	public int getColumnCount()
	{
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public int getRowCount()
	{
		// TODO Auto-generated method stub
		return workstreams.size();
	}

	public String getColumnName(int col)
	{
		return columnNames[col];
	}

	public Class getColumnClass(int c)
	{
		// System.out.println("c="+c+", "+ getValueAt(0, c));
		if (c == 0)
			return String.class;
		if (c == 1)
			return Integer.class;
		if (c == 2)
			return Double.class;
		return getValueAt(0, c).getClass();
	}

	public void setValueAt(Object value, int row, int col)
	{
		if (col == adjColumn)
		{
			System.out.println("setting value");

			// times.put(VTs.get(row), (Double) value);
			// SO.get(row).setAdjustments((Float)value);
			// this.fireTableDataChanged();
			// VM.simulate();
			// this.fireTableDataChanged();
		}
	}

	public boolean isCellEditable(int row, int col)
	{
		// Note that the data/cell address is constant,
		// no matter where the cell appears onscreen.
		if (col != adjColumn)
		{
			return false;
		} else
		{
			return true;
		}
	}

	public void changeType(VolumetricType vt)
	{
		VT = vt;
		if (vt == null)
			workstreams = new ArrayList<Workstream>();
		else
			workstreams = vt.getWorkstreams();
	}

	@Override
	public Object getValueAt(int arg0, int arg1)
	{
		// TODO Auto-generated method stub
		// System.out.println(arg0+" "+arg1);
		// if(!workstreams.isEmpty())
		// {
		if (arg1 == 0)
			return workstreams.get(arg0).getName();
		else if (arg1 == 2)
			return workstreams.get(arg0).getSimTime();
		else if (arg1 == 1)
			return VT.getWorkstreamProportion().get(workstreams.get(arg0))
					* ModelForm._VM.model.get(VT);
		// else if(arg1==4) return SO.get(arg0).getDependentCount();
		// else if(arg1==5) return SO.get(arg0).getAvgtime();
		// else if(arg1==6) return SO.get(arg0).getExplicitAdjustments();
		// else if(arg1==7) return SO.get(arg0).getAdjustments();
		// else if(arg1==8) return SO.get(arg0).getOperationTime();
		// else if(arg1==9) return SO.get(arg0).getOperationTimeStdDev();
		// else if(arg1==10) return SO.get(arg0).getOperationTimeLogMedian();
		// else if(arg1==11) return SO.get(arg0).getOperationTimeLogMode();
		else
			return null;
		// }
		// return null;
	}

	private List<Workstream> workstreams;
	private VolumetricType VT = null;
	// private List<VolumetricType> VTs=new ArrayList<VolumetricType>();
	private String[] columnNames =
	{ "WorkStream", "Number", "Time" };

	// @Override
	// public void handleTimeChange(Service s) {
	// TODO Auto-generated method stub
	// System.out.println("handle time change called "+s);
	// int i=SO.indexOf(s);
	// this.fireTableCellUpdated(i, adjColumn);
	// this.fireTableCellUpdated(i, adjColumn + 1);
	// this.fireTableCellUpdated(i, adjColumn + 2);
	// }

}