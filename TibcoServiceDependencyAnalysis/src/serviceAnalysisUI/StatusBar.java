package serviceAnalysisUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Arrays;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import serviceAnalysisModel.SAControl.DelayListener;

public class StatusBar extends JPanel implements ChangeListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2991596446142811077L;

	public StatusBar()
	{
		super();
		this.setLayout(new BorderLayout());
		this.add(sbt);
		UpdateDelay.setPreferredSize(new Dimension(90,32));
		UpdateDelay.setMinimumSize(new Dimension(90,32));
		spinnerModel.setValue(spinnerList[3]);
		spinnerModel.addChangeListener(this);
		this.add(UpdateDelay,BorderLayout.LINE_START);
		
	}
	
	private StatusBarText sbt=new StatusBarText();
	
	public void setMessage(String message)
	{
		
		sbt.setMessage(message);
	}
	
	public void setFiltered(boolean isFiltered)
	{
		sbt.setFiltered(isFiltered);
	}
	
	public void setPerferredWidth(int w)
	{
		//super.setPreferredSize(new Dimension(w, 32));
		sbt.setPreferredSize(new Dimension(w, 32));
	}
	
	public void showProgress()
	{
		progress.setValue(0);
		
		this.add(progress,BorderLayout.LINE_END);
	}
	
	public void setStatusBarValue(int p)
	{
		progress.setValue(p);
	}
	
	public void removeProgress()
	{
		this.remove(progress);
	}
	
	private final String[] spinnerList={"Very Fast", "Fast", "Moderate", "Slow", "Very Slow"};
	private SpinnerModel spinnerModel = new SpinnerListModel(Arrays.asList(spinnerList));
	private JSpinner UpdateDelay=new JSpinner(spinnerModel);
	public JProgressBar progress = new JProgressBar(0,100);

	@Override
	public void stateChanged(ChangeEvent arg0) {
		String current=(String) spinnerModel.getValue();
		long delay=-1;
		if(current.equals(spinnerList[0]))
		{
			delay=0;
		} else if(current.equals(spinnerList[1])) {
			delay=100;
		} else if(current.equals(spinnerList[2])) {
			delay=1000;
		} else if(current.equals(spinnerList[3])) {
			delay=5000;
		} else if(current.equals(spinnerList[4])) {
			delay=30000;
		}
		
		if(delay!=-1){
			// set the delay
			for(DelayListener l:delayListeners) l.setDelay(delay);
		}
	}
	public void addDelayListener(DelayListener l){
		delayListeners.add(l);
	}
	
	public void removeDelayListener(DelayListener l) {
		delayListeners.remove(l);
	}
	
	private CopyOnWriteArraySet<DelayListener> delayListeners=new CopyOnWriteArraySet<DelayListener>();

}
