package serviceAnalysisUI;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Rectangle;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYLineAnnotation;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.event.AxisChangeEvent;
import org.jfree.chart.event.AxisChangeListener;
import org.jfree.chart.event.ChartChangeEvent;
import org.jfree.chart.event.ChartChangeEventType;
import org.jfree.chart.event.ChartChangeListener;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimePeriod;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.TextAnchor;
import org.jfree.util.ShapeUtilities;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.countMaps;
import serviceAnalysisModel.SAControl.Controller;

public class CountGraph implements ChartChangeListener, AxisChangeListener
{

	/**
	 * 
	 */

	public CountGraph(Service service)
	{
		this.service=service;
		countMaps a = service.getRollingCount(60); //per hour
		HashMap<Date, Integer> counts = a.getCounts();
		HashMap<Date, Integer> rcounts = a.getrCounts();
		HashMap<Date, Integer> acounts = a.getaCounts();
		
		HashMap<String, HashMap<Date, Double>> answers = service.getSampleAverage(period);
		HashMap<Date, Double > averages=answers.get("mean");
		//HashMap<Date, Double > averages=service.getSampleAverage(period); //minute by minute
		HashMap<Date, Double > stddevs=answers.get("stddev");
		HashMap<Date, Double> maxs=service.getSampleMax(period);
		HashMap<Date, Double> mins=service.getSampleMin(period);
		HashMap<Date, Double > maxs2=new HashMap<Date, Double >();
		
/*
  		for(Entry<Date,Double> s:averages.entrySet())
		{
			System.out.println("Date: "+s.getKey()+", Average: "+s.getValue());
		}
*/
		List<TimeTableXYDataset> datasetlist = createDataset(counts, rcounts, acounts,averages,maxs,mins,stddevs);

		TimeTableXYDataset dataset=datasetlist.get(0);
		TimeTableXYDataset dataset2=datasetlist.get(1);
		TimeTableXYDataset dataset3=datasetlist.get(2);
		
		
		chart = createChart(dataset,dataset2,dataset3);
		chart.addChangeListener(this);
		// JFreeChart chart = ChartFactory.createBarChart(
		// "Average Service Times", "Service",
		// "Percentage of total Execution Time", dataset,
		// PlotOrientation.HORIZONTAL, true, false, false);
		chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(600, 350));
		// this.setPreferredSize(new java.awt.Dimension(600, 350));
		chartPanel.setDomainZoomable(true);
		chartPanel.setMouseWheelEnabled(true);
		
		// chartPanel.setFillZoomRectangle(true);
		chartPanel.setRangeZoomable(true);
		// setContentPane(chartPanel);
		// MainForm.ttg=this;
		// this.addWindowListener(this);
	}

	private Service service;
	private int period=1;
	private ArrayList<CountGraphListener> listeners=new ArrayList<CountGraphListener>();
	
	public void addListener(CountGraphListener cgl)
	{
		listeners.add(cgl);
	}
	
	public void changeAveragePeriod(int period)
	{
		//is this service in the table and is it the same service
		List<Service> services = Controller.getController().getServices();
		if(services.contains(service)){
			service=services.get(services.indexOf(service));
		}else {
			System.out.println("service missing from service table");
			//XYPlot plot = (XYPlot) chart.getPlot();
			//maxRenderer.removeAnnotations();
			//plot.clearDomainAxes();
			return;
		}
		
		XYPlot plot = (XYPlot) chart.getPlot();
		maxRenderer.removeAnnotations();
			
		this.period=period;
		countMaps a = service.getRollingCount(60); //per hour
		HashMap<Date, Integer> counts = a.getCounts();
		HashMap<Date, Integer> rcounts = a.getrCounts();
		HashMap<Date, Integer> acounts = a.getaCounts();
		HashMap<String, HashMap<Date, Double>> answers = service.getSampleAverage(period);
		HashMap<Date, Double > averages=answers.get("mean");
		HashMap<Date, Double > maxs=service.getSampleMax(period);
		HashMap<Date, Double > maxs2=new HashMap<Date, Double >();
		HashMap<Date, Double > mins=service.getSampleMin(period);
		HashMap<Date, Double > stddevs=answers.get("stddev");
		List<TimeTableXYDataset> datasetlist = createDataset(counts, rcounts, acounts,averages,maxs,mins,stddevs);

		TimeTableXYDataset dataset=datasetlist.get(0);
		TimeTableXYDataset dataset2=datasetlist.get(1);
		TimeTableXYDataset dataset3=datasetlist.get(2);
		//XYPlot plot = (XYPlot) chart.getPlot();
		
		//avgseries=0;
		
		//System.out.println(dataset2.getSeriesKey(0));
		setMaxRendererForSeries(dataset2);
		
		if(showActiveUsers & !usercounton & dataset3.getSeriesCount()>0){
			usercounton=true;
			NumberAxis axis3 = new NumberAxis("User Count");
			axis3.setAxisLinePaint(Color.orange);
			axis3.setLabelPaint(Color.orange);
			axis3.setTickLabelPaint(Color.orange);
			activeUserAxis=axis3;
			XYLineAndShapeRenderer renderer5 = new XYLineAndShapeRenderer();
			renderer5.setSeriesPaint(0, Color.orange);
			renderer5.setSeriesShapesVisible(0, false);
			renderer5.setSeriesLinesVisible(0, true);
			activeUserRenderer=renderer5;
			activeUserRenderer.setBaseToolTipGenerator(new UserTimeTipGenerator());
			plot.setRenderer(2, renderer5);
			plot.setRangeAxis(2, axis3);
			plot.setDataset(2,dataset3);
			plot.mapDatasetToRangeAxis(2, 2);
		}else if(usercounton) plot.setDataset(2,dataset3);
		
		plot.setDataset(0,dataset);
		plot.setDataset(1,dataset2);
		
		plot.getRenderer().setSeriesVisible(0, showHour);
		
		drawAnnotations();
	}
	
	private XYLineAndShapeRenderer activeUserRenderer;
	
	private void drawAnnotations(){
		maxRenderer.removeAnnotations();
		if(showMax){
			for(XYTextAnnotation aa:annotations){
				aa.setFont(aa.getFont().deriveFont(12));
				//aa.setPaint(Color.RED);
				//System.out.println("displaying annotation "+aa.getText()+" "+aa.getX()+" "+aa.getY());
				maxRenderer.addAnnotation(aa);
			}
		}
		if(showVariance){
			for( XYLineAnnotation aa:lineAnnotations){
			
				//aa.setFont(aa.getFont().deriveFont(12));
				maxRenderer.addAnnotation(aa);
			}
		}
	}
	
	public ChartPanel getChartPanel()
	{
		return chartPanel;
	}

	private ChartPanel chartPanel;
	private JFreeChart chart;

	public JFreeChart getChart()
	{
		return chart;
	}

	private List<TimeTableXYDataset> createDataset(HashMap<Date, Integer> counts,
			HashMap<Date, Integer> rcounts, HashMap<Date, Integer> acounts,
			HashMap<Date, Double> averages,HashMap<Date, Double> maxs,HashMap<Date, Double> mins, HashMap<Date, Double> stddevs)
	{
		// double[][] data=new double[1][services.size()];
		final TimeTableXYDataset dataset = new TimeTableXYDataset();
		final TimeTableXYDataset dataset2 = new TimeTableXYDataset();
		avgseries=-1;minseries=-1;maxseries=-1;outlierseries=-1;
		List<Date> ordList = new ArrayList<Date>();
		
		annotations.clear();
		lineAnnotations.clear();

		for (Date d : counts.keySet())
		{
			ordList.add(d);
		}

		Collections.sort(ordList);

		//what is the average of the maxes
		Double maxavg=0D;
		Collection<Double> mm = maxs.values();
		for(Double m:mm)
			maxavg=maxavg+m;
		double firstmaxavg=0d;
		if(mm.size()>0)firstmaxavg=maxavg/(double)mm.size();
		maxavg=0d;
		
		mm=maxs.values();
		firstmaxavg*=2.666d*outlierFactor*scaleFactor;  //exclude any items as massive outliers if they are 4 times more than first avg
		int n=0;
		for(Double m:mm){
			if(m<firstmaxavg){
				maxavg=maxavg+m;
				n++;
			}
		}
		
		if(n>0)maxavg=maxavg/(double)n;
		else maxavg=firstmaxavg/2.666d*outlierFactor*scaleFactor;
		//for(Double d:averages.values()){  //make sure the averages are all below the up arrow
		//	if(d>=maxavg) maxavg=d;
		//}
		
		Double maxoutlier=maxavg*outlierFactor*scaleFactor;
		//System.out.println("maxavg = "+maxavg);
		//System.out.println("maxoutlier = "+maxoutlier);
		for (Date e : ordList)
		{
			TimePeriod tp = new Minute(e);
			Date eplusoneminute = new Date(e.getTime() + 60000);
			Date eminusonemilli = new Date(e.getTime() -1);
			Date eplusoneminustwomilli = new Date(e.getTime() +59998);

			Integer i1 = 0, i2 = 0;
			Double av=0d, max=0d, min=0d;
			if (rcounts.containsKey(e))
				i1 = rcounts.get(e);
			if (acounts.containsKey(e))
				i2 = acounts.get(e);
			if(averages.containsKey(e))
				av=averages.get(e);
			else
				av=0d;
			if(maxs.containsKey(e))
				max=maxs.get(e);
			else
				max=0d;
			if(mins.containsKey(e))
				min=mins.get(e);
			else
				min=0d;
			dataset.add(tp, i1 + i2, "per hour");
			dataset.add(tp, i2, "per minute");
			dataset.add(new Minute(eplusoneminute), 0, "per hour");
			//dataset2.add(new Minute(eminusonemilli), 0, "Average");
			if(av!=0d){
				if(av<maxoutlier){
					//int a=dataset2.getSeriesCount();
					dataset2.add(tp, av, ""+ period +" minute averages (ms)");
					double spread=av+stddevs.get(e)/2.0d;
					if(spread> maxoutlier)spread=maxoutlier;
					XYLineAnnotation lineAnno=new XYLineAnnotation(e.getTime(),av,e.getTime(),spread,new BasicStroke(0.2f),Color.RED);
					//if(a!=dataset2.getSeriesCount()) avgseries=dataset2.getSeriesCount()-1;
					//System.out.println("Date:stddev "+e+":"+stddevs.get(e));
					lineAnnotations.add(lineAnno);
				}
				else{
					//int a=dataset2.getSeriesCount();
					dataset2.add(tp, maxoutlier, "Max Outlier");
					//if(a!=dataset2.getSeriesCount()) outlierseries=dataset2.getSeriesCount()-1;
					XYTextAnnotation anno = new XYTextAnnotation(""+av.intValue(),e.getTime(),maxoutlier);
					anno.setToolTipText(""+av.intValue());
					anno.setTextAnchor(TextAnchor.TOP_CENTER);
					annotations.add(anno);
				}
					
			}
			if(max!=0d){
				if(max<maxoutlier){
					//int a=dataset2.getSeriesCount();
					dataset2.add(tp, max, "maximum per minute (ms)");
					//if(a!=dataset2.getSeriesCount()) maxseries=dataset2.getSeriesCount()-1;
				}
				else {
					//int a=dataset2.getSeriesCount();
					dataset2.add(tp, maxoutlier, "Max Outlier");
					//if(a!=dataset2.getSeriesCount()) outlierseries=dataset2.getSeriesCount()-1;
					
					//add an annotation
					//System.out.println("adding annotation "+max+" "+e.getTime()+" "+maxoutlier);
					XYTextAnnotation anno = new XYTextAnnotation(""+max.intValue(),e.getTime(),maxoutlier);
					anno.setTextAnchor(TextAnchor.BOTTOM_CENTER);
					//else anno.setTextAnchor(TextAnchor.TOP_CENTER);
					anno.setToolTipText(""+max.intValue());
					annotations.add(anno);
					up*=-1;
				}
			}
			if(min!=0d && min < maxoutlier){
				//int a=dataset2.getSeriesCount();
				dataset2.add(tp, min, "minimum per minute (ms)");
				//if(a!=dataset2.getSeriesCount()) minseries=dataset2.getSeriesCount()-1;
				
			}
			//dataset2.add(new Minute(eplusoneminute), 0,"Average");
			//dataset2.add(new Minute(eplusoneminute), av,"Average");
			// for(int i=0;i<services.size();i++)
			// data[0][i]=(double)services.get(i).getAvgtime();
		}
		ArrayList<TimeTableXYDataset> l=new ArrayList<TimeTableXYDataset>();
		l.add(dataset);
		l.add(dataset2);
		l.add(createLoginSet());
		return l;
		// return DatasetUtilities.createCategoryDataset( "Service Time",
		// "SERVICE.operation ", data);
	}
	
	private int up=1;
	private NumberAxis activeUserAxis;
	
	private TimeTableXYDataset createLoginSet()
	{
		final TimeTableXYDataset dataset = new TimeTableXYDataset();
		TreeMap<Long,Integer> logins=Controller.getController().getReader().getUserCount();
		if(showActiveUsers)
			for(long l:logins.keySet())
			{
				TimePeriod tp = new Minute(new Date(l*60000));
				dataset.add(tp, logins.get(l), "User Count");
			}
		return dataset;
	}
	
	private NumberAxis millisec;

	private JFreeChart createChart(final TimeTableXYDataset dataset,final TimeTableXYDataset dataset2,final TimeTableXYDataset dataset3)
	{

		// StackedBarRenderer bar=new StackedBarRenderer();

		// final JFreeChart chart = ChartFactory.createStackedBarChart(
		// "Rolling Count per Hour", "Date/Time", "Number of Calls", dataset,
		// PlotOrientation.VERTICAL, false, true, false);

		final JFreeChart chart = ChartFactory.createXYStepChart(
				"Quantised Service Performance View", "Date/Time", "Volume of Calls",
				dataset, PlotOrientation.VERTICAL, true, true, false);
		XYPlot plot = (XYPlot) chart.getPlot();

		DateAxis xAxis = new DateAxis();
		SimpleDateFormat formatter = new SimpleDateFormat();
		xAxis.setDateFormatOverride(formatter);
		plot.setDomainAxis(xAxis);
		xAxis.addChangeListener(this);
		plot.setBackgroundPaint(Color.WHITE);
		chart.setBackgroundPaint(Color.WHITE);
		// chart.setBackgroundPaint(new Color(249,231,236));

		// CategoryPlot plot = chart.getCategoryPlot();

		// plot.setRenderer(bar);
		// StackedBarRenderer bar=(StackedBarRenderer)plot.getRenderer();
		// bar.setDrawBarOutline(false);
		// bar.setShadowVisible(false);
		// bar.setBarPainter(new StandardBarPainter());

		//GradientPaint gp0 = new GradientPaint(0.0f, 0.0f, Color.blue, 0.0f,
		//		0.0f, new Color(0, 0, 64));
		//GradientPaint gp1 = new GradientPaint(0.0f, 0.0f, Color.green, 0.0f,
		//		0.0f, new Color(0, 64, 0));

		// plot.getRenderer().setSeriesPaint(0, gp1);
		// plot.getRenderer().setSeriesPaint(1,gp0);
		plot.getRenderer().setSeriesPaint(0, Color.blue);
		plot.getRenderer().setSeriesPaint(1, Color.green);
		plot.getRenderer().setBaseToolTipGenerator(new RateTimeTipGenerator());
		//plot.getRenderer().setSeriesPaint(2, Color.red);
		NumberAxis axis2 = new NumberAxis("milliseconds");
		millisec=axis2;
		axis2.setLabelPaint(Color.red);
		axis2.setTickLabelPaint(Color.red);
		
		
		//ScatterRenderer renderer4=new ScatterRenderer();
		XYLineAndShapeRenderer renderer4 = new XYLineAndShapeRenderer();
		maxRenderer=renderer4;
		maxRenderer.setBaseToolTipGenerator(new RateTimeTipGenerator());
		setMaxRendererForSeries(dataset2);
		//System.out.println("average series number "+avgseries);
		//System.out.println("minseries number "+minseries);
		//System.out.println("maxseries number "+maxseries);
		//System.out.println("outlierseries number "+outlierseries);
		
		plot.setRenderer(1, renderer4);
		plot.setRangeAxis(1, axis2);
		plot.setDataset(1,dataset2);
		plot.mapDatasetToRangeAxis(1, 1);
		
		//XYLineAndShapeRenderer rendererMax = new XYLineAndShapeRenderer();
		//System.out.println("minseries number "+minseries);
		
		

		
		
		if(showActiveUsers && dataset3.getSeriesCount()>0){
			usercounton=true;
			NumberAxis axis3 = new NumberAxis("User Count");
			axis3.setAxisLinePaint(Color.orange);
			axis3.setLabelPaint(Color.orange);
			axis3.setTickLabelPaint(Color.orange);
			activeUserAxis=axis3;

			XYLineAndShapeRenderer renderer5 = new XYLineAndShapeRenderer();
			renderer5.setSeriesPaint(0, Color.orange);
			renderer5.setSeriesShapesVisible(0, false);
			renderer5.setSeriesLinesVisible(0, true);
			activeUserRenderer=renderer5;
			activeUserRenderer.setBaseToolTipGenerator(new UserTimeTipGenerator());
			plot.setRenderer(2, renderer5);
			plot.setRangeAxis(2, axis3);
			plot.setDataset(2,dataset3);
			plot.mapDatasetToRangeAxis(2, 2);
		}
		// CategoryItemRenderer renderer = plot.getRenderer();

		// CategoryAxis xAxis = (CategoryAxis)plot.getDomainAxis();

		// xAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
		// xAxis.setCategoryLabelPositionOffset(0);
		xAxis.setTickLabelFont(new Font("Dialog", Font.PLAIN, 7));
		// doesn't work for date axis but may start one day with a newer version
		// of jfreechart
		xAxis.setLabelAngle(Math.PI / 4.0d);
		plot.clearAnnotations();
		drawAnnotations();
		return chart;
	}
	
	private XYLineAndShapeRenderer maxRenderer;
	
	
	private void setMaxRendererForSeries(final TimeTableXYDataset dataset){
		String averagename=""+ period +" minute averages (ms)";
		String minname="minimum per minute (ms)";
		String maxname="maximum per minute (ms)";
		String outliername="Max Outlier";
		minseries=-1;maxseries=-1;outlierseries=-1;avgseries=-1;
		for(int i=0;i<dataset.getSeriesCount();i++){
			Comparable c=dataset.getSeriesKey(i);
			if(c.equals(averagename))avgseries=i;
			else if(c.equals(minname))minseries=i;
			else if(c.equals(maxname))maxseries=i;
			else if(c.equals(outliername))outlierseries=i;
			
			
		}
		if(avgseries!=-1){
			maxRenderer.setSeriesPaint(avgseries, Color.red);
			maxRenderer.setSeriesShape(avgseries, ShapeUtilities.createDiagonalCross(0.5f, 0.5f));
			//maxRenderer.setSeriesShape(avgseries, ShapeUtilities.createDiagonalCross(4f, 0.5f));
			maxRenderer.setSeriesShapesVisible(avgseries, true);
			maxRenderer.setSeriesLinesVisible(avgseries, false);
			maxRenderer.setSeriesVisible(avgseries,isAvgShown);
		}
		if(minseries!=-1){
			maxRenderer.setSeriesPaint(minseries, Color.GRAY);
			maxRenderer.setSeriesShape(minseries,new Rectangle(-4,0,8,1));
			maxRenderer.setSeriesShapesVisible(minseries, true);
			maxRenderer.setSeriesLinesVisible(minseries, false);
			maxRenderer.setSeriesVisible(minseries,isAvgShown&showMax);
		}
		if(maxseries!=-1){
			maxRenderer.setSeriesPaint(maxseries, Color.LIGHT_GRAY);
			maxRenderer.setSeriesShape(maxseries,new Rectangle(-4,0,8,1));
			maxRenderer.setSeriesShapesVisible(maxseries, true);
			maxRenderer.setSeriesLinesVisible(maxseries, false);
			maxRenderer.setSeriesVisible(maxseries,isAvgShown&showMax);
		}
		if(outlierseries!=-1){
			maxRenderer.setSeriesPaint(outlierseries, Color.red);
			maxRenderer.setSeriesShape(outlierseries,ShapeUtilities.createUpTriangle(4f));
			maxRenderer.setSeriesShapesVisible(outlierseries, true);
			maxRenderer.setSeriesLinesVisible(outlierseries, false);
			maxRenderer.setSeriesVisible(outlierseries,isAvgShown&showMax);
		}
	}
	
	
	private ArrayList<XYTextAnnotation>annotations=new ArrayList<XYTextAnnotation>();
	private ArrayList<XYLineAnnotation>lineAnnotations=new ArrayList<XYLineAnnotation>();
	
	private boolean showHour=true;
	public void showHourlyRate(boolean show){
		if(show!=showHour){
			showHour=show;
			XYPlot plot = (XYPlot) chart.getPlot();
			plot.getRenderer().setSeriesVisible(0, showHour);
		}
	}

	private boolean showMax=true;
	public void showMaxMin(boolean show){
		if(show!=showMax){
			showMax=show;
			drawAnnotations();
			if(isAvgShown){
				if(minseries!=-1)maxRenderer.setSeriesVisible(minseries,showMax);
				if(maxseries!=-1)maxRenderer.setSeriesVisible(maxseries,showMax);
				if(outlierseries!=-1)maxRenderer.setSeriesVisible(outlierseries,showMax);
			}
			else{
				if(minseries!=-1)maxRenderer.setSeriesVisible(minseries,false);
				if(maxseries!=-1)maxRenderer.setSeriesVisible(maxseries,false);
				if(outlierseries!=-1)maxRenderer.setSeriesVisible(outlierseries,false);
			}
		}
	}
	public boolean isMaxShown(){
		return showMax;
	}
	
	private boolean showVariance=true;
	public void showVariance(boolean show){
		if(show!=showVariance){
			showVariance=show;
			drawAnnotations();
		}
	}
	
	private boolean isAvgShown=true;
	public void showAverages(boolean show){
		if(show!=isAvgShown){
			isAvgShown=show;
			if(avgseries!=-1)maxRenderer.setSeriesVisible(avgseries,isAvgShown);
			if(isAvgShown){
				millisec.setVisible(true);
				if(minseries!=-1)maxRenderer.setSeriesVisible(minseries,showMax);
				if(maxseries!=-1)maxRenderer.setSeriesVisible(maxseries,showMax);
				if(outlierseries!=-1)maxRenderer.setSeriesVisible(outlierseries,showMax);
			}
			else{
				if(minseries!=-1)maxRenderer.setSeriesVisible(minseries,false);
				if(maxseries!=-1)maxRenderer.setSeriesVisible(maxseries,false);
				if(outlierseries!=-1)maxRenderer.setSeriesVisible(outlierseries,false);
				millisec.setVisible(false);
			}
		}
	}
	
	private boolean usercounton=false;
	private boolean showActiveUsers=true;

	public void showActiveUsers(boolean show){
		if(show!=showActiveUsers){
			showActiveUsers=show;
			if(activeUserRenderer!=null)
				activeUserRenderer.setSeriesVisible(0,showActiveUsers);
			if(activeUserAxis!=null) 
				activeUserAxis.setVisible(showActiveUsers);
			if(!show)usercounton=false;
			
		}
	}
	
	
	@Override
	public void chartChanged(ChartChangeEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getType().equals(ChartChangeEventType.GENERAL))
		{
			//System.out.println("general event type");
		}
	}

	public void forceTimeRangeChangedEvents()
	{
		Date s=new Date((long) chart.getXYPlot().getDomainAxis().getLowerBound());
		Date l=new Date((long) chart.getXYPlot().getDomainAxis().getUpperBound());
		for(CountGraphListener cgl:listeners)
		{
			cgl.TimeRangeChanged(service, s, l);
		}
	}
	
	@Override
	public void axisChanged(AxisChangeEvent arg0) {
		// TODO Auto-generated method stub
		//System.out.println("axis: "+arg0);
		if(chart!=null){
		if(arg0.getAxis().equals(chart.getXYPlot().getDomainAxis()))
		{
			Date s=new Date((long) chart.getXYPlot().getDomainAxis().getLowerBound());
			Date l=new Date((long) chart.getXYPlot().getDomainAxis().getUpperBound());
			for(CountGraphListener cgl:listeners)
			{
				cgl.TimeRangeChanged(service, s, l);
			}
			//System.out.println("("+new Date((long) chart.getXYPlot().getDomainAxis().getLowerBound())+","+new Date((long) chart.getXYPlot().getDomainAxis().getUpperBound())+")");
		}
		//else {System.out.println(chart +" "+ chart.getXYPlot().getDomainAxis());}
	}}

	int minseries=-1,maxseries=-1,outlierseries=-1,avgseries=-1;
	static double outlierFactor=1.5d;
	
	double scaleFactor=1.0d;
	
	public void scaleFactor(double factor){
		scaleFactor*=factor;
		System.out.println("scale factor="+scaleFactor);
		
	}
	public static boolean setOutlierFactor(String factor){
		try{
			double d=Double.parseDouble(factor);
			if(d>0){
				outlierFactor=d;
				System.out.println("outlierFactor set to "+d);
			}
			else{
				Controller.getController().sendMessage("outlier Factor must be greater than zero");
				System.out.println("outlier Factor must be grater than zero");
				return false;
			}
			return true;
		}catch (NumberFormatException e){
			Controller.getController().sendMessage(e.getMessage());
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	private class RateTimeTipGenerator implements XYToolTipGenerator 
	{

		@Override
		public String generateToolTip(XYDataset arg0, int arg1, int arg2) {
			String tip="";
			DecimalFormat form = new DecimalFormat("0.00");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm");
            Number x = arg0.getX(arg1, arg2);
            
			tip=sdf.format(x);
			Double f0 = null;
			Double f1 = null;
			Double f2 = null;
			Double f3 = null;
			try{
				f0=arg0.getYValue(0, arg2);
				if(f0.isNaN())f0=null;
				f1 = arg0.getYValue(1, arg2);
				if(f1.isNaN())f1=null;
				f2 = arg0.getYValue(2, arg2);
				if(f2.isNaN())f2=null;
				f3 = arg0.getYValue(3, arg2);
				if(f3.isNaN())f3=null;
			}catch(Exception e){
				
			}
			if(f0!=null)tip+=", "+form.format(f0);
			if(f1!=null){
				
				tip+=", "+form.format(f1);
			}
			if(f2!=null){
				
				tip+=", "+form.format(f2);
			}
			if(f3!=null){
				
				tip+=", "+form.format(f3);
			}
			return tip;
		}
		
	}
	
	private class UserTimeTipGenerator implements XYToolTipGenerator 
	{

		@Override
		public String generateToolTip(XYDataset arg0, int arg1, int arg2) {
			String tip="";
			DecimalFormat form = new DecimalFormat("0");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm");
            Number x = arg0.getX(arg1, arg2);
            
			tip=sdf.format(x);
			Double f0 = null;
			Double f1 = null;
			Double f2 = null;
			try{
				f0=arg0.getYValue(0, arg2);
				if(f0.isNaN())f0=null;
				f1 = arg0.getYValue(1, arg2);
				if(f1.isNaN())f1=null;
				f2 = arg0.getYValue(2, arg2);
				if(f2.isNaN())f2=null;
			}catch(Exception e){
				
			}
			if(f0!=null)tip+=", "+form.format(f0);
			if(f1!=null){
				
				tip+=", "+form.format(f1);
			}
			if(f2!=null){
				
				tip+=", "+form.format(f2);
			}
			return tip;
		}
		
	}
}
