package serviceAnalysisUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

import serviceAnalysisModel.SAControl.Controller;

public class TimeFilterDialog implements WindowListener, ActionListener
{

	public TimeFilterDialog()
	{
		Date fromDate = Controller.getController().getFilterFrom();
		System.out.println("from Date " + fromDate);
		Date toDate = Controller.getController().getFilterTo();
		System.out.println("to Date " + toDate);
		if (fromDate.getTime() != 0)
		{
			fromIndicator.setSelected(true);
			// fromDate=new Date();
		} else
		{
			fromIndicator.setSelected(false);
			fromDate = new Date();
		}
		if (toDate.getTime() != Long.MAX_VALUE)
		{
			toIndicator.setSelected(true);
			// toDate=new Date(fromDate.getTime()+3600000);
		} else
		{
			toIndicator.setSelected(false);
			toDate = new Date(fromDate.getTime() + 3600000);
		}
		SpinnerDateModel fromModel = new SpinnerDateModel(fromDate, null, null,
				Calendar.HOUR);
		SpinnerDateModel toModel = new SpinnerDateModel(toDate, null, null,
				Calendar.HOUR);

		fromDateSpinner = new JSpinner(fromModel);
		toDateSpinner = new JSpinner(toModel);

		fromDateSpinner.setEditor(new JSpinner.DateEditor(fromDateSpinner,
				"dd/MM/yyyy HH:mm:ss.SSS"));
		toDateSpinner.setEditor(new JSpinner.DateEditor(toDateSpinner,
				"dd/MM/yyyy HH:mm:ss.SSS"));
		cancel.setActionCommand("cancel");
		ok.setActionCommand("ok");
		cancel.addActionListener(this);
		ok.addActionListener(this);

	}

	JFrame dialog = new JFrame("Filter Event Dialog");
	JCheckBox fromIndicator = new JCheckBox("Set From Filter");
	JCheckBox toIndicator = new JCheckBox("Set To Filter");
	JSpinner fromDateSpinner = null;
	JSpinner toDateSpinner = null;
	JButton cancel = new JButton("cancel");
	JButton ok = new JButton("save");

	public void showDialog(JFrame parent)
	{
		dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		dialog.addWindowListener(this);
		dialog.setLocationRelativeTo(parent);
		if(MainForm.image!=null)
			dialog.setIconImage(MainForm.image);
		Box panel = new Box(BoxLayout.Y_AXIS);

		Box fromPanel = new Box(BoxLayout.X_AXIS);
		Box toPanel = new Box(BoxLayout.X_AXIS);
		fromPanel.add(fromIndicator);
		fromPanel.add(Box.createHorizontalGlue());
		panel.add(fromPanel);
		panel.add(fromDateSpinner);
		toPanel.add(toIndicator);
		toPanel.add(Box.createHorizontalGlue());
		panel.add(toPanel);
		panel.add(toDateSpinner);
		Box bPane = new Box(BoxLayout.X_AXIS);
		bPane.add(Box.createHorizontalGlue());
		bPane.add(cancel);
		bPane.add(ok);
		panel.add(Box.createVerticalStrut(5));
		panel.add(bPane);
		dialog.setContentPane(panel);

		dialog.pack();
		dialog.setVisible(true);

	}

	@Override
	public void windowActivated(WindowEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeactivated(WindowEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		if (arg0.getActionCommand().equals("cancel"))
		{
			dialog.dispose();
		}
		if (arg0.getActionCommand().equals("ok"))
		{
			Date fromDate = ((SpinnerDateModel) (fromDateSpinner.getModel()))
					.getDate();
			Date toDate = ((SpinnerDateModel) (toDateSpinner.getModel()))
					.getDate();
			System.out.println("from Date " + fromDate);
			System.out.println("to Date " + toDate);
			boolean fromSel = fromIndicator.isSelected();
			boolean toSel = toIndicator.isSelected();
			System.out.println("from:" + fromSel);
			System.out.println("to:" + toSel);

			Controller.getController().resetFilters();
			if (fromSel)
				Controller.getController().setFilterFrom(fromDate);
			if (toSel)
				Controller.getController().setFilterTo(toDate);

			dialog.dispose();
		}

	}

}
