package serviceAnalysisUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.BoundedRangeModel;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import javax.swing.table.TableColumn;
import org.jfree.chart.JFreeChart;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisModel.SAControl.MonitorWindowListener;
import serviceAnalysisModel.SAControl.StartMonitorWindowEvent;

public class ServiceTable extends JPanel implements TableModelListener,
		ActionListener
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3641213821560531245L;
	private int columnnumber=-1;
	protected JMenu submenu;
	private String popupColumnName="";
	private int popupRow=-1;
	private int popupColumn=-1;
	
	
	public ServiceTable(List<Service> so)
	{
		// TODO Auto-generated constructor stub
		SO = so;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		// Create a table with a sorter.
		model = new ServiceTableModel(SO);
		sorter = new TableRowSorter<ServiceTableModel>(model);
		table = new JTable(model)
		{

			/**
			 * 
			 */
			private static final long serialVersionUID = 3649465318910754792L;

			// Implement table cell tool tips.
			public String getToolTipText(MouseEvent e)
			{
				String tip = "";
				java.awt.Point p = e.getPoint();
				int rowIndex = rowAtPoint(p);
				int colIndex = columnAtPoint(p);
				// System.out.println("rowindex="+rowIndex);
				if (rowIndex == -1)
					return null;
				// int realColumnIndex = convertColumnIndexToModel(colIndex);
				int realRowIndex = convertRowIndexToModel(rowIndex);
				// System.out.println("realrowindex="+realRowIndex);
				tip = model.getService(realRowIndex).getAddData();
				// System.out.println("tip="+tip);
				return tip;
			}
		};
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		JTableHeader head=table.getTableHeader();
		model.addServiceTableModelListener(this);
		model.addTableModelListener(this);
		
        JMenuItem RTItem = new JMenuItem("RT Graph");
        RTItem.setActionCommand("RTGraph");
        JMenuItem freqItem = new JMenuItem("Frequency Graph");
        freqItem.setActionCommand("Frequency");
        JMenuItem countGraph = new JMenuItem("Quantised View");
        countGraph.setActionCommand("Count");
        JMenuItem highlightService = new JMenuItem("Highlight Row");
        highlightService.setActionCommand("Highlight");
 
        
        JMenuItem RemoveColumn = new JMenuItem("Remove Column");
        RemoveColumn.setActionCommand("RemoveColumn");
        JMenuItem ResetColumns = new JMenuItem("Show all Columns");
        ResetColumns.setActionCommand("ResetColumns");
        JMenuItem RealTimeUpdate = new JMenuItem("RealTime Updates");
        RealTimeUpdate.setActionCommand("Realtime");
        JMenuItem DelayedUpdate = new JMenuItem("Delayed Updates");
        DelayedUpdate.setActionCommand("Delay");
        JMenuItem ReloadTable = new JMenuItem("Reload Rows");
        ReloadTable.setActionCommand("Reload");

        
 
        
        JMenuItem DepItem = new JMenuItem("Set as Dependent");
        DepItem.setActionCommand("dependent");
        JMenuItem ClearDepItem = new JMenuItem("Clear Dependendency");
        ClearDepItem.setActionCommand("cleardependent");

        
        
        
        table.addMouseListener( new MouseAdapter()
        {
        	
            public void mouseReleased(MouseEvent e)
            {
               if(e.isPopupTrigger() & !e.isConsumed()) mousePressed(e);
            }

            public void mousePressed(MouseEvent e)
            {
            	//System.out.println(e);
                if (e.isPopupTrigger())
                {
                	//System.out.println("show popup");
                    JTable source = (JTable)e.getSource();
                    int row = source.rowAtPoint( e.getPoint() );
                    popupRow=row;
                    int column = source.columnAtPoint( e.getPoint() );
                    popupColumn=column;
                    popupColumnName=source.getColumnName(column);
                    int selRow=source.getSelectedRow();
                  //  if(row==-1 && column!=-1)
                   // {
                   // 	System.out.println("column header clicked I hope");
                    	//source.removeColumn(source.getColumn(source.getColumnName(column)));
                   // 	popupMenuRemove.show(e.getComponent(), e.getX(), e.getY());
                    //}
                    //else 
                    if (row!=-1 && selRow!=-1 &&  row!=selRow)
                    {
						int modelRow = source
								.convertRowIndexToModel(row);
						
						popupService = SO.get(modelRow);
                    	//System.out.println(selRow+" "+row);
                        //source.changeSelection(row, column, false, false);
                    	popupMenu2.show(e.getComponent(), e.getX(), e.getY());
                    }
                    else if(row!=-1 && selRow!=-1 && row==selRow)
                    {
                    	popupService=selectedService;
                    	//System.out.println("The same row "+selRow+" "+row);
                    	popupx=e.getXOnScreen();
                    	popupy=e.getYOnScreen();
                    	//System.out.println(source.getColumnName(column));
                    	popupMenu.show(e.getComponent(), e.getX(), e.getY());
                    }
                    e.consume();
                }
            }
        });
        
        head.addMouseListener(new MouseAdapter()
        {
        	public void mouseReleased(MouseEvent e)
            {
               if(e.isPopupTrigger() & !e.isConsumed()) mousePressed(e);
            }
        	
        	 public void mousePressed(MouseEvent e)
             {
             	//System.out.println(e.getSource());
                 if (e.isPopupTrigger())
                 {
                	 if(submenu!=null)popupMenuRemove.remove(submenu);
                	 submenu = createOtherPopup();
                	 popupMenuRemove.add(submenu);
                	 //System.out.println("column header clicked I hope");
                	 //source.removeColumn(source.getColumn(source.getColumnName(column)));
                	 popupMenuRemove.show(e.getComponent(), e.getX(), e.getY());
                	 JTableHeader head=(JTableHeader) e.getSource();
                	 columnnumber=head.columnAtPoint(new Point(e.getX(),e.getY()));
                	// System.out.println("mousepressed finished");
                 }
             }
        });
        
        
/*       RTItem.addActionListener(new ActionListener() {

            
            public void actionPerformed(ActionEvent e) {
                System.out.println("Right-click performed on table and choose RTGraph");
                
            }
        });
*/
        popupMenu.add(RTItem);
        popupMenu.add(freqItem);
        popupMenu.add(countGraph);
        popupMenu.add(highlightService);
        
        popupMenu2.add(DepItem);
        popupMenu2.add(ClearDepItem);
        popupMenuRemove.add(RemoveColumn);
        popupMenuRemove.add(ResetColumns);
        popupMenuRemove.addSeparator();
        popupMenuRemove.add(RealTimeUpdate);
        popupMenuRemove.add(DelayedUpdate);
        popupMenuRemove.add(ReloadTable);
        
        RTItem.addActionListener(this);
        DepItem.addActionListener(this);

        highlightService.addActionListener(this);
        countGraph.addActionListener(this);
        
        freqItem.addActionListener(this);
        ClearDepItem.addActionListener(this);
//        table.setComponentPopupMenu(popupMenu);
        RemoveColumn.addActionListener(this);
        ResetColumns.addActionListener(this);
        RealTimeUpdate.addActionListener(this);
        DelayedUpdate.addActionListener(this);
        ReloadTable.addActionListener(this);
        
		table.setRowSorter(sorter);
		table.setPreferredScrollableViewportSize(new Dimension(1000, 900));
		table.setFillsViewportHeight(true);

		// table.getModel().addTableModelListener(this);
		// model.addTableModelListener(this);

		// For the purposes of this example, better to have a single
		// selection.
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		table.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		TableColumn col = table.getColumnModel().getColumn(0);
		col.setMaxWidth(75);
		col.setMinWidth(25);
		col.setWidth(25);
		col.setPreferredWidth(25);
		// col.setResizable(false);
		col = table.getColumnModel().getColumn(1);
		col.setPreferredWidth(700);
		//col.setMaxWidth(700);
		// col.setResizable(false);
		//col = table.getColumnModel().getColumn(2);
		//col.setPreferredWidth(100);
		//col.setMaxWidth(100);
		// col.setResizable(false);
		//col = table.getColumnModel().getColumn(3);
		//col.setPreferredWidth(100);
		//col.setMaxWidth(100);
		// col.setResizable(false);
		//col = table.getColumnModel().getColumn(4);
		//col.setPreferredWidth(30);
		//col.setMaxWidth(100);
		// col.setResizable(false);

		table.setRowSorter(sorter);
		setupHighlightFields(table);
		//int colNum=model.getColumnNumber("Rate");
		//StatusColumnCellRenderer ratecell = new StatusColumnCellRenderer("Rate");
		//if(colNum>-1)table.getColumnModel().getColumn(colNum).setCellRenderer(ratecell);
		//colNum=model.getColumnNumber("Average");
		//StatusColumnCellRenderer averagecell = new StatusColumnCellRenderer("Average");
		//if(colNum>-1)table.getColumnModel().getColumn(colNum).setCellRenderer(averagecell);

		
		// When selection changes, provide user with row numbers for
		// both view and model.
		table.getTableHeader().addMouseListener(new TableHeaderMouseListener());
		table.getTableHeader().getColumnModel().addColumnModelListener(new TableColumnWidthListener());
		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener()
				{
					public void valueChanged(ListSelectionEvent event)
					{
						int viewRow = table.getSelectedRow();
						if (viewRow < 0)
						{
							// Selection got filtered away.
							// statusText.setText("");
							// System.out.println("selected row negative");
						} else
						{
							int modelRow = table
									.convertRowIndexToModel(viewRow);
							selectedRow=modelRow;
							// statusText.setText(
							// String.format("Selected Row in view: %d. " +
							// "Selected Row in model: %d.",
							// viewRow, modelRow));
							// System.out.println("Row "+modelRow+" selected = "+SO.get(modelRow).getsName());
							// new SDPanel(SO.get(modelRow));
							// sdview.newContentPane.repaint();
							selectedService = SO.get(modelRow);
							//sdview.newContentPane.setService(SO.get(modelRow));
//							if (cfg != null)
//							{
//								cfg.setVisible(false);
//								cfg.setEnabled(false);
//								cfg.dispose();
//							}
//							Map<Service, Map<Double, Double>> clist = new HashMap<Service, Map<Double, Double>>();
//							Map<Service, Map<Double, Double>> dlist = new HashMap<Service, Map<Double, Double>>();
//
//							Iterator<Service> is = selectedService
//									.getConsumers();
//							while (is.hasNext())
//							{
//								Service cs = is.next();
//								clist.put(cs, cs.getSamples());
//							}
//							is = selectedService.getDependents();
//							while (is.hasNext())
//							{
//								Service cs = is.next();
//								dlist.put(cs, cs.getSamples());
//							}
//							if (clist.isEmpty())
//								clist = null;
//							if (dlist.isEmpty())
//								dlist = null;
//							cfg = new CumulativeFrequencyGraph(SO.get(modelRow)
//									.getSName(), (HashMap<Double,Double>)SO.get(modelRow).getSamples().clone(),
//									SO.get(modelRow)
//											.calculateCumulativeDistribution(),
//									clist, dlist);
//							cfg.addWindowListener(MainForm.wl);
//							cfg.pack();
//							if (MainForm.togFreq)
//								cfg.setVisible(true);
//							else
//								cfg.setVisible(false);
						}
					}

				});

		scrollPane = new JScrollPane(table);
		ChangeListener changeListener = new ChangeListener() {
		    public void stateChanged(ChangeEvent e) {
		      BoundedRangeModel m = (BoundedRangeModel)(e.getSource());
		      //setUpdatesEnabled(!(m.getValueIsAdjusting()));
		      isScrolling=m.getValueIsAdjusting();
		    }
		  };
		  
		  scrollPane.getVerticalScrollBar().getModel().addChangeListener(changeListener);
		  scrollPane.getHorizontalScrollBar().getModel().addChangeListener(changeListener);
		// Add the scroll pane to this panel.
		add(scrollPane);

	}
	
	private class TableColumnWidthListener implements TableColumnModelListener
	{
	    @Override
	    public void columnMarginChanged(ChangeEvent e)
	    {
	        /* columnMarginChanged is called continuously as the column width is changed
	           by dragging. Therefore, execute code below ONLY if we are not already
	           aware of the column width having changed */
	        if(!isScrolling)
	        {
	            /* the condition  below will NOT be true if
	               the column width is being changed by code. */
	            if(table.getTableHeader().getResizingColumn() != null)
	            {
	                // User must have dragged column and changed width
	                isScrolling=true;
	            }
	        } 
	    }

	    @Override
	    public void columnMoved(TableColumnModelEvent e) { }

	    @Override
	    public void columnAdded(TableColumnModelEvent e) { }

	    @Override
	    public void columnRemoved(TableColumnModelEvent e) { }

	    @Override
	    public void columnSelectionChanged(ListSelectionEvent e) { }
		
	}
	
	private class TableHeaderMouseListener extends MouseAdapter
	{
	    @Override
	    public void mouseReleased(MouseEvent e)
	    {
	        /* On mouse release, check if column width has changed */
	        isScrolling=false;
	    }
	}
	private boolean isScrolling=false;
	
	public boolean isScrolling()
	{
		return isScrolling;
	}
	
	final JPopupMenu popupMenu = new JPopupMenu();
	final JPopupMenu popupMenu2=new JPopupMenu();
	final JPopupMenu popupMenuRemove = new JPopupMenu();
	
	private JMenu createOtherPopup()
	{
		JMenu menu=new JMenu("Fields");
		OtherMenuAction actionClass=new OtherMenuAction();
		ArrayList<String> columns=model.getUnDisplayedColumns();
		for(String s:columns){
			JMenuItem m=new JMenuItem(s);
			m.addActionListener(actionClass);
			m.setActionCommand(s);
			menu.add(m);
		}
		
		
		return menu;
	}
	
	public final List<String> getColumnsCommands(){
		ArrayList<String> columns=model.getUnDisplayedColumns();
		ArrayList<String> commands=new ArrayList<String>();
		for(String s:columns){
			String c="COMMAND: Window ServiceTable column \""+s+"\" off";
			commands.add(c);
		}
		columns=model.getDisplayedColumns();
		for(String s:columns){
			String c="COMMAND: Window ServiceTable column \""+s+"\" on";
			commands.add(c);
		}
		return commands;
	}
	
	private void createTable()
	{
		//System.out.println("create table called");
		model.removeTableModelListener(this);
		sorter = new TableRowSorter<ServiceTableModel>(model);
		sorter.setRowFilter(currentFilter);
		JTable mytable = new JTable(model)
		{

			/**
			 * 
			 */
			private static final long serialVersionUID = 3916897541446604843L;

			// Implement table cell tool tips.
			public String getToolTipText(MouseEvent e)
			{
				String tip = "";
				java.awt.Point p = e.getPoint();
				int rowIndex = rowAtPoint(p);
				int colIndex = columnAtPoint(p);
				// System.out.println("rowindex="+rowIndex);
				if (rowIndex == -1)
					return null;
				// int realColumnIndex = convertColumnIndexToModel(colIndex);
				int realRowIndex = convertRowIndexToModel(rowIndex);
				// System.out.println("realrowindex="+realRowIndex);
				tip = model.getService(realRowIndex).getAddData();
				// System.out.println("tip="+tip);
				return tip;
			}
		};
		mytable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		mytable.getTableHeader().addMouseListener(new TableHeaderMouseListener());
		mytable.getTableHeader().getColumnModel().addColumnModelListener(new TableColumnWidthListener());

		JTableHeader head=mytable.getTableHeader();
		//model.addTableModelListener(this);
		mytable.addMouseListener( new MouseAdapter()
        {
        	
            public void mouseReleased(MouseEvent e)
            {
               if(e.isPopupTrigger() & !e.isConsumed()) mousePressed(e);
            }

            public void mousePressed(MouseEvent e)
            {
            	//System.out.println(e);
                if (e.isPopupTrigger())
                {
                	//System.out.println("show popup");
                    JTable source = (JTable)e.getSource();
                    int row = source.rowAtPoint( e.getPoint() );
                    popupRow=row;
                    int column = source.columnAtPoint( e.getPoint() );
                    popupColumn=column;
                    popupColumnName=source.getColumnName(column);
                    int selRow=source.getSelectedRow();
                  //  if(row==-1 && column!=-1)
                   // {
                   // 	System.out.println("column header clicked I hope");
                    	//source.removeColumn(source.getColumn(source.getColumnName(column)));
                   // 	popupMenuRemove.show(e.getComponent(), e.getX(), e.getY());
                    //}
                    //else 
                    if (row!=-1 && selRow!=-1 &&  row!=selRow)
                    {
						int modelRow = source
								.convertRowIndexToModel(row);
						
						popupService = SO.get(modelRow);
                    	//System.out.println(selRow+" "+row);
                        //source.changeSelection(row, column, false, false);
                    	popupMenu2.show(e.getComponent(), e.getX(), e.getY());
                    }
                    else if(row!=-1 && selRow!=-1 && row==selRow)
                    {
                    	popupService=selectedService;
                    	//System.out.println("The same row "+selRow+" "+row);
                    	popupx=e.getXOnScreen();
                    	popupy=e.getYOnScreen();
                    	popupMenu.show(e.getComponent(), e.getX(), e.getY());
                    }
                    e.consume();
                }
            }
        });
        
        head.addMouseListener(new MouseAdapter()
        {
        	

			public void mouseReleased(MouseEvent e)
            {
               if(e.isPopupTrigger() & !e.isConsumed()) mousePressed(e);
            }
        	
        	 public void mousePressed(MouseEvent e)
             {
             	//System.out.println(e.getSource());
                 if (e.isPopupTrigger())
                 {
                	 if(submenu!=null)popupMenuRemove.remove(submenu);
                	 submenu = createOtherPopup();
                	 popupMenuRemove.add(submenu);
                	 //System.out.println("column header clicked I hope");
                	 //source.removeColumn(source.getColumn(source.getColumnName(column)));
                	 popupMenuRemove.show(e.getComponent(), e.getX(), e.getY());
                	 
                	 JTableHeader head=(JTableHeader) e.getSource();
                	 columnnumber=head.columnAtPoint(new Point(e.getX(),e.getY()));
                	 //System.out.println("mousepressed finished");
                 }
             }
        });
        
        //sorter=new TableRowSorter<ServiceTableModel>(model);
        mytable.setRowSorter(sorter);
		mytable.setPreferredScrollableViewportSize(new Dimension(1000, 900));
		mytable.setFillsViewportHeight(true);
		

		// table.getModel().addTableModelListener(this);
		// model.addTableModelListener(this);

		// For the purposes of this example, better to have a single
		// selection.
		mytable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		mytable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		TableColumn col = table.getColumnModel().getColumn(0);
		col.setMaxWidth(75);
		col.setMinWidth(25);
		col.setWidth(25);
		col.setPreferredWidth(25);
		// col.setResizable(false);
		col = mytable.getColumnModel().getColumn(1);
		col.setPreferredWidth(700);
		//col.setMaxWidth(700);
		// col.setResizable(false);
		//col = mytable.getColumnModel().getColumn(2);
		//col.setPreferredWidth(100);
		//col.setMaxWidth(100);
		// col.setResizable(false);
		//col = mytable.getColumnModel().getColumn(3);
		//col.setPreferredWidth(100);
		//col.setMaxWidth(100);
		// col.setResizable(false);
		//col = mytable.getColumnModel().getColumn(4);
		//col.setPreferredWidth(30);
		//col.setMaxWidth(30);
		// col.setResizable(false);
		setupHighlightFields(mytable);
		//int colNum=model.getColumnNumber("Rate");
		//if(colNum>-1)mytable.getColumnModel().getColumn(colNum).setCellRenderer(new StatusColumnCellRenderer("Rate"));
		//colNum=model.getColumnNumber("Average");
		//if(colNum>-1)mytable.getColumnModel().getColumn(colNum).setCellRenderer(new StatusColumnCellRenderer("Average"));


		// When selection changes, provide user with row numbers for
		// both view and model.

		mytable.getSelectionModel().addListSelectionListener(
				new ListSelectionListener()
				{
					public void valueChanged(ListSelectionEvent event)
					{
						int viewRow = table.getSelectedRow();
						if (viewRow < 0)
						{
							// Selection got filtered away.
							// statusText.setText("");
							// System.out.println("selected row negative");
						} else
						{
							int modelRow = table
									.convertRowIndexToModel(viewRow);
							selectedRow=modelRow;
							// statusText.setText(
							// String.format("Selected Row in view: %d. " +
							// "Selected Row in model: %d.",
							// viewRow, modelRow));
							// System.out.println("Row "+modelRow+" selected = "+SO.get(modelRow).getsName());
							// new SDPanel(SO.get(modelRow));
							// sdview.newContentPane.repaint();
							selectedService = SO.get(modelRow);
							//sdview.newContentPane.setService(SO.get(modelRow));
/*
							if (cfg != null)
							{
								cfg.setVisible(false);
								cfg.setEnabled(false);
								cfg.dispose();
							}
							Map<Service, Map<Double, Double>> clist = new HashMap<Service, Map<Double, Double>>();
							Map<Service, Map<Double, Double>> dlist = new HashMap<Service, Map<Double, Double>>();

							Iterator<Service> is = selectedService
									.getConsumers();
							while (is.hasNext())
							{
								Service cs = is.next();
								clist.put(cs, cs.getSamples());
							}
							is = selectedService.getDependents();
							while (is.hasNext())
							{
								Service cs = is.next();
								dlist.put(cs, cs.getSamples());
							}
							if (clist.isEmpty())
								clist = null;
							if (dlist.isEmpty())
								dlist = null;
							cfg = new CumulativeFrequencyGraph(SO.get(modelRow)
									.getSName(), (HashMap<Double,Double>)SO.get(modelRow).getSamples().clone(),
									SO.get(modelRow)
											.calculateCumulativeDistribution(),
									clist, dlist);
							cfg.addWindowListener(MainForm.wl);
							cfg.pack();
							if (MainForm.togFreq)
								cfg.setVisible(true);
							else
								cfg.setVisible(false);
								
*/
						}
						
					}

				});
		
		table=mytable;
		model.addTableModelListener(this);
		model.addServiceTableModelListener(this);
	}
	
	private void setupHighlightFields(JTable table)
	{
		System.out.println("setting up highlight fields");
		ServiceTableModel model=(ServiceTableModel) table.getModel();
		TableColumnModel columnModel = table.getColumnModel();
		Enumeration<TableColumn> cols=columnModel.getColumns();
		while(cols.hasMoreElements())
		{
			TableColumn tc=cols.nextElement();
			String columnName=model.getColumnName(tc.getModelIndex());
			
			StatusColumnCellRenderer cell = new StatusColumnCellRenderer(columnName);
			tc.setCellRenderer(cell);

			
		}
		System.out.println("end of setting up highlight fields");
	}

	JScrollPane scrollPane;
	private int popupx=10,popupy=10;
	
	public void resetSelectedRow()
	{
		selectedRow=-1;
	}

	private int selectedRow=-1;
	
	public void toggleFreqWindow()
	{
		//System.out.println("making freq window " + MainForm.togFreq);
		if (cfg != null)
		{
			if (MainForm.togFreq)
				cfg.setVisible(true);
			else
				cfg.setVisible(false);
		}
	}

	public CumulativeFrequencyGraph cfg = null;

	public JFreeChart getCumulativeFrequencyChart()
	{
		if (cfg == null)
			return null;
		return cfg.getChart();
	}

	private UIRowFilter currentFilter=null;
	
	public void setFilter(String layer, String service, boolean modelled)
	{
		currentFilter = new UIRowFilter(layer, service, modelled);

		sorter.setRowFilter(currentFilter);
	}
	
	public void setHighlightFilter(boolean highlight)
	{
		if(currentFilter==null){
			currentFilter = new UIRowFilter("ALL", "", true);
			
		}
		currentFilter.filterHighlighted(highlight);
		sorter.setRowFilter(currentFilter);
	}

	public void addSDView(ServiceDependencyView sd)
	{
		sdview = sd;
		// System.out.println(sd);
	}

	public void stopSorter()
	{
		
		// sorter.allRowsChanged();
		//sorter.modelStructureChanged();
		 table.setRowSorter(null);
		
	}

	public void startSorter()
	{

		table.setRowSorter(sorter);
		sorter.modelStructureChanged();
		// model = new ServiceTableModel(SO);

		// sorter.allRowsChanged();
		
	}

	private List<Service> SO;
	public static ServiceTableModel model;
	private JTable table;
	private ServiceDependencyView sdview = null;
	public TableRowSorter<ServiceTableModel> sorter;
	public Service selectedService;
	Service popupService;

	@Override
	public void tableChanged(TableModelEvent e)
	{
		/*while(isScrolling) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e1) {
				
			}
		}
		int count=0;
		EventQueue queue = Toolkit.getDefaultToolkit().getSystemEventQueue();
	    while (queue.peekEvent() != null & count<10) {
	        try {
	        	count++;
				Thread.sleep(10);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				
			}
	    }
	    */
		//System.out.println("in tableChanged "+e.getType()+" "+e.getFirstRow()+" "+e.getLastRow());
		// TODO Auto-generated method stub
		// System.out.println("erqwerqw");
		//int row = e.getFirstRow();
		//int column = e.getColumn();
		//ServiceTableModel model = (ServiceTableModel) e.getSource();
		// String columnName = model.getColumnName(column);
		//float data = (Float) model.getValueAt(row, column);
		// System.out.println("Value changed to "+ data+
		// " for service operation "+ SO.get(row).getsName());

		//System.out.println("tablechanged fired");
		//System.out.println(e.getType());
		if(e.getType()==TableModelEvent.UPDATE && e.getColumn()==TableModelEvent.ALL_COLUMNS)
		{
			
			setWidths(true);
		}
		
		if(selectedRow>=model.getRowCount()) selectedRow=-1;
		if(selectedRow!=-1){
			
			//System.out.println("reselecting row");
			int tablerow=table.convertRowIndexToView(selectedRow);
			//System.out.println("selectedRow, tableRow: "+selectedRow+", "+tablerow);
			if(tablerow!=-1)table.setRowSelectionInterval(tablerow, tablerow);
		} 
		/*
		if(e.getType()==TableModelEvent.INSERT )//&& selectedRow!=-1)
		{
			
			if(selectedRow>=SO.size()) selectedRow=-1;
			if(selectedRow!=-1){
				
				System.out.println("reselecting row");
				int tablerow=table.convertRowIndexToView(selectedRow);
				table.setRowSelectionInterval(tablerow, tablerow);
			} 
		}
		if(e.getType()==TableModelEvent.UPDATE )
		{
			if(selectedRow>=SO.size()) selectedRow=-1;
			if(selectedRow!=-1){
					
				System.out.println("reselecting row");
				int tablerow=table.convertRowIndexToView(selectedRow);
				table.setRowSelectionInterval(tablerow, tablerow);
			} 
		
			
		}
		*/
	}
	
	
	




	public MonitorWindowListener main=null;
	
	public void recordWidths()
	{
		Enumeration<TableColumn> e =table.getTableHeader().getColumnModel().getColumns();
		widths.clear();
		while( e.hasMoreElements())
		{
			TableColumn tc=e.nextElement();
			int w = tc.getWidth();
			Object o = tc.getHeaderValue().toString();
			//System.out.println("headervalue "+o);
			widths.put(o, w);
		}
	}
	
	public void setWidths(boolean add)
	{
		int i=0;
		Enumeration<TableColumn> e =table.getTableHeader().getColumnModel().getColumns();
		while( e.hasMoreElements())
		{
			TableColumn tc = e.nextElement();
			String o = tc.getHeaderValue().toString();
			Integer w=widths.get(o);
			if(add && w!=null && w>25){
				w=w-10;
				i=i+10;
			}
			//System.out.println("headerwidth "+w);
			//if(w!=null & (o.equals("Layer") | o.equals("Service Operation")))tc.setPreferredWidth(w);
			//if(w!=null) {tc.setPreferredWidth(w);tc.setWidth(w);}
			//table.doLayout();
			//else tc.setPreferredWidth(i);
		}
	}
	
	public void setWidth(String column, int width){
		TableColumnModel columns = table.getTableHeader().getColumnModel();
		Enumeration<TableColumn> e =table.getTableHeader().getColumnModel().getColumns();
		while( e.hasMoreElements())
		{
			TableColumn tc = e.nextElement();
			String o = tc.getHeaderValue().toString();
			if(o.equals(column)){//found my column
				tc.setPreferredWidth(width);
				System.out.println("column adjusted: "+column+"to width "+width);
				break;
			}
		}
	}
	
	
	
	private class dcol extends Thread
	{
		boolean update=false;
		
		
		public void run()
		{
			//if(update) model.fireTableStructureChanged();
			//else
			try {
				javax.swing.SwingUtilities.invokeAndWait(new Runnable()
				{
					public void run()
					{
						table.setRowSorter(null);
						int tablerow=table.convertRowIndexToView(selectedRow);
						
						recordWidths();
						
						int modelnumber=table.convertColumnIndexToModel(columnnumber);
						final String name=model.getColumnName(modelnumber); 
						//System.out.println("columnname "+name);
						TableColumn tc=table.getColumnModel().getColumn(columnnumber);

						//remove the column from the model 
						//(this is horrible but JTable.removeeColumn fails sometimes)
						if(model.columnRemoved(modelnumber, name))
						{
							//tell the sorter to reload itself from model
							//sorter=new TableRowSorter<ServiceTableModel>(model);
							//create a whole new table as firing structure change suffers same problem as removeColumn
							createTable();
							//replace the table in the scroll pane
							scrollPane.setViewportView(table);
							scrollPane.revalidate();
							//reset column selected number
							columnnumber=-1;
						
							//select the selected row in the new table
						
							//System.out.println(tablerow);
							
							if(tablerow>-1){
								int viewRow=table.convertRowIndexToView(tablerow);
								//System.out.println(viewRow);
								table.setRowSelectionInterval(viewRow, viewRow);
							}
							setWidths(false);
							update=true;
						}
						
					}
				});
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getActionCommand().equals("Count")){
			CountGraphWindow g = new CountGraphWindow(
					"Quantised Performance Service View for "
							+ selectedService.getType()+"."+selectedService.getSName(),
					selectedService);
			
			g.setVisible(true);
			g.setBounds(50, 50, 800, 600);
		}
		if(arg0.getActionCommand().equals("Highlight")){
			System.out.println(model.getValueAt(table.convertRowIndexToModel(popupRow), table.convertColumnIndexToModel(popupColumn)));
			if(selectedService!=null && !popupColumnName.equals("")){
				String command=null;
				if(popupColumnName.equals("Service Operation"))
					command="\"Service Operation\" \""+selectedService.getType()+"."+selectedService.getsName()+"\" 0 0 0";
				else if(popupColumnName.equals("Layer")||popupColumnName.equals("Count"))
					command="Layer \""+selectedService.getType()+"."+selectedService.getsName()+"\" 0 0 0";
				else {
					Double value=0D;
					Object o= model.getValueAt(table.convertRowIndexToModel(popupRow), table.convertColumnIndexToModel(popupColumn));
					if(o instanceof Double) value=(Double) o;
					else if(o instanceof Float) value=new Double((Float)o);
					else if(o instanceof Integer) value=new Double((Integer)o);
					command ="\""+popupColumnName+"\" \""+selectedService.getType()+"."+selectedService.getsName()+"\" 0 "+value*1.25+" "+value*2;
				}
				if(command!=null)Controller.getController().processRangeAddCommand(command);
			}
		}
		if(arg0.getActionCommand().equals("Reload")) model.fireTableDataChanged();
		if(arg0.getActionCommand().equals("RemoveColumn") && columnnumber!=-1)
		{
			//System.out.println("remove column, number "+columnnumber);
			
			/**
			 * Had to do all this rubbish as a workaround for JTable.removeColumn as
			 * it would throw exceptions for an internal array out of bounds
			 * There is a bug report which is over 2 years old still open around this
			 * 
			 */
			dcol d=new dcol();
			d.start();
			//d.start();
		}
		if(arg0.getActionCommand().equals("ResetColumns")){
			columnnumber=-1;
			model.resetColumns();
		}
		if(arg0.getActionCommand().equals("Realtime")){
			model.setDelayedUpdates(false);
		}
		if(arg0.getActionCommand().equals("Delay")){
			model.setDelayedUpdates(true);
		}
		if(arg0.getActionCommand().equals("dependent"))
		{
			//System.out.println("dependent  selected");
			//System.out.println(selectedService+" "+popupService);
			if(selectedService!=null & popupService!=null & selectedService!=popupService)
				selectedService.addDependentService(popupService);
		}
		if(arg0.getActionCommand().equals("cleardependent"))
		{
			if(selectedService!=null & popupService!=null & selectedService!=popupService)
			{
				selectedService.removeDependentService(popupService);
			}
		}
		if(arg0.getActionCommand().equals("RTGraph"))
		{
			//System.out.println("RTGraph selected");
			StartMonitorWindowEvent event = new StartMonitorWindowEvent(
					selectedService, popupx, popupy, 600, 250);
			if(main!=null)main.StartMonitor(event);
		}
		if(arg0.getActionCommand().equals("Frequency"))
		{
			if(selectedService!=null)
			{
				//System.out.println("frequency");
				Map<Service, Map<Double, Double>> clist = new HashMap<Service, Map<Double, Double>>();
				Map<Service, Map<Double, Double>> dlist = new HashMap<Service, Map<Double, Double>>();

				Iterator<Service> is = selectedService
						.getConsumers();
				while (is.hasNext())
				{
					Service cs = is.next();
					clist.put(cs, cs.getSamples());
				}
				is = selectedService.getDependents();
				while (is.hasNext())
				{
					Service cs = is.next();
					dlist.put(cs, cs.getSamples());
				}
				if (clist.isEmpty())
					clist = null;
				if (dlist.isEmpty())
					dlist = null;

				CumulativeFrequencyGraph c=new CumulativeFrequencyGraph(selectedService
						.getSName(), selectedService.getSamples(),
						selectedService
								.calculateCumulativeDistribution(),clist,dlist);
				c.pack();
				c.setVisible(true);
			}
		}
	}
	
	public void removeColumn(String column){
		table.setRowSorter(null);
		int tablerow=table.convertRowIndexToView(selectedRow);
		
		recordWidths();
		
		
		//System.out.println("columnname "+name);
		//TableColumn tc=table.getColumnModel().getColumn(columnnumber);

		//remove the column from the model 
		//(this is horrible but JTable.removeeColumn fails sometimes)
		if(model.columnRemoved(column))
		{
			//tell the sorter to reload itself from model
			//sorter=new TableRowSorter<ServiceTableModel>(model);
			//create a whole new table as firing structure change suffers same problem as removeColumn
			createTable();
			//replace the table in the scroll pane
			scrollPane.setViewportView(table);
			scrollPane.revalidate();
			//reset column selected number
			columnnumber=-1;
		
			//select the selected row in the new table
		
			//System.out.println(tablerow);
			
			if(tablerow>-1){
				int viewRow=table.convertRowIndexToView(tablerow);
				//System.out.println(viewRow);
				table.setRowSelectionInterval(viewRow, viewRow);
			}
			setWidths(false);
			
		}
	}
	
	public void addColumn(String column){
		recordWidths();
		if(model.addColumn(column))
		{
			table.setRowSorter(null);
			int tablerow=table.convertRowIndexToView(selectedRow);
			

			createTable();
			//replace the table in the scroll pane
			scrollPane.setViewportView(table);
			scrollPane.revalidate();
			//reset column selected number
			columnnumber=-1;
		
			//select the selected row in the new table
		
			//System.out.println(tablerow);
			
			if(tablerow>-1){
				int viewRow=table.convertRowIndexToView(tablerow);
				//System.out.println(viewRow);
				table.setRowSelectionInterval(viewRow, viewRow);
			}
			setWidths(true);
		}
		
	}
	
	private class OtherMenuAction implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			//System.out.println(arg0.getActionCommand());
			addColumn(arg0.getActionCommand());
			/*
			recordWidths();
			if(model.addColumn(arg0.getActionCommand()))
			{
				table.setRowSorter(null);
				int tablerow=table.convertRowIndexToView(selectedRow);
				

				createTable();
				//replace the table in the scroll pane
				scrollPane.setViewportView(table);
				scrollPane.revalidate();
				//reset column selected number
				columnnumber=-1;
			
				//select the selected row in the new table
			
				//System.out.println(tablerow);
				
				if(tablerow>-1){
					int viewRow=table.convertRowIndexToView(tablerow);
					//System.out.println(viewRow);
					table.setRowSelectionInterval(viewRow, viewRow);
				}
				setWidths(true);
			} */
		}
		
	}
	
	HashMap<Object,Integer> widths=new HashMap<Object,Integer>();

	public class StatusColumnCellRenderer extends DefaultTableCellRenderer {
		public StatusColumnCellRenderer(String field)
		{
			super();
			this.field=field;
			
		}
		private final DecimalFormat formatter = new DecimalFormat( "#.###" );
		  @Override
		  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
			boolean number=false;
			if(java.lang.Number.class.isInstance(value))number=true;
			
			//System.out.println("field "+field+" is a number is "+number);
			if(number & !Integer.class.isInstance(value))value=formatter.format((Number)value);
		    //Cells are by default rendered as a JLabel.
		    JLabel l = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
		    l.setForeground(Color.black);
		    int modelRow=table.convertRowIndexToModel(row);
		    if(modelRow==-1) return l;
		    //Get the status for the current row.
		    ServiceTableModel tableModel = (ServiceTableModel) table.getModel();
		    int colour=tableModel.getCellColour(field,modelRow); 
		    if(colour==ServiceTableModel.WHITE & !isSelected)l.setBackground(Color.WHITE);
		    else if(colour==ServiceTableModel.GREEN)l.setBackground(Color.GREEN);
		    else if(colour==ServiceTableModel.ORANGE)l.setBackground(Color.ORANGE);
		    else if(colour==ServiceTableModel.RED){l.setBackground(Color.RED);l.setForeground(Color.white);}
		    if(number)l.setHorizontalAlignment(SwingConstants.RIGHT);

		  //Return the JLabel which renders the cell.
		  return l;

		}
		public String field="Service Operation";
	}
}
