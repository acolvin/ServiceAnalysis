package serviceAnalysisUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.Math;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.batik.svggen.SVGGraphics2DIOException;
import org.apache.batik.dom.GenericDOMImplementation;

import org.w3c.dom.Document;
import org.w3c.dom.DOMImplementation;

import serviceAnalysisModel.Service;

public class SDPanel extends JPanel
{

	public class SDPanelMouseListener implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent e)
		{
			// TODO Auto-generated method stub
			int x = (e.getX() - 10) / 300;
			int y = (e.getY() - 30) / 100;
			GridPoint gp = new GridPoint(x, y);
			// System.out.println(sg.grid.get(gp));
			Service s = null;
			if (sg.grid.containsKey(gp))
			{
				s = sg.grid.get(gp);
			}
			if (s != null)
			{
				// System.out.println(x+", "+y + " : "+s.getSName());
				setService(s);
			}

		}

		@Override
		public void mouseEntered(MouseEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
			// TODO Auto-generated method stub

		}

	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1323035962301123768L;

	public void paintComponent(Graphics g)
	{
		// System.out.println("painting the pane");
		super.paintComponent(g);

		// int width=1024;
		// clear(g);
		Graphics2D g2d = (Graphics2D) g;
		HashMap<GridPoint, Integer> sl = new HashMap<GridPoint, Integer>();
		// g2d.fill(circle);
		// g2d.draw(square);
		if (SO != null)
		{
			// int dependentdepth=SO.getDependentDepth();
			// System.out.println(SO.getDependentsMaxLayerHeight(1));
			// this.setSize(1024,
			// Math.max(SO.getDependentCount(),SO.getConsumerCount())*20+150);

			// paintUMLCentre(g2d,450,(Math.max(SO.getDependentsMaxLayerHeight(1),SO.getConsumerCount())*100/2+50));

			Set<GridPoint> k = sg.grid.keySet();
			Iterator<GridPoint> ki = k.iterator();
			int elLen;
			GridPoint gp2;
			while (ki.hasNext())
			{
				elLen = 0;
				GridPoint gi = ki.next();
				System.out.println("processing gridpoint " + gi);

				Service s = sg.grid.get(gi);
				if (s.equals(SO))
					elLen = paintUMLLeft(g2d, gi.getX() * 300 + 10,
							gi.getY() * 100 + 110, s, Color.yellow);
				else
					elLen = paintUMLLeft(g2d, gi.getX() * 300 + 10,
							gi.getY() * 100 + 110, s);
				// GridPoint endgi;
				System.out.println("elLen: " + elLen);
				sl.put(gi, new Integer(elLen));
			}

			ki = sg.lines.keySet().iterator();

			while (ki.hasNext())
			{
				GridPoint gi = ki.next();
				System.out.println("line processg from: " + gi);
				// if(sg.lines.containsKey(gi))
				Integer u = sl.get(gi);
				if (u == null)
				{
					u = 200;
					System.out.println("Length:" + gi + " is zero!!");
				}
				elLen = u.intValue();

				{
					ArrayList<GridPoint> gpl = sg.lines.get(gi);
					Iterator<GridPoint> gpli = gpl.iterator();
					while (gpli.hasNext())
					{
						gp2 = gpli.next();
						System.out.println("line processing to: " + gp2);

						System.out.println(gi.toString() + "-->"
								+ gp2.toString());
						paintLine(g2d, gi, gp2, elLen);
					}
				}
			}
		}

	}

	private void paintLine(Graphics2D g2d, GridPoint gi, GridPoint gi2,
			int elLen)
	{
		// TODO Auto-generated method stub
		System.out.println("line " + gi + "->" + gi2);
		if (gi.getY() == gi2.getY())
		{// draw from centre
			g2d.drawLine(gi.getX() * 300 + 10 + elLen + 10,
					gi.getY() * 100 + 110 - 35, gi2.getX() * 300 + 20,
					gi2.getY() * 100 + 110 - 60 + 25);
			g2d.drawLine(gi2.getX() * 300 + 20, gi2.getY() * 100 + 110 - 60
					+ 25, gi2.getX() * 300 + 15, gi2.getY() * 100 + 110 - 60
					+ 20);
			g2d.drawLine(gi2.getX() * 300 + 20, gi2.getY() * 100 + 110 - 60
					+ 25, gi2.getX() * 300 + 15, gi2.getY() * 100 + 110 - 60
					+ 30);
		} else if(gi.getY() < gi2.getY())	
		{
			// g2d.drawLine(gi.getX()*300+10+elLen/2+10, gi.getY()*100+110-10,
			// gi2.getX()*300+20, gi2.getY()*100+110-60+25);
			g2d.drawLine(gi.getX() * 300 + 10 + elLen / 2 + 10,
					gi.getY() * 100 + 110 - 10, gi2.getX() * 300 - 20 + 10,
					gi2.getY() * 100 + 110 - 60);
			g2d.drawLine(gi2.getX() * 300 - 20 + 10,
					gi2.getY() * 100 + 110 - 60, gi2.getX() * 300 + 20 + 12,
					gi2.getY() * 100 + 110 - 60 + 25 - 15);
			g2d.drawLine(gi2.getX() * 300 + 20 + 12, gi2.getY() * 100 + 110
					- 60 + 25 - 15, gi2.getX() * 300 + 20 + 12 - 6, gi2.getY()
					* 100 + 110 - 60 + 25 - 15 + 1);
			g2d.drawLine(gi2.getX() * 300 + 20 + 12, gi2.getY() * 100 + 110
					- 60 + 25 - 15, gi2.getX() * 300 + 20 + 12 - 6, gi2.getY()
					* 100 + 110 - 60 + 25 - 15 - 4);
		} else if(gi.getY() > gi2.getY())
		{
			// g2d.drawLine(gi.getX()*300+10+elLen/2+10, gi.getY()*100+110-10,
			// gi2.getX()*300+20, gi2.getY()*100+110-60+25);
			g2d.drawLine(gi.getX() * 300 + 10 + elLen / 2 + 10,
					gi.getY() * 100 + 110 - 60, gi2.getX() * 300 - 20 + 10,
					gi2.getY() * 100 + 110 - 60+50);
			//arm
			g2d.drawLine(gi2.getX() * 300 - 20 + 10,
					gi2.getY() * 100 + 110 - 60 +50, gi2.getX() * 300 + 20 + 12,
					gi2.getY() * 100 + 110 - 60 +50 - 25 + 15);
			//arrowhead
			g2d.drawLine(gi2.getX() * 300 + 20 + 12, gi2.getY() * 100 + 110
					- 60 +50 - 25 + 15, gi2.getX() * 300 + 20 + 12 - 6, gi2.getY()
					* 100 + 110 - 60 +50 - 25 + 15 - 2);
			//arrowhead
			g2d.drawLine(gi2.getX() * 300 + 20 + 12, gi2.getY() * 100 + 110
					- 60 +50 - 25 + 15, gi2.getX() * 300 + 20 + 12 - 6, gi2.getY()
					* 100 + 110 - 60 +50 - 25 + 15 + 5);
		}
	}

	private void paintUMLCentre(Graphics2D g2d, int x, int y)
	{
		paintUMLCentre(g2d, x, y, SO);
	}

	private int paintUMLLeft(Graphics2D g2d, int x, int y, Service SO)
	{// returns length of elipse
		return paintUMLLeft(g2d, x, y, SO, Color.white);
	}

	private int paintUMLLeft(Graphics2D g2d, int x, int y, Service SO,
			Color colour)
	{
		Point c = new Point(x, y);

		double stdDev, percentile985, percentile95;

		stdDev = SO.getOperationTimeStdDev();
		percentile985 = SO.get985Percentile();
		percentile95 = SO.get95Percentile();
		System.out.println("98.5 percentile of " + SO.getOperationName() + "="
				+ stdDev);

		int i = SwingUtilities.computeStringWidth(g2d.getFontMetrics(),
				SO.getOperationName());
		int j = SwingUtilities.computeStringWidth(g2d.getFontMetrics(),
				new Float(SO.getOperationTime()).toString())
				+ SwingUtilities.computeStringWidth(g2d.getFontMetrics(),
						new Float(percentile95).toString()) + 20;
		int k = SwingUtilities.computeStringWidth(g2d.getFontMetrics(),
				SO.getServiceName());
		int j2 = SwingUtilities.computeStringWidth(g2d.getFontMetrics(),
				new Integer(SO.getCount()).toString())
				+ SwingUtilities.computeStringWidth(g2d.getFontMetrics(),
						new Float(percentile985).toString()) + 20;

		int l = Math.max(k, Math.max(i, Math.max(j, j2)));

		int jp95 = SwingUtilities.computeStringWidth(g2d.getFontMetrics(),
				new Float(percentile95).toString());
		int jp985 = SwingUtilities.computeStringWidth(g2d.getFontMetrics(),
				new Float(percentile985).toString());

		// int l=Math.max(k, Math.max(i,j));
		g2d.setPaint(colour);
		Rectangle a = new Rectangle(c.x, c.y - 70, l + 20 + 20, 50 + 20);
		// g2d.drawRect(c.x, c.y-70, l+20+20, 50+20);
		g2d.fill(a);

		Rectangle b = new Rectangle(c.x, c.y - 70 - 20, k + 20, 20);
		g2d.fill(b);

		g2d.setPaint(Color.white);
		Ellipse2D o = new Ellipse2D.Float(c.x + 10, c.y - 60, l + 20, 50);
		// g2d.drawOval(c.x+10, c.y-60, l+20, 50);
		g2d.fill(o);
		g2d.setPaint(Color.black);

		g2d.draw(a);
		g2d.draw(b);
		g2d.draw(o);

		g2d.drawString(SO.getServiceName(), c.x + 5, c.y - 70 - 5);
		g2d.drawString(SO.getOperationName(), c.x + l / 2 - i / 2 + 20,
				c.y - 60 + 30);
		// centred under operation
		// g2d.drawString(new Float(SO.getOperationTime()).toString() ,
		// c.x+l/2-j/2+20, c.y-60+45);
		// top left
		g2d.drawString(new Float(SO.getOperationTime()).toString(), c.x + 5,
				c.y - 58);
		g2d.drawString(new Integer(SO.getCount()).toString(), c.x + 5, c.y - 2);
		g2d.drawString(new Float(percentile95).toString(), c.x + l + 40 - 5
				- jp95, c.y - 58);
		g2d.drawString(new Float(percentile985).toString(), c.x + l + 40 - 5
				- jp985, c.y - 2);

		return l + 20;
	}

	private void paintUMLCentre(Graphics2D g2d, int x, int y, Service SO)
	{
		// Rectangle r=this.getBounds();
		// System.out.println(r);
		// r.x=0;r.y=0;
		Point c = new Point(x, y);
		int i = SwingUtilities.computeStringWidth(g2d.getFontMetrics(),
				SO.getOperationName());
		// int i=SwingUtilities.computeStringWidth( g2d.getFontMetrics(),
		// SO.getSName());
		int j = SwingUtilities.computeStringWidth(g2d.getFontMetrics(),
				new Float(SO.getOperationTime()).toString());
		int k = SwingUtilities.computeStringWidth(g2d.getFontMetrics(),
				SO.getServiceName());
		int j2 = SwingUtilities.computeStringWidth(g2d.getFontMetrics(),
				new Integer(SO.getCount()).toString());

		int l = Math.max(k, Math.max(i, Math.max(j, j2)));

		g2d.drawRect(c.x - l / 2 - 10 - 10, c.y - 25 - 10 - 20, k + 20, 20);
		g2d.drawString(SO.getServiceName(), c.x - l / 2 - 10 - 10 + 5,
				c.y - 25 - 10 - 5);
		g2d.drawRect(c.x - l / 2 - 10 - 10, c.y - 25 - 10, l + 20 + 20, 50 + 20);
		g2d.drawOval(c.x - l / 2 - 10, c.y - 25, l + 20, 50);
		// g2d.drawString(SO.getSName(), c.x-i/2, c.y+5);
		g2d.drawString(SO.getOperationName(), c.x - i / 2, c.y + 5);
		g2d.drawString(new Float(SO.getOperationTime()).toString(),
				c.x - j / 2, c.y + 20);

	}

	public SDPanel(Service s)
	{
		super();
		this.addMouseListener(new SDPanelMouseListener());
		// pane=new DrawingPane();
		// scrollPane=new JScrollPane(pane);
		// System.out.println("scroll size "+scrollPane.getBounds());
		// scrollPane.setPreferredSize(new Dimension(5,700));
		// add(pane,BorderLayout.CENTER);
		// System.out.println("scroll size "+scrollPane.getBounds());
		// scrollPane.setBounds(scrollPane.getParent().getBounds());

		if (s != null)
		{
			this.setService(s);
			super.invalidate();
		}
	}

	protected void clear(Graphics g)
	{
		super.paintComponent(g);
	}

	public void paintSVG() throws IOException
	{
		DOMImplementation domImpl = GenericDOMImplementation
				.getDOMImplementation();
		String svgNS = "http://www.w3.org/2000/svg";
		Document document = domImpl.createDocument(svgNS, "svg", null);
		SVGGraphics2D svgGenerator = new SVGGraphics2D(document);

		paintComponent(svgGenerator);

		// Finally, stream out SVG to the standard output using
		// UTF-8 encoding.
		boolean useCSS = true; // we want to use CSS style attributes
		String rawName = SO.getsName().replace('/', '-');
		rawName = rawName.replace('\\', '_');
		OutputStream outputStream = new FileOutputStream(rawName + ".svg");
		Writer out = new OutputStreamWriter(outputStream, "UTF-8");
		svgGenerator.stream(out, useCSS /* use css */);
		outputStream.flush();
		outputStream.close();

	}

	public OutputStream produceSVG()
	{
		OutputStream os = new ByteArrayOutputStream();
		DOMImplementation domImpl = GenericDOMImplementation
				.getDOMImplementation();
		String svgNS = "http://www.w3.org/2000/svg";
		Document document = domImpl.createDocument(svgNS, "svg", null);
		SVGGraphics2D svgGenerator = new SVGGraphics2D(document);
		paintComponent(svgGenerator);
		try
		{
			Writer out = new OutputStreamWriter(os, "UTF-8");
			svgGenerator.stream(out, false /* use css */);
			os.flush();
		} catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SVGGraphics2DIOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return os;
	}

	public void paint(Graphics g)
	{
		paintComponent(g);
	}

	public void setService(Service s)
	{
		// System.out.println("in set service "+s);
		if (s == null)
			return;
		SO = s;
		sg = new ServiceGrid();
		sg.fillGrid(s, new GridPoint(1, 0), sg.grid, sg.lines);
		Iterator<Service> it = s.getConsumers();
		int y = 0;
		ArrayList<GridPoint> gls = new ArrayList<GridPoint>();
		gls.add(new GridPoint(1, 0));
		while (it.hasNext())
		{
			sg.grid.put(new GridPoint(0, y), it.next());
			sg.lines.put(new GridPoint(0, y), gls);
			y += 1;
		}
		sg.MaxY = Math.max(sg.MaxY, y);
		Dimension d = new Dimension((sg.MaxX + 4) * 300 + 10,
				(sg.MaxY + 1) * 100 + 10 + 20);
		this.setPreferredSize(d);
		this.setSize(d);

		// SO.getDependentDepth();
		// SO.getDependentsMaxLayerHeight(1);
		// paintComponent(this.getGraphics());
		// this.getRootPane().repaint();
		this.repaint();
		// System.out.println(this.getParent());
		// this.invalidate();
	}

	private Service SO = null;
	private JScrollPane scrollPane;
	// private DrawingPane pane;
	private ServiceGrid sg = new ServiceGrid();
}
