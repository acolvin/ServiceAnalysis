package serviceAnalysisUI;

import java.util.Date;

import serviceAnalysisModel.Service;

public interface CountGraphListener {
	public void TimeRangeChanged(Service service, Date start, Date end);
}
