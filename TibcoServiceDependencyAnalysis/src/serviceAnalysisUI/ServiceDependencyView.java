package serviceAnalysisUI;

import javax.swing.*;
import serviceAnalysisModel.Service;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

public class ServiceDependencyView implements ActionListener
{

	public ServiceDependencyView()
	{
		super();
	}

	public void createGUI()
	{
		// Create and set up the window.
		frame = new JFrame("Service Dependency");
		if(MainForm.image!=null)
			frame.setIconImage(MainForm.image);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.addWindowListener(MainForm.wl);

		// Create and set up the content pane.
		newContentPane = new SDPanel(null);
		newContentPane.setOpaque(true); // content panes must be opaque

		// JButton b1 = new JButton("Disable middle button");
		// b1.setVerticalTextPosition(AbstractButton.CENTER);
		// b1.setHorizontalTextPosition(AbstractButton.LEADING); //aka LEFT, for
		// left-to-right locales
		// b1.setMnemonic(KeyEvent.VK_D);

		JMenu file = new JMenu("File");
		file.setMnemonic('F');
		file.add(createMenuItem("Save as SVG", this, "SaveSVG", 'S',
				KeyEvent.VK_S));
		JMenuBar menu = new JMenuBar();
		menu.add(file);

		frame.setJMenuBar(menu);

		JScrollPane sp = new JScrollPane(newContentPane);
		// JDesktopPane dtopFrame=new JDesktopPane();
		frame.setContentPane(sp);
		// frame.setContentPane(newContentPane);
		newContentPane.setBounds(frame.getBounds());
		// frame.setContentPane(dtopFrame);

		// JInternalFrame iFrame=new JInternalFrame();
		// iFrame.setContentPane(newContentPane);

		// Display the window.
		// iFrame.pack();
		// iFrame.setVisible(true);

		frame.setBounds(50, 50, 500, 500);

		// iFrame.setBounds(5, 5, 700, 700);
		// dtopFrame.setBounds(10, 10, 800, 800);
		// dtopFrame.add(iFrame);

		// frame.pack();
		// frame.setVisible(true);
	}

	public void toggleWindow()
	{
		System.out.println("making SDView window " + MainForm.togSDView);
		if (MainForm.togSDView)
			frame.setVisible(true);
		else
			frame.setVisible(false);
	}

	public void DrawDependency(Service s)
	{

	}

	public JMenuItem createMenuItem(String label, ActionListener listener,
			String command, int mnemonic, int accKey)
	{
		JMenuItem item = new JMenuItem(label);
		item.addActionListener(listener);
		item.setActionCommand(command);
		if (mnemonic != 0)
			item.setMnemonic(mnemonic);
		if (accKey != 0)
			item.setAccelerator(KeyStroke.getKeyStroke(accKey,
					java.awt.Event.CTRL_MASK));
		return item;
	}

	public void ShowUI()
	{
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				frame.setVisible(true);
				// createAndShowGUI();
			}
		});
	}

	public ServiceDependencyView(List<Service> so)
	{
		super();
		SO = so;
	}

	protected JFrame frame = null;
	private List<Service> SO;
	public SDPanel newContentPane;

	@Override
	public void actionPerformed(ActionEvent event)
	{
		// TODO Auto-generated method stub

		JMenuItem item = (JMenuItem) event.getSource();
		String cmd = item.getActionCommand();

		if (cmd.equals("SaveSVG"))
			try
			{
				newContentPane.paintSVG();
			} catch (Exception e)
			{
			}
	}

}
