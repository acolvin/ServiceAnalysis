package serviceAnalysisUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import serviceAnalysisModel.Service;


public class CountGraphWindow extends JFrame implements WindowListener, ActionListener, ItemListener,CountGraphListener, ChangeListener
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3578752254542908308L;
	private JSpinner threadSpinner;
	private CountGraph CG;
	private int initialPeriod=1;
	private final int maxPeriod=6000;
	private JCheckBox timings=new JCheckBox("Show Timings",true);
	private JCheckBox minmax=new JCheckBox("Show Min/Max",true);
	private JCheckBox activeUsers=new JCheckBox("Show Active Users",true);
	private JCheckBox variance=new JCheckBox("Show Variances",true);
	private JCheckBox hour=new JCheckBox("Show Hourly Rate",true);
	private JSlider scaleFactor ;
	private JCheckBoxMenuItem mTimings,mMinmax,mActiveUsers,mVariance,mHour;
	
	private static ArrayList<CountGraphWindow> CountGraphWindows=new ArrayList<CountGraphWindow>();

	/**
	 * 
	 */

	public CountGraphWindow(String title, Service service, int period){
		super(title);
		if(period<=maxPeriod & period>=1)initialPeriod=period;
		create(service);
	}
	
	public CountGraphWindow(String title, Service service)
	{
		super(title);
		create(service);
	}
	
	public static StringBuffer getCountGraphWindowsSettings()
	{
		StringBuffer sb=new StringBuffer();
		synchronized(CountGraphWindows ){
			for(CountGraphWindow cgw:CountGraphWindows){
				Service s=cgw.getService();
				Rectangle r=cgw.getBounds();
				sb.append("COMMAND: Window Count \""+s.getType()+"\" \""+s.getService()+"\" \""+s.getOperationName()+"\" ");
				sb.append(r.x+" "+r.y+" "+r.width+" "+r.height+" "+cgw.initialPeriod+"\n");
				
			}
		}
		return sb;
	}
	
	private Service service=null;
	public Service getService(){
		return service;
	}
	private void create(Service service){
		this.service=service;
		synchronized(CountGraphWindows ){CountGraphWindows.add(this);}
		if(MainForm.image!=null)
			setIconImage(MainForm.image);
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);

		CG = new CountGraph(service);
		CG.addListener(this);
		
		JMenuBar menu=new JMenuBar();
		JMenu menuFile=new JMenu("File");
		menuFile.setMnemonic('F');
		JMenu menuOptions=new JMenu("Options");
		menuOptions.setMnemonic('O');
		menu.add(menuFile);
		menu.add(menuOptions);
		mTimings=MainForm.createToggleMenuItem("Show Timings", this, "mTiming", 'T', KeyEvent.VK_T);
		mTimings.setSelected(true);
		menuOptions.add(mTimings);
		mMinmax=MainForm.createToggleMenuItem("Show Min's/Max's", this, "mMinmax", 'M', KeyEvent.VK_M);
		mMinmax.setSelected(true);
		menuOptions.add(mMinmax);
		mActiveUsers=MainForm.createToggleMenuItem("Show Active Users", this, "mActiveUsers", 'A', KeyEvent.VK_A);
		mActiveUsers.setSelected(true);
		menuOptions.add(mActiveUsers);
		mVariance=MainForm.createToggleMenuItem("Show Variance", this, "mVariance", 'V', KeyEvent.VK_V);
		mVariance.setSelected(true);
		menuOptions.add(mVariance);
		mHour=MainForm.createToggleMenuItem("Show Hourly Rate", this, "mHour", 'H', KeyEvent.VK_H);
		mHour.setSelected(true);
		menuOptions.add(mHour);
		this.setJMenuBar(menu);
		
		scaleFactor = new JSlider(JSlider.VERTICAL,1,centreScale*2,centreScale);
		scaleFactor.addChangeListener(this);
		scaleFactor.setPaintTrack(false);
		scaleFactor.setPaintLabels(true);
		
		
		JPanel cpp=new JPanel(new BorderLayout());
		cpp.add(CG.getChartPanel(),BorderLayout.CENTER);
		cpp.add(scaleFactor,BorderLayout.LINE_END);
		
		threadSpinner = new JSpinner(new SpinnerNumberModel(initialPeriod,1,maxPeriod,1));
		//JPanel jpp=new JPanel();
		//jpp.setLayout(new BoxLayout(jpp,BoxLayout.LINE_AXIS));
		JButton resetButton=new JButton("Draw");
		JPanel jpp2=new JPanel();
		jpp2.setLayout(new BoxLayout(jpp2,BoxLayout.LINE_AXIS));
		JPanel jpp3=new JPanel();
		jpp3.setLayout(new BoxLayout(jpp3,BoxLayout.PAGE_AXIS));
		
		resetButton.setActionCommand("draw");
		resetButton.addActionListener( this);
		
		//JButton menuButton=new JButton("Options...");
		//menuButton.setActionCommand("options");
		//menuButton.addActionListener(this);
		hour.setActionCommand("hour");
		hour.addActionListener(this);
		timings.setActionCommand("timings");
		timings.addActionListener(this);
		minmax.setActionCommand("minmax");
		minmax.addActionListener(this);
		activeUsers.setActionCommand("activeUsers");
		activeUsers.addActionListener(this);
		variance.setActionCommand("variance");
		variance.addActionListener(this);
		jpp2.add(new JLabel("minute(s)"));
		//jpp.add(Box.createHorizontalGlue());
		jpp2.add(threadSpinner);
		//jpp.add(Box.createHorizontalGlue());
		jpp2.add(resetButton);
		//jpp2.add(Box.createHorizontalGlue());
		//jpp2.add(menuButton);
		//jpp.add(Box.createHorizontalGlue());
		//jpp2.add(hour);
		//jpp2.add(Box.createHorizontalStrut(10));
		//jpp2.add(timings);
		//jpp2.add(minmax);
		//jpp2.add(variance);
		//jpp.add(Box.createHorizontalGlue());
		//jpp2.add(Box.createHorizontalStrut(10));
		//jpp2.add(activeUsers);
		
		//jpp2.setBackground(Color.YELLOW);
		//jpp.add(Box.createHorizontalGlue());
		//jpp2.add(jpp);
		//jpp2.add(Box.createHorizontalGlue());
		//jpp3.add(minmax);
		//jpp3.add(activeUsers);
		//jpp2.add(jpp3);
		JPanel mpp=new JPanel();
		mpp.setLayout(new BoxLayout(mpp,BoxLayout.LINE_AXIS));
		mpp.add(new JLabel("mean (ms) "));
		mpp.add(mean);
		mpp.add(Box.createHorizontalGlue());
		mpp.add(new JLabel(" 95th %ile (ms) "));
		mpp.add(per95);
		mpp.add(Box.createHorizontalGlue());
		mpp.add(new JLabel(" 99th %ile (ms) "));
		mpp.add(per99);
		JPanel spp=new JPanel(new BorderLayout());
		spp.add(mpp);
		spp.add(jpp2,BorderLayout.SOUTH);
		JPanel content=new JPanel(new BorderLayout());
		content.add(cpp);
		content.add(spp,BorderLayout.SOUTH);
		
		
		this.setPreferredSize(new java.awt.Dimension(600, 350));

		setContentPane(content);
		// MainForm.ttg=this;
		this.addWindowListener(this);
		
		CG.forceTimeRangeChangedEvents();
		
		ticktock.start();
	}

	private JLabel mean=new JLabel();
	private JLabel per95=new JLabel();
	private JLabel per99=new JLabel();
	@Override
	public void windowActivated(WindowEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent arg0)
	{
		// TODO Auto-generated method stub
		synchronized(CountGraphWindows ){
			CountGraphWindows.remove(this);
		}
		tickstarted=false;
	}

	@Override
	public void windowDeactivated(WindowEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand().equals("draw"))
		{
			//System.out.println("draw clicked");
			redraw=true;
			Integer period = (Integer)threadSpinner.getValue();
			
			if(period!=null)
			{
				
				initialPeriod=period;
				CG.changeAveragePeriod(period);
				//CG.showMaxMin(!CG.isMaxShown());
			}
		}
		else if(e.getActionCommand().equals("minmax"))
		{
			//System.out.println("minmax clicked");
			mMinmax.setSelected(minmax.isSelected());
			CG.showMaxMin(minmax.isSelected());
			
		}
		else if(e.getActionCommand().equals("activeUsers"))
		{
			//System.out.println("activeUsers clicked");
			mActiveUsers.setSelected(activeUsers.isSelected());
			CG.showActiveUsers(activeUsers.isSelected());
		}
		else if(e.getActionCommand().equals("variance"))
		{
			//System.out.println("variance clicked");
			mVariance.setSelected(variance.isSelected());
			CG.showVariance(variance.isSelected());
		}
		else if(e.getActionCommand().equals("timings")){
			//System.out.println("timings clicked");
			mTimings.setSelected(timings.isSelected());
			CG.showAverages(timings.isSelected());
			
		}
		else if(e.getActionCommand().equals("hour")){
			//System.out.println("hour clicked");
			mHour.setSelected(hour.isSelected());
			CG.showHourlyRate(hour.isSelected());
		}
	}

	private final DecimalFormat formatter=new DecimalFormat("#.###");
	
	@Override
	public void TimeRangeChanged(Service service, Date start, Date end) {
		
		//System.out.println("time range changed");
		//redraw=false;  //need to click draw to restart the redraws
		HashMap<String,Double> results=service.limitedMean(start, end, 1);
		Double mean=results.get("MEAN");
		Double per95=results.get("95calc");
		Double per99=results.get("99calc");
		if(mean!=null)this.mean.setText(formatter.format(mean));
		if(per95!=null)this.per95.setText(formatter.format(per95));
		if(per99!=null)this.per99.setText(formatter.format(per99));
	}

	public void recalculate(){
		//System.out.println("Timer triggered");
		CG.changeAveragePeriod(initialPeriod);
	}
	
	private GraphTimer ticktock=new GraphTimer();
	private boolean tickstarted=false;
	private boolean redraw=true;
	
	private class GraphTimer extends Thread
	{
		public GraphTimer()
		{
			super("Count Timer");
			this.setDaemon(true);
		}
		@Override
		public void run() {
			tickstarted=true;
			while(tickstarted)
			try {
				Thread.sleep(60000l); // one minute
				//do something here;
				if(redraw)recalculate();
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("tick thread finished");
		}
		
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		JSlider source = (JSlider)e.getSource();
	    if (!source.getValueIsAdjusting()) {
	    	int value=(int)source.getValue()-centreScale;
	    	//System.out.println(value);
	    	double d=Math.pow(2d, value/6d);
	    	//System.out.println(d);
	    	source.setValue(centreScale);
	    	if(value!=0){
	    		CG.scaleFactor(d);
	    		recalculate();
	    	}
	    }
		
	}
	
	private int centreScale=36;

	@Override
	public void itemStateChanged(ItemEvent e) {
		JCheckBoxMenuItem item = ((JCheckBoxMenuItem) e.getItem());
		int state = e.getStateChange();
		
		if(item==mTimings){
			System.out.println("Timings selected");
			CG.showAverages(item.isSelected());
			timings.setSelected(item.isSelected());
		}
		if(item==mMinmax){
			System.out.println("minmax selected");
			
			CG.showMaxMin(item.isSelected());
			minmax.setSelected(item.isSelected());
		}
		if(item==mActiveUsers){
			System.out.println("active users selected");
			
			CG.showActiveUsers(item.isSelected());
			activeUsers.setSelected(item.isSelected());
		}
		if(item==mVariance){
			System.out.println("variance selected");
			
			CG.showVariance(item.isSelected());
			variance.setSelected(item.isSelected());
		}
		if(item==mHour){
			System.out.println("hour rate selected");
			
			CG.showHourlyRate(item.isSelected());
			hour.setSelected(item.isSelected());
		}
	}
}
