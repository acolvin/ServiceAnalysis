package serviceAnalysisUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisModel.SAControl.TeeConsumer;
import serviceAnalysisModel.SAControl.TextReports;

public class ReportWindow extends JFrame implements TeeConsumer,WindowListener, ActionListener {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 4110866168149448803L;

	public void ReportWindow() {
    	
    	this.setTitle("Reports");
    	if(MainForm.image!=null)
			setIconImage(MainForm.image);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.addWindowListener(this);
    	
    	
    	
    	//tr=new TextReports();
    	
 
    	
    	
    }
    
    public void showUI()
    {
    	ArrayList<String> tags=TextReports.getTagList();
    	for(String s:tags)
    	{
    		System.out.println(s);
    	}
    	setTitle("Reports");
    	JPanel jp=new JPanel(new BorderLayout());
    	
    	
    	
    	//output = new JTextArea();
    	TextReports.TeeOutputStream(this);
    	output.setVisible(true);
    	jp.setPreferredSize(new Dimension(400,400));
//    	jp.add(output);

    	jp.add(new JScrollPane(output));
    	//jp.add(input,BorderLayout.SOUTH);
    	setContentPane(jp);
    	setSize(400, 400);
        setLocationRelativeTo(null);
        setVisible(true);
        setPreferredSize(new Dimension(400,400));
    	
        JMenuBar menu = new JMenuBar();
        JMenu reports = new JMenu("Reports");
        
        reports.add(createMenuItem("Service Report", this, "ServiceReport",
				'S', KeyEvent.VK_S));
        reports.add(createMenuItem("Count Report (second)", this, "CountReport",
				'C', KeyEvent.VK_C));
        reports.add(createMenuItem("Count Report (hour)", this, "CountReportHour",
				'H', KeyEvent.VK_H));
        reports.add(createMenuItem("Count Report (day)", this, "CountReportDay",
				'D', KeyEvent.VK_D));
        
        menu.add(reports);
        this.setJMenuBar(menu);
  	
       	setBounds(300,200,400,400);
       	//System.out.println(output);
       	//output.append("test");
    	javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
		    	pack();  
				f.setVisible(true);
				
			}
		});
    }
    
    JFrame f=this;
    TextReports tr;
    private JTextArea output=new JTextArea();
    private JTextField input=new JTextField();
    
	@Override
	public void appendText(final String text) {
		//System.out.println("appendtext: "+text);
        if (EventQueue.isDispatchThread()) {
        	//System.out.println("outputing");
            output.append(text);
            output.setCaretPosition(output.getText().length());
        } else {

        	javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    appendText(text);
                }
            });

        }

	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		
		TextReports.RemoveTeeOutputStream();
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public JMenuItem createMenuItem(String label, ActionListener listener,
			String command, int mnemonic, int accKey)
	{
		JMenuItem item = new JMenuItem(label);
		item.addActionListener(listener);
		item.setActionCommand(command);

		if (mnemonic != 0)
			item.setMnemonic(mnemonic);
		if (accKey != 0)
			item.setAccelerator(KeyStroke.getKeyStroke(accKey,
					java.awt.Event.CTRL_MASK));
		return item;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getActionCommand().equals("ServiceReport"))
		{
			output.setText("");
			TextReports.ServiceName(Controller.getController().getServices());
		}
		if(arg0.getActionCommand().startsWith("CountReport"))
		{
			int size=1;
			if(arg0.getActionCommand().equals("CountReportHour")) size=60;
			if(arg0.getActionCommand().equals("CountReportDay")) size=60*24;
			
			output.setText("");
			CountReport(size);
		}
		
	}

	private void CountReport(int size)
	{
		for(Service s:Controller.getController().getServices())
		{
			TextReports.sampleCountReport(s, size);
		}
	}
}
