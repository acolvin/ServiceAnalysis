package serviceAnalysisUI;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import serviceAnalysisModel.ServiceMapper;
import serviceAnalysisModel.SAControl.Controller;

public class MapperWindow extends JFrame implements WindowListener,ActionListener, KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4590543582946561563L;
	private MapperTable mt;
	private JPanel panel;
	private JScrollPane scroll;
	private JComboBox type;
	private JTextField matchField;
	private JTextField searchField;
	private JTextField replaceField;
	private JLabel resultField;
	private JTextField testString;
	private JButton addButton;
	private JComboBox addtype;
	private JButton upButton;
	private JButton downButton;

	public MapperWindow()
	{
		if(MainForm.image!=null)
			setIconImage(MainForm.image);
		mt=new MapperTable(this);
		mt.createGUI();
		panel=new JPanel(new BorderLayout());
		//panel.add(mt);
		mt.setVisible(true);
		panel.setVisible(true);
		scroll= new JScrollPane(mt);
		
		panel.add(scroll);
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
		//panel.add(buttonPanel,BorderLayout.SOUTH);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JButton deleteButton = new JButton("Delete");
		deleteButton.setActionCommand("Delete");
		deleteButton.addActionListener(this);
		buttonPanel.add(deleteButton);
		//this.setPreferredSize(new java.awt.Dimension(600, 500));
		
		
		JPanel bottomPanel = new JPanel(new BorderLayout());
		JPanel testPanel = new JPanel();
		testPanel.setLayout(new BoxLayout(testPanel,BoxLayout.LINE_AXIS));
		bottomPanel.add(testPanel);
		bottomPanel.add(buttonPanel, BorderLayout.SOUTH);
		panel.add(bottomPanel,BorderLayout.SOUTH);
		String [] typeStrings={"Service","Operation"};
		type=new JComboBox(typeStrings);
		type.setActionCommand("combo");
		type.addActionListener(this);
		testString=new JTextField("Test String");
		testString.addKeyListener(this);
		resultField =new JLabel("result here");
		testPanel.add(type);
		testPanel.add(testString);
		testPanel.add(resultField);
		
		//to be used for adding
		addtype=new JComboBox(typeStrings);
		matchField=new JTextField();
		searchField=new JTextField();
		replaceField=new JTextField();
		addButton = new JButton("Add");
		addButton.setActionCommand("Add");
		JPanel addPanel = new JPanel();
		addPanel.setLayout(new BoxLayout(addPanel,BoxLayout.LINE_AXIS));
		addPanel.add(addtype);
		addPanel.add(matchField);
		addPanel.add(searchField);
		addPanel.add(replaceField);
		addPanel.add(addButton);
		addButton.addActionListener(this);
		upButton =new JButton("Up");
		downButton = new JButton("Down");
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(upButton);
		buttonPanel.add(downButton);
		upButton.setActionCommand("UP");
		downButton.setActionCommand("DOWN");
		//upButton.setEnabled(false);
		//downButton.setEnabled(false);
		upButton.addActionListener(this);
		downButton.addActionListener(this);
		
		bottomPanel.add(addPanel, BorderLayout.NORTH);
		
		JMenu file = new JMenu("File");
		file.setMnemonic('F');
		file.add(createMenuItem("Load Matches", this, "Load", 'L',
				KeyEvent.VK_L));
		file.add(createMenuItem("Save Matches", this, "Save", 'S',
				KeyEvent.VK_S));
		JMenuBar menu = new JMenuBar();
		menu.add(file);
		
		this.setJMenuBar(menu);
		
		
		setContentPane(panel);
		this.pack();
		this.addWindowListener(this);
		
	}
	
	public JMenuItem createMenuItem(String label, ActionListener listener,
			String command, int mnemonic, int accKey)
	{
		JMenuItem item = new JMenuItem(label);
		item.addActionListener(listener);
		item.setActionCommand(command);
		if (mnemonic != 0)
			item.setMnemonic(mnemonic);
		if (accKey != 0)
			item.setAccelerator(KeyStroke.getKeyStroke(accKey,
					java.awt.Event.CTRL_MASK));
		return item;
	}
	
	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getActionCommand().equalsIgnoreCase("Delete"))
		{
			System.out.println("delete clicked");
			int[] rows=mt.getSelectedRows();
			if(rows.length>0)
			{
				for(int i=rows.length-1;i>=0;i--)
				{
					//System.out.println("delete row "+i);
					mt.deleteMatch(rows[i]);
					
				}
				processTest();
			}
			
		}
		if(arg0.getActionCommand().equalsIgnoreCase("combo"))
		{
			processTest();
		}
		if(arg0.getActionCommand().equalsIgnoreCase("Load"))
		{
			JFileChooser fc = new JFileChooser();
			int returnVal2 = fc.showOpenDialog(this);
			if (returnVal2 == JFileChooser.APPROVE_OPTION)
			{
				File ff = fc.getSelectedFile();
				try
				{
					
					Controller.getController().loadMatchFile(ff.getCanonicalPath());

				} catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				mt.refresh();
			}
		}
		if(arg0.getActionCommand().equalsIgnoreCase("Save"))
		{
			JFileChooser fc = new JFileChooser();
			int returnVal2 = fc.showOpenDialog(this);
			if (returnVal2 == JFileChooser.APPROVE_OPTION)
			{
				File ff = fc.getSelectedFile();
				boolean ret=saveMatches(ff);
				if(!ret)
				{
					//show warning dialog
					 JOptionPane.showMessageDialog(this, "Failed to save Matches to selected file.\n"+errorMessage);
							
				}
				if(ret & errorMessage!=null)
					JOptionPane.showMessageDialog(this, "Saved File with Warning\n"+errorMessage);

			}
		}
		if(arg0.getActionCommand().equalsIgnoreCase("Add"))
		{
			//String testMe=testString.getText();
			
			Object combo=addtype.getSelectedItem();
			int cvalue=0;
			String c=null;
			if(combo.getClass().equals(String.class))c=(String)combo;
			//System.out.println("type="+c);
			if(c!=null)
				if(c.equals("Service"))cvalue=ServiceMapper.SERVICE;
				else if(c.equals("Operation"))cvalue=ServiceMapper.OPERATION;
			
			ServiceMapper.addMatch(matchField.getText(), searchField.getText(), replaceField.getText(), cvalue);
			mt.refresh();
			processTest();
		}
		if(arg0.getActionCommand().equalsIgnoreCase("UP"))
		{
			//System.out.println("up clicked");
			int[] rows=mt.getSelectedRows();
			if(rows.length==1)
			{
				//System.out.println("1 row selected");
				mt.move(true,rows[0]);
				//mt.refresh();
				processTest();
			}
		}
		if(arg0.getActionCommand().equalsIgnoreCase("DOWN"))
		{
			//System.out.println("down clicked");
			int[] rows=mt.getSelectedRows();
			if(rows.length==1)
			{
				//System.out.println("1 row selected"); 
				mt.move(false,rows[0]);
				//mt.refresh();
				processTest();
			}
		}
		
	}


	private String errorMessage=null;
	private boolean saveMatches(File ff) {
		errorMessage=null;
		FileOutputStream out=null;
		boolean ret=true;
		try {
			if(!ff.createNewFile()){
				if(ff.isFile() & ff.delete())
					ff.createNewFile();
				else {errorMessage="Couldn't Delete File"; ret=false;}
			}
			if(ret){
				out=new FileOutputStream(ff);
				out.write("###Matches###\n".getBytes());
				ServiceMapper.saveMatchFile(out);
				out.flush();
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			errorMessage=e.getMessage();
			//e.printStackTrace();
			
			ret= false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			errorMessage=e.getMessage();
			//e.printStackTrace();
			ret= false;
		}
		finally{
			if(out!=null)
				try {
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					errorMessage="Unable to close output: "+e.getMessage();
					//e.printStackTrace();
				}
			
		}
		return ret;
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void processTest()
	{
		String testMe=testString.getText();
		//System.out.println("testMe="+testMe);
		Object combo=type.getSelectedItem();
		int cvalue=0;
		String c=null;
		if(combo.getClass().equals(String.class))c=(String)combo;
		//System.out.println("type="+c);
		if(c!=null)
			if(c.equals("Service"))cvalue=ServiceMapper.SERVICE;
			else if(c.equals("Operation"))cvalue=ServiceMapper.OPERATION;
		
		String output=ServiceMapper.replace(testMe,cvalue);
		//System.out.println("output="+output);
		resultField.setText(output);

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		processTest();
	}



	@Override
	public void keyTyped(KeyEvent arg0) {
		// process the test string
		

	}

}
