package serviceAnalysisUI;

import javax.swing.*;
import serviceAnalysisModel.VolumetricModel;
import serviceAnalysisModel.Workstream;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ModelForm implements ActionListener
{

	private WorkstreamTable wsContentPane;

	public void createGUI()
	{
		// Create and set up the window.
		MainForm.MF = this;
		mainFrame = new JFrame("Volumetric Model");
		if(MainForm.image!=null)
			mainFrame.setIconImage(MainForm.image);
		mainFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		mainFrame.addWindowListener(MainForm.wl);

		GridLayout gl = new GridLayout(2, 0, 10, 10);

		// Container cp=mainFrame.getContentPane();

		// cp.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		// Create and set up the content pane.

		JPanel padding = new JPanel();
		JPanel buttonPanel = new JPanel();
		new BoxLayout(buttonPanel, BoxLayout.Y_AXIS);

		GridBagConstraints cCalculate = new GridBagConstraints();
		GridBagConstraints cVT = new GridBagConstraints();
		GridBagConstraints cWS = new GridBagConstraints();

		cCalculate.gridx = 2;
		cCalculate.gridy = 0;
		cVT.gridy = 1;
		cWS.gridy = 2;
		cVT.gridwidth = 3;
		cWS.gridwidth = 3;
		cVT.weighty = 1;
		cWS.weighty = 1;
		cVT.weightx = 1;
		cWS.weightx = 1;
		cVT.fill = GridBagConstraints.BOTH;
		cWS.fill = GridBagConstraints.BOTH;
		cCalculate.anchor = GridBagConstraints.PAGE_START;

		JButton recalc = new JButton("Calculate");
		recalc.setActionCommand("calculate");
		recalc.setMaximumSize(new Dimension(50, 20));
		recalc.addActionListener(this);
		// buttonPanel.add(recalc);
		// buttonPanel.add(padding);

		newContentPane = new VolumetricTable(SO);
		// newContentPane.setOpaque(true); //content panes must be opaque
		JScrollPane VTscrollPane = new JScrollPane(newContentPane);

		VTscrollPane.setMinimumSize(new Dimension(450, 200));

		wsContentPane = new WorkstreamTable(ws);

		JScrollPane WSscrollPane = new JScrollPane(wsContentPane);

		WSscrollPane.setMinimumSize(new Dimension(450, 200));

		newContentPane.wst = wsContentPane;
		// wsContentPane.setOpaque(true);

		// JPanel p2=new JPanel(gl);
		JPanel p = new JPanel(new GridBagLayout());
		// JPanel p=new JPanel();

		// new BoxLayout(p,BoxLayout.Y_AXIS);

		p.add(recalc, cCalculate);
		p.add(VTscrollPane, cVT);
		p.add(WSscrollPane, cWS);

		p.setOpaque(true);
		newContentPane.setOpaque(true);
		wsContentPane.setOpaque(true);
		// JDesktopPane dtopFrame=new JDesktopPane();
		// p.add(buttonPanel);
		// p2.add(newContentPane);
		// p2.add(wsContentPane);
		// p.add(p2);
		// ((JComponent) cp).setOpaque(true);

		mainFrame.setContentPane(p);
		mainFrame.doLayout();

		// frame.setContentPane(dtopFrame);

		/*
		 * JMenu file = new JMenu("File"); file.setMnemonic('F');
		 * file.add(createMenuItem("Load Model", this, "LoadModel", 'L',
		 * KeyEvent.VK_L)); file.add(createMenuItem("Save Model", this,
		 * "SaveModel", 'S', KeyEvent.VK_S)); file.addSeparator();
		 * file.add(createMenuItem("Reset Timings", this, "ResetTimings", 'R',
		 * KeyEvent.VK_R)); file.add(createMenuItem("Load UI Timings", this,
		 * "LoadUITimings", 'U', KeyEvent.VK_U));
		 * file.add(createMenuItem("Load Tibco Timings", this, "LoadBWTimings",
		 * 'T', KeyEvent.VK_T)); file.add(createMenuItem("Load DB Timings",
		 * this, "LoadDBTimings", 'D', KeyEvent.VK_D));
		 * 
		 * JMenu filter = new JMenu("Filter"); filter.setMnemonic('F'); JMenu
		 * layer = new JMenu("Layer Filter"); layer.setMnemonic('L');
		 * layer.add(createMenuItem("All", this, "FilterLayer_ALL", 'A',
		 * KeyEvent.VK_A)); layer.add(createMenuItem("UI Only", this,
		 * "FilterLayer_UI", 'U', KeyEvent.VK_U));
		 * layer.add(createMenuItem("Tibco Only", this, "FilterLayer_BW", 'T',
		 * KeyEvent.VK_T)); layer.add(createMenuItem("DB Only", this,
		 * "FilterLayer_DB", 'D', KeyEvent.VK_D));
		 * layer.add(createMenuItem("Not Modelled", this, "FilterNotModelled",
		 * 'N', KeyEvent.VK_N)); JMenuItem service =
		 * createMenuItem("Service Filter", this, "FilterService", 'S',
		 * KeyEvent.VK_S);
		 * 
		 * filter.add(layer); filter.add(service);
		 * 
		 * JMenuBar menu = new JMenuBar(); menu.add(file); menu.add(filter);
		 * 
		 * mainFrame.setJMenuBar(menu);
		 */
		// JInternalFrame iFrame=new JInternalFrame();
		// iFrame.setContentPane(newContentPane);

		// Display the window.
		// iFrame.pack();
		// iFrame.setVisible(true);

		mainFrame.setBounds(0, 0, 600, 500);
		// iFrame.setBounds(5, 5, 700, 700);
		// dtopFrame.setBounds(10, 10, 800, 800);
		// dtopFrame.add(iFrame);

		mainFrame.pack();
		// frame.setVisible(true);
	}

	public void clearModel()
	{
		System.out.println("clearing Model Form");

		newContentPane.clearModel();
		newContentPane.model.clear();
		wsContentPane.clear();
		ws.clear();
		SO.clear();
		_VM.clear();
	}

	public void toggleWindow()
	{
		System.out.println("making Model window " + MainForm.togVol);
		if (MainForm.togVol)
			mainFrame.setVisible(true);
		else
			mainFrame.setVisible(false);
	}

	public void calculate()
	{
		SO.simulate();
		SO.simulateSave();
		newContentPane.model.fireTableDataChanged();
		wsContentPane.getModel().fireTableDataChanged();
	}

	public void actionPerformed(ActionEvent event)
	{
		// JMenuItem item = (JMenuItem)event.getSource();
		String cmd = event.getActionCommand();

		if (cmd.equals("calculate"))
		{
			System.out.println("calculate clicked");
			SO.simulate();
			// SO.simulateSave();
			newContentPane.model.fireTableDataChanged();
			wsContentPane.getModel().fireTableDataChanged();

		}
		/*
		 * if (cmd.startsWith("FilterLayer_")) { layerFilter =
		 * cmd.substring(12); setFilter(layerFilter,serviceFilter,true); } else
		 * if (cmd.equals("FilterService")) { serviceFilter =
		 * JOptionPane.showInputDialog(mainFrame,"Enter Service Prefix...",
		 * serviceFilter); setFilter(layerFilter,serviceFilter,true); } else if
		 * (cmd.equals("FilterNotModelled")) { setFilter("ALL","",false); }
		 */
	}

	public JMenuItem createMenuItem(String label, ActionListener listener,
			String command, int mnemonic, int accKey)
	{
		JMenuItem item = new JMenuItem(label);
		item.addActionListener(listener);
		item.setActionCommand(command);
		if (mnemonic != 0)
			item.setMnemonic(mnemonic);
		if (accKey != 0)
			item.setAccelerator(KeyStroke.getKeyStroke(accKey,
					java.awt.Event.CTRL_MASK));
		return item;
	}

	public void setFilter(String layer, String service, boolean modelled)
	{
		System.out.println("Filter (" + layer + "," + service + "," + modelled
				+ ")");
		newContentPane.setFilter(layer, service, modelled);
	}

	public void ShowUI()
	{
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				mainFrame.setVisible(true);
				// createAndShowGUI();
			}
		});
	}

	public void SDPaint()
	{

	}

	public ModelForm(VolumetricModel so)
	{
		super();
		SO = so;
		_VM = SO;
		ws = new ArrayList<Workstream>();
	}

	public void addSDView(ServiceDependencyView SD)
	{
		sd = SD;
		newContentPane.addSDView(SD);
	}

	public void setVM(VolumetricModel vm)
	{
		_VM = vm;
		SO = vm;
	}

	private List<Workstream> ws = null;
	protected JFrame mainFrame = null;
	private VolumetricModel SO;
	private ServiceDependencyView sd = null;
	public VolumetricTable newContentPane = null;

	public static VolumetricModel _VM;
}
