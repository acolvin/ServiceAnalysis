package serviceAnalysisUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.readersWriters.*;

public class RealtimeGraph extends JFrame implements ActionListener,
		WindowListener, BWStatsEventListener
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7512098759615697478L;

	/** The time series data. */
	private TimeSeries series, series2, series3, series4, series5;

	/** The most recent value added. */
	// private double lastValue = 0.0;
	// private double lastWait = 0.0;

	private Service service;
	private BWReader bwReader;

	private double runningSum = 0;
	private int count = 0;

	private TimeSeries tickSeries;

	public RealtimeGraph(Service service, BWReader bwReader)
	{
		super();
		this.service = service;
		this.bwReader = bwReader;
		resetLocations();
		buildUI();
	}

	private boolean showTitle=false; //turn on/off graph title
	
	public RealtimeGraph(Service service, BWReader bwReader,boolean showTitle)
	{
		super();
		this.service = service;
		this.bwReader = bwReader;
		this.showTitle=showTitle;
		resetLocations();
		buildUI();
	}
	XYLineAndShapeRenderer tickRenderer=new XYLineAndShapeRenderer();
	
	private JComboBox<String> locationPicker=new JComboBox<String>(new String[] {""}); 
	
	private void buildUI()
	{
		if(MainForm.image!=null)
			setIconImage(MainForm.image);
		String title = "Real Time Monitoring of " + service.getSName();
		this.setTitle(title);

		series = new TimeSeries("Service Time");
		final TimeSeriesCollection dataset = new TimeSeriesCollection(
				this.series);
		final JFreeChart chart = createChart(dataset);
		if(!showTitle)chart.setTitle(""); //turn off the title

		this.series2 = new TimeSeries("Average Time");
		final TimeSeriesCollection dataset2 = new TimeSeriesCollection(
				this.series2);

		this.series3 = new TimeSeries("Running Average");
		final TimeSeriesCollection dataset3 = new TimeSeriesCollection(
				this.series3);

		this.series4 = new TimeSeries("Service Wait Times");
		final TimeSeriesCollection dataset4 = new TimeSeriesCollection(
				this.series4);
		
		this.tickSeries = new TimeSeries("Tick Tock");
		final TimeSeriesCollection tickDataSet = new TimeSeriesCollection(
				this.tickSeries);

		int maxRange = timeRange + timeRange / 5;
		this.series.setMaximumItemAge(maxRange); // adding these causes index
													// out of bounds from
													// jfreechart
		this.series2.setMaximumItemAge(maxRange);// trying to run from
													// dispatcher thread causes
													// lockup
		this.series3.setMaximumItemAge(maxRange);// running async from
													// dispatcher ok but graph
													// drawn with horizontal
													// lines
		this.series4.setMaximumItemAge(maxRange);
		this.tickSeries.setMaximumItemAge(maxRange);

		XYPlot plot = chart.getXYPlot();
		
		

		plot.setDataset(1, dataset2);
		plot.setDataset(2, dataset3);
		plot.setDataset(3, dataset4);
		plot.setDataset(5, tickDataSet);
		// NumberAxis rangeAxis2 = new NumberAxis("Range Axis 2");
		// rangeAxis2.setAutoRangeIncludesZero(false);

		XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer();
		XYLineAndShapeRenderer renderer2 = new XYLineAndShapeRenderer();
		XYLineAndShapeRenderer renderer3 = new XYLineAndShapeRenderer();
		
		tickRenderer.setSeriesPaint(0, Color.white);
		tickRenderer.setSeriesShapesVisible(0, false);
		tickRenderer.setSeriesVisible(0, tickon);
		tickRenderer.setBaseItemLabelsVisible(false);
		tickRenderer.setBaseSeriesVisibleInLegend(false);
		tickRenderer.setBaseShapesVisible(false);
		tickRenderer.setSeriesItemLabelsVisible(0, false);
		tickRenderer.setSeriesVisibleInLegend(0,false);
		tickRenderer.setSeriesLinesVisible(0, false);
		
		
		renderer1.setSeriesPaint(0, Color.blue);
		renderer2.setSeriesPaint(0, Color.green);
		renderer3.setSeriesPaint(0, Color.yellow);
		renderer3.setSeriesShapesVisible(0, false);

		// DefaultXYItemRenderer dxy1=new DefaultXYItemRenderer();
		// dxy1.setSeriesPaint(1, Color.black);
		// DefaultXYItemRenderer dxy2=new DefaultXYItemRenderer();
		// dxy2.setSeriesPaint(1, Color.yellow);
		// DefaultXYItemRenderer dxy3=new DefaultXYItemRenderer();
		// dxy3.setSeriesPaint(1, Color.ORANGE);
		// dxy3.setSeriesShapesVisible(1, false);
		// plot.setRenderer(1, new DefaultXYItemRenderer());
		// plot.setSecondaryRangeAxis(0, rangeAxis2);
		// plot.mapSecondaryDatasetToRangeAxis(0, new Integer(0));

		// DefaultXYItemRenderer dxy=new DefaultXYItemRenderer();
		// dxy.setSeriesPaint(1, Color.yellow);

		// DefaultXYItemRenderer dxy4=new DefaultXYItemRenderer();
		// dxy.setSeriesPaint(1, Color.ORANGE);

		plot.setRenderer(1, renderer1);
		plot.setRenderer(2, renderer2);
		plot.setRenderer(3, renderer3);
		plot.setRenderer(5, tickRenderer);
		
		

		NumberAxis axis2 = new NumberAxis("Rate (tps)");
		plot.setRangeAxis(1, axis2);
		XYLineAndShapeRenderer renderer4 = new XYLineAndShapeRenderer();
		renderer4.setSeriesPaint(0, Color.black);
		renderer4.setSeriesShapesVisible(0, false);
		plot.setRenderer(4, renderer4);
		this.series5 = new TimeSeries("Call Rate");
		final TimeSeriesCollection dataset5 = new TimeSeriesCollection(
				this.series5);
		this.series5.setMaximumItemAge(maxRange);
		plot.setDataset(4, dataset5);
		plot.mapDatasetToRangeAxis(4, 1);

		final ChartPanel chartPanel = new ChartPanel(chart);
		final JPanel content = new JPanel(new BorderLayout());
		content.add(chartPanel,BorderLayout.CENTER);
		
		ticking.setActionCommand("tick");
		ticking.addActionListener(this);
		JButton resetButton=new JButton("Reset");
		
		resetButton.setActionCommand("reset");
		resetButton.addActionListener(this);
		if(timeRange<10000)timeRange=10000;
		threadSpinner = new JSpinner(new SpinnerNumberModel(timeRange/1000,10,7200,10));
		JPanel jpp=new JPanel();
		jpp.setLayout(new BoxLayout(jpp,BoxLayout.LINE_AXIS));
		
		
		
		jpp.add(ticking);
		jpp.add(Box.createHorizontalGlue());
		jpp.add(new JLabel("range(secs)"));
		jpp.add(threadSpinner);
		jpp.add(resetButton);
		jpp.add(locationPicker);
		locationPicker.setActionCommand("location");
		locationPicker.addActionListener(this);
		
		
		
		
		content.add(jpp,BorderLayout.SOUTH);
		this.setBounds(x, y, width, height);
		// chartPanel.setPreferredSize(new java.awt.Dimension(width, height));
		
		setContentPane(content);
		this.setSize(width, height);
		// Register with the reader
		// bwReader.addListener(service,this);

		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		this.addWindowListener(this);
		// pack();
		// setVisible(true);

		for (int i = 0; i < 100; i++)
			hundredval[i] = 0;

	}
	private boolean tickon=false;
	private JCheckBox ticking=new JCheckBox("Active Axis",tickon);
	

	public boolean isTitleShown()
	{
		return showTitle;
	}
	
	private JSpinner threadSpinner;
	private int x = 0, y = 0, width = 600, height = 250, timeRange = 60000;

	public RealtimeGraph(Service service, BWReader bwReader, int x, int y,
			int w, int h, int timeRange,boolean showTitle)
	{
		super();
		this.service = service;
		this.bwReader = bwReader;
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;
		this.timeRange = timeRange;
		this.showTitle=showTitle;
		resetLocations();
		buildUI();

	}

	// public int getWidth()
	// {
	// return this.getContentPane().getWidth();
	// }

	// public int getHeight()
	// {
	// return this.getContentPane().getHeight();
	// }

	public int getTimeRange()
	{
		return timeRange;
	}

	public Service getService()
	{
		return service;
	}

	@Override
	public void windowActivated(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e)
	{
		// TODO Auto-generated method stub
		// System.out.println("window is closed");
		bwReader.removeListener(service, b);
		f.setEnabled(false);
		tickstarted=false;

	}

	@Override
	public void windowClosing(WindowEvent e)
	{
		tickstarted=false;
		if (RTGraphs != null)
		{
			
			RTGraphs.remove(this);
		}
		
		//tracking rate meter on or off
		boolean b=false;
		for(RealtimeGraph rtg:RTGraphs){
			if(rtg.service.equals(service) && rtg!=this){
				b=true;
				break;
			}
		}
		if(!b) service.setTrackLocationRates(false);//turn off tracking if not used anymore

	}

	@Override
	public void windowDeactivated(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	/**
	 * Creates a sample chart.
	 * 
	 * @param dataset
	 *            the dataset.
	 * 
	 * @return A sample chart.
	 */
	private JFreeChart createChart(final XYDataset dataset)
	{
		final JFreeChart result = ChartFactory.createTimeSeriesChart(
				service.getSName(), "Time", "Response (ms)", dataset, true,
				true, false);
		XYPlot plot = result.getXYPlot();
		xaxis = plot.getDomainAxis();
		xaxis.setAutoRange(true);
		xaxis.setFixedAutoRange(timeRange); // 60 seconds
		// axis.setFixedAutoRange(300000.0); // 5 mins
		
		ValueAxis axis = plot.getRangeAxis();
		axis.setAutoRange(true);
		axis.setAutoRangeMinimumSize(50);
		// axis.setRange(0.0, 1000.0);
		return result;
	}

	private ValueAxis xaxis;
	private GraphRunnable updater = new GraphRunnable();
	private long start = (new Date()).getTime();

	public class GraphRunnable implements Runnable
	{
		// public double iresponse,iwait,iav,irun,irate;
		// public Millisecond inow;

		class GraphPoint
		{
			public double iresponse, iwait, iav, irun, irate;
			public Millisecond inow;

			public GraphPoint(Millisecond now, double response, double wait,
					double avg, double runavg, double rate)
			{
				inow = now;
				iresponse = response;
				iwait = wait;
				iav = avg;
				irun = runavg;
				irate = rate;

			}
		}
		
		

		private ArrayList<GraphPoint> points = new ArrayList<GraphPoint>();

		public void addPoint(Millisecond now, double response, double wait,
				double avg, double runavg, double rate)
		{
			// if(points.size()<100)
			points.add(new GraphPoint(now, response, wait, avg, runavg, rate));
		}

		// public void setiresponse(double r) { iresponse=r;}
		public void run()
		{
			if(pickerUpdate){
				pickerUpdate=false;
				locationPicker.setModel(new DefaultComboBoxModel<String>(locations.toArray(new String[]{})));
			}
			RealtimeGraph.this.series.setNotify(false);
			RealtimeGraph.this.series2.setNotify(false);
			RealtimeGraph.this.series3.setNotify(false);
			RealtimeGraph.this.series4.setNotify(false);
			RealtimeGraph.this.series5.setNotify(false);

			for (GraphPoint gp : points)
			{
				Millisecond end=new Millisecond(new Date(gp.inow.getFirstMillisecond()+(long)gp.iresponse));
				RealtimeGraph.this.series.addOrUpdate(end, gp.iresponse);
				RealtimeGraph.this.series2.addOrUpdate(end, gp.iav);
				RealtimeGraph.this.series3.addOrUpdate(end, gp.irun);
				RealtimeGraph.this.series4.addOrUpdate(end, gp.iwait);
				
				RealtimeGraph.this.series5.addOrUpdate(end, gp.irate);
			}
			RealtimeGraph.this.series.setNotify(true);
			RealtimeGraph.this.series2.setNotify(true);
			RealtimeGraph.this.series3.setNotify(true);
			RealtimeGraph.this.series4.setNotify(true);
			RealtimeGraph.this.series5.setNotify(true);
		}
	}

	/*
	 * original single point version public class GraphRunnable implements
	 * Runnable { public double iresponse,iwait,iav,irun,irate; public
	 * Millisecond inow;
	 * 
	 * 
	 * 
	 * public void setiresponse(double r) { iresponse=r;} public void run() {
	 * int count2; RealtimeGraph.this.series.setNotify(false);
	 * RealtimeGraph.this.series2.setNotify(false);
	 * RealtimeGraph.this.series3.setNotify(false);
	 * RealtimeGraph.this.series4.setNotify(false);
	 * RealtimeGraph.this.series5.setNotify(false);
	 * 
	 * RealtimeGraph.this.series.addOrUpdate(inow, iresponse);
	 * RealtimeGraph.this.series2.addOrUpdate(inow, iav);
	 * 
	 * //if(++count>100)count2=100 ; else count2 = count;
	 * //System.out.println(count2);
	 * RealtimeGraph.this.series3.addOrUpdate(inow, irun);
	 * RealtimeGraph.this.series4.addOrUpdate(inow, iwait);
	 * RealtimeGraph.this.series5.addOrUpdate(inow, irate);
	 * 
	 * // RealtimeGraph.this.series.addOrUpdate(inow, lastValue);
	 * RealtimeGraph.this.series.setNotify(true);
	 * RealtimeGraph.this.series2.setNotify(true);
	 * RealtimeGraph.this.series3.setNotify(true);
	 * RealtimeGraph.this.series4.setNotify(true);
	 * RealtimeGraph.this.series5.setNotify(true); } }
	 */

	private JFrame f = this;
	private BWStatsEventListener b = this;

	public void ShowUI()
	{
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				bwReader.addListener(service, b);
				f.setVisible(true);
				// createAndShowGUI();
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		// TODO Auto-generated method stub
		System.out.println(e.getActionCommand());
		if(e.getActionCommand().equals("reset")) reset();
		if(e.getActionCommand().equals("tick"))processTick();
		if(e.getActionCommand().equals("location"))locationPicked();
		
	}
	
	private void processTick()
	{
		System.out.println("processTick clicked");
		tickon=ticking.isSelected();
		tickRenderer.setSeriesVisible(0, tickon);
		if(tickon)
		{
			ticktock=new GraphTimer();
			ticktock.start();
			
		}
		else
		{
			tickstarted=false;
		}
		
	}

	private double[] hundredval = new double[100];

	// private int count2=0;

	public synchronized void reset()
	{
		count=0;
		runningSum=0;

		for (int i = 0; i < 100; i++)
			hundredval[i] = 0;
		
		series.clear();
		series2.clear();
		series3.clear();
		series4.clear();
		series5.clear();
		tickSeries.clear();
		
		timeRange=(Integer)threadSpinner.getValue()*1000;
		int maxRange=timeRange+timeRange/5;
		this.series.setMaximumItemAge(maxRange);
		this.series2.setMaximumItemAge(maxRange);
		this.series3.setMaximumItemAge(maxRange);
		this.series4.setMaximumItemAge(maxRange);
		this.series5.setMaximumItemAge(maxRange);
		this.tickSeries.setMaximumItemAge(maxRange);
		xaxis.setFixedAutoRange(timeRange);
		
		graphTick=-1;
		start-=1000; //force a draw on next point
		delayPlots=timeRange/1000*5/6;
		if(delayPlots<50)delayPlots=50;
		if(delayPlots>28000)delayPlots=28000;
	}
	
	HashSet<String> locations=new HashSet<String>(); 
	
	private String locationFilter="";
	
	private synchronized void resetLocations(){
		locations.clear();
		locations.add("");
		//moved to swap points to ensure it runs on dispatcher thread
		pickerUpdate=true;
		//locationPicker.setModel(new DefaultComboBoxModel<String>(locations.toArray(new String[]{})));
		locationFilter="";
		
	}
	
	private synchronized void addLocation(String location){
		if(location==null)return;
		if(locations.add(location)){
			//we added a new location moved to swap points to ensure it runs on dispatcher thread
			//System.out.println("location "+location+" added");
			pickerUpdate=true;
			//locationPicker.setModel(new DefaultComboBoxModel<String>(locations.toArray(new String[]{})));
		}
		else return;
	}
	
	private void locationPicked(){
		locationFilter=(String) locationPicker.getSelectedItem();
		if(locationPicker.getSelectedIndex()!=0)service.setTrackLocationRates(true);
		else{
			boolean b=false;
			for(RealtimeGraph rtg:RTGraphs){
				if(rtg.service.equals(service) && rtg!=this){
					b=true;
					break;
				}
			}
			if(!b) service.setTrackLocationRates(false);//turn off tracking if not used anymore
		}
	}
	
	private boolean pickerUpdate=false;
	double average=0;
	@Override
	public synchronized void BWServiceOperationStat(Service s, Date time, double response,
			double average, double wait, double rate,String location)
	{
		
		addLocation(location);
		
		if(locationFilter.length()>0 && !locationFilter.equals(location))return;
		// TODO Auto-generated method stub
		Millisecond now = new Millisecond(time);
		this.average=average;
		double av = average;
		// System.out.println(service.getSName()+" time record received");
		// lastValue=response;
		// lastWait=wait;
		// this.series.setNotify(false);

		// GraphRunnable updater=new GraphRunnable();
		// updater.setiresponse(response);
		// updater.inow=now;
		// updater.iav=av;
		// updater.iwait=wait;
		// updater.irate=rate;

		int c = count % 100;
		runningSum += response;
		runningSum -= hundredval[c];
		updater.addPoint(now, response, wait, av,
				runningSum / (double) Math.min(100, count + 1), rate);
		// updater.irun=runningSum/(double)Math.min(100,count+1);
		hundredval[c] = response;
		count++;
		// System.out.println(now.getMillisecond()-updater.points.get(0).inow.getMillisecond());
		if ((new Date()).getTime() - start > delayPlots) // only draw points every 10
													// milliseconds. otherwise
													// we overload the swing
													// dispatcher
		{
			//if(!tickstarted)ticktock.start();
			plotPoints();
		}

		// try {
		// SwingUtilities.invokeAndWait(updater); //invoke and wait causes a
		// deadlock
		// SwingUtilities.invokeLater(updater); //invokelater causes wierd
		// drawing

		// } catch (java.lang.IndexOutOfBoundsException e) {
		// TODO Auto-generated catch block
		// e.printStackTrace();
		// }

	}
	
	@Override
	public void BWServiceOperationStat(Service s, Date time, double response, double average, double wait,
			HashMap<String, Double> rates, String location) {
		
		addLocation(location);
		
		if(locationFilter.length()>0 && !locationFilter.equals(location))return;
		// TODO Auto-generated method stub
		Millisecond now = new Millisecond(time);
		this.average=average;
		double av = average;
		int c = count % 100;
		runningSum += response;
		runningSum -= hundredval[c];
		Double rate=rates.get(locationFilter);
		if(rate==null)rate=rates.get("");
		if(rate==null)rate=0d;
		updater.addPoint(now, response, wait, av,
				runningSum / (double) Math.min(100, count + 1), rate);
		hundredval[c] = response;
		count++;
		// System.out.println(now.getMillisecond()-updater.points.get(0).inow.getMillisecond());
		if ((new Date()).getTime() - start > delayPlots) // only draw points every 10
													// milliseconds. otherwise
													// we overload the swing
													// dispatcher
		{
			//if(!tickstarted)ticktock.start();
			plotPoints();
		}

	}
	
	private long graphTick=-1;
	
	public void moveOneSec()
	{
		//if((System.currentTimeMillis()-graphTick)>500)
		try{
			//graphTick=System.currentTimeMillis();
			
			//tickSeries.clear();
			//move graph on here
			javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                	Millisecond end=new Millisecond(new Date());
                	tickSeries.addOrUpdate(end,average);
                }
            });
		}catch (Exception e)
		{
			System.out.println("Exception caught"+ e.getMessage());
		}
	}
	
	public void sync()
	{
		if((System.currentTimeMillis()-start)>30000) 
		{	
			plotPoints();
			//System.out.println("flushing graph points");
		}
	}
	
	private int delayPlots=50;

	private ArrayList<RealtimeGraph> RTGraphs = null;

	public void setRTGraphs(ArrayList<RealtimeGraph> rtg)
	{
		RTGraphs = rtg;
	}

	private synchronized GraphRunnable swapPoints()
	{ //this always run on the swing dispatcher thread
		

		
		GraphRunnable g2 = updater;
		updater = new GraphRunnable();
		start = (new Date()).getTime();
		return g2;
	}

	private void plotPoints()
	{
		GraphRunnable g2 = swapPoints();
		try
		{
			SwingUtilities.invokeLater(g2); // invokelater causes wierd drawing
		} catch (java.lang.IndexOutOfBoundsException e)
		{
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}
	
	private GraphTimer ticktock=new GraphTimer();
	private boolean tickstarted=false;
	
	private class GraphTimer extends Thread
	{
		public GraphTimer()
		{
			super("Graph Timer");
			this.setDaemon(true);
		}
		@Override
		public void run() {
			tickstarted=true;
			while(tickstarted)
			try {
				Thread.sleep(200l);
				RealtimeGraph.this.moveOneSec();
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("tick thread finished");
		}
		
	}



}
