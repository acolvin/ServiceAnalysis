package serviceAnalysisUI;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.table.*;

import serviceAnalysisModel.VolModelListener;
import serviceAnalysisModel.VolumetricModel;
import serviceAnalysisModel.VolumetricType;

public class VolumetricTableModel extends AbstractTableModel implements
		VolModelListener
{

	final int adjColumn = 1;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VolumetricTableModel(VolumetricModel vm)
	{
		VM = vm;
		volumes = VM.model;
		times = VM.simulations;
		percentages = VM.savedSimulations;
		// Iterator<Service> i=SO.iterator();
		// while(i.hasNext()){
		// i.next().addServiceListener(this);
		// }
		Iterator<VolumetricType> it = volumes.keySet().iterator();
		while (it.hasNext())
		{
			VolumetricType v = it.next();
			VTs.add(v);
			if (!times.containsKey(v))
			{
				times.put(v, new Double(0));
			}
		}
		VM.addVolModelListener(this);
	}

	private void updateTypes()
	{
		// VTs.clear();
		// times.clear();
		ArrayList<VolumetricType> lVTs = new ArrayList<VolumetricType>();
		volumes = VM.model;
		times = VM.simulations;
		percentages = VM.savedSimulations;

		Iterator<VolumetricType> it = volumes.keySet().iterator();
		while (it.hasNext())
		{
			VolumetricType v = it.next();
			lVTs.add(v);
			if (!times.containsKey(v))
			{
				times.put(v, new Double(0));
			}
		}
		VTs = lVTs;

	}

	public void clear()
	{
		System.out.println("clearing volumetric table Model ");
		VTs.clear();
		volumes.clear();
		times.clear();
		percentages.clear();
		VM.model.clear();
		VM.simulations.clear();
		VM.savedSimulations.clear();
	}

	public VolumetricType getVolumetricType(int index)
	{
		return VTs.get(index);
	}

	@Override
	public int getColumnCount()
	{
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public int getRowCount()
	{
		// TODO Auto-generated method stub
		return VTs.size();
	}

	public String getColumnName(int col)
	{
		return columnNames[col];
	}

	public Class getColumnClass(int c)
	{
		if (c == 0)
			return "".getClass();
		if (c == 1)
			return Integer.class;
		if (c == 2)
			return Double.class;
		if (c == 3)
			return Double.class;
		return getValueAt(0, c).getClass();
	}

	public void setValueAt(Object value, int row, int col)
	{
		if (col == adjColumn)
		{
			System.out.println("setting value");

			times.put(VTs.get(row), (Double) value);
			// SO.get(row).setAdjustments((Float)value);
			// this.fireTableDataChanged();
			VM.simulate();
			this.fireTableDataChanged();
		}
	}

	public boolean isCellEditable(int row, int col)
	{
		// Note that the data/cell address is constant,
		// no matter where the cell appears onscreen.
		if (col != adjColumn)
		{
			return false;
		} else
		{
			return true;
		}
	}

	@Override
	public synchronized Object getValueAt(int arg0, int arg1)
	{
		// TODO Auto-generated method stub
		// System.out.println(arg0+" "+arg1);
		if (VTs != null && !VTs.isEmpty())
		{
			if (arg1 == 0)
			{
				return VTs.get(arg0).getName();
			}
			if (arg1 == 1)
				if (volumes != null && !volumes.isEmpty())
				{
					VolumetricType vt = VTs.get(arg0);
					return volumes.get(vt);
				} else
				{
					return 0;
				}
			if (arg1 == 2)
				if (times != null && !times.isEmpty())
				{
					VolumetricType vt = VTs.get(arg0);
					return times.get(vt);
				} else
				{
					return 0d;
				}
			if (arg1 == 3)
				if (percentages != null && times != null
						&& !percentages.isEmpty() && !times.isEmpty())
				{
					VolumetricType vt = VTs.get(arg0);

					return new Double((percentages.get(vt) - times.get(vt))
							* 100d / percentages.get(vt));
				} else
				{
					return new Double(0);
				}
		} else
		{
			if (arg1 == 3)
				return new Double(0);
			if (arg1 == 0)
				return "";
			if (arg1 == 1)
				return 0;
			if (arg1 == 2)
				return 0d;
		}
		return "";

	}

	private VolumetricModel VM;
	private Map<VolumetricType, Integer> volumes;
	private Map<VolumetricType, Double> times;
	private Map<VolumetricType, Double> percentages;

	private List<VolumetricType> VTs = new ArrayList<VolumetricType>();
	private String[] columnNames =
	{ "Type", "Volume", "Time", "Percentage Saving" };

	// @Override
	public void handleTimeChange()
	{
		// TODO Auto-generated method stub
		// System.out.println("handle time change called "+s);
		// int i=SO.indexOf(s);
		// this.fireTableCellUpdated(i, adjColumn);
		// this.fireTableCellUpdated(i, adjColumn + 1);
		// this.fireTableCellUpdated(i, adjColumn + 2);
		VM.simulate();
	}

	@Override
	public synchronized void fireModelDataChanged()
	{
		updateTypes();
		this.fireTableDataChanged();

	}
}