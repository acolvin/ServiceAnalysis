package serviceAnalysisUI;

import java.awt.Color;
import java.util.Iterator;
import java.util.Map;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import serviceAnalysisModel.Service;

public class FrequencyChart
{
	private String title;
	private XYSeries series1;
	private XYSeries series2;
	private Map<Double, Double> samples;
	private XYSeriesCollection dataset;
	private Map<Service, Map<Double, Double>> consumersamples = null,
			dependantsamples = null;
	private JFreeChart chart;

	public FrequencyChart(String title, Map<Double, Double> samples,
			Map<Double, Double> cumulative)
	{
		this.title = title;
		this.samples = samples;
		series1 = new XYSeries(title);
		Iterator<Double> i = samples.keySet().iterator();
		Double x, y;
		while (i.hasNext())
		{
			x = i.next();
			y = samples.get(x);
			series1.add(x, y);
		}

		series2 = new XYSeries("Cumulative " + title);
		for (Double d : cumulative.keySet())
		{
			series2.add(d, cumulative.get(d));
		}

		dataset = new XYSeriesCollection();
		dataset.addSeries(series1);
		// dataset.addSeries(series2);
		XYSeriesCollection dataset2 = new XYSeriesCollection();
		dataset2.addSeries(series2);

		chart = ChartFactory.createXYLineChart(title,
		// chart title
				"Response",
				// x axis label
				"Frequency",// y axis label
				dataset,
				// data
				PlotOrientation.VERTICAL, true,
				// include legend
				true,
				// tooltips
				false
		// urls
				);

		// CategoryDataset dataset = createDataset(services);
		// JFreeChart chart = createChart(dataset);

		NumberAxis axis2 = new NumberAxis("Cumulative Frequency");

		XYPlot p = chart.getXYPlot();
		// p.setRangeMinorGridlinesVisible(true);
		XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer();
		p.setRangeAxis(1, axis2);
		p.setBackgroundPaint(Color.white);
		p.setDomainGridlinePaint(Color.BLACK);
		p.setRangeGridlinePaint(Color.BLACK);

		renderer1.setSeriesPaint(0, Color.blue);
		renderer1.setSeriesShapesVisible(0, false);
		p.setRenderer(1, renderer1);
		p.setDataset(1, dataset2);

	}

}
