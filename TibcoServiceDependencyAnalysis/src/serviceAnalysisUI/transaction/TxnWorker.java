package serviceAnalysisUI.transaction;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.List;

import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import serviceAnalysisModel.Transaction.EventCollection;
import serviceAnalysisModel.Transaction.TxnRect;

public class TxnWorker extends SwingWorker<HashMap<String, TxnRect>, Integer> implements PropertyChangeListener
{

	public void setRoot(List<String> txns, TxnPanel panel)
	{
		this.txns = txns;
		txnPanel = panel;
		calculated = false;

	}

	private List<String> txns = null;
	private TxnPanel txnPanel = null;
	private boolean calculated = false;
	private HashMap<String, TxnRect> results = null;
	public HashMap<String, TxnRect> partialResults = null;

	@Override
	protected HashMap<String, TxnRect> doInBackground() throws Exception
	{
		// System.out.println("in do background");
		if (txns == null)
			return null;
		if (!calculated)
			results = EventCollection.getInstance().calculatePositions(txns,this);
		calculated = true;
		// System.out.println("leaving do background");
		return results;
	}
	
	//public void processpercent(List<Integer> is)
	//{
		
	//}
	
	public JProgressBar pb=null;
	
	protected void process(List<Integer> is)
	{
		if(pb!=null) pb.setValue(is.get(is.size()-1));
	}
	
	int currentvalue=0;
	
	public void progressEvent(int progressPercent) {
		// TODO Auto-generated method stub
		if(currentvalue!=progressPercent)
		{
			System.out.println("Progress %: "+progressPercent);
			publish(progressPercent);
			currentvalue=progressPercent;
		}
	}

	@Override
	protected void done()
	{
		try
		{
			txnPanel.ProcessTxns();
		} catch (Exception ignore)
		{
		}
	}

	int updatecount=0;
	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		//System.out.println("received rectangle property change");
		updatecount++;
		if(updatecount>200)
		{
			updatecount=0;
			partialResults=(HashMap<String, TxnRect>) arg0.getNewValue();
			txnPanel.ProcessTxns(true);
		}
	}

}
