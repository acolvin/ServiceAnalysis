package serviceAnalysisUI.transaction;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableRowSorter;



public class TransactionTable extends JPanel implements TableModelListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1553340483522569993L;
	private TxnTableModel model;
	private JTable table;
	private JScrollPane scrollPane;
	//private MapperWindow mapperWindow=null;

	public TransactionTable(TxnTableModel model)
	{
		this.model=model;
		table=new JTable(model);
		model.addTableModelListener(this);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		table.setDragEnabled(false);
		table.getTableHeader().setReorderingAllowed(false);
		table.getColumnModel().getColumn(0).setCellRenderer(new DateCellRenderer());
		table.setRowSorter(new TableRowSorter<TxnTableModel>(model));
	}
	
	public class DateCellRenderer extends DefaultTableCellRenderer
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 4090932661725062412L;

		@Override public void setValue(Object aValue) {
		    Object result = aValue;
		    if ((aValue != null) && (aValue instanceof Date)) {
		      result=((Date)aValue).toString();
		    } 
		    super.setValue(result);
		  }
	}
	
	public void addListSelectionListener(ListSelectionListener lsl)
	{
		table.getSelectionModel().addListSelectionListener(lsl);
	}
	
	
	public void refresh()
	{
		model.fireTableDataChanged();
	}
	
	
	
	public List<String> getSelectedTxns()
	{
		int trows[]=table.getSelectedRows();
		ArrayList<String> txns=new ArrayList<String>();
		for(int i:trows)
		{
			
			txns.add((String) model.getValueAt(table.convertRowIndexToModel(i),1));
		}
		return txns;
		
	}
	public int[] getSelectedRows()
	{

		return table.getSelectedRows();
		
	}	
	
	
	public void createGUI()
	{
		this.setLayout(new BorderLayout());
		scrollPane = new JScrollPane(table);
		add(scrollPane);
		//add(table);
		setVisible(true);
		//scrollPane.setVisible(true);
	}

	@Override
	public void tableChanged(TableModelEvent e) {
		
	}
	
	


}
