package serviceAnalysisUI.transaction;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import ServiceAnalysisClient.SAEvent;

import serviceAnalysisModel.Transaction.EventCollection;

public class TxnTableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8060280895781491863L;
	@Override
	public int getColumnCount() {
		
		return 3;
	}

	List<String> rootTxns=null;
	public void initialiseModel(List<String> rawRootTxns)
	{
		rootTxns=new ArrayList<String>();
		for(String s:rawRootTxns)
		{
			if(s!=null) rootTxns.add(s);
		}
		
		size=rootTxns.size();
		
		this.fireTableDataChanged();
		
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return size;
	}
	private int size=0;

	@Override
	public Object getValueAt(int row, int col) {
		if(row<0 || row>=size || rootTxns==null) 
		{
			if(col==0)return (null);
			if(col==1)return null;
			if(col==2) return null;
		}
		
		//a real row
		SAEvent event = EventCollection.getInstance().getSAEvent(rootTxns.get(row));
		if(col==0) {
			Date start;
			if (event.startTime == null)
			{
				// System.out.println("Found a shadow");
				Long l = EventCollection.getInstance()
						.getRangeStartTime(event.txn);
				if (l == null)
					l = 0L;
				start = new Date(l);
			} else
				start = event.startTime;	
			
			return start;
		}
		if(col==1) return event.txn;
		if(col==2) return event.processTime;
		return null;
		
	}
	
	public String getColumnName(int col)
	{
		return columnNames[col];
	}

	private String[] columnNames =
		{"Date/Time","Transaction", "Duration"};
	
	public Class getColumnClass(int c)
	{
		if(c==0)return Date.class;
		if(c==1)return String.class;
		return Integer.class;
	}
	
	//public boolean isCellEditable(int row,int column)
	//{
	//	if(column>0)return true;
	//	else return false;
	//}
}
