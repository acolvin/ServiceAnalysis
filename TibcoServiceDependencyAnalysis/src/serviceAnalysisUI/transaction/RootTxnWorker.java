package serviceAnalysisUI.transaction;

import java.util.List;

import javax.swing.JList;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import database.TransactionProgressListener;

import serviceAnalysisModel.Transaction.EventCollection;




//depricated not used
public class RootTxnWorker extends SwingWorker<List<String>, Integer> implements TransactionProgressListener
{

	public void setFilter(String location,String file,String function)
	{
		this.location=location;
		this.file=file;
		this.function=function;
	}
	
	private String location,file,function;
	private JList list=null;
	private TransactionView view=null;
	
	private long start;
	public void addProgressBar(JProgressBar sb,JList list,TransactionView view)
	{
		this.sb=sb;
		sb.setValue(0);
		this.list=list;
		this.view=view;
		//sb.showProgress();
	}
	private JProgressBar sb=null;

	


	@Override
	protected List<String> doInBackground()
	{
		
		
		start=System.currentTimeMillis();
		return EventCollection.getInstance().getFilteredRootTxns(location, file, function);
		
	}
	
	protected void process(List<Integer> ps)
	{
		if(sb!=null) sb.setValue(ps.get(ps.size()-1));
		//System.out.println("process: ");
	}

	protected void done()
	{
		
		if(sb!=null)
		{
			sb.setValue(100);
			view.reloadTxnList();
		}
		
	}
	
	private int currentvalue=-1;
	@Override
	public void progressEvent(int progressPercent) {
		// TODO Auto-generated method stub
		if(currentvalue!=progressPercent)
		{
			System.out.println("Progress %: "+progressPercent);
			publish(progressPercent);
			currentvalue=progressPercent;
		}
	}



}
