package serviceAnalysisUI.transaction;

import javax.swing.*;
import javax.swing.event.*;
import org.jdesktop.swingx.JXMultiSplitPane;
import org.jdesktop.swingx.MultiSplitLayout;

import ServiceAnalysisClient.SAEvent;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.Transaction.EventCollection;
import serviceAnalysisUI.MainForm;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TransactionView implements ActionListener, ListSelectionListener
{

	public TransactionView()
	{
		super();
		// String
		// layoutDef="(COLUMN (ROW tickbox slider) (ROW (LEAF name=main weight=1.0) (COLUMN weight=0.0 (LEAF name=consumer weight=0.5) (LEAF name=dependant weight=0.5) (LEAF name=plot weight=0.0)) ))";
		String layoutDef = "(COLUMN (ROW (LEAF name=tickbox weight=0.1) (LEAF name=location weight=0.3) (LEAF name=file weight=0.3) (LEAF name=service weight=0.3)) (ROW (LEAF name=main weight=0.8) (LEAF name=consumer weight=0.2) ))";

		MultiSplitLayout.Node modelRoot = MultiSplitLayout
				.parseModel(layoutDef);
		// JXMultiSplitPane msp=new JXMultiSplitPane();
		msp.getMultiSplitLayout().setModel(modelRoot);
	}

	JXMultiSplitPane msp = new JXMultiSplitPane();
	
	private String currentFile="";
	private String currentLocation="";
	private String currentFunction="";
	
	private TxnTableModel model=new TxnTableModel();

	protected void reloadTxnList()
	{
		System.out.println("reload txn");
		DefaultListModel newdlm = new DefaultListModel();
		//dlm.clear();
/*		try {
			rootTxns = rtw.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
*/		
		rootTxns=EventCollection.getInstance().getFilteredRootTxns(currentLocation, currentFile,currentFunction);
		System.out.println("got root txns");
		model.initialiseModel(rootTxns);
		for (String ss : rootTxns)
		{
			SAEvent ev = EventCollection.getInstance().getSAEvent(ss);
			Date start;
			if (ev.startTime == null)
			{
				// System.out.println("Found a shadow");
				Long l = EventCollection.getInstance()
						.getRangeStartTime(ev.txn);
				if (l == null)
					l = 0L;
				start = new Date(l);
			} else
				start = ev.startTime;
			String s = start.toString() + "::-" + ss;
			newdlm.addElement(s);
			//System.out.println(ss); 
		}
		txns.setModel(newdlm);
		dlm=newdlm;
		System.out.println("completed reload");
	}

	private List<String> rootTxns;
	private DefaultListModel dlm = new DefaultListModel();
	private JList txns = new JList();
	private JProgressBar progress = new JProgressBar(0,100);
	
	public JProgressBar getProgressBar()
	{
		return progress;
	}

	private TransactionTable txnPane;
	private RootTxnWorker rtw=null;
	public void createGUI()
	{
		
		//progress.setOrientation(JProgressBar.VERTICAL);
		// Create and set up the window.
		frame = new JFrame("Transactions");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		if(MainForm.image!=null) frame.setIconImage(MainForm.image);
		
		frame.addWindowListener(MainForm.wl);

		txns.setModel(dlm);
		txns.addListSelectionListener(this);
		
		//rtw=new RootTxnWorker();
		//EventCollection.getInstance().addRootFilter(rtw);
		//rtw.addProgressBar(progress, txns,this);
		//rtw.setFilter(currentLocation, currentFile, currentFunction);
		//rtw.doInBackground();
		
		reloadTxnList();
		
		
		
		newContentPane = new TxnPanel();
		newContentPane.owner=this;
		txnPane=new TransactionTable(model);
		txnPane.createGUI();
		txnPane.addListSelectionListener(this);
		JScrollPane listpane = new JScrollPane(txns);
		JScrollPane graphPane = new JScrollPane(newContentPane);

		JButton deps = new JButton("Create Dependencies");
		deps.setActionCommand("Dep");
		deps.addActionListener(this);
		
		JPanel depsPanel=new JPanel(new BorderLayout());
		depsPanel.add(deps,BorderLayout.CENTER);
		depsPanel.add(progress,BorderLayout.SOUTH);
		
		filterPanel=new JPanel(new BorderLayout());
		locationList=new JComboBox(EventCollection.getInstance().getLocations().toArray());
		fileList=new JComboBox(EventCollection.getInstance().getFilenames().toArray());
		fileList.addActionListener(this);
		fileList.setActionCommand("fileAction");
		locationList.addActionListener(this);
		locationList.setActionCommand("locationAction");
		
		functionList=new JComboBox(EventCollection.getInstance().getOperations().toArray());
		functionList.addActionListener(this);
		functionList.setActionCommand("functionAction");
		filterPanel.add(locationList,BorderLayout.NORTH);
		filterPanel.add(fileList,BorderLayout.CENTER);
		filterPanel.add(functionList,BorderLayout.SOUTH);
		// newContentPane.setRoot(EventCollection.getInstance().getRootTxns());
		// newContentPane.setOpaque(true); //content panes must be opaque

		//msp.add(locationList, "location");
		//msp.add(fileList, "file");
		//msp.add(functionList,"service");
		//msp.add(progress,"location");
		msp.add(filterPanel,"service");
		msp.add(depsPanel, "tickbox");
		msp.add(graphPane, "main");
		msp.add(txnPane, "consumer");
		// JButton b1 = new JButton("Disable middle button");
		// b1.setVerticalTextPosition(AbstractButton.CENTER);
		// b1.setHorizontalTextPosition(AbstractButton.LEADING); //aka LEFT, for
		// left-to-right locales
		// b1.setMnemonic(KeyEvent.VK_D);

		// JMenu file = new JMenu("File");
		// file.setMnemonic('F');
		// file.add(createMenuItem("Save as SVG", this, "SaveSVG", 'S',
		// KeyEvent.VK_S));
		// JMenuBar menu = new JMenuBar();
		// menu.add(file);

		// frame.setJMenuBar(menu);

		// JScrollPane sp=new JScrollPane(newContentPane);
		// JDesktopPane dtopFrame=new JDesktopPane();
		frame.setContentPane(msp);
		// frame.setContentPane(newContentPane);
		// newContentPane.setBounds(frame.getBounds());
		// frame.setContentPane(dtopFrame);

		// JInternalFrame iFrame=new JInternalFrame();
		// iFrame.setContentPane(newContentPane);

		// Display the window.
		// iFrame.pack();
		// iFrame.setVisible(true);

		frame.setBounds(50, 50, 1000, 300);

		// iFrame.setBounds(5, 5, 700, 700);
		// dtopFrame.setBounds(10, 10, 800, 800);
		// dtopFrame.add(iFrame);

		// frame.pack();
		// frame.setVisible(true);
	}
	
	private JPanel filterPanel=null;
	private JComboBox fileList=null;
	private JComboBox locationList=null;
	private JComboBox functionList=null;

	public void toggleWindow()
	{
		System.out.println("making SDView window " + MainForm.togSDView);
		if (MainForm.togSDView)
			frame.setVisible(true);
		else
			frame.setVisible(false);
	}

	public JMenuItem createMenuItem(String label, ActionListener listener,
			String command, int mnemonic, int accKey)
	{
		JMenuItem item = new JMenuItem(label);
		item.addActionListener(listener);
		item.setActionCommand(command);
		if (mnemonic != 0)
			item.setMnemonic(mnemonic);
		if (accKey != 0)
			item.setAccelerator(KeyStroke.getKeyStroke(accKey,
					java.awt.Event.CTRL_MASK));
		return item;
	}

	public void ShowUI()
	{
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				frame.setVisible(true);
				// createAndShowGUI();
			}
		});
	}

	protected JFrame frame = null;
	private List<Service> SO;
	public TxnPanel newContentPane;

	@Override
	public void actionPerformed(ActionEvent event)
	{
		// TODO Auto-generated method stub
		if (event.getActionCommand().equalsIgnoreCase("Dep"))
		{
			System.out.println("Create Dependencies from transactions called");
			EventCollection.getInstance().generateHierarchy();
		} else if(event.getActionCommand().equals("fileAction"))
		{
			System.out.println("fileAction");
			this.currentFile = (String)(((JComboBox) event.getSource()).getSelectedItem());
			newContentPane.setRoot(new ArrayList<String>());
			newContentPane.repaint();
	        reloadTxnList();
	        
		}
		else if(event.getActionCommand().equals("locationAction"))
		{
			System.out.println("locationAction");
			this.currentLocation = (String)(((JComboBox) event.getSource()).getSelectedItem());
			newContentPane.setRoot(new ArrayList<String>());
			newContentPane.repaint();
	    
			reloadTxnList();

		}
		else if(event.getActionCommand().equals("functionAction"))
		{
			
			this.currentFunction = (String)(((JComboBox) event.getSource()).getSelectedItem());
			System.out.println("functionAction "+currentFunction);
			newContentPane.setRoot(new ArrayList<String>());
			newContentPane.repaint();
	    System.out.println("calling reload txn");
			reloadTxnList();

		}
		else
		{
			JMenuItem item = (JMenuItem) event.getSource();
			String cmd = item.getActionCommand();
		}

		// if(cmd.equals("SaveSVG"))try{newContentPane.paintSVG();}catch(Exception
		// e){}
	}

	@Override
	public void valueChanged(ListSelectionEvent e)
	{
		 System.out.println("list changing");
		if (e.getValueIsAdjusting() == false)
		{
			newContentPane.setRoot(txnPane.getSelectedTxns());
			newContentPane.repaint();	
		}

	}

}
