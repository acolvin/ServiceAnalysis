package serviceAnalysisUI.transaction;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import ServiceAnalysisClient.SAEvent;

import serviceAnalysisModel.Transaction.EventCollection;
import serviceAnalysisModel.Transaction.TxnRect;

public class TxnPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5249015450527344373L;

	public TxnPanel()
	{
		this.setBackground(Color.WHITE);
	}

	private EventCollection collection = EventCollection.getInstance();
	private HashMap<String, TxnRect> transactions = null;
	private HashMap<String, Color> txnColours = new HashMap<String, Color>();

	public void setRoot(String txn)
	{
		ArrayList<String> list = new ArrayList<String>();
		list.add(txn);
		if(worker!=null & !worker.isDone())
			{
				worker.cancel(true);
				worker=null;
			}
		transactions = collection.calculatePositions(list,worker);

	}

	private TxnWorker worker = new TxnWorker();
	public TransactionView owner=null;
	

	public void setRoot(List<String> txns)
	{
		min = Long.MAX_VALUE;
		max = Long.MIN_VALUE;
		boolean cancelled=true;
		if(worker!=null & !worker.isDone())cancelled=worker.cancel(true);
		System.out.println("Cancelled txn worker "+cancelled);
		worker = new TxnWorker();
		worker.pb=owner.getProgressBar();
		worker.setRoot(txns, this);

		try
		{
			transactions = null;
			repaint();
			worker.execute();
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// transactions=collection.calculatePositions(txns);
	}

	public void ProcessTxns()
	{
		ProcessTxns(false);
	}
	
	public void ProcessTxns(boolean partial)
	{
		try
		{
			if(!partial)transactions = worker.get();
			else transactions=worker.partialResults;
		} catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// calculate the extents
		for (TxnRect t : transactions.values())
		{
			min = Math.min(t.start, min);
			max = Math.max(max, t.end);
			height = Math.max(height, t.layer);
		}
		// System.out.println("setting size to "+
		// getWidth()+", "+height+" "+(int)(height*20+50));
		this.setSize(getWidth(), height * 20 + 50);
		this.setMinimumSize(new Dimension(getWidth(), height * 20 + 50));
		this.setPreferredSize(new Dimension(getWidth(), height * 20 + 50));
		layerTxns.clear();
		for (String txn : transactions.keySet())
		{
			String root = SAEvent.getRootTxn(txn);
			if (!txnColours.containsKey(root))
			{
				int r, g, b;
				r = 64 + (int) (Math.random() * 150);
				g = 64 + (int) (Math.random() * 150);
				b = 64 + (int) (Math.random() * 150);
				Color c = new Color(r, g, b);
				txnColours.put(txn, c);
			}
			// place txn into layer arraylist
			Integer l = transactions.get(txn).layer;
			ArrayList<String> layerlist;
			if (layerTxns.containsKey(l))
			{
				layerlist = layerTxns.get(l);
			} else
			{
				layerlist = new ArrayList<String>();
				layerTxns.put(l, layerlist);
			}
			layerlist.add(txn);
		}
		this.repaint();
	}

	public ArrayList<TxnRect> getProcessesAtTime(long date)
	{
		ArrayList<TxnRect> txnlist = new ArrayList<TxnRect>();
		// go through each layer and determine what is running at that time
		return txnlist;
	}

	private long min = Long.MAX_VALUE, max = Long.MIN_VALUE;
	private int height = 1;
	private HashMap<Integer, ArrayList<String>> layerTxns = new HashMap<Integer, ArrayList<String>>();
	private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");

	public void paint(Graphics g)
	{
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.WHITE);
		g2d.clearRect(0, 0, this.getWidth(), this.getHeight());
		int xoffset = 20;
		int ydepth = 20;
		int yoffset = 50;
		float xscale = ((float) (this.getWidth() - 40)) / (max - min); // 40 is
																		// 2
																		// times
																		// xoffset

		g2d.setColor(Color.BLACK);
		g2d.drawLine(xoffset, yoffset - 1, this.getWidth() - xoffset,
				yoffset - 1);
		g2d.drawLine(xoffset, yoffset - 1, xoffset, yoffset - 5);
		g2d.drawLine(this.getWidth() - xoffset, yoffset - 1, this.getWidth()
				- xoffset, yoffset - 5);

		Date d = new Date(min);
		String s = sdf.format(d);
		g2d.drawString(s, xoffset, yoffset - 20);
		d = new Date(max);
		s = sdf.format(d);
		int i = SwingUtilities.computeStringWidth(g2d.getFontMetrics(), s);
		g2d.drawString(s, this.getWidth() - xoffset - i, yoffset - 20);

		// System.out.println("xscale="+xscale);
		if (transactions != null && xscale > 0 && transactions.size() > 0)
			for (TxnRect t : transactions.values())
			{
				Color c = txnColours.get(SAEvent.getRootTxn(t.sae.txn));

				String name = t.sae.component + "." + t.sae.function + " "
						+ ((int) ((t.end - t.start))) + "ms";
				int namelen = SwingUtilities.computeStringWidth(
						g2d.getFontMetrics(), name);
				if (c != null)
					g2d.setColor(c);
				int barwidth = (int) Math.max(1, ((t.end - t.start) * xscale));
				if (namelen > barwidth)
				{// cant fit text in bar
					// try just name
					name = t.sae.component + "." + t.sae.function;
					namelen = SwingUtilities.computeStringWidth(
							g2d.getFontMetrics(), name);

				}
				if (namelen > barwidth)
				{// cant fit text in bar
					// try duration
					name = "" + (int) (t.end - t.start) + "ms";
					namelen = SwingUtilities.computeStringWidth(
							g2d.getFontMetrics(), name);
					if (namelen > barwidth)
					{
						name = "" + (int) (t.end - t.start);
						namelen = SwingUtilities.computeStringWidth(
								g2d.getFontMetrics(), name);
					}
				}
				if (namelen > barwidth)
				{// cant fit text in bar
					// try duration
					name = "";
					namelen = 0;

				}
				g2d.setColor(Color.WHITE);
				g2d.fillRect((int) ((t.start - min) * xscale + xoffset),
						(t.layer - 1) * ydepth + yoffset,
						(int) Math.max(1, ((t.end - t.start) * xscale)),
						ydepth - 1);

				g2d.setColor(c);
				g2d.setStroke(new BasicStroke(2));
				g2d.drawRect((int) ((t.start - min) * xscale + xoffset + 1),
						(t.layer - 1) * ydepth + yoffset + 2,
						(int) Math.max(1, ((t.end - t.start) * xscale)),
						ydepth - 3);

				g2d.setStroke(new BasicStroke(1));
				if (t.sae.startTime != null)
				{
					g2d.fillRect(
							(int) ((t.start - min) * xscale + xoffset + 1),
							(t.layer - 1) * ydepth + yoffset + 2,
							(int) Math.max(1, ((t.end - t.start) * xscale)),
							ydepth - 3);
					g2d.setColor(Color.BLACK);
					g2d.drawString(name,
							(int) ((t.start - min) * xscale + xoffset) + 2,
							(t.layer) * ydepth + yoffset - 5);
					// System.out.println((int)((t.start-min)*xscale+xoffset)+","+((Integer)(t.layer*ydepth)).toString()+","+((Integer)(int)((t.end-min)*xscale+xoffset)).toString()+","+((Integer)((t.layer+1)*ydepth-1)).toString());
				} else
					g2d.setColor(Color.BLACK);
			}
	}

}
