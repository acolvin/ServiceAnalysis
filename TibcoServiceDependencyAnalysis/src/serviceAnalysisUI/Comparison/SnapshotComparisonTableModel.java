package serviceAnalysisUI.Comparison;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import javax.swing.table.AbstractTableModel;

import serviceAnalysisModel.Service;

public class SnapshotComparisonTableModel extends AbstractTableModel
{

	/**
     * 
     */
	private static final long serialVersionUID = 1477711050896385711L;
	private HashMap<Service, Integer> snapshotCounts;

	public SnapshotComparisonTableModel(HashMap<Service, Double> snapshotTimes,HashMap<Service,Integer> snapshotCounts)
	{
		this.snapshotTimes = snapshotTimes;
		this.snapshotCounts = snapshotCounts;
		Set<Service> s = snapshotTimes.keySet();
		for (Service ser : s)
		{
			SO.add(ser);
			// mean.add(snapshotTimes.get(ser));
		}

	}

	private HashMap<Service, Double> snapshotTimes;
	private String[] columnNames =
	{ "Service", "Snap Shot Mean", "Mean %Difference", "Snap Shot Count", "Current Count" };
	private ArrayList<Service> SO = new ArrayList<Service>();

	// private ArrayList<Double> mean=new ArrayList<Double>();

	@Override
	public int getRowCount()
	{
		// TODO Auto-generated method stub
		return snapshotTimes.size();
	}

	@Override
	public int getColumnCount()
	{
		// TODO Auto-generated method stub
		return 5;
	}

	public String getColumnName(int col)
	{
		return columnNames[col];
	}

	public Class getColumnClass(int c)
	{
		if (c == 0)
			return String.class;
		if (c != 0)
			return Double.class;
		return null;
	}

	public boolean isCellEditable(int row, int col)
	{
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex)
	{
		if (snapshotTimes.size() == 0)
		{
			return null;
		}
		Service ser = SO.get(rowIndex);
		double ssMean = snapshotTimes.get(ser);
		double diff = (ssMean - ser.getAvgtime()) * 100 / ssMean;
		double ssCount=snapshotCounts.get(ser);
		double cdiff = ser.getCount();//(ssCount - ser.getCount()) * 100/ ssCount; 
		
		if (columnIndex == 0)
			return ser.getType()+":"+ser.getSName();
		if (columnIndex == 1)
			return ssMean;
		if (columnIndex == 2)
			return diff;
		if(columnIndex == 3)
			return ssCount;
		if(columnIndex == 4)
			return cdiff;
		return null;
	}

}
