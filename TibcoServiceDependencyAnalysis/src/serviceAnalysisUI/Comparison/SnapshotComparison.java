package serviceAnalysisUI.Comparison;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.swing.JFrame;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisUI.MainForm;

public class SnapshotComparison extends JFrame implements WindowListener
{

	/**
     * 
     */
	private static final long serialVersionUID = 5072407559187702635L;

	public SnapshotComparison(List<Service> serviceList, Date d)
	{
		String title = "Comparison against Snaphot " + d;
		this.setTitle(title);
		if(MainForm.image!=null)
			setIconImage(MainForm.image);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		// add window components here
		serviceOperation = serviceList;
		loadSnapShotServices(d);
		this.setContentPane(new SnapshotComparisonTable(snapshotTimes,snapshotCounts));
		this.addWindowListener(this);
		pack();
	}

	private HashMap<Service, Double> snapshotTimes = new HashMap<Service, Double>();
	private HashMap<Service, Integer> snapshotCounts = new HashMap<Service, Integer>();
	private List<Service> serviceOperation;

	private void loadSnapShotServices(Date date)
	{
		long snapshot = date.getTime();
		String query = "select SERVICE,MEAN,LAYER,SAMPLECOUNT FROM SUMMARY_SNAPSHOT WHERE SNAPSHOTTIMESTAMP="
				+ snapshot;
		Connection con = null;
		Statement statement = null;
		try
		{
			con = DriverManager.getConnection(
					Controller.DBConnectionStr,
					Controller.DBUser,
					Controller.DBPasswd);
			statement = con.createStatement();
			statement.execute(query);
			ResultSet rs = statement.getResultSet();
			while (rs.next())
			{
				String ser = rs.getString("SERVICE");
				String[] names = ser.split("\\.\\.\\.");

				// System.out.println("Processing Service "+sn);
				Double mean = rs.getDouble("MEAN");
				String layer=rs.getString("LAYER");
				Integer count = rs.getInt("SAMPLECOUNT");
				// float r=(float)response+2.5f;
				// double qty=rs.getDouble("QTY");
				// int q=(int)qty;
				System.out.println(layer+":"+names[0]+":"+names[1]);
				int index = serviceOperation.indexOf(new Service(layer,
						names[0], names[1], ""));
				if (index != -1)
				{
					snapshotTimes.put(serviceOperation.get(index), mean);
					snapshotCounts.put(serviceOperation.get(index), count);
				} else
				{
					System.out.println("Service " + names[0] + "." + names[1]
							+ " not found");
				}
			}

		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("number of matching services "
				+ snapshotTimes.size());
	}

	private JFrame f = this;

	public void ShowUI()
	{
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{

				f.setVisible(true);
				// createAndShowGUI();
			}
		});
	}

	@Override
	public void windowOpened(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeactivated(WindowEvent e)
	{
		// TODO Auto-generated method stub

	}

}
