package serviceAnalysisUI.Comparison;

import java.awt.Dimension;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableRowSorter;

import serviceAnalysisModel.Service;

public class SnapshotComparisonTable extends JPanel implements
		TableModelListener
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6168481588817637752L;

	public SnapshotComparisonTable(HashMap<Service, Double> snapshotTimes, HashMap<Service, Integer>snapshotCounts)
	{
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		model = new SnapshotComparisonTableModel(snapshotTimes,snapshotCounts);
		sorter = new TableRowSorter<SnapshotComparisonTableModel>(model);
		table = new JTable(model);
		table.setRowSorter(sorter);
		table.setPreferredScrollableViewportSize(new Dimension(300, 300));
		table.setFillsViewportHeight(true);
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane);

	}

	SnapshotComparisonTableModel model;
	TableRowSorter<SnapshotComparisonTableModel> sorter;
	JTable table;

	@Override
	public void tableChanged(TableModelEvent e)
	{
		// TODO Auto-generated method stub

	}

}
