package serviceAnalysisUI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import serviceAnalysisModel.*;

public class ServiceGrid
{
	public Map<GridPoint, Service> grid = new HashMap<GridPoint, Service>();
	public HashMap<GridPoint, ArrayList<GridPoint>> lines = new HashMap<GridPoint, ArrayList<GridPoint>>();

	public int MaxX = 0, MaxY = 0;

	public GridPoint fillGrid(Service s, GridPoint p,
			Map<GridPoint, Service> g, Map<GridPoint, ArrayList<GridPoint>> l)
	{
		g.put(p, s);
		ArrayList<GridPoint> gpl;
		int moveon = 0;
		System.out.println(s.getSName() + "@" + p.getX() + "," + p.getY());
		GridPoint t = new GridPoint(p.getX(), p.getY());
		t.setX(t.getX() + 1);
		Iterator<Service> it = s.getDependents();
		while (it.hasNext())
		{
			MaxX = t.getX();
			moveon = -1;
			System.out.println("Entering dependents: y=" + t.getY());
			if (l.containsKey(p))
				gpl = l.get(p);
			else
			{
				gpl = new ArrayList<GridPoint>();
				l.put(p, gpl);
			}
			// l.put(p, t);
			gpl.add(t);
			t = this.fillGrid(it.next(), t, g, l);
			t.setY(t.getY() + 1);
			MaxY += 1;
		}
		MaxY += moveon;
		return new GridPoint(p.getX(), t.getY() + moveon);
	}

}
