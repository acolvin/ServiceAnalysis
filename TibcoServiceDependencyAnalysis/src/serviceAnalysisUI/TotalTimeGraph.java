package serviceAnalysisUI;

import java.awt.Color;
import java.util.List;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import serviceAnalysisModel.Service;

public class TotalTimeGraph extends JFrame
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2326276651667243879L;

	/**
	 * 
	 */

	public TotalTimeGraph(String title, List<Service> services)
	{
		super(title);
		if(MainForm.image!=null)
			setIconImage(MainForm.image);
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		CategoryDataset dataset = createDataset(services);
		chart = createChart(dataset);

		// JFreeChart chart = ChartFactory.createBarChart(
		// "Average Service Times", "Service",
		// "Percentage of total Execution Time", dataset,
		// PlotOrientation.HORIZONTAL, true, false, false);
		chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(600, 350));

		chartPanel.setDomainZoomable(true);
		// chartPanel.setFillZoomRectangle(true);

		setContentPane(chartPanel);
		MainForm.ttg = this;
		this.addWindowListener(MainForm.wl);
	}

	private ChartPanel chartPanel;
	private JFreeChart chart;

	private CategoryDataset createDataset(List<Service> services)
	{
		// double[][] data=new double[1][services.size()];
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		double d = 0;
		for (int i = 0; i < services.size(); i++)
			d += (double) services.get(i).getTotalTime();
		for (int i = 0; i < services.size(); i++)
			if ((double) services.get(i).getTotalTime() / d > 0.01)
				dataset.addValue((double) services.get(i).getTotalTime() / d
						* 100, "Total percentage of cpu time", services.get(i)
						.getSName());
		// for(int i=0;i<services.size();i++)
		// data[0][i]=(double)services.get(i).getAvgtime();

		return dataset;
		// return DatasetUtilities.createCategoryDataset( "Service Time",
		// "SERVICE.operation ", data);
	}

	private JFreeChart createChart(final CategoryDataset dataset)
	{

		final JFreeChart chart = ChartFactory.createBarChart(
				"Percentage of Total Execution Time", "Service Operation",
				"Percentage", dataset, PlotOrientation.HORIZONTAL, false, true,
				false);

		chart.setBackgroundPaint(new Color(249, 231, 236));

		CategoryPlot plot = chart.getCategoryPlot();
		plot.getRenderer().setSeriesPaint(0, new Color(128, 0, 0));
		// plot.getRenderer().setSeriesPaint(1, new Color(0, 0, 255));

		return chart;
	}

	public void redrawGraph(List<Service> services)
	{
		CategoryDataset dataset = createDataset(services);
		chart = createChart(dataset);

		// JFreeChart chart = ChartFactory.createBarChart(
		// "Average Service Times", "Service",
		// "Percentage of total Execution Time", dataset,
		// PlotOrientation.HORIZONTAL, true, false, false);
		chartPanel = new ChartPanel(chart);
		setContentPane(chartPanel);
		this.repaint();

	}

	public void toggleWindow()
	{
		System.out.println("making TotTimeGraph window " + MainForm.togSTimes);
		if (MainForm.togSTimes)
		{

			// chart.fireChartChanged();
			// chartPanel.chartChanged(cce);
			this.setVisible(true);

		} else
			this.setVisible(false);
	}

}
