package serviceAnalysisUI;

import javax.swing.table.AbstractTableModel;

import serviceAnalysisModel.ServiceMapper;

public class MapperTableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6210005830035558497L;

	@Override
	public int getColumnCount() {
		
		return 4;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return ServiceMapper.getMatchCount();
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {
		if(arg0>=ServiceMapper.getMatchCount()) return "";
		if(arg1==0)//type
		{
			int type=ServiceMapper.getType(arg0);
			if(type==ServiceMapper.SERVICE) return "Service";
			else return "Operation";
		}
		if(arg1==1)//Match String
		{
			return ServiceMapper.getMatchString(arg0);
		}
		if(arg1==2)
			return ServiceMapper.getPatternString(arg0);
		if(arg1==3)
			return ServiceMapper.getReplacementString(arg0);
		return "no value";
	}
	
	public String getColumnName(int col)
	{
		return columnNames[col];
	}

	private String[] columnNames =
		{"Type","Service Match String", "Search String","Replacement String"};
	
	public Class getColumnClass(int c)
	{
		return String.class;
	}
	
	//public boolean isCellEditable(int row,int column)
	//{
	//	if(column>0)return true;
	//	else return false;
	//}
}
