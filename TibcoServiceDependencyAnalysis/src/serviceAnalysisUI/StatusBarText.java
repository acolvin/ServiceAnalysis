package serviceAnalysisUI;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.net.JarURLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.JLabel;
import javax.swing.border.BevelBorder;
import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisModel.SAControl.MessageTopic.MessageListener;

public class StatusBarText extends JLabel implements MessageListener
{
	/**
     * 
     */
	private static final long serialVersionUID = 8633464539010576978L;

	/** Creates a new instance of StatusBar */
	public StatusBarText()
	{
		super();
		setPreferredSize(new Dimension(900, 32));
		//setMessage("Ready");
		Controller.getController().addMessageListener(this);

		this.setBorder(new BevelBorder(BevelBorder.LOWERED));

	}

	public void setPerferredWidth(int w)
	{
		super.setPreferredSize(new Dimension(w, 32));
	}

	private String m2="Andrew Colvin, 2013 to 2019, Version " + version;
	public void setMessage(String message)
	{
		//System.out.println("m2: "+m2);
		//System.out.println("m: "+m);
		//System.out.println("message: "+message);
		System.out.println("Status Bar: "+message);
		m2=m;
		if (filtered)
		{
			setText("<HTML>" + m2
					+ "<br/>Filtered: " + message + "</HTML>");
		} else
		{
			setText("<HTML>" + m2
					+ "<br/>" + message + "</HTML>");
		}
		
		m=message;
	}
	
	private void redrawMessage()
	{
		if (filtered)
		{
			setText("<HTML>" + m2
					+ "<br/>Filtered: " + m + "</HTML>");
		} else
		{
			setText("<HTML>" + m2
					+ "<br/>" + m + "</HTML>");
		}
	}
	private String m="Andrew Colvin, 2013 to 2019, Version " + getVersion();

	public void setFiltered(boolean isFiltered)
	{
		//System.out.println(isFiltered);
		filtered = isFiltered;
		redrawMessage();
	}

	
	public final static String getVersion()
	{
		if(buildNumber.equals("adhoc")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmm");
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			String version2;
			try{
				version2=version+"-"+sdf.format(new Date(StatusBarText.getTime(StatusBarText.class)));
			}catch(Exception e){
				return version+"- not packaged";
			}
			return version2;
		}
		else return version +"-"+buildNumber;
	}
	private static final String version = "0.99.52";
	private static final String buildNumber="adhoc";
	private boolean filtered = false;

	public static Long getTime(Class<?> cl) {
	    try {
	        String rn = cl.getName().replace('.', '/') + ".class";
	        JarURLConnection j = (JarURLConnection) ClassLoader.getSystemResource(rn).openConnection();
	        return j.getJarFile().getEntry("META-INF/MANIFEST.MF").getTime();
	    } catch (Exception e) {
	        return null;
	    }
	}
	
	@Override
	public void handleMessage(final String message) {
        if (EventQueue.isDispatchThread()) {
        	
            setMessage(message);
            
        } else {

        	javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    setMessage(message);
                }
            });

        }
		
	}

}
