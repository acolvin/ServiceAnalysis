package serviceAnalysisUI;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.*;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.ServiceListener;
import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisModel.SAControl.DataModelChangeListener;
import serviceAnalysisModel.SAControl.DelayListener;
import serviceAnalysisModel.SAControl.RangeChangeListener;
import serviceAnalysisModel.SAControl.ServiceAddedEvent;
import serviceAnalysisModel.SAControl.ServiceAddedListener;

/**
 * Table model used to display the Service List in a table UI
 * 
 * @author apc
 * 
 */
public class ServiceTableModel extends AbstractTableModel implements
		ServiceListener, ServiceAddedListener, TableColumnModelListener,DataModelChangeListener, DelayListener, RangeChangeListener
{

	/**
	 * The column that can have its value directly changed. This is the explicit
	 * adjustments column
	 */
	final int adjColumn = 7;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ServiceTableModel(List<Service> so)
	{
		
		for(String s:startColumnNames)
			columnArray.add(s);
		for(String s:columnNames)
			allnames.add(s);
		SOorig = so;
		Iterator<Service> i = SO.iterator();
		while (i.hasNext())
		{
			Service s=i.next();
			if(s!=null){
				SO.add(s);
				s.addServiceListener(this);
			}
		}
		
		
		
		Controller.getController().addServiceEventListener(this);
		Controller.getController().addModelChangedListener(this);
		Controller.getController().addDelayListener(this);
		MainForm.sbar.addDelayListener(this);
		Controller.getController().addRangeChangeListener(this);
		updater.start();
	}
	
	
	
	private void updateColumn()
	{
		for(String s:columnNames)
		{
			
		}
	}

	public Service getService(int index)
	{
		return SO.get(index);
	}

	@Override
	public int getColumnCount()
	{
		// TODO Auto-generated method stub
		return columnArray.size();
	}

	@Override
	public int getRowCount()
	{
		// TODO Auto-generated method stub
		return SO.size();
	}

	public String getColumnName(int col)
	{
		if(col>=columnArray.size()) 
		{
			System.out.println("column number greater than size!!"+ col);
			col=columnArray.size()-1;
		}
		return columnArray.get(col);
	}

	@SuppressWarnings("unchecked")
	public Class getColumnClass(int c)
	{
		//System.out.println("in getColumnClass "+c+" SO.size="+SO.size());
		//if(SO.size()==1)System.out.println(SO.get(0));
		if (SO.size() == 0 && c < 2)
			return String.class; // returning null caused sorter to blow up
		if (SO.size() == 0 && c < 6)
			return Integer.class;
		if (SO.size() == 0 && c > 5)
			return Double.class;
		return getValueAt(0, c).getClass();
	}

	public void setValueAt(Object value, int row, int col)
	{
		if (col == adjColumn)
		{
			//System.out.println("setting value");
			Float f;
			if (value == null)
				f = 0f;
			else
				f = (Float) value;
			SO.get(row).setAdjustments(f);
			// this.fireTableDataChanged();
		}
	}

	public boolean isCellEditable(int row, int col)
	{
		// Note that the data/cell address is constant,
		// no matter where the cell appears onscreen.
		if (col != adjColumn)
		{
			return false;
		} else
		{
			return true;
		}
	}

	// private Service so=new Service("test.test");

	@Override
	public Object getValueAt(int arg0, int arg1)
	{
		// TODO Auto-generated method stub
		//System.out.println(arg0+" "+arg1);
		//System.out.println("SO size = "+SO.size());
		if(SO.size()==0 || arg1>=columnArray.size() || arg0>SO.size()-1  )
			return null;
		//map the current column number in model to the original column number
		//if i have problems could have a lookup that gets filled in at point when we remove column
		String col=columnArray.get(arg1);
		for(int i=0;i<columnNames.length;i++)
		{
			String ss=columnNames[i];
			if(ss.equals(col))
			{
				arg1=i;
				break;
			}
		}
			
		//if(col==null )return null;
		//if(SO.get(arg0)==null) return null;
		
		return getValue(SO.get(arg0),arg1);
		
		
	}
	
	private Value getFieldValue(Service s,String field)
	{
		int i=allnames.indexOf(field);
		if(i==-1) return null;
		Value v=null;
		Object o=getValue(s,i);
		//System.out.println("class type "+o.getClass());
		if(String.class.isInstance(o))v=new Value(i,(String)o);
		else if(Double.class.isInstance(o))v=new Value(i,(Double)o);
		else if(Float.class.isInstance(o))v=new Value(i,(Float)o);
		else if(Integer.class.isInstance(o))v=new Value(i,(Integer)o);
		else System.out.println("value is not a recognised type: "+o.getClass().getName());
		return v;
	}

	private class Value {
		int fieldNumber;
		Double value;
		Integer integer;
		String name;
		
		public Value(int number,Double d)
		{
			fieldNumber = number;
			value=d;
			name=null;
			integer=null;
		}
		
		public Value(int number, Float f)
		{
			fieldNumber = number;
			value=f.doubleValue();
			name=null;
			integer=null;
		}
		
		public Value(int number, String s)
		{
			fieldNumber = number;
			value=null;
			name=s;
			integer=null;
		}
		
		public Value(int number, Integer i)
		{
			fieldNumber = number;
			value=null;
			name=null;
			integer=i;
		}
		
	}
	
	private Object getValue(Service s,int arg1)
	{
		if (arg1 == 0)
			return s.getType();
		else if (arg1 == 1)
			return s.getSName();
		else if (arg1 == 2)
			return s.getCount();
		else if (arg1 == 3)
			return s.getRate();
		else if (arg1 == 4)
			return s.getConsumerCount();
		else if (arg1 == 5)
			return s.getDependentCount();
		else if (arg1 == 6)
			return s.getAvgtime();
		else if (arg1 == 7)
			return s.getExplicitAdjustments();
		else if (arg1 == 8)
			return s.getAdjustments();
		else if (arg1 == 9)
			return s.getOperationTime();
		else if (arg1 == 10)
			return s.getOperationTimeStdDev();
		else if (arg1 == 11)
			return s.getOperationTimeLogMedian();
		else if (arg1 == 12)
			return s.getOperationTimeLogMode();
		else if (arg1 == 13)
			return s.getAvgWaitTime();
		else if (arg1 == 14)
			return s.getAvgtime() - s.getAvgWaitTime();
		else if (arg1 == 15)
			return s.getHmean();
		else if (arg1 == 16)
			return s.getPercentileSamples(50);
		else if (arg1 == 17)
			return s.getMinTime();
		else if (arg1 == 18)
			return s.getMaxTime();
		else if (arg1 == 19)
			return s.getRunningAverage();
		else
			return null;
	}
	
	private List<Service> SO = Collections.synchronizedList(new ArrayList<Service>());
	private List<Service> SOorig = null;
	private String[] startColumnNames =
	{ "Layer", "Service Operation", "Count", "Rate", "Cs", "Ds",
			"Average", "Explicit Adjustment",
			"Adjustments -/+", "Adjusted Average", "Std Dev", "Avg Wait Time", "CPU Time",
			"Running Average"};
	private String[] columnNames =
	{ "Layer", "Service Operation", "Count", "Rate", "Cs", "Ds",
			"Average", "Explicit Adjustment",
			"Adjustments -/+", "Adjusted Average", "Std Dev",
			"Log Median", "Log Mode", "Avg Wait Time", "CPU Time", "Harmonic Mean",
			"Median","Min","Max","Running Average" };

	public ArrayList<String> getUnDisplayedColumns()
	{	
		ArrayList<String> columns=new ArrayList<String>();
		for(String s:columnNames)
		{
			if(!columnArray.contains(s))columns.add(s);
		}
		return columns;
	}
	
	public ArrayList<String> getDisplayedColumns()
	{
		ArrayList<String> columns=new ArrayList<String>();
		for(String s:columnArray)
		{
			columns.add(s);
		}
		return columns;
	}
	
	public int getColumnNumber(String head)
	{
		int col;
		col=columnArray.indexOf(head);
		return col;
	}
	
	public ArrayList<String> columnArray=new ArrayList<String>();

	private boolean isUserScrolling()
	{
		boolean scroll=true;
		scroll=scroll&serviceTable.isScrolling();
		
		return scroll;
	}
	
	public void setDelayedUpdates(boolean delayed)
	{
		System.out.println("setDelayedUpdates called "+delayed);
		if(delayedUpdates==delayed) return;
		delayedUpdates=delayed;
		inserts.clear();
		updates.clear();

		fireTableDataChanged();
		
	}
	
	private void waitForDispatchQueue()
	{
		
		EventQueue queue = Toolkit.getDefaultToolkit().getSystemEventQueue();
		if(EventQueue.isDispatchThread()) return;  //never sleep on dispatch queue
		while (queue.peekEvent() != null ) {
			try {
				
				Thread.sleep(10);
			} catch (InterruptedException e1) {
			
			
			}
		}
	}
	
	private boolean readyToSendEvent()
	{
		EventQueue queue = Toolkit.getDefaultToolkit().getSystemEventQueue();
		boolean edt=EventQueue.isDispatchThread();
		if(!edt){
			while(isUserScrolling()) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e1) {
					
				}
			}
		
			int count=0;
		
			while (queue.peekEvent() != null & count<100 ) {
				try {
					count++;
					Thread.sleep(10);
				} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				
				}
			}
		}
		return edt;
	}
	
	@Override
	public void fireTableDataChanged()
	{
		if((!updater.isAlive() | updater.isInterrupted())&!Controller.isShuttingDown()){
			System.out.println("The updater thread has stopped.  Starting a new one!");
			updater=new Updater(this);
			updater.start();
		}else if(!Controller.isShuttingDown())
		{
			//System.out.println("Forcing restart of updater thread");
			updater.run=false;
			updater=new Updater(this);
			updater.start();
		}
		if(readyToSendEvent()){ //returns immediately with true if dispatcher thread else waits until dispatcher thread free
			
			synchronized(SO){
				updates.clear();
				inserts.clear();
				if(SOorig.size()!=0) {
					System.out.println("SOorig size="+SOorig.size());
					Service clone[]=SOorig.toArray(new Service[1]);
					SO.clear();
					for(Service s:clone) SO.add(s);
					System.out.println("SO size="+SO.size());
					super.fireTableDataChanged();
				}else 
				{
					SO.clear();
					super.fireTableDataChanged();
				}
				
			} 
		} else
		{
			javax.swing.SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
						
					fireTableDataChanged(); //recall but this time on dispatcher thread
				}
			});	
		}
		
	}
	
	@Override
	public void fireTableRowsInserted(final int from, final int to)
	{
		
		if(readyToSendEvent()){ //returns immediately with true if dispatcher thread else waits until dispatcher thread free
			EventQueue queue = Toolkit.getDefaultToolkit().getSystemEventQueue();
			if(to<SO.size())
			{
				//for(int i=from;i<=to;i++)
					super.fireTableRowsInserted(from,to); 
					//System.out.println("inserted "+from+" to "+to);
			}
		} else
		{
			for(int i=from;i<=to;i++)
			{
				final int j=i;
				try {
					javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
						@Override
						public void run() {
							//System.out.println("inserting "+j);
							fireTableRowsInserted(j,j);
						}
					});
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			/*javax.swing.SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
						
					fireTableRowsInserted(from,to); //recall but this time on dispatcher thread
				}
			});	*/
		}
		
	}
	@Override
	public void fireTableRowsUpdated(final int from, final int to)
	{
		
		if(readyToSendEvent()){ //returns immediately with true if dispatcher thread else waits until dispatcher thread free
				//test that model has not been cleared before firing event - could still be unlucky unless lock model which I am not going to do
				if(to<SO.size())super.fireTableRowsUpdated(from,to);
				//System.out.println("updated "+from+" to "+to);
		} else
		{
			for(int i=from;i<=to;i++)
			{
				final int j=i;
				try {
					javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
						@Override
						public void run() {
							//System.out.println("updating "+j);
							fireTableRowsUpdated(j,j);
						}
					});
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			/*javax.swing.SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
						
					fireTableRowsUpdated(from,to); //recall but this time on dispatcher thread
				}
			});	*/
		}
		
	}
	
	private Updater updater=new Updater(this);
	private boolean delayedUpdates=true;
	private long updateDelay=5000;
	private List<Integer> updates=Collections.synchronizedList(new ArrayList<Integer>());
	private List<Service> inserts=Collections.synchronizedList(new ArrayList<Service>());

	@Override
	public void handleTimeChange(Service s)
	{
		// TODO Auto-generated method stub
		// System.out.println("handle time change called "+s);
		if(!SO.contains(s) ) {
			return;
		}
		else 
		{
			Integer i = SO.indexOf(s);
			if(i>-1)
			{
				if(delayedUpdates ){
					if(!updates.contains(i))updates.add(i);
				} else 
				{
					fireTableRowsUpdated(i, i); //this overridden version forces onto edt
				}
			}
		}
	}

	private boolean firstRun=true;
	private boolean addService=false;
	
	
	private class Updater extends Thread
	{
		public boolean run=true;
		public Updater(ServiceTableModel model)
		{
			this.setName("Service Table Updater");
			this.setDaemon(true);
			this.model=model;
		}
		
		private void inserter(final Service [] services)
		{
			if(readyToSendEvent())
			{
				synchronized(SO){
					int i,j;
					i=SO.size()-1;
					i=Math.max(0, i);
					for(Service s:services)
						SO.add(s);
					j=SO.size()-1;
					if(j>i | j==0) try{fireTableRowsInserted(i,j);}catch(Exception e){e.printStackTrace();}
				}
			}
			else
			{
				try {
					javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
						@Override
						public void run() {
							
							inserter(services);
							
						}
					});
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		boolean fullUpdate;
		private ServiceTableModel model;
		
		public void run()
		{
			
			while(run)
			{
				if(delayedUpdates){
					Integer[] updates1;
					Service[] inserts1;
					int count=getRowCount();
					synchronized(updates){
						synchronized(inserts){
							//java.util.Collections.sort(inserts);
							inserts1=inserts.toArray(new Service[1]);
							inserts.clear();
							if(inserts1.length>30) fullUpdate=true;
						}
					
						updates1 = updates.toArray(new Integer[1]);
						updates.clear();
						//fullUpdate=addService;
						//addService=false;
						if(updates1.length>30)fullUpdate=true; 
						
					}
					final Integer[] updates2=updates1;
					final Service[] inserts2=inserts1;
					//System.out.println("firing delayed updates "+updates1.length);
					
					//javax.swing.SwingUtilities.invokeLater(new Runnable() {
							//@Override
							//public void run() {
								//System.out.println("Delayed updates triggered");
								
								if(fullUpdate)
								{
									if(inserts2[0]!=null)
									{
										inserter(inserts2);
									}
									
									int max=0;
									int min=Integer.MAX_VALUE;
									for(Integer next:updates2){
										if(next!=null){
											if(next>max)max=next;
											if(next<min)min=next;
										}
									}
									addService=false;
									//model.fireTableDataChanged();
									//System.out.println("row count="+count);
									try{
										if(min<=max)model.fireTableRowsUpdated(min,max); //all rows between first and last
									}catch(Exception e){ //just to catch any exception so that updater thread doesnt die
										e.printStackTrace();
									}

									
								} 
								else {
									if(inserts2[0]!=null)
									{
										//System.out.println("calling inserter");
										inserter(inserts2);
									} 
									for(Integer i:updates2)
									{
										if(i!=null)	try{
											model.fireTableRowsUpdated(i,i);
										}catch (Exception e){e.printStackTrace();}
									}
									
								}
							//});

					
				}
				long delayLeft=updateDelay;
				
				
				while(delayLeft>0) {
					try {
						Thread.sleep(Math.min(10, delayLeft));
						delayLeft=Math.min(delayLeft-10, updateDelay);
					} catch (InterruptedException e) {delayLeft=0;run = false;	}
					
					
				}
			}
			
		}
	}

/*	
	public class eventWorker extends SwingWorker<Void, Void>
	{
		@Override
		public Void doInBackground()
		{
			if (t != null & i != -1)
				//did the surrounding rows to try to catch inserts when updating
				t.fireTableRowsUpdated(Math.max(0, i-1), i+1);
			if (t != null & i == -1)
				t.fireTableDataChanged();
			return null;
		}

		public eventWorker(AbstractTableModel a, int j)
		{
			t = a;
			i = j;
		}

		AbstractTableModel t = null;
		int i = -1;

		public void setModel(AbstractTableModel a)
		{
			t = a;
		}

		public void setCol(int j)
		{
			i = j;
		}

	}
*/	
	
	public boolean columnRemoved(String column){
		if(!columnArray.contains(column))return false;
		int columnnumber=columnArray.indexOf(column);
		if(columnnumber<2) return false;
		columnArray.remove(column);
		
		return true;
	}
	public boolean columnRemoved(int column,String columnName)
	{
		//System.out.println("calling columnRemoved");
		if(column>=columnArray.size()) column=columnArray.indexOf(columnName);
		String s=columnArray.get(column);
		//System.out.println("removing column "+s);
		if(!s.equals(columnName)) 
		{
			System.out.println("column name and number do not match");
			column=columnArray.indexOf(columnName);
			return false;
		}
		else
		{
			if(column>columnArray.size()-1|| column<0)
			{
				column=columnArray.indexOf(columnName);
				//System.out.println("resetting columnnumber to "+column);
			}
			if(column<2) return false;
			else
			if(column>-1  && column < columnArray.size()){
				columnArray.remove(column);
				//no need to fire this as i create a new table based on this model
				//this is so yuck but the whole thing is a workaround and breaks model/view separation
				
				//this.fireTableStructureChanged();
/**
				for(String ss:columnArray)
				{
					System.out.print(ss);
					System.out.print(", ");
				}
**/
				
			}
			return true;
		}
	}

	public boolean addColumn(String column)
	{
		if(!columnArray.contains(column))
		{
			columnArray.add(column);
			this.fireTableStructureChanged();
			return true;
		}
		return false;
	}
	
	public void resetColumns()
	{
		columnArray.clear();
		for(String s:columnNames)
			columnArray.add(s);
		fireTableStructureChanged();
	}
	
	public void addService(final Service s)
	{
		synchronized(SO){
			if(SO.contains(s) | inserts.contains(s)) return;
			
		}
		final int i=SO.indexOf(s);
		if(delayedUpdates )
		{
			inserts.add(s);
			
		}
		else 
		{
			javax.swing.SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					System.out.println("Adding service "+s);
					SO.add(s);
					int i=SO.indexOf(s);
					fireTableRowsInserted(i, i);
					
				}
			});	
			
		}
		s.addServiceListener(this);
	}
	
	
	
	@Override
	public void ServiceAdded(ServiceAddedEvent event)
	{
		
		addService(event.getService());
		
	}

	@Override
	public void columnAdded(TableColumnModelEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void columnMarginChanged(ChangeEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void columnMoved(TableColumnModelEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void columnRemoved(TableColumnModelEvent arg0) {
		System.out.println("Column Removed "+arg0.getFromIndex()+","+arg0.getToIndex());
		
	}

	@Override
	public void columnSelectionChanged(ListSelectionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modelChanged() {
		System.out.println("service model changed");
		synchronized(updates){
			synchronized(inserts){
				inserts.clear();
				updates.clear();
				//fireTableDataChanged();
			}
		}

		
		fireTableDataChanged();
	}

	@Override
	public void dataChanged() {
		//if(SO.size()>0)
		//fireTableRowsUpdated(0,SO.size()-1);
		
	}

	private int maxrow=0;
	
	private ServiceTable serviceTable=null;
	public void addServiceTableModelListener(ServiceTable st)
	{
		serviceTable=st;
	}

	@Override
	public void setDelay(long delay) {
		updateDelay=delay;
		
	}
	
	public int getCellColour(int fieldNumber,int row)
	{
		try{
			String field;
			field=columnArray.get(fieldNumber);
			return getCellColour(field,row);
		} catch (Exception e) {return WHITE;}//always make sure we return a colour
	}
	
	public int getCellColour(String field,int i)
	{
		try{
		Service s=SO.get(i);
		if(s==null) return WHITE;
		String name=s.getType()+"."+s.getSName();
		//System.out.println("getCellColour "+field+" "+name);
		Range r=null;
		float f=0;
		
		if(colourFields.containsKey(name))
			r=colourFields.get(name).get(field);
		
		//System.out.println("getCellColour: range object="+r);
		
		if(r==null)return WHITE;
		
		Value v=getFieldValue(s,field);
		//if(v==null)System.out.println("getCellColour: fieldvalue=null");
		//else System.out.println("getCellColour: fieldvalue="+v.value+","+v.name);
		int colour;
		if(v!=null && v.value!=null)
			colour=r.getColour(v.value.floatValue());
		else if(v!=null & v.integer!=null)
			colour=r.getColour(v.integer.floatValue());
		else
		{
			colour=WHITE;
			double running=s.getRunningAverage();
			if(field.equals("Service Operation") && !Double.isNaN(running))
			{
				double average=s.getOperationTime();		
				if(running<=average) colour=GREEN;
				else if(running>average*1.333)colour=RED;
				else colour=ORANGE;
			}
		}
		return colour;
/*		
		if(field==RATE){
			r=rateColour.get(name);
			f=(float)s.getRate();
		}
		else if(field==AVERAGE){ 
			r=averageColour.get(name);
			f=(float)s.getAvgtime();
		}
		if(r==null) return WHITE;
		else return r.getColour(f);
*/
		}catch(Exception e){return WHITE;}
	}
	
	public boolean isHighlighted(int i){
		Service s=SO.get(i);
		String name=s.getType()+"."+s.getSName();
		boolean b=colourFields.containsKey(name);
				//rateColour.containsKey(name)|averageColour.containsKey(name);
		//System.out.println("isHighlighted = "+b+" for name "+name);
		//if(b)return true;
		//else return false;
		return b;
	}
	
	public ConcurrentHashMap<String,ConcurrentHashMap<String,Range>> colourFields=new ConcurrentHashMap<String,ConcurrentHashMap<String,Range>>();
	public ConcurrentHashMap<String, Range> rateColour=new ConcurrentHashMap<String, Range>();
	public ConcurrentHashMap<String, Range> averageColour=new ConcurrentHashMap<String, Range>();
	
	public static final int WHITE=0;
	public static final int GREEN=1;
	public static final int ORANGE=2;
	public static final int RED=3;
	public static final int RATE=0;
	public static final int AVERAGE=1;
	
	private class Range {
		private float green;
		private float orange;
		private float red;

		public Range(float green,float orange,float red)
		{
			this.green=green;
			this.orange=orange;
			this.red=red;
		}
		
		public int getColour(float f)
		{
			if(Float.isNaN(f))return WHITE;
			if(f==green)return GREEN;
			if(f<green)return WHITE;
			else if(f<orange) return GREEN;
			else if(f<red) return ORANGE;
			else return RED;
		}
	}

	private ArrayList<String> allnames=new ArrayList<String>();
	private Boolean lockme=true; //used for synchronization
	@Override
	public void addRange(String field, String service, float green,
			float orange, float red) {
		//System.out.println("addRange "+service);
		if(!allnames.contains(field)) {
			//System.out.println("Field "+field+ " does not exist in the table");
			return;
		}else {
			ConcurrentHashMap<String, Range> fieldMap=colourFields.get(service);
			if(fieldMap==null)synchronized(lockme){
				if(fieldMap==null){//someone didnt complete whilst i was syncronized
					fieldMap=new ConcurrentHashMap<String, Range>();
					colourFields.put(service, fieldMap);
				}
			}
			fieldMap.put(field, new Range(green,orange,red));
		}
		
		
		//if(field.equalsIgnoreCase(columnNames[3]))
		//{
		//	rateColour.put(service,new Range(green,orange,red));
		//}else if(field.equalsIgnoreCase(columnNames[6]))
		//{
		//	averageColour.put(service,new Range(green,orange,red));
		//}
			
		
	}



	@Override
	public void removeRange(String field, String service) {
		if(service==null)return;
		if(field==null)colourFields.remove(service);
		else 
		{
			ConcurrentHashMap<String, Range> fieldMap=colourFields.get(service);
			if(fieldMap!=null)fieldMap.remove(field);
			
		}
		//if(field.equalsIgnoreCase(columnNames[3]))
		//{
		//	rateColour.remove(service);
		//} else if(field.equalsIgnoreCase(columnNames[6]))
		//{
		//	averageColour.remove(service);
		//}
	}
}