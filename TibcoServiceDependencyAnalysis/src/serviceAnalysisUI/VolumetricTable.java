package serviceAnalysisUI;

import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableRowSorter;
import javax.swing.table.TableColumn;
import serviceAnalysisModel.VolumetricModel;

public class VolumetricTable extends JPanel implements TableModelListener
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3641213821560531245L;

	public VolumetricTable(VolumetricModel vModel)
	{
		// TODO Auto-generated constructor stub
		SO = vModel;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		// Create a table with a sorter.
		model = new VolumetricTableModel(vModel);
		sorter = new TableRowSorter(model);
		table = new JTable(model);
		table.setRowSorter(sorter);
		table.setPreferredScrollableViewportSize(new Dimension(400, 200));
		table.setFillsViewportHeight(true);

		// table.getModel().addTableModelListener(this);
		// model.addTableModelListener(this);

		// For the purposes of this example, better to have a single
		// selection.
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		table.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		TableColumn col = table.getColumnModel().getColumn(0);
		col.setPreferredWidth(200);
		col.setMinWidth(50);
		// col.setResizable(false);
		col = table.getColumnModel().getColumn(1);
		col.setPreferredWidth(100);
		col.setMaxWidth(100);
		// col.setResizable(false);
		col = table.getColumnModel().getColumn(2);
		col.setPreferredWidth(100);
		col.setMaxWidth(100);
		// col.setResizable(false);

		this.setMinimumSize(new Dimension(450, 100));

		// When selection changes, provide user with row numbers for
		// both view and model.

		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener()
				{
					public void valueChanged(ListSelectionEvent event)
					{
						int viewRow = table.getSelectedRow();
						if (viewRow < 0)
						{
							// Selection got filtered away.
							// statusText.setText("");
							// System.out.println("selected row negative");
						} else
						{
							int modelRow = table
									.convertRowIndexToModel(viewRow);
							// statusText.setText(
							// String.format("Selected Row in view: %d. " +
							// "Selected Row in model: %d.",
							// viewRow, modelRow));
							// System.out.println("Row "+modelRow+" selected = "+SO.get(modelRow).getsName());
							// new SDPanel(SO.get(modelRow));

							wst.changeVT(model.getVolumetricType(modelRow));

							// sdview.newContentPane.repaint();
							// sdview.newContentPane.setService(SO.get(modelRow));
							// if(cfg!=null){cfg.setVisible(false);
							// cfg.setEnabled(false); cfg.dispose();}
							// cfg=new
							// CumulativeFrequencyGraph(SO.get(modelRow).getSName(),SO.get(modelRow).getSamples());
							// cfg.pack();
							// cfg.setVisible(true);
						}
					}

				});

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setMinimumSize(new Dimension(450, 100));

		// Add the scroll pane to this panel.
		add(scrollPane);
		// add(table);

	}

	public void clearModel()
	{
		System.out.println("clearing Volumetric Table");
		SO.model.clear();
		model.clear();
		this.model.fireTableDataChanged();

		// sorter.allRowsChanged();

	}

	// private CumulativeFrequencyGraph cfg=null;

	public void setFilter(String layer, String service, boolean modelled)
	{
		UIRowFilter filter = new UIRowFilter(layer, service, modelled);

		// sorter.setRowFilter(filter);
	}

	public void addSDView(ServiceDependencyView sd)
	{
		sdview = sd;
		// System.out.println(sd);
	}

	private VolumetricModel SO;
	public VolumetricTableModel model;
	private JTable table;
	private ServiceDependencyView sdview = null;
	private TableRowSorter sorter;
	public WorkstreamTable wst;

	@Override
	public void tableChanged(TableModelEvent e)
	{
		// TODO Auto-generated method stub
		// System.out.println("erqwerqw");
		int row = e.getFirstRow();
		int column = e.getColumn();
		VolumetricTableModel model = (VolumetricTableModel) e.getSource();
		// String columnName = model.getColumnName(column);
		float data = (Float) model.getValueAt(row, column);
		// System.out.println("Value changed to "+ data+
		// " for service operation "+ SO.get(row).getsName());

	}

}
