package serviceAnalysisUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisModel.SAControl.StdOutHandler;
import serviceAnalysisModel.SAControl.TeeConsumer;
import serviceAnalysisModel.SAControl.TextReports;

public class SystemOutWindow extends JFrame implements TeeConsumer,WindowListener, ActionListener {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 673096583548752183L;

	public SystemOutWindow() {
    	
    	this.setTitle("System.out");
    	if(MainForm.image!=null)
			setIconImage(MainForm.image);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.addWindowListener(this);
    	
    	
    	
    	//tr=new TextReports();
    	
 
    	
    	
    }
    
    JTextField input=new JTextField();
   // OutputStreamTee tee=null;
   // OutputStream out=null;
    
    public void showUI()
    {
    	setTitle("System.out");
    	//output.setRows(1000);
    	JMenuItem menuItem = new JMenuItem("Clear");
	    menuItem.setActionCommand("clear");
	    menuItem.addActionListener(this);
	    popup.add(menuItem);
    	
    	JPanel jp=new JPanel(new BorderLayout());
    	//out=System.out;
    	//System.err.println("StdoutW: System.out="+System.out);
    	//tee=new OutputStreamTee(this,out);
    	//System.setOut(new PrintStream(tee));
    	//System.err.println("StdoutW: New System.out="+System.out);
    	//output = new JTextArea();
    	StdOutHandler.addConsumer(this);
    	output.append(StdOutHandler.out.toString());
    	output.setVisible(true);
    	jp.setPreferredSize(new Dimension(400,400));
//    	jp.add(output);
    	jp.add(popup);
    	MouseListener popupListener = new PopupListener();
        output.addMouseListener(popupListener);
    	
    	
    	jp.add(new JScrollPane(output));
    	output.setEditable(false);
    	jp.add(input,BorderLayout.SOUTH);
    	input.setActionCommand("Input");
    	input.addActionListener(this);
    	setContentPane(jp);
    	setSize(400, 400);
        setLocationRelativeTo(null);
        setVisible(true);
        setPreferredSize(new Dimension(400,400));
    	
  	
       	setBounds(300,200,400,400);
       	//System.out.println(output);
       	//output.append("test");
    	javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
		    	pack();  
				f.setVisible(true);
				
			}
		});
    }
    private JPopupMenu popup=new JPopupMenu();
    
    class PopupListener extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
            maybeShowPopup(e);
        }

        public void mouseReleased(MouseEvent e) {
            maybeShowPopup(e);
        }

        private void maybeShowPopup(MouseEvent e) {
            if (e.isPopupTrigger()) {
                popup.show(e.getComponent(),
                           e.getX(), e.getY());
            }
        }
    }
    
    JFrame f=this;
    TextReports tr;
    private JTextArea output=new JTextArea();
    
	@Override
	public void appendText(final String text) {
		//System.err.println("appendtext: "+text);
        if (EventQueue.isDispatchThread()) {
        	//System.out.println("outputing");
        	StringBuilder s=new StringBuilder();
        	
        	s.append(output.getText());
        	//output.setText("");
        	
        	s.append(text);
        	String ss;
        	if(s.length()>100001)ss=s.substring(s.indexOf("\n",s.length()-100000),s.length());
        	else ss=s.toString();
        	//System.err.println(ss.length());
        	output.setText(ss);
        	
            //output.append(text);
            //output.setCaretPosition(output.getText().length());
        } else {

        	javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    appendText(text);
                }
            });

        }

	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		
		//System.setOut((PrintStream) out);
		StdOutHandler.removeConsumer(this);
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getActionCommand().equals("clear"))
		{
			StdOutHandler.clearSystemOutBuffer();
			output.setText("");
		}
		else if(arg0.getActionCommand().equals("Input"))
		{
			String text=input.getText();
			input.setText("");
			if(!text.startsWith("COMMAND: "))
				text="COMMAND: "+text;
			appendText(text+'\n');
			Controller.getController().getReader().newLogFileLine(text, "input line");
		}
		else
			System.out.println(arg0.getActionCommand());
		
	}

}
