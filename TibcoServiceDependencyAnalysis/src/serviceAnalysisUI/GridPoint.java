package serviceAnalysisUI;

public class GridPoint
{
	private int x = 0, y = 0;

	public GridPoint(int X, int Y)
	{
		x = X;
		y = Y;
	}

	public GridPoint()
	{

	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public boolean equals(Object obj)
	{
		System.out.println("gridpoint equals called: " + x + "," + y + "  "
				+ ((GridPoint) obj).getX() + "," + ((GridPoint) obj).getY());
		return ((x == ((GridPoint) obj).getX()) & (y == ((GridPoint) obj)
				.getY()));
	}

	public int hashCode()
	{
		return x * 1000 + y;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	public String toString()
	{
		return "(" + x + "," + y + ")";
	}
}
