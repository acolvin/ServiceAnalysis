package serviceAnalysisModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This object models a business process which calls multiple use cases which us
 * the services to deliver the process. For example, ordering a widget and it's
 * fulfilment
 * 
 * @author apc
 * 
 */
public class VolumetricType implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3179027532612618755L;

	/**
	 * Creates the volumetric type with the given name
	 * 
	 * @param name
	 */
	public VolumetricType(String name)
	{
		super();
		this.name = name;
	}

	private String name;

	/**
	 * Gets the process name
	 * 
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	private List<Workstream> workstreams = new ArrayList<Workstream>();
	private Map<Workstream, Double> workstreamProportion = new HashMap<Workstream, Double>();

	public Map<Workstream, Double> getWorkstreamProportion()
	{
		return workstreamProportion;
	}

	/**
	 * Returns the use cases that are used to fulfil this process
	 * 
	 * @return
	 */
	public List<Workstream> getWorkstreams()
	{
		return workstreams;
	}

	/**
	 * Sets the workstreams that fulfil this process
	 * 
	 * @param workstreams
	 */
	public void setWorkstreams(List<Workstream> workstreams)
	{
		this.workstreams = workstreams;

	}

	/**
	 * Set the process name
	 * 
	 * @param name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Add a use case to this process. Defaults to one iteration
	 * 
	 * @param s
	 */
	public void addWorkstream(Workstream s)
	{
		if (!workstreams.contains(s))
		{
			workstreams.add(s);
			workstreamProportion.put(s, 1D);
		}
	}

	/**
	 * Sets the number of times this use case is called during the lifecycle of
	 * the Process
	 * 
	 * @param w
	 *            the use case
	 * @param multiplier
	 */
	public void setWorkstreamMultiplier(Workstream w, double multiplier)
	{
		workstreamProportion.put(w, multiplier);
	}

	public double getWorkstreamMultiplier(Workstream w)
	{
		return workstreamProportion.get(w);
	}

	/**
	 * Adds the use case to the process and sets the number of times the process
	 * calls the use case during its lifecycle
	 * 
	 * @param s
	 * @param multiplier
	 */
	public void addWorkstream(Workstream s, double multiplier)
	{
		workstreams.add(s);
		workstreamProportion.put(s, multiplier);

	}

	/**
	 * simulate the time taken to process a number of the processes
	 * 
	 * @param volume
	 *            The number of the processes to simulate
	 * @return The total time to run the process volume times
	 */
	public double simulate(int volume)
	{
		Iterator<Workstream> i = workstreams.iterator();
		double time = 0, sim = 0;
		// model.clear();
		while (i.hasNext())
		{
			Workstream workstream = i.next();
			// sim=workstream.getSimTime();
			sim = workstream.simulate(((double) volume * workstreamProportion
					.get(workstream)));
			time += workstream.getSimTime() * volume;
			model.put(workstream, sim * volume);
		}

		return time;
	}

	/**
	 * Gets the model of the process in terms of the use cases that are called
	 * and how many times each use case is called to complete the Process
	 * 
	 * @return
	 */
	public Map<Workstream, Double> getWorkstreamModel()
	{
		return model;
	}

	private Map<Workstream, Double> model = new HashMap<Workstream, Double>();

}
