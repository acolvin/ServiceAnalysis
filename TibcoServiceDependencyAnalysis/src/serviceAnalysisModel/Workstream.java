package serviceAnalysisModel;

import java.util.Iterator;
import java.util.List;

public class Workstream extends Service
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6575005284701428381L;

	public Workstream(String name)
	{
		// super(name);
		// TODO Auto-generated constructor stub
		super(Service.WS_TYPE, "WORKSTREAM", name, "");
	}

	public Workstream(String type, String service, String operation,
			String addData)
	{
		super(type, service, operation, addData);
		// TODO Auto-generated constructor stub
	}

	public Workstream(List<Service> dependents, String name)
	{
		super(dependents, name);
		// TODO Auto-generated constructor stub
	}

	public Workstream(String name, Service dep)
	{
		super(name, dep);
		// TODO Auto-generated constructor stub
	}

	public double simulate(double vol)
	{
		double time = 0;
		Service s = null;
		double iterations = 1;
		Double I = null;
		Iterator<Service> i = super.getDependents();
		while (i.hasNext())
		{
			s = i.next();
			I = super.getIterations().get(s);
			if (I == null)
				iterations = 1d;
			else
				iterations = I.doubleValue();
			time += s.getOperationTime() * iterations;
		}

		simTime = time;
		return time;
	}

	public String getName()
	{
		return super.getOperationName();
	}

	private Double simTime = 0d;

	public Double getSimTime()
	{
		return simTime;
	}
}
