package serviceAnalysisModel;

/**
 * This interface must be implemented by a class if it is interested in any time
 * updates that occur on a service
 * 
 * @author apc
 * 
 */
public interface ServiceListener
{

	/**
	 * The method to implement when a time change notification is triggered
	 * 
	 * @param s
	 *            The service that underwent the event
	 */
	public void handleTimeChange(Service s);
}
