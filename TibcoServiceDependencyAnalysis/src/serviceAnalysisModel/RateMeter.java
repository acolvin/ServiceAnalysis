package serviceAnalysisModel;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 * A general rate meter class that determines how fast the different samples are
 * arriving per second
 * 
 * @author apc
 * 
 */
public class RateMeter implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5424394762586571205L;
	private long[] sampling;
	private int size;
	private boolean initialised = false;

	private int pointer = 0;
	private double multiplier;
	private int numberSamples = 0;
	private HashMap<Date, Double> rateSamples = new HashMap<Date, Double>();

	// private HashMap<Long,Integer> countPerSecond=new HashMap<Long,Integer>();

	/**
	 * Creates a rate meter
	 * 
	 * @param samples
	 *            The number of samples to average the rate over
	 * @param date
	 *            The date/time to start the rate count from
	 */
	public RateMeter(int samples, Date date)
	{
		sampling = new long[samples];
		size = samples;
		multiplier = 1000d * (double) size;
		// initialise(date);

	}

	/**
	 * Initialises a rate meter from a given date/time
	 * 
	 * @param date
	 *            the new date time
	 */
	private synchronized void initialise(Date date)
	{
		if (!initialised)
		{
			pointer=0;
			long a = date.getTime();
			for (int i = 0; i < size; i++)
				sampling[i] = a;
			initialised = true;
			
		}
	}

	/**
	 * Adds a sample to the rate meter at the given date/time
	 * 
	 * @param date
	 *            The date/time of the even
	 */
	public synchronized void addSample(Date date)
	{
		if (!initialised)
		{
			initialise(date);
		}
		//long t=sampling[pointer];
		if(sampling[(pointer-1+size)%size]>date.getTime()) {
			//System.out.println("original "+sampling[(pointer-1+size)%size]);
			//System.out.println("current "+date.getTime());
			sampling[pointer] = sampling[(pointer-1+size)%size];
			int p=(pointer-1+size)%size;
			while(sampling[(p-1+size)%size]>date.getTime() & p!=pointer)
			{
				int q=p;
				p=(p-1+size)%size;
				sampling[q]=sampling[p];
			}
			
			sampling[p]=date.getTime();
			//System.out.println("sample out of order");
			//System.out.println("pointer="+pointer+" p="+p);
			//System.out.println("-1: "+sampling[(p-1+size)%size]);
			//System.out.println("0: "+sampling[p]);
			//System.out.println("+1: "+sampling[(p+1+size)%size]);
		}
		else sampling[pointer] = date.getTime();
		pointer++;
		pointer = pointer % size;
		// moved to service
		if (sampleCapture)
		{
			numberSamples++;
			// System.out.println("number of samples "+numberSamples);
			if (numberSamples >= size)
				rateSamples.put(date, getRate());
			// Long l=date.getTime()/samplePeriod;
			// if(countPerSecond.containsKey(l)) //all this count stuff should
			// move out of here and go into service
			// {
			// Integer i=countPerSecond.get(l);
			// if(i!=null)
			// {
			// i=i+1;
			// }
			// else i=1;
			// countPerSecond.put(l, i);
			// System.out.println(new
			// Date(l*1000)+" "+i+" "+countPerSecond.get(l));
			// }
			// else countPerSecond.put(l,new Integer(1));
		}
		//
	}

	/**
	 * resets the rate meter to an initialised time of now and zero samples
	 */
	public synchronized void reset()
	{
		initialised = false;
		//initialise(new Date());
		numberSamples = 0;
		
		
		rateSamples = new HashMap<Date, Double>();
		// countPerSecond=new HashMap<Long,Integer>();

	}

	/*
	 * public synchronized void resetSamples() { countPerSecond=new
	 * HashMap<Long,Integer>(); }
	 */

	/**
	 * Gets the current rate in events/second sampled over the sampling size
	 * 
	 * @return the rate
	 */
	public synchronized double getRate()
	{
		if(!initialised)return 0;
		int p = pointer - 1;
		if (p == -1)
			p = size - 1;
		if (sampling[p] == sampling[(p + 1) % size])
			return 0d;
		else
			return multiplier
					/ (double) Math.abs(sampling[p] - sampling[(p + 1) % size]);

	}

	/*
	 * moved to service public HashMap<Date,Integer> getSampleCount(int period)
	 * //in samplePeriods { HashMap<Long,Integer>
	 * clone=(HashMap<Long,Integer>)countPerSecond.clone();
	 * HashMap<Date,Integer> counts=new HashMap<Date,Integer>(); for(Entry<Long,
	 * Integer> s:clone.entrySet()) {
	 * //System.out.println(s.getKey()+" "+s.getValue()); Long
	 * l=s.getKey()/period*period; Date d=new Date(l*samplePeriod);
	 * 
	 * if(counts.containsKey(d)) { Integer i=counts.get(d)+s.getValue();
	 * counts.put(d, i); } else {
	 * 
	 * counts.put(d, s.getValue()); } }
	 * 
	 * return counts;
	 * 
	 * }
	 */

	public HashMap<Date, Double> getRateSamples(int period) // bucket size in
															// secs
	{
		HashMap<Date, Double> clone = ((HashMap<Date, Double>) rateSamples
				.clone());
		HashMap<Date, Double> periods = new HashMap<Date, Double>();
		HashMap<Date, Integer> counts = new HashMap<Date, Integer>();
		for (Entry<Date, Double> s : clone.entrySet())
		{
			long t = s.getKey().getTime();
			if (period > 0)
				t = (t / (period * samplePeriod)) * period * samplePeriod;
			Date d = new Date(t);
			int i;
			double mean;
			if (periods.containsKey(d))
			{
				i = counts.get(d);
				mean = periods.get(d);
			} else
			{
				i = 0;
				mean = 0;
			}
			mean = (mean * i + s.getValue().doubleValue()) / (double) (i + 1);
			i++;
			counts.put(d, i);
			periods.put(d, mean);
		}

		return periods;
	}

	/*
	 * moved to the service public HashMap<Date,Integer> getRollingCount(int
	 * period) //in samplePeriods { HashMap<Long,Integer>
	 * clone=(HashMap<Long,Integer>)countPerSecond.clone();
	 * HashMap<Date,Integer> counts=new HashMap<Date,Integer>(); for(Entry<Long,
	 * Integer> s:clone.entrySet()) { Long l=s.getKey(); long r=l; int c=0;
	 * for(int i=0;i<period;i++) { Integer j=clone.get(r); if(j!=null) c+=j;
	 * r--;
	 * 
	 * } counts.put(new Date(l*samplePeriod),c );
	 * 
	 * } return counts; }
	 */

	public HashMap<Date, Double> getRateSamples()
	{
		HashMap<Date, Double> clone = ((HashMap<Date, Double>) rateSamples
				.clone());
		return clone;
	}

	private boolean sampleCapture = true;

	public void setSampleCapture(boolean state)
	{
		sampleCapture = state;
	}

	public boolean isSampleCapture()
	{
		return sampleCapture;
	}

	public void setSamplePeriod(int period)
	{
		samplePeriod = period;
	}

	public int getSamplePeriod()
	{
		return samplePeriod;
	}

	private int samplePeriod = 60000; // in milliseconds

}
