package serviceAnalysisModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * A volumetric model allows the analysis tool to simulate usage of the modelled
 * system to drive different processes a number of times to determine the
 * overall response time of the system, e.g., Processing 200 orders for a
 * product. A product order will call a different set of workstreams to fulfil
 * the order. Each of those Workstreams will call a number of UI steps and
 * therefore through the dependency services.
 * <P>
 * Process = VolumetricType
 * <P>
 * Use Case = Workstream
 * 
 * @author apc
 * 
 */
public class VolumetricModel implements ServiceListener
{

	/**
	 * Adds a {@link VolumetricType} to the volumetric model
	 * 
	 * @param vt
	 */
	public VolumetricType addType(VolumetricType vt)
	{
		// System.out.println(container.containsKey(vt.getName()));
		VolumetricType c = contains(vt.getName());
		if (c == null)
		{
			// System.out.println("adding vtype");
			model.put(vt, 0);
			simulations.put(vt, 0d);
			container.put(vt.getName(), vt);
			c = vt;
		}
		return c;
	}

	/**
	 * Sets the volume that will be processed per unit of time for a given
	 * {@link VolumetricType}
	 * 
	 * @param vt
	 * @param vol
	 */
	public void setVolume(VolumetricType vt, int vol)
	{
		model.put(vt, vol);

	}

	/**
	 * Adds a {@link VolumetricType} and sets its volume per unit of time
	 * 
	 * @param vt
	 * @param vol
	 */
	public void addType(VolumetricType vt, int vol)
	{

		// addType(vt);
		setVolume(addType(vt), vol);
	}

	/**
	 * Does nothing but create the object
	 */
	public VolumetricModel()
	{
		super();
	}

	public void clear()
	{
		System.out.println("clearing Volumetric Model");
		container.clear();
		model.clear();
		simulations.clear();
		savedSimulations.clear();

	}

	/**
	 * Simulates the time taken to process all {@link VolumetricTypes} at their
	 * given volumes. This is achieved by calling simulate on each
	 * {@link VolumetricType}
	 */
	public void simulate()
	{
		// simTime=0;
		// VolumetricType vt=null;
		// Integer I=0;
		Iterator<Entry<VolumetricType, Integer>> iter = model.entrySet()
				.iterator();
		while (iter.hasNext())
		{
			Entry<VolumetricType, Integer> entry = iter.next();
			simulations.put(entry.getKey(),
					entry.getKey().simulate(entry.getValue()));

		}
	}

	/**
	 * Simulates the time taken to process all {@link VolumetricTypes} at their
	 * given volumes. This is achieved by calling simulate on each
	 * {@link VolumetricType}. The results are then stored internally for later
	 * retrieval
	 */
	public void simulateSave()
	{
		// simTime=0;
		// VolumetricType vt=null;
		// Integer I=0;
		Iterator<Entry<VolumetricType, Integer>> iter = model.entrySet()
				.iterator();
		while (iter.hasNext())
		{
			Entry<VolumetricType, Integer> entry = iter.next();
			double d = entry.getKey().simulate(entry.getValue());
			simulations.put(entry.getKey(), d);
			savedSimulations.put(entry.getKey(), d);

		}
	}

	/**
	 * Returns the VolumetricType with the given name or null
	 * 
	 * @param name
	 * @return
	 */
	public VolumetricType contains(String name)
	{
		// System.out.println("contains="+container.get(name));
		if (container.containsKey(name))
		{
			return container.get(name);
		} else
			return null;
	}

	// private double simTime=0;
	public Map<VolumetricType, Integer> model = new HashMap<VolumetricType, Integer>();
	public Map<VolumetricType, Double> simulations = new HashMap<VolumetricType, Double>();
	public Map<VolumetricType, Double> savedSimulations = new HashMap<VolumetricType, Double>();

	private Map<String, VolumetricType> container = new HashMap<String, VolumetricType>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * serviceAnalysisModel.ServiceListener#handleTimeChange(serviceAnalysisModel
	 * .Service)
	 * 
	 * Causes a simulation recalculation
	 */
	@Override
	public void handleTimeChange(Service s)
	{
		// TODO Auto-generated method stub
		this.simulate();
	}

	public void addVolModelListener(VolModelListener listener)
	{
		volListeners.add(listener);
	}

	public void removeVolModelListener(VolModelListener listener)
	{
		volListeners.remove(listener);
	}

	public void fireVolModelChanged()
	{
		for (VolModelListener listener : volListeners)
			listener.fireModelDataChanged();
	}

	private ArrayList<VolModelListener> volListeners = new ArrayList<VolModelListener>();
}
