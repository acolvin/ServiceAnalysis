package serviceAnalysisModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RunningAverage implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 367055604782986739L;
	private List<Double> points =Collections.synchronizedList(new ArrayList<Double>());
	private int runLength=1;
	
	private double currentSum=0;
	private int currentLength=0;
	
	

	
	public RunningAverage(int runLength)
	{
		if(runLength>0)this.runLength=runLength;
	}
	
	
	public void clear()
	{
		points.clear();
		currentSum=0;
		currentLength=0;
	}
	
	public double addPoint(double d)
	{
		double average=0;
		
		if(points.size()==runLength) synchronized(points)
		{
			currentSum-=points.get(0);
			currentSum+=d;
			points.remove(0);
			points.add(d);
			average=currentSum/runLength;
		}
		else 
		{
			currentSum+=d;
			points.add(d);
			currentLength=points.size();
			average=currentSum/currentLength;
		}
		return average;
	}
	
	public double getRunningAverage()
	{
		return currentSum/currentLength;
	}
}
