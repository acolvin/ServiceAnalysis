package serviceAnalysisModel.readersWriters;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.zip.GZIPInputStream;

import networkAgent.listener.ListenerEvent;
import networkAgent.listener.ListenerTCP;
import networkAgent.listener.listenerJMS;
import networkAgent.listener.listenerUDP;

import org.jfree.util.HashNMap;

import database.DBController;
import database.DBTransaction;
import database.snapshot.DeleteSnapshot;
import serviceAnalysisModel.Service;
import serviceAnalysisModel.VolumetricModel;
import serviceAnalysisModel.VolumetricType;
import serviceAnalysisModel.Workstream;
import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisModel.Transaction.EventCollection;
import serviceAnalysisUI.MainForm;
import ServiceAnalysisClient.SAEvent;

/**
 * This Class reads a file and adds the data to the relevant service operation.
 * It is currently hardcoded to handle BW Stats Files and AMS_PERF Logging lines
 * 
 * @author apc
 * 
 */
public class BWReader implements LogFileTailerListener, ListenerEvent
{

	/**
	 * This constructor provides a method to instantiating an object with the
	 * file to be processed and the current list of services. The reading starts
	 * at the last line and will tail the file using the {@link LogFileReader}.
	 * <p>
	 * This class also writes service envents to the database of the appropriate
	 * option is provided <b>--DBEvents</b> on the command line.
	 * 
	 * @param filename
	 *            the absolute file system path of the file to read
	 * @param serviceOperation
	 *            A list of current {@link serviceAnalysisModel.Service} objects
	 */
	public BWReader(String filename, List<Service> serviceOperation)
	{
		this.filename = filename;

		for (int i = 0; i < 1; i++)
		{
			DBWriter d = new DBWriter(this, queue);
			new Thread(d).start();
			writerPool.add(d);
		}
		LogFileReader lreader = new LogFileReader(filename);
		lreader.addLogFileTailerListener(this);
		this.serviceOperation = serviceOperation;
		readers.put(filename, lreader);
		lreader.start();

	}

	private boolean removeReaderAtEOF = false;

	/**
	 * This constructor causes the file to be read from the beginning of the
	 * file. The reader is removed at the end of the file.
	 * 
	 * @param filename
	 *            the absolute file system path of the file to read
	 * @param serviceOperation
	 *            A list of current {@link serviceAnalysisModel.Service} objects
	 * @param readAll
	 *            A flag to tell the object whether to read from the beginning
	 *            of the file or the end
	 */
	public BWReader(String filename, List<Service> serviceOperation,
			boolean readAll)
	{
		this.filename = filename;
		if (readAll)
			removeReaderAtEOF = true;

		for (int i = 0; i < 1; i++)
		{
			DBWriter d = new DBWriter(this, queue);
			new Thread(d).start();
			writerPool.add(d);
		}
		LogFileReader lreader = new LogFileReader(filename, readAll);
		lreader.addLogFileTailerListener(this);
		this.serviceOperation = serviceOperation;
		readers.put(filename, lreader);
		lreader.start();

	}

	private final boolean notify = false;
	public ConcurrentLinkedQueue<ServiceEntry> networkReads = new ConcurrentLinkedQueue<ServiceEntry>();
	private ConcurrentLinkedQueue<ServiceEntry> queue = new ConcurrentLinkedQueue<ServiceEntry>();
	private ArrayList<DBWriter> writerPool = new ArrayList<DBWriter>();

	/**
	 * This method removes a {@link DBWriter} from the pool
	 * 
	 * @param dbw
	 *            The database writer object
	 */
	public void RemoveDBWriter(DBWriter dbw)
	{
		writerPool.remove(dbw);
		// System.out.println("removing writer");
	}

	/**
	 * This method write the service entry line to the database. The
	 * <b>--DBEvents</b> command line options must be set on startup
	 * 
	 * @param se
	 *            The {@link ServiceEntry} object
	 */
	public void DBWrite(ServiceEntry se)
	{
		int size;
		if ((size = queue.size()) > writerPool.size() * 10
				&& writerPool.size() < 100)
		{// size the pool automatically
			DBWriter d = new DBWriter(this, queue);
			new Thread(d).start();
			writerPool.add(d);
			// System.out.println("writer added");

		}
		// System.out.println("queue size: "+size);
		if (se.type == ServiceEntry.SE_TYPE_EVENT
				&& serviceAnalysisModel.readersWriters.DBWriter.DBWriteServiceEvents)
			try
			{

				if (DBWriter.DBCheckEvent)
				{
					Connection con = DriverManager.getConnection(
							Controller.DBConnectionStr,
							Controller.DBUser,
							Controller.DBPasswd);
					Statement statement = con.createStatement();
					statement
							.execute("select 1 from SERVICE_EVENT where SERVICE='"
									+ se.service.getsName()
									+ "' and EVENT_TIME="
									+ se.when.getTime()
									+ " and RESPONSE=" + se.response);
					if (!statement.getResultSet().next())
						queue.offer(se);
					con.close();
				} else
					queue.offer(se);
			} catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else if (se.type == ServiceEntry.SE_TYPE_SAMPLE)
			queue.offer(se);
		else if (se.type == ServiceEntry.SE_TYPE_SUMMARY)
			queue.offer(se);
		else if (se.type == ServiceEntry.SE_TYPE_HIERARCHY)
			queue.offer(se);
		else if (se.type == ServiceEntry.SE_TYPE_SNAP_VOL)
			queue.offer(se);
		else if (se.type == ServiceEntry.SE_TYPE_SNAP_VOL_WS)
			queue.offer(se);
		else if (se.type == ServiceEntry.SE_TYPE_SNAPSHOT_SAVE)
			queue.offer(se);
		else if (se.type == ServiceEntry.SE_TYPE_SAMPLE_COUNT)
			queue.offer(se);
		else if (se.type == -1)
			queue.offer(se);
	}

	/**
	 * This method creates a BWReader without a file to read initially
	 * 
	 * @param serviceOperation
	 *            A list of {@link Service} objects that will be adjusted and
	 *            new services added
	 */
	public BWReader(List<Service> serviceOperation)
	{
		this.serviceOperation = serviceOperation;

		for (int i = 0; i < 1; i++)
		{

			DBWriter d = new DBWriter(this, queue);
			new Thread(d).start();
			writerPool.add(d);
			seqp = new ServiceEntryQueuePoller(this, networkReads);
			seqp.start();
		}

	}

	private ServiceEntryQueuePoller seqp = null;

	private HashMap<Integer, listenerUDP> UDPlisteners = new HashMap<Integer, listenerUDP>();

	public void startUDPListener(int port) throws SocketException
	{
		// networkReads=new ConcurrentLinkedQueue<ServiceEntry>();
		listenerUDP lUDP;

		lUDP = new listenerUDP(networkReads, port);
		UDPlisteners.put(port, lUDP);
		lUDP.start();

	}

	public void stopUDPListener(int port)
	{
		listenerUDP lUDP = UDPlisteners.get(port);
		if (lUDP != null)
			lUDP.stop = true;
		Controller.getController().sendMessage("Stopped UDP Listener");

	}
	
	public void startTCPClient(String host,int port)
	{
		String key=host+":"+port;
		if(TCPClients.containsKey(key))
		{
			stopTCPClient(host,port);
		}
		try {
			ListenerTCP t=new ListenerTCP(networkReads,host,port);
			TCPClients.put(key, t);
			t.start();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			Controller.getController().sendMessage("Host not Found, failed to connect to TCP service");
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Controller.getController().sendMessage(e.getMessage());
			Controller.getController().sendMessage("Could not connect to TCP service");

			//e.printStackTrace();
		}
	}
	
	public List<ListenerTCP> getTCPClientList(){
		ArrayList<ListenerTCP> al = new ArrayList<ListenerTCP>();
		for(ListenerTCP s:TCPClients.values())al.add(s);
		return al;
	}
	
	public void sendTCPCommand(String host,int port,String command)
	{
		String key=host+":"+port;
		ListenerTCP t=TCPClients.get(key);
		if(t!=null)t.sendcommand(command);
	}
	
	public char[] getTCPEncryptedMessage(String host,int port,String message){
		String key=host+":"+port;
		ListenerTCP t=TCPClients.get(key);
		if(t==null)return null;
		return t.buildBase64EncMessage(message);
	}
	
	public void stopTCPClient(String host,int port)
	{
		String key=host+":"+port;
		stopTCPClient(key);
		
	}
	
	private void stopTCPClient(String key)
	{
		ListenerTCP t=TCPClients.get(key);
		if(t!=null){
			t.interrupt();
			TCPClients.remove(key);
		}
	}
	
	public void stopAllTCPClients()
	{
		HashMap<String,ListenerTCP> c=(HashMap<String,ListenerTCP>)TCPClients.clone();
		for(String key:c.keySet()) stopTCPClient(key);
	}
	
	private HashMap<String,ListenerTCP> TCPClients=new HashMap<String,ListenerTCP>();

	listenerJMS lJMS = null;

	public void startJMSListener()
	{
		if (lJMS == null)
		{
			lJMS = new listenerJMS(networkReads);
			lJMS.registerListener(this);
			lJMS.start();
		}
	}

	public void stopJMSListener()
	{
		if (lJMS != null)
			lJMS.stop = true;
		lJMS = null;
	}

	/**
	 * This method pauses all file readers from tailing their files
	 */
	public void pauseReaders()
	{
		for (LogFileReader l : readers.values())
		{
			l.pauseTailing();
		}

		if (seqp != null)
			seqp.pause = true;
	}

	/**
	 * This method restarts all paused file reader
	 */
	public void restartReaders()
	{
		for (LogFileReader l : readers.values())
		{
			l.restartTailing();
		}
		seqp.pause = false;
	}

	/**
	 * This method creates a reader and reads the file specified from the
	 * beginning and then tails the file until it is removed.
	 * 
	 * @param filename
	 *            The full pathname of the file on the file system
	 */
	public void readAllFile(String filename)
	{
		if(BWReader.isGZipped(new File(filename))){
			try {
				PipeReader pr = new PipeReader(new FileInputStream(new File(filename)));
				pr.addListener(this);
				pr.filename=filename;
				pr.poll=false;
				pr.start();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			LogFileReader lreader = new LogFileReader(filename, true);
			lreader.addLogFileTailerListener(this);
			readers.put(filename, lreader);
			lreader.start();
		}
	}
	
	public static boolean isGZipped(File file) {   
	     RandomAccessFile raf;
		try {
			raf = new RandomAccessFile(file, "r");
			boolean b= GZIPInputStream.GZIP_MAGIC == (raf.read() & 0xff | ((raf.read() << 8) & 0xff00));
			raf.close();
			return b;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	    
	}
	
	/**
	 * This method creates a reader and reads the file specified from the end
	 * and then tails the file until it is removed.
	 * 
	 * @param filename
	 *            The full pathname of the file on the file system
	 */
	public boolean addReader(String filename)
	{
		if(readers.containsKey(filename)) return false;
		LogFileReader lreaderl = new LogFileReader(filename);
		lreaderl.addLogFileTailerListener(this);
		readers.put(filename, lreaderl);
		lreaderl.start();
		return true;
	}

	/**
	 * This method removes a reader that is currently processing the specified
	 * log file
	 * 
	 * @param filename
	 *            The full pathname of the file on the file system
	 */
	public boolean removeReader(String filename)
	{
		if (readers.containsKey(filename))
		{
			LogFileReader lfr = readers.get(filename);
			System.out.println("stop reader");
			readers.remove(filename);
			lfr.stopTailing();
			return true;
			// lfr.removeLogFileTailerListener(this);
		}
		return false;
	}
	
	public void removeAllReaders()
	{
		for(LogFileReader lfr:readers.values()) lfr.stopTailing();
		//for(LogFileReader lfr:readers.values())
		//{
		//	if(lfr.getState()!=Thread.State.TERMINATED){System.out.println("terminating reader");lfr.interrupt();}
		//}
		readers.clear();
	}

	public List<Service> getServices()
	{
		return serviceOperation;
	}

	private String filename;
	//private LogFileReader lreader;
	private List<Service> serviceOperation;
	private VolumetricModel vm;
	private HashNMap listeners = new HashNMap();
	private ConcurrentHashMap<String, LogFileReader> readers = new ConcurrentHashMap<String, LogFileReader>();

	// private LinkedBlockingQueue<Connection> connectionFreePool=new
	// LinkedBlockingQueue<Connection>();

	public void setVolumetricModel(VolumetricModel vm)
	{
		this.vm = vm;
	}
	
	public void clearVolumetricModel()
	{
		if(this.vm!=null) vm.clear();
		
	}

	/**
	 * @param s
	 *            The {@link Service} that is to be listened to
	 * @param l
	 */
	public void addListener(Service s, BWStatsEventListener l)
	{
		synchronized (listeners)
		{
			listeners.add(s, l);
		}
	}

	/**
	 * @param s
	 * @param l
	 */
	public void removeListener(Service s, BWStatsEventListener l)
	{
		synchronized (listeners)
		{
			listeners.remove(s, l);
		}
	}

	/**
	 * @return
	 */
	public Map<Date, String> getSnapshots()
	{
		HashMap<Date, String> snapshots = new HashMap<Date, String>();
		// query the database
		// String query="select distinct SNAPSHOTTIMESTAMP FROM SERVICE";
		String query = "select SNAPSHOTTIMESTAMP, SNAPSHOTDESC from SNAPSHOTS";
		Connection con = null;
		Statement statement = null;
		try
		{
			con = DriverManager.getConnection(
					serviceAnalysisModel.SAControl.Controller.DBConnectionStr,
					serviceAnalysisModel.SAControl.Controller.DBUser,
					serviceAnalysisModel.SAControl.Controller.DBPasswd);
			statement = con.createStatement();
			statement.execute(query);
		} catch (SQLException e)
		{
				DBController.DBC.start();
				if( DBController.isRunning())
				{
					try{
						con = DriverManager.getConnection(
								serviceAnalysisModel.SAControl.Controller.DBConnectionStr,
								serviceAnalysisModel.SAControl.Controller.DBUser,
								serviceAnalysisModel.SAControl.Controller.DBPasswd);
						statement = con.createStatement();
						statement.execute(query);
					}catch(SQLException se)
					{
						se.printStackTrace();
					}
				}else e.printStackTrace();
			
		}
		// iterate through record set and add to the list
		try
		{
			ResultSet rs = statement.getResultSet();
			while (rs.next())
			{
				long l = rs.getLong("SNAPSHOTTIMESTAMP");
				Date d = new Date(l);
				String s = rs.getString("SNAPSHOTDESC");
				// System.out.println("snapshot date "+d);
				snapshots.put(d, s);
			}

			con.close();
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return snapshots;
	}
	
	public void deleteSnapshot(Date date)
	{
		DeleteSnapshot dsnap=new DeleteSnapshot();
		dsnap.setSnapshot(date);
		DBTransaction dbt=new DBTransaction();
		dbt.addDBAction(dsnap);
		try {
			dbt.execute();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	/**
	 * Loads a snapshot from the database
	 * 
	 * @param date
	 *            snapshot identifier
	 */
	public void loadSnapShot(Date date)
	{
		long snapshot = date.getTime();
		System.out.println("in load snapshot, service size ="+serviceOperation.size() );
		
		Controller.getController().sendMessage("Loading Snapshot Services");
		loadSnapShotServices(date);
		
		System.out.println("in load snapshot, service size ="+serviceOperation.size() );


		String query = "select LAYER,SERVICE,RESPONSE,QTY,WAIT FROM SERVICE_SNAPSHOT WHERE SNAPSHOTTIMESTAMP="
				+ snapshot;
		Connection con = null;
		Statement statement = null;
		try
		{
			try{
				con = DriverManager.getConnection(
					serviceAnalysisModel.SAControl.Controller.DBConnectionStr,
					serviceAnalysisModel.SAControl.Controller.DBUser,
					serviceAnalysisModel.SAControl.Controller.DBPasswd);
				
			}catch (SQLException se)
			{
				DBController.DBC.start();
				if( DBController.isRunning())
				{
					con = DriverManager.getConnection(
						serviceAnalysisModel.SAControl.Controller.DBConnectionStr,
						serviceAnalysisModel.SAControl.Controller.DBUser,
						serviceAnalysisModel.SAControl.Controller.DBPasswd);
					
				}else throw se;
			}
			statement = con.createStatement();
			statement.execute(query);
			ResultSet rs = statement.getResultSet();
			while (rs.next())
			{
				String layer=rs.getString("LAYER");
				String s = rs.getString("SERVICE");
				String[] names = s.split("\\.\\.\\.");
				double response = rs.getDouble("RESPONSE");
				float r = (float) response + 0.5f;
				double qty = rs.getDouble("QTY");
				int q = (int) qty;
				double waitd = rs.getDouble("WAIT");
				float wait = (float) waitd;
				//System.out.println("in load snapshot, service size ="+serviceOperation.size() );
				int index = serviceOperation.indexOf(new Service(layer,
						names[0], names[1], ""));
				if (index == -1)
				{
					System.out.println("Adding service "+names[0]+"."+names[1]);
					Service service = new Service(layer, names[0], names[1], "");
					serviceOperation.add(service);
					controller.fireServiceAddedEvents(service);
					// if(serviceAnalysisUI.ServiceTable.model!=null){
					// service.addServiceListener(serviceAnalysisUI.ServiceTable.model);
					// serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();
					// }
				}
				Service service = serviceOperation.get(serviceOperation
						.indexOf(new Service(layer, names[0], names[1], "")));
				for (int i = 0; i < q; i++)
				{
					service.addTime(r);
					service.addWaitTime(wait);
				}

			}

		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Controller.getController().sendMessage("Loading Adjustments");
		loadSnapShotAdjustments(date);
		Controller.getController().sendMessage("Loading Volumetrics");
		loadSnapShotVolumetrics(date);
		loadSnapShotVolumetricsWS(date);
		Controller.getController().sendMessage("Loading Login Data");
		loadSnapShotLogin(date);
		System.out.println("at end of load snapshot, service size ="+serviceOperation.size() );
		Controller.getController().sendMessage("Loaded snapshot with "+serviceOperation.size()+" Services");
	}

	private void loadSnapShotVolumetrics(Date date)
	{
		System.out.println("load vol");
		long snapshot = date.getTime();
		String query = "select VOLUMETRIC,QTY FROM VOLUMETRICS WHERE SNAPSHOTTIMESTAMP="
				+ snapshot;
		Connection con = null;
		Statement statement = null;
		try
		{
			con = DriverManager.getConnection(
					serviceAnalysisModel.SAControl.Controller.DBConnectionStr,
					serviceAnalysisModel.SAControl.Controller.DBUser,
					serviceAnalysisModel.SAControl.Controller.DBPasswd);
			statement = con.createStatement();
			statement.execute(query);
			ResultSet rs = statement.getResultSet();
			while (rs.next())
			{

				String vn = rs.getString("VOLUMETRIC");
				int qty = rs.getInt("QTY");
				// System.out.println("adding volumetric type "+vn+" "+qty);
				VolumetricType vt = new VolumetricType(vn);
				// vm=serviceAnalysisUI.ModelForm._VM;
				vm.addType(vt);
				vt = vm.contains(vn);
				vm.setVolume(vt, qty);
				vm.simulate();
				vm.simulateSave();
				vm.fireVolModelChanged();
				// if(serviceAnalysisUI.ServiceTable.model!=null){
				// MainForm.MF.newContentPane.model.updateTypes();
				// MainForm.MF.newContentPane.model.fireTableDataChanged();
				// }
			}

		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void loadSnapShotVolumetricsWS(Date date)
	{
		System.out.println("load vol WS");
		long snapshot = date.getTime();
		String query = "select VOLUMETRIC,WORKSTREAM,ITERATION FROM VOLUMETRICS_HIER WHERE SNAPSHOTTIMESTAMP="
				+ snapshot;
		Connection con = null;
		Statement statement = null;
		try
		{
			con = DriverManager.getConnection(
					serviceAnalysisModel.SAControl.Controller.DBConnectionStr,
					serviceAnalysisModel.SAControl.Controller.DBUser,
					serviceAnalysisModel.SAControl.Controller.DBPasswd);
			statement = con.createStatement();
			statement.execute(query);
			ResultSet rs = statement.getResultSet();
			while (rs.next())
			{
				String vn = rs.getString("VOLUMETRIC");
				String wsn = rs.getString("WORKSTREAM");
				double qty = rs.getDouble("ITERATION");
				// System.out.println("adding volumetric type/ws "+vn+" "+wsn+" "+qty);
				VolumetricType vt = new VolumetricType(vn);
				// vm=serviceAnalysisUI.ModelForm._VM;
				vm.addType(vt);
				vt = vm.contains(vn);

				Workstream ws = new Workstream(wsn);
				int index = serviceOperation.indexOf(ws);
				if (index == -1)
				{
					serviceOperation.add(ws);
					controller.fireServiceAddedEvents(ws);
					// if(serviceAnalysisUI.ServiceTable.model!=null){
					// ws.addServiceListener(serviceAnalysisUI.ServiceTable.model);
					// serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();
					// }
				}
				ws = (Workstream) serviceOperation.get(index);

				vt.addWorkstream(ws);
				vt.setWorkstreamMultiplier(ws, qty);

				vm.simulate();
				vm.simulateSave();
				vm.fireVolModelChanged();
				// if(serviceAnalysisUI.ServiceTable.model!=null){
				// MainForm.MF.newContentPane.model.updateTypes();
				// MainForm.MF.calculate();
				// }
			}

		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void loadSnapShotAdjustments(Date date)
	{
		long snapshot = date.getTime();
		String query = "select SERVICE_NAME,SERVICE_OPERATION,SERVICE_LAYER,SERVICE_ADJ FROM SERVICE WHERE SNAPSHOTTIMESTAMP="
				+ snapshot;
		Connection con = null;
		Statement statement = null;
		try
		{
			con = DriverManager.getConnection(
					serviceAnalysisModel.SAControl.Controller.DBConnectionStr,
					serviceAnalysisModel.SAControl.Controller.DBUser,
					serviceAnalysisModel.SAControl.Controller.DBPasswd);
			statement = con.createStatement();
			statement.execute(query);
			ResultSet rs = statement.getResultSet();
			while (rs.next())
			{
				String sn = rs.getString("SERVICE_NAME");
				String so = rs.getString("SERVICE_OPERATION");
				String sl = rs.getString("SERVICE_LAYER");
				double adj = rs.getDouble("SERVICE_ADJ");
				// double response=rs.getDouble("RESPONSE");
				// float r=(float)response+2.5f;
				// double qty=rs.getDouble("QTY");
				// int q=(int)qty;
				if (adj != 0)
				{
					Service service;
					int index = serviceOperation.indexOf(new Service(sl, sn,
							so, ""));
					if (index == -1)
					{
						service = new Service(sl, sn, so, "");
						serviceOperation.add(service);
						controller.fireServiceAddedEvents(service);
						// if(serviceAnalysisUI.ServiceTable.model!=null)
						// service.addServiceListener(serviceAnalysisUI.ServiceTable.model);

					} else
					{
						service = serviceOperation.get(index);
					}
					service.setAdjustments((float) (adj));
					controller.fireServicesDataChanged();
					// if(serviceAnalysisUI.ServiceTable.model!=null)
					// serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();
				}
				// Service service =
				// serviceOperation.get(serviceOperation.indexOf(new
				// Service(s)));
				// for(int i=0;i<q;i++)service.addTime(r);
			}

		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private void loadSnapShotLogin(Date date){
		long snapshot = date.getTime();
		//String query1="select USERNAME from LOGGEDIN WHERE SNAPSHOTTIMESTAMP="+snapshot;
		//String query2="select TIMESTAMP,COUNT from LOGINS WHERE SNAPSHOTTIMESTAMP="+snapshot;
		//String query3="select USERNAME,TIMESTAMP from AUTOLOGOUTS WHERE SNAPSHOTTIMESTAMP="+snapshot;
		String query4="select USERNAME, STARTTIME, ENDTIME from LOGIN_EVENTS WHERE SNAPSHOTTIMESTAMP="+snapshot;
		Connection con = null;
		Statement statement = null;
		try
		{
			con = DriverManager.getConnection(
					serviceAnalysisModel.SAControl.Controller.DBConnectionStr,
					serviceAnalysisModel.SAControl.Controller.DBUser,
					serviceAnalysisModel.SAControl.Controller.DBPasswd);

			statement = con.createStatement();
/*
			statement.execute(query1);
			ResultSet rs1 = statement.getResultSet();
			
			
			HashSet<String> log_in = new HashSet<String>();
			HashMap<String,Long> autologs=new HashMap<String,Long>();
			TreeMap<Long,Integer> u_counts=new TreeMap<Long,Integer>();
			
			while(rs1.next()){//log_in
				String u=rs1.getString("USERNAME");
				log_in.add(u);
				
			}
			
			statement.execute(query2);
			ResultSet rs2 = statement.getResultSet();
			
			while(rs2.next()){//u_counts
				Long ts=rs2.getLong("TIMESTAMP");
				Integer c=rs2.getInt("COUNT");
				u_counts.put(ts, c);
			}
			
			statement.execute(query3);
			ResultSet rs3 = statement.getResultSet();
			while(rs3.next()){//autologs
				String u=rs3.getString("USERNAME");
				Long ts=rs3.getLong("TIMESTAMP");
				autologs.put(u, ts);
			}
			//System.out.println("log_in size="+log_in.size());
			//System.out.println("u_counts size="+u_counts.size());
			//System.out.println("autologs size="+autologs.size());
			setLoggedIn(log_in);
			setLogins(u_counts);
			setAutoLogouts(autologs);
*/			
			statement.execute(query4);
			ResultSet rs4 = statement.getResultSet();
			while(rs4.next()){
				String u=rs4.getString("USERNAME");
				long start=rs4.getLong("STARTTIME");
				long end=rs4.getLong("ENDTIME");
				profile.addLoginEvent(u, start,end);
			}
			
		}catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void loadSnapShotServices(Date date)
	{
		long snapshot = date.getTime();
		String query = "select SERVICE_NAME,SERVICE_OPERATION,SERVICE_LAYER FROM SERVICE WHERE SNAPSHOTTIMESTAMP="
				+ snapshot;
		Connection con = null;
		Statement statement = null;
		try
		{
			con = DriverManager.getConnection(
					serviceAnalysisModel.SAControl.Controller.DBConnectionStr,
					serviceAnalysisModel.SAControl.Controller.DBUser,
					serviceAnalysisModel.SAControl.Controller.DBPasswd);
			statement = con.createStatement();
			statement.execute(query);
			ResultSet rs = statement.getResultSet();
			while (rs.next())
			{
				String sn = rs.getString("SERVICE_NAME");
				String so = rs.getString("SERVICE_OPERATION");
				String sl = rs.getString("SERVICE_LAYER");
				// double response=rs.getDouble("RESPONSE");
				// float r=(float)response+2.5f;
				// double qty=rs.getDouble("QTY");
				// int q=(int)qty;
				int index = serviceOperation
						.indexOf(new Service(sl, sn, so, ""));
				if (index == -1 && !sl.equalsIgnoreCase("WS"))
				{
					Service service = new Service(sl, sn, so, "");
					serviceOperation.add(service);
					controller.fireServiceAddedEvents(service);
					// if(serviceAnalysisUI.ServiceTable.model!=null){
					// service.addServiceListener(serviceAnalysisUI.ServiceTable.model);
					// serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();
					// }
				} else if (index == -1 && sl.equalsIgnoreCase("WS"))
				{
					Workstream service = new Workstream(so);
					serviceOperation.add(service);
					controller.fireServiceAddedEvents(service);
					// if(serviceAnalysisUI.ServiceTable.model!=null){
					// service.addServiceListener(serviceAnalysisUI.ServiceTable.model);
					// serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();
					// }
				}
				// Service service =
				// serviceOperation.get(serviceOperation.indexOf(new
				// Service(s)));
				// for(int i=0;i<q;i++)service.addTime(r);
			}

		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		loadSnapShotServicesHier(date);
		loadSnapshotSampleCount(date);
		System.out.println("Service size at end of service load "+serviceOperation.size());
	}

	private void loadSnapShotServicesHier(Date date)
	{
		long snapshot = date.getTime();
		String query = "select PARENT_SERVICE_NAME,PARENT_SERVICE_OPERATION,PARENT_SERVICE_LAYER,CHILD_SERVICE_NAME,CHILD_SERVICE_OPERATION,CHILD_SERVICE_LAYER FROM SERVICE_HIER WHERE SNAPSHOTTIMESTAMP="
				+ snapshot;
		Connection con = null;
		Statement statement = null;
		try
		{
			con = DriverManager.getConnection(
					serviceAnalysisModel.SAControl.Controller.DBConnectionStr,
					serviceAnalysisModel.SAControl.Controller.DBUser,
					serviceAnalysisModel.SAControl.Controller.DBPasswd);
			statement = con.createStatement();
			statement.execute(query);
			ResultSet rs = statement.getResultSet();
			while (rs.next())
			{
				String psn = rs.getString("PARENT_SERVICE_NAME");
				String pso = rs.getString("PARENT_SERVICE_OPERATION");
				String psl = rs.getString("PARENT_SERVICE_LAYER");
				String csn = rs.getString("CHILD_SERVICE_NAME");
				String cso = rs.getString("CHILD_SERVICE_OPERATION");
				String csl = rs.getString("CHILD_SERVICE_LAYER");

				// double response=rs.getDouble("RESPONSE");
				// float r=(float)response+2.5f;
				// double qty=rs.getDouble("QTY");
				// int q=(int)qty;
				Service pService, cService;
				int index = serviceOperation.indexOf(new Service(csl, csn, cso,
						""));
				if (index == -1)
				{
					cService = new Service(csl, csn, cso, "");
					serviceOperation.add(cService);
					controller.fireServiceAddedEvents(cService);
					// if(serviceAnalysisUI.ServiceTable.model!=null){
					// cService.addServiceListener(serviceAnalysisUI.ServiceTable.model);
					// serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();
					// }
				} else
				{
					cService = serviceOperation.get(index);
				}
				index = serviceOperation
						.indexOf(new Service(psl, psn, pso, ""));
				if (index == -1)
				{
					pService = new Service(psl, psn, pso, "");
					serviceOperation.add(pService);
					controller.fireServiceAddedEvents(pService);
					// if(serviceAnalysisUI.ServiceTable.model!=null){
					// pService.addServiceListener(serviceAnalysisUI.ServiceTable.model);
					// serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();
					// }
				} else
				{
					pService = serviceOperation.get(index);
				}
				pService.addDependentService(cService);
				// Service service =
				// serviceOperation.get(serviceOperation.indexOf(new
				// Service(s)));
				// for(int i=0;i<q;i++)service.addTime(r);
			}

		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void loadSnapshotSampleCount(Date date)
	{
		class Store
		{
			Store()
			{
				counts=new HashMap<Long,Integer>();
				sums=new HashMap<Long,Double>();
				ssq=new HashMap<Long,Double>();
				mins=new HashMap<Long,Double>();
				maxs=new HashMap<Long,Double>();
			}
			HashMap<Long,Integer> counts;
			HashMap<Long,Double> sums,mins,maxs,ssq;
			int size=60000;
		}
		long snapshot = date.getTime();
		String sizequery = "select SNAPSHOTTIMESTAMP,SERVICE_NAME,SERVICE_OPERATION,SERVICE_LAYER,SAMPLESIZE From SAMPLESIZE WHERE SNAPSHOTTIMESTAMP="
				+ snapshot;
		String countquery ="select SNAPSHOTTIMESTAMP, SERVICE_NAME, SERVICE_OPERATION, SERVICE_LAYER, SAMPLE, COUNT,SSUM,SMIN,SMAX,SSQ from samplecounts WHERE SNAPSHOTTIMESTAMP="
				+snapshot;
		Connection con = null;
		Statement statement = null;
		//HashMap<Service,Integer> sizes=new HashMap<Service,Integer>();
		HashMap<Service,Store> counts=new HashMap<Service,Store>();
		try
		{
			con = DriverManager.getConnection(
					serviceAnalysisModel.SAControl.Controller.DBConnectionStr,
					serviceAnalysisModel.SAControl.Controller.DBUser,
					serviceAnalysisModel.SAControl.Controller.DBPasswd);
			statement = con.createStatement();
			statement.execute(sizequery);
			
			ResultSet rs = statement.getResultSet();
			while (rs.next())
			{
				String sn = rs.getString("SERVICE_NAME");
				String so = rs.getString("SERVICE_OPERATION");
				String st = rs.getString("SERVICE_LAYER");
				int size= rs.getInt("SAMPLESIZE");
				Service s=new Service(st,sn,so,st);
				Store store=new Store();
				store.size=size;
				counts.put(s, store);
			}
			statement.execute(countquery);
			
			rs = statement.getResultSet();
			while (rs.next())
			{
				String sn = rs.getString("SERVICE_NAME");
				String so = rs.getString("SERVICE_OPERATION");
				String st = rs.getString("SERVICE_LAYER");
				Service s=new Service(st,sn,so,st);
				Long sample=rs.getLong("SAMPLE");
				Integer count=rs.getInt("COUNT");
				Double sum=rs.getDouble("SSUM");
				Double min=rs.getDouble("SMIN");
				Double max=rs.getDouble("SMAX");
				Double ssq=rs.getDouble("SSQ");
				Store store=counts.get(s);
				store.counts.put(sample, count);
				if(sum!=null)store.sums.put(sample, sum);
				if(min!=null)store.mins.put(sample, min);
				if(max!=null)store.maxs.put(sample, max);
				if(ssq!=null)store.ssq.put(sample, ssq);
				
			}
			
			for(Service s:counts.keySet())
			{
				int i=serviceOperation.indexOf(s);
				Service reals;
				Store store=counts.get(s);
				if(i==-1)
				{		
					System.out.println("not found service while adding counts! "+s);
					serviceOperation.add(s);
					reals=s;
					controller.fireServiceAddedEvents(s);					
				}
				else reals=serviceOperation.get(i);
				reals.setSamplePeriod(store.size);
				reals.setCountPerSecond(store.counts);
				reals.setSumsPerSecond(store.sums);
				reals.setSumSqPerSecond(store.ssq);
				reals.setMaxPerSecond(store.maxs);
				reals.setMinPerSecond(store.mins);
				
			}
		}catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void addChildService(Service pService, Service cService)
	{
		int index = serviceOperation.indexOf(cService);
		if (index == -1)
		{

			serviceOperation.add(cService);
			controller.fireServiceAddedEvents(cService);
			// if(serviceAnalysisUI.ServiceTable.model!=null){
			// cService.addServiceListener(serviceAnalysisUI.ServiceTable.model);
			// serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();
			// }
		} else
		{
			cService = serviceOperation.get(index);
		}
		System.out.println("pService:" + pService);
		index = serviceOperation.indexOf(pService);
		if (index == -1)
		{

			serviceOperation.add(pService);
			controller.fireServiceAddedEvents(pService);
			// if(serviceAnalysisUI.ServiceTable.model!=null){
			// pService.addServiceListener(serviceAnalysisUI.ServiceTable.model);
			// serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();
			// }
		} else
		{
			pService = serviceOperation.get(index);
		}
		pService.addDependentService(cService);

	}

	private long fromfilter = 0;
	private long tofilter = Long.MAX_VALUE;

	public void setFromFilter(Date date)
	{
		fromfilter = date.getTime();
		System.out.println(date.getTime());
	}

	public void setToFilter(Date date)
	{
		tofilter = date.getTime();
	}

	public void setFromFilter(long date)
	{
		fromfilter = date;
	}

	public void setToFilter(long date)
	{
		tofilter = date;
	}

	public Date getFromFilter()
	{
		return new Date(fromfilter);
	}

	public Date getToFilter()
	{
		return new Date(tofilter);
	}

	public void processServiceEntry(ServiceEntry se)
	{
		//System.out.println("se type:"+se.type);
		//System.out.println("se service:"+se.service);
		Service consumer = null, service = null;

		if (se != null && se.type == ServiceEntry.SE_TYPE_SET)
		{
			// System.out.println("recursion");
			for (ServiceEntry s : se.seList)
			{
				processServiceEntry(s);
			}

		}

		if (se != null && se.type == ServiceEntry.SE_TYPE_VIT)
		{

			// vm=serviceAnalysisUI.ModelForm._VM;
			vm.addType(se.vol);
			VolumetricType vt = vm.contains(se.vol.getName());

			if (se.service.getClass() == Workstream.class)
			{
				Workstream w;
				if (serviceOperation.contains(se.service))
				{
					w = ((Workstream) serviceOperation.get(serviceOperation
							.indexOf(se.service)));

				} else
					w = (Workstream) se.service;

				vt.addWorkstream(w);
				vt.setWorkstreamMultiplier(w, se.qty);

			} else
			{
				System.err
						.println("Error: you tried to add a service directly to a VT");
			}
		}

		if (se != null && se.type == ServiceEntry.SE_TYPE_SIT)
		{
			se.consumer.addDependentIterations(se.service,
					Double.valueOf(se.qty));
		}

		if (se != null && se.type == ServiceEntry.SE_TYPE_TIME)
		{
			// find service
			
			if (serviceOperation.contains(se.service))
			{
				// System.out.println("Found Service");
				service = serviceOperation.get(serviceOperation
						.indexOf(se.service));
				// add adddata tags here
				// System.out.println("addData1="+se.service.getAddData());
				service.addDataTags(se.service.getAddData());
				// System.out.println("addData2="+service.getAddData());

			} else
			{
				// System.out.println("Adding Service");
				service = se.service;
				synchronized(serviceOperation){
					serviceOperation.add(service);}
				controller.fireServiceAddedEvents(service);
				// if(serviceAnalysisUI.ServiceTable.model!=null){
				// service.addServiceListener(serviceAnalysisUI.ServiceTable.model);
				// serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();
				// }
			}

			if (se.wait != 0) // just set adjustment
			{
				service.setAdjustments((float) se.wait);
			}
			if (se.response >= 0)
			{
				service.addTime((float) se.response, notify);
				
			}
		}

		if (se != null && se.type == ServiceEntry.SE_TYPE_VOL)
		{
			// System.out.println("adding volumetric type to model");
			// vm=serviceAnalysisUI.ModelForm._VM;
			vm.addType(se.vol);
			VolumetricType vt = vm.contains(se.vol.getName());

			if (se.service != null && serviceOperation.contains(se.service))
			{
				vt.addWorkstream((serviceAnalysisModel.Workstream) serviceOperation
						.get(serviceOperation.indexOf(se.service)));
				// System.out.println("added workstream to vol model");
			}
			// System.out.println("volume of type="+se.volume);
			vm.setVolume(vt, se.volume);
			// vm.simulate();

			// if(serviceAnalysisUI.ServiceTable.model!=null)
			// MainForm.MF.newContentPane.model.updateTypes();
			vm.simulateSave();
			vm.fireVolModelChanged();
			// if(serviceAnalysisUI.ServiceTable.model!=null)
			// MainForm.MF.newContentPane.model.fireTableDataChanged();
		}

		if (se != null && se.service != null
				&& se.type < ServiceEntry.SE_TYPE_VOL)
		{// we have succeeded in reading the line with an extension
			//System.out.println("Service Entry for table");
			if (se.when == null
					|| (se.when.getTime() > fromfilter && se.when.getTime() < tofilter))
			{
				if (se.consumer != null)
				{
					if (serviceOperation.contains(se.consumer))
					{
						// System.out.println("Found Consumer");
						consumer = serviceOperation.get(serviceOperation
								.indexOf(se.consumer));
					} else
					{
						// System.out.println("Adding Consumer");
						consumer = se.consumer;
						synchronized(serviceOperation){
						serviceOperation.add(consumer);}
						controller.fireServiceAddedEvents(consumer);
						// if(serviceAnalysisUI.ServiceTable.model!=null){
						// consumer.addServiceListener(serviceAnalysisUI.ServiceTable.model);
						// serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();
						// }
					}
				}
				if (serviceOperation.contains(se.service))
				{
					 //System.out.println("Found Service");
					
					service = serviceOperation.get(serviceOperation
							.indexOf(se.service));
					// System.out.println("addData1="+se.service.getAddData());
					service.addDataTags(se.service.getAddData());
					// System.out.println("addData2="+service.getAddData());
				} else
				{
					 //System.out.println("Adding Service");
					service = se.service;
					synchronized(serviceOperation){
					serviceOperation.add(service);}
					controller.fireServiceAddedEvents(service);
					// if(serviceAnalysisUI.ServiceTable.model!=null)
					// {
					// service.addServiceListener(serviceAnalysisUI.ServiceTable.model);
					// serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();
					// }

				}

				if (service != null)
				{
					if (consumer != null)
						consumer.addDependentService(service);

					if (se.when != null)
					{
						DBWrite(new ServiceEntry(service, se.when, se.response));
						long endTime=se.when.getTime();
			//System.out.println(endTime);
						endTime+=(int)se.response;
						
						double rate = service.calculateRate(new Date(endTime),se.location);
			//System.out.println(se.response);
						service.addWaitTime((float) se.wait);
						service.addTime((float) se.response, notify);
						service.addTimeSample(new Long((new Date(endTime)).getTime()/service.getSamplePeriod()) , se.response);
						if(se.loginEvent)addLogin(se.when.getTime(),se.user);
						
						setAutoLogout(se.when.getTime(),se.user);
						if(se.logoutEvent)addLogout(se.when.getTime(),se.user);
						if(controller.isUserWatched(se.user)){
							//output se
							//System.out.println(se.userView());
							controller.addUserEvent(se.user, se);
						}
						// add transaction code here
						if (se.txn != null)
						{
							// System.out.println("-----------------");
							// //testing
							// System.out.println("got a transaction "+se.txn);
							// //testing
							// now do something
							// System.out.println("root transaction "+SAEvent.getRootTxn(se.txn));
							// System.out.println("parent transaction "+SAEvent.getParentTxn(se.txn));
							SAEvent event = new SAEvent(se.txn,
									service.getType(),
									service.getServiceName(),
									service.getOperationName(), se.when,
									(int) (se.response + 0.5));
							event.location=se.location;
							event.file=se.filename;
							EventCollection.getInstance().addSAEvent(event);
							// System.out.println("Root Event start "+EventCollection.getInstance().getRangeStartTime(SAEvent.getRootTxn(event.txn)));
							// System.out.println("Root Event range "+EventCollection.getInstance().getRange(SAEvent.getRootTxn(event.txn)));
							// System.out.println("Root Event height "+EventCollection.getInstance().getTransactionHeight(SAEvent.getRootTxn(event.txn)));

							// this set is for testing purposes as well
							// if(!txn.contains(SAEvent.getRootTxn(event.txn)))
							// txn.add(SAEvent.getRootTxn(event.txn));
							// HashMap<String,TxnRect>
							// rects=EventCollection.getInstance().calculatePositions(txn);
							// for(TxnRect rect:rects.values())
							// System.out.println(rect.toString());
							// System.out.println("-----------------");

						}

						Iterator it = listeners.getAll(service);
						while (it.hasNext())
						{
							BWStatsEventListener l;
							synchronized (listeners)
							{
								l = (BWStatsEventListener) it.next();
							}
							synchronized (l)
							{
								l.BWServiceOperationStat(service, se.when,
										se.response, service.getAvgtime(),
										se.wait, service.getRate(se.location),se.location);
							}
						}
						EventAlerter alerter = EventAlerter.getInstance();
						alerter.alert(se);
					}
				}
			}
		}
		if (se != null && se.type == ServiceEntry.SE_TYPE_COMMAND)
		{
			controller.processCommand(se.command, se.args);
		}
		
		// if(MainForm.MF!=null)
		// MainForm.MF.newContentPane.model.updateTypes();
		vm.fireVolModelChanged();
		if (vm != null)
			vm.simulateSave();

		// MainForm.MF.newContentPane.model.fireTableDataChanged();
	}

	// for testing purposes of transactions
	ArrayList<String> txn = new ArrayList<String>(); // this is for testing
														// purposes only

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * serviceAnalysisModel.readersWriters.LogFileTailerListener#newLogFileLine
	 * (java.lang.String)
	 */
	@Override
	public void newLogFileLine(String s,String filename)
	{
		ExtensionHandler RTR = ExtensionHandler.getInstance();
		ServiceEntry se = RTR.ProcessLogFileLine(s);
		
		if(se!=null)
		{
			se.filename=filename;
			if(se.location == null )se.location="localhost";
			if(se.location.equals("")) se.location="localhost";
		}
		processServiceEntry(se);
	}

	// private HashMap<String,Service> requestService=new
	// HashMap<String,Service>();
	// private HashMap<String,Double> requestWait=new HashMap<String,Double>();

	/**
	 * This method processes an AMS_PERF log line creating or extracting the UI
	 * or BW {@link Service} object and adding the time event and dependency as
	 * required
	 * 
	 * @param s
	 *            The String line to process as an AMS_PERF log line
	 */
	/*
	 * public void newAMSPERFLogFileLine(String s){
	 * //System.out.println("AMS_PERF line: "+s); if(!s.contains("<Error>") &
	 * s.contains("<AMS_PERF>")){ //ignore error lines and non perf lines
	 * if(s.contains("request start:")){ // code here to handle request number
	 * String request=
	 * s.substring(s.indexOf("request start: ")+15,s.length()-2);
	 * if(request.contains("[null]")) request=request.substring(1,
	 * request.length()-7).trim();
	 * 
	 * if(request.contains("[")){ int i=request.indexOf("["); int
	 * j=request.indexOf("]"); request=request.substring(i+1, j); }
	 * if(request.contains("systemtools"))
	 * request=request.substring(request.indexOf("systemtools")+12);
	 * if(request.contains("home/"))
	 * request=request.substring(request.indexOf("home/"));
	 * if(request.contains("?"))
	 * request=request.substring(0,request.indexOf("?"));
	 * 
	 * if(request.startsWith("/AMSPortalWebProject/com/ams2/"))
	 * request=request.substring(30);
	 * 
	 * if(request.startsWith("/AMSPortalWebProject/com/ams/"))
	 * request=request.substring(29);
	 * if(request.endsWith("do"))request=request.substring(0,
	 * request.length()-3);
	 * if(request.endsWith("jsp"))request=request.substring(0,
	 * request.length()-4);
	 * request=request.replace("showAMSBodyPortletSystemTools", "");
	 * //System.out.println("Processed Request: "+request);
	 * 
	 * Service service=new Service(Service.UI_TYPE,"PORTAL",request,"UI");
	 * 
	 * if (serviceOperation.contains(service)){ service=
	 * serviceOperation.get(serviceOperation.indexOf(service)); } else {
	 * serviceOperation.add(service);
	 * service.addServiceListener(serviceAnalysisUI.ServiceTable.model);
	 * serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();
	 * 
	 * }
	 * 
	 * int y=s.indexOf("<Request #")+10; int z=s.indexOf(',', y); String
	 * reqNumber=s.substring(y, z); requestService.put(reqNumber, service);
	 * 
	 * } if(s.contains("service start:")){//get request service and add
	 * dependency if new one Service service; int y=s.indexOf("<Request #")+10;
	 * int z=s.indexOf(',', y); String reqNumber=s.substring(y, z);
	 * if(requestService.containsKey(reqNumber)){
	 * service=requestService.get(reqNumber); y=s.indexOf("service start:")+15;
	 * z=s.length()-2; String so=s.substring(y, z); //System.out.println(so);
	 * String sn[]=so.split("\\.", 2); //System.out.println(sn[0]+"-"+sn[1]);
	 * String sname=sn[0].toUpperCase(); String oname=sn[1].toLowerCase();
	 * Service service2=new Service("BW",sname,oname,""); Service service3=new
	 * Service("BW",sname,oname,""); Service service4=new
	 * Service("BW",sname,oname,"");
	 * 
	 * if (serviceOperation.contains(service2)){ service2=
	 * serviceOperation.get(serviceOperation.indexOf(service2)); } else
	 * if(serviceOperation.contains(service3)){
	 * service2=serviceOperation.get(serviceOperation.indexOf(service3)); } else
	 * if(serviceOperation.contains(service4)){
	 * service2=serviceOperation.get(serviceOperation.indexOf(service4)); } else
	 * { serviceOperation.add(service2);
	 * service2.addServiceListener(serviceAnalysisUI.ServiceTable.model);
	 * serviceAnalysisUI.ServiceTable.model.fireTableDataChanged(); }
	 * service.addDependentService(service2);
	 * 
	 * } // else { // System.out.println("Request: " + reqNumber+
	 * " service not available"); // } } if(s.contains("service end:")){ int
	 * i=s.indexOf("duration(ms):"); String ws=s.substring(i+14,s.length()-2);
	 * //System.out.println(ws); Double wait=new Double(ws); int
	 * y=s.indexOf("<Request #")+10; int z=s.indexOf(',', y); String
	 * reqNumber=s.substring(y, z); if(requestWait.containsKey(reqNumber)) {
	 * wait+=requestWait.get(reqNumber); } requestWait.put(reqNumber, wait);
	 * 
	 * } if(s.contains("request end: ")){ //process this line only Service
	 * service; int y=s.indexOf("<Request #")+10; int z=s.indexOf(',', y);
	 * String reqNumber=s.substring(y, z);
	 * 
	 * String req= s.substring(s.indexOf("request end: ")+13,s.length()-1);
	 * String reqs[]=req.split(",");
	 * 
	 * 
	 * if(requestService.containsKey(reqNumber)){
	 * service=requestService.get(reqNumber); requestService.remove(reqNumber);
	 * } else { String request=reqs[0]; if(request.contains("[null]"))
	 * request=request.substring(1, request.length()-7).trim();
	 * 
	 * if(request.contains("[")){ int i=request.indexOf("["); int
	 * j=request.indexOf("]"); request=request.substring(i+1, j); }
	 * if(request.contains("systemtools"))
	 * request=request.substring(request.indexOf("systemtools")+12);
	 * if(request.contains("home/"))
	 * request=request.substring(request.indexOf("home/"));
	 * if(request.contains("?"))
	 * request=request.substring(0,request.indexOf("?"));
	 * 
	 * if(request.startsWith("/AMSPortalWebProject/com/ams2/"))
	 * request=request.substring(30);
	 * 
	 * if(request.startsWith("/AMSPortalWebProject/com/ams/"))
	 * request=request.substring(29);
	 * 
	 * service=new Service(Service.UI_TYPE,"PORTAL",request,"UI");
	 * 
	 * }
	 * 
	 * //reqs[1]=reqs[1].trim(); String
	 * duration=reqs[1].substring(reqs[1].indexOf
	 * (":")+1,reqs[1].length()-1).trim();
	 * //System.out.println("Request: "+request);
	 * //System.out.println("Duration: "+duration); String ds[]=s.split(">");
	 * String dateString=ds[0].substring(5, ds[0].length()-12);
	 * //System.out.println("Date: "+dateString+" "+ds[9].substring(2)); Long
	 * L=new Long(ds[9].substring(2)); long ll=L.longValue(); Date when=new
	 * Date(ll); Float millisecs=Float.parseFloat(duration);
	 * 
	 * 
	 * 
	 * 
	 * //System.out.println("Processed Request: "+request);
	 * 
	 * 
	 * if (serviceOperation.contains(service)){ service=
	 * serviceOperation.get(serviceOperation.indexOf(service)); } else {
	 * serviceOperation.add(service);
	 * service.addServiceListener(serviceAnalysisUI.ServiceTable.model);
	 * serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();
	 * 
	 * }
	 * 
	 * 
	 * if(service!=null) { DBWrite(new ServiceEntry(service,when,millisecs));
	 * service.addTime(millisecs,notify); service.calculateRate(when);
	 * //System.out.println(when); double wait=0D;
	 * 
	 * if(requestWait.containsKey(reqNumber)) { wait=requestWait.get(reqNumber);
	 * //System.out.println(millisecs+" "+wait); requestWait.remove(reqNumber);
	 * } service.addWaitTime((float)wait);
	 * 
	 * Iterator it=listeners.getAll(service); while(it.hasNext()) {
	 * BWStatsEventListener l; synchronized(listeners)
	 * {l=(BWStatsEventListener)it.next();} synchronized(l){
	 * l.BWServiceOperationStat
	 * (service,when,millisecs,service.getAvgtime(),wait); } } } }//end of
	 * request end line processing }//end of ignore error lines }
	 */

	public static int weblog_time_location = 0;
	public static int weblog_response_location = 0;
	public static int weblog_uri_location = 0;
	public static char weblog_separation_char = ' ';

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * serviceAnalysisModel.readersWriters.LogFileTailerListener#fileEndReached
	 * (java.lang.String)
	 */
	@Override
	public void fileEndReached(String filename)
	{
		// TODO Auto-generated method stub
		System.out.println("end of file " + filename);
		MainForm.sbar.setMessage("end of file " + filename);

		if (removeReaderAtEOF)
			removeReader(filename);
		if (readers.containsKey(filename))
		{
			LogFileReader lfr = readers.get(filename);
			System.out.println("removing reader");
			lfr.removeLogFileTailerListener(this);
			//readers.remove(filename);
			controller.removeFileMonitor(filename);
		}

	}

	private Controller controller;

	public VolumetricModel getVolumetricModel()
	{
		return vm;
	}

	public Controller getController()
	{
		return controller;
	}

	public void setController(Controller controller)
	{
		this.controller = controller;
	}

	@Override
	public void closed()
	{
		// TODO Auto-generated method stub
		lJMS = null;

	}
	//private TreeMap<Long,Integer> logins=new TreeMap<Long,Integer>();
	private SortedMap<Long,Integer> logins=Collections.synchronizedSortedMap(new TreeMap<Long,Integer>());
	private Map<String,Long> autoLogouts=Collections.synchronizedMap(new HashMap<String,Long>());
	private Set<String> loggedIn=Collections.synchronizedSet(new HashSet<String>());

	private UserProfiles profile=new UserProfiles();
	
	public UserProfiles getProfiles(){
		return profile;
	}
	
	public SortedMap<Long,Integer> getLogins(){
		return new TreeMap<Long,Integer>(logins);
	}
	
	public Map<String,Long> getAutoLogouts(){
		return new HashMap<String,Long>(autoLogouts);
	}
	
	public Set<String> getLoggedIn(){
		return new HashSet<String>(loggedIn);
	}
	
	public void setLogins(SortedMap<Long,Integer> logins){
		this.logins=Collections.synchronizedSortedMap(new TreeMap<Long,Integer>(logins));
		//System.out.println("Logins size="+this.logins.size());
	}
	
	public void setAutoLogouts(Map<String,Long> autoLogouts){
		this.autoLogouts=Collections.synchronizedMap(new HashMap<String,Long>(autoLogouts));
		//System.out.println("AutoLogouts size="+this.autoLogouts.size());
	}
	public void setLoggedIn(Set<String> loggedIn){
		this.loggedIn=Collections.synchronizedSet(new HashSet<String>(loggedIn));
		//System.out.println("user size="+this.loggedIn.size());
	}
	
	private int ocount=0;
	
	public void addLogin(long time,String user)
	{
		if(newMethod)return;
			
		ocount++;
		long min=time/1000/60;
		synchronized(logins) {
			if(logins.containsKey(min)){
				Integer i=logins.get(min)+1;
				logins.put(min, i);
			} else logins.put(min, 1);
			if(user!=null & !user.equals("")){
				loggedIn.add(user);
				setAutoLogout(time,user);
			}
		}
		
	}
	
	public void addLogout(long time,String user)
	{
		if(newMethod){
			profile.addLogoutEvent(user, time);
			return;
		}
		ocount--;
		long min=time/1000/60;
		synchronized (logins){
			if(logins.containsKey(min)){
				Integer i=logins.get(min)-1;
				logins.put(min, i);
			} else logins.put(min, -1);
			if(user!=null & !user.equals(""))
			{
				//System.out.println("removing autologout for user " +user);
				loggedIn.remove(user);
				 Long u2 = autoLogouts.remove(user);
				 //System.out.println("remove auto "+ocount+" "+autoLogouts.size()+" at time "+u2);
			}
			else System.out.println("logout called without user");
		}
		
		
		
	}
	
	public void clearLoginCount()
	{
		if(newMethod)profile.reset();
		else synchronized(logins){
			logins.clear(); 
			ocount=0;
			autoLogouts.clear();
			loggedIn.clear();
			
			
		}
	}
	
	public void zeroCount()
	{
		if(newMethod) return;
		long l=System.currentTimeMillis();
		zeroCount(l);
	}
	
	public void zeroCount(long time)
	{
		if(newMethod)return;
		long l=time/60000;
		synchronized (logins){
			if(logins.containsKey(l)){
				Integer i=logins.get(l)-ocount;
				logins.put(l, i);
			}else logins.put(l, -ocount);
			ocount=0;
		}
		autoLogouts.clear();
		loggedIn.clear();
	}
	
	public static int autoLogoutPeriod=40; //mins
	
	public void setAutoLogout(long time,String user)
	{
		if(user==null) return;
		if(user.equals("")) return;
		//add call to new system
		profile.addLoginEvent(user, time);
		if(newMethod)return;
		
		//System.out.println(time);
		long l=time/60000;
		synchronized(logins){
			if(autoLogouts.containsKey(user))
			{
				long oldTime=autoLogouts.get(user);
				if(l-oldTime<autoLogoutPeriod) {
					//replace auto logout
					if(loggedIn.contains(user))
						autoLogouts.put(user, l+autoLogoutPeriod);
				}
				else //an auto logout must have occurred
				{
					//System.out.println("calling logout from autologout "+user+" "+new Date(l*60000)+" "+new Date(oldTime*60000)+" "+(l-oldTime));
					addLogout(oldTime*60000,user);
					loggedIn.add(user);
					autoLogouts.put(user, l+autoLogoutPeriod);
				}
			} else
				if(loggedIn.contains(user))
					autoLogouts.put(user, l+autoLogoutPeriod);
				else {//for some reason I seem to not have a user so will add a login now
					//addLogin(time,user);
				}
			//System.out.println("add auto "+ocount+" "+autoLogouts.size()+" at time "+(l+autoLogoutPeriod));
		}
	}
	
	private boolean newMethod=true;
	public TreeMap<Long,Integer> getUserCount()
	{
		if(newMethod) return profile.getActiveUserCounts();
		TreeMap<Long,Integer> counts=new TreeMap<Long,Integer>();
		Integer count=0;
		synchronized (logins){
	
			//System.out.println("User Count "+ logins.size());
			TreeMap<Long,Integer> sortme=new TreeMap<Long,Integer>();

			for(Long l:autoLogouts.values())
			{
				Integer i=sortme.get(l);
				if(i==null)sortme.put(l, -1);
				else sortme.put(l,i-1);
			}
			for(Long l:logins.keySet())
			{
				Integer i=sortme.get(l);
				if(i==null)sortme.put(l, logins.get(l));
				else sortme.put(l,i+logins.get(l));
			}
			for(Long l:sortme.keySet())
			{
				
				count+=sortme.get(l);
				count=Math.max(0,count);
				//System.out.println(""+count+"@"+l);
				counts.put(l, count);
				//System.out.println(""+new Date(l*60000)+", "+l+", "+count);
			}
//			System.out.println("user count prior to autologouts "+count +", "+ocount+" autologout entry count = "+autoLogouts.size());
			//add auto logouts
			
/*			
			for(Long l:autoLogouts.values())
			{
				sortme.add(l);
			}
			Collections.sort(sortme);
			for(Long l:sortme)
			{
				count--;
				Integer current=counts.get(l);
				if(current==null) 
				{
					System.out.println("no existing entry for "+l);
					//get the closest
					Entry<Long, Integer> entry = counts.floorEntry(l);
					if(entry==null) current=-1;
					else current=entry.getValue()-1;
					
				}
				else
				{
					System.out.println("reducing current by one");
					current -= 1;
				}
				System.out.println(l+", "+current);
				counts.put(l, current);
			}
*/			
		}
		//System.out.println("final user count "+count +", "+ocount);
		return counts;
	}
}
