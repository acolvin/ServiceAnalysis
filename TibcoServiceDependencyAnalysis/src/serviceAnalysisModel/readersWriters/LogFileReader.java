package serviceAnalysisModel.readersWriters;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * 
 * @author framework by InformIT Java Reference Guide
 * @author changes by apc to extend its functionality
 */

public class LogFileReader extends Thread
{

	private long sampleInterval = 1000;
	private Set<LogFileTailerListener> listeners = new HashSet<LogFileTailerListener>();
	private File logfile;
	private String filename;
	private boolean tailing = false;
	private boolean pause = false;
	private static final long dayinmilli=1000*60*60*24;
	private static final long hourinmilli=1000*60*60; 
	
	private static int dormantHoursBeforeClose=36;
	
	private FileMonitor monitor;
	
	private long lastRead=0;

	private boolean startAtBeginning = false;

	public LogFileReader(File file)
	{
		super("File Reader - "+file.getName());
		this.setDaemon(true);
		this.logfile = file;
		filename = file.getPath();

	}

	public LogFileReader(String filename)
	{
		super("File Reader - "+filename);
		this.setDaemon(true);
		logfile = new File(filename);
		this.filename = filename;

	}

	public LogFileReader(String filename, boolean readAll)
	{
		super("File Reader - "+filename);
		this.setDaemon(true);
		logfile = new File(filename);
		if (readAll)
			startAtBeginning = true;
		this.filename = filename;

	}
	
	public String getFilename(){
		return new String(filename);
	}

	public synchronized void addLogFileTailerListener(LogFileTailerListener l)
	{
		this.listeners.add(l);
	}

	public synchronized void removeLogFileTailerListener(LogFileTailerListener l)
	{
		synchronized (this.listeners)
		{
			this.listeners.remove(l);
		}
	}

	protected synchronized void fireNewLogFileLine(String line)
	{
		synchronized (this.listeners)
		{
			for (Iterator<LogFileTailerListener> i = this.listeners.iterator(); i
					.hasNext();)
			{
				LogFileTailerListener l = (LogFileTailerListener) i.next();
				synchronized (l)
				{
					l.newLogFileLine(line,filename);
				}
			}
		}
	}

	protected void fireFileEndReached()
	{
		for (Iterator<LogFileTailerListener> i = this.listeners.iterator(); i
				.hasNext();)
		{
			LogFileTailerListener l = (LogFileTailerListener) i.next();
			System.out.println("firing eof "+filename);
			l.fileEndReached(filename);
		}
	}

	private void internalStopTailing()
	{
		System.out.println("Stopping file reader for file: "+filename);
		this.tailing = false;
		fireFileEndReached();
		
	}
	
	public void stopTailing()
	{
		interrupt();
	}

	public void pauseTailing()
	{
		pause = true;
	}

	public void restartTailing()
	{
		pause = false;
	}

	@Override
	public void run()
	{
		// The file pointer keeps track of where we are in the file
		long filePointer,endPointer;

		// Determine start point
		if (this.startAtBeginning)
		{
			filePointer = 0;
			endPointer=this.logfile.length();
		} else
		{
			filePointer = this.logfile.length();
			endPointer=Long.MAX_VALUE;
		}

		//start the monitor
		monitor=new FileMonitor();
		monitor.start();
		try
		{
			// Start tailing
			this.tailing = true;
			RandomAccessFile file = new RandomAccessFile(logfile, "r");
			while (this.tailing & !this.isInterrupted())
			{
				try
				{
					// System.out.println("entered try block");
					// Compare the length of the file to the file pointer
					long fileLength = this.logfile.length();
					//if(!startAtBeginning & System.currentTimeMillis()-logfile.lastModified()>dayinmilli){
						//we have been waiting a day without an update time to close the file
					//	this.tailing=false;
					//}
					if (!startAtBeginning & (fileLength < filePointer))
					{
						System.out.println("reopen file");
						// Log file must have been rotated or deleted;
						// reopen the file and reset the file pointer
						file.close();
						if(logfile.exists())
						{
							file = new RandomAccessFile(logfile, "r");
							filePointer = 0;
						}
						else 
						{
							this.tailing=false;
							pause=true;
						}
						
						
					}

					if (startAtBeginning & ((fileLength <= filePointer) | (filePointer>=endPointer)))
					{
						pause = true;
						this.tailing = false;
					}

					if (!pause & (fileLength > filePointer) & (filePointer<endPointer))
					{
						// There is data to read
						file.seek(filePointer);
						// System.out.println("before while "+filePointer);
						String line = file.readLine();
						filePointer = file.getFilePointer();
						// System.out.println(line);
						while (line != null & tailing==true & (filePointer<endPointer))
						{
							try
							{
								
								
								this.fireNewLogFileLine(line);
							} catch (Exception e)
							{
								// TODO: handle exception
								System.out.println(logfile.getCanonicalPath()
										+ ": error thrown");
								System.out.println(line);
								e.printStackTrace();
							}
							line = file.readLine();
							lastRead=System.currentTimeMillis();
							filePointer = file.getFilePointer();
							// System.out.println("in while "+file.getFilePointer());
						}
						filePointer = file.getFilePointer();
						// System.out.println("after while "+filePointer);
					}

					// Sleep for the specified interval
					// System.out.println("time to sleep");
					//System.out.println("in sleep");
					if(!this.isInterrupted())sleep(this.sampleInterval);
					//System.out.println("out of sleep");
					
				} catch (InterruptedException e){
				} catch (Exception e)			
				{ //not expecting this
					System.out.println(e);
				} 
			}

			// Close the file that we are tailing
			System.out.println("File reading ending for file: "+filename);
			file.close();
			fireFileEndReached();
			monitor.stopMonitor();

		} catch (Exception e)
		{
			e.printStackTrace();
			monitor.stopMonitor();
		}
	}

	public void interrupt()
	{
		System.out.println("interrupting reader");
		internalStopTailing();
		monitor.interrupt();
		super.interrupt();
		
	}
	
	private class FileMonitor extends Thread {
		public FileMonitor(){
			this.setName("File Tailer Monitor Thread - "+filename);
			this.setDaemon(true);
		}
		private long lastReported=0;
		private int count=0;
		private boolean stop=false;
		
		public void stopMonitor(){
			stop=true;
		}
		
		public void run(){
			while(!stop){
				if(lastRead!=0 && System.currentTimeMillis()-lastRead>hourinmilli) {
					if(System.currentTimeMillis()-lastReported>hourinmilli){
						//System.out.println("no records in log file for more than an hour: "+filename);
						lastReported=System.currentTimeMillis();
						count++;
						System.out.println("no records in log file for more than "+count+" hour(s): "+filename);
//  can add in code here to close file after a period of time.  Just call stopTailing
						if(count>=dormantHoursBeforeClose){
							stopMonitor();
							stopTailing();
							
						}
					
					}
					
				} else count=0;
				try {
					//Thread.sleep(3000l); //for testing
					Thread.sleep(300000l);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
			
			}
			
		}
	}
	
	public static void setDormantHoursBeforeClose(int hours){
		if(hours>0) dormantHoursBeforeClose=hours;
	}
	
	public static int getDormantHoursBeforeClose(){
		return dormantHoursBeforeClose;
	}
}
