package serviceAnalysisModel.readersWriters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PushbackInputStream;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

public class PipeReader extends Thread
{
	public PipeReader(InputStream in)
	{
		super("Pipe Reader");
		this.setDaemon(true);
		InputStreamReader converter;
		try {
			converter = new InputStreamReader(PipeReader.maybeDecompress(in));
			this.in = new BufferedReader(converter);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	public String filename="stdin";
	public boolean poll=true; //this variable hen true means that the reader will poll forever
	
	private BufferedReader in;

	private ArrayList<LogFileTailerListener> listeners = new ArrayList<LogFileTailerListener>();

	public void addListener(LogFileTailerListener l)
	{
		listeners.add(l);
	}

	public boolean read = true;

	public void run()
	{
		String s = "";
		if(in==null) read=false;
		while (read)
		{
			try
			{
				s = in.readLine();
				if (s != null)
				{
					for (LogFileTailerListener l : listeners)
						l.newLogFileLine(s,filename);
				}
				else {
					if(!poll)
						read=false;
					else
					try {
						Thread.sleep(1000l);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
				}

			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				read=false;
			}

		}
		for (LogFileTailerListener l : listeners)
			l.fileEndReached(filename);
		try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static InputStream maybeDecompress(InputStream input) throws IOException {
	    final PushbackInputStream pb = new PushbackInputStream(input, 2);

	    int header = pb.read();
	    if(header == -1) {
	        return pb;
	    }

	    int b = pb.read();
	    if(b == -1) {
	        pb.unread(header);
	        return pb;
	    }

	    pb.unread(new byte[]{(byte)header, (byte)b});

	    header = (b << 8) | header;

	    if(header == GZIPInputStream.GZIP_MAGIC) {
	        return new GZIPInputStream(pb);
	    } else {
	        return pb;
	    }
	}
}
