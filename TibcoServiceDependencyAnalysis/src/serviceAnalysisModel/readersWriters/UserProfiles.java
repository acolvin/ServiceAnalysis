package serviceAnalysisModel.readersWriters;

import java.util.HashMap;
import java.util.TreeMap;

public class UserProfiles {

	public HashMap<String,UserLoginProfile> profiles=new HashMap<String,UserLoginProfile>();
	
	public UserProfiles(){
		
	}
	
	public void reset(){
		profiles.clear();
		counterStale=true;
	}
	
	public void addUser(String user){
		synchronized(lock){
			profiles.put(user, new UserLoginProfile(user));
		}
	}
	
	public void addLoginEvent(UserLoginEvent event){
		synchronized(lock){
			if(!profiles.containsKey(event.user))addUser(event.user);
			UserLoginProfile profile = profiles.get(event.user);
			profile.addEvent(event);
			counterStale=true;
		}
		
	}
	
	public void addLoginEvent(String user, long time){
		synchronized(lock){
			addLoginEvent (new UserLoginEvent(user,time));
		}
	}
	
	public void addLoginEvent(String user, long start, long end){//used to restore 
		synchronized(lock){
			if(!profiles.containsKey(user))addUser(user);
			UserLoginProfile profile = profiles.get(user);
			profile.addEvent(user,start,end);
			counterStale=true;
		}
	}
	
	public void addLogoutEvent(String user, long time){
		if(user==null || user.length()==0) return;
		synchronized(lock){
			if(!profiles.containsKey(user))return;
			UserLoginProfile profile = profiles.get(user);
			if(profile.setLogout(time))counterStale=true;
		}
	}
	
	private boolean counterStale=true;
	private TreeMap<Long,Integer> counts=new TreeMap<Long,Integer>();
	
	public TreeMap<Long,Integer> getActiveUserCounts(){
		synchronized(lock){
			if(!counterStale) return counts;
			
			counts.clear();
			synchronized(profiles){
				for(UserLoginProfile p:profiles.values()){
					Long start=p.getEarliestEvent();
					Long end=p.getLatestEvent();
					//System.out.println("event range for user "+p.user+ " is ["+start+","+end+"]");
					if(start!=null){
						long t=((long)(start/60000l));
						long e=((long)(end/60000l));
						while(t<=e){
							if(p.isLoggedIn(t*60000l)){
								//System.out.println("user "+p.user+" is logged in @"+t*60000l);
								if(counts.containsKey(t)){
									counts.put(t, counts.get(t)+1);
								}else counts.put(t,1);
							}
							else { //put zero counter in
								if(!counts.containsKey(t))counts.put(t, 0);
							}
							
							
							t+=1l;
						}
					}
				}
				counterStale=false;
			}
			
		}
		return counts;
	}
	
	public static Object lock=new Object();
}
