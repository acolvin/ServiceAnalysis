package serviceAnalysisModel.readersWriters;

import java.util.Date;

import serviceAnalysisModel.Service;

/**
 * Used to read and write sample data to and from the database
 * 
 * @author apc
 * 
 */
public class ServiceSample
{

	public ServiceSample(Date snapShot, Service service, double response,
			double qty)
	{
		this.service = service;
		this.when = snapShot;
		this.response = response;
		this.qty = qty;
	}

	public Service service;
	public Date when;
	public double response;
	public double qty;

}
