package serviceAnalysisModel.readersWriters;

public class UserLoginEvent implements Comparable<UserLoginEvent>{
	public long start,end;
	public String user;
	
	public UserLoginEvent(String user,long start){
		this.user=user;
		this.start=start;
		this.end=start+SESSION_TIMEOUT;
	}
	public UserLoginEvent(String user,long start,long end){
		this.user=user;
		this.start=start;
		this.end=end;
	}
	public UserLoginEvent(UserLoginEvent event){
		user=new String(event.user);
		start=event.start;
		end=event.end;
	}
	
	public boolean isLoggedIn(long time){
		if(start<=time && time<=end) return true;
		 
			
			return false;
	}
	
	public boolean isOverlapping(UserLoginEvent event){
		return isLoggedIn(event.start) || isLoggedIn(event.end);
		
	}
	
	public boolean addevent(UserLoginEvent event){
		if(event==null)return false;
		UserLoginEvent union = getUnion(event);
		if(union==null) return false; //independent event
		
		start=union.start;
		end=union.end;
		return true;
	}
	
	public UserLoginEvent getUnion(UserLoginEvent event){
		if(!isOverlapping(event))return null;
		//System.out.println("events overlap get union");
		  //System.out.println("   Current event "+toString());
		  //System.out.println("   Adding Event "+ event.toString());
		UserLoginEvent union=new UserLoginEvent(user,Math.min(start, event.start),Math.max(end,event.end));
		  //System.out.println("   Union Event "+ union.toString());

		//addevent(event);
		return union;
	}
	
	public void setLogout(long end){
		if(end>start)this.end=end;
	}
	
	public String toString(){
		return user+","+start+","+end;
	}
	public static long SESSION_TIMEOUT=600000l-1l; //one millisec less than 10 mins

	

	@Override
	public int compareTo(UserLoginEvent o) {
		if(start<o.start)return -1;
		if(start>o.start) return 1;
		else if(end<o.end)return -1;
		else if(end>o.end)return 1;
		else return 0;
		
	}
	
	@Override
	public boolean equals(Object o){
		if(o==null)return false;
		if(o instanceof UserLoginEvent){
			UserLoginEvent e=(UserLoginEvent)o;
			if(user.equals(e.user) && start==e.start && end==e.end) return true;
		}
		return false;
	}
	
	@Override
	public int hashCode(){
		String s=user+start+end;
		return s.hashCode();
	}
}
