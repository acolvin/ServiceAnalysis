package serviceAnalysisModel.readersWriters;

import java.io.File;
import java.util.logging.Logger;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.HiddenFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import serviceAnalysisModel.SAControl.Controller;

/**
 * This class monitors a directory for changing files and created files
 * </br>
 * it uses Apache Commons IO 
 * @author andrew
 *
 */
public class DirectoryTailer implements FileAlterationListener 
{

	public DirectoryTailer(String directory, String filePattern)
	{
		dir=directory;
		pattern=filePattern;
		
		File d=new File(dir);
		if(d.exists() & d.isDirectory())
		{
			// Create a FileFilter
		      IOFileFilter directories = FileFilterUtils.and(
		                                      FileFilterUtils.directoryFileFilter(),
		                                      HiddenFileFilter.VISIBLE);
		      IOFileFilter files       = FileFilterUtils.and(
		                                      FileFilterUtils.fileFileFilter(),
		                                      FileFilterUtils.suffixFileFilter(pattern));
		      IOFileFilter filter = FileFilterUtils.or(directories, files);

		      // Create the File system observer and register File Listeners
		      FileAlterationObserver observer = new FileAlterationObserver(d, filter);
		      
		      observer.addListener(this);
		      
		      monitor = new FileAlterationMonitor();
		      monitor.addObserver(observer);
		      try
		      {
				monitor.start();
		      } catch (Exception e)
		      {
				// TODO Auto-generated catch block
		    	  Logger.getAnonymousLogger().warning("Unable to start directory watcher "+directory+" for "+filePattern);
		    	  monitor=null;
		    	  e.printStackTrace();
				
		      }
		      
		}
		
	}
	
	
	public void stop()
	{
		try
		{
			if(monitor!=null)monitor.stop();
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String dir, pattern;
	FileAlterationMonitor monitor;
	
	
	@Override
	public void onDirectoryChange(File arg0)
	{
		//System.out.println("Dir Change "+arg0.getName());
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onDirectoryCreate(File arg0)
	{
		// TODO Auto-generated method stub
		//System.out.println("Dir Create "+arg0.getName());
	}
	@Override
	public void onDirectoryDelete(File arg0)
	{
		// TODO Auto-generated method stub
		//System.out.println("Directory Deleted: "+arg0.getName());
	}
	@Override
	public void onFileChange(File arg0)
	{
		// TODO Auto-generated method stub
		//System.out.println("File Change "+arg0.getName());
		Controller.getController().monitorFile(arg0.getAbsolutePath());
	}
	@Override
	public void onFileCreate(File arg0)
	{
		// TODO Auto-generated method stub
		System.out.println("File Create "+arg0.getName());		
		Controller.getController().monitorFile(arg0.getAbsolutePath());
	}
	@Override
	public void onFileDelete(File arg0)
	{
		// TODO Auto-generated method stub
		//System.out.println("File Delete "+ arg0.getName());
		//if we are monitoring it it will be closed by the tailer
		//Controller.getController().removeFileMonitor(arg0.getAbsolutePath());
	}
	@Override
	public void onStart(FileAlterationObserver arg0)
	{
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onStop(FileAlterationObserver arg0)
	{
		// TODO Auto-generated method stub
		
	}
}
