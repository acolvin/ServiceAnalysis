package serviceAnalysisModel.readersWriters;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.VolumetricType;
import serviceAnalysisModel.Workstream;

/**
 * Used to create entry read from files by file readers
 * 
 * @author apc
 *
 */
/**
 * @author apc
 * 
 */
public class ServiceEntry implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1302857275965139627L;

	/**
     * 
     */
	//private static final long serialVersionUID = -4141026780581023456L;

	/**
	 * Basic ServiceEntry to notify of a time sample. The service will be
	 * created if it is not in the model.
	 * 
	 * @param service
	 * @param when
	 *            The time of the sample event
	 * @param response
	 *            The duration the service took to execute
	 *            <P>
	 *            Manually set the wait time (field wait) of the service
	 *            execution in the ServiceEntry object
	 */
	public ServiceEntry(Service service, Date when, double response)
	{
		this.service = service;
		this.when = when;
		this.response = response;

		type = SE_TYPE_EVENT;
	}

	public ServiceEntry(Date snapShot)
	{
		type = -1;
		this.when = snapShot;
	}

	/**
	 * Creates a time sample event and provides the consumer service. Both
	 * services will be created if not already in the model
	 * 
	 * @param service
	 * @param when
	 * @param response
	 * @param consumer
	 */
	public ServiceEntry(Service service, Date when, double response,
			Service consumer)
	{
		this.service = service;
		this.when = when;
		this.response = response;
		this.consumer = consumer;
		type = SE_TYPE_EVENT;
	}

	/**
	 * Used by the database writer
	 * 
	 * @param snapShot
	 * @param service
	 * @param response
	 * @param qty
	 */
	public ServiceEntry(Date snapShot, Service service, double response,
			double qty)
	{
		this.service = service;
		this.when = snapShot;
		this.response = response;
		this.qty = qty;
		type = SE_TYPE_SAMPLE;
	}

	public ServiceEntry(Date snapShot, Service service, int type)
	{
		this.service = service;
		this.when = snapShot;
		if (type == SE_TYPE_SUMMARY)
		{
			// System.out.println("summary record created");
			this.type = SE_TYPE_SUMMARY;
		}
		if (type == SE_TYPE_HIERARCHY)
		{
			this.type = SE_TYPE_HIERARCHY;

		}
		if (type == -1)
		{
			this.type = -1;
		}
	}

	/**
	 * Creates a volumetric type in the model and links it to the workstream
	 * 
	 * @param volType
	 * @param wsType
	 */
	public ServiceEntry(VolumetricType volType, Workstream wsType)
	{
		vol = volType;
		service = wsType;
		type = SE_TYPE_VOL;
	}

	/**
	 * Creates a ServiceEntry that holds more ServiceEntries. These are all
	 * processed as a group.
	 * 
	 * @param seList
	 *            List of ServiceEntry Objects
	 */
	public ServiceEntry(List<ServiceEntry> seList)
	{
		this.seList = seList;
		type = SE_TYPE_SET;
	}

	/**
	 * Sets the iteration count between a volumetricType and its WorkStream
	 * 
	 * @param volType
	 * @param service
	 * @param iteration
	 */
	public ServiceEntry(VolumetricType volType, Service service,
			double iteration)
	{
		vol = volType;
		this.service = service;
		qty = iteration;
		type = SE_TYPE_VIT;
	}

	/**
	 * Creates the iteration count between 2 services
	 * 
	 * @param consumer
	 * @param service
	 * @param iteration
	 */
	public ServiceEntry(Service consumer, Service service, double iteration)
	{
		this.consumer = consumer;
		this.service = service;
		qty = iteration;
		type = SE_TYPE_SIT;
	}

	public List<ServiceEntry> seList;

	/**
	 * Creates a service entry that allows an initial time and adjustment to be
	 * set to a service use value of -1 for time if it is not to be applied and
	 * only adjustments
	 * 
	 * @param service
	 * @param time
	 * @param adjustment
	 */
	public ServiceEntry(Service service, double time, double adjustment)
	{
		this.service = service;
		response = time;
		wait = adjustment;
		type = SE_TYPE_TIME;
	}

	public ServiceEntry(Date snapshot, VolumetricType vol, int qty)
	{
		when = snapshot;
		this.vol = vol;
		this.volume = qty;
		type = SE_TYPE_SNAP_VOL;
	}

	public ServiceEntry(Date snapshot, VolumetricType vol, Workstream ws,
			double multi)
	{
		// used by snapshot writer/reader
		type = SE_TYPE_SNAP_VOL_WS;
		when = snapshot;
		this.vol = vol;
		service = ws;
		qty = multi;

	}

	public ServiceEntry(Date snapshot, String description)
	{
		type = SE_TYPE_SNAPSHOT_SAVE;
		when = snapshot;
		this.description = description;

	}

	public ServiceEntry(String command, String args)
	{
		type = SE_TYPE_COMMAND;
		this.command = command;
		this.args = args;
	}
	
	public ServiceEntry(Date snapshot,Service service,int samplePeriod,HashMap<Long,Integer> countPerSample)
	{
		type=SE_TYPE_SAMPLE_COUNT;
		this.samplePeriod=samplePeriod;
		this.service=service;
		this.countPerSample=countPerSample;
		when = snapshot;
	}
	
	public String userView(){
		String view;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
		Date when2=new Date((long)(when.getTime()+((Double)response).longValue()));
		view=user+" | "+when.getTime()+" | "+when2.getTime()+ " | "+sdf.format(when)+" | "+sdf.format(when2);
		view+=" | "+response;
		view+=" | "+service.getType()+" | "+service.getServiceName()+" | "+service.getOperationName();
		view+=" | "+location+" | "+filename;
		return view;
	}

	public static String getUserViewHeader(){
		return "User | Start | End | Start d/t | End d/t | Response | Layer | Service | Operation | Server | File";
	}
	
	public int samplePeriod=60000;
	public HashMap<Long,Integer> countPerSample;
	public Service service, consumer = null;
	public VolumetricType vol;
	public Date when;
	public double response, wait = 0;
	public double qty = 1;
	public int type = -1;
	public double adjustment = 0;
	public int volume = 0;
	public String description;
	public String command = "";
	public String args = "";
	public String txn = null;
	public String location=null;
	public String filename="";
	public boolean loginEvent=false;
	public boolean logoutEvent=false;
	public String user=null;

	public static final int SE_TYPE_SNAP_VOL = 0;
	public static final int SE_TYPE_EVENT = 1;
	public static final int SE_TYPE_SAMPLE = 2;
	public static final int SE_TYPE_SUMMARY = 3;
	public static final int SE_TYPE_HIERARCHY = 4;
	public static final int SE_TYPE_VOL = 5;
	public static final int SE_TYPE_SET = 6;
	public static final int SE_TYPE_VIT = 7;
	public static final int SE_TYPE_SIT = 8;
	public static final int SE_TYPE_TIME = 9;
	public static final int SE_TYPE_SNAP_VOL_WS = 10;
	public static final int SE_TYPE_SNAPSHOT_SAVE = 11;
	public static final int SE_TYPE_SAMPLE_COUNT=12;
	public static final int SE_TYPE_COMMAND = 100;

}
