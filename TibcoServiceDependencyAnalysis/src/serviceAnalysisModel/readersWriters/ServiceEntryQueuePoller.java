package serviceAnalysisModel.readersWriters;

import java.util.concurrent.ConcurrentLinkedQueue;

public class ServiceEntryQueuePoller extends Thread
{
	public ServiceEntryQueuePoller(BWReader bwr,
			ConcurrentLinkedQueue<ServiceEntry> queue)
	{
		super("ServiceEntry Queue Handler");
		this.setDaemon(true);
		this.reader = bwr;
		this.queue = queue;
	}

	BWReader reader;
	ConcurrentLinkedQueue<ServiceEntry> queue;

	public void run()
	{
		while (run)
		{
			if (pause)
			{
				try
				{
					Thread.sleep(300);
				} catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}
			} else
			{
				ServiceEntry se = queue.poll();
				if (se != null)
				{
					try
					{
						reader.processServiceEntry(se);
					} catch (Exception e)
					{

						// added to catch concurrent error when removing
						// listener
					}

				} else
					try
					{
						Thread.sleep(300);
					} catch (InterruptedException e)
					{
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
			}
		}
	}

	public boolean run = true;
	public boolean pause = false;
}
