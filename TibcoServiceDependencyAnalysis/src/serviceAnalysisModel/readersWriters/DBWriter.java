package serviceAnalysisModel.readersWriters;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.Workstream;
import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisUI.MainForm;

public class DBWriter extends Thread
{

	public DBWriter(BWReader BWR, ConcurrentLinkedQueue<ServiceEntry> queue)
	{
		super("Database Writer");
		// so=s;
		// w=when;
		// r=response;
		bwr = BWR;
		this.queue = queue;
		con = createConnection();
	}

	private Service so;
	private Date w;
	private double r;
	private BWReader bwr;
	private ConcurrentLinkedQueue<ServiceEntry> queue;
	private Connection con;

	private Connection createConnection()
	{
		try
		{
			Connection c = DriverManager.getConnection(
					Controller.DBConnectionStr, Controller.DBUser,
					Controller.DBPasswd);
			// connectionFreePool.add(c);
			c.setAutoCommit(false);
			return c;
		} catch (SQLException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return null;
		}
	}

	public boolean exit = false;
	private int sleep = 0;

	@Override
	public void run()
	{
		// TODO Auto-generated method stub
		// System.out.println(Thread.currentThread().getId()+": getting connection");
		// con=bwr.getConnection();

		while (!exit)
		{
			ServiceEntry se = queue.poll();
			if (se == null)
				try
				{
					if (sleep > 10)
					{
						exit = true;
						con.close();
					}
					sleep(500);
					sleep++;
				} catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			else
			{
				sleep = (Math.max(sleep - 2, 0));
				if (con == null)
					exit = true;
				else
				{
					if (se.type == ServiceEntry.SE_TYPE_EVENT)
					{
						if (DBWriter.DBWriteServiceEvents)
							try
							{
								con.createStatement().executeUpdate(
										"insert into SERVICE_EVENT (SERVICE, EVENT_TIME, RESPONSE) values ('"
												+ se.service.getServiceName()
												+ "..."
												+ se.service.getOperationName()
												+ "'," + se.when.getTime()
												+ "," + se.response + ")");
								con.commit();
							} catch (SQLException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					}//nothing from here should get called anymore
					if (se.type == ServiceEntry.SE_TYPE_SNAPSHOT_SAVE)
					{
						try
						{
							// System.out.println("sample");
							con.createStatement().executeUpdate(
									"insert into SNAPSHOTS (SNAPSHOTTIMESTAMP, SNAPSHOTDESC) values ("
											+ se.when.getTime() + ",'"
											+ se.description + "')");
							con.commit();
						} catch (SQLException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (se.type == -1)
					{
						System.out.println("snapshot save complete");
						MainForm.sbar.setMessage("Save Snapshot Complete "
								+ se.when);
					}

					if (se.type == ServiceEntry.SE_TYPE_SAMPLE)
					{
						// save sample to the database here
						try
						{
							// System.out.println("sample");
							con.createStatement()
									.executeUpdate(
											"insert into SERVICE_SNAPSHOT (SNAPSHOTTIMESTAMP, SERVICE, RESPONSE, QTY,WAIT) values ("
													+ se.when.getTime()
													+ ",'"
													+ se.service
															.getServiceName()
													+ "..."
													+ se.service
															.getOperationName()
													+ "',"
													+ se.response
													+ ","
													+ se.qty
													+ ","
													+ se.wait
													+ ")");
							con.commit();
						} catch (SQLException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (se.type == ServiceEntry.SE_TYPE_SNAP_VOL)
					{
						try
						{
							long when = se.when.getTime();
							String vol = se.vol.getName();
							int qty = se.volume;

							String sqlValues = when + ",'" + vol + "'," + qty;
							System.out.println(sqlValues);

							con.createStatement().executeUpdate(
									"insert into VOLUMETRICS (SNAPSHOTTIMESTAMP, VOLUMETRIC, QTY) values ("
											+ sqlValues + ")");
							con.commit();
						} catch (SQLException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (se.type == ServiceEntry.SE_TYPE_SNAP_VOL_WS)
					{
						System.out.println("saving an vol iteration");
						long when = se.when.getTime();
						String vol = se.vol.getName();
						double qty = se.qty;
						String wsn = ((Workstream) se.service).getName();

						String sqlValues = when + ",'" + vol + "', '" + wsn
								+ "'," + qty;
						System.out.println(sqlValues);
						try
						{
							con.createStatement()
									.executeUpdate(
											"insert into VOLUMETRICS_HIER (SNAPSHOTTIMESTAMP, VOLUMETRIC, WORKSTREAM, ITERATION) values ("
													+ sqlValues + ")");
							con.commit();
						} catch (SQLException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					if (se.type == ServiceEntry.SE_TYPE_SUMMARY)
					{
						try
						{
							long when = se.when.getTime();
							String service = se.service.getServiceName()
									+ "..." + se.service.getOperationName();
							double avg = se.service.getAvgtime();
							double stddev = se.service.getOperationTimeStdDev();
							if (stddev != stddev)
								stddev = 0;
							double median = se.service
									.getOperationTimeLogMedian();
							if (median != median)
								median = 0; // check for NaN
							double mode = se.service.getOperationTimeLogMode();
							if (mode != mode)
								mode = 0;
							int c = se.service.getCount();
							String sqlValues = when + ",'" + service + "',"
									+ avg + "," + stddev + "," + median + ","
									+ mode + "," + c;

							//System.out.println(sqlValues);
							con.createStatement()
									.executeUpdate(
											"insert into SUMMARY_SNAPSHOT (SNAPSHOTTIMESTAMP, SERVICE, MEAN, STDDEV, MEDIAN, MODE, SAMPLECOUNT) values ("
													+ sqlValues + ")");
							con.commit();
						} catch (SQLException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
					if (se.type == ServiceEntry.SE_TYPE_HIERARCHY
							&& se.consumer == null)
					{
						try
						{
							// System.out.println("sample");
							con.createStatement()
									.executeUpdate(
											"insert into SERVICE (SNAPSHOTTIMESTAMP, SERVICE_NAME, SERVICE_OPERATION, SERVICE_LAYER,SERVICE_ADJ) values ("
													+ se.when.getTime()
													+ ",'"
													+ se.service
															.getServiceName()
													+ "','"
													+ se.service
															.getOperationName()
													+ "','"
													+ se.service.getType()
													+ "',"
													+ se.service
															.getExplicitAdjustments()
													+ ")");
							con.commit();
						} catch (SQLException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (se.type == ServiceEntry.SE_TYPE_SAMPLE_COUNT)
					{
						System.out.println("writing sample data for "+se.service.getSName());
						writeSampleCounts(con,se);
					}
					if (se.type == ServiceEntry.SE_TYPE_HIERARCHY
							&& se.consumer != null)
					{
						try
						{
							// System.out.println("sample");
							con.createStatement()
									.executeUpdate(
											"insert into SERVICE_HIER (SNAPSHOTTIMESTAMP, PARENT_SERVICE_NAME, PARENT_SERVICE_OPERATION, PARENT_SERVICE_LAYER, CHILD_SERVICE_NAME, CHILD_SERVICE_OPERATION, CHILD_SERVICE_LAYER) values ("
													+ se.when.getTime()
													+ ",'"
													+ se.consumer
															.getServiceName()
													+ "','"
													+ se.consumer
															.getOperationName()
													+ "','"
													+ se.consumer.getType()
													+ "','"
													+ se.service
															.getServiceName()
													+ "','"
													+ se.service
															.getOperationName()
													+ "','"
													+ se.service.getType()
													+ "')");
							con.commit();
						} catch (SQLException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
							try {
								con.rollback();
							} catch (SQLException e1) {
								
							}
						}

					}

				}
			}
		}
		// remove this reader

		bwr.RemoveDBWriter(this);
	}

	
	public void writeSampleCounts(Connection con,ServiceEntry se) 
	{
		try {
			String sname=se.service.getServiceName();
			String sop=se.service.getOperationName();
			String stype=se.service.getType();
			String f="insert into samplesize(SNAPSHOTTIMESTAMP,SERVICE_NAME,SERVICE_OPERATION,SERVICE_LAYER ,SAMPLESIZE) values("+se.when.getTime()+",'"+sname+"','"+sop+"','"+stype+"',"+se.samplePeriod+")";
			//System.out.println(f);
			con.createStatement().executeUpdate(f);
			String s="insert into samplecounts (SNAPSHOTTIMESTAMP, SERVICE_NAME, SERVICE_OPERATION, SERVICE_LAYER, SAMPLE, COUNT) values ("+se.when.getTime()
				+",'"+sname+"'"
				+",'"+sop+"'"
				+",'"+stype+"',";
			for(Long l:se.countPerSample.keySet())
			{
				int c=se.countPerSample.get(l);
				String ss=s+l+","+c+")";
				con.createStatement().executeUpdate(ss);
			}
			con.commit();
		} catch(SQLException sqle)
		{
			sqle.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	public static boolean DBWriteServiceEvents = false;
	public static boolean DBCheckEvent = true;
}
