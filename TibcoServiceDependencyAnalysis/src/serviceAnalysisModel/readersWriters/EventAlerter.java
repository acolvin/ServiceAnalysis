package serviceAnalysisModel.readersWriters;

import java.util.concurrent.ConcurrentHashMap;

import serviceAnalysisModel.Service;

public class EventAlerter {
	private static EventAlerter me=null;
	private static final Integer synconme=0; 
	
	private EventAlerter(){
		
	}
	
	public static EventAlerter getInstance(){
		synchronized(synconme){
			if(me==null){
				me=new EventAlerter();
			}
		}
		return me;
	}
	
	//rules storage
	//service, threshold
	private ConcurrentHashMap<Service, Double> rules=new ConcurrentHashMap<Service,Double>();
	
	public void addRule(Service service, Double threshold){
		rules.put(service, threshold);
	}
	
	public void removeAlert(Service service){
		rules.remove(service);
	}
	
	public void removeAllRules()
	{
		rules.clear();
	}
	
	//do alert
	public void alert(ServiceEntry se){
		if(rules.containsKey(se.service)){
			if(se.response>rules.get(se.service)){
				System.out.println("ALERT:"+se.when+":"+se.service.toString()+":"+se.user+":"+se.response);
			}
		}
	}
}
