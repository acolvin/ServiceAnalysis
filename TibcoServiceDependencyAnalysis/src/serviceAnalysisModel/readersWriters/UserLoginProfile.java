package serviceAnalysisModel.readersWriters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;



public class UserLoginProfile {

	public String user;
	private TreeMap<Long,UserLoginEvent> events;
	
	
	public UserLoginProfile(String user){
		this.user=user;
		events =  (new TreeMap<Long,UserLoginEvent>());
	}
	
	public void addEvent(UserLoginEvent event){
		synchronized(UserProfiles.lock){
			SortedMap<Long, UserLoginEvent> s = events.subMap(event.start-UserLoginEvent.SESSION_TIMEOUT, event.end+UserLoginEvent.SESSION_TIMEOUT+1);
			if(s.isEmpty()){
				//System.out.println("There are no events in range to look for merging");
				events.put(event.start, event);
				return;
			}
			UserLoginEvent ev=new UserLoginEvent(event);
			UserLoginEvent ev2=new UserLoginEvent(event);
			ArrayList<Long> removethese=new ArrayList<Long>();
			Set<Long> ks = s.keySet();
			Iterator<Long>it=ks.iterator();
			while(it.hasNext()){
				Long t=it.next();
				UserLoginEvent ev1 = events.get(t);
				//System.out.println("Comparing "+ev.toString()+" with "+ev1.toString());
				if(ev.addevent(ev1)){
					//System.out.println("event extended to "+ev);
					removethese.add(t);
				}
				/*UserLoginEvent e = ev.getUnion(s.get(t));
				if(e!=null){
					events.remove(t);
					ev=e;
					ev2=e;
				} else if(ev2!=null){
					events.put(ev2.start, ev2);
					ev2=null;
				}
				*/
				
			}
			for(Long t:removethese) events.remove(t);
			events.put(ev.start, ev);
		}
	}
	
	public void addEvent(String user, long start, long end){//used for restore
		UserLoginEvent ev=new UserLoginEvent(user, start,end);
		events.put(start, ev);
	}
	
	public boolean isLoggedIn(long time){
		synchronized(UserProfiles.lock){
		
			SortedMap<Long, UserLoginEvent> s = events.headMap(time+1);
			if(s.isEmpty())return false;
			//boolean result=false;
			//for(UserLoginEvent ev:s.values()){
			//	result=result|ev.isLoggedIn(time);
			//}
			//return result;
			return s.get(s.lastKey()).isLoggedIn(time);  //the last key should be ok to check for login as all other are earlier
		}
	}
	
	public Long getEarliestEvent(){
		synchronized(UserProfiles.lock){
			if(events.isEmpty())return null;
			return events.firstKey();
		}
	}
	
	public Long getLatestEvent(){
		synchronized(UserProfiles.lock){
			if(events.isEmpty())return null;
			return events.lastKey()+UserLoginEvent.SESSION_TIMEOUT+60000l;
		}
	}
	
	public boolean setLogout(long time){
		synchronized(UserProfiles.lock){
			if(isLoggedIn(time)){
				Long eStart = events.floorKey(time);
				if(eStart==null)return false;
				UserLoginEvent event=events.get(eStart);
				if(event.end<time) return false;
				if(event.end==time)return true;
				if(event.end-time<UserLoginEvent.SESSION_TIMEOUT){
					//we are bringing this event to a close early
					event.end=time;
					return true;
				}else{
					//we need to split the event - how do we do it? need a record of login events
					//can only assume it should start at the end of the original
					long origEnd=event.end;
					event.end=time;
					UserLoginEvent ev1 = new UserLoginEvent(event.user,time+1,origEnd);
					events.put(time+1, ev1);
					return true;
					
				}
				
				
			}
		}
		return false;
	}
	
	public Collection<UserLoginEvent> getEvents(){
		synchronized(UserProfiles.lock){
			return (new TreeMap<Long,UserLoginEvent>(events)).values();
		}
	}
}
