package serviceAnalysisModel.readersWriters;

import java.util.HashMap;

import serviceAnalysisModel.Service;

public interface BWStatsEventListener
{

	public void BWServiceOperationStat(Service s, java.util.Date time,
			double response, double average, double wait, double rate, String location);
	public void BWServiceOperationStat(Service s, java.util.Date time,
			double response, double average, double wait, HashMap<String,Double> rates, String location);

}
