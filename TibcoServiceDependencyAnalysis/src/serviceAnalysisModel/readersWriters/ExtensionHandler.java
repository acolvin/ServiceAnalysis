/**
 * 
 */
package serviceAnalysisModel.readersWriters;

import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;
import java.util.TreeSet;

//import com.sun.org.apache.xalan.internal.lib.Extensions;

/**
 * This class loads the extensions and calls the processLogFileLine.
 * <P>
 * If an extension does not recognise the line then it should return null.
 * <P>
 * The first extension in tha call chain to return a valid ServiceEntry will
 * stop further processing.
 * <P>
 * The singleton is created by calling getInstance and is initialised in the
 * BWReader class which also calls the process method for each log line.
 * 
 * @author apc
 * 
 */
public class ExtensionHandler
{

	private static ExtensionHandler service;
	private ServiceLoader<serviceAnalysisModel.readersWriters.extension.RealTimeReader> loader;

	/**
	 * Creates a new instance of ExtensionHandler. This is a private method as
	 * this is a singleton pattern
	 */
	private ExtensionHandler()
	{
		loader = ServiceLoader
				.load(serviceAnalysisModel.readersWriters.extension.RealTimeReader.class);
		Iterator<serviceAnalysisModel.readersWriters.extension.RealTimeReader> readers = loader
				.iterator();
		while (readers.hasNext())
		{
			serviceAnalysisModel.readersWriters.extension.RealTimeReader r = readers
					.next();
			extensions.add(r);
			System.out.println("Loaded extension "
					+ r.getClass().getCanonicalName());
		}
	}

	/**
	 * Retrieve the singleton static instance of ExtensionHandler.
	 */
	public static synchronized ExtensionHandler getInstance()
	{
		if (service == null)
		{
			service = new ExtensionHandler();
		}
		return service;
	}

	private TreeSet<serviceAnalysisModel.readersWriters.extension.RealTimeReader> extensions = new TreeSet<serviceAnalysisModel.readersWriters.extension.RealTimeReader>();

	/**
	 * Processes the log file line through the extensions until the first one
	 * returns non null
	 * 
	 * @param line
	 *            The string from the log file
	 * @return null or a ServiceEntry when gives the service being called, when
	 *         it was called, its response and its consuming service when known
	 *         (or null)
	 */
	public ServiceEntry ProcessLogFileLine(String line)
	{
		ServiceEntry success = null;

		try
		{
			Iterator<serviceAnalysisModel.readersWriters.extension.RealTimeReader> readers = extensions
					.iterator(); // loader.iterator();
			while (success == null && readers.hasNext())
			{

				serviceAnalysisModel.readersWriters.extension.RealTimeReader d = readers
						.next();
				//System.out.println(d.getClass());
				// System.out.println(d.getOrderNumber());
				success = d.processLogFileLine(line);
				// if(success!=null){
				// System.out.println(success.service.getSName()+" "+success.when+" "+success.response);
				// }
			}
		} catch (ServiceConfigurationError serviceError)
		{
			success = null;
			serviceError.printStackTrace();

		}
		return success;
	}

}
