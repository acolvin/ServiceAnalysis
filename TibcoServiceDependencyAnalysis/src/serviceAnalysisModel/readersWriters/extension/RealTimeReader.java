/**
 * 
 */
package serviceAnalysisModel.readersWriters.extension;

import serviceAnalysisModel.readersWriters.ServiceEntry;

/**
 * This abstract class is provided to support the ServiceLoader api provided in
 * java 6.
 * <P>
 * It allows the application to be extended with new file loaders. All log file
 * lines will be sent to the readers until the first one that processes it
 * successfully.
 * <P>
 * Each reader has an order number and readers are called in the order from
 * lowest first to highest
 * <P>
 * A couple of readers are provided as examples
 * <li>A comment line processor (lines starting with # or ; ) order 10.
 * <li>A model file reader. Reads lines from a model file which is defined by
 * the command line reader. Order 1
 * <P>
 * New readers should have an order number above 10 unless they need to process
 * before the comment reader
 * 
 * <P>
 * </P>
 * 
 * @author apc
 * 
 */
public abstract class RealTimeReader implements Comparable<RealTimeReader>
{

	/**
	 * Called to process the line of data
	 * 
	 * @param line
	 *            The line of data from the log file
	 * @return true if processed else false
	 */
	public abstract ServiceEntry processLogFileLine(String line);

	/**
	 * Specifies the call priority order of the extension (priority order 1-10
	 * are reserved for extensions that must process before comment lines)
	 * 
	 * @return an integer greater than 10
	 */
	public abstract int getOrderNumber();

	public int compareTo(RealTimeReader arg0)
	{
		// TODO Auto-generated method stub
		if (this.getOrderNumber() > ((RealTimeReader) arg0).getOrderNumber())
			return 1;
		if (this.getOrderNumber() < ((RealTimeReader) arg0).getOrderNumber())
			return -1;
		return 0;
	}

}
