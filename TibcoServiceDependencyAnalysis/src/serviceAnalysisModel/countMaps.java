package serviceAnalysisModel;

import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

public class countMaps
{
	private HashMap<Date, Integer> rCounts; // rolling count excluding actuals
	private HashMap<Date, Integer> aCounts; // count during that period
	private HashMap<Date, Integer> counts;
	private HashMap<Date, Double> averages; //averages per sample
	private HashMap<Date, Double> maximums; //averages per sample


	
	public HashMap<Date, Double> getMaximums() {
		return maximums;
	}

	public void setMaximums(HashMap<Date, Double> maximums) {
		this.maximums = maximums;
	}

	public HashMap<Date, Double> getAverages() {
		return averages;
	}

	public void setAverages(HashMap<Date, Double> averages) {
		this.averages = averages;
	}

	public HashMap<Date, Integer> getrCounts()
	{
		return rCounts;
	}

	public synchronized void setrCounts(HashMap<Date, Integer> rCounts)
	{
		this.rCounts = rCounts;
		calculateCounts();
	}

	public HashMap<Date, Integer> getaCounts()
	{
		return aCounts;
	}

	public synchronized void setaCounts(HashMap<Date, Integer> aCounts)
	{
		this.aCounts = aCounts;
		calculateCounts();
	}

	public synchronized HashMap<Date, Integer> getCounts()
	{
		return counts;
	}

	private void calculateCounts()
	{
		if (rCounts != null)
			counts = (HashMap<Date, Integer>) rCounts.clone();
		else
			counts = new HashMap<Date, Integer>();
		if (aCounts == null)
			return;
		for (Entry<Date, Integer> e : aCounts.entrySet())
		{
			Integer i = 0;
			if (counts.containsKey(e.getKey()))
			{

				i = counts.get(e.getKey()) + e.getValue();
			} else
			{
				// if(rCounts!=null)System.out.println("not in Counts, "+e.getKey()+" "+rCounts.containsKey(e.getKey()));
				i = e.getValue();
				// i=0;
			}
			counts.put(e.getKey(), i);
		}
	}

	public HashMap<Date, Integer> getRCounts()
	{
		return rCounts;
	}

}
