package serviceAnalysisModel.SAControl;

import serviceAnalysisModel.Service;

public class ServiceAddedEvent
{
	public ServiceAddedEvent(Service service)
	{
		this.service = service;
	}

	public Service getService()
	{
		return service;
	}

	Service service = null;
}
