package serviceAnalysisModel.SAControl;

public class MonitorChangeEvent
{
	public MonitorChangeEvent(String file, int eventType)
	{
		this.file = file;
		this.eventType = eventType;
	}

	private String file;
	private int eventType = 0;

	public static final int _ADD = 1;
	public static final int _DEL = 2;

	public int getEventType()
	{
		return eventType;
	}

	public String getFile()
	{
		return file;
	}
}
