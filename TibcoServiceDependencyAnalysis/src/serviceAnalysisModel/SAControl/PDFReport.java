package serviceAnalysisModel.SAControl;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import javax.swing.SwingUtilities;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.batik.svggen.SVGGraphics2DIOException;
import org.jfree.chart.JFreeChart;
import org.w3c.dom.DOMImplementation;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.DefaultFontMapper;
import com.lowagie.text.pdf.FontMapper;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;

import serviceAnalysisModel.Service;
import serviceAnalysisUI.CountGraph;

public class PDFReport
{
	public PDFReport(Service service)
	{
		this.service = service;
	}

	private Service service = null;

	// private JFreeChart frequencyChart=null;

	public void produceServiceReport(JFreeChart frequencyChart)
	{

		CountGraph a = new CountGraph(service);
		String fff=service.getSName();
		fff=fff.replace('/', '_');
		fff=fff.replace('\\', '_');
		// write the chart to a PDF file...
		File fileName = new File(System.getProperty("user.home") + "/"
				+ fff + ".pdf");
		try
		{
			saveChartAsPDF(fileName, a.getChart(), 500, 300,
					new DefaultFontMapper(), frequencyChart);
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private OutputStream createChartSVG(JFreeChart chart, float width,
			float height)
	{

		OutputStream os = new ByteArrayOutputStream();
		DOMImplementation domImpl = GenericDOMImplementation
				.getDOMImplementation();
		String svgNS = "http://www.w3.org/2000/svg";
		org.w3c.dom.Document document = domImpl.createDocument(svgNS, "svg",
				null);
		SVGGraphics2D svgGenerator = new SVGGraphics2D(document);
		chart.draw((Graphics2D) svgGenerator, new Rectangle2D.Double(0, 0,
				width, height));

		try
		{
			Writer out = new OutputStreamWriter(os, "UTF-8");
			svgGenerator.stream(out, false /* use css */);
			os.flush();
		} catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SVGGraphics2DIOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return os;
	}

	private void saveChartAsPDF(File file, JFreeChart chart, float width,
			float height, FontMapper mapper, JFreeChart frequencyChart)
			throws IOException
	{

		OutputStream out = new BufferedOutputStream(new FileOutputStream(file));
		writeChartAsPDF(out, chart, width, height, mapper, frequencyChart);
		out.close();
	}

	private void writeChartAsPDF(OutputStream out, JFreeChart chart,
			float width, float height, FontMapper mapper,
			JFreeChart frequencyChart) throws IOException
	{
		Rectangle pagesize = new Rectangle(width, height);
		Document document = new Document();// pagesize, 50, 50, 50, 50);
		pagesize = document.getPageSize();
		try
		{
			PdfWriter writer = PdfWriter.getInstance(document, out);
			document.addAuthor("Service Analysis");
			document.addSubject("Service Report");

			document.open();
			PdfContentByte cb = writer.getDirectContent();

			PdfTemplate tp = cb.createTemplate(pagesize.width(),
					pagesize.height());
			Graphics2D g2 = tp.createGraphics(pagesize.width(),
					pagesize.height(), mapper);
			System.out.println(pagesize.width());
			System.out.println(pagesize.height());

			int swidth = SwingUtilities.computeStringWidth(g2.getFontMetrics(),
					"Service Report for " + service.getSName());
			g2.drawString("Service Report for " + service.getSName(),
					(pagesize.width() - swidth) / 2, 40);

			int ypos = 72, ld = 12;

			String line = "Mean=" + service.getAvgtime();
			g2.drawString(line, 50, ypos);
			ypos += ld;
			line = "Mode (estimate)=" + service.getOperationTimeLogMode();
			g2.drawString(line, 50, ypos);
			ypos += ld;
			line = "Geometric Mean=" + service.getOperationTimeLogMedian();
			g2.drawString(line, 50, ypos);
			ypos += ld;
			line = "Median=" + service.getPercentileSamples(50);
			g2.drawString(line, 50, ypos);
			ypos += ld;
			line = "Harmonic Mean=" + service.getHmean();
			g2.drawString(line, 50, ypos);
			ypos += ld;

			line = "Number of Calls=" + service.getCount();
			g2.drawString(line, 50, ypos);
			ypos += ld;
			line = "Average Wait Times=" + service.getAvgWaitTime();
			g2.drawString(line, 50, ypos);
			ypos += ld;
			line = "Standard Deviation=" + service.getOperationTimeStdDev();
			g2.drawString(line, 50, ypos);
			ypos += ld;
			line = "95th Percentile=" + service.getPercentileSamples(95);
			g2.drawString(line, 50, ypos);
			ypos += ld;
			line = "99th Percentile=" + service.getPercentileSamples(99);
			g2.drawString(line, 50, ypos);
			ypos += ld;
			line = "Number of Dependent Services="
					+ service.getDependentCount();
			g2.drawString(line, 50, ypos);
			ypos += ld;
			line = "Number of Consumering Services="
					+ service.getConsumerCount();
			g2.drawString(line, 50, ypos);
			ypos += ld;

			Rectangle2D r2D = new Rectangle2D.Double(
					(pagesize.width() - width) / 2, ypos, width,
					pagesize.height() - ypos - 40);
			if (frequencyChart != null)
				frequencyChart.draw(g2, r2D);

			g2.dispose();
			cb.addTemplate(tp, 0, 0);

			document.newPage();
			cb = writer.getDirectContent();

			tp = cb.createTemplate(pagesize.width(), pagesize.height());
			g2 = tp.createGraphics(pagesize.width(), pagesize.height(), mapper);

			// tp.rectangle((pagesize.width()-width)/2-5, 100-5, width+10,
			// height+10);
			r2D = new Rectangle2D.Double(50, 25, (pagesize.width() - 100),
					pagesize.height() - 50);
			// r2D = new Rectangle2D.Double((pagesize.width()-width)/2, ypos,
			// width, pagesize.height()-(height+100));
			// g2.drawRect((new Float((pagesize.width()-width)/2)).intValue()-5,
			// ypos-5, (new Float(width)).intValue()+10, (new
			// Float(height)).intValue()+10);
			chart.draw(g2, r2D);
			g2.dispose();
			cb.addTemplate(tp, 0, 0);
		} catch (DocumentException de)
		{
			System.err.println(de.getMessage());
		}
		document.close();
	}

}
