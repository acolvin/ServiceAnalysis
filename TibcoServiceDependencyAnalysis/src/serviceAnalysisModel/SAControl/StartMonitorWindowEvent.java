package serviceAnalysisModel.SAControl;

import serviceAnalysisModel.Service;

public class StartMonitorWindowEvent
{

	public StartMonitorWindowEvent(Service service, int x, int y, int w, int h)
	{
		this.service = service;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}

	Service service;
	int x = 0, y = 0, w = 500, h = 300;

	private int time = 60000;

	public int getTime()
	{
		return time;
	}

	boolean title=true;
	
	public void showTitle(boolean on)
	{
		title=on;
	}
	
	public boolean isTitle()
	{
		return title;
	}
	
	public void setTime(int time)
	{
		this.time = time;
	}

	public Service getService()
	{
		return service;
	}

	public int getX()
	{
		return x;
	}

	public int getW()
	{
		return w;
	}

	public int getH()
	{
		return h;
	}

	public int getY()
	{
		return y;
	}

}
