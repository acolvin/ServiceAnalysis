package serviceAnalysisModel.SAControl;

import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamTee extends OutputStream {

	private StringBuilder buffer;	
	private TeeConsumer consumer;
    public OutputStream target;
    
    public OutputStreamTee(TeeConsumer consumer, OutputStream target)
    {
    	buffer=new StringBuilder(128);
    	this.consumer=consumer;
    	this.target=target;
    }
    
    public void close() throws IOException
    {
    	flush();
    	consumer=null;
    	closed=true;
    	target=getTarget();
    }
    
    private boolean closed=false;
    
    public OutputStream getTarget()
    {
    	if(target instanceof OutputStreamTee)
    	{
    		OutputStreamTee o=(OutputStreamTee)target;
    		if(o.closed==true) return o.getTarget();
    		else return o;
    	}
    	else return target;
    }
    
    public void flush() throws IOException
    {
    	if(consumer!=null)
    	{
    		consumer.appendText(buffer.toString());
			buffer.delete(0, buffer.length());
    	}
    	if(target!=null)target.flush();
    }
    
    public String toString()
    {
    	return "TeeOutputStream "+consumer+" "+target;
    }
    
    public void write(byte[] b) throws IOException
    {
    	//System.out.println("im coming out");
    	for(int i=0;i<b.length;i++){ write((int)b[i]);}
    }
    
	@Override
	public void write(int b) throws IOException {
		//System.out.println("write "+b);
		if(consumer!=null)
		{
			
			char c = (char) b;
			//System.out.println("write to tee "+c);
			String value = Character.toString(c);
			buffer.append(value);
			if (value.equals("\n")) {
				consumer.appendText(buffer.toString());
				buffer.delete(0, buffer.length());
            
			}
		}
        if(target!=null)target.write(b);
	}

}
