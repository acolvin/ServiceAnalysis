package serviceAnalysisModel.SAControl;

import java.util.Date;

public interface ControlMBean
{
	public void clearServices();

	public void pauseReaders();

	public void RestartReaders();

	public void resetServiceTimings();

	public void saveTimings(String desc);

	public void loadTimings(Date d);

	public void shutdown();

	public void loadFile(String filename);

	public void startUDPListener(int port);

	public void stopUDPListener();

	public void startJMSListener();

	public void stopJMSListener();

	public void loadMatchFile(String filename);

	public void resetRateMeters();

	public void monitorFile(String filename);

	public void removeFileMonitor(String filename);

	public String getServiceDetails(String service, String operation);

	public void printServices();

	public void outputToFile(String filename);

	public void closeOutputFile();

}
