package serviceAnalysisModel.SAControl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

public class StdOutHandler extends StreamHandler implements TeeConsumer
{

	public StdOutHandler()
	{
		super(System.out, new SimpleFormatter());
		this.setLevel(Level.ALL);
	}

	public void close()
	{
		flush();
	}

	public void publish(LogRecord record)
	{
		super.publish(record);
		flush();
	}

	@Override
	public void appendText(String text) {
		out.append(text);
		for(TeeConsumer consumer:teeConsumers)
		{
			consumer.appendText(text);
		}
		//do not add any system.out.print statements here or you will get an infinite loop
	}
	
	public static void addConsumer(TeeConsumer consumer)
	{
		teeConsumers.add(consumer);
	}
	
	public static void removeConsumer(TeeConsumer consumer)
	{
		teeConsumers.remove(consumer);
	}
	
	private static List<TeeConsumer> teeConsumers=Collections.synchronizedList(new ArrayList<TeeConsumer>());
	
	public static void clearSystemOutBuffer()
	{
		out=new StringBuffer(10240);
	}
	
	public static StringBuffer out=new StringBuffer(10240);
}
