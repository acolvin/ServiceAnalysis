package serviceAnalysisModel.SAControl;

import serviceAnalysisModel.Service;

public interface MonitorWindowListener
{
	public void StartMonitor(StartMonitorWindowEvent event);

	public void dumpWindows();
	
	public void adjustWindow(String name,int x,int y,int w,int h);
	
	public void setHighlight(String name, boolean on);
	
	public void addColumn(String name, String column, boolean on);
	
	public void setColumn(String name, String column, int width);
	
	public void openWindow(String name, Service s, int x, int y, int w, int h, int period);
}
