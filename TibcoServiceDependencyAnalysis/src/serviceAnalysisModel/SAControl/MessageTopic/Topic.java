package serviceAnalysisModel.SAControl.MessageTopic;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class Topic {

	private Topic()
	{
		
	}
	
	
	private static Topic instance=null;
	
	public synchronized static Topic getInstance()
	{
		if(instance==null) instance=new Topic();
		return instance;
	}
	
	
	private CopyOnWriteArrayList<MessageListener> listeners=new CopyOnWriteArrayList<MessageListener>();
	
	public void addListener(MessageListener listener)
	{
		listeners.add(listener);
	}
	
	public void removeListener(MessageListener listener)
	{
		listeners.remove(listener);
	}

	private LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<String>();
	
	public void addMessage(String message)
	{
		try {
			if(sender==null)startSender();
			if(!sender.getRunningStatus()) startSender();
			queue.put(message);
		} catch (InterruptedException e) {
			System.out.println("message sending was interrupted. Message: "+message);
		}
	}
	
	private void startSender()
	{
		sender=new Sender(); 
		sender.start();
	}
	
	private Sender sender = null;
	
	private class Sender extends Thread
	{
		public Sender()
		{
			setDaemon(true);
			setName("Message Sender");			
		}
		
		private boolean run=false;
		
		public boolean getRunningStatus()
		{
			return run;
		}
		
		public void interrupt()
		{
			run=false;
			super.interrupt();
		}
		
		public void run()
		{
			run=true;
			while(run)
			{
				try {
					String message=queue.take();
					sendToListeners(message);
					
				} catch (InterruptedException e) {
					run=false;
				}
			}
		}
	}

	public void sendToListeners(String message) {
		for(MessageListener l:listeners)
		{
			l.handleMessage(message);
		}
		
	}
	
} 
