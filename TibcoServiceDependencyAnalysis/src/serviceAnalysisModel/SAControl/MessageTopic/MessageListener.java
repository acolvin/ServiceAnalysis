package serviceAnalysisModel.SAControl.MessageTopic;

public interface MessageListener {
	public void handleMessage(String message);
}
