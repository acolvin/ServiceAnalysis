package serviceAnalysisModel.SAControl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;



import serviceAnalysisModel.Service;

public class TextReports
{
	public TextReports()
	{
//		System.err.println("initialising textreports");
//		if(!(out instanceof OutputStream))out=System.out;
//		if(!(tee instanceof OutputStreamTee)) tee=null;
		
	//	for (int i = 0; i < fields.length; i++)
		//	allowedColumns.add(fields[i]);

	}
	
	

	public static void rollingCountReport(Service s, int size)
	{
		HashMap<Date, Integer> counts = s.getRollingCount(size).getCounts();
		try
		{
			out.write((s.getSName() + ": Number of Requests per Rolling "
					+ s.ratem.getSamplePeriod() / 1000 * size
					+ " seconds against time" + '\n').getBytes());
			out.write("Date(ms),Date,Count\n".getBytes());
			out.flush();
			for (Entry<Date, Integer> d : counts.entrySet())
			{
				String line = d.getKey().getTime() + ","
						+ d.getKey().toString() + "," + (d.getValue()) + '\n';
				out.write(line.getBytes());
			}
			out.flush();

		} catch (IOException e)
		{

		}
	}

	public static void sampleCountReport(Service s, int size)
	{
		HashMap<Date, Integer> counts = s.getSampleCount(size);
		List<Date> col=new ArrayList<Date>();
		try
		{
			out.write((s.getSName() + ": Number of Requests per "
					+ s.ratem.getSamplePeriod() / 1000 * size
					+ " seconds against time" + '\n').getBytes());
			out.write("Date(ms),Date,Count\n".getBytes());
			out.flush();
			for(Date d:counts.keySet())col.add(d);
			Collections.sort(col);
			for (Date d : col)
			{
				String line = d.getTime() + ","
						+ d.toString() + "," + (counts.get(d)) + '\n';
				out.write(line.getBytes());
			}
			out.flush();

		} catch (IOException e)
		{

		}
	}

	public static void rateReport(Service s, int size)
	{
		HashMap<Date, Double> rates = s.getRates(size);
		try
		{
			out.write((s.getSName() + ": Transactions Rates" + '\n').getBytes());
			out.write(("Date(ms),Date,Rate (TPS),Transactions per "
					+ s.ratem.getSamplePeriod() / 1000 * size + " seconds\n")
					.getBytes());
			out.flush();

		} catch (IOException e)
		{

		}

		for (Entry<Date, Double> d : rates.entrySet())
		{
			String line = d.getKey().getTime()
					+ ","
					+ d.getKey().toString()
					+ ","
					+ (d.getValue())
					+ ","
					+ (d.getValue() * size * (s.ratem.getSamplePeriod() / 1000))
					+ '\n';
			try
			{
				out.write(line.getBytes());
				out.flush();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public static void ServiceName(List<Service> SO)
	{
		String title = "Index, Service, Operation, Count, Mean, Wait,StdDev,95th %ile,99th %ile\n";
		OutputStream out;
		if(TextReports.out==null)out=System.out;
		else out=TextReports.out;
		try
		{
			//PrintStream p=new PrintStream(out);
			//System.out.print("rep "+out);
			//p.print(title);
			//System.out.write(title.getBytes());
			out.write(title.getBytes());
			//System.err.println("TextReport: System.out="+System.out);
			//System.err.println("TextReport: write out="+out);
			int i = 0;
			for (Service s : SO)
			{
				String t = i + ", ";
				t += s.getServiceName() + ", ";
				t += s.getOperationName() + ", ";
				t += s.getCount() + ", ";
				t += s.getAvgtime() + ", ";
				t += s.getAvgWaitTime() + ", ";
				t += s.getOperationTimeStdDev() + ", ";
				t += s.getPercentileSamples(95.0d) + ", ";
				t += s.getPercentileSamples(99.0d);
				t += "\n";
				//p.print(t);
				out.write(t.getBytes());
				i++;

			}
			//p.flush();
			out.flush();
			

		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	public ArrayList<String> getReports()
	{
		ArrayList<String> a=new ArrayList<String>();
		for(String s:reports.keySet()) a.add(s);
		return a;
	}

	private HashMap<String, ArrayList<String>> reports = new HashMap<String, ArrayList<String>>();
	private ArrayList<String> allowedColumns = new ArrayList<String>();
	private final String[] fields =
	{ "Service", "Mean", "StdDev", "Count", "Percentile" };

	public void addColumnToReport(String reportName, String columnType, Object arg)
	{
		Method[] methods=this.getClass().getMethods();
		String methodName="add"+columnType+"Field";
		String printName="print"+columnType+"Tag";
		//System.out.println("looking for method: "+methodName);
		Method callingMethod=null;
		Method printMethod=null;
		for(Method method:methods)
		{
			if(method.getName().contains(methodName))
			{
				//System.out.println("found method");
				callingMethod=method;
			}
			if(method.getName().contains(printName))
				printMethod=method;
			//else
			//{
			//	System.out.println("couldn't found method: "+method.getName());
			//}
		}
		//System.out.println(printMethod.getName());
		if(callingMethod!=null)
			try {
				if (!reports.containsKey(reportName))
					reports.put(reportName, new ArrayList<String>());
				ArrayList<String> report = reports.get(reportName);
				if(arg==null)
					callingMethod.invoke(this,report);
				else
					callingMethod.invoke(this,report, arg);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else if(printMethod!=null)
		{//no calling method define but there is a print method so use  default insertion
			System.out.println("in print method found "+columnType);
				if (!reports.containsKey(reportName))
					reports.put(reportName, new ArrayList<String>());
				ArrayList<String> report = reports.get(reportName);
				if(arg==null)
					addField(report,columnType);
				else
					addField(report,columnType+"::"+arg.toString());
			
		}
		
	}

	public static ArrayList<String> getTagList()
	{
		ArrayList<String> a=new ArrayList<String>();
		Method[] methods=TextReports.class.getMethods();
		for(Method m:methods)
		{
			if(m.getName().contains("print") && m.getName().endsWith("Tag"))
			{
				String s=m.getName();
				String ss=s.substring(5, s.length()-3);
				a.add(ss);
			}
		}
		return a;
	}
	
	public String getTag(String tag,boolean header,Object[] obs)
	{
		String[] split=tag.split("::");
		String tagname=split[0];
		Object[] newobs;
		if(obs!=null)
		{
			newobs=new Object[obs.length+split.length-1];
			for(int i=0;i<newobs.length;i++)
			{
				if(i<obs.length)newobs[i]=obs[i];
				else newobs[i]=split[i-obs.length+1];
			}
		}
		else if(split.length>1)
		{
			newobs=new Object[split.length-1];
			for(int i=1;i<split.length;i++)
				newobs[i-1]=split[i];
		}
		else
			newobs=obs;
		String output="";
		String methodName="print"+tagname+"Tag";
		Method printMethod=null;
		//System.out.println(obs);
		try {
			printMethod=this.getClass().getMethod(methodName, boolean.class, Object[].class);
			//System.out.println(printMethod);
			if(printMethod!=null)
				output=(String)printMethod.invoke(this, header,newobs);
			else
				output="err: NoMethod";
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			output="err: NotAllowed";
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			output="err: NoMethod "+methodName+"(boolean,Object[])";
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			output="err: WrongArgs";
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			output="err: NotAllowed";
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			output="err: NotAllowed";
		}
		
		return output;
	}
	
	public String printMinTag(boolean header,Object[] obs)
	{
		if(header) return "Min";
		else if(obs[0] instanceof Service)
		{
			return ""+((Service)obs[0]).getMinTime();
		}
		else 
			return "err: NoService";
	}
	
	public String printMaxTag(boolean header,Object[] obs)
	{
		if(header) return "Max";
		else if(obs[0] instanceof Service)
		{
			return ""+((Service)obs[0]).getMinTime();
		}
		else 
			return "err: NoService";
	}
	
	public String printMeanTag(boolean header,Object[] obs)
	{
		if(header) return "Mean";
		else if(obs[0] instanceof Service) 
		{
			return ""+((Service)obs[0]).getAvgtime();
		}
		else 
			return "err: NoService";
	}
	
	public String printPercentileTag(boolean header,Object[] obs)
	{
		if(obs==null) return "Error: No Value";
		if(header) 
		{
			String s="Percentile: ";
			try {
				Double d=Double.parseDouble((String) obs[0]);
				s=s+obs[0];
				return s;
			} catch(NumberFormatException nfe)
			{
				return "Error: Invalid Number "+obs[0]; 
			}
			
		}
		else if(obs[0] instanceof Service) 
		{
			String s="";
			try {
				Double d=Double.parseDouble((String) obs[1]);
				s=s+((Service)obs[0]).getPercentileSamples(d);
				return s;
			} catch(NumberFormatException nfe)
			{
				return "Error: Invalid Number "+obs[0]; 
			}
			
		}
		else 
			return "err: NoService";
	}
	
	public String printServiceTag(boolean header,Object[] obs)
	{
		if(header) return "Service";
		else if(obs[0] instanceof Service) 
		{
			return ""+((Service)obs[0]).getServiceName();
		}
		else 
			return "err: NoService";
	}
	
	public String printOperationTag(boolean header,Object[] obs)
	{
		if(header) return "Operation";
		else if(obs[0] instanceof Service) 
		{
			return ""+((Service)obs[0]).getOperationName();
		}
		else 
			return "err: NoService";
	}
	
	public String printStdDevTag(boolean header,Object[] obs)
	{
		if(header) return "Standard Deviation";
		else if(obs[0] instanceof Service) 
		{
			return ""+((Service)obs[0]).getOperationTimeStdDev();
		}
		else 
			return "err: NoService";
	}
	
	public String printCountTag(boolean header,Object[] obs)
	{
		if(header) return "Number of Samples";
		else if(obs[0] instanceof Service) 
		{
			return ""+((Service)obs[0]).getCount();
		}
		else 
			return "err: NoService";
	}
	
	public String printHMeanTag(boolean header, Object[] obs)
	{
		if(header) return "Harmonic Mean";
		else if(obs[0] instanceof Service)
		{
			return ""+((Service)obs[0]).getHmean();
		}
		else 
			return "err: NoService";
	}
	
	public String printGMeanTag(boolean header, Object[] obs)
	{
		if(header) return "Geometric Mean";
		else if(obs[0] instanceof Service)
		{
			return ""+((Service)obs[0]).getGmean();
		}
		else 
			return "err: NoService";
	}
	
	public String printLogModeTag(boolean header, Object[] obs)
	{
		if(header) return "Log-Normal Modal Value";
		else if(obs[0] instanceof Service)
		{
			return ""+((Service)obs[0]).getOperationTimeLogMode();
		}
		else 
			return "err: NoService";
	}
	public String printConsumersCountTag(boolean header, Object[] obs)
	{
		if(header) return "Number of Consumers";
		else if(obs[0] instanceof Service)
		{
			return ""+((Service)obs[0]).getConsumerCount();
		}
		else 
			return "err: NoService";
	}
	
	public String printDependantCountTag(boolean header, Object[] obs)
	{
		if(header) return "Number of Dependants";
		else if(obs[0] instanceof Service)
		{
			return ""+((Service)obs[0]).getDependentCount();
		}
		else 
			return "err: NoService";
	}
	
	public String printDependantsTag(boolean header, Object[] obs)
	{
		if(obs==null && !header)return "err: NoService";
		if(header ) return "Dependants of Service";
		else if(!header & obs[0] instanceof Service)
		{
			Service service=(Service)obs[0];
			Iterator<Service> it=service.getDependents();
			String s=""+service.getSName()+'\n';
			while(it.hasNext())
			{
				s=s+'\t'+it.next().getSName()+'\n';
			}
			return s;
		}
		else 
			return "err: NoService";
	}	
	
	public String printWaitTag(boolean header, Object[] obs)
	{
		if(header) return "Wait Time";
		else if(obs[0] instanceof Service)
		{
			return ""+((Service)obs[0]).getAvgWaitTime();
		}
		else 
			return "err: NoService";
	}
	
	public void printReport(String reportName)
	{
		String line = "";
		int cNum = 1;

		if (reports.containsKey(reportName))
		{
			List<String> columns = reports.get(reportName);
			// title line
			for (String column : columns)
			{
				String t = column;

				{
					t=getTag(column,true,null);
					//if(!column.equals("Mean"))t = column;
				}
				if (cNum++ == 1)
					line = t;
				else
					line += "," + t;

			}
			//System.out.println(line);
			line += '\n';
			try
			{
				out.write(line.getBytes());
				out.flush();

			} catch (IOException e)
			{

			}

			// rows
			for (Service s : Controller.getController().getServices())
			{
				
				cNum = 1;
				line = "";
				for (String column : columns)
				{
				

					{
						Object[] obs={s};
						String t = getTag(column,false,obs);
						if (cNum++ == 1)
							line = t;
						else
							line += "," + t;

					}
				}
				line += '\n';
				try
				{
					out.write(line.getBytes());
					out.flush();

				} catch (IOException e)
				{

				}
			}
		}
	}
	
	
	private void addField(ArrayList<String> report,String name)
	{
		report.add(name);
	}
/*
	public void addPercentileField(ArrayList<String> report, Object value)
	{
		String s = "Percentile::" + value.toString();
		//System.out.println("adding percentile: "+s);
		report.add(s);
	}
*/

	public void addServiceField(ArrayList<String> report)
	{
		report.add("Service");
		report.add("Operation");
	}

//	public void addStdDevField(ArrayList<String> report)
//	{
//		report.add("StdDev");
//	}

	public static void openFile(String filename)
	{
		File f = new File(filename);
		if (f.exists() && !f.isFile())
			return;
		if (!f.exists())
			try
			{
				f.createNewFile();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
		if (f.exists() && f.canWrite())
		{
			try
			{
				//System.out.println("outputstream "+tee);
				if(tee==null)
					out = new FileOutputStream(f);
				else
					tee.target=new FileOutputStream(f);
			} catch (FileNotFoundException e)
			{
				// TODO Auto-generated catch block

				e.printStackTrace();
				return;
			}
		} else
			return;

	}
	
	public static void TeeOutputStream(TeeConsumer consumer)
	{
		
		OutputStream o;
		if(out==null)o=System.out;
		else o=out;
		tee=new OutputStreamTee(consumer,o);
		out=tee;
		//System.out.println(out);
	}
	
	public static void RemoveTeeOutputStream()
	{
		if(tee.target!=null)out=tee.getTarget();
		if(!(out instanceof OutputStreamTee))
		{
			tee=null;
			out=null;
		}
		else
		{
			if(out!=null)
			{
				tee=(OutputStreamTee) out;
				out=tee.getTarget();
			}
		}
		//System.out.println("removing tee "+out);
	}
	
	

	public static void closeFile()
	{
		//System.out.println("closefile called, tee="+tee);
		if (out!=null && out != System.out && out!=tee)
		{
			try
			{
				out.flush();
				out.close();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			out = System.out;
			//System.out.println(out);
		}
		if(out!=null && out==tee && tee.target!=System.out)
		{
			try {
				tee.target.flush();
				tee.target.close();
				tee.target=System.out;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//System.out.println("tee="+tee);
	}

	
	private static OutputStream out=System.out;
	private static OutputStreamTee tee=null;
}
