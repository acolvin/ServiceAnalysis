package serviceAnalysisModel.SAControl;

public interface DataModelChangeListener
{
	public void modelChanged();

	public void dataChanged();
}
