package serviceAnalysisModel.SAControl;

import java.awt.Frame;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import javax.swing.JFrame;

import serviceAnalysisModel.Service;

public class Control implements ControlMBean
{

	@Override
	public void clearServices()
	{
		JMXServer.control.ClearServices();

	}

	@Override
	public void pauseReaders()
	{
		// TODO Auto-generated method stub
		JMXServer.control.PauseReaders();

	}

	@Override
	public void RestartReaders()
	{
		// TODO Auto-generated method stub
		JMXServer.control.RestartReaders();
	}

	@Override
	public void resetServiceTimings()
	{
		// TODO Auto-generated method stub
		JMXServer.control.resetServiceTimings();
	}

	@Override
	public void saveTimings(String desc)
	{
		// TODO Auto-generated method stub
		JMXServer.control.saveTimings(desc);
	}

	@Override
	public void loadTimings(Date d)
	{
		// TODO Auto-generated method stub
		JMXServer.control.loadTimings(d);
	}

	@Override
	public void shutdown()
	{
		// TODO Auto-generated method stub
		JMXServer.control.shutdown();
		//force close any frames
		for(Frame f:JFrame.getFrames()) f.dispose();
	}

	@Override
	public void loadFile(String filename)
	{
		// TODO Auto-generated method stub
		JMXServer.control.loadFile(filename);
	}

	@Override
	public void startUDPListener(int port)
	{
		// TODO Auto-generated method stub
		JMXServer.control.startUDPListener(port);
	}

	public void printServices()
	{
		TextReports.ServiceName(JMXServer.control.getServices());
	}

	public void outputToFile(String filename)
	{
		TextReports.openFile(filename);
	}

	public void closeOutputFile()
	{
		TextReports.closeFile();
	}

	@Override
	public void stopUDPListener()
	{
		// TODO Auto-generated method stub
		JMXServer.control.stopUDPListener();
	}

	@Override
	public void loadMatchFile(String filename)
	{
		// TODO Auto-generated method stub
		try
		{
			JMXServer.control.loadMatchFile(filename);
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void resetRateMeters()
	{
		// TODO Auto-generated method stub
		JMXServer.control.resetRateMeters();
	}

	@Override
	public void monitorFile(String filename)
	{
		// TODO Auto-generated method stub
		JMXServer.control.monitorFile(filename);
	}

	@Override
	public void removeFileMonitor(String filename)
	{
		// TODO Auto-generated method stub
		JMXServer.control.removeFileMonitor(filename);
	}

	@Override
	public String getServiceDetails(String service, String operation)
	{
		// TODO Auto-generated method stub
		Service s = new Service("BW", service, operation, "");
		String out = "";
		if (JMXServer.control.getServices().contains(s))
		{
			s = JMXServer.control.getServices().get(
					JMXServer.control.getServices().indexOf(s));
			out = s.getSName() + "," + s.getAvgtime() + "," + s.getCount()
					+ "," + s.getOperationTimeStdDev();
		}
		return out;
	}

	@Override
	public void startJMSListener()
	{
		// needed because of JMX server classpath issues

		JMXServer.control.startJMSListenerFromJMX();
	}

	@Override
	public void stopJMSListener()
	{
		// TODO Auto-generated method stub
		JMXServer.control.stopJMSListener();
	}

}
