package serviceAnalysisModel.SAControl;

import java.lang.management.ManagementFactory;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

public class JMXServer extends Thread
{
	public JMXServer(Controller control,boolean daemon)
	{
		super("JMX Thread");
		this.setDaemon(daemon);
		JMXServer.control = control;
		mbs = ManagementFactory.getPlatformMBeanServer();
		try
		{
			name = new ObjectName("serviceAnalysisModel.SAControl:type=Control");
			Control mbean = new Control();
			mbs.registerMBean(mbean, name);

		} catch (MalformedObjectNameException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstanceAlreadyExistsException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MBeanRegistrationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotCompliantMBeanException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void run()
	{
		try
		{
			while(true) //this thread keeps the headless version alive
				Thread.sleep(1000l);
		} catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			System.out.println("Exiting");
		}
	}

	public static Controller control;
	MBeanServer mbs;
	ObjectName name;
}
