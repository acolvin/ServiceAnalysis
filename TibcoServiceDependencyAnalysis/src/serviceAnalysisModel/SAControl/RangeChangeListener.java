package serviceAnalysisModel.SAControl;

public interface RangeChangeListener {
	public void addRange(String field, String service, float green, float orange, float red);
	public void removeRange(String field, String service);
}
