package serviceAnalysisModel.SAControl;

import java.awt.Frame;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.SocketException;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SimpleTimeZone;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;

import networkAgent.listener.ListenerTCP;
import database.DBTransaction;
import database.snapshot.SaveSnapshot;
import database.snapshot.SnapshotServices;
import database.snapshot.SnapshotSummary;
import database.snapshot.SnapshotVolumetrics;
import serviceAnalysisModel.*;
import serviceAnalysisModel.SAControl.MemoryWarningSystem.MemoryListener;
import serviceAnalysisModel.SAControl.MessageTopic.MessageListener;
import serviceAnalysisModel.SAControl.MessageTopic.Topic;
import serviceAnalysisModel.Transaction.EventCollection;
import serviceAnalysisModel.readersWriters.BWReader;
import serviceAnalysisModel.readersWriters.DirectoryTailer;
import serviceAnalysisModel.readersWriters.EventAlerter;
import serviceAnalysisModel.readersWriters.PipeReader;
import serviceAnalysisModel.readersWriters.ServiceEntry;
import serviceAnalysisModel.readersWriters.UserLoginEvent;
import serviceAnalysisUI.CountGraph;

public class Controller
{
	private Controller()
	{
		if (controller == null)
		{
			
			controller = this;
			SO = new ArrayList<Service>();
			bwReader = new BWReader(SO);
			bwReader.setController(this);
			bwReader.setVolumetricModel(new VolumetricModel());
			EventCollection.getInstance();
			startUDPListenerRange(5555,5800);
			daemon=false;
			startJMX(false);
			if (stdin)
				startstdinreader();
		}
	}

	private Controller(List<Service> so, VolumetricModel vm)
	{
		if (controller == null)
		{
			controller = this;
			SO = so;
			bwReader = new BWReader(so);
			bwReader.setController(this);
			bwReader.setVolumetricModel(vm);
			daemon=false;
			startJMX(false);
			EventCollection.getInstance();
			if (stdin)
				startstdinreader();
		}
	}

	public synchronized static Controller ControllerFactory(List<Service> so,
			VolumetricModel vm)
	{
		System.out.println("creating controller - so/vm");
		if (controller == null)
		{
			controller = new Controller(so, vm);

			// SO = so;
			// bwReader = new BWReader(so);
			// bwReader.setController(this);
			// bwReader.setVolumetricModel(vm);
			// startJMX();
			// if(stdin)
			// startstdinreader();
			// setLoggingLevel(Level.FINE);
			initialiseLogging();
			createMemoryMonitor();

		}
		return controller;
	}
	
	private static boolean daemon=true;
	
	

	public synchronized static Controller ControllerFactory(BWReader bwr)
	{
		System.out.println("creating controller - bwr");
		if (Controller.controller == null)
		{
			
			Controller.controller = new Controller(bwr);
			// bwReader=bwr;
			// bwReader.setController(this);
			// SO=bwr.getServices();
			// startJMX();
			// if(stdin) startstdinreader();
			// setLoggingLevel(Level.FINE);
			initialiseLogging();
			
			createMemoryMonitor(); 
			EventCollection.getInstance();
			//MemoryWarningSystem.setPercentageUsageThreshold(.80);

		}
		return controller;
	}

	public static MemoryWarningSystem memoryMonitor=new MemoryWarningSystem();
	
	
	private static void createMemoryMonitor(){
		System.out.println("creating memory monitor");
		//memoryMonitor=new MemoryWarningSystem();
		
		MemoryWarningSystem.setPercentageUsageThreshold(.80);
		
	}
	
	public static Controller getController()
	{
		if (controller != null)
			return controller;
		else
		{
			//EventCollection.getInstance();
			controller = new Controller();
			setLoggingLevel(Level.ALL);
			initialiseLogging();
			
			return controller;
		}
	}

	private Controller(BWReader bwr)
	{
		if (controller == null)
		{
			
			System.out.println("im here");
			controller = this;
			bwReader = bwr;
			bwReader.setController(this);
			SO = bwr.getServices();
			//startUDPListener(5555);
			startUDPListenerRange(5555,5800);

			startJMSListener();
			
			daemon=true;
			startJMX(true);
			if (stdin)
				startstdinreader();
			memoryMonitor.addListener(ml);
		}
	}

	public static void setLoggingLevel(Level newLevel)
	{
		LOGGER.logp(Level.FINE, Controller.class.getName(), "SetLoggingLevel",
				"Setting New Logging Level, " + newLevel.getName());
		parentLogger.setLevel(newLevel);
	}

	private static void initialiseLogging()
	{
		// LOGGER.setUseParentHandlers(true);
		// LOGGER.setLevel(Level.WARNING);
		parentLogger.setLevel(Level.INFO);
		StdOutHandler ch = new StdOutHandler();
		// ch.setLevel(Level.INFO);
		parentLogger.addHandler(ch);
	}

	private static Controller controller = null;

	private static Logger parentLogger = Logger
			.getLogger("serviceAnalysisModel");
	private final static Logger LOGGER = Logger.getLogger(Controller.class
			.getName());

	private JMXServer js;
	private void startJMX(boolean daemon)
	{
		js = new JMXServer(this,daemon);
		js.start();
	}

	private void startstdinreader()
	{
		if (pipeReader == null)
		{
			pipeReader = new PipeReader(System.in);
			pipeReader.addListener(bwReader);
			pipeReader.start();
		}
	}

	private PipeReader pipeReader = null;

	public void PauseReaders()
	{
		LOGGER.logp(Level.FINE, Controller.class.getName(), "PauseReaders",
				"Pausing Readers");
		bwReader.pauseReaders();

	}
	
	public void removeAllReaders()
	{
		bwReader.removeAllReaders();
	}

	public void RestartReaders()
	{
		LOGGER.logp(Level.FINE, Controller.class.getName(), "RestartReaders",
				"Restarting Readers");
		bwReader.restartReaders();

	}
	
	public void addDirectoryListener(String dir, String suffix)
	{
		System.out.println("Watching Directory "+dir+" for files ending in "+suffix);
		DirectoryTailer dt=new DirectoryTailer(dir,suffix);
		directoryWatchers.add(dt);
	}

	private CopyOnWriteArrayList<DirectoryTailer> directoryWatchers=new CopyOnWriteArrayList<DirectoryTailer>();
	
	public void removeDirectoryListener(String dir, String suffix)
	{
		System.out.println("number of watchers = "+directoryWatchers.size());
		for(DirectoryTailer dt:directoryWatchers)
		{
			if(dt.dir.equals(dir) && dt.pattern.equals(suffix))
			{
				dt.stop();
				directoryWatchers.remove(dt);
			}
		}
	}
	
	public void clearDirectoryListeners()
	{
		for(DirectoryTailer dt:directoryWatchers)
		{	if(dt==null) break;
			dt.stop();
			directoryWatchers.remove(dt);
		}
	}
	
	public void ClearServices()
	{

		LOGGER.logp(Level.FINE, Controller.class.getName(), "ClearServices",
				"Clearing all Services from Model");
		ClearandPauseServices();
		RestartReaders();
		//this is only here for test reasons
		//addDirectoryListener("/home/andrew/testdirectory",".csv");
	}

	private void ClearandPauseServices()
	{
		LOGGER.logp(Level.FINE, Controller.class.getName(),
				"ClearandPauseReaders",
				"Pausing Readers and clearing all services");
		PauseReaders();
		SO.clear();
		bwReader.clearLoginCount();
		bwReader.clearVolumetricModel();
		serviceAnalysisModel.Transaction.EventCollection.getInstance().clear();
		// bwReader.getVolumetricModel().clear();
		LOGGER.logp(Level.FINER, Controller.class.getName(),
				"ClearandPauseReaders",
				"Telling the world that the model has changed");

		fireModelChangedEvents();
	}

	public void resetServiceTimings()
	{
		PauseReaders();
		serviceAnalysisModel.Transaction.EventCollection.getInstance().clear();
		synchronized(SO){
			for (Service s : SO)
			{
				s.resetService();
				s.notifyEvents();
			}
		}
		bwReader.clearLoginCount();
		RestartReaders();
	}
	
	private boolean saving=false;

	public void saveTimings(String desc,DBTransaction dbt)
	{
		//System.out.println(new Date());
		PauseReaders();
		Date sampleSnapshot = new Date();
		//DBTransaction dbt=new DBTransaction();
		
		//snapshot header
		SaveSnapshot s1=new SaveSnapshot();
		s1.setSnapshot(sampleSnapshot, desc);
		dbt.addDBAction(s1);
		SnapshotSummary s2=new SnapshotSummary();
		s2.setSnapshot(sampleSnapshot, SO);
		dbt.addDBAction(s2);
		SnapshotServices s3=new SnapshotServices();
		s3.setSnapshot(sampleSnapshot, SO);
		dbt.addDBAction(s3);
		SnapshotVolumetrics s4=new SnapshotVolumetrics();
		s4.setSnapshot(sampleSnapshot, bwReader.getVolumetricModel());
		dbt.addDBAction(s4);
		
		try {
			saving=true;
			dbt.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		saving=false;
		RestartReaders();
		//System.out.println(new Date());
		System.out.println("snapshot save complete");
	//	MainForm.sbar.setMessage("Save Snapshot Complete "
	//			+ sampleSnapshot + " in "+ (System.currentTimeMillis()-sampleSnapshot.getTime()) +" ms");
	}

	
	public void saveTimings(String desc)
	{
		
		DBTransaction dbt=new DBTransaction();
		saveTimings(desc,dbt);
	}
	
	public void saveTimings2(String desc) //this is now obsolete
	{
		
		//System.out.println(new Date());
		PauseReaders();
		Date sampleSnapshot = new Date();
		if (desc == null || desc.isEmpty())
			desc = sampleSnapshot.toString();
		if (desc.length() > 98)
			desc = desc.substring(0, 98);
		bwReader.DBWrite(new ServiceEntry(sampleSnapshot, desc));
		for (Service s : SO)
		{
			if (s.getSamples().size() > 0)
			{
				bwReader.DBWrite(new ServiceEntry(sampleSnapshot, s,
						ServiceEntry.SE_TYPE_SUMMARY));
				for (Map.Entry<Double, Double> entry : s.getSamples()
						.entrySet())
				{
					double response = entry.getKey();
					double qty = entry.getValue();
					double wait = s.getAvgWaitTime();
					ServiceEntry se = new ServiceEntry(sampleSnapshot, s,
							response, qty);
					se.wait = wait;
					bwReader.DBWrite(se);

				}
			}
			ServiceEntry se = new ServiceEntry(sampleSnapshot, s,
					ServiceEntry.SE_TYPE_HIERARCHY);
			// se.adjustment=(double)s.getExplicitAdjustments();
			bwReader.DBWrite(se);
			Iterator<Service> it = s.getConsumers();
			while (it.hasNext())
			{

				ServiceEntry ss = new ServiceEntry(sampleSnapshot, s,
						ServiceEntry.SE_TYPE_HIERARCHY);
				ss.consumer = it.next();
				bwReader.DBWrite(ss);
			}
			HashMap<Long,Integer> cps=s.getCountPerSecond();
			if(cps.size()>0)
			{//samplecounts to save
				ServiceEntry ss=new ServiceEntry(sampleSnapshot,s,s.getSamplePeriod(),cps);
				bwReader.DBWrite(ss);
			}
		}
		// volumetric types
		for (VolumetricType v : bwReader.getVolumetricModel().model.keySet())
		{
			int qty = bwReader.getVolumetricModel().model.get(v);
			bwReader.DBWrite(new ServiceEntry(sampleSnapshot, v, qty));
			// now do volume->ws

			for (Workstream ws : v.getWorkstreamProportion().keySet())
			{
				double multi = v.getWorkstreamMultiplier(ws);
				bwReader.DBWrite(new ServiceEntry(sampleSnapshot, v, ws, multi));
			}

		}

		// System.out.println("sending end");
		bwReader.DBWrite(new ServiceEntry(sampleSnapshot));

		RestartReaders();
		//System.out.println(new Date());
	}

	public void loadTimings(Date d)
	{
		ClearandPauseServices();
		SO.clear();
		bwReader.loadSnapShot(d);
		for (Service s : SO)
		{
			s.notifyEvents();
		}
		
		fireModelChangedEvents();
		RestartReaders();
	}
	
	public void deleteTimings(Date date)
	{
		bwReader.deleteSnapshot(date);
	}
	
	public void stopTCPClients()
	{
		bwReader.stopAllTCPClients();
	}

	private  static boolean shuttingDown=false;
	
	public static boolean isShuttingDown()
	{
		return Controller.shuttingDown;
	}
	
	public void shutdown() 
	{
		Controller.shuttingDown=true;
		
		clearDirectoryListeners();
		removeAllReaders();
		stopTCPClients();
		int count=0;
		
		while(saving & count<120)
		{
			System.out.println("Waiting for up to "+(120-count)+" seconds for save to complete");
			count++;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block

			}
			
		}
		database.DBController.shutdown();
/*		Connection con;
		try
		{
			con = DriverManager
					.getConnection(DBConnectionStr+";shutdown=true", DBUser, DBPasswd);
			con.createStatement().executeUpdate("SHUTDOWN");
			con.close();
			System.out.println("database shutdown");
		} catch (SQLException e1)
		{
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			
		}
*/
		stopUDPListener();
		stopJMSListener();
		TextReports.closeFile(); // ensure it is flushed (event would be best so
									// any client could clean up)
		
		
		if(!daemon) js.interrupt();
		//System.err.println("should be stopped");
/*		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			
		}
		System.err.println("hasn't shutdown yet");
		System.exit(0); //force it if it isnt dead*/
		
		for(Frame f:JFrame.getFrames()) f.dispose();
	}

	public void loadFile(String filename)
	{
		bwReader.readAllFile(filename);
	}

	public void loadMatchFile(String filename) throws FileNotFoundException,
			IOException
	{
		ServiceMapper.readMatchFile(filename);
	}

	
	public void startUDPListenerRange(int sport,int eport)
	{
		if(sport<eport)
		{
			//System.out.println("trying to start in range "+sport+"-"+eport);
			int i=sport;
			while(i<= eport & port==0)
			{
				//System.out.println("trying to start "+i);
				startUDPListener(i);
				i++;
			}
		}
		else if(sport==eport)startUDPListener(sport);
		System.out.println("Started UDP on port "+port);
		
	}
	
	public int getUDPPort()
	{
		return this.port;
	}
	
	public void startUDPListener(int port)
	{

		
		try
		{
			bwReader.startUDPListener(port);
			this.port = port;
			//System.out.println("Started UDP listener on port "+ port);
		} catch (SocketException e)
		{

			//e.printStackTrace();
			//this.port = 0;
		}

	}
	
	public void startTCPClient(String host,int port)
	{
		bwReader.startTCPClient(host, port);
	}
	
	public void stopTCPClient(String host,int port)
	{
		bwReader.stopTCPClient(host, port);
	}
	
	public void sendTCPCommand(String host, int port, String command)
	{
		bwReader.sendTCPCommand(host, port, command);
	}

	public void startJMSListener()
	{
		bwReader.startJMSListener();
	}

	public void startJMSListenerFromJMX()
	{
		// this async process using command syntax is required because
		// of a class path issue with the mbeanserver not using the application
		// standard class loader. Found google references identifying the Sun
		// implementation.

		bwReader.networkReads.offer(new ServiceEntry("StartJMS", ""));
	}

	public void stopJMSListener()
	{
		bwReader.stopJMSListener();
	}

	public void stopUDPListener()
	{
		if (port != 0)
			bwReader.stopUDPListener(port);
	}

	public void stopUDPListener(int port)
	{
		bwReader.stopUDPListener(port);
	}

	public void resetRateMeters()
	{
		synchronized(SO){
			for (Service s : SO)
			{
				s.ratem.reset();
				s.notifyEvents();
			}
		}
	}

	public void monitorFile(String filename)
	{
		//System.out.println("monitorfile: "+filename);
		if(bwReader.addReader(filename))
			fireFileMonitorEvents(new MonitorChangeEvent(filename,
				MonitorChangeEvent._ADD));

	}

	public void removeFileMonitor(String filename)
	{
		if(filename!=null & bwReader.removeReader(filename))
			fireFileMonitorEvents(new MonitorChangeEvent(filename,
				MonitorChangeEvent._DEL));
	}

	public List<Service> getServices()
	{
		return SO;
	}

	public BWReader getReader()
	{
		return bwReader;
	}

	public void addFileMonitorListener(FileMonitorChangeListener fmc)
	{
		fileMonitors.add(fmc);
	}

	public void removeFileMonitorListener(FileMonitorChangeListener fmc)
	{
		fileMonitors.remove(fmc);
	}

	private void fireFileMonitorEvents(MonitorChangeEvent event)
	{
		for (FileMonitorChangeListener fmc : fileMonitors)
			fmc.fileMonitorChangeListener(event);
	}

	public void addServiceEventListener(ServiceAddedListener listener)
	{
		serviceListeners.add(listener);
	}

	public void removeServiceEventListener(ServiceAddedListener listener)
	{
		serviceListeners.remove(listener);
	}

	public void fireServiceAddedEvents(Service s)
	{
		//System.out.println("ServiceAddedEvent fired");
		ServiceAddedEvent event = new ServiceAddedEvent(s);
		for (ServiceAddedListener listener : serviceListeners)
		{
			//System.out.println("send to listener "+listener);
			listener.ServiceAdded(event);
		}
	}

	public void addMonitorWindowListener(MonitorWindowListener listener)
	{
		monitorWindowListeners.add(listener);
	}

	public void removeMonitorWindowListener(MonitorWindowListener listener)
	{
		monitorWindowListeners.remove(listener);
	}
	
	public void fireWindowResizeEvents(String name, int x, int y, int w, int h){
		for (MonitorWindowListener listener : monitorWindowListeners)
			listener.adjustWindow(name, x, y, w, h);
	}
	
	public void fireWindowOpenEvents(String name, Service s,int x, int y, int w, int h,int period){
		for (MonitorWindowListener listener : monitorWindowListeners)
			listener.openWindow(name, s,x, y, w, h,period);
	}
	
	public void fireWindowColumnChangeEvents(String name, String column,boolean on){
		for (MonitorWindowListener listener : monitorWindowListeners)
			listener.addColumn(name, column, on);
	}
	
	public void fireWindowColumnChangeEvents(String name, String column,int width){
		for (MonitorWindowListener listener : monitorWindowListeners)
			listener.setColumn(name, column, width);
	}
	
	public void fireHighlightEvents(String name, boolean on){
		for (MonitorWindowListener listener : monitorWindowListeners)
			listener.setHighlight(name, on);
	}

	public void fireMonitorWindowEvents(StartMonitorWindowEvent event)
	{
		for (MonitorWindowListener listener : monitorWindowListeners)
			listener.StartMonitor(event);
	}

	public void fireWindowDumpEvents()
	{
		//dump service table settings
		
		// dump monitor windows
		for (MonitorWindowListener listener : monitorWindowListeners)
			listener.dumpWindows();
		
	}

	public void addModelChangedListener(DataModelChangeListener d)
	{
		modelListeners.add(d);
	}

	public void removeModelChangedListener(DataModelChangeListener d)
	{
		modelListeners.remove(d);
	}

	public void fireModelChangedEvents()
	{
		for (DataModelChangeListener d : modelListeners)
		{
			d.modelChanged();
		}
	}

	public void fireServicesDataChanged()
	{
		for (DataModelChangeListener d : modelListeners)
		{
			d.dataChanged();
		}
	}

	public Map<Date, String> getSnapshots()
	{
		return bwReader.getSnapshots();
	}

	private final String[] helpStrings={
			"StartJMS: Start JMS Listener",
			"TCPConnect [STOP] <host> <port>: connect or disconnect to a TCP Directory Agent",
			"Password <password>: set password to authenticate against collector",
			"Reset Services: clear timings of all services",
			"Reset meters: reset all rate meters",
			"Reset filters: reset all time filters",
			"Reset: clear all services",
			"Help: this help message",
			"Read <file>: load file",
			"Monitor [Remove] <file>: tail file for events or stop tailing",
			"Save <name>: save data to snapshot",
			"List: list snapshot descriptions to standard out",
			"Load <name>: load snapshot",
			"Exit|Quit|Shutdown: close application",
			"UserCount [clear|zero]: clear all user login counts or set to zero at this time",
			"Directory Watch <Directory> <extension>: watch directory for files with set extension and tail those that are changing",
			"Directory Remove <Directory> <extension>: remove the specified directory watcher",
			"Directory clear watchers: remove all watchers",
			"Window dump: dump details of the windows currently open",
			"Window Monitor <layer> <service> <operation> <x> <y> <w> <h> [period] [show title]: open the specified real time window at a location and dimension and optionally set the timespan and the title on/off",
			"Window ServiceTable column \"<column name>\" <on|off>: add or remove the specified column to the table",
			"Window ServiceTable highlight <on|off>: turn on or off the highlight filter",
			"Window ServiceTable position <x> <y> <w> <h>: set the position and size of the service table",
			"Window ServiceTable columnwidth \"<column name>\" <width>: set the width of the specified column in the service table",
			"Window Count \"<Layer>\" \"<Service>\" \"<Operation>\" <x> <y> <w> <h> <period>: open the count graph for the specified service with specified dimension and location and set the average aggregation to period",
			"Print Report <name>: print the report with the specified name",
			"Print Services: print list of services",
			"Output <file>: send reports to this file",
			"Close: close the output file and reset it to standard out",
			"Filter From <yyyy-MM-dd HH:mm:ss>: only load events that are after this date/time",
			"Filter To <yyyy-MM-dd HH:mm:ss>: only load events up to this date/time",
			"Report Add <report Name> <Column> [value]: create report and add the column to it",
			"Report Rates Service Operation Period: output the fixed rate report",
			"Report Counts Service Operation Period: output the fixed  count report",
			"Report RollingCount Service Operation Period: output the fixedrolling count report",
			"TCPCommand read <server> <port> <file>: read a file on the server and send it to the client",
			"TCPCommand read <server> <port> <directory> <extension>: monitor another directory on the server and send file updates to all clients",
			"TCPCommand monitor <server> <port> <file>: monitor file on server and send all changes to all clients",
			"TCPCommand stopall <server> <port>: stop monitoring all files and directories",
			"TCPCommand chpasswd <host> <port> <current password> <new password>: change collector password once authenticated",
			"TCPCommand resetpasswd <host> <port> <user> <admin password> <new user password>: change collector password for a user",
			"TCPCommand disableuser <host> <port> <user>: delete a user from the collector (must be an admin user)",
			"TCPCommand adduser <host> <port> <admin password> <user> <user password>: add a user to the collector (must be an admin)",
			"TCPCommand addadmin|deladmin <host> <port> <admin password> <user>: add or remove an admin role to a user (must be an admin to run this command)",
			"TCPCommand live <host> <port> on|off: turn on or off live updates from the TCPCollector",
			"TCPCommand select <host> <port> timings from \"<dd/MM/yyyy HH:MM>\" to \"<dd/MM/yyyy HH:MM>\": retrieve records between the given dates",
			"TCPCommand select <host> <port> timings day <dd/MM/yyyy>: select all records on a given day",
			"TCPCommand select <host> <port> stop: stop selection of records",
			"Delay <number>: change delay between updates to number milliseconds",
			"Highlight <field name> <layer.service.operation> <green> <orange> <red>: colour fields based on threshold values.  Fields can be \"Rate\" or \"Average\"",
			"Alert add <layer> <service> <operation> <threshold>: Set an event alert to a service at specified threshold",
			"Alert del <layer> <service> <operation>: Remove any event alert against the specified service",
			"Alert clear: clear all alert rules",
			"OutlierFactor <value>: Count graph outlier calculation scale factor.  Should be greater than 1 (default 1.5)",
			"UserWatch <name>: Output all user tagged actions",
			"UserWatchOff: Turn off all user watching",
			"ListCapturedUsers: Lists the captured users",
			"PrintUserEvents <user>: Prints the events collected for the specified user",
			"SaveUserEvents <user regex pattern> <file>",
			"SetInactivityThreshold <minutes>: Sets the period after which a user is considered to be logged out (default 10)",
	""};
	
	public void commandHelp()
	{
		
		for(String s:helpStrings)
			System.out.println(s);
		System.out.println("Allowed Report Tags");
		for(String s:  TextReports.getTagList())
			System.out.println('\t'+s);
	}
	
	public void processCommand(String command, String args)
	{
		System.out.println("process command");
		//String [] test=Controller.tokenize(args);
		//for(String s:test)System.out.println(s);
		if(command.equalsIgnoreCase("SetInactivityThreshold")){
			setInactivityThreshold(args);
		}
		if(command.equalsIgnoreCase("Password"))
		{
			ListenerTCP.password=args;
		}
		if(command.equalsIgnoreCase("UserWatchOff")){
			this.setWatch(false);
		}
		if(command.equalsIgnoreCase("UserWatch")){
			if(args.length()>0){
				this.addUserWatch(args);
				this.setWatch(true);
			}
		}
		if(command.equalsIgnoreCase("ListCapturedUsers")){
			printCapturedUsers();
		}
			
		
		if(command.equalsIgnoreCase("PrintUserEvents")){
			if(args.length()>0){
				printUserEvents(args.trim());
			}
			else System.out.println("wrong number of arguments");
		}
		if(command.equalsIgnoreCase("SaveUserEvents")){
			String s[]=Controller.tokenize(args.trim());
			if(s.length==2){
				//String s[]=args.split(" ");
				
				saveUserEvents(s[0],s[1]);
			}
			else System.out.println("wrong number of arguments");
		}
		
		if(command.equalsIgnoreCase("outlierFactor")){
			CountGraph.setOutlierFactor(args);
		}
		if(command.equalsIgnoreCase("Highlight"))
		{
			processRangeAddCommand(args);
		}
		if (command.equalsIgnoreCase("StartJMS"))
		{
			startJMSListener();
		}
		if(command.equalsIgnoreCase("Delay"))
		{
			try {
				long ms=Long.parseLong(args.trim());
				setServiceTableUpdateDelay(ms);
			} catch (NumberFormatException n)
			{
				System.out.println("Delay: "+ args + " is not a Number");
			}
		}
		if(command.equalsIgnoreCase("TCPCommand"))
		{
			String com=null;
			String subargs="";
			String lower=args.toLowerCase().trim();
			int minlength=Integer.MAX_VALUE;
			if(lower.startsWith("read")) {
				com="read";
				subargs=args.substring(5);
				minlength=3;
				
			} else if(lower.startsWith("stopall")){
				com="stop";
				subargs=args.substring(8);
				minlength=2;
			} else if(lower.startsWith("monitor")){
				com="monitor";
				subargs=args.substring(8);
				minlength=3;
			} else if(lower.startsWith("chpasswd")){
				com="chpasswd";
				subargs=args.substring(9);
				minlength=4;
			} else if(lower.startsWith("resetpasswd")){
				com="resetpasswd";
				subargs=args.substring(12);
				minlength=5;
			}else if(lower.startsWith("disableuser")){
				com="disableuser";
				subargs=args.substring(12);
				minlength=3;
			}else if(lower.startsWith("adduser")){
				com="adduser";
				subargs=args.substring(8);
				minlength=5; //host port adminPassword user password
			}else if(lower.startsWith("addadmin")){
				com="addadmin";
				subargs=args.substring(9);
				minlength=4; //host port adminPassword user
			}else if(lower.startsWith("deladmin")){
				com="deladmin";
				subargs=args.substring(9);
				minlength=4; //host port adminPassword user
			}else if(lower.startsWith("live")){
				com="live";
				subargs=args.substring(5);
				minlength=3; //host port on|off 
			}else if(lower.startsWith("select")) {//add select methods here
				if(lower.contains("stop")){
					com="selectstop";
					subargs=args.substring(7);
					minlength=3; //host port stop
				}else if(lower.contains(" timings ")){
					//timings from or timings day
					if(lower.contains(" day ")){
						//host port day dd/mm/yyyy
						com="selectday";
						subargs=args.substring(7);
						minlength=4;
					}else if(lower.contains(" from ")){
						//host port from "dd/mm/yyyy hh:mm" to "dd/mm/yyyy hh:mm"
						com="selectbetween";
						subargs=args.substring(7);
						minlength=4;
					}
				}
			
			}
				
			String split[]=Controller.tokenize(subargs.trim());
			//String split[] = subargs.trim().split(" ");
			if(com==null || split.length<minlength) return;
			else try{
				int port=Integer.parseInt(split[1]);
				if(com.equals("chpasswd")){
					String passwordBundle=new String(split[2]+"::"+split[3]);
					//System.out.println("Password Bundle="+passwordBundle);
					
					char[] enc=bwReader.getTCPEncryptedMessage(split[0], port, passwordBundle);
					if(enc==null)return;
					split[2]=System.getProperty("user.name")+",";
					split[2]=split[2]+new String(enc);
					split[3]="";
				}
				if(com.equals("resetpasswd")){
					//split[0]=host
					//split[1]=port
					//split[2]=user
					//split[3]=admin password
					//split[4]=new password
					int i=random.nextInt();
					String passwordBundle=new String(""+i+"::"+split[3]+"::"+split[4]);
					//System.out.println("Password Bundle="+passwordBundle);
					
					char[] enc=bwReader.getTCPEncryptedMessage(split[0], port, passwordBundle);
					if(enc==null)return;
					//split[2]=System.getProperty("user.name")+",";
					split[2]=split[2]+","+new String(enc);
					split[3]="";
					split[4]="";
				}
				if(com.equals("disableuser")){
					//split[0]=host
					//split[1]=port
					//split[2]=user
					int i=random.nextInt();
					String passwordBundle=new String(""+i);
					char[] enc=bwReader.getTCPEncryptedMessage(split[0], port, passwordBundle);
					if(enc==null)return;
					split[2]=split[2]+","+i+","+new String(enc);
				}
				if(com.equals("adduser")){
					//split[0]=host
					//split[1]=port
					//split[2]=admin password
					//split[3]=user
					//split[4]=password
					int i=random.nextInt();
					String passwordBundle=new String(""+i+"::"+split[2]+"::"+split[3]+"::"+split[4]);
					//System.out.println("Password Bundle="+passwordBundle);
					
					char[] enc=bwReader.getTCPEncryptedMessage(split[0], port, passwordBundle);
					if(enc==null)return;
					//split[2]=System.getProperty("user.name")+",";
					split[2]=""+i+","+new String(enc);
					split[3]="";
					split[4]="";
				}
				if(com.equals("deladmin")|com.equals("addadmin")){
					//split[0]=host
					//split[1]=port
					//split[2]=admin password
					//split[3]=user
					int i=random.nextInt();
					String passwordBundle=new String(""+i+"::"+split[2]+"::"+split[3]);
					char[] enc=bwReader.getTCPEncryptedMessage(split[0], port, passwordBundle);
					if(enc==null)return;
					//split[2]=System.getProperty("user.name")+",";
					split[2]=""+i+","+new String(enc);
					split[3]="";
					
				}
				if(com.equalsIgnoreCase("selectstop")){
					//split[0]=host
					//split[1]=port
					//split[2]=stop
					com="select";
				}
				if(com.equalsIgnoreCase("selectbetween") | com.equalsIgnoreCase("selectday")){
					String f=createSelectString(split);
					split[3]=f.trim();
					com="select";
					for(int i=4;i<split.length;i++) split[i]="";
				}
				/*
				if(com.equalsIgnoreCase("selectbetween")){
					//split[0]=host
					//split[1]=port
					//split[2]=timings
					//split[3]=from
					//split[4]=dd/MM/yyyy HH:mm
					//split[5]=to
					//split[6]=dd/MM/yyyy HH:mm
					if(split.length>7){System.out.println("invalid command"); return;}
					com="select";
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
					Date from=sdf.parse(split[4]);
					split[4]=""+from.getTime();
					//Date to;
					if(split.length==7) {
						Date to=sdf.parse(split[6]);
						split[6]=""+to.getTime();
						split[5]="";
					}
					else {
						//to=new Date();
						split[4]+=" "+System.currentTimeMillis();
					}
					split[3]="";
				}
				if(com.equalsIgnoreCase("selectday")){
					//split[0]=host
					//split[1]=port
					//split[2]=timings
					//split[3]=day
					//split[4]=dd/MM/yyyy

					if(split.length!=5 ){System.out.println("invalid command"); return;}
					com="select";
					SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
					Date from=sdf2.parse(split[4]+" 00:00:00.000");
					Date to=sdf2.parse(split[4]+" 23:59:59.999");
					split[4]=""+from.getTime()+" "+to.getTime();
					

					split[3]="";
				}
				*/
				for(int i=2;i<split.length;i++)
					if(!split[i].equals(""))com=com+" "+split[i];
				com=com.trim();	
				if(com.startsWith("select"))System.out.println(com);
				sendTCPCommand(split[0],port,com);
				
			}catch (NumberFormatException e)
			{
				System.out.println(split[1]+" is not a number");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("invalid date/time - \"dd/MM/yyyy HH:mm\" ensure the surrounding \" are supplied");
			}
			
		}
		if(command.equalsIgnoreCase("TCPConnect"))
		{
			//System.out.println("Start TCP Client");
			String split[]=Controller.tokenize(args.trim());
			//String split[] = args.trim().split(" ");
			if(split.length==2){
				System.out.println("Start TCP Client");
				try{
					int port=Integer.parseInt(split[1]);
					startTCPClient(split[0],port);
					
				}catch (NumberFormatException e)
				{
					System.out.println(split[1]+" is not a number");
				}
			}
			else if(split.length==3 && split[0].equalsIgnoreCase("stop"))
			{
				System.out.println("Stop TCP Client");
				try{
					int port=Integer.parseInt(split[2]);
					stopTCPClient(split[1],port);
					
				}catch (NumberFormatException e)
				{
					System.out.println(split[2]+" is not a number");
				}
			}
			else System.out.println("usage: TCPConnect host port");
		}
		
		if (command.equalsIgnoreCase("Reset"))
		{
			if (args.equalsIgnoreCase("Services"))
				resetServiceTimings();
			if (args.equalsIgnoreCase("meters"))
				resetRateMeters();
			if (args.equalsIgnoreCase("filters"))
				resetFilters();
			if (args.length() == 0)
				ClearServices();
		}
		if (command.equalsIgnoreCase("Read") && args.length() > 0)
			loadFile(args);
		if (command.equalsIgnoreCase("Monitor") && args.length() > 0)
		{
			String split[]=Controller.tokenize(args.trim());
			//String split[] = args.trim().split(" ");
			if (split[0].equalsIgnoreCase("remove"))
				removeFileMonitor(split[1]);
			else
				monitorFile(args.trim());
		}
		if (command.equalsIgnoreCase("Save"))
			saveTimings(args);
		if (command.equalsIgnoreCase("List"))
		{
			for (Entry<Date, String> es : this.getSnapshots().entrySet())
			{
				System.out.println(es.getKey() + ", " + es.getValue());
			}
		}
		if (command.equalsIgnoreCase("load"))
		{
			if (args.length() > 0)
			{
				Map<Date, String> ss = this.getSnapshots();
				if (ss.containsValue(args))
				{
					Date d = null;
					for (Entry<Date, String> es : this.getSnapshots()
							.entrySet())
					{
						if (es.getValue().equals(args))
							d = es.getKey();
					}
					if (d != null)
					{
						ClearandPauseServices();
						bwReader.loadSnapShot(d);
						fireModelChangedEvents();
						RestartReaders();
					}

				}
			}
		}
		if(command.equalsIgnoreCase("alert"))
			alert(args);
		if (command.equalsIgnoreCase("exit"))
			shutdown();
		if (command.equalsIgnoreCase("quit"))
			shutdown();
		if (command.equalsIgnoreCase("shutdown"))
			shutdown();
		if (command.equalsIgnoreCase("Print"))
		{
			if (args.startsWith("Report"))
			{
				String split[]=Controller.tokenize(args.trim());
				//String split[] = args.split(" ");
				if (split.length == 2)
					printReport(split[1]);
			}
			if (args.equalsIgnoreCase("Services"))
				TextReports.ServiceName(getServices());
		}
		if (command.equalsIgnoreCase("output"))
		{
			if (args.length() > 0)
				TextReports.openFile(args);
		}
		if (command.equalsIgnoreCase("close"))
			TextReports.closeFile();
		// if(command.equalsIgnoreCase("NOOP")) ;
		if (command.equalsIgnoreCase("Filter"))
		{
			String split[] = args.trim().split(" ", 2);
			if (split.length == 2)
			{
				if (split[0].equalsIgnoreCase("From"))
					setFilterFrom(split[1]);// yyyy-MM-dd HH:mm:ss
				if (split[0].equalsIgnoreCase("To"))
					setFilterTo(split[1]);
			}
		}
		if (command.equalsIgnoreCase("Report")) // COMMAND: Report Add
												// reportName Column (value)
		{
			if (args.startsWith("Add"))
			{
				System.out.println("add row " + args);
				String split[]=Controller.tokenize(args.trim());
				//String split[] = args.split(" ");
				String reportName = null;
				String column = null;
				Double value = null;
				if (split.length >= 2)
					reportName = split[1];
				if (split.length >= 3)
					column = split[2];
				if (split.length == 4)
					value = new Double(split[3]);
				if (column != null)
				{
					System.out.println("adding row " + reportName + " "
							+ column + " " + value);
					addReportLine(reportName, column, value);
				}
			}
			if (args.startsWith("Rates")) // COMMAND: Report Rates Service
											// Operation period
			{
				int period = 0;
				String split[]=Controller.tokenize(args.trim());
				//String split[] = args.split(" ");
				if (split.length >= 3)
				{
					Service service = new Service("BW", split[1], split[2], "");
					if (split.length == 4)
						period = new Integer(split[3]);
					if (SO.contains(service))
					{
						service = SO.get(SO.indexOf(service));
						TextReports.rateReport(service, period);
					}
				}
			}
			if (args.startsWith("Counts")) // COMMAND: Report Counts Service
											// Operation period
			{
				int period = 1;
				String split[]=Controller.tokenize(args.trim());
				//String split[] = args.split(" ");
				if (split.length >= 3)
				{
					Service service = new Service("BW", split[1], split[2], "");
					if (split.length == 4)
						period = new Integer(split[3]);
					if (SO.contains(service))
					{
						service = SO.get(SO.indexOf(service));
						TextReports.sampleCountReport(service, period);
					}
				}
			}
			if (args.startsWith("RollingCount")) // COMMAND: Report RollingCount
													// Service Operation period
			{
				int period = 1;
				String split[]=Controller.tokenize(args.trim());
				//String split[] = args.split(" ");
				if (split.length >= 3)
				{
					Service service = new Service("BW", split[1], split[2], "");
					if (split.length == 4)
						period = new Integer(split[3]);
					if (SO.contains(service))
					{
						service = SO.get(SO.indexOf(service));
						TextReports.rollingCountReport(service, period);
					}
				}
			}

			if (args.startsWith("Capture")) // COMMAND: Report Capture Service
											// Operation (Start or Stop) [period
											// int ms]
			{
				String split[]=Controller.tokenize(args.trim());
				//String split[] = args.split(" ");
				if (split.length >= 4)
				{
					Service service = new Service("BW", split[1], split[2], "");
					if (SO.contains(service))
					{
						service = SO.get(SO.indexOf(service));
						int rate = 60000;
						if (split.length == 5)
							rate = new Integer(split[4]);

						if (split[3].equalsIgnoreCase("Start"))
							service.startRateSampleCapture(rate);
						else if (split[3].equalsIgnoreCase("Stop"))
							service.stopRateSampleCapture();
						else
							System.out.println("nor start or stop " + split[3]);
					}
				} else
					System.out.println("split length != 4");
			}
		}
		if (command.equalsIgnoreCase("Window")) // COMMAND: Window Monitor
												//            Layer Service Operation X Y W H [period]
												// COMMAND: Window ServiceTable position x y w h
												// COMMAND: Window ServiceTable highlight on/off
												// COMMAND: Window ServiceTable column "column name" on/off
												// COMMAND: Window ServiceTable columnwidth "column name" width
												// COMMAND: Window Count Layer Service Operation X Y W H [period]
		{
			String split[]=Controller.tokenize(args.trim());
			//String split[] = args.trim().split(" ", 8);
			if(split[0].equalsIgnoreCase("ServiceTable"))
			{
				if(split[1].equalsIgnoreCase("column")){
					boolean on=false;
					if(split[split.length-1].equalsIgnoreCase("on"))on=true;
					String column=split[2];
					//for(int i=3;i<split.length-1;i++) column+=" "+split[i];
					//if(column.startsWith("\""))column=column.substring(1);
					//if(column.endsWith("\""))column=column.substring(0, column.lastIndexOf("\""));
					//System.out.println("column name: "+column);
					fireWindowColumnChangeEvents("ServiceTable",column,on);
				}
				else if(split[1].equalsIgnoreCase("columnwidth")){
					try{
						int width=new Integer(split[split.length-1]);
						String column=split[2];
						for(int i=3;i<split.length-1;i++) column+=" "+split[i];
						if(column.startsWith("\""))column=column.substring(1);
						if(column.endsWith("\""))column=column.substring(0, column.lastIndexOf("\""));
						fireWindowColumnChangeEvents("ServiceTable",column,width);

					}catch (Exception e){
						System.out.println("Invalid Command: "+args);
					}
					
				}
				else if(split[1].equalsIgnoreCase("position")){
					try{
					int x = new Integer(split[2]);
					int y = new Integer(split[3]);
					int w = new Integer(split[4]);
					int h = new Integer(split[5]);
					fireWindowResizeEvents("ServiceTable",x,y,w,h);
					
					}catch (Exception e){
						System.out.println("Invalid command: "+ args);
					}
				}else if(split[1].equalsIgnoreCase("highlight")){
					boolean on=false;
					if(split[2].equalsIgnoreCase("on")) on = true;
					fireHighlightEvents("ServiceTable",on);
				}
			}
			else if(split[0].equalsIgnoreCase("Count")){
				System.out.println("processing count window");
				int i = SO.indexOf(new Service(split[1], split[2], split[3], ""));
				if(i==-1)
				{
					synchronized(SO){
					SO.add(new Service(split[1], split[2], split[3], ""));
					i=SO.indexOf(new Service(split[1], split[2], split[3], ""));
					}
					fireServiceAddedEvents(SO.get(i));
				}
				if(i!=-1){
					if (i != -1)
					try{
						int x = new Integer(split[4]);
						int y = new Integer(split[5]);
						int w = new Integer(split[6]);
						int h = new Integer(split[7]);
						int period=1;
						if(split.length==9)period=new Integer(split[8]);
						fireWindowOpenEvents("Count",SO.get(i),x,y,w,h,period);
						
					}catch(Exception e){
						System.out.println("Invalid Command: "+args);
						e.printStackTrace();
					}
				}
			}
			else if (split.length >= 8 & split[0].equalsIgnoreCase("Monitor"))
			{
				int i = SO.indexOf(new Service(split[1], split[2], split[3], ""));
				if(i==-1)
				{
					synchronized(SO){
					SO.add(new Service(split[1], split[2], split[3], ""));
					i=SO.indexOf(new Service(split[1], split[2], split[3], ""));
					}
					fireServiceAddedEvents(SO.get(i));
				}
				if (i != -1)
				{
					int x = new Integer(split[4]);
					int y = new Integer(split[5]);
					int w = new Integer(split[6]);
					int h = new Integer(split[7]);

					StartMonitorWindowEvent event = new StartMonitorWindowEvent(
							SO.get(i), x, y, w, h);
					if (split.length == 9){
						if(split[8].equalsIgnoreCase("true")|split[8].equalsIgnoreCase("on")) event.showTitle(true);
						else if(split[8].equalsIgnoreCase("false")|split[8].equalsIgnoreCase("off")) event.showTitle(false);
						else event.setTime(new Integer(split[8]));
					}
					if (split.length == 10){
						if(split[9].equalsIgnoreCase("true")|split[9].equalsIgnoreCase("on")) event.showTitle(true);
						else if(split[9].equalsIgnoreCase("false")|split[9].equalsIgnoreCase("off")) event.showTitle(false);
						event.setTime(new Integer(split[8]));
					}
					
					fireMonitorWindowEvents(event);

				}
			} else if (args.trim().equalsIgnoreCase("dump"))
			{
				fireWindowDumpEvents();
			}
		}
		if (command.equalsIgnoreCase("dump"))
		{

		}
		if(command.equalsIgnoreCase("Directory"))
		{
			String split[]=Controller.tokenize(args.trim());
			//String split[]=args.split(" ");
			if(split[0].equalsIgnoreCase("watch") && split.length==3)
			{//lets watch a directory
				addDirectoryListener(split[1],split[2]);
			}
			else if(split[0].equalsIgnoreCase("remove") && split.length==3)
			{//remove a directory watcher
				removeDirectoryListener(split[1],split[2]);
			}
			else if(args.trim().equalsIgnoreCase("clear watchers"))
			{
				clearDirectoryListeners();
			}
		}
		if(command.equalsIgnoreCase("UserCount"))
		{
			String split[]=Controller.tokenize(args.trim());
			//String split[]=args.split(" ");
			if(args.toLowerCase().trim().equals("")) 
				bwReader.getUserCount();
			else if(args.toLowerCase().trim().equals("clear"))
				bwReader.clearLoginCount();
			else if(args.toLowerCase().trim().equals("zero"))
				bwReader.zeroCount();
			
		}
		if(command.equalsIgnoreCase("help")) commandHelp();
	}
	
	private String createSelectString(String[] args) throws ParseException{
		//host, port, timings, from, "dd/MM/yyyy HH:mm"[, to, "dd/MM/yyyy HH:mm", layer, "query", service, "query", operation, "query", user, "query"] 
		//host, port, timings, day, dd/MM/yyyy, [layer, "query", service, "query", operation, "query", user, "query"]
		//order of items after timings can change
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		int from=0; //0 not found, 1 found, -1 processed
		int to=0;
		int day=0;
		String rate=null;
		String fromString="",toString="",layerString=null,serviceString=null,operationString=null,userString=null;
		String com="";
		for(int i=2;i<args.length;i++){
			if(from==1){
				from = -1;
				
				Date fromDate=sdf.parse(args[i]);
				//com+=""+fromDate.getTime()+" ";
				fromString=""+fromDate.getTime();
				if(to==0) toString=""+System.currentTimeMillis();
			}
			else if(day==1){
				day=-1;
				SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
				Date fromDate=sdf2.parse(args[i]+" 00:00:00.000");
				Date toDate=sdf2.parse(args[i]+" 23:59:59.999");
				//com+=""+fromDate.getTime()+" "+toDate.getTime();
				fromString=""+fromDate.getTime();
				toString=""+toDate.getTime();
			}
			else if(to==1){
				to = -1;
				
				
				Date toDate=sdf.parse(args[i]);
				//com+=""+fromDate.getTime()+" ";
				toString=""+toDate.getTime();
				
			}
			//else if(args[i].equalsIgnoreCase("timings")){
			//	com+="timings ";
			//}
			else if(args[i].equalsIgnoreCase("from") ){
				if(from==0 & day==0){
					from=1;
					day=-1;
				}
				else throw new IllegalArgumentException("Only one from/day is allowed");
			}
			else if(args[i].equalsIgnoreCase("day")){
				if(day==0 && from==0){
					day=1;
					from=-1;
					to=-1;
				}
				else throw new IllegalArgumentException("Only one from/day is allowed");
			}
			else if(args[i].equalsIgnoreCase("to") ){
				if(day==0 | from==-1 & day==-1){
					to=1;
					day=-1;
				}
				else throw new IllegalArgumentException("Cannot have to and day together or 2 entries with to");
			}
			else if(args[i].equalsIgnoreCase("layer")){
				i++;
				layerString=args[i];
			}
			else if(args[i].equalsIgnoreCase("service")){
				i++;
				serviceString=args[i];
			}
			else if(args[i].equalsIgnoreCase("operation")){
				i++;
				operationString=args[i];
			}
			else if(args[i].equalsIgnoreCase("user")){
				i++;
				userString=args[i];
			}
			else if(args[i].equalsIgnoreCase("rate")){
				i++;
				rate=args[i];
			}
		}
		if(from!=-1 & to!=-1 &  day!=-1)throw new IllegalArgumentException("Need a to and from date or a day");

		com=fromString+" "+toString;
		System.out.println("command args:"+com);
		if(layerString!=null | serviceString!=null | operationString!=null | userString!=null | rate!=null){
			//we have a filter
			if(layerString==null)layerString=".*";
			if(serviceString==null)serviceString=".*";
			if(operationString==null)operationString=".*";
			if(userString==null)userString=".*";
			
			com+=" "+layerString+" "+serviceString+" "+operationString+" "+userString;
		}
		if(rate!=null)com+=" "+rate;
		System.out.println("command args:"+com);
		return com;
	}

	private void alert(String args) {
		//args: action layer service operation value
		EventAlerter alerter = EventAlerter.getInstance();
		String tokens[]=tokenize(args);
		String c="alert";
		for(String t:tokens){
			c+=" \""+t+"\"";
		}
		sendMessage(c);
		if(tokens[0].equalsIgnoreCase("clear")){
			alerter.removeAllRules();
			sendMessage("cleared all alert rules");
			
		}else {
			
			if(tokens.length<4){ 
				sendMessage("alert command error: alert action [<service type> <service> <operation> [<threshold>]]");
				return;
			}
			Service s=new Service(tokens[1],tokens[2],tokens[3],"");
			Double d=null;
			try{
				if(tokens.length==5)d=Double.parseDouble(tokens[4]);
			} catch(NumberFormatException e){
				sendMessage("alert command error: "+tokens[4]+" is not a number");
				return;
			}
			if(tokens[0].equalsIgnoreCase("add")){
				if(d==null){
					sendMessage("alert command error: adding a rule needs a threshold");
					return;
				}
				alerter.addRule(s, d);
			}
			else {
				alerter.removeAlert(s);
			}
			
		}
	}

	private void setServiceTableUpdateDelay(long ms) {
		for(DelayListener l:delayListeners) l.setDelay(ms);
		
	}
	
	public void addDelayListener(DelayListener l){
		delayListeners.add(l);
	}
	
	public void removeDelayListener(DelayListener l) {
		delayListeners.remove(l);
	}
	
	private CopyOnWriteArraySet<DelayListener> delayListeners=new CopyOnWriteArraySet<DelayListener>();
	private void setFilterFrom(String datestring)
	{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleTimeZone tz = new SimpleTimeZone(0, "GMT");
		df.setTimeZone(tz);
		try
		{
			Date when = df.parse(datestring);
			bwReader.setFromFilter(when);

		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			// e.printStackTrace();
			LOGGER.logp(Level.WARNING, Controller.class.getName(),
					"SetFilterFrom", "Failed to read date " + datestring);

		}

	}

	public void setFilterFrom(Date when)
	{
		bwReader.setFromFilter(when);
	}

	public ArrayList<String> getReportList()
	{
		return reportService.getReports();
	}
	
	public ArrayList<String> getReportTags()
	{
		return TextReports.getTagList();
	}
	
	
	public void addReportLine(String report, String column, Double value)
	{
		if (value == null)
			reportService.addColumnToReport(report, column,null);
		else
			reportService.addColumnToReport(report, column, value);
	}

	public void printReport(String report)
	{
		reportService.printReport(report);
	}

	public Date getFilterFrom()
	{

		return bwReader.getFromFilter();
	}

	public Date getFilterTo()
	{
		return bwReader.getToFilter();
	}

	public void resetFilters()
	{
		bwReader.setFromFilter(0);
		bwReader.setToFilter(Long.MAX_VALUE);
	}

	private void setFilterTo(String datestring)
	{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleTimeZone tz = new SimpleTimeZone(0, "GMT");
		df.setTimeZone(tz);
		try
		{
			Date when = df.parse(datestring);
			bwReader.setToFilter(when);

		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			// e.printStackTrace();
			LOGGER.logp(Level.WARNING, Controller.class.getName(),
					"SetFilterTo", "Failed to read date " + datestring);

		}
	}

	public void setFilterTo(Date when)
	{
		bwReader.setToFilter(when);
	}

	public void sendMessage(String message){
		topic.addMessage(message);
	}
	
	public void addMessageListener(MessageListener listener){
		topic.addListener(listener);
	}
	
	public void removeMessageListener(MessageListener listener){
		topic.removeListener(listener);
	}
	
	public void addRangeChangeListener(RangeChangeListener rcl)
	{
		rangeMonitors.add(rcl);
	}
	
	public void removeRangeChangeListener(RangeChangeListener rcl)
	{
		rangeMonitors.remove(rcl);
	}
	
	public void fireRangeChangeEvents(String field,String service,float green,float orange,float red)
	{
		for(RangeChangeListener rcl:rangeMonitors) rcl.addRange(field, service, green, orange, red);
	}
	
	public static String[] tokenize(String s){
		final char watchfor=' ';
		char previousChar=' ';
		int proc=0;
		boolean quoted=false;
		String split="";
		StringBuffer sb=new StringBuffer();
		ArrayList<String> list=new ArrayList<String>();
		char[] c=s.trim().toCharArray();
		for(int i=0;i<c.length;i++)
		{
			if(previousChar=='\\'){
				sb.append(c[i]);
				if(c[i]=='\\'){
					previousChar='_';
					
				}
				else previousChar=c[i];
			} else if(c[i]=='\\'){
				previousChar='\\';
			}
			else if(c[i]=='\"' & quoted){
				quoted=false;
				previousChar='\"';
			}else if(c[i]=='\"' & !quoted){
				quoted=true;
				previousChar='\"';
			}
			else if(c[i]==watchfor){
				//System.out.println("end of token? \'"+previousChar+"\' "+quoted);
				if(previousChar!=watchfor & !quoted){
					//System.out.println("end of token "+sb);
					
					split=new String(sb);
					sb=new StringBuffer();
					list.add(split);
					previousChar=watchfor;
				}
				else if(quoted)sb.append(watchfor);
			}else {
				sb.append(c[i]);
				previousChar=c[i];
			}
			
		}
		list.add(new String(sb));
		return list.toArray(new String[1]);
		
	}
	
	public void processRangeAddCommand(String command)
	{
		
		//String f,s,g,o,r;
		char watchfor=' ' ,previousChar=' ';
		int proc=0;
		String[] processing=new String[5];
		processing[0]="";
		char[] c=(command.trim()).toCharArray();
		
		for(int i=0;i<command.length();i++)
		{
			if(c[i]=='"' & previousChar==' ') watchfor='"';
			else if(c[i]==' ' & previousChar=='"') watchfor=' ';
			else if(c[i]==watchfor & previousChar!=watchfor) {processing[++proc]="";}
			else processing[proc]=processing[proc]+c[i];
			previousChar=c[i];
		}
		System.out.println("processRange command="+command);
		System.out.println("ProcessRange field="+processing[0]);
		System.out.println("ProcessRange service="+processing[1]);
		System.out.println("ProcessRange green="+processing[2]);
		System.out.println("ProcessRange orange="+processing[3]);
		System.out.println("ProcessRange red="+processing[4]);
		try {
			if(processing[2]==null || processing[2]=="")throw new NumberFormatException("green value ("+processing[2]+") is not a number");
			if(processing[3]==null || processing[3]=="")throw new NumberFormatException("orange value ("+processing[3]+") is not a number");
			if(processing[4]==null || processing[4]=="")throw new NumberFormatException("red value ("+processing[4]+") is not a number");
			float green=Float.parseFloat(processing[2]);
			float orange=Float.parseFloat(processing[3]);
			float red=Float.parseFloat(processing[4]);
			fireRangeChangeEvents(processing[0],processing[1],green,orange,red);
		}catch (NumberFormatException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private Topic topic=Topic.getInstance();
	
	private TextReports reportService = new TextReports();
	private ArrayList<MonitorWindowListener> monitorWindowListeners = new ArrayList<MonitorWindowListener>();
	private ArrayList<ServiceAddedListener> serviceListeners = new ArrayList<ServiceAddedListener>();
	private ArrayList<DataModelChangeListener> modelListeners = new ArrayList<DataModelChangeListener>();
	private ArrayList<FileMonitorChangeListener> fileMonitors = new ArrayList<FileMonitorChangeListener>();
	private ArrayList<RangeChangeListener> rangeMonitors = new ArrayList<RangeChangeListener>();
	private List<Service> SO;
	private BWReader bwReader;
	private int port = 0;
	private SecureRandom random=new SecureRandom();

	
	public static final String DBConnectionStrINT = "jdbc:derby:SAdb/derbyDB";
	public static final String DBConnectionStrJDBC = "jdbc:derby://localhost:1527/SAdb/derbyDB";
	public static final String DBConnectionStr=DBConnectionStrJDBC;
	public static final String DBConnectionStrHSQL = "jdbc:hsqldb:file:.Database.SA/SAdb";
	public static final String DBUser = "SA";
	public static final String DBPasswd = "SA";

	public static boolean stdin = false;
	
	public void loadAutoRun()
	{
		if(!runAuto) return;
		File f=new File("autorun.asap");
		String s;
		if(f.exists() & f.canRead() & f.isFile()) try{
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			BWReader reader = getReader();
			//if(reader==null)System.out.println("reader is null");
			while ((s = br.readLine()) != null){
				//System.out.println("autorun: "+s);
				if(s!=null)getReader().newLogFileLine(s, f.getAbsolutePath());
			}
			fr.close();
		}catch (IOException e){
			System.out.println(e.getMessage());
			
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
		
		else System.out.println("can't find file "+f.getAbsolutePath());
	}

	public void addUserWatch(String user){
		if(user!=null && !user.equals("") && !userOutput.contains(user)){
			userOutput.add(Pattern.compile(user));
			//userEvents.put(user, new ArrayList<ServiceEntry>());
		}
	}
	
	public void printCapturedUsers(){
		Set<String> st = userEvents.keySet();
		if(st.size()==0) System.out.println("The user events captured");
		else for(String s:st) System.out.println(s);
	}
	
	public void addUserEvent(String user,ServiceEntry se){
		if(isUserWatched(user)){
			if(!userEvents.containsKey(user))userEvents.put(user, Collections.synchronizedList(new ArrayList<ServiceEntry>()));
			userEvents.get(user).add(se);
		}
	}
	
	public ArrayList<String> getUserEvents(String user){
		if(!isUserWatched(user))return null;
		
		List<ServiceEntry> evs = new ArrayList<ServiceEntry>(userEvents.get(user));
		ArrayList<String> events=new ArrayList<String>();
		for(ServiceEntry se:evs)events.add(se.userView());
		Collections.sort(events);
		return events;
	}
	
	public void printUserEvents(String user){
		ArrayList<String> events = getUserEvents(user);
		if(events==null || events.size()==0)System.out.println("There are no events for "+user);
		else {
			System.out.println(ServiceEntry.getUserViewHeader());
			for(String e:events)System.out.println(e);
		}
	}
	
	public void saveUserEvents(String userPattern, String filename){
		try {
			boolean headerWritten=false;
			System.out.println("output file "+filename);
			FileWriter fw=new FileWriter(filename);
			PrintWriter pw=new PrintWriter(fw);
			Pattern pattern = Pattern.compile(userPattern);
			for(String user:userEvents.keySet()){
				Matcher m = pattern.matcher(user);
				boolean b=m.matches();
				if(b){
					ArrayList<String> events = getUserEvents(user);
					if(events!=null && events.size()>0){
						System.out.println("    User to be saved "+user);
						if(!headerWritten){
							headerWritten=true;
							pw.println(ServiceEntry.getUserViewHeader());
						}
						for(String e:events){
							pw.println(e);
						}
					}
				}
			}
			System.out.println("Closing File");
			pw.close();
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
			
		}
	}
	
	public boolean isUserWatched(String user){
		
		if(watch ){
			boolean b=false;
			for(Pattern p:userOutput){
				Matcher m = p.matcher(user);
				b=m.matches();
				if(b)break;
			}
						
			return b;
		}
		else return false;
	}
	
	public void setWatch(boolean watch){
		this.watch=watch;
		if(watch==false){
			userOutput.clear();
			userEvents.clear();
		}
		//else {
		//	memoryMonitor.addListener(ml);
		//}
	}
	
	private void setInactivityThreshold(String args){
		System.out.println("setInactivityThreshold "+args);
		try{
			int m=Integer.parseInt(args);
			setInactivityThreshold(m);
		}catch (NumberFormatException e) {
			System.out.println("   args is not a valid integer");
		}
	}
	
	private void setInactivityThreshold(int minutes){
		long l=minutes*60l*1000l-1;
		UserLoginEvent.SESSION_TIMEOUT=l;
	}
	
	
	
	//private MemoryWatch memoryWatcher=new MemoryWatch();
	private MemoryWatch ml=new MemoryWatch();
	
	public void removeUserWatch(String user){
		userOutput.remove(user);
		userEvents.remove(user);
	}
	
	public class MemoryWatch implements MemoryListener {

		@Override
		public void memoryUsageLow(long usedMemory, long maxMemory) {
			System.out.println("memory getting low, used="+usedMemory+", maximum="+maxMemory);
			
			if(watch){
				watch=false;
				System.out.println("Stopped watching users");
			}
			
		}
		
	}
	private boolean watch=false;
	private Set<Pattern> userOutput=Collections.synchronizedSet(new HashSet<Pattern>());
	private ConcurrentHashMap<String,  List<ServiceEntry>> userEvents=new ConcurrentHashMap<String, List<ServiceEntry>>();
	public static boolean runAuto=true;
}
