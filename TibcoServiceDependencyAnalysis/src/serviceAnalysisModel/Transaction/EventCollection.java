package serviceAnalysisModel.Transaction;


import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import serviceAnalysisModel.Service;
import serviceAnalysisModel.SAControl.Controller;
import serviceAnalysisModel.SAControl.MemoryWarningSystem;
import serviceAnalysisModel.SAControl.MemoryWarningSystem.MemoryListener;
import serviceAnalysisModel.readersWriters.BWReader;
import serviceAnalysisUI.transaction.RootTxnWorker;
import serviceAnalysisUI.transaction.TxnWorker;

import ServiceAnalysisClient.SAEvent;

public class EventCollection implements MemoryListener
{
	
	private final Collection<CollectionListener> listeners =
		      new ArrayList<CollectionListener>();
	
	  public interface CollectionListener {
		    public void collectorChange(boolean state, int collectionsize);
		  }
	  
	  public boolean addListener(CollectionListener listener) {
		  	if(listeners.contains(listener))return true;
		    return listeners.add(listener);
		  }

		  public boolean removeListener(CollectionListener listener) {
		    return listeners.remove(listener);
		  }

	private EventCollection()
	{
		instance = this;
		System.out.println("getting memory monitor");
		mws=Controller.memoryMonitor;
		mws.addListener(this);
		//MemoryWarningSystem.setPercentageUsageThreshold(.80);
		
	}

	private MemoryWarningSystem mws;
	private static EventCollection instance = null;

	public static EventCollection getInstance()
	{
		if (instance == null)
			instance = new EventCollection();
		return instance;
	}

	private static boolean collectTxn = false;

	public boolean setCollection(boolean on)
	{
		collectTxn=on;
		if(!on) {
			mws.removeListener(this);
			return false;
		
		}
		else
		{
			if(txnEvents.size()>100000)
			{
				collectTxn=false;
				System.out.println("you have 100,000 transactions already captured!");
				System.out.println("Please clear them before capturing more");
			}
			
			/*MemoryMXBean mbean = ManagementFactory.getMemoryMXBean();
			
			long used = mbean.getHeapMemoryUsage().getUsed();
			long max = mbean.getHeapMemoryUsage().getMax();
			long avail=mbean.getHeapMemoryUsage().getCommitted();
			int ff = mbean.getObjectPendingFinalizationCount();
			if(max==-1){
			
				System.out.println("max memory is unlimited");
				max=avail;
				System.out.println("available memory is "+avail);
			}
			
			System.out.println((double)used/(double)max);
			System.out.println("to be deleted "+ff);
			if((double)used/(double)max>=0.8d) {
				System.out.println("forcing capture off");
				System.gc();
					collectTxn=false;
			}	
			*/
			
			if(collectTxn==true)
			{
				mws.addListener(this);
				System.out.println("capture "+collectTxn);
			}
			else {
				mws.removeListener(this);
				System.out.println("capture "+collectTxn);
			}
			
			return collectTxn;
		}
	}
	
	public void addSAEvent(SAEvent event)
	{
		if (collectTxn == false){
			
			return;
		}
		
		if(txnEvents.size()>100000)
		{
			collectTxn=false;
			mws.removeListener(this);
			for(CollectionListener c: listeners) c.collectorChange(false, txnEvents.size());

		}
		
		String root = SAEvent.getRootTxn(event.txn);
		SAEvent rootEvent = null;
		// System.out.println("Add this event" );
		// System.out.println(event.toString());
		// add this event to the pool
		if (txnEvents.containsKey(event.txn))
		{
			// we have a template so need to move the children
			if (event.children == null | event.children.size() == 0)
				event.children = txnEvents.get(event.txn).children;
			else
				System.out.println("The incoming event has children!");
		}
		txnEvents.put(event.txn, event);

		if (rootTxnEvents.containsKey(root))
		{
			if (root.equals(event.txn))
			{// we have received the root out of order and need to drop the
				// original
				// need to get all the child events and add it to our event
				// it may be a template but could have children still
				// assume the incoming event has no children
				rootEvent = rootTxnEvents.get(root);
				event.children = rootEvent.children;
				rootTxnEvents.put(root, event);
				txnEvents.put(root, event);
				// System.out.println("root received out of order replacing original"
				// );
				// System.out.println(root.toString());
			}
			rootEvent = rootTxnEvents.get(root);

		} else
		{// this root doesnt exist

			if (root.equals(event.txn))
			{// but it is me
				rootTxnEvents.put(root, event);
				txnEvents.put(root, event);
				rootEvent = event;
				// System.out.println("I am the root");

			} else
			{// Need a new root event lets make it an SAEvent with nulls
				// System.out.println("The root doesnt exist creating a template");
				rootEvent = new SAEvent(root, null, null, null);
				rootTxnEvents.put(root, rootEvent);
				txnEvents.put(root, rootEvent);
			}
		}
		// System.out.println("Root Event");
		// System.out.println(root.toString());

		String parent = SAEvent.getParentTxn(event.txn);

		// now deal with parents
		if (parent == null)
		{// I am a root
			// System.out.println("No Parent" );
			// txnEvents.put(event.txn, event);
		} else
		{// we have a parent
			if (parent.equals(root))
			{// level 2 my parent is the root
				// System.out.println("My Parent is the root adding as child" );

				rootEvent.addChild(event);

			} else
			{
				// I have parents which aren't the root
				// System.out.println("My Parent is not the root" );
				String s[] = parent.split("--");
				if (s.length == 2) // must be >=2 as root is 1
				{// I am the only parent add the child to me
					// System.out.println("Only one Parent from root" );
					SAEvent parentEvent = null;
					if (txnEvents.containsKey(parent))
					{ // use the current parent
						// System.out.println("Found the parent" );

						parentEvent = txnEvents.get(parent);
					} else
					{ // use a template parent
						// System.out.println("Create new Parent" );
						parentEvent = new SAEvent(parent, null, null, null);
						txnEvents.put(parent, parentEvent);
						rootEvent.addChild(parentEvent);

					}
					parentEvent.addChild(event);
					// System.out.println("Add this event to Parent" );
					// System.out.println(parentEvent.toString() );
				} else
				{ // create all individual template parents if necessary
					String ptxn = root;

					SAEvent parentEvent = null;
					SAEvent oldParent = rootEvent;
					for (int i = 1; i < s.length; i++)
					{
						ptxn += "--" + s[i];
						// System.out.println("this txn "+ptxn);

						if (txnEvents.containsKey(ptxn))
						{ // use the current parent
							parentEvent = txnEvents.get(ptxn);
							// System.out.println("transaction found");
						} else
						{ // use a template parent
							parentEvent = new SAEvent(ptxn, null, null, null);
							txnEvents.put(ptxn, parentEvent);
							// System.out.println("create template transaction");

						}
						oldParent.addChild(parentEvent);
						// System.out.println("adding this to its parent");
						// System.out.println(oldParent.toString());
						oldParent = parentEvent;
					}
					parentEvent.addChild(event);
				}
			}
		}
		// System.out.println("+++++++++++");
		// String s[]=event.txn.split("--");
		// System.out.println(rootTxnEvents.get(s[0]).toString());
		// String trans="";
		// for(int i=0;i<s.length;i++)
		// {
		// trans+=s[i];
		// System.out.println(txnEvents.get(trans).toString());
		// trans+="--";
		// }
		// System.out.println("+++++++++++");
	}

	public synchronized void clear()
	{
		rootTxnEvents.clear();
		txnEvents.clear();
	}

	public Long getRangeStartTime(String txn) // returns null if transaction
												// doesnt exist or is a template
												// with no children that aren't
												// templates
	{
		Long earliest = null;
		if (!txnEvents.containsKey(txn))
			return earliest;

		SAEvent sae = txnEvents.get(txn);
		if (sae.children.size() == 0)// it is just me
		{
			// System.out.println("no children");
			if (sae.startTime == null)
				return null;
			else
				return sae.startTime.getTime();
		}
		if (sae.startTime != null)
			earliest = sae.startTime.getTime();
		for (String s : sae.children.keySet())
		{
			Long l = this.getRangeStartTime(s);
			if (l != null & earliest == null)
				earliest = l;
			if (l != null & earliest != null)
				earliest = Math.min(l, earliest);
		}

		return earliest;
	}

	public Long getRangeEndTime(String txn) // returns null if transaction
											// doesnt exist or is a template
											// with no children that arent
											// templates
	{
		Long latest = null;
		if (!txnEvents.containsKey(txn))
			return latest;

		// String s[]=txn.split("--");

		SAEvent sae = txnEvents.get(txn);
		if (sae.children.size() == 0)// it is just me
		{
			if (sae.startTime == null)
				return null;
			else
				return sae.startTime.getTime() + sae.processTime;
		}
		if (sae.startTime != null)
			latest = sae.startTime.getTime() + sae.processTime;
		for (String s : sae.children.keySet())
		{
			Long l = this.getRangeEndTime(s);
			if (l != null & latest == null)
				latest = l;
			if (l != null & latest != null)
				latest = Math.max(l, latest);
		}

		return latest;
	}

	public long getRange(String txn)
	{
		Long starttime, endtime;
		starttime = getRangeStartTime(txn);
		endtime = getRangeEndTime(txn);
		if (starttime == null | endtime == null)
			return 0;
		return endtime - starttime;
	}

	public int getTransactionHeight(String txn)
	{
		if (txn == null || !txnEvents.containsKey(txn))
			return 0;
		int height = 1, branchheight = 0;
		SAEvent sae = txnEvents.get(txn);

		for (String s : sae.children.keySet())
		{
			int branch = getTransactionHeight(s);
			branchheight = Math.max(branchheight, branch);

		}

		return height + branchheight;
	}

	public int getDefaultHeight(String txn)
	{
		String s[] = txn.split("--");
		return s.length;
	}
	
	private HashMap<TxnWorker,HashMap<String,TxnRect>> workersRectangles=
			new HashMap<TxnWorker,HashMap<String,TxnRect>>();
	
	public HashMap<String,TxnRect> getMyRectangles(TxnWorker twork)
	{
		return workersRectangles.get(twork);
	}

	public HashMap<String, TxnRect> calculatePositions(List<String> rootTxns,TxnWorker twork)
	{
		HashMap<String, TxnRect> rectangles = new HashMap<String, TxnRect>();
		PropertyChangeSupport mPcs =
		        new PropertyChangeSupport(this);
		if(twork!=null){
			workersRectangles.put(twork,rectangles);
		 	mPcs.addPropertyChangeListener(twork);
		}
		int max=rootTxns.size();
		int count=0;
		for (String txn : rootTxns)
		{
			// System.out.println("calculating positions for "+txn);
			SAEvent sae = rootTxnEvents.get(txn);
			if (sae != null)
				rectangles = calcpos(rectangles, sae, getDefaultHeight(sae.txn));
			count++;
			//System.out.println("display(%) "+count*100/max);
			if(twork!=null){
				if(twork.isCancelled()) 
				{
					workersRectangles.remove(twork);
					return null;
				}
				mPcs.firePropertyChange("rectangles",
                    null, rectangles.clone());
				twork.progressEvent(count*100/max);
			}
		}
		if(twork!=null)
		{
			workersRectangles.remove(twork);
			mPcs.removePropertyChangeListener(twork);
		}
		// System.out.println("number of rectangles "+ rectangles.size());
		return rectangles;
	}

	private HashMap<String, TxnRect> calcpos(
			HashMap<String, TxnRect> rectangles, SAEvent sae, int baselevel)
	{
		// System.out.println("caclpos called");
		if (sae != null & getRangeStartTime(sae.txn) != null)
		{

			TxnRect t = new TxnRect(getRangeStartTime(sae.txn),
					getRangeEndTime(sae.txn), Math.max(baselevel,
							getDefaultHeight(sae.txn)));
			// check for overlaps with other txn
			t.sae = sae;
			boolean foundpos = false;
			while (!foundpos)
			{
				foundpos = true;
				if (rectangles.size() != 0)
				{
					for (TxnRect r : rectangles.values())
					{
						// System.out.println(t.overlap(r));
						foundpos = foundpos & !t.overlap(r);
					}
					if (!foundpos)
						t.layer = t.layer + 1;
				} else
					foundpos = true;
			}
			rectangles.put(sae.txn, t);
			// now position the children
			for (SAEvent s : sae.children.values())
			{
				// calculate base level
				// get level of root

				rectangles = calcpos(rectangles, s,
						Math.max(baselevel, t.layer));
			}
		}
		return rectangles;
	}
	
	public List<String> getFilenames()
	{
		ArrayList<String> a = new ArrayList<String>();
		a.add("");
		Collection<SAEvent> col = txnEvents.values();
		for(SAEvent sae : col)
		{
			if(!a.contains(sae.file)) a.add(sae.file);
		}
		return a;
	}

	public List<String> getLocations()
	{
		ArrayList<String> a = new ArrayList<String>();
		a.add("");
		Collection<SAEvent> col = txnEvents.values();
		for(SAEvent sae : col)
		{
			if(!a.contains(sae.location)) a.add(sae.location);
		}
		return a;
	}

	public List<String> getOperations()
	{
		ArrayList<String> a = new ArrayList<String>();
		a.add("");
		Collection<SAEvent> col = txnEvents.values();
		for(SAEvent sae : col)
		{
			String function=sae.layer+"..."+sae.component+"..."+sae.function;
			if(!a.contains(function) & !function.contains("null")) a.add(function);
		}
		Collections.sort(a);
		return a;
	}
	
	public void addRootFilter(RootTxnWorker worker)
	{
		this.worker =worker;
	}
	private RootTxnWorker worker=null;
	
	
	public List<String> getFilteredRootTxns(String location,String file,String function)
	{
		ArrayList<String> a = new ArrayList<String>();
		ArrayList<SAEvent> fl=new ArrayList<SAEvent>();
		Collection<SAEvent> col = txnEvents.values();
		String ffile="";
		String llocation="";
		String ffunction="";
		if(file!=null)ffile=file;
		if(location!=null)llocation=location;
		if(function!=null)ffunction=function;
		
		int perNum=col.size();
		int num=0;
		for(SAEvent sae : col)
		{
			String f=sae.layer+"..."+sae.component+"..."+sae.function;
			if(!ffile.equals("") && sae.file.equals(ffile))
			{
				if( !llocation.equals("") & llocation.equals(sae.location)){
					if(ffunction.equals("") || ffunction.equals(f))fl.add(sae);
				}
				else if(llocation.equals("")){
					if(ffunction.equals("") || ffunction.equals(f))fl.add(sae);
				}
			}
			else if(ffile.equals(""))
			{
				if(!llocation.equals("") & llocation.equals(sae.location)){
					if(ffunction.equals("") || ffunction.equals(f))fl.add(sae);	
				}
				else if(llocation.equals("")){
					if(ffunction.equals("") || ffunction.equals(f))fl.add(sae);
				}
			}
			num++;
			if(worker!=null)worker.progressEvent(num*50/perNum);
		}
		ArrayList<SAEventFacade> evlist = new ArrayList<SAEventFacade>();
		HashSet<SAEventFacade> eevlist=new HashSet<SAEventFacade>();
		perNum=fl.size();
		num=0;
		for(SAEvent sae :fl)
		{
			String rTxnNumber=SAEvent.getRootTxn(sae.txn);
			SAEvent rTxn=rootTxnEvents.get(rTxnNumber);
			if(rTxn!=null)
			{
				SAEventFacade saef=new SAEventFacade(rTxn);
				//if(!evlist.contains(saef)) evlist.add(saef);
				eevlist.add(saef);
			}
			num++;
			if(worker!=null)worker.progressEvent(num*50/perNum+50);
			
		}
		for(SAEventFacade ev:eevlist)
			evlist.add(ev);
		Collections.sort(evlist);
		for (SAEventFacade ev : evlist)
			a.add(ev.sae.txn);
		
		return a;
	}

/*	private List<String> getRootTxns()
	{
		ArrayList<String> a = new ArrayList<String>();
		// ArrayList<Date> b = new ArrayList<Date>();
		// ArrayList<String> c = new ArrayList<String>();
		// HashMap<Date,String> ll=new HashMap<Date,String>();
		// Enumeration<String> e=rootTxnEvents.keys();
		Collection<SAEvent> col = rootTxnEvents.values();
		ArrayList<SAEventFacade> evlist = new ArrayList<SAEventFacade>();
		for (SAEvent ev : col)
			evlist.add(new SAEventFacade(ev));
		Collections.sort(evlist);

		/*
		 * while(e.hasMoreElements()) { String next=e.nextElement();
		 * //a.add(next);
		 * 
		 * if(rootTxnEvents.get(next).startTime!=null) {
		 * b.add(rootTxnEvents.get(next).startTime);
		 * ll.put(rootTxnEvents.get(next).startTime, next); } else { Long
		 * l=getRangeStartTime(next); System.out.println("range start time is "+
		 * l); if(l==null) a.add(next); else { Date d=new Date(l.longValue());
		 * System.out.println(d); b.add(d); ll.put(d, next);
		 * 
		 * } } }
		 * 
		 * 
		 * Collections.sort(b); //for(Date d:b) //{ // a.add(ll.get(d)); //}
		 * /
		for (SAEventFacade ev : evlist)
			a.add(ev.sae.txn);
		return a;
	}
*/

	public SAEvent getSAEvent(String txn)
	{
		return txnEvents.get(txn);
	}

	public void generateHierarchy()
	{
		BWReader bwr = Controller.getController().getReader();
		for (SAEvent sae : txnEvents.values())
		{
			String pTxn = SAEvent.getParentTxn(sae.txn);
			if (pTxn != null)
			{
				SAEvent pae = txnEvents.get(pTxn);
				if (pae != null)
				{
					Service pService = new Service(pae.layer, pae.component,
							pae.function, "");
					Service cService = new Service(sae.layer, sae.component,
							sae.function, "");
					if (pae.layer != null & pae.component != null
							& pae.function != null & sae.layer != null
							& sae.component != null & sae.function != null)
						bwr.addChildService(pService, cService);
				}
			}
		}
	}

	private ConcurrentHashMap<String, SAEvent> rootTxnEvents = new ConcurrentHashMap<String, SAEvent>();
	private ConcurrentHashMap<String, SAEvent> txnEvents = new ConcurrentHashMap<String, SAEvent>();

	public void memoryUsageLow(long usedMemory, long maxMemory) {
		System.out.println("memory getting low, used="+usedMemory+", maximum="+maxMemory);
		if(collectTxn){
			System.out.println("Turning off transaction capturing");
			System.out.println("Number of events in system is "+txnEvents.size());
			collectTxn=false;
			mws.removeListener(this);
			for(CollectionListener c: listeners) c.collectorChange(false, txnEvents.size());
		}
	}

}
