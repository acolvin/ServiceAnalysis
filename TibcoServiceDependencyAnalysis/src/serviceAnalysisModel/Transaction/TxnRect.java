package serviceAnalysisModel.Transaction;

import ServiceAnalysisClient.SAEvent;

public class TxnRect
{
	public Long start = null, end = null;
	public int layer = 0;
	public SAEvent sae = null;

	public TxnRect()
	{

	}

	public TxnRect(long start, long end, int layer)
	{
		if (start > end)
		{
			this.start = end;
			this.end = start;
			this.layer = layer;
		} else
		{
			this.start = start;
			this.end = end;
			this.layer = layer;
		}
	}

	public boolean overlap(TxnRect t)
	{
		if (t.layer != layer)
			return false;
		else
			return !(start >= t.end | end <= t.start);
	}

	public String toString()
	{
		return sae.component + "." + sae.function + " (" + start + "," + end
				+ "," + layer + ") txn=" + sae.txn;
	}

}
