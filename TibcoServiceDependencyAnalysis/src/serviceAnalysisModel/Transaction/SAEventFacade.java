package serviceAnalysisModel.Transaction;

import java.util.Date;

import ServiceAnalysisClient.SAEvent;

public class SAEventFacade implements Comparable<SAEventFacade>
{
	public SAEventFacade(SAEvent sae)
	{
		this.sae = sae;
	}

	public SAEvent sae;
	
	private int fHashCode=0;
	

	@Override 
	public int hashCode() 
	{
	    if ( fHashCode == 0 ) 
	    {
	      fHashCode = sae.txn.hashCode();
	    }
	    return fHashCode;
	  }
	
	@Override
	public boolean equals(Object o)
	{
		 if ( this == o ) return true;
		 if ( !(o instanceof SAEventFacade) ) return false;
		 SAEventFacade saef=(SAEventFacade)o;
		 return sae.equivelent(saef.sae);
	}

	@Override
	public int compareTo(SAEventFacade o)
	{
		// System.out.println("comparing "+this+" with "+o);
		if (o == null && sae == null)
			return 0;
		if (o == null)
			return -1; // if either object is null return
		if (sae == null)
			return 1;

		Date thisstart = sae.startTime;
		Date otherstart = o.sae.startTime;
		long thisstartmilli = 0;
		long otherstartmilli;

		if (thisstart == null)
		{
			Long l = EventCollection.getInstance().getRangeStartTime(sae.txn);
			if (l == null)
				thisstartmilli = 0;
			else
				thisstartmilli = l;
		} else
		{
			thisstartmilli = thisstart.getTime();
		}
		if (otherstart == null)
		{
			Long l = EventCollection.getInstance().getRangeStartTime(o.sae.txn);
			if (l == null)
				otherstartmilli = 0;
			else
				otherstartmilli = l;
		} else
		{
			otherstartmilli = otherstart.getTime();
		}
		// System.out.println("calculated millsecs "+thisstartmilli+", "+otherstartmilli+" difference "+(thisstartmilli-otherstartmilli));
		if (thisstartmilli == otherstartmilli)
		{/* System.out.println("Returning 0"); */
			return 0;
		} else if (thisstartmilli < otherstartmilli)
		{/* System.out.println("Returning -1"); */
			return -1;
		} else
		{/* System.out.println("Returning 1"); */
			return 1;
		}

	}

}
