/**
 * 
 */
package serviceAnalysisModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * This service represents a Service Operation modelling any type of system
 * interaction. It can have a type which indicates the subsystem that provides
 * the service operation combination.
 * 
 * @author apc
 * 
 */

public class Service implements Serializable, Comparable<Object>
{

	/**
     * 
     */
	private static final long serialVersionUID = 7454487010496835802L;

	public static final String UI_TYPE = "UI";
	public static final String BW_TYPE = "BW";
	public static final String DB_TYPE = "DB";
	public static final String WS_TYPE = "WS";

	public RateMeter ratem = new RateMeter(100, new Date());
	public HashMap<String,RateMeter> locationRates=new HashMap<String,RateMeter>();

	/**
	 * The basic constructor creating the service operation object. The use of
	 * this has been deprecated and the method made private.
	 * 
	 * @param name
	 *            The name of the service operation in the form
	 *            "service.operation"
	 */
	private Service(String name)
	{
		super();

		String[] names = name.split("\\.");
		this.type = "BW";
		if (name.length() == 2)
		{
			this.service = ServiceMapper.replace(names[0],
					ServiceMapper.SERVICE);
			this.operation = ServiceMapper.replace(names[1],
					ServiceMapper.OPERATION);
		} else
		{
			String servicename = "";
			for (int i = 0; i < names.length - 1; i++)
			{
				servicename += names[i];
				if (i < names.length - 2)
					servicename += ".";
			}
			this.service = ServiceMapper.replace(servicename,
					ServiceMapper.SERVICE);
			this.operation = ServiceMapper.replace(names[names.length - 1],
					ServiceMapper.OPERATION);
		}

		this.addData = "";
		sName = service + "." + operation;
	}

	/**
	 * This constructor allows the Service object to be created specifying the
	 * its different major properties in the constructor
	 * 
	 * @param type
	 *            The subsystem type that provides the Service
	 * @param service
	 *            The service name
	 * @param operation
	 *            The operation name
	 * @param addData
	 *            A string object to hold additional data if required
	 */
	public Service(String type, String service, String operation, String addData)
	{
		super();

		this.type = type;
		this.service = ServiceMapper.replace(service, ServiceMapper.SERVICE);
		this.operation = ServiceMapper.replace(operation,
				ServiceMapper.OPERATION);
		this.addData = addData;
		sName = this.service + "." + this.operation;

	}

	/**
	 * This constructor creates a Service object and allows a list of services
	 * to be supplied that will be used as a default set of dependent services
	 * 
	 * @param dependents
	 *            The List of service.operations that this operation calls
	 * @param name
	 *            The "service.operation" name
	 */
	public Service(List<Service> dependents, String name)
	{
		this(name);
		this.dependents = dependents;

	}

	/**
	 * Create a service operation and adds the given service as a dependent. A
	 * check is made that these two services are not equivalent (ie same service
	 * name and operation name).
	 * 
	 * @param name
	 *            The "service.operation" to create
	 * @param dep
	 *            The Service which is identified as a dependent service of this
	 *            one.
	 */
	public Service(String name, Service dep)
	{
		this(name);
		if (!dep.getSName().equals(name))
			dependents.add(dep);
	}

	public String toString()
	{
		return getSName();
	}

	/**
	 * Models the provided service as a dependent of this service object. It
	 * checks that the two services are not equivalent and sets this service as
	 * a consumer of the provided service.
	 * 
	 * @param dep
	 *            The Service object that is to be set as the dependent.
	 */
	public void addDependentService(Service dep)
	{
		if (!dependents.contains(dep) && !dep.getSName().equals(sName))
		{
			dependents.add(dep);
			iterations.put(dep, 1d);
			notifyEvents();
		}
		// System.out.println("Adding Dependent Service: "+dep.getSName()+
		// " to " + sName);
		dep.addConsumingService(this);

	}
	
	public void removeDependentService(Service dep)
	{
		if(dependents.contains(dep))
		{
			dependents.remove(dep);
			iterations.remove(dep);
			notifyEvents();
			dep.removeConsumingService(this);
		}
	}

	// used to set the number of times this service calls a dependant service
	/**
	 * Sets the number of times this service calls a dependent service in an
	 * average transaction. The default is 1.0
	 * 
	 * @param dep
	 *            The dependent service
	 * @param iter
	 *            The number of times the dependent service is called
	 */
	public void setDependentIteration(Service dep, double iter)
	{
		iterations.put(dep, iter);
	}

	// adds iterations from this service to the dependent service
	/**
	 * Adds the given iteration number to the existing iterations that this
	 * service calls a dependent
	 * 
	 * @param dep
	 *            The dependent Service
	 * @param iter
	 *            The value representing the iteration count to be added to the
	 *            existing count
	 * @return The resulting iteration count
	 */
	public double addDependentIterations(Service dep, double iter)
	{
		Double i = iterations.get(dep);
		if (i == null)
			i = 0d;
		i = i + iter;
		iterations.put(dep, i);
		return i;
	}

	// gets the iterations map of all dependent services
	/**
	 * Returns a map of keyed by dependent service and the iteration for each
	 * dependent Service
	 * 
	 * @return The map of dependent iteration combinations
	 */
	public Map<Service, Double> getIterations()
	{
		return iterations;
	}

	// private Date previousTime=new Date();
	// private Double rate=0d;

	/**
	 * This operation manages the rate meter measuring the time number of
	 * samples received per second. This is returned based on the last 100
	 * samples to give stability.
	 * 
	 * @param when
	 *            The time of the timed sample
	 */
	public double calculateRate(Date when)
	{
		// long a=when.getTime();
		// long diff;
		// synchronized(previousTime)
		// {diff=a-previousTime.getTime();previousTime=when;}
		// synchronized(rate){rate=1000d/(new Double(diff)).doubleValue();}
		ratem.addSample(when);
		if (sampleCapture)
		{
			// numberSamples++;
			// System.out.println("number of samples "+numberSamples);
			// if(number>=size)rateSamples.put(date,getRate());
			Long l = when.getTime() / samplePeriod;
			synchronized(countPerSecond)
			{
				if (countPerSecond.containsKey(l)) // all this count stuff should
			
												// move out of here and go into
												// service
				{
					Integer i = countPerSecond.get(l);
					if (i != null)
					{
						i = i + 1;
					} else
						i = 1;
					countPerSecond.put(l, i);
				// System.out.println(new
				// Date(l*1000)+" "+i+" "+countPerSecond.get(l));
				} else
					countPerSecond.put(l, new Integer(1));
			}
		}
		return ratem.getRate();
	}

	private boolean trackLocationRates=false;
	public void setTrackLocationRates(boolean track){
		trackLocationRates=track;
		if(!trackLocationRates)synchronized(locationRates){
			System.out.println("Clearing location rates for service "+toString());
			locationRates.clear();
		}
	}
	
	public double calculateRate(Date when,String location)
	{
		if(!trackLocationRates || location==null || location.length()==0)return calculateRate(when);
		RateMeter rate;
		synchronized(locationRates){
			if(!locationRates.containsKey(location))locationRates.put(location,new RateMeter(100, new Date()));
			rate=locationRates.get(location);
		}
		// long a=when.getTime();
		// long diff;
		// synchronized(previousTime)
		// {diff=a-previousTime.getTime();previousTime=when;}
		// synchronized(rate){rate=1000d/(new Double(diff)).doubleValue();}
		ratem.addSample(when);
		rate.addSample(when);
		if (sampleCapture)
		{
			// numberSamples++;
			// System.out.println("number of samples "+numberSamples);
			// if(number>=size)rateSamples.put(date,getRate());
			Long l = when.getTime() / samplePeriod;
			synchronized(countPerSecond)
			{
				if (countPerSecond.containsKey(l)) // all this count stuff should
			
												// move out of here and go into
												// service
				{
					Integer i = countPerSecond.get(l);
					if (i != null)
					{
						i = i + 1;
					} else
						i = 1;
					countPerSecond.put(l, i);
				// System.out.println(new
				// Date(l*1000)+" "+i+" "+countPerSecond.get(l));
				} else
					countPerSecond.put(l, new Integer(1));
			}
		}
		return rate.getRate();
	}
	/**
	 * stores the samples per period
	 */
	private HashMap<Long, Integer> countPerSecond = new HashMap<Long, Integer>();
	private HashMap<Long, Double> sumPerSecond = new HashMap<Long, Double>();
	private HashMap<Long, Double> sumSqPerSecond = new HashMap<Long, Double>();
	private HashMap<Long, Double> maxPerSecond = new HashMap<Long, Double>();
	private HashMap<Long, Double> minPerSecond = new HashMap<Long, Double>();
	private boolean sampleCapture = true;

	public void setSamplePeriod(int period)
	{
		samplePeriod = period;
		ratem.setSamplePeriod(period);
	}

	@SuppressWarnings("unchecked")
	public HashMap<Long, Integer> getCountPerSecond()
	{
		return (HashMap<Long, Integer>) countPerSecond.clone();
		
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<Long, Double> getSumsPerSecond()
	{
		return (HashMap<Long, Double>) sumPerSecond.clone();
		
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<Long, Double> getSumSqPerSecond()
	{
		return (HashMap<Long, Double>) sumSqPerSecond.clone();
		
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<Long, Double> getMaxPerSecond()
	{
		return (HashMap<Long, Double>) maxPerSecond.clone();
		
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<Long, Double> getMinPerSecond()
	{
		return (HashMap<Long, Double>) minPerSecond.clone();
		
	}
	
	public void setCountPerSecond(HashMap<Long, Integer> countPerSample)
	{
		countPerSecond=countPerSample;
	}
	public void setSumsPerSecond(HashMap<Long, Double> sumsPerSample)
	{
		sumPerSecond=sumsPerSample;
	}
	public void setSumSqPerSecond(HashMap<Long, Double> sumSqSample)
	{
		sumSqPerSecond=sumSqSample;
	}

	public void setMaxPerSecond(HashMap<Long, Double> maxPerSample)
	{
		maxPerSecond=maxPerSample;
	}
	
	public void setMinPerSecond(HashMap<Long, Double> minPerSample)
	{
		minPerSecond=minPerSample;
	}

	
	public int getSamplePeriod()
	{
		return samplePeriod;
	}

	private int samplePeriod = 60000; // in milliseconds

	public void setSampleCapture(boolean state)
	{
		sampleCapture = state;
		ratem.setSampleCapture(state);
	}

	public boolean isSampleCapture()
	{
		return sampleCapture;
	}

	public countMaps getRollingCount(int period) // in samplePeriods
	{
		@SuppressWarnings("unchecked")
		HashMap<Long, Integer> clone = (HashMap<Long, Integer>) countPerSecond.clone();
		HashMap<Date, Integer> rcounts = new HashMap<Date, Integer>();
		HashMap<Date, Integer> counts = new HashMap<Date, Integer>();
		/*
		 * for(Entry<Long, Integer> s:clone.entrySet()) { Long l=s.getKey();
		 * long r=l; int c=0; for(int i=0;i<period;i++) { Integer
		 * j=clone.get(r); if(j!=null) c+=j; r--;
		 * 
		 * } counts.put(new Date(l*samplePeriod),c ); }
		 */

		for (Entry<Long, Integer> s : clone.entrySet())
		{
			Long l = s.getKey();
			long r = l;
			Integer c = 0;
			Date d;

			for (int i = 0; i < period; i++)
			{
				r = (l + i) * samplePeriod;
				d = new Date(r);
				if (i == 0)
					c = counts.get(d);
				else
					c = rcounts.get(d);
				if (c == null)
				{
					if (i == 0)
						counts.put(d, s.getValue());
					else
						rcounts.put(d, s.getValue());
				} else
				{
					c = c + s.getValue();
					if (i == 0)
						counts.put(d, c);
					else
						rcounts.put(d, c);
				}

			}
		}

		countMaps a = new countMaps();
		a.setaCounts(counts);
		a.setrCounts(rcounts);

		return a;
	}

	public HashMap<Date, Integer> getSampleCount(int period) // in samplePeriods
	{
		@SuppressWarnings("unchecked")
		HashMap<Long, Integer> clone = (HashMap<Long, Integer>) countPerSecond
				.clone();
		HashMap<Date, Integer> counts = new HashMap<Date, Integer>();
		for (Entry<Long, Integer> s : clone.entrySet())
		{
			// System.out.println(s.getKey()+" "+s.getValue());
			Long l = s.getKey() / period * period;
			Date d = new Date(l * samplePeriod);

			if (counts.containsKey(d))
			{
				Integer i = counts.get(d) + s.getValue();
				counts.put(d, i);
			} else
			{

				counts.put(d, s.getValue());
			}
		}

		return counts;

	}
	
	public HashMap<Date, Double> getSampleMax(int period) // in samplePeriods
	{
		@SuppressWarnings("unchecked")
		HashMap<Long, Double> clone = (HashMap<Long, Double>) maxPerSecond
				.clone();
		HashMap<Date, Double> maxs = new HashMap<Date, Double>();
		for (Entry<Long, Double> s : clone.entrySet())
		{
			//System.out.println(s.getKey()+" "+s.getValue());
			Long l = s.getKey() / period * period;
			Date d = new Date(l * samplePeriod);
			//System.out.println(""+new Date(s.getKey()*samplePeriod)+"-->"+d);
			if (maxs.containsKey(d))
			{
				Double m = Math.max(maxs.get(d) , s.getValue());
				//System.out.println("max("+maxs.get(d)+","+s.getValue()+")="+m);
				maxs.put(d, m);
			} else
			{

				maxs.put(d, s.getValue());
			}
		}

		return maxs;

	}
	
	public HashMap<Date, Double> getSampleMin(int period) // in samplePeriods
	{
		@SuppressWarnings("unchecked")
		HashMap<Long, Double> clone = (HashMap<Long, Double>) minPerSecond
				.clone();
		HashMap<Date, Double> mins = new HashMap<Date, Double>();
		for (Entry<Long, Double> s : clone.entrySet())
		{
			//System.out.println(s.getKey()+" "+s.getValue());
			Long l = s.getKey() / period * period;
			Date d = new Date(l * samplePeriod);
			//System.out.println(""+new Date(s.getKey()*samplePeriod)+"-->"+d);
			if (mins.containsKey(d))
			{
				Double m = Math.min(mins.get(d) , s.getValue());
				//System.out.println("min("+mins.get(d)+","+s.getValue()+")="+m);
				mins.put(d, m);
				
			} else
			{
				//System.out.println("min being set to "+s.getValue());
				mins.put(d, s.getValue());
			}
		}

		return mins;

	}
	
	public HashMap<String,Double> limitedMean(Date start, Date end, int period)
	{
		@SuppressWarnings("unchecked")
		HashMap<Long,Double> cloneSum= (HashMap<Long, Double>) sumPerSecond.clone();
		@SuppressWarnings("unchecked")
		HashMap<Long, Integer> cloneCount = (HashMap<Long, Integer>) countPerSecond
				.clone();
		ArrayList<Double> ordered=new ArrayList<Double>();
//		HashMap<Date, Double> averages = new HashMap<Date, Double>();
//		HashMap<Date, Integer> groupCounts=new HashMap<Date, Integer>();
		double fullsum=0;
		double fullcount=0;
		for (Entry<Long, Double> s : cloneSum.entrySet())
		{
			double sum=s.getValue();
			Integer count=cloneCount.get(s.getKey());
			if(count!=null)
			{
				Long l = s.getKey() / period * period;
				Date d = new Date(l * samplePeriod);
				if(d.after(start) && d.before(end))
				{
					fullsum+=sum;
					fullcount+=count;
					if(count>0)
					{
						ordered.add(sum/count);
					}
				}
			}

		}
		Collections.sort(ordered);
		HashMap<String,Double> results=new HashMap<String,Double>();
		double per95num=95d*((double)(ordered.size()))/100d;
		double per99num=99d*((double)(ordered.size()))/100d;
		if(ordered.size()>0){
			int i=(int)Math.floor(per95num)-1;
			if(i<0)i=0;
			double lower = ordered.get(i);
			
			i=(int)Math.ceil(per95num)-1;
			if(i>=ordered.size())i=ordered.size()-1;
			double upper = ordered.get(i);
			double value=0;
			if(lower==upper) value=lower;
			else
				value=(upper-lower)*(per95num-Math.floor(per95num))+lower;
			results.put("95",ordered.get(i));
			results.put("95calc",value);
			
			
			i=(int)Math.floor(per99num)-1;
			if(i<0)i=0;
			lower = ordered.get(i);
			i=(int)Math.ceil(per99num)-1;
			if(i>=ordered.size())i=ordered.size()-1;
			upper = ordered.get(i);
			if(lower==upper) value=lower;
			else
				value=(upper-lower)*(per99num-Math.floor(per99num))+lower;
			results.put("99",ordered.get(i));
			results.put("99calc",value);
		}
		if(fullcount==0)fullcount=1;
		results.put("MEAN", fullsum/fullcount);
		return results;
	}
	
	public HashMap<String, HashMap<Date, Double>> getSampleAverage(int period)
	{
		@SuppressWarnings("unchecked")
		HashMap<Long,Double> cloneSum= (HashMap<Long, Double>) sumPerSecond.clone();
		HashMap<Long,Double> cloneSqSum= new HashMap<Long, Double>( sumSqPerSecond);
		@SuppressWarnings("unchecked")
		HashMap<Long, Integer> cloneCount = (HashMap<Long, Integer>) countPerSecond
				.clone();
		HashMap<Date, Double> averages = new HashMap<Date, Double>();
		HashMap<Date, Integer> groupCounts=new HashMap<Date, Integer>();
		HashMap<Date, Double> sumsq = new HashMap<Date, Double>();
		for (Entry<Long, Double> s : cloneSum.entrySet())
		{
			double sum=s.getValue();
			Integer count=cloneCount.get(s.getKey());
			Double sum2=cloneSqSum.get(s.getKey());
			if(sum2==null)sum2=0D;
			if(count!=null)
			{
				Long l = s.getKey() / period * period;
				Date d = new Date(l * samplePeriod);
				Date d2=new Date((l+1)*samplePeriod);
				Date d3=new Date((l-1)*samplePeriod);
				if(averages.get(d3)==null)averages.put(d3, 0d);
				if(averages.get(d2)==null)averages.put(d2, 0d);
				if(sumsq.get(d3)==null)sumsq.put(d3, 0d);
				if(sumsq.get(d2)==null)sumsq.put(d2, 0d);
				Double current=averages.get(d);
				if(current==null)
					current=0d;
				Double sqcurrent=sumsq.get(d);
				if(sqcurrent==null)
					sqcurrent=0d;
				
				Integer currentCount=groupCounts.get(d);
				if(currentCount==null) currentCount=count;
				else currentCount+=count;
				
				groupCounts.put(d, currentCount);
				
				averages.put(d , sum+current);
				sumsq.put(d, sum2+sqcurrent);
			}
			
			
		}
		
		for (Entry<Date, Double> s : averages.entrySet())
		{
			double sum=s.getValue();
			
			Date d=s.getKey();
			double sum2;
			if(sumsq.containsKey(d))sum2=sumsq.get(d);
			else sum2=0d;
			Integer i=groupCounts.get(d);
			if(i!=null)
			{
				double average=sum/i;
				averages.put(d, average);
				if(i<2)sumsq.put(d, 0d);
				else {
					double stddev=Math.sqrt((sum2 - 2.0d*average*sum + i*average*average)/(i));
					sumsq.put(d,stddev);
				}
				
			}
			else {
				averages.put(d, 0d);
				sumsq.put(d, 0d);
			}
		}
		HashMap<String, HashMap<Date, Double>> answer = new HashMap<String,HashMap<Date,Double>>();
		answer.put("mean", averages);
		answer.put("stddev", sumsq);
		return answer;
		
	}
	/**
	 * Allows the service to have all of its counters reset. It does not remove
	 * the consumers,dependents or iterations
	 * 
	 */
	public synchronized void resetService()
	{
		avgtime = 0;
		adjustments = 0;
		explicitAdjustment = 0;
		sumSqAvgTime = 0;
		sumAvgTime = 0;
		sumLog = 0;
		sumLogSq = 0;
		initialTime = -1;
		lastnewtime = -1;
		number = 0;
		waitNumber = 0;
		avgWaitTime = 0;
		samples = new HashMap<Double, Double>();
		sumHarmonic = 0;
		maxTime=-1; 
		minTime=Float.MAX_VALUE;
		countPerSecond.clear();
		sumPerSecond.clear();
		sumSqPerSecond.clear();
		maxPerSecond.clear();
		minPerSecond.clear();
		cumulative.clear();
		ratem.reset();
		locationRates.clear();
		runningAverage.clear();
		setTrackLocationRates(false);
		//ratem = new RateMeter(100, new Date());
		notifyEvents();

	}

	/**
	 * Gets the current rate from the rate meter measured over the last 100
	 * samples
	 * 
	 * @return The current sample rate
	 */
	public double getRate()
	{
		return ratem.getRate();
		// synchronized(rate){return rate;}
	}
	
	public HashMap<String,Double> getRate(String location)
	{
		HashMap<String,Double> res=new HashMap<String,Double>();
		res.put("", ratem.getRate());
		if(location==null || location.length()==0 || !locationRates.containsKey(location)) return res;
		else res.put(location, locationRates.get(location).getRate());
		return res;
		// synchronized(rate){return rate;}
	}
	
	

	public void startRateSampleCapture(int rate)
	{
		if (!ratem.isSampleCapture())
		{
			ratem.setSamplePeriod(rate);
		} else
		{
			ratem.setSampleCapture(false);
			ratem.reset();
			ratem.setSamplePeriod(rate);

		}
		ratem.setSampleCapture(true);
	}

	public void stopRateSampleCapture()
	{
		ratem.setSampleCapture(false);
	}

	public HashMap<Date, Double> getRates()
	{
		return ratem.getRateSamples();
	}
	

	public HashMap<Date, Double> getRates(int size)
	{
		return ratem.getRateSamples(size);
	}

	/**
	 * Sets the given Service as a consumer of this
	 * 
	 * @param con
	 *            The consuming service operation
	 */
	public void addConsumingService(Service con)
	{
		if (!consumers.contains(con) && !con.getSName().equals(sName))
		{
			consumers.add(con);
			notifyEvents();
			// System.out.println("Adding Consuming Service: "+con.getsName()+" to "+
			// sName);
			// System.out.println("Number of consumers: "+ consumers.size());
		}
	}
	
	public void removeConsumingService(Service con)
	{
		if(consumers.contains(con)) 
		{
			consumers.remove(con);
			con.removeDependentService(this);
			notifyEvents();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if(obj==null)return false;
		if(obj.getClass()!=this.getClass())return false;
		Service a = (Service) obj;
		return this.equals(a);
	}

	public int hashCode()
	{
		return sName.hashCode();

	}

	/**
	 * Returns true if the "service.operation" strings of the 2 Services match
	 * 
	 * @param s
	 *            The service to test against this one
	 * @return true/false
	 */
	public boolean equals(Service s)
	{

		return s.getServiceName().equals(service) & s.getType().equals(type)
				& s.getOperationName().equals(operation);
		// return s.getSName().equals(sName);
	}

	/**
	 * Checks if this service would be equivalent to one created from the given
	 * "service.operation" string
	 * 
	 * @param name
	 *            The "service.operation" string to check for match
	 * @return true/false
	 */
	// public boolean equals(String name){
	// return name.equals(sName);
	// }

	/**
	 * Outputs all dependent services to standard out
	 */
	public void print()
	{
		// Service i;
		Iterator<Service> it = dependents.iterator();
		while (it.hasNext())
		{
			System.out.println(sName + ", " + (it.next().getSName()));
		}
	}

	private RunningAverage runningAverage=new RunningAverage(10);
	private float initialTime = -1, lastnewtime = -1, maxTime=-1, minTime=Float.MAX_VALUE;

	/**
	 * Adds a response time event to the service which calculating many internal
	 * numbers and returns the new average. Adds the response event to the
	 * frequency sample set. If notify is true the change of average time is
	 * notified to all consumers of this service operation as an offset from the
	 * last value.
	 * 
	 * @param newtime
	 *            The new response time to add
	 * @param notify
	 *            true/false
	 * @return The new average time
	 */
	public float addTime(float newtime, boolean notify)
	{
		if (initialTime == -1)
			lastnewtime = initialTime = getOperationTime();
		float adj = lastnewtime - initialTime;
		lastnewtime = addTime(newtime);
		// System.out.println(initialTime+" "+getOperationTime()+ " "+
		// newtime+" "+adj);
		if (notify)
			adjConsumerTimes(-lastnewtime + adj + initialTime);
		notifyEvents();
		return lastnewtime;
	}

	/**
	 * Adds a response time event to the service which calculating many internal
	 * numbers and returns the new average. Adds the response event to the
	 * frequency sample set.
	 * 
	 * 
	 * @param newtime
	 *            The new response time to add
	 * @return The current average after adding the response time
	 */
	public synchronized float addTime(float newtime)
	{
		if(newtime>maxTime)maxTime=newtime;
		if(newtime<minTime)minTime=newtime;
		if(runningAverage==null)runningAverage=new RunningAverage(10);
		runningAverage.addPoint(newtime);
		sumSqAvgTime += (double) newtime * (double) newtime;
		sumAvgTime += (double) newtime;
		sumLog += Math.log(newtime);
		sumLogSq += Math.pow(Math.log(newtime), 2);
		sumHarmonic += 1d / (double) newtime;

		double sample = Math.floor((double) newtime) + 0.5d;
		if (samples.containsKey(sample))
		{
			samples.put(sample, new Double(
					samples.get(sample).doubleValue() + 1.0d));
		} else
			samples.put(sample, new Double(1.0d));

		return avgtime = (avgtime * number + newtime) / ++number;
	}

	public synchronized void addTimeSample(Long time, double response)
	{
		if (sumPerSecond.containsKey(time))
		{
			sumPerSecond.put(time, new Double(
					sumPerSecond.get(time).doubleValue()+response ));
			sumSqPerSecond.put(time, new Double(
					sumSqPerSecond.get(time).doubleValue()+response*response ));
			maxPerSecond.put(time, Math.max(maxPerSecond.get(time).doubleValue(),response));
			minPerSecond.put(time, Math.min(minPerSecond.get(time).doubleValue(),response));

		} else {
			sumPerSecond.put(time, new Double(response));
			sumSqPerSecond.put(time, new Double(response*response));
			minPerSecond.put(time, response);
			maxPerSecond.put(time, response);
		}
	}
	
	
	
	private double sumHarmonic = 0;
	private float avgWaitTime = 0;
	private int waitNumber = 0;

	public float addWaitTime(float waitTime)
	{
		return avgWaitTime = (avgWaitTime * waitNumber + waitTime)
				/ ++waitNumber;
	}

	public float getAvgWaitTime()
	{
		return avgWaitTime;
	}
	
	public float getMaxTime()
	{
		if(maxTime!=-1)return maxTime;
		else return 0;
	}
	
	public float getMinTime()
	{
		if(minTime!=Float.MAX_VALUE) return minTime;
		else return 0;
	}

	public double getRunningAverage()
	{
		if(runningAverage==null){
			//maybe an initialisation issue
			System.out.println("runningAverage is null for "+getsName());
			runningAverage=new RunningAverage(10);
			return 0;
		}
		return runningAverage.getRunningAverage();
	}
	
	/**
	 * As the response graph of any service.operation if of log normal format
	 * and not normal format, this method calculate and returns the median
	 * assuming log normality.
	 * 
	 * @return The log median value: exp(mean of the log of the response times)
	 */
	public double getOperationTimeLogMedian()
	{
		return Math.exp(sumLog / (double) number);
	}

	private HashMap<Double, Double> cumulative = new HashMap<Double, Double>();

	public HashMap<Double, Double> calculateCumulativeDistribution()
	{
		double c = (double) number / 100d;
		TreeSet<Double> ordered = new TreeSet<Double>();

		for (Double d : samples.keySet())
		{
			ordered.add(d); // order the samples

		}

		double running = 0;

		for (Double d : ordered)
		{
			double cd = samples.get(d);
			running += cd;
			cumulative.put(d, running / c);
		}
		return cumulative;

	}

	public synchronized double getPercentileSamples(double percentile)
	{
		double p = 0;
		if (number == 0)
			return 0d;
		if (number == 1)
			return (double) getAvgtime();
		// int entry=(int)percentile*number/100;
		HashMap<Double, Double> cfd = calculateCumulativeDistribution();
		HashMap<Double, Double> invcfd = new HashMap<Double, Double>();
		Double min = 0d, max = -1d;
		TreeSet<Double> ordered = new TreeSet<Double>();
		for (Double d : samples.keySet())
		{
			ordered.add(d); // order the samples
		}
		Iterator<Double> keys = ordered.iterator();
		while (keys.hasNext())
		{
			Double k = keys.next();
			Double n = cfd.get(k);
			if (n != null)
			{
				// n=n*100/number;
				// System.out.println("inv key "+n+"value "+k);
				invcfd.put(n, k);
			}

		}

		ordered.clear();
		for (Double d : invcfd.keySet())
		{
			ordered.add(d); // order the samples
		}
		min = ordered.floor(percentile);
		if (min == null)
			min = 0D;
		// System.out.println("min 95th "+min);
		max = ordered.ceiling(percentile);
		if (max == null)
			max = min;
		// System.out.println("max 95th "+max);
		Double p1 = invcfd.get(min);
		Double p2 = invcfd.get(max);
		if (p1 == null | p2 == null)
			return 0;
		if ((max - min) <= 0.0d)
			return p1;
		p = (p2 - p1) / (max - min) * (percentile - min) + p1;
		// System.out.println(p1+" "+p+" "+p2);
		return p;
	}

	/**
	 * Calculates the modal value assuming the distribution of all samples are a
	 * perfect log normal graph
	 * 
	 * @return The response distributions modal value
	 */
	public double getOperationTimeLogMode()
	{
		if(number==0)return 0d;
		if(number==1)return getAvgtime();
		return Math
				.exp(Math.log(getOperationTimeLogMedian())
						- ((sumLogSq - 2.0d
								* Math.log(getOperationTimeLogMedian())
								* sumLog + number
								* Math.pow(
										Math.log(getOperationTimeLogMedian()),
										2.0d)) / (number - 1)));
	}

	public double getHmean()
	{
		if (number == 0)
			return 0d;
		else
			return number / sumHarmonic;
	}

	public double getGmean()
	{
		if (number == 0)
			return 0d;
		else
			return Math.exp(sumLog / number);
	}

	/**
	 * The (sample) standard deviation of all the response events
	 * 
	 * @return standard deviation
	 */
	public double getOperationTimeStdDev()
	{
		// System.out.println(getOperationName()+" "+sumSqAvgTime+" "+avgtime+" "+sumAvgTime+" "+number);
		if (number < 2)
			return 0;

		return Math
				.sqrt((sumSqAvgTime - 2.0d * (double) avgtime * sumAvgTime + (double) number
						* (double) avgtime * (double) avgtime)
						/ ((double) number - 1.0d));
	}

	/**
	 * Calculates the 95 Percentile of the response events assuming a log normal
	 * distribution
	 * 
	 * @return The 95th Percentile
	 */
	public double get95Percentile()
	{
		return avgtime + 1.65 * getOperationTimeStdDev();
	}

	/**
	 * Calculates the 98.5th Percentile of the response events assuming a log
	 * normal distribution
	 * 
	 * @return The 98.5th Percentile
	 */
	public double get985Percentile()
	{
		return avgtime + 2.1 * getOperationTimeStdDev();
	}

	/**
	 * Adjusts each consumer by a number of millisecs
	 * 
	 * @param adjtime
	 *            The number of millisecs to adjust each consumer
	 */
	public void adjConsumerTimes(double adjtime)
	{
		Iterator<Service> it = consumers.iterator();
		while (it.hasNext())
		{
			Service consumer = it.next();

			consumer.adjTime(adjtime * consumer.getIterations().get(this));
		}
		notifyEvents();
	}

	/**
	 * Adjusts this service by the number of millisecs specified. These
	 * adjustments are kept separately and do not affect calculations from
	 * response events. The adjustments are then replicated up to all of this
	 * services consumers
	 * 
	 * @param adjtime
	 */
	public void adjTime(double adjtime)
	{
		adjustments += adjtime;
		// System.out.println(sName+": "+adjustments);
		Iterator<Service> it = consumers.iterator();
		while (it.hasNext())
		{
			Service consumer = it.next();

			consumer.adjTime(adjtime * consumer.getIterations().get(this));
		}
		notifyEvents();
	}

	/**
	 * Calculates the current time based on the average and adjustments
	 * 
	 * @return average-adjustments
	 */
	public float getOperationTime()
	{
		return avgtime - adjustments;
	}

	private HashMap<Service, Double> iterations = new HashMap<Service, Double>();
	private String type = BW_TYPE;
	private String service = "";
	private String operation = "";
	private String addData = "";
	private boolean modelled = true;

	private String sName = "";
	private int number = 0;
	private float avgtime = 0, adjustments = 0, explicitAdjustment = 0;
	private double sumSqAvgTime = 0, sumAvgTime = 0;
	private double sumLog = 0, sumLogSq = 0;
	private List<Service> dependents = new ArrayList<Service>();
	private List<Service> consumers = new ArrayList<Service>();

	private HashMap<Double, Double> samples = new HashMap<Double, Double>();
	

	public HashMap<Double, Double> getSamples()
	{
		return samples;
	}

	/**
	 * unused.....
	 * 
	 * @param modelled
	 */
	public void setModelled(boolean modelled)
	{
		this.modelled = modelled;
	}

	/**
	 * unused....
	 * 
	 * @return
	 */
	public boolean isModelled()
	{
		return modelled;
	}

	/**
	 * Gets the subsystem type of this service
	 * 
	 * @return The subsystem type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * Gets the current number of samples for this service object
	 * 
	 * @return the response sample count
	 */
	public int getCount()
	{
		return number;
	}

	/**
	 * Gets the service name of the service.operation (currently only works for
	 * the long form of the constructor).
	 * 
	 * @return The service name
	 */
	public String getService()
	{
		return service;
	}

	/**
	 * Gets the operation name of the service.operation (currently only works
	 * for the long form of the constructor).
	 * 
	 * @return
	 */
	public String getOperation()
	{
		return service;
	}

	/**
	 * Gets the additional data
	 * 
	 * @return
	 */
	public String getAddData()
	{
		return addData;
	}

	public void setAddData(String data)
	{
		addData = data;
	}

	public void addDataTags(String data)
	{
		if (addData == null || addData.length() == 0)
			setAddData(data);
		else
		{
			String[] split = data.split(",");
			List<String> a = Arrays.asList(split);
			for (String s : a)
			{
				if (!addData.contains(s))
					addData += "," + s;
				// System.out.println("added tag");
			}
		}
	}

	/**
	 * Gets the "service.operation" name
	 * 
	 * @return Returns the service and operation in the form "service.operation"
	 */
	public String getSName()
	{
		return sName;
	}

	/**
	 * Gets the "service.operation" name (don't ask)
	 * 
	 * @return Returns the service and operation in the form "service.operation"
	 */
	public String getsName()
	{
		return sName;
	}

	/**
	 * Gets the operation name part of the service.operation
	 * 
	 * @return The service name
	 */
	public String getOperationName()
	{
		// System.out.println("Spliiting '"+sName+"'");
		String s[] = sName.split("\\.");
		// System.out.println(s.length);
		if (s.length > 1)
		{
			if (s[s.length - 1].equals("0"))
				return s[s.length - 2] + "." + s[s.length - 1];
			else
				return s[s.length - 1];
		} else
		{
			return "length<=1";
		}
	}

	/**
	 * Gets the service name part of the service.operation
	 * 
	 * @return the service name
	 */
	public String getServiceName()
	{
		String o = "." + getOperationName();
		return sName.substring(0, sName.indexOf(o));
		// return service;
	}

	/**
	 * Gets the current average time of all the response samples
	 * 
	 * @return The average response time
	 */
	public float getAvgtime()
	{
		return avgtime;
	}

	/**
	 * The explicit adjustments are those that have been made to the service
	 * directly and excludes those that have been replicated up through the
	 * consumer ripple (implicit adjustments)
	 * 
	 * @return The adjustments to the service response time
	 */
	public float getExplicitAdjustments()
	{
		return explicitAdjustment;
	}

	/**
	 * Gets the value of all adjustments (explicit and implicit)
	 * 
	 * @return
	 */
	public float getAdjustments()
	{
		return adjustments;
	}

	/**
	 * Sets an explicit adjustment to the service response time
	 * 
	 * @param adjustments
	 */
	public void setAdjustments(float adjustments)
	{
		float s = explicitAdjustment;
		this.explicitAdjustment = adjustments;
		this.adjTime(-s + adjustments);
	}

	/**
	 * Returns the sum of all the samples adjusted by the explicit and implicit
	 * adjustments
	 * 
	 * @return Sum of response time of samples - adjustments* count
	 */
	public float getTotalTime()
	{
		return number * (avgtime - adjustments);
	}

	/**
	 * Outputs the service time data to standard out in the following format
	 * service.operation, average, average-adjustments
	 */
	public void printTimes()
	{
		if (adjustments != 0)
			System.out.println(sName + ", " + avgtime + ", "
					+ (avgtime - adjustments));
	}

	/**
	 * Prints this services consumers to standard out
	 */
	public void printConsumers()
	{
		Iterator<Service> it = consumers.iterator();
		while (it.hasNext())
		{
			System.out.println(it.next().getSName() + " consumes" + sName);
		}
	}

	/**
	 * Obtains the dependent services of this service
	 * 
	 * @return Iterator over the dependent services
	 */
	public Iterator<Service> getDependents()
	{
		return dependents.iterator();
	}

	/**
	 * Obtains the consuming services of this service
	 * 
	 * @return
	 */
	public Iterator<Service> getConsumers()
	{
		return consumers.iterator();
	}

	/**
	 * Adds a listener to be notified when a time change occurs for this service
	 * 
	 * @param sl
	 *            The Listener
	 */
	public void addServiceListener(ServiceListener sl)
	{
		SL.add(sl);
	}

	/**
	 * Notifies each of the service listeners that the service time has changed
	 */
	public void notifyEvents()
	{
		
		if (!SL.isEmpty())
		{
			Iterator<ServiceListener> i = SL.iterator();
			while (i.hasNext())
			{
				i.next().handleTimeChange(this);
			}
		}
	}

	private List<ServiceListener> SL = new CopyOnWriteArrayList<ServiceListener>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 * 
	 * Compares two services based on the current operation time
	 */
	@Override
	public int compareTo(Object arg0)
	{
		// TODO Auto-generated method stub
		if (this.getOperationTime() < ((Service) arg0).getOperationTime())
			return 1;
		if (this.getOperationTime() > ((Service) arg0).getOperationTime())
			return -1;
		return 0;
	}

	/**
	 * Returns the count of the number of consuming services
	 * 
	 * @return number of consuming services
	 */
	public int getConsumerCount()
	{
		return consumers.size();
	}

	/**
	 * Returns the count of the number of dependent services
	 * 
	 * @return the count
	 */
	public int getDependentCount()
	{
		return dependents.size();
	}

	/**
	 * Recursively walks the service tree to find the generational layer that
	 * has the maximum number of services. It effectively lays the tree out in a
	 * grid and determines the height
	 * 
	 * @param currentHeight
	 *            The maximum height at the previous layers
	 * @return the new maximum height
	 */
	public int getDependentsMaxLayerHeight(int currentHeight)
	{
		if (dependents.size() == 0)
			return currentHeight;
		else
		{
			int j = Math.max(dependents.size(), 1);
			int height = 0;
			Iterator<Service> it = dependents.iterator();
			while (it.hasNext())
			{
				Service so = it.next();
				// System.out.println(so.sName+" "+so.getDependentCount());
				height = height + so.getDependentCount();

			}
			height = Math.max(height, j);
			return Math.max(height, currentHeight);
		}
	}

	/**
	 * Walks the tree to see how deep the dependency hierarchy goes from this
	 * point
	 * 
	 * @return the maximum depth following all branches to their conclusion
	 */
	public int getDependentDepth()
	{
		if (dependents.size() == 0)
		{
			// System.out.println("Service: "+sName+", depth: 0");
			return 0;
		} else
		{
			Iterator<Service> it = dependents.iterator();
			int i = 1, j = 0, d[] = new int[dependents.size()];
			while (it.hasNext())
			{
				d[j] = it.next().getDependentDepth();
				j = j + i;
			}
			for (j = 0; j < dependents.size(); j++)
			{
				i = Math.max(i, d[j]);
			}
			// System.out.println("Service: "+sName+", depth: "+i);
			return i + 1;
		}
	}

}