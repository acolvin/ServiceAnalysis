package serviceAnalysisModel;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

public class ServiceMapper
{

	public ServiceMapper()
	{

	}

	private static CopyOnWriteArraySet<String> matchStringsService = new CopyOnWriteArraySet<String>();
	private static ConcurrentHashMap<String, String> patternsService = new ConcurrentHashMap<String, String>();
	private static ConcurrentHashMap<String, String> replacementsService = new ConcurrentHashMap<String, String>();
	private static CopyOnWriteArraySet<String> matchStringsOperation = new CopyOnWriteArraySet<String>();
	private static ConcurrentHashMap<String, String> patternsOperation = new ConcurrentHashMap<String, String>();
	private static ConcurrentHashMap<String, String> replacementsOperation = new ConcurrentHashMap<String, String>();

	public static final int SERVICE = 1;
	public static final int OPERATION = 2;
	
	public static String getMatchString(int row)
	{
		String value =null;
		String array[]=new String[getMatchCount()];
		if(row<getMatchCount())
		{
			if(row>=matchStringsService.size())
			{
				value=matchStringsOperation.toArray(array)[row-matchStringsService.size()];
			}
			else
				value=matchStringsService.toArray(array)[row];
		}
		//System.out.println("Match: "+value);
		return value;
	}
	
	public static int getType(int row)
	{
		if(row>=patternsService.size()) return OPERATION;
		else return SERVICE;
	}
	
	public static int moveMatch(int row, int newrow)
	{
		int type=getType(row);
		String match=getMatchString(row);
		String pattern=getPatternString(row);
		String replacement=getReplacementString(row);
		//ADD TESTS HERE - IMPORTANT 
		if(type==OPERATION)newrow-=matchStringsService.size();
		removeMatch(match,type);		
		int i=addMatch(match,pattern,replacement,type,newrow);
		if(type==OPERATION)i+=matchStringsService.size();
		return i;
	}
	
	public static String getPatternString(int row)
	{
		String value =null;
		String key=getMatchString(row);
		if(row>=patternsService.size())
			value=patternsOperation.get(key);
		else
			value=patternsService.get(key);
		//System.out.println("Search: "+value);
		return value;
	}
	
	public static String getReplacementString(int row)
	{
		String value =null;
		String key=getMatchString(row);
		if(row>=replacementsService.size())
			value=replacementsOperation.get(key);
		else
			value=replacementsService.get(key);
		return value;
	}
	
	public static int getMatchCount()
	{
		return matchStringsService.size()+matchStringsOperation.size();
	}

	public static int addMatch(String match, String pattern,
			String replacement, int type,int order)
	{
		if(order<0)order=0;
		int res=0;
		if (type == SERVICE)
		{
			
			if(order>=matchStringsService.size())
			{
				addMatch(match,pattern,replacement,type);
				res=matchStringsService.size()-1;
			}
			else
			{
				res=order;
				String[] t=matchStringsService.toArray(new String[0]);
				matchStringsService.clear();
				for(int i=0;i<t.length;i++)
				{
					if(i==order) {
						matchStringsService.add(match);
						matchStringsService.add(t[i]);
					}
					else
						matchStringsService.add(t[i]);
				}
				
			}
			
			patternsService.put(match, pattern);
			replacementsService.put(match, replacement);
		} else if (type == OPERATION)
		{
			if(order>=matchStringsOperation.size())
			{
				res=matchStringsOperation.size();
				addMatch(match,pattern,replacement,type);
			}
			else
			{
				res=order;
				String[] t=matchStringsOperation.toArray(new String[0]);
				matchStringsOperation.clear();
				for(int i=0;i<t.length;i++)
				{
					if(i==order) {
						matchStringsOperation.add(match);
						matchStringsOperation.add(t[i]);
					}
					else
						matchStringsOperation.add(t[i]);
				}
			}
			
			patternsOperation.put(match, pattern);
			replacementsOperation.put(match, replacement);
		}
		return res;

	}
	
	public static void addMatch(String match, String pattern,
			String replacement, int type )
	{
		if (type == SERVICE)
		{
			matchStringsService.add(match);
			patternsService.put(match, pattern);
			replacementsService.put(match, replacement);
		} else if (type == OPERATION)
		{
			matchStringsOperation.add(match);
			patternsOperation.put(match, pattern);
			replacementsOperation.put(match, replacement);
		}

	}
	
	public static void removeMatch(String match, int type)
	{
		if (type == SERVICE)
		{
			matchStringsService.remove(match);
			patternsService.remove(match);
			replacementsService.remove(match);
		} else if (type == OPERATION)
		{
			matchStringsOperation.remove(match);
			patternsOperation.remove(match);
			replacementsOperation.remove(match);
		}

	}

	public static String replace(String s, int type)
	{
		String ss = s;

		if (type == SERVICE)
		{
			for (String m : matchStringsService)
			{
				if (ss.matches(m))
				{
					 //System.out.println(ss+" and "+m + " match");
					ss = ss.replaceAll(patternsService.get(m),
							replacementsService.get(m));

				} else
				{
					 //System.out.println(ss+" and "+m + " do not match");
				}
			}
		}

		if (type == OPERATION)
		{
			for (String m : matchStringsOperation)
			{
				if (ss.matches(m))
				{
					ss = ss.replaceAll(patternsOperation.get(m),
							replacementsOperation.get(m));
				}
			}
		}
		// System.out.println("Returned string="+ss);
		return ss;
	}

	public static Service matchService(Service service)
	{
		String sername = service.getServiceName();
		String opname = service.getOperationName();
		sername = replace(sername, SERVICE);
		opname = replace(opname, OPERATION);
		return new Service(service.getType(), sername, opname, "");
	}

	public static void readMatchFile(String filename)
			throws FileNotFoundException, IOException
	{
		FileReader fr;
		String line;
		int linenumber = 1;
		fr = new FileReader(filename);
		BufferedReader br = new BufferedReader(fr);
		while ((line = br.readLine()) != null)
		{ // format type,match,pattern,replacement
			if (linenumber == 1 && !line.equalsIgnoreCase("###Matches###"))
			{
				System.out.println("Not a Match File");
				br.close();
				fr.close();
				return;
			} else if (linenumber > 1)
			{
				String[] lineparts = line.split("\\,", 4);
				int type = -1;
				if (lineparts[0].equalsIgnoreCase("SERVICE"))
					type = SERVICE;
				if (lineparts[0].equalsIgnoreCase("OPERATION"))
					type = OPERATION;
				addMatch(lineparts[1], lineparts[2], lineparts[3], type);
			}
			linenumber++;
		}
		br.close();
		fr.close();

	}
	public static void saveMatchFile(OutputStream out) throws IOException
	{
		for(String s: matchStringsService)
		{
			String pattern=patternsService.get(s);
			String replacement=replacementsService.get(s);
			if(pattern!=null && replacement!=null)
			{
				String o="SERVICE,"+s+","+pattern+","+replacement+"\n";
				out.write(o.getBytes());
			}
			
		}
		for(String s: matchStringsOperation)
		{
			String pattern=patternsOperation.get(s);
			String replacement=replacementsOperation.get(s);
			if(pattern!=null && replacement!=null)
			{
				String o="OPERATION,"+s+","+pattern+","+replacement+"\n";
				out.write(o.getBytes());
			}
			
		}
	}
}
