package serviceAnalysisModel;

import java.io.FileInputStream;
import java.util.zip.GZIPInputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.util.zip.GZIPOutputStream;
import java.io.ObjectOutputStream;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Iterator;
import java.util.List;
//import java.util.Map;
//import java.util.Map.Entry;
//import java.util.StringTokenizer;

//import serviceAnalysisModel.readersWriters.ServiceEntry;

/**
 * Responsible for reading and processing a model file. This file can contain
 * the complete set of services for all subsystem types, initial times for each
 * service, iterations, dependencies, volumetric and workstream model components
 * 
 * @author apc
 * 
 */
public class ModelFileReader
{

	/**
	 * Replacement for the TFileReader. As of yet unused
	 * 
	 * @param filename
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static List<Service> LoadTibcoModel(String filename)
			throws FileNotFoundException, IOException
	{
		List<Service> serviceList = new ArrayList<Service>();

		String nextLine = "";
		String service = "";
		String operation = "";
		String addData = "";
		int line = 0;
		Service parentService = null;

		BufferedReader reader = new BufferedReader(new FileReader(filename));
		Boolean found = false;

		while ((nextLine = reader.readLine()) != null)
		{
			line++;

			found = false;
			service = null;
			operation = null;
			if (nextLine.contains("<sharedConfig>"))
			{
			} else if (parentService != null && nextLine.contains("\t"))
			{
				String fields[] = nextLine.split("/");
				service = fields[4].toUpperCase();

				for (int index = 5; index < fields.length; index++)
				{
					if (!found)
					{
						if (fields[index].contentEquals("JMS")
								|| fields[index].contentEquals("HTTP")
								|| fields[index].contains("WaitNotifyWrapper"))
						{
							addData = fields[index];
							found = true;
							operation = fields[index + 1];
							if (operation.lastIndexOf(".process") > 0)
								operation = operation.substring(0,
										operation.lastIndexOf(".process"));
							operation = operation.toLowerCase();
						}
					}
				}

				if (found)
				{
					Service subService = new Service(Service.BW_TYPE, service,
							operation, addData);
					int subIndex = serviceList.indexOf(subService);
					if (subIndex >= 0)
					{
						subService = serviceList.get(subIndex);
					} else
					{
						serviceList.add(subService);
					}

					parentService.addDependentService(subService);
					subService.addConsumingService(parentService);
				}

			} else if (!nextLine.contains("HelperFunc"))
			{// service line
				String fields[] = nextLine.split("/"); // split the line based
														// on /
				if (fields.length < 3)
					System.out.println(line + ": " + nextLine);
				service = fields[3].toUpperCase();
				for (int index = 4; index < fields.length; index++)
				{
					if (!found)
					{
						// do nothing if we have finished processing
						if (fields[index].contains("Processes")
								|| fields[index].contentEquals("SubProcess")
								|| fields[index].contentEquals("MainProcess")
								|| fields[index].contentEquals("Main"))
						{
							operation = fields[index - 1].toLowerCase();
							found = true;
							if (operation.lastIndexOf(".process") > 0)
								operation = operation.substring(0,
										operation.lastIndexOf(".process"));
						} else if (fields[index].contentEquals("JMS")
								|| fields[index].contentEquals("HTTP")
								|| fields[index].contains("WaitNotifyWrapper"))
						{
							addData = fields[index];
							operation = fields[index + 1].toLowerCase();
							found = true;
							if (operation.lastIndexOf(".process") > 0)
								operation = operation.substring(0,
										operation.lastIndexOf(".process"));
						}
					}
				}

				if (!found)
				{
					parentService = null;
					// System.out.println("unhandled pattern: " + s);
				} else
				{
					Service newService = new Service(Service.BW_TYPE, service,
							operation, addData);

					int servIndex = serviceList.indexOf(newService);
					if (servIndex >= 0)
					{
						newService = serviceList.get(servIndex);
					} else
					{
						serviceList.add(newService);
					}

					parentService = newService;

				}
			}
		}

		reader.close();

		return serviceList;
	}

	/**
	 * Replacement for timing file load in TFileReader. As yet unused
	 * 
	 * @param type
	 * @param filename
	 * @param serviceList
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void loadPerformanceTimings(String type, String filename,
			List<Service> serviceList) throws FileNotFoundException,
			IOException
	{
		String nextLine;
		String servicename, serviceoperation;
		float millisecs = 0;
		BufferedReader br = new BufferedReader(new FileReader(filename));

		while ((nextLine = br.readLine()) != null)
		{
			String fields[] = nextLine.split(",");
			servicename = fields[0].substring(0, fields[0].lastIndexOf("."))
					.trim().toUpperCase();
			serviceoperation = fields[0]
					.substring(fields[0].lastIndexOf(".") + 1).trim()
					.toLowerCase();
			millisecs = Float.valueOf(fields[1].trim());

			Service newService = new Service(type, servicename,
					serviceoperation, "");
			int servIndex;
			if ((servIndex = serviceList.indexOf(newService)) >= 0)
			{
				serviceList.get(servIndex).addTime(millisecs);
			} else
			{
				newService.setModelled(false);
				newService.addTime(millisecs);
				serviceList.add(newService);
			}
		}
		br.close();

	}

	/**
	 * Replacement for Adjustment file loader in TFileReader. As yet unused
	 * 
	 * @param filename
	 * @param serviceList
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void loadAdjustmentTimings(String filename,
			List<Service> serviceList) throws FileNotFoundException,
			IOException
	{
		String servicetype, servicename, serviceoperation;
		String nextLine;
		float millisecs = 0;
		BufferedReader br = new BufferedReader(new FileReader(filename));

		while ((nextLine = br.readLine()) != null)
		{
			String fields[] = nextLine.split(",");
			servicetype = fields[0];
			servicename = fields[1].substring(0, fields[1].lastIndexOf("."))
					.trim().toUpperCase();
			serviceoperation = fields[1]
					.substring(fields[1].lastIndexOf(".") + 1).trim()
					.toLowerCase();
			millisecs = Float.valueOf(fields[2].trim());

			Service newService = new Service(servicetype, servicename,
					serviceoperation, "");
			int servIndex;
			if ((servIndex = serviceList.indexOf(newService)) >= 0)
			{
				serviceList.get(servIndex).setAdjustments(millisecs);
			} else
			{
				newService.setModelled(false);
				newService.setAdjustments(millisecs);
				serviceList.add(newService);
			}
		}

		br.close();
	}

	/**
	 * Serialisation file load routine. As yet cannot be called from UI
	 * 
	 * @param filename
	 * @return
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public static List<Service> loadSerializedModel(String filename)
			throws FileNotFoundException, ClassNotFoundException, IOException
	{
		FileInputStream fis = new FileInputStream(filename);
		GZIPInputStream gzis = new GZIPInputStream(fis);
		ObjectInputStream in = new ObjectInputStream(gzis);

		List<Service> serviceList = (List<Service>) in.readObject();
		in.close();

		return serviceList;
	}

	/**
	 * Serialisation file save routine. Cannot be driven from UI yet
	 * 
	 * @param serviceList
	 * @param filename
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static void saveSerializedModel(List<Service> serviceList,
			String filename) throws FileNotFoundException,
			ClassNotFoundException, IOException
	{
		FileOutputStream fos = new FileOutputStream(filename);
		GZIPOutputStream gzos = new GZIPOutputStream(fos);
		ObjectOutputStream out = new ObjectOutputStream(gzos);

		out.writeObject(serviceList);
		out.flush();
		out.close();
	}

	/**
	 * This method loads a model file into the system. Model file definition is
	 * ....
	 * 
	 * @param filename
	 * @param serviceList
	 * @param vm
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void LoadNewModel(String filename, List<Service> serviceList,
			VolumetricModel vm) throws FileNotFoundException, IOException
	{
		String nextLine = "";
		String recType = "";
		String layer = "";
		String service = "";
		String operation = "";
		String subLayer = "";
		String subService = "";
		String subOperation = "";
		String subProtocol = "";
		// String subAsync = "";
		// int line=0;
		// Service parentService = null;

		BufferedReader reader = new BufferedReader(new FileReader(filename));
		// Boolean found=false;

		while ((nextLine = reader.readLine()) != null)
		{
			if (nextLine.startsWith("MODELTIME,"))
			{
				// Service servObj;
				String s[] = nextLine.split(",");
				if ((s.length == 5 || s.length == 6))
				{
					layer = s[1].toUpperCase();
					String servicename = s[2].toUpperCase();
					String operationname = s[3].toLowerCase();
					double time = new Double(s[4]);
					double adjustment = 0;
					if (time < 0)
						time = -1;
					if (s.length == 6)
						adjustment = new Double(s[5]);
					if (!layer.equalsIgnoreCase("WS"))
					{
						Service srvObj = new Service(layer, servicename,
								operationname, "");
						// System.out.println("servicename="+servicename);
						// System.out.println("operationname="+operationname);
						// servObj = new Service(layer, servicename,
						// operationname, "");
						// return new ServiceEntry(servObj,time,adjustment);
						if (serviceList.contains(srvObj))
						{
							// System.out.println("Found Service");
							srvObj = serviceList.get(serviceList
									.indexOf(srvObj));
						} else
						{
							// System.out.println("Adding Service");

							serviceList.add(srvObj);
							// service.addServiceListener(serviceAnalysisUI.ServiceTable.model);
							// serviceAnalysisUI.ServiceTable.model.fireTableDataChanged();

						}
						if (time >= 0)
						{
							srvObj.addTime((float) time);
						}
						if (adjustment != 0) // just set adjustment
						{
							srvObj.setAdjustments((float) adjustment);
						}

					}
				}
			}
			if (!nextLine.startsWith("#") && !nextLine.startsWith("MODELTIME,"))

			{
				String[] split = nextLine.split(",", 9);

				recType = split[0];
				layer = split[1];
				service = split[2].toUpperCase();
				operation = split[3].toLowerCase();
				if (!split[4].equals(""))
				{
					subLayer = split[4];
					subService = split[5].toUpperCase();
					subOperation = split[6].toLowerCase();
					subProtocol = split[7];
					// subAsync = split[8];
				} else
				{
					subLayer = null;
				}

				if (recType.equalsIgnoreCase("MODEL")
						&& !layer.equalsIgnoreCase("VOL"))
				{
					Service servObj;
					if (layer.equalsIgnoreCase("WS"))
						servObj = new Workstream(operation);
					else
						servObj = new Service(layer, service, operation, "");
					// Service servObj = new Service(service + "." + operation);

					if (serviceList.contains(servObj))
					{
						servObj = serviceList.get(serviceList.indexOf(servObj));
					} else
					{
						serviceList.add(servObj);
						if (layer.equalsIgnoreCase("WS"))
							servObj.addServiceListener(vm);
					}

					if ((subLayer != null) && (subLayer.length() > 0))
					{
						Service subServObj;
						if (subLayer.equalsIgnoreCase("WS"))
						{
							subServObj = new Workstream(subOperation);
							// System.out.println("workstream found "+
							// subServObj);
						} else
							subServObj = new Service(subLayer, subService,
									subOperation, "");
						// Service subServObj = new Service(subService + "." +
						// subOperation);

						if (serviceList.contains(subServObj))
						{
							subServObj = serviceList.get(serviceList
									.indexOf(subServObj));
						} else
						{
							System.out.println("UI Sub Service Not Found ("
									+ subService + "." + subOperation + ")");
							serviceList.add(subServObj);
							if (subLayer.equalsIgnoreCase("WS"))
								subServObj.addServiceListener(vm);
						}

						servObj.addDependentService(subServObj);
						// subServObj.addConsumingService(servObj);
					}
				} else if (recType.equalsIgnoreCase("MODEL")
						&& layer.equalsIgnoreCase("VOL"))
				{
					// volumetric model
					VolumetricType vt = new VolumetricType(operation);
					vm.addType(vt); // you can add multiple times it make no
									// difference
					vt = vm.contains(operation); // get the original if it is a
													// duplicate
					if (subLayer != null)
					{
						// System.out.println("Adding Workstream "+ subOperation
						// +" to "+vt.getName());
						Workstream w = new Workstream(subOperation);
						if (serviceList.contains(w))
						{
							w = (Workstream) serviceList.get(serviceList
									.indexOf(w));
							// System.out.println("workstream found " +
							// w.getSName());
						} else
						{
							serviceList.add(w);
						}
						vt.addWorkstream(w);
					}
				} else if (recType.equalsIgnoreCase("ITERATION"))
				{
					// handle iteration rows in here
					if (layer.equalsIgnoreCase("VOL"))
					{
						// volumetrics
						VolumetricType vt = vm.contains(operation);
						if (vt == null)
						{
							System.out.println("VolumetricType " + operation
									+ " does not exist");
						} else
						{
							Integer vol = Integer.valueOf(subLayer);
							vm.setVolume(vt, vol);
						}
					} else if (layer.equalsIgnoreCase("SER"))
					{
						// Service iteration
						if (service.equalsIgnoreCase("VOLUMETRIC"))
						{// V model multipliers
							VolumetricType vt = vm.contains(operation);
							Workstream w = new Workstream(subOperation);
							if (serviceList.contains(w))
							{
								w = (Workstream) serviceList.get(serviceList
										.indexOf(w));
								// System.out.println("workstream found " +
								// w.getSName());
							}
							vt.setWorkstreamMultiplier(w,
									Double.valueOf(subProtocol));
						}

					} else
					// Service2Service multiplier
					{
						Service servObj, subServObj;
						servObj = new Service(layer, service, operation, "");
						subServObj = new Service(subLayer, subService,
								subOperation, "");
						if (serviceList.contains(servObj))
						{
							servObj = serviceList.get(serviceList
									.indexOf(servObj));
						}
						if (serviceList.contains(subServObj))
						{
							subServObj = serviceList.get(serviceList
									.indexOf(subServObj));
						}

						servObj.addDependentIterations(subServObj,
								Double.valueOf(subProtocol));

					}
				}
			}
		}
		reader.close();
	}

	public static void loadNewPerformanceTimings(String filename,
			List<Service> serviceList) throws FileNotFoundException,
			IOException
	{
		String nextLine;
		// String servicename, serviceoperation;
		BufferedReader br = new BufferedReader(new FileReader(filename));

		while ((nextLine = br.readLine()) != null)
		{
			String fields[] = nextLine.split(",", 7);
			// String recType = fields[0];
			String layer = fields[1];
			// String dateTime = fields[2];
			String service = fields[3].toUpperCase();
			String operation = fields[4].toLowerCase();
			float evalTime = Float.parseFloat(fields[5]);
			float waitTime = Float.parseFloat(fields[6]);

			Service newService = new Service(layer, service, operation, "");

			int servIndex = serviceList.indexOf(newService);
			if (servIndex >= 0)
			{
				serviceList.get(servIndex).addTime(evalTime + waitTime);
			} else
			{
				newService.setModelled(false);
				newService.addTime(evalTime + waitTime);
				serviceList.add(newService);
			}
		}

		br.close();

	}

}
