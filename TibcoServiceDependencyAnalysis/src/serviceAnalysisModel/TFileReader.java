package serviceAnalysisModel;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SimpleTimeZone;
import java.util.Map.Entry;

import serviceAnalysisModel.SAControl.Controller;

/**
 * This class is used to read Tibco BW file components such as job stats and the
 * initial service load file It is mandatory as it initialises all the variable
 * for other readers.
 * 
 * @author apc
 * 
 */
public class TFileReader
{

	/**
	 * Process a BW stats file uploading the time events into memory on start up
	 * 
	 * @param filename
	 *            The file containing the stats
	 * @throws FileNotFoundException
	 *             Thrown when the file cannot be found
	 * @throws IOException
	 *             Thrown when the file cannot be read for whatever reason
	 */
	@SuppressWarnings("resource")
	public void ProcessJobStatsFile(String filename)
			throws FileNotFoundException, IOException
	{
		FileReader fr;

		Connection con = null;
		try
		{
			con = DriverManager.getConnection(
					Controller.DBConnectionStr,
					Controller.DBUser,
					Controller.DBPasswd);
		} catch (SQLException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();

		}

		// System.out.println("loading file");
		String servicename, serviceoperation, time, s;
		float millisecs = 0;
		// Service so=null;
		fr = new FileReader(filename);
		BufferedReader br = new BufferedReader(fr);
		while ((s = br.readLine()) != null)
		{
			if (!s.startsWith("jobId,"))
			{
				String b[] = s.split(",");
				if (b[6].trim().equals("success"))
				{
					if (!(s.contains("starterProcesses")
							|| s.contains("EngineStart.process") || s
								.contains("WSInspection")))
					{
						time = b[4].trim();
						millisecs = Float.valueOf(time);
						String so[] = b[1].trim().split("/");
						if (so.length >= 4)
						{
							servicename = so[3].toUpperCase();
							if (servicename.contains("SERVICE"))
								servicename = servicename
										.replace("SERVICE", "");
							serviceoperation = so[so.length - 1];
							int r = 0;
							if (serviceoperation.startsWith("ws"))
								r = 2;
							serviceoperation = serviceoperation.substring(r,
									serviceoperation.length() - 8)
									.toLowerCase();
							if (serviceoperation.contains("_v"))
							{
								serviceoperation = serviceoperation.substring(
										0, serviceoperation.indexOf("_v"));
							}
							if (serviceoperation.contains("getreferencedata"))
								serviceoperation = "get";

							// System.out.println("reading jobstats: "+servicename+"."+serviceoperation+" running in "+millisecs);
							if (serviceOperation.contains(new Service("BW",
									servicename, serviceoperation, "")))
							{
								Service lso = serviceOperation
										.get(serviceOperation
												.indexOf(new Service("BW",
														servicename,
														serviceoperation, "")));
								lso.addTime(millisecs);
								writeDB(lso, b[2], millisecs);
							} else
							{
								if (serviceoperation.startsWith("getreference"))
								{
									serviceoperation = "get";
									servicename = "RDS_V1.0";
								}
								if (serviceoperation.contains("clocks_v"))
									serviceoperation = serviceoperation
											.split("_")[0];
								if (serviceoperation
										.contains("performcheckfordvandexam"))
									serviceoperation = "performcheckfordvandexam";
								if (serviceoperation
										.contains("performdeprecateactivebringups"))
									serviceoperation = "deprecateactivebringups";
								if (serviceoperation
										.contains("getopencashsession_v1.0"))
									serviceoperation = "getopencashsession";
								if (serviceoperation
										.contains("capturepaperapplicationjmsstarter"))
									serviceoperation = "capturepaperapplication";
								if (serviceoperation.contains("jmsstarter"))
									serviceoperation = serviceoperation
											.replace("jmsstarter", "");
								if (serviceOperation.contains(new Service("BW",
										servicename, serviceoperation, "")))
								{
									serviceOperation.get(
											serviceOperation
													.indexOf(new Service("BW",
															servicename,
															serviceoperation,
															""))).addTime(
											millisecs);
								}

								else if (servicename.length() > 0
										&& serviceoperation.length() > 0)
								{
									Service y = new Service("BW", servicename,
											serviceoperation, "");
									serviceOperation.add(y);
									y.addTime(millisecs);
									writeDB(y, b[2], millisecs);
									// add to database here
									// b[2] is request time

								} else
								{
									System.out.println("reading jobstats: "
											+ servicename + "."
											+ serviceoperation + " running in "
											+ millisecs);
									System.out.println("Not in List");

								}
							}

						}
					}
				}
			}
		}
	}

	/**
	 * Writes service events to the database (controlled by --DBEvents and can
	 * be costly) Initialises the BWReader on first use.
	 * 
	 * @param service
	 *            The Service that the events is the focus of
	 * @param date
	 *            The date/time the event happened
	 * @param response
	 *            The response time
	 */
	private void writeDB(Service service, String date, double response)
	{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		SimpleTimeZone tz = new SimpleTimeZone(0, "GMT");
		df.setTimeZone(tz);
		Date when = new Date();
		if (false)
		{// lets not write anything - delete this code

			try
			{
				when = df.parse(date);
			} catch (ParseException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try
			{
				Connection con = DriverManager.getConnection(
						Controller.DBConnectionStr,
						Controller.DBUser,
						Controller.DBPasswd);
				Statement statement = con.createStatement();
				boolean bs = statement
						.execute("select 1 from SERVICE_EVENT where SERVICE='"
								+ service.getsName() + "' and EVENT_TIME="
								+ when.getTime() + " and RESPONSE=" + response);

				if (!statement.getResultSet().next())
				{
					// System.out.println("record added");
					con.createStatement().executeUpdate(
							"insert into SERVICE_EVENT (SERVICE, EVENT_TIME, RESPONSE) values ('"
									+ service.getSName() + "',"
									+ when.getTime() + "," + response + ")");
					con.commit();
					con.close();
				} else
					con.close();
			} catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else
		{
			if (bwr == null)
				bwr = new serviceAnalysisModel.readersWriters.BWReader(
						serviceOperation);

			if (serviceAnalysisModel.readersWriters.DBWriter.DBWriteServiceEvents)
				try
				{
					Connection con = DriverManager.getConnection(
							Controller.DBConnectionStr,
							Controller.DBUser,
							Controller.DBPasswd);
					Statement statement = con.createStatement();
					statement
							.execute("select 1 from SERVICE_EVENT where SERVICE='"
									+ service.getsName()
									+ "' and EVENT_TIME="
									+ when.getTime()
									+ " and RESPONSE="
									+ response);
					if (!statement.getResultSet().next())
						bwr.DBWrite(new serviceAnalysisModel.readersWriters.ServiceEntry(
								service, when, response));
					con.close();
				} catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	/**
	 * The {@link BWReader} used throughout the system.
	 */
	private serviceAnalysisModel.readersWriters.BWReader bwr = null;

	/**
	 * Processes the Service Time File supplied on the command line. The
	 * services must exist in the service list prior to its call
	 * 
	 * @param filename
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public void ProcessTimeFile(String filename) throws FileNotFoundException,
			IOException
	{
		FileReader fr;
		String servicename, serviceoperation, time, s;
		float millisecs = 0;
		// Service so=null;
		fr = new FileReader(filename);
		BufferedReader br = new BufferedReader(fr);
		while ((s = br.readLine()) != null)
		{
			String b[] = s.split(",");
			servicename = b[0].substring(0, b[0].lastIndexOf(".")).trim()
					.toUpperCase();
			// System.out.println(servicename);
			serviceoperation = b[0].substring(b[0].lastIndexOf(".") + 1).trim()
					.toLowerCase();
			// System.out.println(serviceoperation);
			time = b[1].trim();
			millisecs = Float.valueOf(time);
			if (serviceOperation.contains(new Service("BW", servicename,
					serviceoperation, "")))
			{
				serviceOperation.get(
						serviceOperation.indexOf(new Service("BW", servicename,
								serviceoperation, ""))).addTime(millisecs);
			}
		}
	}

	/**
	 * Processes the Service Adjustment file provided on the command line. The
	 * Services must exist in the Service List prior to calling this operation
	 * 
	 * @param filename
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public void ProcessAdjFile(String filename) throws FileNotFoundException,
			IOException
	{
		FileReader fr;
		String servicename, serviceoperation, time, s;
		float millisecs = 0;
		// Service so=null;
		fr = new FileReader(filename);
		BufferedReader br = new BufferedReader(fr);
		while ((s = br.readLine()) != null)
		{
			String b[] = s.split(",");
			servicename = b[0].substring(0, b[0].lastIndexOf(".")).trim()
					.toUpperCase();
			serviceoperation = b[0].substring(b[0].lastIndexOf(".") + 1).trim()
					.toLowerCase();
			time = b[1].trim();
			millisecs = Float.valueOf(time);
			serviceOperation.get(
					serviceOperation.indexOf(new Service("BW", servicename,
							serviceoperation, ""))).setAdjustments(millisecs);
		}

	}

	/**
	 * Prints the service times from the service list to standard out
	 */
	public void PrintTimes()
	{
		System.out.println("Service.operation, Original Time, Adjusted Time");
		Iterator<Service> i = serviceOperation.iterator();
		while (i.hasNext())
		{
			i.next().printTimes();
		}
	}

	/**
	 * Processes the mandatory service file provided on the command line. The
	 * format of this file is specific to that grabbed from our BW projects
	 * which have been pre-processed by a set of sed and grep commands to
	 * flatten the xml This method should be deprecated for usage outside this
	 * project
	 * 
	 * @param filename
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public void ProcessFile(String filename) throws FileNotFoundException,
			IOException
	{
		FileReader fr;
		String s, service, operation;
		int line = 0;
		String previousSO = "", previousOO = "";

		if (filename == null | filename == "")
			return;

		fr = new FileReader(filename);

		BufferedReader br = new BufferedReader(fr);
		Boolean h = false, helperFunc = false;

		while ((s = br.readLine()) != null)
		{
			line++;
			// System.out.println(line);
			h = false; // dependency found in string
			service = null;
			operation = null;
			if (s.contains("<sharedConfig>"))
			{
			} else if (s.contains("\t") && !helperFunc)
			{// dependency line
				// System.out.println("dependency line: " +s);
				int i = serviceCallingTimes.get(previousSO + "." + previousOO);
				i++;
				serviceCallingTimes.put(previousSO + "." + previousOO, i);

				// what service is being called??

				String b[] = s.split("/");
				service = b[4].toUpperCase();
				if (service.contains("SERVICE"))
					service = service.replace("SERVICE", "");
				h = false;
				for (int ii = 5; ii < b.length; ii++)
				{
					if (!h)
						if (b[ii].contentEquals("JMS")
								|| b[ii].contentEquals("HTTP")
								|| b[ii].contains("WaitNotifyWrapper"))
						{
							operation = b[ii + 1];
							h = true;
							if (operation.lastIndexOf(".process") > 0)
								operation = operation.substring(0,
										operation.lastIndexOf(".process"));
							operation = operation.toLowerCase();
							if (operation.contains("_v"))
							{
								operation = operation.substring(0,
										operation.indexOf("_v"));
							}
						}

				}
				if (h)
				{
					String cSS = service;
					String cOO = operation;
					String cSO = cSS + "." + cOO;
					if (serviceCalledTimes.containsKey(cSO))
					{
						serviceCalledTimes.put(cSO,
								(serviceCalledTimes.get(cSO) + 1));
					} else
					{
						serviceCalledTimes.put(cSO, 1);
						serviceCallingTimes.put(cSO, 0);
					}
					Service sv = null;

					boolean bb = serviceOperation.contains(new Service("BW",
							previousSO, previousOO, ""));
					if (bb)
					{
						sv = serviceOperation.get(serviceOperation
								.indexOf(new Service("BW", previousSO,
										previousOO, "")));
					} else
					{
						sv = new Service("BW", previousSO, previousOO, "");
						serviceOperation.add(sv);
					}
					Service ss;
					if (serviceOperation.contains(new Service("BW", service,
							operation, "")))
						ss = serviceOperation.get(serviceOperation
								.indexOf(new Service("BW", service, operation,
										"")));
					else
						ss = new Service("BW", service, operation, "");
					sv.addDependentService(ss);
					if (!serviceOperation.contains(ss))
						serviceOperation.add(ss);

				}

			} else if (s.contains("HelperFunc"))
			{
				helperFunc = true;
			} else
			{// service line
				helperFunc = false;
				String b[] = s.split("/"); // split the line based on /
				if (b.length < 3)
					System.out.println(line + ": " + s);
				service = b[3].toUpperCase();
				if (service.contains("SERVICE"))
					service = service.replace("SERVICE", "");
				for (int i = 4; i < b.length; i++)
				{
					if (!h) // do nothing if we have finished processing
						if (b[i].contains("Processes")
								|| b[i].contentEquals("SubProcess")
								|| b[i].contentEquals("MainProcess")
								|| b[i].contentEquals("Main"))
						{
							operation = b[i - 1].toLowerCase();
							if (operation.contains("_v"))
							{
								operation = operation.substring(0,
										operation.indexOf("_v"));
							}

							h = true;
						} else if (b[i].contentEquals("JMS")
								|| b[i].contentEquals("HTTP")
								|| b[i].contains("WaitNotifyWrapper"))
						{
							operation = b[i + 1].toLowerCase();
							if (operation.contains("_v"))
							{
								operation = operation.substring(0,
										operation.indexOf("_v"));
							}

							h = true;
							if (operation.lastIndexOf(".process") > 0)
								operation = operation.substring(0,
										operation.lastIndexOf(".process"));
							if (operation.contains("_v"))
							{
								operation = operation.substring(0,
										operation.indexOf("_v"));
							}

						}
				}
				if (!h)
				{
					// System.out.println("unhandled pattern: " + s);

				} else
				{
					previousSO = service;
					previousOO = operation;
					if (!serviceCalledTimes.containsKey(previousSO + "."
							+ previousOO))
					{
						serviceCalledTimes
								.put(previousSO + "." + previousOO, 0);
					}
					if (!serviceCallingTimes.containsKey(previousSO + "."
							+ previousOO))
					{
						serviceCallingTimes.put(previousSO + "." + previousOO,
								0);
						if (!serviceOperation.contains(new Service("BW",
								previousSO, previousOO, "")))
							serviceOperation.add(new Service("BW", previousSO,
									previousOO, ""));
					}
				}
			}
		}

		System.out.println("Service.operation, Service Calls, Called by");
		Iterator<Entry<String, Integer>> it = serviceCallingTimes.entrySet()
				.iterator();
		while (it.hasNext())
		{

			Entry<String, Integer> a = it.next();
			System.out.println(a.getKey() + "," + a.getValue() + ", "
					+ serviceCalledTimes.get(a.getKey()));

		}

		Iterator<Service> ti = serviceOperation.iterator();
		while (ti.hasNext())
		{
			ti.next().print();
		}

	}

	/**
	 * Returns the list of {@link Service} objects that the system is modelling
	 * 
	 * @return The {@link List} object containing the {@Service}
	 *         objects
	 */
	public List<Service> getServiceOperation()
	{
		return serviceOperation;
	}

	/**
	 * Prints the consumers of the provided service name to standard out
	 * 
	 * @param so
	 *            name of the service operation in the form "SERVICE.operation"
	 */
	public void printConsumer(String so)
	{
		String[] names = so.split(".");
		serviceOperation.get(
				serviceOperation.indexOf(new Service("BW", names[0], names[1],
						""))).printConsumers();
	}

	/**
	 * Provides a sorted list of service objects whose average response times
	 * are greater than the threshold
	 * 
	 * @param threshold
	 *            minimum response time to be included.
	 * @return
	 */
	public static List<Service> getSortedTimeList(float threshold)
	{
		List<Service> TL = new ArrayList<Service>();
		Iterator<Service> it = serviceOperation.iterator();
		while (it.hasNext())
		{
			Service s = it.next();
			if (s.getOperationTime() > threshold)
			{
				TL.add(s);
			}
		}
		Collections.sort(TL);
		return TL;
	}

	/**
	 * Produces a new {@link List} of {@link Service} objects sorted on average
	 * response times. The {@link Service} objects will have their average
	 * response times fall between the thresholds to be included in the new
	 * list.
	 * 
	 * @param minThreshold
	 * @param maxThreshold
	 * @return
	 */
	public static List<Service> getSortedTimeList(float minThreshold,
			float maxThreshold)
	{
		
		List<Service> TL = new ArrayList<Service>();
		Iterator<Service> it = serviceOperation.iterator();
		while (it.hasNext())
		{
			Service s = it.next();
			if (s.getOperationTime() > minThreshold
					&& s.getOperationTime() < maxThreshold)
			{
				TL.add(s);
			}
		}
		Collections.sort(TL);
		return TL;
	}

	/**
	 * Produces a new {@link List} of {@link Service} objects sorted on average
	 * response times. The {@link Service} objects listed will be of the type
	 * specified and the average response times will fall between the thresholds
	 * to be included in the new list.
	 * 
	 * @param minThreshold
	 * @param maxThreshold
	 * @param type
	 *            The subsystem type (BW,UI,DB,..)
	 * @return
	 */
	public static List<Service> getSortedTimeList(float minThreshold,
			float maxThreshold, String type)
	{
		List<Service> TL = new ArrayList<Service>();
		Iterator<Service> it = serviceOperation.iterator();
		while (it.hasNext())
		{
			Service s = it.next();
			if (s.getOperationTime() > minThreshold
					&& s.getOperationTime() < maxThreshold
					&& s.getType().equalsIgnoreCase(type))
			{
				TL.add(s);
			}
		}
		Collections.sort(TL);
		return TL;
	}

	/**
	 * Produces a new {@link List} of {@link Service} objects sorted by average
	 * response times
	 * 
	 * @return A new list of {@link Service} objects sorted by average response
	 *         times
	 */
	public List<Service> getSortedTimeList()
	{
		return getSortedTimeList(0);
	}

	/**
	 * Prints a sorted list of services and their average response times to
	 * standard out
	 */
	public void printServiceTimes()
	{
		System.out.println("Service.Operation time output");
		List<Service> r = getSortedTimeList();
		Iterator<Service> i = r.iterator();
		while (i.hasNext())
		{
			i.next().printTimes();
		}
	}

	private Map<String, Integer> serviceCalledTimes = new HashMap<String, Integer>();
	private Map<String, Integer> serviceCallingTimes = new HashMap<String, Integer>();
	/**
	 * Holds the list of {@link Service} objects as an {@link ArrayList}
	 */
	private static List<Service> serviceOperation = new ArrayList<Service>();

}
